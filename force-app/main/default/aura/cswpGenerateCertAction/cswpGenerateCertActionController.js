({
    init: function (cmp, helper) {
        var languageItems = [
            { label: $A.get("$Label.c.Cswp_English"), value: "en_US" },
            { label: $A.get("$Label.c.Cswp_Japanese"), value: "ja_JP" },
            { label: $A.get("$Label.c.Cswp_Korean"), value: "ko_KR"},
            { label: $A.get("$Label.c.Cswp_Chinese"), value: "zh_TW"},
            { label: $A.get("$Label.c.Cswp_ChineseSimplified"), value: "zh_CN"}];

        var sizeItems = [
            { label: $A.get("$Label.c.Cswp_A4"), value: "A4" },
            { label: $A.get("$Label.c.Cswp_Html"), value: "HTML" },
            { label: $A.get("$Label.c.Cswp_Letter"), value: "Letter" }];

        cmp.set("v.sizeOptions", sizeItems);
        cmp.set("v.languageOptions", languageItems);
    },

    handleLanguageChange: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected option
        var selectedLanguage = event.getParam("value");
        cmp.set("v.selectedLanguage", selectedLanguage);
    },

    handleSizeChange: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected option
        var selectedSize = event.getParam("value");
        cmp.set("v.selectedSize", selectedSize);
    },

    handleFullCalibrationResultChange: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected option
        var fullRes = event.getParam("checked");
        cmp.set("v.fullRes", fullRes);
    },

    generateButtonClicked: function (cmp, event, helper) {
        // This will contain the string of the "value" attribute of the selected option
        console.log("generate button clicked");
        var selectedLanguage = cmp.get("v.selectedLanguage");
        var selectedSize = cmp.get("v.selectedSize");
        var ContentDocumentID = "";
        var CBDTemplate = "";
        var recId = cmp.get("v.recordId");
        var fullRes = cmp.get("v.fullRes");

        var action = cmp.get("c.getContentDocumentID");
        action.setParams({ recordID : recId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let returnObj = JSON.parse(response.getReturnValue())
                ContentDocumentID = returnObj.cdId;
                let certTemplate = returnObj.cbdTemplate === null ? "" : returnObj.cbdTemplate;
                helper.redirectToCertificateVFPage(cmp, ContentDocumentID, selectedLanguage, recId, selectedSize, fullRes, certTemplate);
                var loggingAction = cmp.get("c.logActivity");
                loggingAction.setParams({ itemId : recId , itemName : ContentDocumentID , action : "generate" , customMessage : "Generated" });
                loggingAction.setCallback(this, function(logResponse) {
                    var logState = logResponse.getState();
                    if (logState === "SUCCESS") {
                        console.log("Activity Created");
                    }
                });
                $A.enqueueAction(loggingAction);
            }
            else if (state === "INCOMPLETE") {

            }
            else if (state === "ERROR") {
                
            }
        });
        $A.enqueueAction(action);
    }
})