({
    redirectToCertificateVFPage: function(cmp, ContentDocumentLinkID, selectedLanguage, recordID, selectedSize, fullRes, certTemplate) {

        var certificateTemplate = "";
        if(certTemplate.includes('FSE')) { certificateTemplate = 'FSE'; }
        if(certTemplate.includes('Factory')) { certificateTemplate = 'Factory'; }
        if(certTemplate.includes('Customer')) { certificateTemplate = 'Customer'; }

        if(selectedSize === 'A4') {
            window.open("/apex/cswpCalBox"+ certificateTemplate +"Certificate?linkID="+ContentDocumentLinkID+'&calBoxDataId='+recordID+'&lang='+selectedLanguage+'&fullRes='+fullRes, '_blank');        
        } else if(selectedSize === 'HTML') {
            window.open("/apex/cswpCalBoxCertificateHtml?linkID="+ContentDocumentLinkID+'&calBoxDataId='+recordID+'&lang='+selectedLanguage+'&fullRes='+fullRes, '_blank');        
        }
        else {
            window.open("/apex/cswpCalBox"+ certificateTemplate +"CertificateLetter?linkID="+ContentDocumentLinkID+'&calBoxDataId='+recordID+'&lang='+selectedLanguage+'&fullRes='+fullRes, '_blank');        
        }

        var action = cmp.get("c.storeCertificate");

        action.setParams({ linkId : ContentDocumentLinkID, pageLanguage : selectedLanguage, entityId : recordID, fullRes : fullRes, calBoxDataId : recordID, certTemplate : certificateTemplate });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            }
        });
        $A.enqueueAction(action);
    }
})