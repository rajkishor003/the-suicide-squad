({
    doInit: function(cmp) {
        var action = cmp.get("c.getUserEmail");

        action.setCallback(this, function(response){
            var state = response.getState();

            if (state === "SUCCESS") {
                cmp.set("v.loggedInUserEmail", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);

        var checkPageAccess = cmp.get("c.checkCustomerAdminPageAccess");

        checkPageAccess.setCallback(this, function(response){
            var state = response.getState();

            if (state === "SUCCESS") {
                cmp.set("v.hasPermission", response.getReturnValue());
            }
        });
        $A.enqueueAction(checkPageAccess);

        var userName = cmp.get("c.getUserName");

        userName.setCallback(this, function(response){
            var state = response.getState();

            if (state === 'SUCCESS') {
                cmp.set("v.loggedInUserName", response.getReturnValue());
            }
        });
        $A.enqueueAction(userName);
    },

    goHome: function(cmp, event) {
        var navService = cmp.find("navService");
        var homeReference = {
            type: 'comm__namedPage',
            attributes: {
                name: 'Home'
            }
        };

        event.preventDefault();
        navService.navigate(homeReference);
    },

    redirect: function(cmp, event) {
        var navService = cmp.find("navService");
        var pageName = event.currentTarget.dataset.name;

        if (pageName === 'external') {
            var pageLink = event.currentTarget.dataset.link;

            window.open(pageLink, '_blank');
        } else {
            var refference = {
                type: 'comm__namedPage',
                attributes: {
                    name: pageName
                }
            };

            navService.navigate(refference);
            event.preventDefault();
        }
    },

    logOut: function() {
        window.location.replace("login");
    }
})