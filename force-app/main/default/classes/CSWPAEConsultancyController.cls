public with sharing class CSWPAEConsultancyController {
    
    @AuraEnabled (cacheable=true)
    public static List<Case> getConsultancyTickets(){
        try {
            List<Case> cList = [SELECT Id, Subject, Reason, Type, Status, Priority,CreatedBy.Name, Cswp_Assignee__c, AE_Calculated_Time_Spent__c FROM Case WHERE Cswp_Jira_Project__c != NULL ORDER BY LastModifiedDate Desc];
            return cList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled (cacheable=true)
    public static List<Cswp_Jira_Project__c> getJiraProjects(){
        try {
            Id contactId = [SELECT Id,ContactId FROM User WHERE Id = :UserInfo.getUserId()][0].ContactId;
            if(contactId != null) {
                Id accId = [SELECT Id, AccountId FROM Contact WHERE Id = :ContactId][0].AccountId;
                List<Cswp_Jira_Project__c> projectList = [SELECT Id , Name, CSWP_Group__r.cswp_Customer__c,Cswp_Default_Assignee_ID__c,Cswp_Default_Assignee_Name__c,Cswp_Jira_Customer__c FROM Cswp_Jira_Project__c WHERE CSWP_Group__r.cswp_Customer__c = :accId ORDER BY Name ASC];
                return projectList;
            }
            return null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled (cacheable=true)
    public static TicketOverViewWrapper getTicketOverview(){
        List<TicketOverViewWrapper> tovList = new List<TicketOverViewWrapper>();
        try {
            List<CSWP_Contract__c> contractList = new List<CSWP_Contract__c> ();
            Id contactId = [SELECT Id,ContactId FROM User WHERE Id = :UserInfo.getUserId()][0].ContactId;
            if(contactId != null) {
                //Community User
                Id accId = [SELECT Id, AccountId FROM Contact WHERE Id = :ContactId][0].AccountId;
                contractList = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,Cswp_Start_Date__c,Cswp_End_Date__c,Total_Time_Hours__c,AE_Total_Time_Spent__c FROM CSWP_Contract__c WHERE Account__c = :accId];
            }
            else {
                //Admin User
                contractList = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,Cswp_Start_Date__c,Cswp_End_Date__c,Total_Time_Hours__c,AE_Total_Time_Spent__c FROM CSWP_Contract__c];
            }
            for(CSWP_Contract__c con : contractList) {
                TicketOverViewWrapper tov = new TicketOverViewWrapper();
                if(con.Cswp_Contract_Status__c == 'ACTIVE') {
                    tov.hasActiveContract = true;
                }
                else {
                    tov.hasActiveContract = false;
                }
                if(con.Cswp_End_Date__c != null && con.Cswp_Contract_Status__c == 'ACTIVE'){
                    if(con.Cswp_End_Date__c > System.today()){
                        tov.hasActiveContract = true;
                    }
                    else {
                        tov.hasActiveContract = false;
                    }
                }

                if(con.AE_Total_Hours__c <= con.Total_Time_Hours__c) {
                    tov.totalHoursExceed = true;
                }
                else {
                    tov.totalHoursExceed = false;
                }
                if(con.AE_Total_Hours__c != null) {
                    tov.totalHours = con.AE_Total_Hours__c;
                }

                if(con.AE_Total_Time_Spent__c != null) {
                    tov.timeSpent = con.AE_Total_Time_Spent__c / 3600;
                }

                tovList.add(tov);
            }
            if(!tovList.isEmpty()) {
                return tovList[0];
            }
            else {
                return null;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    
    public class TicketOverViewWrapper {
        @AuraEnabled public Double totalHours {get;set;}
        @AuraEnabled public Double timeSpent {get;set;}
        @AuraEnabled public Boolean hasActiveContract {get;set;}
        @AuraEnabled public Boolean totalHoursExceed {get;set;}

    }

}