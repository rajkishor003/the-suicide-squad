@isTest
public class CSWPAEConsultancyController_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Cswp_Contract__c cc = new Cswp_Contract__c();
        cc.Account__c = acc.Id;
        cc.AE_Total_Hours__c = 100;
        cc.Cswp_Contract_Status__c = 'ACTIVE';
        cc.Cswp_Start_Date__c = System.Today();
        cc.Cswp_End_Date__c = System.today().addDays(60);
        cc.AE_Total_Time_Spent__c = 2;
        cc.Cswp_Contract_ID__c = 12345;
        insert cc;  

        User testUser2 = CSWPTestUtil.createPortalUser(acc);
        insert testUser2;
    }
    @isTest
    static void CSWPAEConsultancyController_Test() {
        User testUser = [Select id,contactId from User Limit 1];
        CSWPAEConsultancyController.getConsultancyTickets();
        CSWPAEConsultancyController.getTicketOverview();  
        Case c = new Case();
        c.Type = 'Test';
        c.Subject = 'Test Subject';
        c.Description = 'Test Description';
        c.ContactId = testUser.contactId;
        c.AE_Time_Spent__c = 5;
        insert c;

        System.runAs(testUser) {
            Case c2 = new Case();
            c2.Type = 'Test';
            c2.Subject = 'Test Subject';
            c2.Description = 'Test Description';
            c2.ContactId = testUser.contactId;
            c2.AE_Time_Spent__c = 5;
            insert c2;
        }
        CswpTicketManagerController.getOpenTickets(String.valueOf(c.Id),0);
        CswpTicketManagerController.getMyTickets(String.valueOf(c.Id),0);
        CswpTicketManagerController.getTicketDetail(String.valueOf(c.Id));
        CswpTicketManagerController.getOpenTickets(null,0);
        CswpTicketManagerController.getMyTickets(null,0);
        CswpTicketManagerController.getTicketDetail(null);

    }
}