public with sharing class CSWPAccountTriggerHandler implements Triggers.Handler,Triggers.AfterInsert{

    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }

    public static void AfterInsert(Triggers.Context context) {
        //CSWPAccountTriggerHandler.CreateAutomaticCSWPGroup(context);
    }

    // public static void CreateAutomaticCSWPGroup(Triggers.Context context) {
    //     List<cswp_Group__c> groupList = new List<cswp_Group__c>();
    //     Map<Id,Account> newMap = (Map<Id,Account>)context.props.newMap;
    //     for(Account acc : newMap.values()) {
    //         cswp_Group__c newGroup = new cswp_Group__c();
    //         newGroup.cswp_Customer__c = acc.Id;
    //         //newGroup.Name = acc.Name;
    //         newGroup.Group_Name__c = acc.Name;
    //         newGroup.cswp_GroupPermission__c = getDefaultCSWPPermission();
    //         groupList.add(newGroup); 
    //     }
    //     Database.insert(groupList,false);
    // }

    // private static Id getDefaultCSWPPermission() {
    //     cswp_GroupPermission__c perm = [SELECT ID From cswp_GroupPermission__c Where Name = 'FullAccess' LIMIT 1];
    //     return perm != null ? perm.Id : null;
    // }
}