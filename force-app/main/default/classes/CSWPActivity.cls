public class CSWPActivity {

    public class EventMessage{
        @AuraEnabled
        public String itemName {get;set;}
        @AuraEnabled
        public String itemId {get;set;}
        @AuraEnabled
        public String parentItemName {get;set;}
        @AuraEnabled
        public String message {get;set;}
        @AuraEnabled
        public String actionName {get;set;}
        //public String ipAddress{get;set;}
        //public String userId{get;set;}
        //public String companyName {get;set;}
        //public String email {get;set;}
    }

    private EventMessage eventMessage;
    public CSWPActivity(EventMessage event){
        this.eventMessage = event;
    }

    public Database.SaveResult publishEvent(){
        return Eventbus.publish(this.initEvent());
    }

    private CSWP_ActivityEvent__e initEvent(){
        CSWP_ActivityEvent__e event = new CSWP_ActivityEvent__e(
            cswp_ItemName__c = eventMessage.itemName,
            cswp_ItemId__c = eventMessage.itemId,
            cswp_ParentItemName__c = eventMessage.parentItemName,
            cswp_ActionTime__c = DateTime.Now(),
            cswp_ActionName__c = eventMessage.actionName,
            cswp_UserName__c = UserInfo.getUserName(),
            cswp_UserSID__c = UserInfo.getUserId(),
            cswp_Email__c = UserInfo.getUserEmail(),
            cswp_Message__c = eventMessage.message
        );
        if(!System.isBatch() && !System.isFuture() && !System.isQueueable()){
            if(!Test.isRunningTest()){
                event.cswp_IP__c = CSWPUtil.getSessionInfoByName('SourceIp');
            }else{
                event.cswp_IP__c = '110.110.99.75';
            }
        }
        User currentUser = [Select Id,Country,Contact.Account.Name From User Where Id=: UserInfo.getUserId()];
        event.cswp_Company__c = currentUser.Contact?.Account?.Name;
        event.cswp_Country__c = currentUser.Country;
        return event;
    }

}