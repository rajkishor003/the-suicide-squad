public with sharing class CSWPApprovalProcessController {

    @AuraEnabled
    public static void ApproveFileWithFromWithCategory(String recordId, String category, String arId) {
        if(category != 'Reject') updateFileCategoryFromEmail(recordId, category);
        List<ProcessInstanceWorkitem> workItems = CSWPSObjectSelector.getApprovalProcessWithRecId(recordId, arId);
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        for(ProcessInstanceWorkitem workItem : workItems){
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setWorkitemId(workItem.Id);
            //Valid values are: Approve, Reject, or Removed. 
            //Only system administrators can specify Removed.
            String processAction = (category == 'Reject' ? category : 'Approve');
            req.setAction(processAction);
            req.setComments('Approved/Rejected From Email.');
            requests.add(req);
        }
        Approval.ProcessResult[] processResults = Approval.process(requests);
    }

    @TestVisible private static void updateFileCategoryFromEmail(String recId, String category) {
        List<cswp_File__c> fileList = CSWPSobjectSelector.getCswpFilesByIdSet(new Set<Id>{recId});
        if(category == 'NTD') { category = 'Non-Technical'; }
        for(cswp_File__c f : fileList) {
            f.cswp_Category__c = category;
        }
        update fileList;
    }
}