@isTest
public  class CSWPApprovalProcessController_Test {
    @TestSetup
    static void makeData(){
        //create files to check private method
        Account testaccount = new Account(Name='Test Account');
        insert testaccount;
        User testUser = CSWPTestUtil.createCommunityUser(testaccount);
        insert testUser;
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        
        System.runAs(adminUsers[0]){
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            
            insert new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = ps.Id);
        }
        
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByName().get('Document Sharing').getRecordTypeId();
        cswp_Workspace__c workspace = CSWPTestUtil.createCswpWorkspace('Test Workspace', testaccount);
        workspace.cswp_CreateOnExternal__c = false;
        workspace.RecordTypeId = clientDocumentRecordId;
        insert workspace;

        CSWPFolderTriggerHandler.isEnabled = false;
        cswp_Folder__c folder = CSWPTestUtil.createCswpFolder('Test Folder', 100.00, workspace);
        insert folder;

       
        List<cswp_File__c> files = new List<cswp_File__c>();
        cswp_File__c f = CSWPTestUtil.createCswpFile('Test File 1', workspace, folder, testUser);
        f.cswp_Status__c= 'New';
       
        files.add(CSWPTestUtil.createCswpFile('Test File 2', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 3', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 4', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 5', workspace, folder, testUser));
        insert f;
        //create ProcessInstanceWorkitem to check method
    }
    @isTest
    static void testApproval(){
        cswp_File__c f = [SELECT Id,cswp_Category__c FROM cswp_File__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(f.id);
        req1.setSubmitterId(UserInfo.getUserId());
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        // Submit on behalf of a specific submitter
        Approval.ProcessResult result = Approval.process(req1);


        
        // Submit the record to specific process and skip the criteria evaluation
       
        
        
        // Submit the approval request for the account
       
        
        // Verify the result
        System.assert(result.isSuccess());

        System.assertEquals(
            'Pending', result.getInstanceStatus(), 
            'Instance Status'+result.getInstanceStatus());
        // Approve the submitted request
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        CSWPApprovalProcessController.ApproveFileWithFromWithCategory( f.Id,  'NTD', newWorkItemIds.get(0) );
    }
    @isTest
    static void testUpdateFileFromEmail(){
        cswp_File__c f = [SELECT Id,cswp_Category__c FROM cswp_File__c LIMIT 1];
        
        CSWPApprovalProcessController.updateFileCategoryFromEmail(f.id,'NTD' );
        cswp_File__c afterF =[SELECT Id,cswp_Category__c FROM cswp_File__c WHERE Id=: f.Id];
        System.assertEquals(afterF.cswp_Category__c, 'Non-Technical');
    }



}