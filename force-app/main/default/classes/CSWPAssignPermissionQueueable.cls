public with sharing class CSWPAssignPermissionQueueable implements Queueable {

    public Map<String, Set<String>> groupIdToPermissionSets;
    public Map<String, Set<String>> groupIdToAddedUsersMap;
    public Map<String, Set<String>> groupIdToRemovedUsersMap;

    public CSWPAssignPermissionQueueable(Map<String, Set<String>> groupIdToPermissionSets, Map<String, Set<String>> groupIdToAddedUsersMap, Map<String, Set<String>> groupIdToRemovedUsersMap) {
        this.groupIdToPermissionSets = groupIdToPermissionSets;
        this.groupIdToAddedUsersMap = groupIdToAddedUsersMap;
        this.groupIdToRemovedUsersMap = groupIdToRemovedUsersMap;
    }

    public void execute(QueueableContext qc) {
        //System.debug('groupIdToPermissionSets ----->' + groupIdToPermissionSets); 
        //System.debug('groupIdToAddedUsersMap ----->' + groupIdToAddedUsersMap); 
        //System.debug('groupIdToRemovedUsersMap ----->' + groupIdToRemovedUsersMap); 
        assignPermissionSets(groupIdToPermissionSets, groupIdToAddedUsersMap);
        removePermissionSets(groupIdToPermissionSets, groupIdToRemovedUsersMap);
    }

    private static void assignPermissionSets(Map<String, Set<String>> groupIdToPermissionSets, Map<String, Set<String>> groupIdToUsersMap) {
        List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment> ();
        for(String groupId : groupIdToPermissionSets.keySet()) {
            if(!groupIdToUsersMap.containsKey(groupId)) {
                continue;
            }
            Set<String> permissionSetIds = groupIdToPermissionSets.get(groupId);
            //System.debug('permissionSetIds -----> ' + permissionSetIds);
            Set<String> userIdSet = groupIdToUsersMap.get(groupId);
            //System.debug('userIdSet -----> ' + userIdSet);
            for(String userId : userIdSet) {
                //System.debug('userId ----> ' + userId);
                for(String permissionSetId : permissionSetIds) {
                    //System.debug('permissionSetId -----> ' + permissionSetId);
                    for(PermissionSetAssignment psa : assignmentList) {
                        if(psa.PermissionSetId == permissionSetId && psa.AssigneeId == userId) {
                            continue;
                        }
                    }
                    assignmentList.add(new PermissionSetAssignment(
                        PermissionSetId = permissionSetId,
                        AssigneeId = userId
                    ));
                }
            }
        }
        //System.debug('assignmentList -----> ' + assignmentList);
        if(!assignmentList.isEmpty()) {
            Database.insert(assignmentList, false);
        }
    }

    private static void removePermissionSets(Map<String, Set<String>> groupIdToPermissionSets, Map<String, Set<String>> groupIdToUsersMap) {
        List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment> ();
        Set<String> permissionIds = new Set<String> ();
        Set<String> userIds = new Set<String> ();
        for(String groupId : groupIdToPermissionSets.keySet()) {
            if(groupIdToUsersMap.containsKey(groupId)) {
                permissionIds.addAll(groupIdToPermissionSets.get(groupId));
                userIds.addAll(groupIdToUsersMap.get(groupId));
            }
        }
        //System.debug('permissionIds ----_> ' + permissionIds);
        //System.debug('userIds ----_> ' + userIds);
        for(PermissionSetAssignment permissionSetAssignment : [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId IN :permissionIds OR AssigneeId IN :userIds]) {
            //System.debug('permissionSetAssignment.PermissionSetId -----> ' + permissionSetAssignment.PermissionSetId);
            //System.debug('permissionSetAssignment.AssigneeId -----> ' + permissionSetAssignment.AssigneeId);
            for(String groupId : groupIdToPermissionSets.keySet()) {
                //System.debug('groupIdToPermissionSets.get(groupId) -----> ' + groupIdToPermissionSets.get(groupId));
                //System.debug('groupIdToUsersMap.get(groupId) -----> ' + groupIdToUsersMap.get(groupId));
                if(groupIdToPermissionSets.get(groupId).contains(permissionSetAssignment.PermissionSetId) && groupIdToUsersMap.get(groupId).contains(permissionSetAssignment.AssigneeId)) {
                    assignmentList.add(permissionSetAssignment);
                }
            }
        }

        //System.debug('remove assignmentList ----> ' + assignmentList);

        if(!assignmentList.isEmpty()) {
            delete assignmentList;
        }
    }
}