public with sharing class CSWPAssignPermissionSetBatch implements Database.Batchable<sObject> {
    
    String query;
    Integer intervalMinutes;

    public CSWPAssignPermissionSetBatch(Integer intervalMinutes) {
        this.intervalMinutes = intervalMinutes;
        query = 'SELECT Id, cswp_PublicGroupId__c, cswp_Users__c FROM cswp_Group__c WHERE cswp_PublicGroupId__c != NULL';
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<cswp_Group__c> cswpGroupList = (List<cswp_Group__c>) scope;
        Set<String> publicGroupIdSet = new Set<String> ();
        Map<String, Set<String>> publicGroupIdToUserIdsMap = new Map<String, Set<String>> ();
        for(cswp_Group__c cswpGroup : cswpGroupList) {
            publicGroupIdSet.add(cswpGroup.cswp_PublicGroupId__c);
        }

        for(GroupMember user : CSWPSobjectSelector.getGroupMemberByGroupIds(publicGroupIdSet)) {
            if(!publicGroupIdToUserIdsMap.containsKey(user.GroupId)) {
                publicGroupIdToUserIdsMap.put(user.GroupId, new Set<String> ());
            }
            publicGroupIdToUserIdsMap.get(user.GroupId).add(user.UserOrGroupId);
        }

        for(cswp_Group__c cswpGroup : cswpGroupList) {
            if(publicGroupIdToUserIdsMap.containsKey(cswpGroup.cswp_PublicGroupId__c)) {
                Set<String> userIdSet = publicGroupIdToUserIdsMap.get(cswpGroup.cswp_PublicGroupId__c);
                cswpGroup.cswp_Users__c = String.join((Iterable<String>)userIdSet, ',');
            }
        }

        Database.update(cswpGroupList, false);
    }

    public void finish(Database.BatchableContext bc) {
        CSWPAssignPermissionSetSchedule.rescheduleThisJob(intervalMinutes);
    }
}