public with sharing class CSWPAssignPermissionSetSchedule implements Schedulable {

    public Integer intervalMinutes;
    public CSWPAssignPermissionSetSchedule(Integer intervalMinutes) {
        this.intervalMinutes = intervalMinutes;
    }

    public void execute(SchedulableContext sc) {
        if(!Test.isRunningTest()) {
            Id jobId = sc.getTriggerId();
            System.abortJob(jobId);
        }
        Database.executeBatch(new CSWPAssignPermissionSetBatch(intervalMinutes));
    }

    public static String rescheduleThisJob(Integer interval) {
        DateTime nextRunTime = DateTime.now().addMinutes(interval);
        String cronString = nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + nextRunTime.month() + ' ? ' + nextRunTime.year();
        return System.schedule(CSWPAssignPermissionSetSchedule.class.getName() + '-' + DateTime.now().getTime(), cronString, new CSWPAssignPermissionSetSchedule(interval));
    }
}