global class CSWPCalibrationBoxBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, Name, OwnerID, Cswp_Generation_Date__c, Cswp_Calibration_Status__c, '+
                       ' Cswp_Calibration_Due_Date__c, CreatedById, Cswp_Days_Left_To_Calibration_Expire__c FROM Cswp_Calibration_Box_Data__c '+
                       ' WHERE Cswp_Days_Left_To_Calibration_Expire__c =2';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Cswp_Calibration_Box_Data__c> scope)
    {
        List<String> createdByIds = new List<String>();
        for(Cswp_Calibration_Box_Data__c cbd : scope) {
            createdByIds.add(cbd.OwnerID);
        }
        List<User> createdUsers = [SELECT ID, Name, Username, CompanyName, ContactId, Contact.Name, Contact.Email, Contact.AccountId, Contact.Account.Name FROM User WHERE ID IN: createdByIds];
        Map<String,String> user_Mail_Map = new Map<String,String>();
        for(User u:createdUsers) {
            user_Mail_Map.put(u.Id, U.Contact.Email);
        } 

        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage>();
        for(Cswp_Calibration_Box_Data__c cbd : scope) {
            if(user_Mail_Map.get(cbd.OwnerId) == null) continue;
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { user_Mail_Map.get(cbd.OwnerId) };
            message.optOutPolicy = 'FILTER';
            message.subject = Label.Cswp_Calibration_Due_Date_Mail_Subject;
            message.plainTextBody = Label.Cswp_Calibration_Due_Date_Mail_Body + ' : '+URL.getSalesforceBaseUrl().toExternalForm()+ '/'+cbd.Id;
            messages.add(message);
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }

    global void finish(Database.BatchableContext BC)
    {
    }
}