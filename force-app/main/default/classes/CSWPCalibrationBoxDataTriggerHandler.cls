public with sharing class CSWPCalibrationBoxDataTriggerHandler implements Triggers.Handler, Triggers.AfterUpdate, Triggers.AfterInsert, Triggers.BeforeInsert {

    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }

    public static void AfterUpdate(Triggers.Context context) {
        if(context.props.isChanged(Cswp_Calibration_Box_Data__c.Cswp_Group__c)||context.props.isChanged(Cswp_Calibration_Box_Data__c.Cswp_Group_To_Share__c)) {
            CSWPCalibrationBoxDataTriggerHandler.HandleGroupAssignment(context);
        }
    }

    public static void AfterInsert(Triggers.Context context) {
        Map<Id,Cswp_Calibration_Box_Data__c> newMap = (Map<Id,Cswp_Calibration_Box_Data__c>)context.props.newMap;
        List<Cswp_Calibration_Box_Data__Share> shareList = new List<Cswp_Calibration_Box_Data__Share>();
        Set<Id> groupIds = new Set<Id>();
        for(Cswp_Calibration_Box_Data__c cbd : newMap.values()) {
            if(cbd.Cswp_Group__c != null) { groupIds.add(cbd.Cswp_Group__c); }
            if(cbd.Cswp_Group_To_Share__c != null) groupIds.add(cbd.Cswp_Group_To_Share__c);
        }
        Map<Id,Cswp_Group__c> groupMap = CSWPSObjectSelector.getCswpGroupsByIds(groupIds);
        for(Cswp_Calibration_Box_Data__c cbd : newMap.values()) {
            if(cbd.Cswp_Group__c != null) { 
                Cswp_Calibration_Box_Data__Share dataShare = CreateSharingForRecord(cbd.Id, groupMap.get(cbd.Cswp_Group__c).cswp_PublicGroupId__c);
                shareList.add(dataShare);
            }
            if(cbd.Cswp_Group_To_Share__c != null) { 
                Cswp_Calibration_Box_Data__Share dataShare = CreateSharingForRecord(cbd.Id, groupMap.get(cbd.Cswp_Group_To_Share__c).cswp_PublicGroupId__c);
                shareList.add(dataShare);
            }
        }
        List<Database.SaveResult> sr = Database.insert(shareList,false);
    }

    public static void BeforeInsert(Triggers.Context context) {
        CSWPCalibrationBoxDataTriggerHandler.HandleAutomaticGroupAssignment(context);
    }

    public static void HandleGroupAssignment(Triggers.Context context) {
        List<Cswp_Calibration_Box_Data__Share> shareList = new List<Cswp_Calibration_Box_Data__Share>();
        Map<Id,Cswp_Calibration_Box_Data__c> newMap = (Map<Id,Cswp_Calibration_Box_Data__c>)context.props.newMap;
        Map<Id,Cswp_Calibration_Box_Data__c> oldMap = (Map<Id,Cswp_Calibration_Box_Data__c>)context.props.oldMap;
        Map<Id,Id> deleteRecGroupMap = new Map<Id,Id>();
        Set<Id> groupIds = new Set<Id>();
        for(Cswp_Calibration_Box_Data__c data :(List<Cswp_Calibration_Box_Data__c>)context.props.newList) { 
            //If Sharing Removed
            if(oldMap.get(data.Id).Cswp_Group__c!=null && data.Cswp_Group__c == null) { 
                groupIds.add(oldMap.get(data.Id).Cswp_Group__c);
                deleteRecGroupMap.put(data.Id, oldMap.get(data.Id).Cswp_Group__c);
            }
            if(oldMap.get(data.Id).Cswp_Group_To_Share__c!=null && data.Cswp_Group_To_Share__c == null) {
                groupIds.add(oldMap.get(data.Id).Cswp_Group_To_Share__c);
                deleteRecGroupMap.put(data.Id, oldMap.get(data.Id).Cswp_Group_To_Share__c);
            }
            if(data.Cswp_Group__c != null) groupIds.add(data.Cswp_Group__c);
            if(data.Cswp_Group_To_Share__c != null) groupIds.add(data.Cswp_Group_To_Share__c);
        } 
        if(groupIds.size() == 0) return;
        Map<Id,Cswp_Group__c> groupMap = CSWPSObjectSelector.getCswpGroupsByIds(groupIds);
        for(Cswp_Calibration_Box_Data__c data :(List<Cswp_Calibration_Box_Data__c>)context.props.newList) {
            if(groupMap.get(data.Cswp_Group__c) == null) continue;
            Cswp_Calibration_Box_Data__Share dataShare = CreateSharingForRecord(data.Id, groupMap.get(data.Cswp_Group__c).cswp_PublicGroupId__c);
            shareList.add(dataShare);

            if(groupMap.get(data.Cswp_Group_To_Share__c) == null) continue;
            Cswp_Calibration_Box_Data__Share dataShareGroup = CreateSharingForRecord(data.Id, groupMap.get(data.Cswp_Group_To_Share__c).cswp_PublicGroupId__c);
            shareList.add(dataShareGroup);
        }
        Set<Id> deleteParentIds = new Set<Id>();
        Set<Id> deleteGroupIds = new Set<Id>();

        for(String recId : deleteRecGroupMap.keySet()) {
            deleteParentIds.add(recId);
            deleteGroupIds.add(groupMap.get(deleteRecGroupMap.get(recId)).cswp_PublicGroupId__c);
        }
        List<Cswp_Calibration_Box_Data__Share> shareList2Del = CSWPSobjectSelector.getCalibrationBoxDataShareWithParentAndGroup(deleteParentIds, deleteGroupIds);
        List<Database.SaveResult> sr = Database.insert(shareList,false);
        List<Database.DeleteResult> dr = Database.delete(shareList2Del,false);
    }

    public static Cswp_Calibration_Box_Data__Share CreateSharingForRecord(String recId, String groupId) {
        Cswp_Calibration_Box_Data__Share dataShare = new Cswp_Calibration_Box_Data__Share();
        dataShare.ParentId = recId;
        dataShare.UserOrGroupId = groupId;
        dataShare.AccessLevel = 'Read';
        dataShare.RowCause = Schema.Cswp_Calibration_Box_Data__Share.RowCause.Manual;
        return dataShare;
    }

    public static void HandleAutomaticGroupAssignment(Triggers.Context context) {
        ID loggedinUserID = UserInfo.getUserId();
        ID cswpGroupID;
        List<Group> gList = CSWPSobjectSelector.getPublicGroupsOfUser(loggedinUserID);
        Set<Id> groupIds = new Set<Id>();
        for(Group g : gList) { groupIds.add(g.Id); }
        List<Cswp_Group__c> cswpGroupList = CSWPSobjectSelector.getCswpGroupsOfPublicGroups(groupIds);
        if(cswpGroupList.size() > 0) {cswpGroupID=cswpGroupList[cswpGroupList.size()-1].ID;}
        List<Cswp_Calibration_Box_Data__c> newList = (List<Cswp_Calibration_Box_Data__c>)context.props.newList;
        for(Cswp_Calibration_Box_Data__c cbd : newList) {
            cbd.Cswp_Group__c = cswpGroupID;
            cbd.Cswp_Certificate_Template_Picklist__c = cswpGroupList[cswpGroupList.size()-1].Cswp_Group_Type__c;
        }
    }
}