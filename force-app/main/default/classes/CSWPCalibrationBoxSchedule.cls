global class CSWPCalibrationBoxSchedule implements Schedulable{

    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight

    global void execute(SchedulableContext sc) {
        CSWPCalibrationBoxBatch b1 = new CSWPCalibrationBoxBatch();
        ID batchprocessid = Database.executeBatch(b1,200);
    }
}