public with sharing class CSWPCaseTriggerHandler implements Triggers.Handler,Triggers.AfterUpdate,Triggers.AfterInsert , Triggers.AfterDelete {

    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }

    public void afterInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPCaseTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void afterUpdate(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPCaseTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_UPDATE);
        if(isEnabled){
            then(context);
        }
    }


    public void afterDelete(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPCaseTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_DELETE);
        if(isEnabled){
            then(context);
        }
    }

    public void SendFieldChangeNotification(Map<String,Case> fieldsToCase) {
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Case.fields.getMap();
        String htmlBody = '<p>The following fields have been updated in your case.</p>';
        String ownerEmail = '';
        String ownerID = '';
        String caseNumber = '';
        for(String field : fieldsToCase.keySet()) {
            htmlBody += '<p><b>'+fieldMap.get(field).getDescribe().getLabel() + ' : </b>' + fieldsToCase.get(field).get(field) + '</p>';
            ownerID = (String)fieldsToCase.get(field).get('OwnerId');
            caseNumber = (String)fieldsToCase.get(field).get('CaseNumber');
        }
        User u = [SELECT Id,Email from User where ID = :ownerID LIMIT 1];
        ownerEmail = u.Email;
        System.debug('htmlBody ===> ' + htmlBody);
        Id senderId = [Select id from orgwideemailaddress limit 1].Id;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.OrgWideEmailAddressId = senderId;
        message.toAddresses = new String[] { ownerEmail};
        message.optOutPolicy = 'FILTER';
        message.subject = 'Your case #'+CaseNumber+' has been updated' ;
        message.HtmlBody = htmlBody;
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
    public void then(Triggers.context context) {
        List<Case> newCaseList = (List<Case>) context.props.newList;
        List<Case> oldCaseList = (List<Case>) context.props.oldList;
        Map<Id,Case> newCaseMap = (Map<Id,Case>) context.props.newMap;
        Map<Id,Case> oldCaseMap = (Map<Id,Case>) context.props.oldMap;
        if(context.props.isAfter) {
            if(context.props.isInsert) {
                List<Case> toBeCreated = new List<Case>();
                List<Cswp_Jira_Project__c> jiraProjectList = [SELECT Id , Name,Cswp_Project_ID__c,Cswp_Issue_Type_ID__c, CSWP_Group__r.cswp_Customer__c FROM Cswp_Jira_Project__c];
                if(!jiraProjectList.isEmpty()){
                    for(Cswp_Jira_Project__c jpl : jiraProjectList) {
                        for(Case c : newCaseList) {
                            if(c.Cswp_Jira_Project__c == jpl.Id && c.AccountId == jpl.CSWP_Group__r.cswp_Customer__c) {
                                toBeCreated.add(c);
                                JSFS.API.createJiraIssue(jpl.Cswp_Project_ID__c,jpl.Cswp_Issue_Type_ID__c,toBeCreated,null);
                            }
                        }
                    }
                } 
            } else if (context.props.isUpdate) {
                Map<Id,Id> caseToAccount = new Map<Id,Id>();
                Map<Id,Double> accountToTimeSpent = new Map<Id,Double>();
                Double totalTimeSpent = 0.0;
                Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Case.fields.getMap();
                Map<String,Case> fieldLabelToCase = new Map<String,Case> ();
                List<Case> pushToJira = new List<Case>();
                for(Case c : newCaseList) {
                    if(c.Cswp_Jira_Project__c != null) {
                        pushToJira.add(c);
                        for (String str : fieldMap.keyset()) { 
                            if(newCaseMap.get(c.Id).get(str) != oldCaseMap.get(c.Id).get(str)){
                                System.debug('field changed new value ==> ' + newCaseMap.get(c.Id).get(str));
                                System.debug('field changed old value ==> ' + oldCaseMap.get(c.Id).get(str));
                                if(str != 'SystemModstamp'
                                    && str != 'LastModifiedById'
                                    && str != 'AE_Time_Spent__c') {
                                    fieldLabelToCase.put(str,newCaseMap.get(c.Id));
                                }
                            }
                        }
                        if(newCaseMap.get(c.Id).get('AE_Time_Spent__c') != oldCaseMap.get(c.Id).get('AE_Time_Spent__c')){
                            if(newCaseMap.get(c.Id).get('AccountId') != null && newCaseMap.get(c.Id).get('Cswp_Jira_Project__c') != null) {
                                caseToAccount.put((Id)newCaseMap.get(c.Id).get('Id'),(Id)newCaseMap.get(c.Id).get('AccountId'));
                            }
                        }
                    }
                }
                
                if(!caseToAccount.isEmpty()) {
                    List<Case> caseList = [SELECT Id,AccountId,AE_Time_Spent__c,Cswp_Jira_Project__c FROM Case WHERE AccountId IN :caseToAccount.values() AND Cswp_Jira_Project__c != null];
                    for(Case c : caseList) {
                        if(c.AE_Time_Spent__c != null) {
                            totalTimeSpent += c.AE_Time_Spent__c;
                            accountToTimeSpent.put(c.AccountId,totalTimeSpent);
                        }
                    }
                    List<CSWP_Contract__c> contractList = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,AE_Total_Time_Spent__c,Account__c FROM CSWP_Contract__c WHERE Account__c IN :caseToAccount.values()];
                    if(!contractList.isEmpty()) {
                        for(CSWP_Contract__c con : contractList) {
                            con.AE_Total_Time_Spent__c = accountToTimeSpent.get(con.Account__c);
                        }
                        update contractList;
                    }
                }
                if(!pushToJira.isEmpty()) {
                    JSFS.API.pushUpdatesToJira(pushToJira, null);
                }
                System.debug('fieldLabelToCase == > ' + fieldLabelToCase);
                if(!fieldLabelToCase.isEmpty()){
                    SendFieldChangeNotification(fieldLabelToCase);
                }
            } else if (context.props.isDelete) {
                Map<Id,Id> caseToAccount = new Map<Id,Id>();
                Double deletedTimeSpent = 0.0;

                for(Case c : oldCaseList) {
                    if(c.AccountId != null && c.Cswp_Jira_Project__c != null) {
                        caseToAccount.put(c.Id,c.AccountId);
                    }
                }

                if(!caseToAccount.isEmpty()){
                    for(Case oldCase : oldCaseList) {
                        if(oldCase.AE_Time_Spent__c != null && oldCase.Cswp_Jira_Project__c != null) {
                            deletedTimeSpent += oldCase.AE_Time_Spent__c;
                        }
                    }
                    List<CSWP_Contract__c> contractList = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,AE_Total_Time_Spent__c,Account__c FROM CSWP_Contract__c WHERE Account__c IN :caseToAccount.values()];
                    System.debug('contractList' + contractList);
                    if(!contractList.isEmpty()) {
                        for(CSWP_Contract__c con : contractList) {
                            if(deletedTimeSpent != 0.0) {
                                con.AE_Total_Time_Spent__c = con.AE_Total_Time_Spent__c - deletedTimeSpent;
                            }
                        }
                        update contractList;
                    }
                }
            }
        }
    }
}