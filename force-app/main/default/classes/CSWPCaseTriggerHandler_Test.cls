@isTest

private class CSWPCaseTriggerHandler_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Cswp_Contract__c cc = new Cswp_Contract__c();
        cc.Account__c = acc.Id;
        cc.AE_Total_Hours__c = 100;
        cc.Cswp_Contract_Status__c = 'ACTIVE';
        cc.Cswp_Start_Date__c = System.Today();
        cc.Cswp_End_Date__c = System.today().addDays(60);
        cc.AE_Total_Time_Spent__c = 2;
        cc.Cswp_Contract_ID__c = 12345;
        insert cc;

        Contact con = new Contact();
        con.LastName = 'Test AE Consultancy';
        con.AccountId = acc.Id;
        
        insert con;

        Case c = new Case();
        c.Type = 'Test';
        c.Subject = 'Test Subject';
        c.Description = 'Test Description';
        c.ContactId = con.Id;
        c.AE_Time_Spent__c = 5;
        insert c;
    }
    @isTest
    static void updateTimeSpent() {
        Case c = [select id, AE_Time_Spent__c from case limit 1];
        c.AE_Time_Spent__c = 15;
        update c;

    }
    @isTest
    static void deleteCase(){
        Case c = [select id, AE_Time_Spent__c,AccountId from case limit 1];
        List<CSWP_Contract__c> contractList = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,AE_Total_Time_Spent__c,Account__c FROM CSWP_Contract__c WHERE Account__c = :c.AccountId];
        Double before = contractList.get(0).AE_Total_Time_Spent__c;
        Test.startTest();
        delete c;
        Test.stopTest();
        List<CSWP_Contract__c> contractList2 = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,AE_Total_Time_Spent__c,Account__c FROM CSWP_Contract__c WHERE Account__c = :c.AccountId];
        Double after = contractList2.get(0).AE_Total_Time_Spent__c;
        System.assertNotEquals(before, after);
    }
}