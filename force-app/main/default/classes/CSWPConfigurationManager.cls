/*************************************************************************************************************
 * @name			CSWPConfigurationManager
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			22 / 11 / 2020
 * @description		CSWP Configuration Util class
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-22		oguzalp					Changes desription
 *
**************************************************************************************************************/
public inherited sharing class CSWPConfigurationManager {

    private static CSWPConfigurationManager instance = null;
    public Map<String,Object> cswpConfigurations = new Map<String,Object>();
    private CSWPConfigurationManager(){
        this.cswpConfigurations = getAllCSWPConfigurations();
    }
    public static CSWPConfigurationManager getInstance(){
        if(instance == null){ 
            return new CSWPConfigurationManager();
        }
        return instance;
    }

    public Map<String,Object> getAllCSWPConfigurations(){
        if(!this.cswpConfigurations.isEmpty()){
            return this.cswpConfigurations;
        }
        Map<String,cswp_AppConfig__c> configs = cswp_AppConfig__c.getAll();
        for (String key : configs.keyset()) {
            cswp_AppConfig__c c = configs.get(key);
            cswpConfigurations.put(key, convertValueByType(c.Value__c,c.DataType__c));
        }
        return cswpConfigurations;
    }

    /*********************************************************************************************************
     * @author			oguzalp <oguz.alp@emakina.com.tr>
     * @created			22 / 11 / 2020
     * @param			String configName : CSWP Configuration API Name, like 'SharepointSiteHost'
     * @return			Object: configuration value
    **********************************************************************************************************/
    public Object getConfigValue(String configName){
        return this.cswpConfigurations.get(configName);
    }

    /*********************************************************************************************************
     * @author			oguzalp <oguz.alp@emakina.com.tr>
     * @created			22 / 11 / 2020
     * @param			String handlerClassName : the name of the trigger handler class
     * @param			String param : Trigger event name, like onAfterInsert
     * @return			Boolean: True if trigger enabled, otherwise false
    **********************************************************************************************************/
    public static Boolean IsTriggerEnabled(String handlerClassName, String eventName){
        try {
            TriggerConfig__mdt triggerConfig = [Select DeveloperName,IsEnabled__c From TriggerConfig__mdt 
            Where TriggerHandlerClass__c = :handlerClassName AND TriggerEventName__c=: eventName Limit 1];
            return triggerConfig.IsEnabled__c;            
        } catch (Exception ex) {
            return true;
        }
    }

    private Object convertValueByType(String value, String type){
        switch on type{
            when 'String' {
               return value; 
            }
            when 'Integer'{
                return Integer.valueOf(value);
            }
            when 'Decimal'{
                return Decimal.valueOf(value);
            }
            when 'Boolean'{                    
                return (value.equalsIgnoreCase('YES') || value.equalsIgnoreCase('true') || value == '1')  ? true : false;
            }
            when 'Date'{                    
                return Date.valueOf(value);
            }
            when 'DateTime'{                    
                return DateTime.valueOf(value);
            }
            when else {
                return value;
            }
        }
    }
    
    
}