public with sharing class CSWPConfirmedNotificationController {
    @AuraEnabled
    public static String confirmNotification(Id notificationId){
        
        String confirmedNotificationId;
        List<CSWP_Confirmed_Notification__c> confirmedNotifications = CSWPSobjectSelector.getCswpConfirmedNotificationWithUserIdAndNotificationId( UserInfo.getUserId(),  NotificationId);
        if(confirmedNotifications.isEmpty()){
            CSWP_Confirmed_Notification__c confirmedNotification = new CSWP_Confirmed_Notification__c();
            confirmedNotification.CSWP_User__c = UserInfo.getUserId();
            confirmedNotification.CSWP_Notification__c = NotificationId;
            confirmedNotification.cswp_is_Confirmed__c = true;
            insert confirmedNotification;
            confirmedNotificationId= confirmedNotification.id;
            return confirmedNotificationId;
        }
       
        CSWP_Confirmed_Notification__c confirmedNotification = confirmedNotifications.get(0);
        confirmedNotification.cswp_is_Confirmed__c = true;
        update confirmedNotification;
        confirmedNotificationId= confirmedNotification.id;
        return confirmedNotificationId;
        
       
    }
}