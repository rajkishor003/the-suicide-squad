@IsTest
public class CSWPConfirmedNotificationController_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='Test Account');
        insert acc;
        User testUser = CSWPTestUtil.createCommunityUser(acc);
       
        insert testUser;
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        
        System.runAs(adminUsers[0]){
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            insert new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = ps.Id);
        }

        CSWP_Notification__c notification = CSWPTestUtil.createNotificationWithoutGroup('System notification'
                                                                            ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                            , system.today()
                                                                            , system.today()+20
                                                                            , true
                                                                            , false
                                                                            , false
                                                                            , false
                                                                            , 'CSWP_System_Notification');
        insert notification;
        CSWP_Notification_Translation__C nTranslation= CSWPTestUtil.createNotificationTranslation('ja'
                                                                                                    , 'System Notification JP'
                                                                                                    , 'JP Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                                                    , notification);
        insert nTranslation;

    }
    @IsTest
    public static void testConfirmNotificationWhenConfirmedNotificationNotExists() {
        List<CSWP_Notification__c> nList = [SELECT Id FROM CSWP_Notification__c LIMIT 1];
        List<User> users = [Select Id from User LIMIT 1];
        User testUser= users.get(0);
        String confirmedId;
        System.runAs(testuser){
           
            confirmedId = CSWPConfirmedNotificationController.confirmNotification(nList.get(0).Id);
        }
        System.assert(confirmedId!= null);
    }
    @IsTest
    public static void testConfirmNotificationWhenConfirmedNotficationExists() {
        List<CSWP_Notification__c> nList = [SELECT Id FROM CSWP_Notification__c LIMIT 1];
        List<User> users = [Select Id from User LIMIT 1];
        User testUser= users.get(0);
        CSWP_Confirmed_Notification__c confirmedNotification = new CSWP_Confirmed_Notification__c();
        confirmedNotification.CSWP_User__c = testUser.Id;
        confirmedNotification.CSWP_Notification__c =nList.get(0).Id;
        confirmedNotification.cswp_is_Confirmed__c = false;
        insert confirmedNotification;
        String confirmedId;
        System.runAs(testuser){
           
            confirmedId = CSWPConfirmedNotificationController.confirmNotification(nList.get(0).Id);
        }
        System.assert(confirmedId!= null);
    }
}