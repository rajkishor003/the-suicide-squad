public with sharing class CSWPContactUsController {
    @AuraEnabled
    public static string saveCase(Case caseRec) {
      string caseId;
      try{
        User currentUser = [SELECT Id, ContactId, Contact.AccountId, Contact.Account.Name, Email, Phone, FirstName, LastName FROM User WHERE Id = :UserInfo.getUserId()];
        String webName = currentUser.FirstName + ' ' + currentUser.LastName;
        caseRec.ContactId = currentUser.ContactId;
        caseRec.AccountId = currentUser.Contact.AccountId;
        caseRec.SuppliedCompany = currentUser.Contact.Account.Name;
        caseRec.SuppliedEmail = currentUser.Email;
        caseRec.SuppliedName = webName.length() > 80 ? currentUser.FirstName : webName;
        caseRec.SuppliedPhone = currentUser.Phone;
        Insert caseRec;
        caseId = caseRec.Id;
      } catch(Exception ex){
        system.debug('Exception===>'+ex.getMessage());
      }
      return caseId;
    }

    @AuraEnabled
    public static String createContentDocumentLinks(String[] contentDocumentIds, String caseId) {
      try{
        List<ContentDocument> contentDocumentList = [SELECT Id, ParentId FROM ContentDocument WHERE Id IN :contentDocumentIds];
        List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink> ();
        for(ContentDocument contentDocument : contentDocumentList) {
          contentDocumentLinkList.add(new ContentDocumentLink(
            ContentDocumentId = contentDocument.Id,
            LinkedEntityId = caseId,
            ShareType = 'I',
            Visibility = 'AllUsers'
          ));
        }
        Database.insert(contentDocumentLinkList, true);
        return caseId;
      } catch (Exception e) {
        return e.getMessage() + ' in line number ' + e.getLineNumber();
      }
      
    }

    @AuraEnabled
    public static String deleteContentDocument(String documentId) {
      List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument WHERE Id = :documentId];
      if(!contentDocumentList.isEmpty()) {
        delete contentDocumentList;
      }
      return documentId;

    }
}