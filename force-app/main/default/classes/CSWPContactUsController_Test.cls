@isTest
private with sharing class CSWPContactUsController_Test {

    @isTest
    static void testContentDocumentMethods(){
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;

        ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        String[] docIds = new String[]{contentVersionSelect.ContentDocumentId};

        Account acc = new Account(Name='Test Account');
        insert acc; 
        User testuser = CSWPTestUtil.createCommunityUser(acc);
        insert testuser;
        String caseId = '';
        Case newCase = new Case();
        System.runAs(testuser){
            caseId = CSWPContactUsController.saveCase(newCase);
        }

        String testCaseId = '';
        Test.startTest();
        testCaseId = CSWPContactUsController.createContentDocumentLinks(docIds, caseId);
        Test.stopTest();

        System.assertNotEquals('',testCaseId, 'a case Id is returned sucessfully');
        System.assertEquals(testCaseId,caseId, 'returned case Id and given case Id match');

        List<ContentDocumentLink> contentDocumentLinkList = [SELECT Id FROM ContentDocumentLink WHERE contentDocumentId =: contentVersionSelect.ContentDocumentId];
        System.assertNotEquals(contentDocumentLinkList.size(),0,'links are created');

        String documentId = CSWPContactUsController.deleteContentDocument(contentVersionSelect.ContentDocumentId);
        System.assertNotEquals(null,documentId,'deleted document Id returned');
        System.assertEquals(contentVersionSelect.ContentDocumentId, documentId, 'given document Id and deleted document Id match');
        
    }
}