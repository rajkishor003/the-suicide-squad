public class CSWPContent {

    public class File{
        //possibles objects: cswp_Workspace__c and cswp_Folder__c
        public SObject parentObject {get;set;}
        public CSWP_File__c cswpFile {get; set;} 
        public String contentDocumentId {get;set;}
        public String contentVersionId {get;set;}
        public String approverId {get;set;}
        public String parentRootFolderId {get;set;}
        public DateTime externalCreatedDate {get;set;}
        public DateTime modifiedDateTime {get;set;}
        public String hubItemId {get;set;}
        public String externalContentUrl {get;set;}
        public String externalDocumentUrl {get;set;}
        public String downloadUrl {get;set;}
        public String internalFilePath {get;set;}
        public Integer contentSize {get;set;}
        public String mimeType {get;set;}
        public String name {get;set;}
        public String title {get;set;}
        public Boolean isSynced {get;private set;}
        public String description {get;set;}
        public String fileExtension {get;set;}
        //Default
        public File(){ }

        public File(ConnectApi.RepositoryFileSummary hubFile){
            this.hubItemId = hubFile.id;
            this.name = hubFile.name;
            this.title = hubFile.title;
            this.contentSize = hubFile.contentSize;
            this.downloadUrl= (hubFile.downloadUrl == null ? '' : hubFile.downloadUrl.removeStart(CSWPUtil.CSWP_SITE_PREFIX));
            this.externalCreatedDate = hubFile.createdDate;
            this.externalContentUrl = hubFile.externalContentUrl;
            this.mimeType = hubFile.mimeType;
            this.isSynced = hubFile.createdDate!=null;
        }
        public File(ContentVersion version,SObject parentObject){
            this.name = version.PathOnClient;
            this.title = version.Title;
            this.contentSize = version.ContentSize;
            this.contentDocumentId = version.ContentDocumentId;
            this.contentVersionId = version.Id;
            this.fileExtension = version.FileExtension;
            this.parentObject = parentObject;
        }

        public File(ContentVersion version, ConnectApi.RepositoryFileSummary hubFile){
            this(hubFile);
            this.contentDocumentId = version.ContentDocumentId;
            this.contentVersionId = version.Id;
            this.fileExtension = version.FileExtension;
        }

        
        public CSWP_File__c initCswpFile(SObject parent){
            this.parentObject = parent;
            this.cswpFile = new CSWP_File__c(
                cswp_UploadedBy__c = UserInfo.getUserId(),
                cswp_UploadedDateTime__c = this.externalCreatedDate,
                cswp_FileSize__c = this.contentSize,
                Name = this.name,
                cswp_HubItemId__c = this.hubItemId,
                cswp_MimeType__c = this.mimeType,
                cswp_DownloadUrl__c = (this.downloadUrl==null ? '' : this.downloadUrl.removeStart(CSWPUtil.CSWP_SITE_PREFIX)),
                cswp_ContentDocumentId__c = this.contentDocumentId,
                cswp_LastContentVersionId__c = this.contentVersionId,
                cswp_WebUrl__c = this.externalContentUrl,
                cswp_FileExtension__c = this.fileExtension
            );
            if(this.parentObject == null){
                return this.cswpFile;
            }
            Schema.SObjectType objectType = parentObject.getSObjectType();
            if(objectType == Schema.cswp_Workspace__c.getSobjectType()){
                CSWP_Workspace__c parentWorkspace = (CSWP_Workspace__c)parentObject;
                this.cswpFile.cswp_Workspace__c = parentWorkspace.Id;
                if(String.isNotBlank(parentWorkspace.cswp_Approver__c)){
                    this.approverId = parentWorkspace.cswp_Approver__c;   
                }
            }else if(objectType == Schema.cswp_Folder__c.getSobjectType()){
                CSWP_Folder__c parentFolder = (CSWP_Folder__c)parentObject;
                this.cswpFile.cswp_Folder__c = parentObject.Id;
                if(String.isNotBlank(parentFolder.cswp_Workspace__r?.cswp_Approver__c)){
                    this.approverId = parentFolder.cswp_Workspace__r.cswp_Approver__c;
                }
            }
            //Approver is being set in cswp file before flow. 
            //this.cswpFile.cswp_Approver__c = this.approverId;
            //Both cswpWorkspace and folder object has same path api name!
            cswpFile.cswp_FilePath__c = (String)parentObject.get('cswp_Path__c') + '/' + this.name;
            return this.cswpFile;
        }     
    }

    public virtual class DriveItem{
        @AuraEnabled
        public String id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String path {get;  set;}
        @AuraEnabled
        public String breadcrumb {get;set;}
        @AuraEnabled
        public String type {get; set;}
        @AuraEnabled
        public String icon {get; set;}
        @AuraEnabled  
        public String description {get; set;}
        @AuraEnabled
        public Decimal contentSize {get; set;}
        @AuraEnabled
        public Datetime createdDate {get; set;}
        @AuraEnabled
        public String createdBy {get; set;}
        @AuraEnabled
        public Datetime lastModifiedDate {get; set;}
        @AuraEnabled
        public String lastModifiedBy {get; set;}
        @AuraEnabled
        public String status {get; set;}
        @AuraEnabled
        public String downloadUrl {get; set;}
        @AuraEnabled
        public String folderName {get; set;}
        @AuraEnabled
        public Boolean isRoot {get;set;}
        @AuraEnabled 
        public String createdDateFormatted {get; set;}
        @AuraEnabled
        public String lastModifiedDateFormatted {get; set;}
        
        public DriveItem(SObject driveItemSObj){
            if(driveItemSObj.getSObjectType() == Schema.cswp_File__c.getSobjectType()){
              this.initByCswpFile((cswp_File__c) driveItemSObj);
            }else if(driveItemSObj.getSObjectType() == Schema.cswp_Folder__c.getSobjectType()){
              this.initByCswpFolder((cswp_Folder__c) driveItemSObj);
            }
        } 
        private void initByCswpFile(CSWP_File__c cswpFile){
          this.type = 'file';    
          this.id = cswpFile.id;
          this.name = cswpFile.Name;
          this.status = cswpFile.cswp_Status__c;
          this.path = cswpFile.cswp_FilePath__c;
          this.downloadUrl = cswpFile.cswp_DownloadUrl__c!=null ? 
            cswpFile.cswp_DownloadUrl__c.removeStart(CSWPUtil.CSWP_SITE_PREFIX) : '';
          //this.folderName = (cswpFile.cswp_Folder__c != null ? cswpFile.cswp_Folder__r.Name : cswpFile.cswp_Workspace__r.Name );
          this.description = cswpFile.cswp_Description__c;
          this.contentSize = cswpFile.cswp_FileSize__c == null ? 0.00 :
            CSWPUtil.convertByteToKilobyte((Long)cswpFile.cswp_FileSize__c );
          this.lastModifiedBy = cswpFile.LastModifiedBy.Name;
          this.createdBy = (cswpFile.cswp_UploadedBy__c != null ? cswpFile.cswp_UploadedBy__r.Name : '');
          this.createdDate = cswpFile.CreatedDate;
          this.createdDateFormatted = CSWPUtil.formatDateTime(cswpFile.CreatedDate);
          //(cswpFile.cswp_UploadedDateTime__c != null ? cswpFile.cswp_UploadedDateTime__c.format('mm/dd/yyyy HH:mm') : null);
          this.lastModifiedDate = cswpFile.LastModifiedDate;
          this.lastModifiedDateFormatted = CSWPUtil.formatDateTime(cswpFile.LastModifiedDate);
          this.icon = CSWPMimeTypeHelper.getIconByExtension(cswpFile.cswp_FileExtension__c);
        }

        private void initByCswpFolder(CSWP_Folder__c folder){
          this.type = 'folder';
          this.id = folder.id;
          this.name = folder.name;
          this.path = folder.cswp_Path__c;
          this.breadcrumb = folder.cswp_Breadcrumb__c;
          this.contentSize = folder.cswp_ContentSize__c==null ? 0.00 : 
            CSWPUtil.convertByteToKilobyte((Long)folder.cswp_ContentSize__c);
          this.description = folder.cswp_Description__c;
          this.isRoot = folder.cswp_IsRoot__c;
          this.createdDate = folder.CreatedDate;
          this.createdDateFormatted = CSWPUtil.formatDateTime(folder.CreatedDate);
            //folder.cswp_createdDateTime__c!=null ? folder.cswp_createdDateTime__c.format('mm/dd/yyyy HH:mm') : null;
          this.createdBy = folder.CreatedBy.Name;
          this.lastModifiedBy = folder.LastModifiedBy?.Name;
          this.lastModifiedDate = folder.LastModifiedDate;
          this.lastModifiedDateFormatted = CSWPUtil.formatDateTime(folder.LastModifiedDate);
          this.downloadUrl = '';//There is no downloadUrl for folders
          this.icon = 'doctype:folder';
        }
    }

    public class Permission {
        @AuraEnabled
        public String groupId {get;set;}
        @AuraEnabled
        public String groupName {get;set;}
        @AuraEnabled
        public Boolean canDownload {get;set;}
        @AuraEnabled
        public Boolean canDelete {get;set;}
        @AuraEnabled
        public Boolean canUpload {get;set;} 
        @AuraEnabled
        public Boolean canCreateFolder {get;set;}
        @AuraEnabled
        public Boolean canCreateSubFolder {get;set;}
        @AuraEnabled
        public Boolean canRename {get;set;}

        public Permission(){}

        public Permission(cswp_GroupPermission__c perm){    
            this.canDownload = perm.cswp_Download__c;
            this.canRename = perm.cswp_Rename__c;
            this.canUpload = perm.cswp_Upload__c;
            this.canCreateFolder = perm.cswp_CreateFolder__c;
            this.canCreateSubFolder = perm.cswp_CreateSubFolder__c;
            this.canDelete = perm.cswp_Delete__c;
        }
    }

    public class DeleteResult{
        @AuraEnabled
        public String itemId {get;set;}
        @AuraEnabled
        public Boolean isSuccess {get;set;}
        @AuraEnabled
        public String itemName {get;set;}
        @AuraEnabled
        public String error {get;set;}
    }
}