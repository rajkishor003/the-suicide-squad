@isTest
public class CSWPContentDocumentLinkHandler_Test {
   @isTest
   static void makeData(){
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;

      Cswp_Contract__c cc = new Cswp_Contract__c();
      cc.Account__c = acc.Id;
      cc.AE_Total_Hours__c = 100;
      cc.Cswp_Contract_Status__c = 'ACTIVE';
      cc.Cswp_Start_Date__c = System.Today();
      cc.Cswp_End_Date__c = System.today().addDays(60);
      cc.AE_Total_Time_Spent__c = 2;
      cc.Cswp_Contract_ID__c = 12345;
      insert cc;

      Contact con = new Contact();
      con.LastName = 'Test AE Consultancy';
      con.AccountId = acc.Id;
      
      insert con;

      Case c = new Case();
      c.Type = 'Test';
      c.Subject = 'Test Subject';
      c.Description = 'Test Description';
      c.ContactId = con.Id;
      c.AE_Time_Spent__c = 5;
      insert c;
      ContentVersion content=new ContentVersion(); 
      content.Title='Header_Picture1'; 
      content.PathOnClient='/' + content.Title + '.jpg'; 
      Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
      content.VersionData=bodyBlob; 
      content.origin = 'H';
      insert content;

      ContentDocumentLink cdl = new ContentDocumentLink();
      cdl.LinkedEntityId = c.Id;
      cdl.Visibility = 'AllUsers';
      cdl.ContentDocumentId = [select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
      insert cdl;
   }
}