public with sharing class CSWPContentDocumentLinkTriggerHandler implements Triggers.Handler,Triggers.AfterInsert {
    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }
    public void afterInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPContentDocumentLinkTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void then(Triggers.context context) {
        List<ContentDocumentLink> newContentDocumentList = (List<ContentDocumentLink>) context.props.newList;
        List<ContentDocumentLink> oldContentDocumentList = (List<ContentDocumentLink>) context.props.oldList;
        if(context.props.isAfter) {
            if(context.props.isInsert) {
                Map<String, Set<Id>> sObjectsByType = new Map<String, Set<Id>>();
                Map<String, List<SObject>> sObjectsToBePushed = new Map<String, List<SObject>>(); 
                  
                for (ContentDocumentLink contentDocumentLink : newContentDocumentList) {
                    String sObjectType = String.valueOf(contentDocumentLink.LinkedEntityId.getSObjectType());
                    if(sObjectType == 'Case') {
                        if (sObjectsByType.containsKey(sObjectType)) {
                            sObjectsByType.get(sObjectType).add(contentDocumentLink.LinkedEntityId);
                        } else {
                            sObjectsByType.put(sObjectType, new Set<Id>{contentDocumentLink.LinkedEntityId});
                        }
                    }
                }
            
                for(String sObjectType : sObjectsByType.keySet()) {
                    if(sObjectType == 'Case') {
                        Set<Id> ids = sObjectsByType.get(sObjectType);
                        String sObjectsById = 'SELECT Id FROM ' + sObjectType + ' where Id IN :ids';
                        List<SObject> toBePushed = Database.query(sObjectsById);
                        if (toBePushed.size() > 0) {
                            sObjectsToBePushed.put(sObjectType, toBePushed);
                        }
                    }
                }
            
                if (sObjectsToBePushed.size() > 0) {
                    //JSFS.API.pushUpdatesToJiraWithMap(sObjectsToBePushed, oldContentDocumentList);
                }
            }
        }
    }
}