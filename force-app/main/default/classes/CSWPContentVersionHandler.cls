public class CSWPContentVersionHandler implements Triggers.Handler, /*Triggers.AfterUpdate,*/ Triggers.AfterInsert
{
        
    public Boolean criteria(Triggers.Context context){
        if(System.isFuture() || System.isQueueable() || System.isScheduled()){
            return false;
        }else{
            return true;
        }
    }

    /* No longer used.
    public void AfterUpdate(Triggers.Context context){
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPContentVersionHandler.class.getName(), CSWPUtil.ON_AFTER_UPDATE);
        if(isEnabled){
            then(context);
        }
    }
    public void then(Triggers.Context context){
        Boolean isChanged = context.props.isChanged(ContentVersion.cswp_ParentId__c);
        if(isChanged){
            List<Id> changedVersionIds = context.props.filterChanged(ContentVersion.cswp_ParentId__c);
            //CSWPRepositoryManager.uploadFilesAsRepoItemAsync(new Set<Id>(changedVersionIds));
        }        
    }
    */
    public void AfterInsert(Triggers.Context context){
      List<ContentVersion> versions = (List<ContentVersion>)context.props.newList;
      //CSWPUtil.debugLog('BEFOREINSERT.CONTENTVERSION:> ' + JSON.serializePretty(versions));
      CSWPUtil.debugLog('BEFOREINSERT.CONTENTVERSION.LocationId:> ' + versions.get(0).FirstPublishLocationId);
      try{
        //versions.get(0).addError('Your limit is over!');
        checkStorageLimit(versions);  
      }catch(Exception ex){
        CSWPUtil.errorLog('ContentVersion.AfterInsert.ERROR >>>> ' + ex.getMessage());
      }
    }

    public void checkStorageLimit(List<ContentVersion> versions){
      String errorMessage = 'Looks like you hit the storage limit. Please contact your administrator and upgrade your plan';
      //total byte
      Decimal totalSize=0;
      CSWPUtil.debugLog('ParentId===>' + versions.get(0).FirstPublishLocationId);
      Id parentId = Id.valueOf(versions.get(0).FirstPublishLocationId);
      Id accountId;
      for (ContentVersion file : versions) {
        totalSize += Decimal.valueOf(file.ContentSize);
      }
      if(parentId.getSobjectType() == cswp_Folder__c.getSObjectType()){
        //TODO: move it to the selector class
        cswp_Folder__c folder = [Select Id, cswp_Workspace__r.cswp_Account__c From cswp_Folder__c Where Id=:parentId Limit 1];
        accountId = Id.valueOf(folder.cswp_Workspace__r.cswp_Account__c); 
      }else if(parentId.getSobjectType() == cswp_Workspace__c.getSObjectType()) {
        //TODO: move it to the selector class
         cswp_Workspace__c workspace = [Select Id, cswp_Account__c From cswp_Workspace__c Where Id=:parentId Limit 1]; 
         accountId = Id.valueOf(workspace.cswp_Account__c);
      }
      //get cswp Preferences
      //TODO: move it to the selector class
      cswp_AccountPreference__c pref = [ SELECT cswp_Account__c,cswp_Current_Storage__c,
        cswp_StorageLimit__c, cswp_Active__c 
        FROM cswp_AccountPreference__c 
        WHERE cswp_Account__c =:accountId AND cswp_Active__c = true Limit 1];
      
       //Testing purposes we will use MB
       //Decimal totalSize = CSWPUtil.convertByteToMegabyte(totalSize);
       Decimal storageLimit = pref.cswp_StorageLimit__c * Math.pow(1024, 3);
       Decimal currentStorage = pref.cswp_Current_Storage__c * Math.pow(1024, 3);
       CSWPUtil.debugLog('STORAGELIMIT :> ' + storageLimit);
       CSWPUtil.debugLog('CURRENTSTORAGE+TotalSize :> ' + currentStorage+ totalSize);
       CSWPUtil.debugLog('TotalSize :> ' + totalSize);
      if((currentStorage + totalSize) >= storageLimit ){
        CSWPUtil.debugLog('totalSize :> ' + totalSize);
        versions.get(0).addError(errorMessage);
      }
    }
}