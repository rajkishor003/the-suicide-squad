public with sharing class CSWPCookieController {
    @AuraEnabled(cacheable=true)
    public static String getUserCookies(){
        Id userId = UserInfo.getUserId();
        User user =  CSWPSObjectSelector.getUser(userID);

      
        List<CSWP_Cookie__c> cookies =  CSWPSObjectSelector.getCookiesByContactId(user.ContactId);
        if(cookies.isEmpty()){
           return 'None';
        }
        return cookies.get(0).cswp_Cookie_Type__c;
    }
    @AuraEnabled
    public static void saveUserCookies(String cookieType){
        String hasCookies = getUserCookies();
        if(!hasCookies.contains('None')){
            throw new Cswp_Custom_Exception(Label.CSWP_Cookie_Saving_Error_Text);
        }
        else{
            Id userId = UserInfo.getUserId();
            User u =  CSWPSObjectSelector.getUser(userID);
            CSWP_Cookie__c newCookie = new CSWP_Cookie__c();
            newCookie.cswp_Cookie_Type__c = cookieType;
            newCookie.CSWP_Acception_Date__c = system.today();
            newCookie.CSWP_Contact__c= u.ContactId;
            insert newCookie;
  
        }
    }
    
    @AuraEnabled(cacheable=true)
    public static String getTrakingId(){
        List<cswp_Google_Analaytics_Tracking_Id__mdt> trackingID =  CSWPSObjectSelector.getGoogleTrackingId();
        return trackingID.get(0).Tracking_Id__c;
    }
}