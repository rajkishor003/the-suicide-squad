global class CSWPCreateCaseCommentFromEmail implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult  handleInboundEmail(Messaging.inboundEmail email, 
                                                       Messaging.InboundEnvelope env) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String myPlainText= '';
        String myHtmlBody = '';
        String caseId = '';
        myPlainText = email.plainTextBody;
        myHtmlBody = email.htmlBody;
        if(myPlainText.contains('cswp/s/case/500')){
            caseId = myPlainText.substringAfterLast('cswp/s/case/').substring(0, 15);
        }
        if(String.isNotBlank(caseId) && String.isNotBlank(myHtmlBody)) {

            Case c = [SELECT Id, ContactId FROM case WHERE ID = :caseId LIMIT 1];
            Id userId = [SELECT Id FROM User WHERE ContactId = :c.ContactId lIMIT 1].Id;                                      
            FeedItem fi = new FeedItem();
            fi.Title = email.Subject;
            fi.Visibility = 'AllUsers';
            fi.CreatedById = userId;
            //fi.Body = myPlainText.substringBefore('wrote:');
            if(myHtmlBody.contains('dir="ltr"')) {
                myHtmlBody = myHtmlBody.substringBeforeLast('dir="ltr"').substringBeforeLast('<div');
                if(myHtmlBody.contains('<hr')) {
                    myHtmlBody = myHtmlBody.substringBeforeLast('<hr');
                }
            }
            System.debug('myHtmlBody11 = > ' + myHtmlBody);
            fi.Body = myHtmlBody;
            fi.parentId = Id.valueOf(caseId);
            insert fi ;
    
            if(email.textAttachments != null) {
                System.debug('textAttachments is not null ===> ');
                for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
                    ContentVersion cv = new ContentVersion();
                    cv.VersionData = Blob.valueOf(tAttachment.body);
                    cv.Title = tAttachment.fileName;
                    cv.PathOnClient = tAttachment.fileName;
                    insert cv;
    
                    cv = [select Id,Title, ContentDocumentId from ContentVersion where id = :cv.id limit 1];
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.LinkedEntityId = c.Id;
                    cdl.ContentDocumentId = cv.ContentDocumentId;
                    cdl.ShareType = 'V';
                    insert cdl;
                    
                    FeedAttachment feedAttachment = new FeedAttachment();
                    feedAttachment.FeedEntityId = fi.Id;
                    feedAttachment.RecordId = cv.Id; 
                    feedAttachment.Title = cv.Title;
                    feedAttachment.Type = 'CONTENT';
                    insert feedAttachment;
                }
            }   
    
            if (email.binaryAttachments != null) {
                System.debug('binary attachment is not null');
                for (Messaging.InboundEmail.binaryAttachment binAttach :email.binaryAttachments){
                        ContentVersion cv = new ContentVersion();
                        cv.VersionData = binAttach.body;
                        cv.Title = binAttach.fileName;
                        cv.PathOnClient = binAttach.fileName;
                        insert cv;
                        cv = [select Id,ContentDocumentId,Title from ContentVersion where id = :cv.id limit 1];
                        ContentDocumentLink cdl = new ContentDocumentLink();
                        cdl.LinkedEntityId = c.Id;
                        cdl.ContentDocumentId = cv.ContentDocumentId;
                        cdl.ShareType = 'V';
                        insert cdl;
                        
                        FeedAttachment feedAttachment = new FeedAttachment();
                        feedAttachment.FeedEntityId = fi.Id;
                        feedAttachment.RecordId = cv.Id; 
                        feedAttachment.Title = cv.Title;
                        feedAttachment.Type = 'CONTENT';
                        insert feedAttachment;
                    }
            }
            result.success = true;
        }
        else {
            result.success = false;
        }
        return result;
    }
}