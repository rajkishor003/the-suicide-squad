@isTest
public class CSWPCreateCaseCommentFromEmail_Test {
    @isTest
    static void createInboundEmail() {
        Account acc = CSWPTestUtil.createAccount();
        insert acc;

        Contact c = new Contact(Lastname = 'Doe', Firstname = 'John', AccountId = acc.Id);
        insert c;

        User u = new User ();
        u.ProfileID = [Select Id From Profile Where Name='CSWP Community User'].id;
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
        u.TimeZoneSidKey = 'America/New_York';
        u.LocaleSidKey = 'en_US';
        u.FirstName = 'first';
        u.LastName = 'last';
        u.Username = 'test@uniquedomain.com';
        u.CommunityNickname = 'testUser123';
        u.Alias = 't1';
        u.Email = 'no@email.com';
        u.IsActive = true;
        u.ContactId = c.Id;
        insert u;


        Case testCase = new Case (ContactId = c.Id, Subject = 'Test', Description = 'Test Description');
        insert testCase;

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'Test Subject';
        email.fromName = 'test test';
        email.plainTextBody = 'Hello, this a test email body./'+testCase.Id;
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1]; 
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        envelope.fromAddress = 'user@acme.com';
        
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };

        CSWPCreateCaseCommentFromEmail catcher = new CSWPCreateCaseCommentFromEmail();
        Messaging.InboundEmailResult result = catcher.handleInboundEmail(email, envelope);
    }
}