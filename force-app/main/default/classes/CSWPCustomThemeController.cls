public with sharing class CSWPCustomThemeController {

    @AuraEnabled
    public static String getUserEmail() {
      return userinfo.getUserEmail();
    }

    @AuraEnabled
    public static Boolean checkCustomerAdminPageAccess(){
      try {
        System.debug('Permission Check ==> ' + FeatureManagement.checkPermission('Cswp_Customer_Admin_Page_Access'));
        return FeatureManagement.checkPermission('Cswp_Customer_Admin_Page_Access');
      } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
      }
    }

    @AuraEnabled
    public static String getUserName() {
      return userinfo.getName();
    }
}