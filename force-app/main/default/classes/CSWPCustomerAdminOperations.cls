public without sharing class CSWPCustomerAdminOperations {
    @future
    public static void deleteGroupMembers(String serializedGUList) {
        List<GroupMember> memberList = (List<GroupMember>) JSON.deserialize(serializedGUList, List<GroupMember>.class);
        delete memberList;

    }
    @future
    public static void createGroupMembers(String serializedGUList) {
        List<cswp_group_user__c> guList = (List<cswp_group_user__c>) JSON.deserialize(serializedGUList, List<cswp_group_user__c>.class);
        List<GroupMember> gmList = new List<GroupMember>();
        for(cswp_group_user__c cgu : guList)  {
            GroupMember gm = new GroupMember();
            gm.GroupId = cgu.Cswp_Public_Group_ID__c;
            gm.UserOrGroupId = cgu.Cswp_User__c;
            gmList.add(gm);
        }
        if(!gmList.isEmpty()) {
            Database.insert(gmList,false);
        }
    }

    @future
    public static void RemovePermissionSetFromCswpGrupUsers(String jsonCguList) { 

        List<cswp_group_user__c> guList = (List<cswp_group_user__c>) JSON.deserialize(jsonCguList, List<cswp_group_user__c>.class);
        Set<Id> cswpGroupIds = new Set<Id>();
        Map<Id,Set<Id>> userToCswpGroups = new Map<Id,Set<Id>>();
        Set<Id> userIds = new Set<Id>();
        for(cswp_group_user__c cgu : guList) {
            cswpGroupIds.add(cgu.cswp_group__c);
            userIds.add(cgu.Cswp_User__c);
            if(userToCswpGroups.get(cgu.Cswp_User__c) == null){
                userToCswpGroups.put(cgu.Cswp_User__c, new Set<Id>());
            }
            userToCswpGroups.get(cgu.Cswp_User__c).add(cgu.cswp_group__c);
        }

        Map<Id,CSWP_Group__c> cswpGroupMap = new  Map<Id,CSWP_Group__c> ([SELECT Id ,(Select Id,cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Sets__r) From Cswp_Group__c WHERE ID =: cswpGroupIds]);
        Set<Id> permSetIds = new Set<Id>();
        Map<Id,Set<Id>> userToPermissionSets = new Map<Id,Set<Id>>();
        for(Id uID : userIds) {
            for(Id cswpGroupId : userToCswpGroups.get(uID)){
                for(CSWP_Assigned_Permission_Set__c cap : cswpGroupMap.get(cswpGroupId).CSWP_Assigned_Permission_Sets__r) {
                    if(userToPermissionSets.get(uID) == null){
                        userToPermissionSets.put(uID, new Set<Id>());
                    }
                    userToPermissionSets.get(uID).add(cap.cswp_Permission_Set_Id__c);
                    permSetIds.add(cap.cswp_Permission_Set_Id__c);
                }
            }
        }
        List<PermissionSetAssignment> toBeDeleted = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> existingPsaList = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetID IN :permSetIds AND AssigneeId IN :userToPermissionSets.keySet()];
        for(PermissionSetAssignment psa : existingPsaList){
            for(Id uId : userToPermissionSets.keySet()) {
                if(uId == psa.AssigneeId && userToPermissionSets.get(uId).contains(psa.PermissionSetId)) {
                    toBeDeleted.add(psa);
                }
            }
        }
        System.debug('toBeDeleted === > ' + toBeDeleted);
        if(!toBeDeleted.isEmpty()){
            delete toBeDeleted;
        }
    }

    @future
    public static void AddPermissionSetToCswpGrupUsers(String jsonCguList) { 
        List<cswp_group_user__c> guList = (List<cswp_group_user__c>) JSON.deserialize(jsonCguList, List<cswp_group_user__c>.class);
        Set<Id> cswpGroupIds = new Set<Id>();
        Map<Id,Set<Id>> userToCswpGroups = new Map<Id,Set<Id>>();
        Set<Id> userIds = new Set<Id>();
        for(cswp_group_user__c cgu : guList) {
            cswpGroupIds.add(cgu.cswp_group__c);
            userIds.add(cgu.Cswp_User__c);
            if(userToCswpGroups.get(cgu.Cswp_User__c) == null){
                userToCswpGroups.put(cgu.Cswp_User__c, new Set<Id>());
            }
            userToCswpGroups.get(cgu.Cswp_User__c).add(cgu.cswp_group__c);
        }

        Map<Id,CSWP_Group__c> cswpGroupMap = new  Map<Id,CSWP_Group__c> ([SELECT Id ,(Select Id,cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Sets__r) From Cswp_Group__c WHERE ID =: cswpGroupIds]);
        Set<Id> permSetIds = new Set<Id>();
        Map<Id,Set<Id>> userToPermissionSets = new Map<Id,Set<Id>>();
        List<PermissionSetAssignment> newPsaList = new List<PermissionSetAssignment>();
        for(Id uID : userIds) {
            for(Id cswpGroupId : userToCswpGroups.get(uID)){
                for(CSWP_Assigned_Permission_Set__c cap : cswpGroupMap.get(cswpGroupId).CSWP_Assigned_Permission_Sets__r) {
                    //if(ps_userMap.get(psId) != null && ps_userMap.get(psId).contains(userId)) continue;
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.PermissionSetId = cap.cswp_Permission_Set_Id__c;
                    psa.AssigneeId = uID;
                    newPsaList.add(psa);
                }
            }
        }
        Database.insert(newPsaList, false);
    }


}