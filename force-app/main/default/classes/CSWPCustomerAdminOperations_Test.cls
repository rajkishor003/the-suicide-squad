@isTest
public class CSWPCustomerAdminOperations_Test {
   @TestSetup
   static void makeData(){
       UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
     

        cswp_Group__c cswpGroup;
        
        System.runAs(adminUsers[0]){
            Account acc = new Account(Name='Test Account');
            insert acc;
            User testuser = CSWPTestUtil.createCommunityUser(acc);
            testuser.Email = 'e.turhan@emakina.com.tr';
            insert testuser;
            User testuser2 = CSWPTestUtil.createCommunityUser(acc);
            testuser2.Email = 'e.turhan@emakina.com.tr';
            testuser2.LanguageLocaleKey = 'ja';
            testUser2.Username = 'test1@uniquedomain.com';
            testUser2.CommunityNickname = 'test2User123';
            insert testuser2;
            User testuser3 = CSWPTestUtil.createCommunityUser(acc);
            testuser3.Email = 'e.turhan@emakina.com.tr';
            testUser3.Username = 'test3@uniquedomain.com';
            testUser3.CommunityNickname = 'test3User123';
            insert testuser3;
            cswp_GroupPermission__c groupPermission = CSWPTestUtil.createCswpGroupPermission(true, true, true, true, 'Test', 'Test', true);
            insert groupPermission;
            PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            cswpGroup = CSWPTestUtil.createCswpGroup('Test Group', acc, 'Test Description');
            cswpGroup.cswp_GroupPermission__c = groupPermission.Id;
            

            

            insert cswpGroup;
            CSWP_Assigned_Permission_Set__c assignedPermSet = CSWPTestUtil.createAssignedPermissionSet('testAssignedPermission',cswpgroup,permSet.Id);
            insert assignedPermSet;
            
            String uIdJson =  '[{"Label":"CSWP Portal User","Id":"'+testuser.Id+'"},{"Label":"CSWP Portal User","Id":"'+testuser2.Id+'"}]';
            CSWPGroupController.AddUsersToPublicGroup(cswpgroup.Id, uIdJson);
        }
       
   }
   @isTest
   static void testCreateGroupMembersAdminOperations(){
        List<cswp_group_user__c> cguList = new List<cswp_group_user__c>();
        User testUser = [SELECT Id FROM User WHERE Username = 'test3@uniquedomain.com' LIMIT 1];
        cswp_Group__c  cswpgroup= [SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c LIMIT 1]; 
        System.debug(cswpgroup);
        System.debug(testUser);
        cswp_Group_User__c gUser = new cswp_Group_User__c();
        gUser.CSWP_Group__c = cswpgroup.Id;
        gUser.Cswp_User__c = testUser.Id;
        gUser.Cswp_Is_Active__c = true;
        insert gUser;
        cswp_Group_User__c gUser2 = [SELECT Id, cswp_Group__c, cswp_User__c, cswp_Is_Active__c, Cswp_Public_Group_ID__c FROM cswp_Group_User__c WHERE Cswp_User__c=:testUser.Id LIMIT 1];
        System.debug(gUser2.Cswp_Public_Group_ID__c);
        cguList.add(gUser2);
        Test.startTest();
        CSWPCustomerAdminOperations.createGroupMembers(JSON.serialize(cguList));
        CSWPCustomerAdminOperations.AddPermissionSetToCswpGrupUsers(JSON.serialize(cguList));
        Test.stopTest();
        List<GroupMember> gm = [SELECT Id FROM GroupMember WHERE UserOrGroupId =: testUser.Id];
        Map<Id,CSWP_Group__c> cswpGroupMap = new  Map<Id,CSWP_Group__c> ([SELECT Id ,(Select Id,cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Sets__r) From Cswp_Group__c WHERE ID =: cswpGroup.Id]);
        Set<Id> permSetIds = new Set<Id>();
        Map<Id,Set<Id>> userToPermissionSets = new Map<Id,Set<Id>>();
 
           
        for(CSWP_Assigned_Permission_Set__c cap : cswpGroupMap.get(cswpgroup.Id).CSWP_Assigned_Permission_Sets__r) {
           
            permSetIds.add(cap.cswp_Permission_Set_Id__c);
        }
            
        
       
        List<PermissionSetAssignment> psaList = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetID IN :permSetIds AND AssigneeId =:testUser.Id ];
        System.assert(!gm.isEmpty());
        System.assert(!psaList.isEmpty());
        
        
   }
   @isTest
   static void testDeleteGroupMembersAdminOperations(){
       
      
        cswp_Group__c  cswpgroup= [SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c LIMIT 1]; 
        
        List<cswp_Group_User__c> gUsers = [SELECT Id, cswp_Group__c, cswp_User__c, cswp_Is_Active__c, Cswp_Public_Group_ID__c FROM cswp_Group_User__c WHERE cswp_Group__c=:cswpgroup.Id ];
        List<Id> userIds = new List<Id>();
        for (cswp_Group_User__c u : gUsers) {userIds.add(u.cswp_User__c);}
        List<GroupMember> gm = [SELECT Id FROM GroupMember WHERE GroupID=:cswpgroup.Id ];
        
        Test.startTest();
        CSWPCustomerAdminOperations.deleteGroupMembers(JSON.serialize(gm));
        CSWPCustomerAdminOperations.RemovePermissionSetFromCswpGrupUsers(JSON.serialize(gUsers));
        Test.stopTest();
       // List<PermissionSetAssignment> psaList = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId IN :userIds ];
       // System.debug(psaList);

        Set<Id> cswpGroupIds = new Set<Id>();
        cswpGroupIds.add(cswpgroup.Id);
   
        Map<Id,CSWP_Group__c> cswpGroupMap = new  Map<Id,CSWP_Group__c> ([SELECT Id ,(Select Id,cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Sets__r) From Cswp_Group__c WHERE ID =: cswpGroupIds]);
        Set<Id> permSetIds = new Set<Id>();
        Map<Id,Set<Id>> userToPermissionSets = new Map<Id,Set<Id>>();
        for(Id uID : userIds) {
            for(Id cswpGroupId : cswpGroupIds){
                for(CSWP_Assigned_Permission_Set__c cap : cswpGroupMap.get(cswpGroupId).CSWP_Assigned_Permission_Sets__r) {
                    if(userToPermissionSets.get(uID) == null){
                        userToPermissionSets.put(uID, new Set<Id>());
                    }
                    userToPermissionSets.get(uID).add(cap.cswp_Permission_Set_Id__c);
                    permSetIds.add(cap.cswp_Permission_Set_Id__c);
                }
            }
        }
       
        List<PermissionSetAssignment> psaList = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetID IN :permSetIds AND AssigneeId IN :userToPermissionSets.keySet()];
        
        List<GroupMember> gm2 = [SELECT Id FROM GroupMember WHERE GroupID =: cswpgroup.Id];
        System.assert(gm2.isEmpty());
        System.assert(psaList.isEmpty());
        
        
   }
}