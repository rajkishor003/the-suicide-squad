@isTest
public  class CSWPFeedItemTrigger_Test {
    @isTest
    static void makeData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        User testUser2 = CSWPTestUtil.createPortalUser(acc);
        insert testUser2;

        Case c = new Case();
        c.Type = 'Test';
        c.Subject = 'Test Subject';
        c.Description = 'Test Description';
        c.ContactId = testUser2.contactId;
        c.AE_Time_Spent__c = 5;
        insert c;
        
        FeedItem fi = new FeedItem();
        fi.visibility = 'AllUsers';
        fi.Body = '<i>[JIRA] Emre commented on...</i>Test feed body';
        fi.Type ='TextPost';
        fi.isRichText = true;
        fi.parentId = c.Id;
        insert fi ;

        FeedComment fc = new FeedComment();
        fc.FeedItemId = fi.Id;
        fc.CommentBody = '<img>Test image </img>';
        insert fc;
    }

}