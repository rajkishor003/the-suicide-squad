public with sharing class CSWPFileDetailController {

    @AuraEnabled (cacheable=true)
    public static FileDetailWrapper getFileDetail(String fileId) {
        cswp_File__c file = CSWPSobjectSelector.getCswpFilesByIdSet(new Set<Id> {fileId})[0];
        String workspaceId = String.isNotBlank(file.cswp_Workspace__c) ? file.cswp_Workspace__c : file.cswp_Folder__r.cswp_Workspace__c;
        CSWPContent.Permission permission = CSWPSobjectSelector.getUserPermissionsByWorkspaceId(workspaceId);
        return new FileDetailWrapper(file, permission);
    }

    @AuraEnabled 
    public static Boolean deleteFile(String fileId) {
        delete CSWPSobjectSelector.getCswpFilesByIdSet(new Set<Id> {fileId});
        return true;
    }

    public class FileDetailWrapper {
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String path {get; set;}
        @AuraEnabled
        public String downloadUrl {get; set;}
        @AuraEnabled
        public Boolean canDelete {get; set;}
        @AuraEnabled
        public Boolean canRename {get; set;}
        @AuraEnabled
        public Boolean canDownload {get; set;}
        @AuraEnabled
        public Boolean canUpload {get; set;}
        @AuraEnabled
        public String uploadedTime {get; set;}
        @AuraEnabled
        public String downloadedTime {get; set;}

        public FileDetailWrapper(cswp_File__c file, CSWPContent.Permission permission) {
            this.name = file.Name;
            this.path = file.cswp_FilePath__c;
            this.downloadUrl = file.cswp_DownloadUrl__c;
            this.canDelete = permission.canDelete;
            this.canRename = permission.canRename;
            this.canDownload = permission.canDownload;
            this.canUpload = permission.canUpload;
            this.uploadedTime = CSWPUtil.formatDateTime(file.cswp_UploadedDateTime__c);
            this.downloadedTime = CSWPUtil.formatDateTime(file.cswp_Last_Downloaded_Time__c);
        }
    }
}