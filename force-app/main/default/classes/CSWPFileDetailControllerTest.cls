@IsTest
public class CSWPFileDetailControllerTest {
    
    @TestSetup
    public static void createTestData(){
        CSWPFolderTriggerHandler.isEnabled = false;
        Account account = CSWPTestUtil.createAccount();
        insert account;

        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        // User user = new User();
        // System.runAs(adminUsers[0]){

        //     Contact c = new Contact(LastName = 'Contact Last Name', AccountId = account.id);
        //     insert c;

        //     user.ProfileID = [Select Id From Profile Where Name='CSWP Community User'].id;
        //     user.EmailEncodingKey = 'ISO-8859-1';
        //     user.LanguageLocaleKey = 'en_US';
        //     user.TimeZoneSidKey = 'America/New_York';
        //     user.LocaleSidKey = 'en_US';
        //     user.FirstName = 'first';
        //     user.LastName = 'last';
        //     user.Username = 'test@uniquedomain.com';
        //     user.CommunityNickname = 'testUser123';
        //     user.Alias = 't1';
        //     user.Email = 'no@email.com';
        //     user.IsActive = true;
        //     user.ContactId = c.Id;

        //     insert user;
        // }

        cswp_GroupPermission__c groupPermission = CSWPTestUtil.createCswpGroupPermission(true, true, true, true, 'Test', 'Test', true);
        insert groupPermission;

        cswp_Group__c cswpGroup = CSWPTestUtil.createCswpGroup('Test Group', account, 'Test Description');
        cswpGroup.cswp_GroupPermission__c = groupPermission.Id;
        insert cswpGroup;

        cswp_Workspace__c workspace = CSWPTestUtil.createCswpWorkspace('Test Workspace', account);
        workspace.cswp_Group__c = cswpGroup.Id;
        insert workspace;

        cswp_Folder__c folder = CSWPTestUtil.createCswpFolder('Test Folder', 123, workspace);
        insert folder;

        cswp_File__c file = CSWPTestUtil.createCswpFile('Test File', workspace, folder, adminUsers[0]);
        insert file;
    }

    @IsTest
    public static void testGetFileDetail() {
        cswp_File__c file = [SELECT Id FROM cswp_File__c LIMIT 1];
        CSWPFileDetailController.FileDetailWrapper wrapper = CSWPFileDetailController.getFileDetail(file.Id);
        System.assert(wrapper.canDelete);
        System.assert(wrapper.canDownload);
        System.assertEquals(wrapper.name, 'Test File');
    }

    @IsTest
    public static void testDeleteFile() {
        CSWPFileTriggerHandler.isEnabled = false;
        cswp_File__c file = [SELECT Id FROM cswp_File__c LIMIT 1];
        String fileId = file.Id;
        Boolean isDeleted = CSWPFileDetailController.deleteFile(fileId);
        System.assert(isDeleted);
        List<cswp_File__c> fileList = [SELECT Id FROM cswp_File__c WHERE Id = :fileId];
        System.assert(fileList.isEmpty());
    }
}