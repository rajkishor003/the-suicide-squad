public with sharing class CSWPFileTriggerHandler implements Triggers.Handler,
   Triggers.AfterUpdate,
   Triggers.AfterDelete,
   Triggers.AfterInsert,
   Triggers.BeforeUpdate {

    @TestVisible private static Boolean isEnabled = true;

  
  public Boolean criteria(Triggers.Context context) {
    if (System.isFuture() || System.isQueueable() || System.isBatch() || !isEnabled) {
      return false;
    }
    return true;
  }

    public static void BeforeUpdate(Triggers.Context context) {
		if(context.props.isChanged(CSWP_File__c.cswp_Status__c)){
			CSWPFileTriggerHandler.handleStatusChangeForCategoryAndApprovalProcess(context);
		}
    }

  public static void AfterUpdate(Triggers.Context context){
    Boolean isAfterUpdateEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPFileTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_UPDATE);
    if(!isAfterUpdateEnabled){
      return;
    }
    if(context.props.isChanged(cswp_File__c.Name)){
      CSWPFileTriggerHandler.handleFileNameChanged(context);
    }
    if(context.props.isChanged(CSWP_File__c.cswp_Status__c)){
      CSWPFileTriggerHandler.handleStatusChangeForApprovalProcess(context);
      CSWPFileTriggerHandler.sendEmailtoSubscribersOfWorkspace(context);
    }
  }

	public static void afterInsert(Triggers.Context context){
		CSWPFileTriggerHandler.sendEmailtoSubscribersOfWorkspace(context);
	}

  public static void AfterDelete(Triggers.Context context){
    Boolean isAfterDeleteEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWPFileTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_DELETE);
    if(!isAfterDeleteEnabled){
      return;
    }
    //CSWPRepositoryManager.deleteFileFromExternal(context.props.oldMap.keySet());
    List<String> filePaths = new List<String>();
    List<cswp_File__c> oldFiles = (List<cswp_File__c>)context.props.oldList;
    
    for(cswp_File__c file: oldFiles){
      if(string.isNotBlank(file.cswp_FilePath__c)){
        filePaths.add(file.cswp_FilePath__c);
      }
    }
    if(!filePaths.isEmpty()){
      CSWPRepositoryManager.deleteExternalItems(filePaths);
    }

	  CSWPFileTriggerHandler.sendEmailtoSubscribersOfWorkspace(context);
  }

  public static void handleFileNameChanged(Triggers.Context context){
    Map<Id,cswp_File__c> oldMap = (Map<Id,cswp_File__c>)context.props.oldMap;
    for(cswp_File__c file :(List<cswp_File__c>)context.props.newList){
      cswp_File__c oldFile = (cswp_File__c)oldMap.get(file.Id);
      if( oldFile.Name!=file.Name && String.isNotBlank(file.cswp_FilePath__c)){
        if(!file.Name.endsWith('.' + file.cswp_FileExtension__c)){
          file.addError(System.Label.cswp_FileExtensionChangeError);
        }
        String oldPath = oldFile.cswp_ParentFolderPath__c + '/' + oldFile.Name;
        CSWPRepositoryManager.updateExternalItemName(oldPath, file.Name); 
      }
    }
  }


  public static void handleStatusChangeForApprovalProcess(Triggers.Context context){
    Set<Id> eligibleFileIds = new Set<Id>();
    Map<Id,Sobject> oldMap = context.props.oldMap;   
    for(CSWP_File__c file: (List<cswp_File__c>)context.props.newList){
      String oldValue = ((CSWP_File__c)oldMap.get(file.Id)).cswp_Status__c;
      String newValue = file.cswp_Status__c;
      if( (oldValue == 'New' || oldValue == 'Waiting for Approval') && (newValue=='Approved' || newValue == 'Rejected')){
        eligibleFileIds.add(file.Id);
      }
    }
    if(!eligibleFileIds.isEmpty()){
      CSWPRepositoryManager.uploadFilesAfterApprovalProcess(eligibleFileIds);
    }
  }

	public static void sendEmailtoSubscribersOfWorkspace(Triggers.Context context) {
		Set<Id> workspaceIds = new Set<Id>();
		Set<Id> userIds = new Set<Id>();
		List<cswp_File__c> fileList = new List<cswp_File__c>();
		
		if(context.props.isDelete) {
			List<cswp_File__c> oldList = (List<cswp_File__c>)context.props.oldList; 
			for(cswp_File__c file : oldList) {
				if(file.cswp_Status__c == 'Approved') {
					fileList.add(file);
				}
			}
		}
		else if(context.props.isInsert) {
			List<cswp_File__c> newList = (List<cswp_File__c>)context.props.newList; 
			for(cswp_File__c file : newList) {
				if(file.cswp_Status__c == 'Approved') {
					fileList.add(file);
				}
			}
		}
		else {
			Map<Id,Sobject> oldMap = context.props.oldMap;
			for(CSWP_File__c file: (List<cswp_File__c>)context.props.newList) {
				String oldValue = ((CSWP_File__c)oldMap.get(file.Id)).cswp_Status__c;
				String newValue = file.cswp_Status__c;
				if( (oldValue == 'New' || oldValue == 'Waiting for Approval') && (newValue=='Approved')){
					fileList.add(file);
				}
			}
		}
		
		Map<Id, Set<Id>> ws_usersMap = new Map<Id, Set<Id>>();
		Map<Id, Cswp_Workspace__c> wsMap = new Map<Id, Cswp_Workspace__c>();
		Map<Id, List<cswp_File__c>> wsId_FilesMap = new Map<Id, List<cswp_File__c>>();

		for(cswp_File__c file : fileList) {
			workspaceIds.add(file.Cswp_Workspace__c);
			
			if(wsId_FilesMap.get(file.Cswp_Workspace__c) == null) {
				wsId_FilesMap.put(file.Cswp_Workspace__c, new List<Cswp_File__c>());
				wsId_FilesMap.get(file.Cswp_Workspace__c).add(file);
			}
			else {
				wsId_FilesMap.get(file.Cswp_Workspace__c).add(file);
			}
			
		}
		List<Cswp_Workspace__c> workspaceList = CSWPSobjectSelector.getWorkspacesByIdSet(workspaceIds);
		for(Cswp_Workspace__c ws : workspaceList) {
			wsMap.put(ws.Id, ws);
		}

		List<Cswp_Workspace_Subscription__c> wssList = CSWPSobjectSelector.getWorkspaceSubscriptionsByWorkspaceIds(workspaceIds);
		for(Cswp_Workspace_Subscription__c wss : wssList) {
			userIds.add(wss.CSwp_Subscriber__c);
			if(ws_usersMap.get(wss.Cswp_Workspace__c) == null) {
				ws_usersMap.put(wss.Cswp_Workspace__c, new Set<Id>());
				ws_usersMap.get(wss.Cswp_Workspace__c).add(wss.Cswp_Subscriber__c);
			}
			else {
				ws_usersMap.get(wss.Cswp_Workspace__c).add(wss.Cswp_Subscriber__c);
			}
		}

		Map<Id, User> userMap = new Map<Id, User> (CSWPSobjectSelector.getUsersByIds(userIds));
		Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage>();
		for(ID wsId : ws_usersMap.keySet()) {
			Cswp_workspace__c thisWs = wsMap.get(wsId);
			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
			List<String> toAddresses = new List<String>();
            for(Id userId : ws_usersMap.get(wsId)) {
				toAddresses.add(userMap.get(userId).Email);
			}
			message.setToAddresses(toAddresses);
			message.optOutPolicy = 'FILTER';
			message.subject = Label.cswp_subscribe_mail_subject;
			String mailBody = '';
			mailBody += Label.cswp_subscribe_mail_body + ' \n' ;
			for(cswp_File__c file: wsId_FilesMap.get(wsId)) {
				mailBody += ' \n' + file.Name ;
			}
			mailBody += ' \n \n '+Label.cswp_subscribe_mail_body_footer;
			mailBody += ' \n '+ thisWs.Name;
			mailBody += ' \n \n '+ Label.cswp_subscribe_mail_body_footer2;
			message.plainTextBody = mailBody;
      		messages.add(message);
		}
    	Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
	}
    
    public static void handleStatusChangeForCategoryAndApprovalProcess(Triggers.Context context) {
        Map<Id,Sobject> oldMap = context.props.oldMap;

        for(CSWP_File__c file: (List<cswp_File__c>)context.props.newList) {
            String oldValue = ((CSWP_File__c)oldMap.get(file.Id)).cswp_Status__c;
            String newValue = file.cswp_Status__c;

            if( (file.Cswp_Has_Category__c == false) && (oldValue == 'New' || oldValue == 'Waiting for Approval') && (newValue=='Approved')) {
                file.addError(System.Label.Cswp_Category_Selection_for_Approval);
            }
        }
    }
}