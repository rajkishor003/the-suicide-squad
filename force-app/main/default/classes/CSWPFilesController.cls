public with sharing class CSWPFilesController {
    
    @AuraEnabled (cacheable = true)
    public static List<FilesWrapper> getLastlyAddedFiles() {
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ClientDocument').getRecordTypeId();
        User user = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        List<FilesWrapper> fileList = new List<FilesWrapper> ();
        Map<Id, cswp_Workspace__c> workspaceMap = new Map<Id, cswp_Workspace__c> ([SELECT Id FROM cswp_Workspace__c WHERE RecordTypeId = :clientDocumentRecordId]);
        for(cswp_File__c cswpFile : [SELECT Id, Name, cswp_UploadedBy__r.FirstName, cswp_UploadedBy__r.LastName, cswp_UploadedDateTime__c FROM cswp_File__c WHERE (cswp_Folder__r.cswp_Workspace__c IN :workspaceMap.keySet() AND cswp_Workspace__c IN :workspaceMap.keySet()) AND cswp_Status__c = 'Approved' ORDER BY cswp_UploadedDateTime__c DESC NULLS LAST LIMIT 5]) {
            //DateTime convertedUplodedTime = CSWPUtil.convertToUserTime(cswpFile.cswp_UploadedDateTime__c);
            String convertedUploadedTime = CSWPUtil.formatDateTime(cswpFile.cswp_UploadedDateTime__c);
            System.debug('convertedUploadedTime ----> ' + convertedUploadedTime);
            String ownerName = String.isBlank(cswpFile.cswp_UploadedBy__r.FirstName) ? cswpFile.cswp_UploadedBy__r.LastName : cswpFile.cswp_UploadedBy__r.FirstName + ' ' + cswpFile.cswp_UploadedBy__r.LastName;
            fileList.add(new FilesWrapper(cswpFile.Name, ownerName, convertedUploadedTime, cswpFile.Id, '/cswp/s/cswp-file-detail?recordId=' + cswpFile.Id));
        }
        return fileList;
    }

    @AuraEnabled
    public static List<FilesWrapper> getRefreshedLastlyAddedFiles() {
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ClientDocument').getRecordTypeId();
        User user = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        List<FilesWrapper> fileList = new List<FilesWrapper> ();
        Map<Id, cswp_Workspace__c> workspaceMap = new Map<Id, cswp_Workspace__c> ([SELECT Id FROM cswp_Workspace__c WHERE RecordTypeId = :clientDocumentRecordId]);
        for(cswp_File__c cswpFile : [SELECT Id, Name, cswp_UploadedBy__r.FirstName, cswp_UploadedBy__r.LastName, cswp_UploadedDateTime__c FROM cswp_File__c WHERE (cswp_Folder__r.cswp_Workspace__c IN :workspaceMap.keySet() AND cswp_Workspace__c IN :workspaceMap.keySet()) AND cswp_Status__c = 'Approved' ORDER BY cswp_UploadedDateTime__c DESC NULLS LAST LIMIT 5]) {
            //DateTime convertedUplodedTime = CSWPUtil.convertToUserTime(cswpFile.cswp_UploadedDateTime__c);
            String convertedUploadedTime = CSWPUtil.formatDateTime(cswpFile.cswp_UploadedDateTime__c);
            System.debug('convertedUploadedTime ----> ' + convertedUploadedTime);
            String ownerName = String.isBlank(cswpFile.cswp_UploadedBy__r.FirstName) ? cswpFile.cswp_UploadedBy__r.LastName : cswpFile.cswp_UploadedBy__r.FirstName + ' ' + cswpFile.cswp_UploadedBy__r.LastName;
            fileList.add(new FilesWrapper(cswpFile.Name, ownerName, convertedUploadedTime, cswpFile.Id, '/cswp/s/cswp-file-detail?recordId=' + cswpFile.Id));
        }
        return fileList;
    }

    @AuraEnabled (cacheable = true)
    public static List<FilesWrapper> getLastlyDownloadedFiles() {
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ClientDocument').getRecordTypeId();
        User user = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        List<FilesWrapper> fileList = new List<FilesWrapper> ();
        Map<Id, cswp_Workspace__c> workspaceMap = new Map<Id, cswp_Workspace__c> ([SELECT Id FROM cswp_Workspace__c WHERE RecordTypeId = :clientDocumentRecordId]);
        for(cswp_File__c cswpFile : [SELECT Id, Name, cswp_UploadedBy__r.FirstName, cswp_UploadedBy__r.LastName, cswp_Last_Downloaded_Time__c FROM cswp_File__c WHERE (cswp_Folder__r.cswp_Workspace__c IN :workspaceMap.keySet() AND cswp_Workspace__c IN :workspaceMap.keySet()) AND cswp_Status__c = 'Approved' AND cswp_Last_Downloaded_Time__c != NULL ORDER BY cswp_Last_Downloaded_Time__c DESC NULLS LAST LIMIT 5]) {
            //DateTime convertedDownloadTime = CSWPUtil.convertToUserTime(cswpFile.cswp_Last_Downloaded_Time__c);
            String convertedDownloadTime = CSWPUtil.formatDateTime(cswpFile.cswp_Last_Downloaded_Time__c);
            String ownerName = String.isBlank(cswpFile.cswp_UploadedBy__r.FirstName) ? cswpFile.cswp_UploadedBy__r.LastName : cswpFile.cswp_UploadedBy__r.FirstName + ' ' + cswpFile.cswp_UploadedBy__r.LastName;
            fileList.add(new FilesWrapper(cswpFile.Name, ownerName, convertedDownloadTime, cswpFile.Id, '/cswp/s/cswp-file-detail?recordId=' + cswpFile.Id));
        }
        return fileList;
    }

    @AuraEnabled
    public static List<FilesWrapper> getRefreshedLastlyDownloadedFiles() {
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ClientDocument').getRecordTypeId();
        User user = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        List<FilesWrapper> fileList = new List<FilesWrapper> ();
        Map<Id, cswp_Workspace__c> workspaceMap = new Map<Id, cswp_Workspace__c> ([SELECT Id FROM cswp_Workspace__c WHERE RecordTypeId = :clientDocumentRecordId]);
        for(cswp_File__c cswpFile : [SELECT Id, Name, cswp_UploadedBy__r.FirstName, cswp_UploadedBy__r.LastName, cswp_Last_Downloaded_Time__c FROM cswp_File__c WHERE (cswp_Folder__r.cswp_Workspace__c IN :workspaceMap.keySet() AND cswp_Workspace__c IN :workspaceMap.keySet()) AND cswp_Status__c = 'Approved' AND cswp_Last_Downloaded_Time__c != NULL ORDER BY cswp_Last_Downloaded_Time__c DESC NULLS LAST LIMIT 5]) {
            //DateTime convertedDownloadTime = CSWPUtil.convertToUserTime(cswpFile.cswp_Last_Downloaded_Time__c);
            String convertedDownloadTime = CSWPUtil.formatDateTime(cswpFile.cswp_Last_Downloaded_Time__c);
            String ownerName = String.isBlank(cswpFile.cswp_UploadedBy__r.FirstName) ? cswpFile.cswp_UploadedBy__r.LastName : cswpFile.cswp_UploadedBy__r.FirstName + ' ' + cswpFile.cswp_UploadedBy__r.LastName;
            fileList.add(new FilesWrapper(cswpFile.Name, ownerName, convertedDownloadTime, cswpFile.Id, '/cswp/s/cswp-file-detail?recordId=' + cswpFile.Id));
        } 
        return fileList;
    }

    @AuraEnabled (cacheable = true)
    public static List<FilesWrapper> getAllFiles() {
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ClientDocument').getRecordTypeId();
        User user = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        Map<Id, cswp_Workspace__c> workspaceMap = new Map<Id, cswp_Workspace__c> ([SELECT Id FROM cswp_Workspace__c WHERE RecordTypeId = :clientDocumentRecordId]);
        List<FilesWrapper> fileList = new List<FilesWrapper> ();
        for(cswp_File__c cswpFile : [SELECT Id, Name, cswp_UploadedBy__r.FirstName, cswp_UploadedBy__r.LastName, cswp_Last_Downloaded_Time__c FROM cswp_File__c WHERE (cswp_Folder__r.cswp_Workspace__c IN :workspaceMap.keySet() AND cswp_Workspace__c IN :workspaceMap.keySet()) AND cswp_Status__c = 'Approved' LIMIT 5]) {
            //DateTime convertedDownloadTime = CSWPUtil.convertToUserTime(cswpFile.cswp_Last_Downloaded_Time__c);
            String convertedDownloadTime = CSWPUtil.formatDateTime(cswpFile.cswp_Last_Downloaded_Time__c);
            String ownerName = String.isBlank(cswpFile.cswp_UploadedBy__r.FirstName) ? cswpFile.cswp_UploadedBy__r.LastName : cswpFile.cswp_UploadedBy__r.FirstName + ' ' + cswpFile.cswp_UploadedBy__r.LastName;
            fileList.add(new FilesWrapper(cswpFile.Name, ownerName, convertedDownloadTime, cswpFile.Id, '/cswp/s/cswp-file-detail?recordId=' + cswpFile.Id));
        }
        return fileList;
    }
    
    
    @AuraEnabled(cacheable = true)
    public static List<FolderWrapper> getFolders(){
        Id DocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ProductDocument').getRecordTypeId();
        User user = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        List<FolderWrapper> folderList = new List<FolderWrapper> ();
        for(cswp_Workspace__c cswpfolder : [SELECT Id, Name FROM cswp_Workspace__c WHERE  RecordTypeId =:DocumentRecordId]){
            folderList.add(new FolderWrapper(cswpfolder.Name, cswpfolder.id, '/cswp/s/technical-documentation?recordId=' + cswpfolder.id));
        }
        return folderList;
    }

    public class FilesWrapper {
        @AuraEnabled
        public String fileName {get;set;}
        @AuraEnabled
        public String fileOwner {get;set;}
        @AuraEnabled
        public String fileDateTime {get;set;}
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String recordURL {get;set;}

        public FilesWrapper(String fileName, String fileOwner, String fileDateTime, String id, String recordURL) {
            this.id = id;
            this.filename = fileName;
            this.fileOwner = fileOwner;
            this.fileDateTime = fileDateTime;
            this.recordURL = recordURL;
        }
    }

    public class FolderWrapper{
        @AuraEnabled
        public String folderName{get;set;}
        @AuraEnabled
        public String id{get;set;}
        @AuraEnabled
        public String recordURL {get;set;}
        
        public FolderWrapper(String folderName, String id, String recordURL){
            this.id = id;
            this.folderName = folderName;
            this.recordUrl = recordURL;
        }

    }
}