@isTest
private class CSWPFilesController_Test {
    @TestSetup
    static void makeData(){
        Account testaccount = new Account(Name='Test Account');
        insert testaccount;
        User testUser = CSWPTestUtil.createCommunityUser(testaccount);
        insert testUser;
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        
        System.runAs(adminUsers[0]){
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            
            insert new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = ps.Id);
        }
        
        Id clientDocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByName().get('Document Sharing').getRecordTypeId();
        cswp_Workspace__c workspace = CSWPTestUtil.createCswpWorkspace('Test Workspace', testaccount);
        workspace.cswp_CreateOnExternal__c = false;
        workspace.RecordTypeId = clientDocumentRecordId;
        insert workspace;

        CSWPFolderTriggerHandler.isEnabled = false;
        cswp_Folder__c folder = CSWPTestUtil.createCswpFolder('Test Folder', 100.00, workspace);
        insert folder;

        List<cswp_File__c> files = new List<cswp_File__c>();
        files.add(CSWPTestUtil.createCswpFile('Test File 1', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 2', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 3', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 4', workspace, folder, testUser));
        files.add(CSWPTestUtil.createCswpFile('Test File 5', workspace, folder, testUser));
        insert files;

        Id DocumentRecordId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ProductDocument').getRecordTypeId();
        cswp_Workspace__c workspace1 = CSWPTestUtil.createCswpWorkspace('Test Workspace 1', testaccount);
        workspace1.cswp_CreateOnExternal__c = false;
        workspace1.RecordTypeId = DocumentRecordId;
        insert workspace1;

        CSWPFolderTriggerHandler.isEnabled = false;
        cswp_Folder__c folder1 = CSWPTestUtil.createCswpFolder('Test Folder 1', 100.00, workspace1);
        insert folder1;

    }

    @IsTest
    private static void testFileMethods(){
        List<Account> testaccount = [SELECT Id FROM Account];
       
        List<User> test = [SELECT Id From User];

      
        
        List<CSWPFilesController.FilesWrapper> filesGeneratedLastlyAdded = new List<CSWPFilesController.FilesWrapper>();
        List<CSWPFilesController.FilesWrapper> filesGeneratedLastlyDownloaded = new List<CSWPFilesController.FilesWrapper>();
        List<CSWPFilesController.FilesWrapper> filesGeneratedAll = new List<CSWPFilesController.FilesWrapper>();
        System.runAs(test[0]){
            filesGeneratedLastlyAdded = CSWPFilesController.getLastlyAddedFiles();
            filesGeneratedLastlyDownloaded = CSWPFilesController.getLastlyDownloadedFiles();
            filesGeneratedAll = CSWPFilesController.getAllFiles();
        }
        System.assertNotEquals(0,filesGeneratedLastlyAdded.size(),'files list size is bigger than 0');
        System.assertNotEquals(0,filesGeneratedLastlyDownloaded.size(),'files list size is bigger than 0');
        System.assertNotEquals(0,filesGeneratedAll.size(),'files list size is bigger than 0');
    }

    @IsTest
    private static void testgetFolders(){
        List<Account> testaccount = [SELECT Id FROM Account];
      
        List<User> test = [SELECT Id From User];

     
        
        List<CSWPFilesController.FolderWrapper> folders = new List<CSWPFilesController.FolderWrapper>();
        
        System.runAs(test[0]){
            folders = CSWPFilesController.getFolders();
        }
        System.assertNotEquals(0,folders.size(),'folder was retrieved');
    }

}