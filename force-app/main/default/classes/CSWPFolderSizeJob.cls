public class CSWPFolderSizeJob implements Queueable{
    public String folderId {get;private set;}

    public CSWPFolderSizeJob(String initialFolderId) {
        this.folderId = initialFolderId;
    }

    public void execute(QueueableContext context){
        this.calculateFolderSize(this.folderId);
    }
  
    public void calculateFolderSize(String theFolderId){
       //cswp_TotalSize__c   : total file sizes | rollup field that sum up approved files
       //cswp_ContentSize__c : total file size + total folder size(contentsize__c)
       List<cswp_Folder__c> folder = [
        SELECT Id,Name, cswp_ParentFolder__c,cswp_TotalSize__c,cswp_ContentSize__c,
        (SELECT Id,Name, cswp_TotalSize__c,cswp_ContentSize__c FROM cswp_SubFolders__r WHERE cswp_ContentSize__c >0)
        FROM cswp_Folder__c
        WHERE Id=:theFolderId LIMIT 1];
  
        cswp_Folder__c theFolder = folder.get(0);    
        Decimal totalContentSize = theFolder.cswp_TotalSize__c;
        for(cswp_Folder__c subf :theFolder.cswp_SubFolders__r){
          totalContentSize+= subf.cswp_ContentSize__c;
        }
        theFolder.cswp_ContentSize__c = totalContentSize;
        Database.SaveResult res = Database.update(theFolder);
        CSWPUtil.debugLog('Folder SAVE Result :>' + res);
        if(String.isNotBlank(theFolder.cswp_ParentFolder__c)){
          calculateFolderSize(theFolder.cswp_ParentFolder__c);
        }else{
          return;
        }
    }
}