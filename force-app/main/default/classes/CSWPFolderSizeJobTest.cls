@isTest
private with sharing class CSWPFolderSizeJobTest {
    @isTest
    static void testJob(){
        CSWPFolderTriggerHandler.isEnabled = false;
        Account account = CSWPTestUtil.createAccount();
        insert account;

        cswp_Workspace__c workspace = CSWPTestUtil.createCswpWorkspace('Test Workspace', account);
        insert workspace;

        cswp_Folder__c folder = CSWPTestUtil.createCswpFolder('Test Folder', 100, workspace);
        insert folder;

        cswp_Folder__c subFolder = CSWPTestUtil.createSubFolder('Test Subfolder',folder,30);
        subFolder.cswp_Workspace__c=workspace.Id;
        insert subFolder;

        Test.startTest();
        CSWPFolderSizeJob job = new CSWPFolderSizeJob (folder.Id);
        ID jobID = System.enqueueJob(job);
        Test.stopTest(); 
        
        System.assertNotEquals(jobID,null,'job is queued');
    }
}