/*************************************************************************************************************
 * @name			CSWPFolderTriggerHandler
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			10 / 12 / 2020
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * No.		Date			    Author	  Description
 * ----	------------	---------	----------------------------------------------
 * 1.0		2020-12-10		oguzalp	   Changes desription
 * 1.1   2020-12-30    ozguneser   Added method to get foldersize from Sharepoint.
 *
 **************************************************************************************************************/
public with sharing class CSWPFolderTriggerHandler implements Triggers.Handler, Triggers.AfterInsert, Triggers.AfterUpdate, Triggers.AfterDelete {
  @TestVisible
  public static Boolean isEnabled = true;

  public Boolean criteria(Triggers.Context context) {
    System.debug('CSWPFolderTriggerHandler.criteria.isQueueable:>>>.' + System.isQueueable());
    if (System.isQueueable() || System.isBatch() || !isEnabled) {
      return false;
    }
    return true;
  }

  public void AfterInsert(Triggers.Context context) {
    Boolean isAfterInsertEnabled = CSWPConfigurationManager.IsTriggerEnabled(
      CSWPFolderTriggerHandler.class.getName(),
      CSWPUtil.ON_AFTER_INSERT
    );
    if (!isAfterInsertEnabled) {
      return;
    }
    //Create folder on after folder insert
    if (!System.isFuture()) {
      CSWPRepositoryManager.createFolderOnSite(context.props.newMap.keySet());
    }
  }

  public void AfterUpdate(Triggers.Context context) {
    Boolean isAfterUpdateEnabled = CSWPConfigurationManager.IsTriggerEnabled(
      CSWPFolderTriggerHandler.class.getName(),
      CSWPUtil.ON_AFTER_UPDATE
    );
    if (!isAfterUpdateEnabled) {
      return;
    }

    Set<Id> ids = new Set<Id>();
    for(cswp_Folder__c folder: (List<cswp_Folder__c>)context.props.newList){
      if(String.isNotBlank(folder.cswp_ParentHubItemID__c) && String.isBlank(folder.cswp_HubItemId__c)){
        ids.add(folder.Id);
      }
    }
    /** context.props.isChanged(cswp_Folder__c.cswp_ParentHubItemId__c) */
    if(!System.isFuture() && !ids.isEmpty()){
      CSWPRepositoryManager.getHubItemByParentFolderAsync(  ids
       //new Set<Id>( context.props.filterChanged(cswp_Folder__c.cswp_ParentHubItemId__c) )
      );
    }

    if (!System.isFuture() && context.props.isChanged(cswp_Folder__c.Name)) {
      handleNameChange(context);
    }
    //This can be triggered within future method
    calculateFolderSize(context);
  }

  public void AfterDelete(Triggers.Context context) {
    Boolean isAfterDeleteEnabled = CSWPConfigurationManager.IsTriggerEnabled(
      CSWPFolderTriggerHandler.class.getName(),
      CSWPUtil.ON_AFTER_DELETE
    );
    if (!isAfterDeleteEnabled) {
      return;
    }

    if (System.isFuture()) {
      return;
    }

    deleteThen(context);
  }

  public static void deleteThen(Triggers.Context context) {
    List<String> externaIds = new List<String>();
    List<String> parentFolderIds = new List<String>();
    for (cswp_Folder__c folder : (List<cswp_Folder__c>) context.props.oldList) {
      if (String.isNotBlank(folder.cswp_ExternalId__c)) {
        externaIds.add(folder.cswp_ExternalId__c);
      }
      if (String.isNotBlank(folder.cswp_ParentFolder__c)) {
        parentFolderIds.add(folder.cswp_ParentFolder__c);
      }
    }
    if (!externaIds.isEmpty()) {
      CSWPRepositoryManager.deleteFolderFromExternal(externaIds);
    }

    //TODO: Refactor it! Move it to do generic method
    if (
      !parentFolderIds.isEmpty() &&
      Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()
    ) {
      Id jobId = System.enqueueJob(new CSWPFolderSizeJob(parentFolderIds[0]));
    }
  }

  public static void handleNameChange(Triggers.Context context) {
    Map<Id, SObject> oldMap = (Map<Id, SObject>) context.props.oldMap;
    Set<Id> eligibleIds = new Set<Id>();
    for (
      cswp_Folder__c newFolder : (List<cswp_Folder__c>) context.props.newList
    ) {
      cswp_Folder__c oldFolder = (cswp_Folder__c) oldMap.get(newFolder.Id);
      if (
        String.isNotBlank(newFolder.cswp_Path__c) &&
        oldFolder.Name != newFolder.Name
      ) {
        String oldPath =
          oldFolder.cswp_ParentFolderPath__c +
          '/' +
          oldFolder.Name;
        CSWPRepositoryManager.updateExternalItemName(oldPath, newFolder.Name);
      }
    }
  }

  public static void calculateFolderSize(Triggers.Context context) {
    // Check the rollup field does trigger the update trigger
    if (context.props.isChanged(cswp_Folder__c.cswp_TotalSize__c)) {
      List<Id> folderIds = context.props.filterChanged(
        cswp_Folder__c.cswp_TotalSize__c
      );
      if (
        !folderIds.isEmpty() &&
        Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()
      ) {
        Id jobId = System.enqueueJob(new CSWPFolderSizeJob(folderIds[0]));
      }
    }
  }
}