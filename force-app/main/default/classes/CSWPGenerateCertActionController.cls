public with sharing class CSWPGenerateCertActionController {

    @AuraEnabled
    public static string getContentDocumentID( string recordID ){
        try {
            Map<String, String> idMap = new Map<String, String>();
            Cswp_Calibration_Box_Data__c cbd = [SELECT Id, Cswp_Certificate_Template_Picklist__c From Cswp_Calibration_Box_Data__c WHERE ID =: recordID];
            List<ContentDocument> cdList = CSWPSObjectSelector.GetContentDocumentWithVersions(recordID, 'Data File');
            idMap.put('cdId', cdList[0].Id);
            idMap.put('cbdTemplate', cbd.Cswp_Certificate_Template_Picklist__c);
            return JSON.serialize(idMap);
        } catch (Exception e) { throw new AuraHandledException(e.getMessage()); }
    }

    @AuraEnabled
    public static void storeCertificate(String linkId, String pageLanguage, string entityId, Boolean fullRes, String calBoxDataId, String certTemplate){
        try {
            Cswp_CalBoxFileUploadController.StoreCertificate(linkId, pageLanguage, entityId, fullRes, calBoxDataId, certTemplate);
        } catch (Exception e) { throw new AuraHandledException(e.getMessage()); }
    }

    @AuraEnabled
    public static void logActivity(String itemId, String itemName, String action, String customMessage){
        try {
            CswpActivity.EventMessage msg = new CswpActivity.EventMessage();
            msg.itemName = itemName;
            msg.itemId = itemId;
            msg.message = customMessage;
            msg.actionName = action;
            Database.SaveResult saveResult = new CSWPActivity(msg).publishEvent();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}