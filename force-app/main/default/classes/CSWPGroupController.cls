public with sharing class CSWPGroupController {
    
    @AuraEnabled
    public static String AddUsersToPublicGroup(String cswpGroupId, String selectedUser) {
        try {
            Map<Id,Cswp_Group__c> groupMap = CSWPSobjectSelector.getCswpGroupsByIds(new Set<Id>{cswpGroupId});
            List<Object> lst_JsonParse = (List<Object>)Json.deserializeUntyped(selectedUser);
            List<GroupMember> gmList = new List<GroupMember>();
            //Cswp Group User List
            List<cswp_group_user__c> guList = new List<cswp_group_user__c>();
            Set<Id> userIds = new Set<Id>();
            for(Object obj : lst_JsonParse)
            {
                Map<String,Object> mp_StrObj = (Map<string,Object>)obj;
                GroupMember gm = new GroupMember();
                gm.GroupId = groupMap.get(cswpGroupId).cswp_PublicGroupId__c;
                gm.UserOrGroupId = (String)mp_StrObj.get('Id');
                userIds.add((Id)mp_StrObj.get('Id'));
                gmList.add(gm);

                // Create Cswp Group User
                cswp_group_user__c gu = new cswp_group_user__c();
                gu.CSWP_Group__c = (Id)cswpGroupId;
                gu.Cswp_User__c = (Id)mp_StrObj.get('Id');
                gu.Cswp_Is_Active__c = true;
                guList.add(gu);
            }
            insert gmList;
            // Called futured method to avoid the mixed dml operation error
            //AddUsersToCswpGroupUser(JSON.serialize(guList));
            Id jobId = System.enqueueJob(new CSWPHandleGroupUserAssignmentsQueueable(JSON.serialize(guList)));

            return HandlePermissionSetForCswpGroupUserChange(cswpGroupId, userIds, 'Add');
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @future 
    public static void AddUsersToCswpGroupUser(String serializedGUList) {
        List<cswp_group_user__c> guList = (List<cswp_group_user__c>) JSON.deserialize(serializedGUList, List<cswp_group_user__c>.class);
        List<cswp_group_user__c> guList2Upsert = new List<cswp_group_user__c>();
        Set<Id> cswpGroupIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        for(cswp_group_user__c gu : guList){
            cswpGroupIds.add(gu.CSWP_Group__c);
            userIds.add(gu.Cswp_User__c);
        }
        List<Cswp_Group_user__c> existingGuList = CSWPSobjectSelector.getGroupUsersByCswpGroupAndUserIds(cswpGroupIds, userIds);
        Map<String, Cswp_Group_user__c> existingGuMap = new Map<String, Cswp_Group_user__c>();
        
        for(Cswp_Group_user__c gu : existingGuList) {
            existingGuMap.put(String.valueOf(gu.CSWP_Group__c)+String.valueOf(gu.Cswp_User__c), gu);
        }
        
        for(cswp_group_user__c gu : guList) {
            String idComb = String.valueOf(gu.CSWP_Group__c)+String.valueOf(gu.Cswp_User__c);
            if(existingGuMap.get(idComb) != null) {
                existingGuMap.get(idComb).Cswp_Is_Active__c = true;
                guList2Upsert.add(existingGuMap.get(idComb));
            }
            else {
                guList2Upsert.add(gu);
            }
        }    
        upsert guList2Upsert;
    }

    @AuraEnabled
    public static void RemoveUsersFromPublicGroup(String cswpGroupId, String selectedUser) {
        try {
            Map<Id,Cswp_Group__c> groupMap = CSWPSobjectSelector.getCswpGroupsByIds(new Set<Id>{cswpGroupId});
            List<Object> lst_JsonParse = (List<Object>)Json.deserializeUntyped(selectedUser);
            Set<Id> userIds = new Set<Id>();
            for(Object obj : lst_JsonParse)
            {
                Map<String,Object> mp_StrObj = (Map<string,Object>)obj;
                userIds.add((Id)mp_StrObj.get('Cswp_User__c'));
            }
            List<GroupMember> gmList = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember Where UserOrGroupId IN: userIds AND GroupId =: groupMap.values()[0].cswp_PublicGroupId__c];
            delete gmList;
            HandlePermissionSetForCswpGroupUserChange(cswpGroupId, userIds, 'Remove');
            List<cswp_group_user__c> deletedGUList = [SELECT Id, Cswp_User__c,CSWP_Group__c FROM cswp_group_user__c WHERE Cswp_User__c IN:userIds AND CSWP_Group__c =:cswpGroupId];
            DeleteUsersFromCswpGroupUser(JSON.serialize(deletedGUList));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @future 
    public static void DeleteUsersFromCswpGroupUser(String serializedGUList) {
        List<cswp_group_user__c> guList = (List<cswp_group_user__c>) JSON.deserialize(serializedGUList, List<cswp_group_user__c>.class); 
        delete guList;
    }

    public static String HandlePermissionSetForCswpGroupUserChange(String cswpGroupId, Set<Id> userIds, String operation) {
        CSWP_Group__c cswpGroup = [SELECT Id From Cswp_Group__c WHERE ID =: cswpGroupId];

        List<CSWP_Assigned_Permission_Set__c> apsList = [Select ID,cswp_Permission_Set_Id__c From CSWP_Assigned_Permission_Set__c WHERE cswp_Group__c =: cswpGroupId ];
        Set<Id> permSetIds = new Set<Id>();
        
        for(CSWP_Assigned_Permission_Set__c aps : apsList) {
            permSetIds.add(aps.cswp_Permission_Set_Id__c);
        }

        Map<Id, Set<Id>> ps_userMap = new Map<Id, Set<Id>>();
        List<PermissionSetAssignment> existingPsaList = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetID IN:permSetIds AND AssigneeId IN: userIds];
        
        for(PermissionSetAssignment psa : existingPsaList) {
            if(ps_userMap.get(psa.PermissionSetId) == null) {
                ps_userMap.put(psa.PermissionSetId, new Set<Id>());
                ps_userMap.get(psa.PermissionSetId).add(psa.AssigneeId);
            }
            else {
                ps_userMap.get(psa.PermissionSetId).add(psa.AssigneeId);
            }
        }

        if(operation == 'Add') {
            List<PermissionSetAssignment> newPsaList = new List<PermissionSetAssignment>();
            for(String psId : permSetIds) {
                for(String userId : userIds) {
                    if(ps_userMap.get(psId) != null && ps_userMap.get(psId).contains(userId)) continue;
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.PermissionSetId = psId;
                    psa.AssigneeId = userId;
                    newPsaList.add(psa);
                }
            }
            Database.SaveResult[] saveResultList = database.insert(newPsaList, false);
            for(Database.SaveResult sr : saveResultList) {
                if(sr.getErrors().size() > 0) {
                    return 'user licence error';
                }
            }
        }
        else if(operation == 'Remove') {
            database.delete(existingPsaList);
        }
        return 'Success';
    }

    @AuraEnabled
    public static List<cswp_group_user__c> GetCswpGroupUsers() { 
        System.debug('License Checkkk === > ' + FeatureManagement.checkPermission('Cswp_Customer_Admin_Page_Access'));
        Id currentUserId = UserInfo.getUserId();
        List<cswp_group_user__c> getCswpGroupIds = [SELECT Id, Cswp_User__c,CSWP_Group__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE Cswp_User__c =:currentUserId];
        Set<Id> cswpGroupIds = new Set<Id>();
        for(cswp_group_user__c cgu : getCswpGroupIds) {
            cswpGroupIds.add(cgu.CSWP_Group__c);
        }
        List<cswp_group_user__c> cguList = [SELECT Id,Cswp_User__c,CSWP_Group__c,CSWP_Group__r.Name,Cswp_Group_Name__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE CSWP_Group__c IN:cswpGroupIds ORDER BY Cswp_User_Name__c];
        return cguList;
    }
    @AuraEnabled
    public static List<cswp_group_user__c> MakeActiveCswpGroupUsers(String selectedUser) {
        Set<Id> userIds = new Set<Id>();
        Set<Id> cswpGroupIds = new Set<Id>(); 
        List<Object> lst_JsonParse = (List<Object>)Json.deserializeUntyped(selectedUser);
        for(Object obj : lst_JsonParse)
        {
            Map<String,Object> mp_StrObj = (Map<string,Object>)obj;
            userIds.add((Id)mp_StrObj.get('Cswp_User__c'));
            cswpGroupIds.add((Id)mp_StrObj.get('CSWP_Group__c'));
        }
        System.debug('cswpGroupIds ==> ' + cswpGroupIds);
        System.debug('userIds ==> ' + userIds);

        Id currentUserId = UserInfo.getUserId();
        List<cswp_group_user__c> cguList = [SELECT Id, Cswp_User__c,CSWP_Group__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE CSWP_Group__c IN :cswpGroupIds AND Cswp_User__c IN:userIds];
        for(cswp_group_user__c cgu :cguList){
            cgu.Cswp_Is_Active__c = true;
        }
        update cguList;
        return cguList;
    }

    @AuraEnabled
    public static List<cswp_group_user__c> MakeDeactiveCswpGroupUsers(String selectedUser) {
        Set<Id> cguIds = new Set<Id>();
        List<Object> lst_JsonParse = (List<Object>)Json.deserializeUntyped(selectedUser);
        for(Object obj : lst_JsonParse)
        {
            Map<String,Object> mp_StrObj = (Map<string,Object>)obj;
            cguIds.add((Id)mp_StrObj.get('Id'));
        }  
        List<cswp_group_user__c> cguList = [SELECT Id, Cswp_User__c,CSWP_Group__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c,Cswp_Public_Group_ID__c FROM cswp_group_user__c WHERE Id IN :cguIds];
        for(cswp_group_user__c cgu :cguList){
            cgu.Cswp_Is_Active__c = false;
        }
        update cguList;
        return cguList;
    }

    @AuraEnabled
    public static List<cswp_group_user__c> getAvailableUsersToActivate(){
        try {
            Id currentUserId = UserInfo.getUserId();
            List<cswp_group_user__c> getCswpGroupIds = [SELECT Id, Cswp_User__c,CSWP_Group__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE Cswp_User__c =:currentUserId];
            Set<Id> cswpGroupIds = new Set<Id>();
            for(cswp_group_user__c cgu : getCswpGroupIds) {
                cswpGroupIds.add(cgu.CSWP_Group__c);
            }
            List<cswp_group_user__c> cguList = [SELECT Id,Cswp_User__c,CSWP_Group__c,CSWP_Group__r.Name,Cswp_Group_Name__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE CSWP_Group__c IN:cswpGroupIds AND Cswp_Is_Active__c = false ORDER BY Cswp_User_Name__c];
            return cguList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<cswp_group_user__c> getAvailableUsersToDeactivate(){
        try {
            Id currentUserId = UserInfo.getUserId();
            List<cswp_group_user__c> getCswpGroupIds = [SELECT Id, Cswp_User__c,CSWP_Group__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE Cswp_User__c =:currentUserId];
            Set<Id> cswpGroupIds = new Set<Id>();
            for(cswp_group_user__c cgu : getCswpGroupIds) {
                cswpGroupIds.add(cgu.CSWP_Group__c);
            }
            List<cswp_group_user__c> cguList = [SELECT Id,Cswp_User__c,CSWP_Group__c,CSWP_Group__r.Name,Cswp_Group_Name__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE CSWP_Group__c IN:cswpGroupIds AND Cswp_Is_Active__c = true ORDER BY Cswp_User_Name__c];
            return cguList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void AddCswpGroupLinkToPublicGroup(String cswpGroupId, String selectedCswpGroups) {
        try {
            List<Object> lst_JsonParse = (List<Object>)Json.deserializeUntyped(selectedCswpGroups);
            List<Cswp_Group_Links__c> list2Insert = new List<Cswp_Group_Links__c>();

            for(Object obj : lst_JsonParse)
            {
                Map<String,Object> mp_StrObj = (Map<string,Object>)obj;
                Id linkedGroupId = ((Id)mp_StrObj.get('Id'));
                Cswp_Group_Links__c newLink = new Cswp_Group_Links__c(
                    CSWP_Master_Group__c = cswpGroupId,
                    CSWP_Linked_Group__c = linkedGroupId
                );
                list2Insert.add(newLink);
            }
            insert list2Insert;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void RemoveCswpGroupLinkFromPublicGroup(String cswpGroupId, String selectedCswpGroups){
        try {
            List<CSWP_Group_Links__c> glList = (List<CSWP_Group_Links__c>) JSON.deserialize(selectedCswpGroups, List<CSWP_Group_Links__c>.class);
            delete glList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @future
    public static void AddUsersToCswpGroupFromFlow(String cswpGroupId, List<String> userIds) {
        Map<Id,Cswp_Group__c> groupMap = CSWPSobjectSelector.getCswpGroupsByIds(new Set<Id>{cswpGroupId});
        List<GroupMember> gmList = new List<GroupMember>();
        for(String userId : userIds) {
            GroupMember gm = new GroupMember();
            gm.GroupId = groupMap.get(cswpGroupId).cswp_PublicGroupId__c;
            gm.UserOrGroupId = userId;
            gmList.add(gm);
        }
        insert gmList;
    }
}