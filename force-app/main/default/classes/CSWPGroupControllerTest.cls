@isTest
public class CSWPGroupControllerTest {
    
    @TestSetup
    static void makeData() {
        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];
        system.runAs(adminUsers[0]) {
            Account acc = new Account(Name='Test Account');
            insert acc;
            User testuser = CSWPTestUtil.createCommunityUser(acc);
            insert testuser;
            cswp_GroupPermission__c groupPermission = CSWPTestUtil.createCswpGroupPermission(true, true, true, true, 'Test', 'Test', true);
            insert groupPermission;
            cswp_Group__c cswpGroup = CSWPTestUtil.createCswpGroup('Test Group', acc, 'Test Description');
            cswpGroup.cswp_GroupPermission__c = groupPermission.Id;
            insert cswpGroup;
            PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
          
            CSWP_Assigned_Permission_Set__c assignedPermSet = CSWPTestUtil.createAssignedPermissionSet('testAssignedPermission',cswpgroup,permSet.Id);
            insert assignedPermSet;
        }        
    }

    @IsTest
    public static void testCswpGroupAddOperation() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];

        String uIdJson =  '[{"Label":"CSWP Portal User","Id":"'+testuser.Id+'"}]';
        Test.startTest();
        CSWPGroupController.AddUsersToPublicGroup(cswpgroup.Id, uIdJson);
        Test.stopTest();
        
        List<cswp_group_user__c> getCswpGroupIds = [SELECT Id, Cswp_User__c,CSWP_Group__c,Cswp_Email__c,Cswp_Username__c,Cswp_Is_Active__c,Cswp_User_Name__c FROM cswp_group_user__c WHERE Cswp_User__c =:testuser.Id];
        System.assert(!getCswpGroupIds.isEmpty());
        System.runAs(testUser){
                List<cswp_Group_User__c> cgu = CSWPGroupController.GetCswpGroupUsers();
            
                System.assert(!cgu.isEmpty());
        }

    }
    @IsTest
    public static void testCswpGroupRemoveOperation() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];

        String uIdJson =  '[{"Label":"CSWP Portal User","Id":"'+testuser.Id+'"}]';

        cswp_group_user__c gu = new cswp_group_user__c();
        gu.CSWP_Group__c = cswpGroup.Id;
        gu.Cswp_User__c = testuser.Id;
        gu.Cswp_Is_Active__c = true;
        insert gu;
        Test.startTest();
        CSWPGroupController.RemoveUsersFromPublicGroup(cswpgroup.Id, uIdJson);
        Test.stopTest();
        
        List<cswp_group_user__c> deletedGUList = [SELECT Id, Cswp_User__c,CSWP_Group__c FROM cswp_group_user__c WHERE Cswp_User__c =:testuser.Id AND CSWP_Group__c =:cswpgroup.Id];

      
        System.assert(deletedGUList.isEmpty());
       
        
    }
    @isTest
    public static void testMakeActiveCSWPGroupUsers(){
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];
        cswp_group_user__c gu = new cswp_group_user__c();
        gu.CSWP_Group__c = cswpGroup.Id;
        gu.Cswp_User__c = testuser.Id;
        gu.Cswp_Is_Active__c = true;
        insert gu;

        Test.startTest();
        String IdJson =  '[{"CSWP_Group__c":"'+cswpgroup.Id+'","Cswp_User__c":"'+testuser.Id+'"}]';
        List<cswp_group_user__c> cguList = CSWPGroupController.MakeActiveCswpGroupUsers( IdJson);
        Test.stopTest();
        System.assert(cguList.get(0).Cswp_Is_Active__c);


    }
    @isTest
    public static void testMakeDectiveCSWPGroupUsers(){
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];
        cswp_group_user__c gu = new cswp_group_user__c();
        gu.CSWP_Group__c = cswpGroup.Id;
        gu.Cswp_User__c = testuser.Id;
        gu.Cswp_Is_Active__c = true;
        insert gu;

        Test.startTest();
        String IdJson =  '[{"Id":"'+gu.Id+'"}]';
        List<cswp_group_user__c> cguList = CSWPGroupController.MakeDeactiveCswpGroupUsers( IdJson);
        Test.stopTest();
        System.assert(!cguList.get(0).Cswp_Is_Active__c);
    }
    
    @IsTest
    public static void testgetAvailableUsersToActivate() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];
        cswp_group_user__c gu = new cswp_group_user__c();
        gu.CSWP_Group__c = cswpGroup.Id;
        gu.Cswp_User__c = testuser.Id;
        gu.Cswp_Is_Active__c = false;
        insert gu;
       
       
        System.runAs(testUser){
                List<cswp_Group_User__c> cgu = CSWPGroupController.getAvailableUsersToActivate();
                System.assert(!cgu.isEmpty());
                System.assert(!cgu.get(0).Cswp_Is_Active__c);
        }
  
    }
    @IsTest
    public static void testgetAvailableUsersToDeactivate() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];
        cswp_group_user__c gu = new cswp_group_user__c();
        gu.CSWP_Group__c = cswpGroup.Id;
        gu.Cswp_User__c = testuser.Id;
        gu.Cswp_Is_Active__c = true;
        insert gu;
       
       
        System.runAs(testUser){
                List<cswp_Group_User__c> cgu = CSWPGroupController.getAvailableUsersToDeactivate();
                System.assert(!cgu.isEmpty());
                System.assert(cgu.get(0).Cswp_Is_Active__c);
        }
  
    }
}