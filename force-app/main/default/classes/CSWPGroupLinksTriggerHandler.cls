public with sharing class CSWPGroupLinksTriggerHandler implements Triggers.Handler, Triggers.AfterInsert, Triggers.AfterDelete {
    
    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }

    public static void AfterInsert(Triggers.Context context) {
        Map<Id,CSWP_Group_Links__c> newMap = (Map<Id,CSWP_Group_Links__c>)context.props.newMap;
        HandleCswpGroupUserCreationForLinks(newMap, 'Insert');
        HandlePublicGroupAssignmentsForLinkAddition(newMap.keySet());
        HandlePermissionSetAssignmentsForLinksAddition(newMap.keySet());
    }

    public static void AfterDelete(Triggers.Context context) {
        Map<Id,CSWP_Group_Links__c> oldMap = (Map<Id,CSWP_Group_Links__c>)context.props.oldMap;
        HandleCswpGroupUserCreationForLinks(oldMap, 'Delete');
        HandlePublicGroupAssignmentsForLinkRemoval(oldMap);
        HandlePermissionSetAssignmentsForLinksRemoval(oldMap);
    }

    private static void HandlePublicGroupAssignmentsForLinkRemoval(Map<Id,CSWP_Group_Links__c> oldMap) {
        Map<Id, Set<Id>> master_linkedGroupMap = new Map<Id, Set<Id>>();
        Set<Id> cswpGroupIds = new Set<Id>();
        Map<Id, Id> pgId_cswpGroupId_Map = new Map<Id, Id>();
        Set<Id> pgMembersToDelete = new Set<Id>();

        for(CSWP_Group_Links__c gl : oldMap.values()) {
            cswpGroupIds.add(gl.CSWP_Master_Group__c);
            cswpGroupIds.add(gl.CSWP_Linked_Group__c);

            if(master_linkedGroupMap.get(gl.CSWP_Master_Group__c) == null) {
                master_linkedGroupMap.put(gl.CSWP_Master_Group__c, new Set<Id>());
            }
            master_linkedGroupMap.get(gl.CSWP_Master_Group__c).add(gl.CSWP_Linked_Group__c);
        }
        Set<Id> masterPgIds = new Set<Id>();
        Set<Id> linkedPgIds = new Set<Id>();
        
        Map<Id,Cswp_Group__c> cswpGroupsMap = CSWPSobjectSelector.getCswpGroupsByIds(cswpGroupIds);

        for(Cswp_Group__c cswpg : cswpGroupsMap.values()) {
            pgId_cswpGroupId_Map.put(cswpg.cswp_PublicGroupId__c, cswpg.Id);
            if(master_linkedGroupMap.containsKey(cswpg.Id)) masterPgIds.add(cswpg.cswp_PublicGroupId__c);
            else linkedPgIds.add(cswpg.cswp_PublicGroupId__c);
        }

        List<GroupMember> pgmList = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN:masterPgIds AND UserOrGroupId IN:linkedPgIds ];
        for(GroupMember gm : pgmList) {            
            if(master_linkedGroupMap.keySet().contains(cswpGroupsMap.get(pgId_cswpGroupId_Map.get(gm.GroupId)).Id)) {
                for(Id linkedGroupId : master_linkedGroupMap.get(cswpGroupsMap.get(pgId_cswpGroupId_Map.get(gm.GroupId)).Id)) {
                    if(gm.UserOrGroupId == cswpGroupsMap.get(linkedGroupId).cswp_PublicGroupId__c) {
                        pgMembersToDelete.add(gm.Id);
                    }
                }
            }
        }
        CSWPGroupLinksTriggerHandler.deletePublicGroupMembersByIdSetFuture(pgMembersToDelete);
    }

    @future
    private static void HandlePublicGroupAssignmentsForLinkAddition(Set<Id> groupLinkIds) {
        List<CSWP_Group_Links__c> groupLinkList = CSWPSobjectSelector.getCswpGroupLinksByIdSet(groupLinkIds);
        List<GroupMember> pgMembers = new List<GroupMember>();        
        Map<Id,Id> cswpGroupPublicGroup_Map = new Map<Id,Id>();
        Set<Id> cswpGroupIds = new Set<Id>();
        for(CSWP_Group_Links__c gl : groupLinkList) {
            cswpGroupIds.add(gl.CSWP_Linked_Group__c);
            cswpGroupIds.add(gl.CSWP_Master_Group__c);
        }

        Map<Id,Cswp_Group__c> cswpGroupsList = CSWPSobjectSelector.getCswpGroupsByIds(cswpGroupIds);
        for(Cswp_Group__c cswpg : cswpGroupsList.values()) {
            cswpGroupPublicGroup_Map.put(cswpg.Id, cswpg.cswp_PublicGroupId__c);
        }
        for(CSWP_Group_Links__c gl : groupLinkList) {
            GroupMember gm = new GroupMember();
            gm.GroupId = cswpGroupPublicGroup_Map.get(gl.CSWP_Master_Group__c);
            gm.UserOrGroupId = cswpGroupPublicGroup_Map.get(gl.CSWP_Linked_Group__c);
            pgMembers.add(gm);
        }
        insert pgMembers; 
    }

    private static void HandleCswpGroupUserCreationForLinks(Map<Id,CSWP_Group_Links__c> triggerMap, String triggerOperation) {
        Set<Id> linkedGroupIds = new Set<Id>();
        for(CSWP_Group_Links__c link : triggerMap.values()) {
            linkedGroupIds.add(link.CSWP_Linked_Group__c);
        }
        List<cswp_group_user__c> cswpGroupUsersToInsert_or_Delete = new List<cswp_group_user__c>();
        Map<String, List<String>> cswpGroup_UsersMap = CSWPSobjectSelector.getUsersOfCswpGroupByIdSet(linkedGroupIds);

        if(triggerOperation == 'Insert') {
            // Create CSWP Group User Records For Added Link
            for(CSWP_Group_Links__c link : triggerMap.values()) {
                for(String userId : cswpGroup_UsersMap.get(link.CSWP_Linked_Group__c)) {
                    cswp_group_user__c gu = new cswp_group_user__c();
                    gu.CSWP_Group__c = link.CSWP_Master_Group__c;
                    gu.Cswp_User__c = userId;
                    gu.Cswp_Is_Active__c = true;
                    cswpGroupUsersToInsert_or_Delete.add(gu);
                }
            }
            //insert cswpGroupUsersToInsert_or_Delete;
        }
        else if(triggerOperation == 'Delete') {
            Set<Id> deletedGroupUserIds = new Set<Id>();
            Set<Id> deletedCswpGroupIds = new Set<Id>();
            for(List<String> userIDs : cswpGroup_UsersMap.values()) {
                deletedGroupUserIds.addAll((Set<Id>)JSON.deserialize(JSON.serialize(userIDs), Set<Id>.class));
            }
            deletedCswpGroupIds = (Set<Id>)JSON.deserialize(JSON.serialize(cswpGroup_UsersMap.keySet()), Set<Id>.class);
            List<cswp_group_user__c> guList = CSWPSobjectSelector.getGroupUsersByCswpGroupAndUserIds(deletedCswpGroupIds, deletedGroupUserIds);
            List<cswp_group_user__c> eliminatedGuList = new List<cswp_group_user__c>();
            for(cswp_group_user__c gu : guList) {

                // if() {

                // }
            }
        }
    }

    @future
    private static void HandlePermissionSetAssignmentsForLinksAddition(Set<Id> groupLinkIds) {
        List<CSWP_Group_Links__c> groupLinkList = CSWPSobjectSelector.getCswpGroupLinksByIdSet(groupLinkIds);
        Set<Id> linkedGroupIds = new Set<Id>();
        Set<Id> masterGroupIds = new Set<Id>();
        for(CSWP_Group_Links__c link : groupLinkList) {
            linkedGroupIds.add(link.CSWP_Linked_Group__c);
            masterGroupIds.add(link.CSWP_Master_Group__c);
        }
        Map<String, List<String>> cswpGroup_UsersMap = CSWPSobjectSelector.getUsersOfCswpGroupByIdSet(linkedGroupIds);

        Map<String, String> resultMap = new Map<String, String>();
        for(String cswpGroupId : cswpGroup_UsersMap.keySet()) {
            Set<Id> userIds = (Set<Id>)JSON.deserialize(JSON.serialize(cswpGroup_UsersMap.get(cswpGroupId)), Set<Id>.class);
            String returnMsg = CSWPGroupController.HandlePermissionSetForCswpGroupUserChange(cswpGroupId, userIds, 'Add');
            resultMap.put(cswpGroupId, returnMsg);
        }
    }

    private static void HandlePermissionSetAssignmentsForLinksRemoval(Map<Id,CSWP_Group_Links__c> oldMap) {
        List<CSWP_Group_Links__c> groupLinkList = oldMap.values();
        Set<Id> linkedGroupIds = new Set<Id>();
        Set<Id> masterGroupIds = new Set<Id>();
        for(CSWP_Group_Links__c link : groupLinkList) {
            linkedGroupIds.add(link.CSWP_Linked_Group__c);
            masterGroupIds.add(link.CSWP_Master_Group__c);
        }
        Map<String, List<String>> cswpGroup_UsersMap = CSWPSobjectSelector.getUsersOfCswpGroupByIdSet(linkedGroupIds);

        Map<String, String> resultMap = new Map<String, String>();
        for(String cswpGroupId : cswpGroup_UsersMap.keySet()) {
            Set<Id> userIds = (Set<Id>)JSON.deserialize(JSON.serialize(cswpGroup_UsersMap.get(cswpGroupId)), Set<Id>.class);
            String returnMsg = CSWPGroupController.HandlePermissionSetForCswpGroupUserChange(cswpGroupId, userIds, 'Remove');
            resultMap.put(cswpGroupId, returnMsg);
        }
    }

    @future
    private static void deletePublicGroupMembersByIdSetFuture(Set<Id> pgMemberIds) {
        delete [SELECT Id from GroupMember Where ID IN:pgMemberIds];
    }
}