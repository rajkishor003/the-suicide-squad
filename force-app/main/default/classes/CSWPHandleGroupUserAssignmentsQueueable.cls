public with sharing class CSWPHandleGroupUserAssignmentsQueueable implements Queueable{
    
    String serializedGUList;

    public CSWPHandleGroupUserAssignmentsQueueable(String serializedGUList) {
        this.serializedGUList = serializedGUList;
    }


    public void execute(QueueableContext qc) {
        List<cswp_group_user__c> guList = (List<cswp_group_user__c>) JSON.deserialize(serializedGUList, List<cswp_group_user__c>.class);
        List<cswp_group_user__c> guList2Upsert = new List<cswp_group_user__c>();
        Set<Id> cswpGroupIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        for(cswp_group_user__c gu : guList){
            cswpGroupIds.add(gu.CSWP_Group__c);
            userIds.add(gu.Cswp_User__c);
        }
        List<Cswp_Group_user__c> existingGuList = CSWPSobjectSelector.getGroupUsersByCswpGroupAndUserIds(cswpGroupIds, userIds);
        Map<String, Cswp_Group_user__c> existingGuMap = new Map<String, Cswp_Group_user__c>();
        
        for(Cswp_Group_user__c gu : existingGuList) {
            existingGuMap.put(String.valueOf(gu.CSWP_Group__c)+String.valueOf(gu.Cswp_User__c), gu);
        }
        
        for(cswp_group_user__c gu : guList) {
            String idComb = String.valueOf(gu.CSWP_Group__c)+String.valueOf(gu.Cswp_User__c);
            if(existingGuMap.get(idComb) != null) {
                existingGuMap.get(idComb).Cswp_Is_Active__c = true;
                guList2Upsert.add(existingGuMap.get(idComb));
            }
            else {
                guList2Upsert.add(gu);
            }
        }    
        upsert guList2Upsert;
    }
}