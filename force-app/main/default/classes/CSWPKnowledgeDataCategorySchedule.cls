public without sharing class CSWPKnowledgeDataCategorySchedule implements Schedulable {

    public void execute(SchedulableContext sc) {
        DateTime fiveMinBefore = DateTime.now().addMinutes(-6);
        Map<Id, Knowledge__kav> knowledgeMap = new Map<Id, Knowledge__kav> ([SELECT Id FROM Knowledge__kav WHERE IsMasterLanguage = true AND CreatedDate >= :fiveMinBefore AND PublishStatus != 'Online']);
        if(!knowledgeMap.isEmpty()) {
            CSWPKnowledgeTriggerHandler.assignDataCategory(knowledgeMap.keySet());
        }
    }
}