/*************************************************************************************************************
 * @name			CSWPKnowledgeTriggerHandler
 * @author			Ozgun Eser <o.eser@emakina.com.tr>
 * @created			11 / 01 / 2021
 * @description		Auto-assign data category according to CSWP_Data_Category__c picklist field.
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
* No.		Date	    Author	    Description
* ----	------------	---------	----------------------------------------------
* 1.0   2021-01-11		Ozgun Eser	Auto-assign data category according to CSWP_Data_Category__c picklist field.
 *
**************************************************************************************************************/
public without sharing class CSWPKnowledgeTriggerHandler implements Triggers.Handler, Triggers.AfterInsert, Triggers.AfterUpdate {
    
    public static Boolean isScheduled = false;

    public Boolean criteria(Triggers.Context context) {
        if(System.isScheduled() || System.isFuture() || isScheduled) {
            return false;
        }
        return true;
    }

    public void afterInsert(Triggers.context context) {
        if(CSWPConfigurationManager.IsTriggerEnabled(CSWPKnowledgeTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT) && !isScheduled) {
            then(context);
        }
    }

    public void afterUpdate(Triggers.context context) {
        if(CSWPConfigurationManager.IsTriggerEnabled(CSWPKnowledgeTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_UPDATE)) {
            then(context);
        }
    }

    private void then(Triggers.context context) {
        if(context.props.isAfter && context.props.isInsert) {
            Set<Id> knowledgeIdSet = context.props.newMap.keySet();
            if(knowledgeIdSet.size() > 2) {
                List<CronTrigger> ctList = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name LIKE 'Assign_Data_Categories_Schedule_Job%' AND NextFireTime > :System.now() AND EndTime = null LIMIT 1];
                if(ctList.isEmpty()) {
                    DateTime nextRunTime = DateTime.now().addMinutes(5);
                    String cronString = nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + nextRunTime.month() + ' ? ' + nextRunTime.year();
                    System.schedule('Assign_Data_Categories_Schedule_Jobs-' + System.now(), cronString, new CSWPKnowledgeDataCategorySchedule());
                    isScheduled = true;
                }
            } else {
                assignDataCategory(knowledgeIdSet);
            }
        } else if(context.props.isAfter && context.props.isUpdate) {
            List<Id> changedKnowledgeIdList = context.props.filterChanged(Knowledge__kav.CSWP_Data_Category__c);
            if(!changedKnowledgeIdList.isEmpty()) {
                removeDataCategory(changedKnowledgeIdList);
                assignDataCategory(new Set<Id> (changedKnowledgeIdList));
            }
        }
    }

    public static void assignDataCategory(Set<Id> knowledgeIdSet) {
        List<Knowledge__kav> knowledgeList = [SELECT Id, CSWP_Data_Category__c, IsMasterLanguage FROM Knowledge__kav WHERE Id IN :knowledgeIdSet AND IsMasterLanguage = true];
        List<Knowledge__DataCategorySelection> selectionList = new List<Knowledge__DataCategorySelection> ();
        for(Knowledge__kav knowledge : knowledgeList) {
            CategoryWrapper categoryWrapper = getCategoryByName(knowledge.CSWP_Data_Category__c);
            if(categoryWrapper != null) {
                selectionList.add(new Knowledge__DataCategorySelection(
                    ParentId = knowledge.Id,
                    DataCategoryGroupName = categoryWrapper.groupName,
                    DataCategoryName = categoryWrapper.name
                ));
            }
        }
        if(!selectionList.isEmpty()) {
            Database.insert(selectionList, false);
        }
    }

    private void removeDataCategory(List<Id> knowledgeIdSet) {
        List<Knowledge__DataCategorySelection> selectionListToDelete = [SELECT Id, ParentId FROM Knowledge__DataCategorySelection WHERE ParentId IN :knowledgeIdSet];
        if(!selectionListToDelete.isEmpty()) {
            delete selectionListToDelete;
        }
    }

    private static CategoryWrapper getCategoryByName(String name) {
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        try {
            describeCategoryResult = Schema.describeDataCategoryGroups(new List<String> {'KnowledgeArticleVersion'});
            List<DataCategoryGroupSobjectTypePair> pairList = new List<DataCategoryGroupSobjectTypePair> ();
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult) {
                DataCategoryGroupSobjectTypePair pair = new DataCategoryGroupSobjectTypePair();
                pair.setSobject(singleResult.getSobject());
                pair.setDataCategoryGroupName(singleResult.getName());
                pairList.add(pair);
            }

            describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairList, false);

            for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult) {
                DataCategory[] topLevelCategories = singleResult.getTopCategories();
                for(DataCategory topLevelCategory : topLevelCategories) {
                    for(DataCategory category : topLevelCategory.getChildCategories()) {
                        if(category.getName() == name) {
                            return new CategoryWrapper(singleResult.getName(), category.getName());
                        }
                    } 
                }
            }
            return null;
        } catch (Exception e) {
            System.debug('Exception is ----> ' + e.getMessage() + '\nStacktrace: ' + e.getStackTraceString());
            return null;
        }
    }

    private class CategoryWrapper {
        public String groupName {get; set;}
        public String name {get; set;}

        public CategoryWrapper(String groupName, String name) {
            this.groupName = groupName;
            this.name = name;
        }
    }
}