@IsTest
public class CSWPKnowledgeTriggerHandlerTest {
    
    @IsTest
    public static void testKnowledgeTrigger() {
        String category1;
        String category2;
        String category1Name;
        String category2Name;
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        describeCategoryResult = Schema.describeDataCategoryGroups(new List<String> {'KnowledgeArticleVersion'});
        List<DataCategoryGroupSobjectTypePair> pairList = new List<DataCategoryGroupSobjectTypePair> ();
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult) {
            DataCategoryGroupSobjectTypePair pair = new DataCategoryGroupSobjectTypePair();
            pair.setSobject(singleResult.getSobject());
            pair.setDataCategoryGroupName(singleResult.getName());
            pairList.add(pair);
        }

        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairList, false);

        for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult) {
            DataCategory[] topLevelCategories = singleResult.getTopCategories();
            for(DataCategory topLevelCategory : topLevelCategories) {
                for(DataCategory category : topLevelCategory.getChildCategories()) {
                    if(String.isBlank(category1)) {
                        category1 = category.getName();
                        category1Name = category.getName();
                        continue;
                    } else if(String.isBlank(category2)) {
                        category2 = category.getName();
                        category2Name = category.getName();
                        break;
                    } else {
                        break;
                    }
                } 
            }
        }

        User knowledgeUser = [SELECT Id FROM User WHERE UserPermissionsKnowledgeUser=true AND IsActive = true LIMIT 1];
        System.runAs(knowledgeUser){
            Knowledge__kav knowledge = CSWPTestUtil.createFAQKnowledge('Test-Title', 'Test Question', 'Test Answer', category1, 'en_US');
            insert knowledge;

            List<Knowledge__DataCategorySelection> selectionList = [SELECT Id, ParentId, DataCategoryName FROM Knowledge__DataCategorySelection WHERE ParentId = :knowledge.Id];
            System.assertEquals(1, selectionList.size());
            System.assertEquals(category1Name, selectionList[0].DataCategoryName);

            knowledge.CSWP_Data_Category__c = category2;
            update knowledge;
            selectionList = [SELECT Id, ParentId, DataCategoryName FROM Knowledge__DataCategorySelection WHERE ParentId = :knowledge.Id];
            System.assertEquals(1, selectionList.size());
            System.assertEquals(category2Name, selectionList[0].DataCategoryName);

            Test.startTest();
            Knowledge__kav knowledge2 = CSWPTestUtil.createFAQKnowledge('Test-Title2', 'Test Question2', 'Test Answer2', category1, 'en_US');
            Knowledge__kav knowledge3 = CSWPTestUtil.createFAQKnowledge('Test-Title3', 'Test Question3', 'Test Answer3', category1, 'en_US');
            Knowledge__kav knowledge4 = CSWPTestUtil.createFAQKnowledge('Test-Title4', 'Test Question4', 'Test Answer4', category1, 'en_US');
            insert new List<Knowledge__kav> {knowledge2, knowledge3, knowledge4};
            Test.stopTest();
        }
    }
}