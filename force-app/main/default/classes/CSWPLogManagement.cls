/*************************************************************************************************************
 * @name			CSWPLogManagement.cls
 * @author			emre akkus <e.akkus@emakina.com.tr>
 * @created			06 / 01 / 2021
 * @description		CSWPLogManagement class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2021-01-06		emreakkus				Initial Version.
**************************************************************************************************************/
public with sharing class CSWPLogManagement {
    public static void createLogForCSWP(String reason, String source, Boolean sendEmail, String type, String recordKey) {
        Cswp_Log__c log        = new Cswp_Log__c();
        log.Cswp_Reason__c     = reason;
        log.Cswp_Source__c     = source;
        log.Cswp_Send_Email__c = sendEmail;
        log.Cswp_Type__c       = type;
        log.Cswp_Record_Key__c = recordKey;
        insert log;
    }
}