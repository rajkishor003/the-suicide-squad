global class CSWPMultiSelectFlowValues {

    public class FlowInputs {
        @InvocableVariable public String selectedChoices;
        @InvocableVariable public String cswpGroupId;
    }

    @InvocableMethod 
    public static List<list<string>> CheckValues(List<FlowInputs> request) {
       if(!(request.isEmpty()) && request[0].selectedChoices != ''){ 
           String tempStr = request[0].selectedChoices;
           String cswpGroupId  = request[0].cswpGroupId;
           List<String> lstnew = tempStr.split(';'); 
           Id idToProccess = lstnew[0];
           Schema.sObjectType entityType = idToProccess.getSObjectType();
           if(entityType == User.sObjectType) {
                CSWPGroupController.AddUsersToCswpGroupFromFlow(cswpGroupId, lstnew);
           }
           list<list<string>> finalLst = new list<list<string>>(); 
           finalLst.add(lstnew); 
           return finalLst; 
       } 
       else return null; 
    }
 }