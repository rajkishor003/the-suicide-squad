@isTest
public class CSWPMultiSelectFlowValues_Test{
    @isTest
    static void CSWPMultiSelectFlowValuesTest(){

        Account acc = CSWPTestUtil.createAccount();
        insert acc;

        User testUser = CSWPTestUtil.createCommunityUser(acc);
        insert testUser;

        cswp_Group__c cswpgroup = CSWPTestUtil.createCswpGroup('test group',acc,'test group');

        Database.SaveResult svr = database.insert(cswpgroup , false);

        List<string> tempLst = new list<string>();
        tempLst.add('Emre');
        tempLst.add('Salesforce');
        CSWPMultiSelectFlowValues.FlowInputs fl = new CSWPMultiSelectFlowValues.FlowInputs();
        fl.selectedChoices = testUser.Id;
        fl.cswpGroupId = cswpgroup.Id;
        List<CSWPMultiSelectFlowValues.FlowInputs> fList = new List<CSWPMultiSelectFlowValues.FlowInputs>();
        fList.add(fl);
        CSWPMultiselectFlowvalues.CheckValues(fList);
    }
}