global class CSWPNotificationBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, Name, cswp_Content_Message__c,cswp_Is_Active__c,cswp_End_Date__c,CSWP_Group__r.Id,cswp_Start_Date__c,CSWP_Group__c,CSWP_Image_URL__c, (SELECT Id,Name,cswp_Language__c,cswp_Content_Message__c,cswp_Notification__c FROM cswp_Notification_Translations__r) FROM cswp_Notification__c WHERE cswp_Start_Date__c = TODAY AND cswp_Is_Active__c = true AND cswp_Send_Email__c= true';
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<cswp_Notification__c> scope)
    {   
        Boolean languageFlag = false;
        Set<Id> userIDs = new Set<Id>();
        Map<cswp_Notification__c,List<User>> mapNotificationToUsers = new  Map<cswp_Notification__c,List<User>>();
        for(Cswp_Notification__c n : scope) {
           
            List<User> users =  CSWPSobjectSelector.getUsersOfCswpGroup(n.CSWP_Group__r.Id);
            
            for(User user: users){
                userIDs.add(user.Id);
            }
            mapNotificationToUsers.put(n,users );
        }



        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage>();
        for(Cswp_Notification__c n : scope) {
            if(mapNotificationToUsers.get(n) == null) continue;
            List<User> users = mapNotificationToUsers.get(n);
            for(User u: users){
                
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {u.Email };
             
               
                if(!u.LanguageLocaleKey.contains('en')){
                    if(!n.cswp_Notification_Translations__r.isEmpty()){
                        for(cswp_Notification_Translation__c notificationTranslation:n.cswp_Notification_Translations__r){
                            if(notificationTranslation.cswp_Language__c.contains(u.LanguageLocaleKey)){
                                message.setHtmlBody (notificationTranslation.cswp_Content_Message__c) ;
                                message.subject = notificationTranslation.Name;
                                languageFlag=true;
                            }
                        }
                    }
                }
             
                if(!languageFlag){
                    message.subject = n.Name;
                    message.setHtmlBody (n.cswp_Content_Message__c) ;
                }  
               
                messages.add(message);
            }
           
        }
        sendEmails(messages);
     
     
    }

    global void finish(Database.BatchableContext BC)
    {


    }
    public static void sendEmails(  Messaging.SingleEmailMessage[] messages) {
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
}