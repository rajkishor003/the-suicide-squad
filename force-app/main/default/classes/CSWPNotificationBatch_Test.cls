@isTest
public class CSWPNotificationBatch_Test {
    @TestSetup
    static void makeData(){
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
     

        cswp_Group__c cswpGroup;
        
        System.runAs(adminUsers[0]){
            Account acc = new Account(Name='Test Account');
            insert acc;
            User testuser = CSWPTestUtil.createCommunityUser(acc);
            testuser.Email = 'e.turhan@emakina.com.tr';
            insert testuser;
            User testuser2 = CSWPTestUtil.createCommunityUser(acc);
            testuser2.Email = 'e.turhan@emakina.com.tr';
            testuser2.LanguageLocaleKey = 'ja';
            testUser2.Username = 'test1@uniquedomain.com';
            testUser2.CommunityNickname = 'test2User123';
            insert testuser2;
            cswp_GroupPermission__c groupPermission = CSWPTestUtil.createCswpGroupPermission(true, true, true, true, 'Test', 'Test', true);
            insert groupPermission;
            PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            cswpGroup = CSWPTestUtil.createCswpGroup('Test Group', acc, 'Test Description');
            cswpGroup.cswp_GroupPermission__c = groupPermission.Id;
            

            

            insert cswpGroup;
            CSWP_Assigned_Permission_Set__c assignedPermSet = CSWPTestUtil.createAssignedPermissionSet('testAssignedPermission',cswpgroup,permSet.Id);
            insert assignedPermSet;
            
            String uIdJson =  '[{"Label":"CSWP Portal User","Id":"'+testuser.Id+'"},{"Label":"CSWP Portal User","Id":"'+testuser2.Id+'"}]';
            CSWPGroupController.AddUsersToPublicGroup(cswpgroup.Id, uIdJson);
        }
        CSWP_Notification__c systemN = CSWPTestUtil.createNotificationWithGroup('System notification'
                                                                                ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                                , system.today()
                                                                                , system.today()+20
                                                                                , cswpGroup
                                                                                , true
                                                                                , false
                                                                                , false
                                                                                , true
                                                                                , 'CSWP_System_Notification');

        insert systemN;                                                                   
        CSWP_Notification_Translation__c systemT =CSWPTestUtil.createNotificationTranslation('ja'
                                                                                                , 'System Notification JP'
                                                                                                , 'JP Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                                                , systemN);
        insert systemT;  
 
    }
    @isTest
    private static void sendsEmailOnFinish() { 
        Test.startTest();   
        CSWPNotificationBatch b = new CSWPNotificationBatch();
        ID jobId =Database.executeBatch(b,1);
                 
        Test.stopTest();
        Integer Afterinvocations = Limits.getEmailInvocations();
         // Get the information from the CronTrigger API object
         AsyncApexJob jobInfo = [SELECT Status, NumberOfErrors
         FROM AsyncApexJob WHERE Id = :jobID];
        
        // Assert emails have been sent
       
        System.assertEquals('Completed', jobInfo.Status);
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('CSWPNotificationBatch', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');
    
    
    }
    @isTest
    private static void sendsEmail() { 
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] {'e.turhan@emakina.com.tr' };
        message.setHtmlBody ('Test body') ;
        message.subject = 'Test subject';
        messages.add(message);
        CSWPNotificationBatch.sendEmails(messages);
        
        Integer invocations = Limits.getEmailInvocations();    
        System.assertEquals(1, invocations);
    
    
    }
}