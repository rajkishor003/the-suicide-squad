public with sharing class CSWPNotificationController {
    @AuraEnabled (cacheable = true)
    public static NotificationWrapper getNotifacationForLandingPage(){
        String userLanguage = UserInfo.getLanguage();
        cswp_Notification__c notification = CSWPSobjectSelector.getLandinPageNotification().get(0);
 
        List<cswp_Notification_Translation__c> notificationTranslations = CSWPSobjectSelector.getCswpNotificationTranslationsByIdAndLanguage(notification.Id, userLanguage);
        if(notificationTranslations.size()==0){
            return new NotificationWrapper(notification.Name
                                            , notification.cswp_Content_Message__c
                                            , notification.cswp_Start_Date__c.format()
                                            , notification.id
                                            , '/cswp/s/cswp-notification-detail?recordId='+notification.id
                                            , notification.CSWP_Image_URL__c);

        }
        cswp_Notification_Translation__c notificationTranslation = notificationTranslations.get(0);
        return new NotificationWrapper(notificationTranslation.Name
                                        , notificationTranslation.cswp_Content_Message__c 
                                        , notificationTranslation.cswp_Notification__r.cswp_Start_Date__c.format()
                                        , notification.id
                                        , '/cswp/s/cswp-notification-detail?recordId='+notification.id
                                        , notification.CSWP_Image_URL__c); 

    }

    @AuraEnabled (cacheable = true)
    public static List<NotificationWrapper> getUserNotifacations(){
        String userLanguage = UserInfo.getLanguage();
        ID loggedinUserID = UserInfo.getUserId();
        List<PermissionSetAssignment> permission = CSWPSobjectSelector.getUserPermissionSetAssignmentByUserId(loggedinUserID);
        Boolean hasPermission =  !permission.isEmpty();
        System.debug('Permission Check ==> ' + hasPermission);
        if(!hasPermission){
            return null;
        }

        

        List<Group> gList = CSWPSobjectSelector.getPublicGroupsOfUser(loggedinUserID);
        Set<Id> groupIds = new Set<Id>();
        for(Group g : gList) { groupIds.add(g.Id); }
        
        List<Cswp_Group__c> cswpGroupList = CSWPSobjectSelector.getCswpGroupsOfPublicGroups(groupIds);
        Set<Id> cswp_groupIds = new Set<Id>();
        for(Cswp_Group__c cswp_g : cswpGroupList) { cswp_groupIds.add(cswp_g.Id); }

        List<CSWP_Confirmed_Notification__c> cnList = CSWPSobjectSelector.getCswpConfirmedNotificationsWithUserId(loggedinUserID);
        Set<Id> confirmedNIds = new Set<Id>();
        for(CSWP_Confirmed_Notification__c c : cnList) { confirmedNIds.add(c.cswp_Notification__r.Id); }

        List<cswp_Notification__c> notificationList  = CSWPSobjectSelector.getCswpNotificationsWithGroupIdSetAndNotConfirmedIdSet( cswp_groupIds, confirmedNIds, userLanguage);
        notificationList.addAll(CSWPSobjectSelector.getCswpNotificationsWithoutGroupAndNotConfirmedNIdSet( confirmedNIds, userLanguage));
      
        Set<Id> notificationIds = new Set<Id>();
        for(cswp_Notification__c n : notificationList) { notificationIds.add(n.Id); }
        List<NotificationWrapper> notificationWrapperList = new List<NotificationWrapper> ();
        if(userLanguage.contains('en')){
            for(cswp_Notification__c notification: notificationList){
                notificationWrapperList.add(new NotificationWrapper(notification.Name
                                                                    , notification.cswp_Content_Message__c
                                                                    , notification.cswp_Start_Date__c.format()
                                                                    , notification.id
                                                                    , '/cswp/s/cswp-notification-detail?recordId='+notification.id
                                                                    , notification.CSWP_Image_URL__c) );
            }
        }
        else{
          
            for(cswp_Notification__c notification: notificationList){
                if(notification.cswp_Notification_Translations__r.isEmpty()){
                    notificationWrapperList.add(new NotificationWrapper(notification.Name
                    , notification.cswp_Content_Message__c
                    , notification.cswp_Start_Date__c.format()
                    , notification.id
                    , '/cswp/s/cswp-notification-detail?recordId='+notification.id
                    , notification.CSWP_Image_URL__c) );
                }
                else{
                    for(cswp_Notification_Translation__c notificationTranslation:notification.cswp_Notification_Translations__r){
                        notificationWrapperList.add(new NotificationWrapper(notificationTranslation.Name
                                                , notificationTranslation.cswp_Content_Message__c
                                                , notification.cswp_Start_Date__c.format()
                                                , notification.id
                                                ,'/cswp/s/cswp-notification-detail?recordId='+notification.id
                                                , notification.CSWP_Image_URL__c));
                    }
                }
               
            }
       
        }
        return notificationWrapperList;
    }
    @AuraEnabled (cacheable = true)
    public static List<NotificationWrapper> getUserSystemNotifacations(){
        String userLanguage = UserInfo.getLanguage();

        ID loggedinUserID = UserInfo.getUserId();
        List<PermissionSetAssignment> permission = CSWPSobjectSelector.getUserPermissionSetAssignmentByUserId(loggedinUserID);
        Boolean hasPermission =  !permission.isEmpty();
        if(!hasPermission){
            return null;
        }
        List<Group> gList = CSWPSobjectSelector.getPublicGroupsOfUser(loggedinUserID);
        Set<Id> groupIds = new Set<Id>();
        for(Group g : gList) { groupIds.add(g.Id); }

        List<Cswp_Group__c> cswpGroupList = CSWPSobjectSelector.getCswpGroupsOfPublicGroups(groupIds);
        Set<Id> cswp_groupIds = new Set<Id>();
        for(Cswp_Group__c cswp_g : cswpGroupList) { cswp_groupIds.add(cswp_g.Id); }

        List<CSWP_Confirmed_Notification__c> cnList = CSWPSobjectSelector.getCswpConfirmedNotificationsWithUserId(loggedinUserID);
        Set<Id> confirmedNIds = new Set<Id>();
        for(CSWP_Confirmed_Notification__c c : cnList) { confirmedNIds.add(c.cswp_Notification__r.Id); }

        List<cswp_Notification__c> notificationList  =CSWPSobjectSelector.getCswpNotificationsWithGroupIdSetAndNotConfirmedIdSetAndRecordType(cswp_groupIds, confirmedNIds,  userLanguage,'CSWP_System_Notification');
        notificationList.addAll(CSWPSobjectSelector.getCswpNotificationsWithoutGroupAndNotConfirmedIdSetAndRecordType( confirmedNIds, userLanguage, 'CSWP_System_Notification'));

        Set<Id> notificationIds = new Set<Id>();
        for(cswp_Notification__c n : notificationList) { notificationIds.add(n.Id); }
        List<NotificationWrapper> notificationWrapperList = new List<NotificationWrapper> ();
        if(userLanguage.contains('en')){
            for(cswp_Notification__c notification: notificationList){
                notificationWrapperList.add(new NotificationWrapper(notification.Name
                                                                    , notification.cswp_Content_Message__c
                                                                    , notification.cswp_Start_Date__c.format()
                                                                    , notification.id
                                                                    , '/cswp/s/cswp-notification-detail?recordId='+notification.id
                                                                    , notification.CSWP_Image_URL__c) );
            }
        }
        else{
          
            for(cswp_Notification__c notification: notificationList){
                if(notification.cswp_Notification_Translations__r.isEmpty()){
                    notificationWrapperList.add(new NotificationWrapper(notification.Name
                    , notification.cswp_Content_Message__c
                    , notification.cswp_Start_Date__c.format()
                    , notification.id
                    , '/cswp/s/cswp-notification-detail?recordId='+notification.id
                    , notification.CSWP_Image_URL__c) );
                }
                else{
                    for(cswp_Notification_Translation__c notificationTranslation : notification.cswp_Notification_Translations__r){
                        notificationWrapperList.add(new NotificationWrapper(notificationTranslation.Name
                                                , notificationTranslation.cswp_Content_Message__c
                                                , notification.cswp_Start_Date__c.format() 
                                                , notification.id
                                                ,'/cswp/s/cswp-notification-detail?recordId='+notification.id
                                                , notification.CSWP_Image_URL__c));
                    }
                }
               
            }
       
        }
        return notificationWrapperList;
    }
    
    public class NotificationWrapper {
        @AuraEnabled
        public String notificationName {get;set;}
        @AuraEnabled
        public String notificationContent {get;set;}
        @AuraEnabled
        public String notificationStartDate {get;set;}
        @AuraEnabled
        public String notificationImageURL {get;set;}
        @AuraEnabled
        public String recordURL {get;set;}
        @AuraEnabled
        public String id {get;set;}
       

        public NotificationWrapper(String notificationName
                                , String notificationContent
                                , String notificationStartDate
                                , String id
                                , String recordURL
                                , String notificationImageURL) {
            this.id = id;
            this.notificationName = notificationName;
            this.notificationContent = notificationContent;
            this.notificationStartDate = notificationStartDate;
            this.recordURL = recordURL;
            this.notificationImageURL = notificationImageURL;
                                    
        }
    }
}