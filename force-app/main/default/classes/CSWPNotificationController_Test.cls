@IsTest
public class CSWPNotificationController_Test {
    @TestSetup
    static void makeData(){
        List<cswp_Notification__c> notifications = new List<cswp_Notification__c>();
        List<cswp_Notification_Translation__c> translations = new List<cswp_Notification_Translation__c>();
        Account acc = new Account(Name='Test Account');
        insert acc;
        User testUser = CSWPTestUtil.createCommunityUser(acc);
       
        insert testUser;
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        
        System.runAs(adminUsers[0]){
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            insert new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = ps.Id);
        }
        CSWP_Notification__C landingPage = CSWPTestUtil.createNotificationWithoutGroup('Landing Page'
                                                                            ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                            , system.today()
                                                                            , system.today()+20
                                                                            , true
                                                                            , true
                                                                            , false
                                                                            , false
                                                                            , 'CSWP_Service_Notification');
        insert landingPage;
        CSWP_Notification_Translation__C landingPageTranslation = CSWPTestUtil.createNotificationTranslation('ja'
                                                                    , 'Landing Page JP'
                                                                    , 'JP Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                    , landingPage);
        insert landingPageTranslation;
     
       CSWP_Notification__c systemN = CSWPTestUtil.createNotificationWithoutGroup('System notification'
                                                                            ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                            , system.today()
                                                                            , system.today()+20
                                                                            , true
                                                                            , false
                                                                            , false
                                                                            , false
                                                                            , 'CSWP_System_Notification');
      
        insert systemN;                                                                 
        CSWP_Notification_Translation__c systemT =CSWPTestUtil.createNotificationTranslation('ja'
                                                                    , 'System Notification JP'
                                                                    , 'JP Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                    , systemN);
        insert systemT;                       
        CSWP_Notification__c serviceN = CSWPTestUtil.createNotificationWithoutGroup('Service notification'
                                                                    ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                    , system.today()
                                                                    , system.today()+20
                                                                    , true
                                                                    , false
                                                                    , false
                                                                    , false
                                                                    , 'CSWP_Service_Notification');
        insert serviceN;
                                                                    
       
  
    }
    @IsTest
    static void testgetLandignPageMessageWithUserLanguageEng(){
        List<User> users = [Select Id from User LIMIT 1];
        User testUser= users.get(0);
        CSWPNotificationController.NotificationWrapper wrapper;
        System.runAs(testuser){
           
            wrapper = CSWPNotificationController.getNotifacationForLandingPage();
        }
       
        System.assert(wrapper.notificationContent.contains('Nam vel lectus tortor.'));
        System.assertEquals(wrapper.notificationName, 'Landing Page');
        
    }
    @IsTest
    static void testGetUserNotifications(){
        List<User> users = [Select Id from User LIMIT 1];
        User testUser= users.get(0);
        List<CSWPNotificationController.NotificationWrapper> usersSystemNotifications= new List<CSWPNotificationController.NotificationWrapper>();

        List<CSWPNotificationController.NotificationWrapper> usersNotifications= new List<CSWPNotificationController.NotificationWrapper>();
        System.runAs(testuser){
           
            usersNotifications = CSWPNotificationController.getUserNotifacations();
            usersSystemNotifications = CSWPNotificationController.getUserSystemNotifacations();
        }
       
        System.assertNotEquals(0,usersNotifications.size());
        System.assertNotEquals(0,usersSystemNotifications.size());
       
   
        
    }
    @IsTest
    static void testGetUserNotificationsWithDifferentLanguage(){
        Account acc = new Account(Name='Test Account1');
        insert acc;
        User testUser = CSWPTestUtil.createCommunityUser(acc);
        testUser.Username = 'tes1t@uniquedomain.com';
        testUser.CommunityNickname = 'tes2User123';
        testUser.LanguageLocaleKey = 'ja';
        insert testUser;
        List<CSWPNotificationController.NotificationWrapper> usersSystemNotifications= new List<CSWPNotificationController.NotificationWrapper>();

        List<CSWPNotificationController.NotificationWrapper> usersNotifications= new List<CSWPNotificationController.NotificationWrapper>();
        System.runAs(testuser){
           
            usersNotifications = CSWPNotificationController.getUserNotifacations();
            usersSystemNotifications = CSWPNotificationController.getUserSystemNotifacations();
        }
      
        System.assertNotEquals(0,usersNotifications.size());
        System.assertNotEquals(0,usersSystemNotifications.size());
       
   
        
    }
    @IsTest
    static void testgetLandignPageMessageWithUserLanguageDifferent(){
        Account acc = new Account(Name='Test Account1');
        insert acc;
        User testUser = CSWPTestUtil.createCommunityUser(acc);
        testUser.Username = 'tes1t@uniquedomain.com';
        testUser.CommunityNickname = 'tes2User123';
        testUser.LanguageLocaleKey = 'ja';
        insert testUser;
      
       
        CSWPNotificationController.NotificationWrapper wrapper;
        System.runAs(testuser){
           
            wrapper = CSWPNotificationController.getNotifacationForLandingPage();
        }
      
        System.assert(wrapper.notificationContent.contains('JP'));
        System.assertEquals(wrapper.notificationName, 'Landing Page JP');
        
    }
}