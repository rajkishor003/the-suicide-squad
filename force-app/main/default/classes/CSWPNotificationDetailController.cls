public with sharing class CSWPNotificationDetailController {
    @AuraEnabled (cacheable=true)
    public static NotificationWrapper getNotificationDetail(String notificationId) {
        cswp_Notification__c notification = CSWPSobjectSelector.getCswpNotificationsByIdSet(new Set<Id> {notificationId})[0];
        String userLanguage = UserInfo.getLanguage();
        if(!userLanguage.contains('en')){
            List<CSWP_Notification_Translation__c>  nTranslationList = CSWPSobjectSelector.getCswpNotificationTranslationsByIdSetAndLanguage(new Set<Id> {notificationId},userLanguage);
            if(!nTranslationList.isEmpty()){
                CSWP_Notification_Translation__c notificationTranslation = nTranslationList.get(0);
                return new NotificationWrapper(notificationTranslation.Name
                        , notificationTranslation.cswp_Content_Message__c
                        , notificationTranslation.CSWP_Notification__r.cswp_Start_Date__c.format()
                        , notificationTranslation.CSWP_Notification__r.cswp_User_must_confirm__c);
            }
        }
        
        return new NotificationWrapper(notification.Name
                                        , notification.cswp_Content_Message__c
                                        , notification.cswp_Start_Date__c.format()
                                        , notification.cswp_User_must_confirm__c);
    }
    public class NotificationWrapper {
        @AuraEnabled
        public String notificationName {get;set;}
        @AuraEnabled
        public String notificationContent {get;set;}
        @AuraEnabled
        public String notificationStartDate {get;set;}
        @AuraEnabled
        public Boolean notificationUserMustConfirm {get;set;}
       

        public NotificationWrapper(String notificationName
                                    , String notificationContent
                                    , String notificationStartDate
                                    , Boolean notificationUserMustConfirm) {
         
            this.notificationName = notificationName;
            this.notificationContent = notificationContent;
            this.notificationStartDate = notificationStartDate;
            this.notificationUserMustConfirm = notificationUserMustConfirm;
        }
    }

 
}