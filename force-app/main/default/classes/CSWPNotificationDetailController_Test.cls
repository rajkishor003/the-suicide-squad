@IsTest
public  class CSWPNotificationDetailController_Test {
    @TestSetup
    public static void makeData(){
        Account acc = new Account(Name='Test Account');
        insert acc;
        User testUser = CSWPTestUtil.createCommunityUser(acc);
       
        insert testUser;
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        
        System.runAs(adminUsers[0]){
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            insert new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = ps.Id);
        }
       
        CSWP_Notification__c notification = CSWPTestUtil.createNotificationWithoutGroup('Service notification'
                                                                    ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                    , system.today()
                                                                    , system.today()+20
                                                                    , true
                                                                    , false
                                                                    , false
                                                                    , false
                                                                    , 'CSWP_Service_Notification');
        insert notification;
        CSWP_Notification_Translation__C nTranslation= CSWPTestUtil.createNotificationTranslation('ja'
                                                                                                    , 'Service notification JP'
                                                                                                    , 'JP Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                                                    , notification);
        insert nTranslation;

    }
    @IsTest
    public static void testGetNotificationDetail() {
        List<User> users = [Select Id from User LIMIT 1];
        User testUser= users.get(0);
        
        CSWP_Notification__c notification = [SELECT Id FROM CSWP_Notification__c LIMIT 1];
        CSWPNotificationDetailController.NotificationWrapper wrapper;
        System.runAs(testuser){
           
            wrapper = CSWPNotificationDetailController.getNotificationDetail(notification.Id);
        }
        
        System.assert(wrapper.notificationContent.contains('Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'));
        System.assertEquals(wrapper.notificationName, 'Service notification');
    }
    @IsTest
    public static void testGetNotificationTranslationDetail() {
        Account acc = new Account(Name='Test Account1');
        insert acc;
        User testUser = CSWPTestUtil.createCommunityUser(acc);
        testUser.Username = 'tes1t@uniquedomain.com';
        testUser.CommunityNickname = 'tes2User123';
        testUser.LanguageLocaleKey = 'ja';
        insert testUser;
      
        CSWP_Notification__c notification = [SELECT Id FROM CSWP_Notification__c LIMIT 1];
        CSWPNotificationDetailController.NotificationWrapper wrapper;
        System.runAs(testuser){
           
            wrapper = CSWPNotificationDetailController.getNotificationDetail(notification.Id);
        }
       
        System.assert(wrapper.notificationContent.contains('JP'));
        System.assertEquals(wrapper.notificationName, 'Service notification JP');
    }

    
}