global class CSWPNotificationSchedule implements Schedulable{

    //public static String sched = '0 00 00 * * ?';  //Every Day at Midnight

    global void execute(SchedulableContext sc) {
        CSWPNotificationBatch b1 = new CSWPNotificationBatch();
        ID batchprocessid = Database.executeBatch(b1,200);
    }
}