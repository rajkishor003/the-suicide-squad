@isTest
public class CSWPNotificationSchedule_Test {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @TestSetup
    static void makeData(){
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
     

        cswp_Group__c cswpGroup;
        
        System.runAs(adminUsers[0]){
            Account acc = new Account(Name='Test Account');
            insert acc;
            User testuser = CSWPTestUtil.createCommunityUser(acc);
            testuser.Email = 'e.turhan@emakina.com.tr';
            insert testuser;
            User testuser2 = CSWPTestUtil.createCommunityUser(acc);
            testuser2.Email = 'e.turhan@emakina.com.tr';
            testuser2.LanguageLocaleKey = 'ja';
            testUser2.Username = 'test1@uniquedomain.com';
            testUser2.CommunityNickname = 'test2User123';
            insert testuser2;
            cswp_GroupPermission__c groupPermission = CSWPTestUtil.createCswpGroupPermission(true, true, true, true, 'Test', 'Test', true);
            insert groupPermission;
            PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
            cswpGroup = CSWPTestUtil.createCswpGroup('Test Group', acc, 'Test Description');
            cswpGroup.cswp_GroupPermission__c = groupPermission.Id;
            

            

            insert cswpGroup;
            CSWP_Assigned_Permission_Set__c assignedPermSet = CSWPTestUtil.createAssignedPermissionSet('testAssignedPermission',cswpgroup,permSet.Id);
            insert assignedPermSet;
            
            String uIdJson =  '[{"Label":"CSWP Portal User","Id":"'+testuser.Id+'"},{"Label":"CSWP Portal User","Id":"'+testuser2.Id+'"}]';
            CSWPGroupController.AddUsersToPublicGroup(cswpgroup.Id, uIdJson);
        }
        CSWP_Notification__c systemN = CSWPTestUtil.createNotificationWithGroup('System notification'
                                                                                ,'Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                                                                , system.today()
                                                                                , system.today()+20
                                                                                , cswpGroup
                                                                                , true
                                                                                , false
                                                                                , false
                                                                                , true
                                                                                , 'CSWP_System_Notification');

        insert systemN;                                                                   
        CSWP_Notification_Translation__c systemT =CSWPTestUtil.createNotificationTranslation('ja'
                                , 'System Notification JP'
                                , 'JP Nam vel lectus tortor. Ut lectus tortor, lobortis eget.'
                                , systemN);
        insert systemT;  
    }
    @isTest
    public static void testSchedule(){
        List<AsyncApexJob> jobsBefore = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where  ApexClass.Name = 'CSWPNotificationSchedule'];
        System.assertEquals(0, jobsBefore.size(), 'not expecting any asyncjobs');
    
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP,
            new CSWPNotificationSchedule());
        // Verify the scheduled job has not run yet.
       
        // Stopping the test will run the job synchronously
        Test.stopTest();
        // Now that the scheduled job has executed,
        // check that our tasks were created
        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('CSWPNotificationSchedule', jobsScheduled[0].ApexClass.Name, 'expecting specific scheduled job');

        // check apex batch is in the job list
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('CSWPNotificationBatch', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');

    
    }
}