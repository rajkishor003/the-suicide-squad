public with sharing class CSWPProgressBarController {
    @AuraEnabled (cacheable = true)
    public static StorageWrapper getStorageInfo() {
        Decimal currentStorage = 0.00;
        Decimal storageLimit = 0.00;
        Decimal progress = 0.00;
        try{
            User currentUser = [SELECT Id, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];
            List<cswp_AccountPreference__c> accountPreferenceList = [SELECT Id, cswp_StorageLimit__c, cswp_Current_Storage__c FROM cswp_AccountPreference__c WHERE cswp_Account__c = :currentUser.Contact.AccountId AND cswp_Active__c = true ORDER BY LastModifiedDate DESC LIMIT 1];
            if(!accountPreferenceList.isEmpty()) {
                currentStorage = accountPreferenceList[0].cswp_Current_Storage__c == null ? 0 : accountPreferenceList[0].cswp_Current_Storage__c;
                storageLimit = accountPreferenceList[0].cswp_StorageLimit__c == null ? 0 : accountPreferenceList[0].cswp_StorageLimit__c;
                progress = currentStorage * 100 / storageLimit;
            }
            return new StorageWrapper(currentStorage, storageLimit, progress);
        } catch (Exception e) {
            System.debug('Exception is ----> ' + e.getMessage() + ' in line number ' + e.getLineNumber());
            return new StorageWrapper(0, 0, 0.00);
        }
    }

    public class StorageWrapper {
        @AuraEnabled
        public Decimal currentStorage {get;set;}
        @AuraEnabled
        public Decimal storageLimit {get;set;}
        @AuraEnabled
        public Decimal progress {get;set;}

        public StorageWrapper(Decimal currentStorage, Decimal storageLimit, Decimal progress) {
            this.currentStorage = currentStorage;
            this.storageLimit = storageLimit;
            this.progress = progress;
        }
    }
}