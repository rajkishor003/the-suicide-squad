@IsTest
public class CSWPProgressBarControllerTest {
    
    @IsTest
    public static void testGetStorageInfo() {
        Account account = CSWPTestUtil.createAccount();
        insert account;

        User user = CSWPTestUtil.createCommunityUser(account);
        insert user;

        cswp_AccountPreference__c accountPreference = CSWPTestUtil.createAccountPreference(account);
        accountPreference.cswp_Current_Storage__c = 100;
        accountPreference.cswp_StorageLimit__c = 200;
        insert accountPreference;

        System.runAs(user) {
            CSWPProgressBarController.StorageWrapper storage = CSWPProgressBarController.getStorageInfo();
            System.assertEquals(storage.currentStorage, 100);
            System.assertEquals(storage.storageLimit, 200);
            System.assertEquals(storage.progress, 50);
        }
    }

    @IsTest
    public static void testGetStorageInfoWithError() {
        CSWPProgressBarController.StorageWrapper storage = CSWPProgressBarController.getStorageInfo();
        System.assertEquals(storage.currentStorage, 0.00);
        System.assertEquals(storage.storageLimit, 0.00);
        System.assertEquals(storage.progress, 0.00);
    }
}