/*************************************************************************************************************
 * @name			CSWPRepositoryManager
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			25 / 11 / 2020
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-25		oguzalp					Changes desription
 *
 **************************************************************************************************************/
public with sharing class CSWPRepositoryManager {
  @TestVisible
  private static final String testHubItemId = 'item:L3NpdGVzL1NhbGVzZm9yY2VmaWxlcmVwb3NpdG9yeWRldnRlc3Q:6b996eec-9fe1-4150-bb52-2f824c53dd1a:7a464605-7c6b-432e-b4c0-e1b89b00d9cd:99';
  public static cswp_Workspace__c createRepositorFolderByWorkspace(
    cswp_Workspace__c workspace
  ) {
    String rootPath = workspace.cswp_InternalParentPath__c;
    if (String.isBlank(rootPath)) {
      CSWPUtil.errorlog('ROOT PATH CAN NOT BE BLANK!');
      workspace.cswp_IsSync__c = false;
      workspace.cswp_LastSyncTime__c = null;
      return workspace;
    }

    SharepointService service = SharepointService.init();
    SharepointService.DriveItem rootItem = service.getDriveItemByPath(rootPath);
    SharepointService.DriveItem createdFolderItem;
    //Dont Create Folder If Folder Exist with The Same Name on Sharepoint
    createdFolderItem = service.getDriveItemByPath(rootPath+'/'+workspace.Name);
    //Dont Create Folder If Folder Exist with The Same Name on Sharepoint
    if(createdFolderItem == null) {
        createdFolderItem = service.createFolder(
            workspace.Name,
            rootItem.id
        );
    }
    if (createdFolderItem != null) {
      workspace.cswp_ExternalId__c = createdFolderItem.id;
      workspace.cswp_ExternalContentLength__c = createdFolderItem.size;
      workspace.cswp_ExternalParentPath__c = createdFolderItem.parentReference.path;
      workspace.Name = createdFolderItem.name;
      workspace.cswp_IsSync__c = true;
      workspace.cswp_LastSyncTime__c = DateTime.now();
      //TODO: Get hubItemId
      CSWPUtil.debugLog( ' createRepositorFolderByWorkspace.workspace:>>>> ' + JSON.serializePretty(workspace)
      );
      ConnectApi.RepositoryFolderItem repoItem = SharepointService.getHubItemByParentId(
        Test.isRunningTest() ? testHubItemId : workspace.cswp_ParentHubItemID__c,
        workspace.name,
        'folder'
      );
      CSWPUtil.debugLog(
        ' createRepositorFolderByWorkspace.repoItem:>>>> ' +
        JSON.serializePretty(repoItem)
      );
      if (repoItem.folder != null) {
        workspace.cswp_HubItemId__c = repoItem.folder.id;
      }
      else {
        CSWPLogManagement.createLogForCSWP('Hub Item Cannot be found on Sharepoint', 'CSWPRepositoryManager.createRepositorFolderByWorkspace', false, 'CalloutException', createdFolderItem.name);
      }
      return workspace;
    } else {
      workspace.cswp_IsSync__c = false;
      workspace.cswp_LastSyncTime__c = null;
      return workspace;
    }
  }

  @future(callout=true)
  public static void createRepositorFolderByWorkspaceAsync(
    Set<Id> workspaceIds
  ) {
    List<cswp_Workspace__c> workspaces = CSWPSobjectSelector.getWorkspacesByIdSet(
      workspaceIds
    );
    for (cswp_Workspace__c workspace : workspaces) {
      createRepositorFolderByWorkspace(workspace);
    }
    Database.update(workspaces);
  }

  @future(callout=true)
  public static void getHubItemIdByParentAsync(Set<Id> workspaceIds) {
    //TODO: Check if it is needed.
    // List<cswp_Workspace__c> workspaces = CSWPSObjectSelector.getWorkspacesByIdSet(workspaceIds);
    //TODO: add it to the selector class
    List<cswp_Workspace__c> workspaces = [
      SELECT Id, Name, cswp_ParentHubItemID__c
      FROM cswp_Workspace__c
      WHERE cswp_ParentHubItemID__c != NULL AND ID IN :workspaceIds
    ];
    for (cswp_Workspace__c space : workspaces) {
      ConnectApi.RepositoryFolderItem repoItem = SharepointService.getHubItemByParentId(
        space.cswp_ParentHubItemID__c,
        space.name,
        'folder'
      );
      if (repoItem.folder != null) {
        space.cswp_HubItemId__c = repoItem.folder.id;
      }
    }
    //Skip the Folder Trigger
    Triggers.skips.add(CSWPWorkspaceTriggerHandler.class);
    Database.update(workspaces);
    Triggers.skips.remove(CSWPWorkspaceTriggerHandler.class);
  }

  @future(callout=true)
  public static void getHubItemByParentFolderAsync(Set<Id> folderIds){
    getFolderHubItemByParent(folderIds);
  }


  //Only cswpFile and cswpFolders can be deleted.!
  //CSWP delete trigger will inoke this method.
  @future(callout=true)
  public static void deleteExternalItems(List<String> drivePaths) {
    if (drivePaths.size() >= 50) {
      return;
    }
    SharepointService service = SharepointService.init();
    Boolean success = false;
    for (Integer i = 0; i < drivePaths.size(); i++) {
      SharepointService.DriveItem item = service.getDriveItemByPath(
        drivePaths.get(i)
      );
      if (item != null) {
        success = service.deleteDriveItem(item.id);
      }
      CSWPUtil.debugLog('DELETE.FILE.ITEM.RESULT:>>> ' + success);
    }
  }
  //CSWPFolderTriggerHandler will invoke this!
  @future(callout=true)
  public static void deleteFolderFromExternal(List<String> folderExternalIds) {
    if (folderExternalIds.size() >= 100) {
      return;
    }
    SharepointService service = SharepointService.init();
    for (Integer i = 0; i < folderExternalIds.size(); i++) {
      Boolean success = service.deleteDriveItem(folderExternalIds.get(i));
      CSWPUtil.debugLog('DELETE.FILE.ITEM.RESULT:>>> ' + i + ' = ' + success);
    }
  }
  @future(callout=true)
  public static void updateExternalItemName(String path, String newName) {
    try {
      SharepointService service = SharepointService.init();
      SharepointService.DriveItem item = service.updateItemByGivenPath(
        path,
        newName
      );
      CSWPUtil.debugLog('updatedDriveItem :>>> ' + JSON.serialize(item));
    } catch (Exception ex) {
      System.debug(ex.getMessage());
    }
  }

  public static String getFileExternalId(String cswpFileId) {
    CSWP_File__c file = CSWPSobjectSelector.getCswpFilesByIdSet(
        new Set<Id>{ ID.valueOf(cswpFileId) }
      )
      .get(0);
    SharepointService service = SharepointService.init();
    String extId = '';
    if (String.isNotBlank(file.cswp_ExternalId__c)) {
      extId = file.cswp_ExternalId__c;
    } else {
      extId = service.getDriveItemByPath(file.cswp_FilePath__c).id;
    }
    return extId;
  }

  public static void getFolderHubItemByParent(Set<Id> folderIds) {
    if (folderIds == null || folderIds.isEmpty()){
      return;
    }
    List<cswp_Folder__c> folders = CSWPSobjectSelector.getFoldersByIdSet(
      folderIds
    );
    String parentHubItemId;
    for (cswp_Folder__c folder : folders) {
      if (String.isnotBlank(folder.cswp_ParentHubItemID__c)) {
        parentHubItemId = folder.cswp_ParentHubItemID__c;
      }else if(String.isNotBlank(folder.cswp_ParentFolder__c) && folder.cswp_ParentFolder__r.cswp_IsSync__c){
        parentHubItemId = folder.cswp_ParentFolder__r.cswp_HubItemId__c;
      }else if(String.isNotBlank(folder.cswp_Workspace__c)){
        parentHubItemId = folder.cswp_Workspace__r.cswp_HubItemId__c;
      }
      System.debug(LoggingLevel.DEBUG, 'getFolderHubItemByParent:>>>' +  parentHubItemId);
      if(String.isNotBlank(parentHubItemId)){
        ConnectApi.RepositoryFolderItem folderItem = SharepointService.getHubItemByParentId(
          parentHubItemId,
          folder.Name,
          'folder'
        );
        CSWPUtil.debugLog( 'CSWP: FOLDERITEM.HUB.ID:>>> : ' + folderItem?.folder?.id);
        if (String.isNotBlank(folderItem?.folder?.id)) {
          folder.cswp_HubItemId__c = folderItem.folder.id;
        }
      }
    }
    //Skip the Folder and Workspace Trigger
    // Triggers.skips.add(cswp_Workspace_TriggerHandler.class);
    // Triggers.skips.add(CSWPFolderTriggerHandler.class);
    Database.SaveResult[] result = Database.update(folders,false);
    CSWPUtil.debugLog('SAVERESULT:>>> ' + Json.serializePretty(result));
    // Triggers.skips.remove(CSWPFolderTriggerHandler.class);
  }

  /*@future(callout=true)
  public static void uploadFilesAsRepoItemAsync(Set<Id> contentFileIds) {
    if (contentFileIds == null && contentFileIds.size() == 0) {
      return;
    }
    List<ContentVersion> files = CSWPSobjectSelector.getContentVersionsByIdSet(
      contentFileIds
    );
    List<ContentVersion> eligibleFiles = new List<ContentVersion>();
    Schema.SObjectType objectType;
    for (ContentVersion file : files) {
      if (String.isBlank(file.cswp_ParentId__c)) {
        continue;
      }
      eligibleFiles.add(file);
    }
    Id parentId = Id.valueOf(eligibleFiles.get(0).cswp_ParentId__c);
    // Upload the files to the external System
    uploadFilesToFolderOrWorkspace(parentId, eligibleFiles);
  }*/

  //UploadFilesToFolderOrWorkspace
  public static List<cswp_File__c> uploadFilesToFolderOrWorkspace(
    Id parentPlaceId,
    List<ContentVersion> files
  ) {
    SObject parentPlace;
    SObjectType parentType = parentPlaceId.getSobjectType();
    Boolean updateParentMust = false;
    Boolean isCSWPInternalAdmin = CSWPUtil.isCSWPAdminUser();
    Boolean isCommunityUser = CSWPUtil.isCommunityUser();

    if (parentType == Schema.cswp_Workspace__c.getSobjectType()) {
      parentPlace = CSWPSobjectSelector.getWorkspacesByIdSet(
          new Set<Id>{ parentPlaceId }
        )
        .get(0);
    }
    if (parentType == Schema.cswp_Folder__c.getSobjectType()) {
      parentPlace = CSWPSobjectSelector.getFoldersByIdSet(
          new Set<Id>{ parentPlaceId }
        )
        .get(0);
    }
    List<cswp_File__c> cswpFiles = new List<cswp_File__c>();
    List<CSWPContent.File> contentFiles = new List<CSWPContent.File>();
    if (String.isBlank((String) parentPlace.get('cswp_HubItemId__c'))) {
      CSWPUtil.debugLog('parentPlace.cswp_HubItemId__c.ISBLANK');
      CSWPUtil.debugLog( 'parentPlace.cswp_ParentHubItemID__c:>' + (String) parentPlace.get('cswp_ParentHubItemID__c')
      );
      ConnectApi.RepositoryFolderItem repoItem = SharepointService.getHubItemByParentId(
        Test.isRunningTest() ? testHubItemId : (String) parentPlace.get('cswp_ParentHubItemID__c'),
        (String) parentPlace.get('Name'),
        'folder'
      );
      CSWPUtil.debugLog( 'ParentHub FolderItem :> ' + JSON.serializePretty(repoItem.folder) );
      if (repoItem.folder != null) {
        parentPlace.put('cswp_HubItemId__c', repoItem.folder.id);
        updateParentMust = true;
      }
    }
    for (ContentVersion file : files) {
      //Then do not create external file, put them into approval process
      /*isCSWPInternalAdmin &&*/ //&& !Test.isRunningTest()
      if (!isCommunityUser) {
        CSWPContent.File fileObj = new CSWPContent.File(file, parentPlace);
        fileObj.initCswpFile(parentPlace);
        cswpFiles.add(fileObj.cswpFile);
        contentFiles.add(fileObj);
        continue;
      }

      CSWPContent.File fileContent = CSWPRepositoryManager.uploadContentVersionToRemoteRepo(
        file,
        parentPlace
      );
      contentFiles.add(fileContent);
      cswpFiles.add(fileContent.cswpFile);
    }
    //cswpFolder is mandatory for the files, so we should create one root folder for product-document libraries,if it does not exist yet!
    if (parentType == Schema.cswp_Workspace__c.getSobjectType()) {
      cswp_Folder__c rootFolder = getRootFolderForWorkspace( (cswp_Workspace__c) parentPlace );
      for (cswp_File__c cf : cswpFiles) {
        cf.cswp_Folder__c = rootFolder.Id;
      }
    }
    Database.SaveResult[] results = Database.insert(cswpFiles, false);
    CSWPUtil.debugLog( 'AFTER INSERT CSWP FILES  ==> ' + JSON.serializePretty(cswpFiles) ); //&& !Test.isRunningTest()
    /** isCSWPInternalAdmin && */ 
    if (!isCommunityUser) {
      linkContentVersionToFile(contentFiles);
      // Send the file to approval automatically.
      sendFilesToApproval(contentFiles);
    } else {
      // Convert them to the external repo item
      //TODO: call activity event and saved the activitiy
      //TODO: check if it still need it.
      convertFileVersionToExternalForm(contentFiles);
    }
    if (updateParentMust) {
      //Update parent place
      Database.update(parentPlace, false);
    }
    return cswpFiles;
  }

  public static void sendFilesToApproval(List<CSWPContent.File> files) {
    List<Approval.ProcessRequest> approvalRequests = new List<Approval.ProcessRequest>();
    CSWPUtil.debugLog('sendFileToApprovalProcess:>>>>>>' + files);
    for (CSWPContent.File file : files) {
      if (String.isBlank(file.approverId)) {
        continue;
      }
      Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
      request.setObjectId(file.cswpFile.Id);
      request.setNextApproverIds(new List<Id>{ file.approverId });
      request.setComments(System.Label.cswp_ApprovalProcessComment);
      approvalRequests.add(request);
    }
    Approval.ProcessResult[] results = Approval.process(approvalRequests);
    CSWPUtil.debugLog(
      'APPROVAL PROCESS RESULT :>>>\n' + JSON.serializePretty(results)
    );
  }

  @future(callout=true)
  public static void uploadFilesAfterApprovalProcess(Set<Id> cswpFileIds) {
    //TODO: create and populate cswpParentHubItemId on cswpFile and populate it in before flow.
    SharepointService service = SharepointService.init();
    List<cswp_File__c> cswpFiles = CSWPSobjectSelector.getCswpFilesByIdSet(
      cswpFileIds
    );
    Map<Id, cswp_File__c> updatedFiles;
    //It keeps file Id and lastContentVersionId
    Map<Id, Id> contentVersionFileIds = new Map<Id, Id>();
    Set<Id> contentVersionIds = new Set<Id>();
    List<CSWPContent.File> contentFiles = new List<CSWPContent.File>();
    CSWP_File__c firstFile = cswpFiles.get(0);
    SObject parentPlace;
    String parentHubItemId;
    if (String.isNotBlank(firstFile.cswp_Folder__c)) {
      //If it is dummy folder.(root and hidden)
      if (
        firstFile.cswp_Folder__r.cswp_IsRoot__c &&
        firstFile.cswp_Folder__r.cswp_IsHidden__c
      ) {
        parentPlace = CSWPSobjectSelector.getWorkspacesByIdSet(
            new Set<Id>{ firstFile.cswp_Workspace__c }
          )
          .get(0);
      } else {
        parentPlace = CSWPSobjectSelector.getFoldersByIdSet(
            new Set<Id>{ firstFile.cswp_Folder__c }
          )
          .get(0);
      }
    } else {
      parentPlace = CSWPSobjectSelector.getWorkspacesByIdSet(
          new Set<Id>{ firstFile.cswp_Workspace__c }
        )
        .get(0);
    }
    for (Cswp_File__c cswpFile : cswpFiles) {
      contentVersionFileIds.put(
        cswpFile.cswp_LastContentVersionId__c,
        cswpFile.Id
      );
    }
    updatedFiles = new Map<Id, cswp_File__c>();
    List<ContentVersion> fileVersions = CSWPSobjectSelector.getContentVersionsByIdSet(
      contentVersionFileIds.keyset()
    );
    for (ContentVersion file : fileVersions) {
      CSWPContent.File contentFile = CSWPRepositoryManager.uploadContentVersionToRemoteRepo(
        file,
        parentPlace
      );
      contentFile.cswpFile.Id = contentVersionFileIds.get(file.Id); //file.cswp_FileId__c;
      contentFiles.add(contentFile);
      updatedFiles.put(contentFile.cswpFile.Id, contentFile.cswpFile);
    }
    List<ContentVersion> convertedContents = convertFileVersionToExternalForm(
      contentFiles
    );
    for (ContentVersion version : convertedContents) {
      if (updatedFiles.containsKey(version.cswp_FileId__c)) {
        updatedFiles.get(version.cswp_FileId__c)
          .cswp_LastContentVersionId__c = version.Id;
        //Remove old content document id from file contentdocument field
        updatedFiles.get(version.cswp_FileId__c)
          .cswp_ContentDocumentId__c = null;
      }
    }
    Database.SaveResult[] result = Database.update(updatedFiles.values());
    CSWPUtil.debugLog(
      'UPDATED FILES> RESULT :>>>>> \n' + JSON.serializePretty(result)
    );
    //Contert files to external form
  }

  //This method needs to be invoked within an async method that allows callout to external systems.
  public static CSWPContent.File uploadContentVersionToRemoteRepo(
    ContentVersion file,
    SObject parentPlace
  ) {
    CSWPContent.File fileContent = new CSWPContent.File();
    String mimeType = CSWPMimeTypeHelper.getMimeType(file.FileExtension);
    String parentHubItemId = Test.isRunningTest()
      ? testHubItemId
      : (String) parentPlace.get('cswp_HubItemId__c');
    System.debug(LoggingLevel.DEBUG, 'ParanetHubId:>>>>>' + parentHubItemId);
    ConnectApi.BinaryInput fileBinaryInput = new ConnectApi.BinaryInput(
      file.VersionData,
      mimeType,
      file.PathOnClient
    );
    //Upload files to the external system!
    // System.debug(LoggingLevel.DEBUG, 'Binary Input:>>>>>>>>' + fileBinaryInput);
    ConnectApi.RepositoryFileSummary hubFile = SharepointService.uploadFileAsRepoItem(
      parentHubItemId,
      fileBinaryInput
    );

    if (hubFile != null) {
      fileContent = new CSWPContent.File(file, hubFile);
      fileContent.initCswpFile(parentPlace);
      //fileContent.setExternalId();
    }
    CSWPUtil.debugLog(
      'CSWP FILE WAS UPLOADED====>\n' + hubFile == null
        ? 'NULL'
        : JSON.serializePretty(hubFile)
    );
    return fileContent;
  }

  public static List<ContentVersion> convertFileVersionToExternalForm(
    List<CSWPContent.File> files
  ) {
    List<Id> tobeDeletedContentDocuments = new List<Id>();
    List<ContentVersion> newContentVersions = new List<ContentVersion>();
    String repoId = SharepointService.getContentHubRepositoryId();
    for (CSWPContent.File theFile : files) {
      //CSWPUtil.debugLog('THE FILE ===>\n' + JSON.serializePretty( theFile.cswpFile ));
      if (String.isBlank(theFile.cswpFile.id)) {
        continue;
      }
      tobeDeletedContentDocuments.add(theFile.contentDocumentId);
      ContentVersion fileVersion = new ContentVersion(
        Title = theFile.title,
        PathOnClient = theFile.name,
        cswp_Sync__c = theFile.isSynced,
        cswp_SyncTime__c = theFile.externalCreatedDate,
        cswp_FileId__c = theFile.cswpFile.Id,
        FirstPublishLocationId = theFile.cswpFile.Id,
        cswp_ParentId__c = theFile.parentObject.Id,
        ContentLocation = 'E',
        Origin = 'H',
        ExternalDataSourceId = repoId,
        ExternalDocumentInfo1 = theFile.externalContentUrl,
        ExternalDocumentInfo2 = theFile.hubItemId
      );
      if (UserInfo.getUserType() != 'Standard') {
        fileVersion.NetworkId = CSWPUtil.getCswpCommunityId();
      }
      //Add new content versions
      newContentVersions.add(fileVersion);
    }
    //1.insert new content contentversions
    Database.SaveResult[] resultInsertVersion = Database.insert(
      newContentVersions,
      false
    );
    //TODO: Error log needed
    CSWPUtil.debugLog(
      'CONTENT VERSION Insert RESULT ==> ' +
      JSON.serializePretty(resultInsertVersion)
    );
    //2. delete old content documents
    deleteContents(tobeDeletedContentDocuments);
    return newContentVersions;
  }

  private static void linkContentVersionToFile(List<CSWPContent.File> files) {
    List<ContentDocumentLink> documentLinks = new List<ContentDocumentLink>();
    List<ContentVersion> updateVersions = new List<ContentVersion>();
    for (CSWPContent.File theFile : files) {
      if (String.isBlank(theFile.cswpFile.id)) {
        continue;
      }
      documentLinks.add(
        new ContentDocumentLink(
          ContentDocumentId = theFile.contentDocumentId,
          LinkedEntityId = theFile.cswpFile.id,
          ShareType = 'I',
          Visibility = 'InternalUsers'
        )
      );
      updateVersions.add(
        new ContentVersion(
          Id = theFile.contentVersionId,
          cswp_FileId__c = theFile.cswpFile.id
        )
      );
    }
    //Link content to the files
    Database.SaveResult[] docLinkResult = Database.insert(documentLinks, false);
    //Update content versions with their file Ids.
    Database.SaveResult[] contentUpdateResult = Database.update(
      updateVersions,
      false
    );
  }

  private static void deleteContents(List<Id> contentDocumentIds) {
    Database.DeleteResult[] result = Database.delete(contentDocumentIds, false);
    //TODO: Error Log needed.
    /*CSWPUtil.debugLog(
      'CONTENT Documents DELETE RESULT ==> ' + JSON.serializePretty(result)
    );*/
  }

  public static cswp_Folder__c getRootFolderForWorkspace( cswp_Workspace__c workspace ) {
    //workspace.Name.toLowerCase() +
    String externalId = '%' + '-root-' + workspace.Id;
    List<cswp_Folder__c> folder = [
      SELECT Id, cswp_Workspace__c, cswp_ExternalId__c
      FROM cswp_Folder__c
      WHERE
        cswp_ExternalId__c LIKE :externalId
        AND cswp_Workspace__c = :workspace.Id
        AND cswp_isHidden__c = TRUE
        AND cswp_IsRoot__c = TRUE
      ORDER BY CreatedDate DESC
    ];
    if (!folder.isEmpty()) {
      return folder.get(0);
    }
    cswp_Folder__c rootFolder = new cswp_Folder__c(
      Name = workspace.Name + '_' + CSWPUtil.ROOT_FOLDER_PRODUCT_LIB,
      cswp_Workspace__c = workspace.Id,
      cswp_ExternalId__c = workspace.Name + '-root-' + workspace.Id,
      cswp_IsRoot__c = true,
      cswp_IsHidden__c = true
    );
    Database.UpsertResult result = Database.Upsert(
      rootFolder,
      Schema.cswp_Folder__c.cswp_ExternalId__c
    );
    CSWPUtil.debugLog('CREATE ROOT FOLDER RESULT ===> \n' + result);
    return rootFolder;
  }

  @future(callout=true)
  public static void createFolderOnSite(Set<Id> cswpFolderIds) {
    List<cswp_Folder__c> folders = CSWPSobjectSelector.getFoldersByIdSet(
      cswpFolderIds
    );
    SharepointService service = SharepointService.init();
    Map<Id, SharepointService.DriveItem> folderItems = new Map<Id, SharepointService.DriveItem>();
    for (cswp_Folder__c folder : folders) {
      String parentId, parentHubItemId;
      if (String.isNotBlank(folder.cswp_ParentFolder__c)) {
        parentId = folder.cswp_ParentFolder__r.cswp_ExternalId__c;
        parentHubItemId = folder.cswp_ParentFolder__r.cswp_HubItemId__c;
      } else {
        parentId = folder.cswp_Workspace__r.cswp_ExternalId__c;
        parentHubItemId = folder.cswp_Workspace__r.cswp_hubItemId__c;
      }
      if (
        String.isNotBlank(parentId) &&
        !(folder.cswp_IsRoot__c && folder.cswp_IsHidden__c)
      ) {
        CSWPUtil.debugLog('CreateOnSite.parentID===>' + parentId);
        SharepointService.DriveItem folderItem = service.createFolder(
          folder.Name,
          parentId
        );
        CSWPUtil.debugLog(
          'CreateOnSite.FolderItem ===>' + JSON.serializePretty(folderItem)
        );
        if (folderItem != null && String.isNotBlank(folderItem.id)) {
          folder.cswp_ExternalId__c = folderItem.id;
          folder.cswp_ExternalParentPath__c = folderItem.parentReference.path;
          folder.cswp_ParentFolderId__c = folderItem.parentReference.id;
          folder.cswp_createdDateTime__c = folderItem.createdDateTime;
          folder.cswp_Creator__c = UserInfo.getUserId();
          folder.cswp_ContentSize__c = folderItem.size;
          folder.cswp_IsSync__c = true;
          //Try to get folder hub item id by its parent
          if (String.isNotBlank(parentHubItemId)) {
            CSWPUtil.debugLog(
              'createFolderOnSite.parentHubItemId==>' + parentHubItemId
            );
            ConnectApi.RepositoryFolderItem repoItem = SharepointService.getHubItemByParentId(
              parentHubItemId,
              folder.Name,
              'folder'
            );
            CSWPUtil.debugLog(
              'CSWP: FOLDERITEM INFO : \n' + JSON.serializePretty(repoItem)
            );
            if (repoItem != null && repoItem.folder != null) {
              folder.cswp_HubItemId__c = repoItem.folder.id;
            }
          }
        } else {
          folder.cswp_IsSync__c = false;
          folder.cswp_createdDateTime__c = null;
        }
        CSWPUtil.debugLog(
          'createFolderOnSite.FOLDERITEM.RESULT :>>>>>> \n' +
          JSON.serialize(folderItem)
        );
        //folderItems.put(folder.Id, folderItem);
      }
    }
    Database.SaveResult[] results = Database.update(folders);
    // CSWPUtil.debugLog(
    //   'CREATEFOLDERONSITE.FOLDERS.UPDATE.RESULT :>>>>\n' +
    //   JSON.serialize(results)
    // );
  }
}