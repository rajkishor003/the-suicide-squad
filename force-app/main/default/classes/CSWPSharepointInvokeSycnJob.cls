public class CSWPSharepointInvokeSycnJob {
    @InvocableMethod(label='Invoke Sharepoint Sync Job' description='Executes the CSWPSharepointSycnQueueableJob' category='')
    public static void invokeSharepointSyncJob(List<Id> recordIds){
      System.enqueueJob(new CSWPSharepointSycnQueueableJob(recordIds.get(0)));
    }
}