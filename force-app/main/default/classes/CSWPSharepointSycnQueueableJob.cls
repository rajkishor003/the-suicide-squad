/**
 * @author			oguz <oguz.alp@emakina.com.tr>
 * @created			29 / 07 / 2021
 * @name			CSWPSharepointSycnQueueableJob.cls
 * @description		It fetches the parent item's children from sharepoint and creates folder/files into salesforce
 */
public class CSWPSharepointSycnQueueableJob implements Queueable, Database.AllowsCallouts {
  public SObject parent { get; set; }
  public List<String> itemIds { get; set; }
  public String parentExtId { get; set; }
  public String workspaceExtId { get; set; }

  private cswp_folder__c parentRootFolderOfWorkspace { get; set; }

  Boolean populateParentFolder = false;
  private static final Integer CHUNK_SIZE = 10;
  /**
   * parentWorkspace can be either cswp_Workspace__or a cswp_Folder__c
   */
  public CSWPSharepointSycnQueueableJob(String recordId) {
    //reserved for future usage
    if ( Id.valueOf(recordId).getSobjectType() == cswp_Workspace__c.getSObjectType() ) {
      this.parent = getWorkspaceById(recordId);
      this.parentRootFolderOfWorkspace = getRootFolderByWorkspaceId(recordId);
      System.debug( LoggingLevel.DEBUG, 'PARENT_ROOT_FOLDER :> ' + this.parentRootFolderOfWorkspace );
      this.workspaceExtId = (String) parent.get('cswp_ExternalId__c');
    } else if (
      Id.valueOf(recordId).getSobjectType() == cswp_Folder__c.getSObjectType()
    ) {
      this.parent = getFolderById(recordId);
      this.workspaceExtId = String.valueOf(
        parent.getSObject('cswp_Workspace__r').get('cswp_ExternalId__c')
      );
      populateParentFolder = true;
    }
    System.debug(LoggingLevel.DEBUG, 'WorkspaceId:>>' + this.workspaceExtId);
    if (this.parent != null) {
      this.parentExtId = (String) parent.get('cswp_ExternalId__c');
    }
  }

  public CSWPSharepointSycnQueueableJob(List<String> externalItemIds) {
    this.itemIds = externalItemIds;
    populateParentFolder = true;
  }

  cswp_Workspace__c getWorkspaceById(String wsId) {
    return (new List<cswp_Workspace__c>(
        [
          SELECT Id, Name, cswp_ExternalId__c
          FROM cswp_Workspace__c
          WHERE Id = :wsId
          LIMIT 1
        ]
      ))
      .get(0);
  }

  cswp_Folder__c getRootFolderByWorkspaceId(String wsId) {
    String rootExternalId = '%' + '-root-' + wsId;
    return (new List<cswp_folder__c>(
        [
          SELECT Id, Name, cswp_ExternalId__c
          FROM cswp_folder__c
          WHERE
            cswp_Workspace__c = :wsId
            AND cswp_IsHidden__c = TRUE
            AND cswp_IsRoot__c = TRUE
            AND cswp_ExternalId__c LIKE :rootExternalId
        ]
      ))
      .get(0);
  }

  cswp_Folder__c getFolderById(String fId) {
    return (new List<cswp_Folder__c>(
        [
          SELECT
            Id,
            Name,
            cswp_ExternalId__c,
            cswp_Workspace__r.cswp_ExternalId__c
          FROM cswp_Folder__c
          WHERE Id = :fId
          LIMIT 1
        ]
      ))
      .get(0);
  }

  public void execute(QueueableContext ctx) {
    if (String.isNotBlank(this.parentExtId)) {
      createChildItems(this.parentExtId);
    } else if (!itemIds.isEmpty()) {
      // do something recursively
      System.debug(LoggingLevel.DEBUG, 'ITEM >> Ids >>>' + itemIds);
      // Map<String,SObject> subItems = fethCreatedChildren(itemIds);
      Set<String> subItemIds = fethCreatedChildren(itemIds);

      System.debug( LoggingLevel.DEBUG, 'SUB_ITEMS.SIZE>>>>>' + subItemIds.size()
      );
      // createMassChildItems(subItems.keySet());
      createMassChildItems(subItemIds);
    }
  }

  void createMassChildItems(Set<String> externalIds) {
    if (externalIds == null || externalIds.isEmpty()) {
      return;
    }
    List<SharepointService.DriveItem> allSubDriveItems = new List<SharepointService.DriveItem>();
    Savepoint sp;
    try {
      /**
       * TODO: Try the batch items
       */
      Map<String, List<SharepointService.DriveItem>> driveItemsAsMap = getBatchChildItems(
        new List<String>(externalIds)
      );

      if (driveItemsAsMap.isEmpty()) {
        return;
      }

      for (String itemId : driveItemsAsMap.keySet()) {
        allSubDriveItems.addAll(driveItemsAsMap.get(itemId));
      }
      System.debug( LoggingLevel.DEBUG, 'CALLOUT.LIMIT >>>>> ' + System.Limits.getLimitCallouts() + ' | CURRENT_CALLOUT_NO:>>> ' + System.Limits.getCallouts());

      List<SObject> subItems = new List<SObject>();
      sp = Database.setSavePoint();
      for (SharepointService.DriveItem item : allSubDriveItems) {
        subItems.add(convertDriveItemToSobject(item));
      }
      List<Id> successIds = insertChildItemAsSobject(subItems);
      if (!successIds.isEmpty()) {
        getFolderHubItemIds(new Set<Id>(successIds));
        System.enqueueJob(new CSWPSharepointSycnQueueableJob(successIds));
      }
    } catch (Exception ex) {
      System.debug(
        LoggingLEvel.ERROR,
        'AN ERROR OCCURED>createMassChildItems>' + ex.getMessage()
      );
      Database.rollback(sp);
    }
  }

  void getFolderHubItemIds(Set<Id> childItems){
    Set<Id> childIdClone = new Set<Id>(childItems);
    for(Id childId: childIdClone){
      if(childId.getSobjectType() == cswp_File__c.getSObjectType()){
        childIdClone.remove(childId);
      }
    }
    CSWPRepositoryManager.getHubItemByParentFolderAsync( childIdClone );
  }

  void createChildItems(String externalId) {
    Savepoint sp;
    try {
      List<SharepointService.DriveItem> items = getChildItems(externalId);
      if (items == null && items.size() == 0)
        return; // Log it!
      List<SObject> childItems = new List<SObject>();
      sp = Database.setSavePoint();
      for (SharepointService.DriveItem ditem : items) {
        // SObject sitem = convertDriveItemToSobject(ditem);
        childItems.add(convertDriveItemToSobject(ditem));
      }
      List<Id> ids = insertChildItemAsSobject(childItems);
      if (!ids.isEmpty()) {
        getFolderHubItemIds(new Set<Id>(ids));
        System.enqueueJob(new CSWPSharepointSycnQueueableJob(ids));
      }
    } catch (Exception ex) {
      System.debug( LoggingLevel.ERROR, 'AN ERROR OCCURED. CREATE_CHILD_ITEMS:>>> ' + ex.getMessage() +
        'stack:\n' +
        ex.getStackTraceString()
      );
      Database.rollback(sp);
    }
  }

  List<Id> insertChildItemAsSobject(List<SObject> subItems) {
    Database.SaveResult[] result = Database.insert(subItems, false);
    List<String> successIds = new List<String>();
    for (Database.SaveResult res : result) {
      if (res.isSuccess()) {
        successIds.add(res.getId());
      }
    }

    return successIds;
  }

  List<SharepointService.DriveItem> getChildItems(String externalItemId) {
    SharepointService service = SharepointService.init();
    List<SharepointService.DriveItem> childItems = service.getChildItemsByParentItemId(
      externalItemId
    );
    return childItems;
  }

  Map<String, List<SharepointService.DriveItem>> getBatchChildItems(
    List<String> externalItemIdList
  ) {
    SharepointService service = SharepointService.init();
    Map<String, List<SharepointService.DriveItem>> result = new Map<String, List<SharepointService.DriveItem>>();
    try {
      if (externalItemIdList.size() < CHUNK_SIZE) {
        result = service.getChildItemsBatchable(externalItemIdList);
      } else {
        List<Object> chunkedList = CSWPUtil.splitArrayIntoChunks(
          externalItemIdList,
          CHUNK_SIZE
        );
        for (Object extItemList : chunkedList) {
          List<String> itemIdsList = (List<String>) extItemList;
          result.putAll(service.getChildItemsBatchable(itemIdsList));
        }
      }
    } catch (Exception ex) {
      System.debug(
        LoggingLevel.ERROR,
        'ERROR >> getBatchChildItems >> ' +
        ex.getMessage() +
        '|' +
        ex.getStackTraceString()
      );
    }
    return result;
  }

  Set<String> fethCreatedChildren(List<String> ids) {
    List<Id> folderIds = new List<Id>();
    List<Id> fileIds = new List<Id>();
    Set<String> folderExternalIds = new Set<String>();

    for (String objid : ids) {
      Id objectId = Id.valueOf(objid);
      //Take only folders in order to get their sub items
      if (objectId.getSObjectType() == Schema.cswp_Folder__c.getSobjectType()) {
        folderIds.add(objectId);
      }
    }
    if (!folderIds.isEmpty()) {
      for (cswp_Folder__c folder : getFolderByIdList(folderIds)) {
        folderExternalIds.add(folder.cswp_ExternalId__c);
      }
    }
    return folderExternalIds;
  }

  List<cswp_Folder__c> getFolderByIdList(List<Id> ids) {
    return [
      SELECT
        Id,
        Name,
        cswp_Workspace__c,
        cswp_ExternalId__c,
        cswp_ParentFolder__c
      FROM cswp_Folder__c
      WHERE Id IN :ids
    ];
  }

  /**
   * It convert to CSWPFolder or CSWPFile
   */
  SObject convertDriveItemToSobject(
    SharepointService.DriveItem sharepointItem
  ) {
    //System.debug(LoggingLevel.DEBUG, 'convertDriveItemToSobject.sharepointDriveItem.:>>> ' + sharepointItem );
    sObject item;
    if (sharepointItem.folder != null) {
      item = initCSWPFolder(sharepointItem);
    }
    if (sharepointItem.file != null) {
      item = initCSWPFile(sharepointItem);
    }
    System.debug(
      LoggingLevel.DEBUG,
      'convertDriveItemToSobject.SObjectItem.:>>> ' + item
    );
    return item;
  }

  cswp_Folder__c initCSWPFolder(SharepointService.DriveItem driveItem) {
    cswp_Folder__c folder = new cswp_Folder__c(
      Name = driveItem.name,
      cswp_ExternalId__c = driveItem.id,
      cswp_ContentSize__c = (driveItem.size != null
        ? Decimal.valueof(driveItem.size)
        : null),
      cswp_ExternalParentPath__c = driveItem.parentReference?.path,
      cswp_IsSync__c = true,
      cswp_WebUrl__c = driveItem.webUrl,
      cswp_CreatedFromSharepoint__c = true,
      cswp_createdDateTime__c = driveItem.createdDateTime
    );
    if (String.isNotBlank(this.workspaceExtId)) {
      folder.cswp_Workspace__r = new cswp_Workspace__c(
        cswp_ExternalId__c = this.workspaceExtId
      );
    }
    if (populateParentFolder) {
      folder.cswp_ParentFolder__r = (driveItem.parentReference != null
        ? new cswp_Folder__c(cswp_ExternalId__c = driveItem.parentReference?.id)
        : null);
    }

    return folder;
  }

  cswp_File__c initCSWPFile(SharepointService.DriveItem driveItem) {
    cswp_File__c file = new cswp_File__c(
      Name = driveItem.name,
      cswp_ExternalId__c = driveItem.id,
      cswp_FileSize__c = driveItem.size != null
        ? Decimal.valueOf(driveItem.size)
        : null,
      cswp_Status__c = 'Approved',
      cswp_WebUrl__c = driveItem.webUrl,
      cswp_MimeType__c = driveItem.file?.mimeType,
      cswp_FileHash__c = driveItem.file?.hashes?.quickXorHash,
      cswp_CreatedFromSharepoint__c = true,
      cswp_UploadedDateTime__c = driveItem.createdDateTime,
      cswp_UploadedBy__c = UserInfo.getUserId()
    );
    System.debug(
      LoggingLevel.DEBUG,
      'initCSWPFile.WorkspaceExternalId:>>' + this.workspaceExtId
    );
    if (String.isNotBlank(this.workspaceExtId)) {
      file.cswp_Workspace__r = new cswp_Workspace__c(
        cswp_ExternalId__c = this.workspaceExtId
      );
    }
    if (this.parentRootFolderOfWorkspace != null) {
      file.cswp_Folder__r = new cswp_Folder__c(
        cswp_ExternalId__c = this.parentRootFolderOfWorkspace.cswp_ExternalId__c
      );
    } else if (populateParentFolder) {
      file.cswp_Folder__r = (driveItem.parentReference != null
        ? new cswp_Folder__c(cswp_ExternalId__c = driveItem.parentReference?.id)
        : null);
    }
    return file;
  }
}