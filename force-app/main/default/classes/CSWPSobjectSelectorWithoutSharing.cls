public without sharing class CSWPSobjectSelectorWithoutSharing {
    
    @AuraEnabled
    public static List<Cswp_System__c> getSystemsWithSerialNumber(String serialNumber) {
        return [SELECT Id, Name, Cswp_As_Delivered_Condition__c, Cswp_Calibration_Box_Data__c, Cswp_Host_Name__c,
                Cswp_Rhel_Version__c, Cswp_Serial_Number__c, Cswp_Smt_Version__c, Cswp_Tasks_Start_Date__c,
                Cswp_Calibration_Box_Data__r.Cswp_Certificate_Version__c, Cswp_Calibration_Box_Data__r.Cswp_Operator_Name__c, 
                Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c, Cswp_Calibration_Box_Data__r.Cswp_Temperature__c, Cswp_Calibration_Box_Data__r.Cswp_Humidity__c,
                Cswp_Calibration_Box_Data__r.Cswp_Is_Archived__c,Cswp_Calibration_Box_Data__r.Cswp_Account__c, Cswp_Calibration_Box_Data__r.Cswp_Sync_With_Install_Base__c,
                Cswp_Calibration_Box_Data__r.Cswp_Group__c, Cswp_Calibration_Box_Data__r.Cswp_Created_From_Cswp_Portal__c, Cswp_Calibration_Box_Data__r.Cswp_Calibration_Due_Date__c,
                Cswp_Calibration_Box_Data__r.Cswp_Calibration_Status__c, Cswp_Calibration_Box_Data__r.Cswp_Days_Left_To_Calibration_Expire__c,
                Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c
                FROM Cswp_System__c WHERE Cswp_Serial_Number__c =: serialNumber ORDER BY Cswp_Calibration_Box_Data__r.Cswp_Certificate_Version__c DESC];
    }

}