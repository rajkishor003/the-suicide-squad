public with sharing class CSWPSystemTriggerHandler implements Triggers.Handler,Triggers.AfterUpdate,Triggers.AfterInsert {
    
    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }

    public static void AfterUpdate(Triggers.Context context) {
        if(context.props.isChanged(Cswp_System__c.Cswp_Serial_Number__c)) {
            CSWPSystemTriggerHandler.HandleReportNumberUpdate(context);
        }
    }

    public static void AfterInsert(Triggers.Context context) {
        CSWPSystemTriggerHandler.HandleReportNumberUpdate(context);
    }

    public static void HandleReportNumberUpdate(Triggers.Context context) {
        Set<String> cbdIds = new Set<String>();
        for(Sobject sys : context.props.newMap.values()) {
            cbdIds.add(((Cswp_System__c)sys).Cswp_Calibration_Box_Data__c);
        }
        List<Cswp_Calibration_Box_Data__c> cbdList = CSWPSobjectSelector.getCalBoxDataWithIdSet(cbdIds);
        Map<String,Cswp_Calibration_Box_Data__c> cbdMap = new Map<String,Cswp_Calibration_Box_Data__c>();
        for(Cswp_Calibration_Box_Data__c cbd : cbdList) {
            cbdMap.put(cbd.Id, cbd);
        }

        List<Cswp_Calibration_Box_Data__c> list2Upd = new List<Cswp_Calibration_Box_Data__c>();
        for(Sobject sys : context.props.newMap.values()) {
            Cswp_System__c castedSys = (Cswp_System__c)sys;            
            cbdMap.get(castedSys.Cswp_Calibration_Box_Data__c).Cswp_Report_Number__c = castedSys.Cswp_Serial_Number__c+'_'+String.valueOf(cbdMap.get(castedSys.Cswp_Calibration_Box_Data__c).Cswp_Generation_Date__c).replace('-','').replace(':','').deleteWhitespace();
            list2Upd.add(cbdMap.get(castedSys.Cswp_Calibration_Box_Data__c));
        }
        update list2Upd;
    }
}