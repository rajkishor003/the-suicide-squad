@isTest
public with sharing class CSWPTestUtil {


      //Returns Serialized DriveItem Json.
      public static String createDriveFolderForTest(String itemName,String id ,String parentPath){
        SharepointService.DriveItem item = new SharepointService.DriveItem();
        item.id = id;
        item.name = itemName;
        item.size = 0;
        item.folder = new SharepointService.FolderFacet();
        item.folder.childCount = 1;
        item.createdDateTime = DateTime.now();
        item.lastModifiedDateTime = DateTime.now();
        item.parentReference = new SharepointService.ParentFacet();
        item.parentReference.driveId = 'b!60W5xPolmUiTd8j-3XP31OxumWvhn1BBu1IvgkxT3RoFRkZ6a3wuQ7TA4bibANnN';
        item.parentReference.driveType = 'folder';
        item.parentReference.path = '/drives/b!60W5xPolmUiTd8j-3XP31OxumWvhn1BBu1IvgkxT3RoFRkZ6a3wuQ7TA4bibANnN/root:/client-documents' + parentPath;
        return JSON.serialize(item);
    }

    public static cswp_AppConfig__c createAppConfig(String name, String dataType, String description){
        cswp_AppConfig__c config = new cswp_AppConfig__c();
        config.Name = name;
        config.DataType__c = dataType;
        config.Description__c = description;
        return config;
    }

    public static Account createAccount(){
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        return testAccount;
    }
    public static cswp_GroupPermission__c createCswpGroupPermission(Boolean createFolder, Boolean createSubFolder, Boolean deleteflag, Boolean download, String groupMemberId, String type, Boolean upload){
        cswp_GroupPermission__c cswpGroupPermission = new cswp_GroupPermission__c();
        cswpGroupPermission.cswp_CreateFolder__c = createFolder;
        cswpGroupPermission.cswp_CreateSubFolder__c = createSubFolder;
        cswpGroupPermission.cswp_Delete__c = deleteflag;
        cswpGroupPermission.cswp_Download__c = download;
        cswpGroupPermission.cswp_GroupMemberId__c = groupMemberId;
        cswpGroupPermission.cswp_Type__c = type;
        cswpGroupPermission.cswp_Upload__c = upload;
        return cswpGroupPermission;
    }
    public static cswp_Group__c createCswpGroup (Account customer, String description, cswp_GroupPermission__c groupPermission, String publicGroupId) {
		cswp_Group__c cswpGroup = new cswp_Group__c();
		cswpGroup.cswp_Customer__c = customer.Id;
        cswpGroup.cswp_Description__c = description;
        cswpGroup.cswp_GroupPermission__c = groupPermission.Id;
        cswpGroup.cswp_PublicGroupId__c = publicGroupId;
		return cswpGroup;
    }
    
    public static cswp_Group__c createCswpGroup (String name, Account customer, String description) {
        cswp_Group__c cswpGroup = new cswp_Group__c();
        cswpGroup.Group_Name__c = name;
		cswpGroup.cswp_Customer__c = customer.Id;
        cswpGroup.cswp_Description__c = description;
		return cswpGroup;
    }
    
    public static cswp_Workspace__c createCswpWorkspace(String name, Account account) {
        return new cswp_Workspace__c(
            Name = name,
            cswp_Account__c = account.Id
        );
    }

    public static cswp_Folder__c createCswpFolder(String name, Decimal contentSize, cswp_Workspace__c cswpWorkspace) {
        return new cswp_Folder__c(
            Name = name,
            cswp_ContentSize__c = contentSize,
            cswp_IsRoot__c = true,
            cswp_Workspace__c = cswpWorkspace.Id
        );
    }


    public static cswp_Folder__c createSubFolder(String name, cswp_Folder__c folder, Decimal contentSize){
        return new cswp_Folder__c(
            Name = name,
            cswp_ParentFolder__c = folder.Id,
            cswp_ContentSize__c = contentSize
        );
    }

    public static cswp_AccountPreference__c createAccountPreference(Account account) {
        return new cswp_AccountPreference__c(
            cswp_Account__c = account.Id,
            cswp_Active__c = true
        );
    }

    public static cswp_File__c createCswpFile(String name, cswp_Workspace__c workspace, cswp_Folder__c folder, User uploader){
        return new cswp_File__c(
            Name = name,
            cswp_Workspace__c = workspace.Id,
            cswp_Folder__c = folder.Id,
            cswp_UploadedDateTime__c = System.today(),
            cswp_Last_Downloaded_Time__c = System.today(),
            cswp_UploadedBy__c = uploader.Id,
            cswp_Status__c = 'Approved'
        );
    }
    
    public static Knowledge__kav createFAQKnowledge(String title, String question, String answer, String dataCategory, String language) {
        Id faqRecordId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('FAQ').getRecordTypeId();
        return new Knowledge__kav(
            Title = title,
            UrlName = title,
            Language = language,
            RecordTypeId = faqRecordId,
            Question__c = question,
            Answer__c = answer,
            CSWP_Data_Category__c = dataCategory
        );
    }

    public static User createPortalUser(Account acc) {
        User communityUser = new User();
        Contact c = new Contact(LastName = 'Contact Last Name', AccountId = acc.id, Email = 'test@test.com');
        insert c;

        communityUser.ProfileID = [Select Id From Profile Where Name='CSWP Community User'].id;
        communityUser.EmailEncodingKey = 'ISO-8859-1';
        communityUser.LanguageLocaleKey = 'en_US';
        communityUser.TimeZoneSidKey = 'America/New_York';
        communityUser.LocaleSidKey = 'en_US';
        communityUser.FirstName = 'first';
        communityUser.LastName = 'last';
        communityUser.Username = 'test@uniquedomain.com';
        communityUser.CommunityNickname = 'testUser123';
        communityUser.Alias = 't1';
        communityUser.Email = 'no@email.com';
        communityUser.IsActive = true;
        communityUser.ContactId = c.Id;

        return communityUser;
    }

    public static User createCommunityUser(Account acc){
        User communityUser = new User();
        
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];

        adminUsers[0].UserRoleId = userRole.Id;
        update adminUsers[0];
        User user = new User();
        System.runAs(adminUsers[0]){

            Contact c = new Contact(LastName = 'Contact Last Name', AccountId = acc.id);
            insert c;

            user.ProfileID = [Select Id From Profile Where Name='CSWP Community User'].id;
            user.EmailEncodingKey = 'ISO-8859-1';
            user.LanguageLocaleKey = 'en_US';
            user.TimeZoneSidKey = 'America/New_York';
            user.LocaleSidKey = 'en_US';
            user.FirstName = 'first';
            user.LastName = 'last';
            user.Username = 'test@uniquedomain.com';
            user.CommunityNickname = 'testUser123';
            user.Alias = 't1';
            user.Email = 'no@email.com';
            user.IsActive = true;
            user.ContactId = c.Id;

            communityUser = user;
        }
        return communityUser;
    }

    public static CSWP_Assigned_Permission_Set__c createAssignedPermissionSet(String name, cswp_Group__c cswpGroup, Id permissionSetId) {
        return new CSWP_Assigned_Permission_Set__c(
            Name = name,
            cswp_Group__c = cswpGroup.Id,
            cswp_Permission_Set_Id__c = permissionSetId
        );
    }

    public static ContentVersion createContentVersion(String title, String path, String dataB) {
        return new ContentVersion(
            title = title,
            PathOnClient = path+'.ZIP',
            versiondata = EncodingUtil.base64Decode(dataB)
        );
    }
    public static CSWP_Notification__c createNotificationWithGroup(String title, String content, Date startDate, Date endDate, cswp_Group__c cswpgroup,Boolean isActive, Boolean isLandingPage, Boolean mustConfirm,Boolean sendEmail, String RecordType){
        Id recordTypeId = Schema.SObjectType.cswp_Notification__c.getRecordTypeInfosByDeveloperName().get(RecordType).getRecordTypeId();
      
        return new CSWP_Notification__c( Name = title
                                        , cswp_Content_Message__c = content
                                        , cswp_Start_Date__c = startDate
                                        , cswp_End_Date__c= endDate
                                        , cswp_Group__c = cswpgroup.Id
                                        , cswp_is_Active__c = isActive
                                        , cswp_User_must_Confirm__c = mustConfirm
                                        , cswp_send_Email__c = sendEmail
                                        , RecordTypeId = recordTypeId );
    }
    public static CSWP_Notification__c createNotificationWithoutGroup(String title, String content, Date startDate, Date endDate,Boolean isActive, Boolean isLandingPage, Boolean mustConfirm, Boolean sendEmail, String RecordType){
        Id recordTypeId = Schema.SObjectType.cswp_Notification__c.getRecordTypeInfosByDeveloperName().get(RecordType).getRecordTypeId();
       
        return new CSWP_Notification__c( Name = title
                                        , cswp_Content_Message__c = content
                                        , cswp_Start_Date__c = startDate
                                        , cswp_End_Date__c= endDate 
                                        , cswp_is_Active__c = isActive
                                        , cswp_USer_must_Confirm__c = mustConfirm
                                        , cswp_send_Email__c = sendEmail
                                        , RecordTypeId = recordTypeId);
    }
    public static CSWP_Notification_Translation__c createNotificationTranslation(String language, String title , String content, cswp_Notification__c notification){
        return new CSWP_Notification_Translation__c( Name = title
                                                    , cswp_Content_Message__c = content
                                                    , cswp_Notification__c = notification.Id
                                                    , cswp_Language__c = language);
    }
}