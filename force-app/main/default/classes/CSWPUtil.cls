public class CSWPUtil {

    /**
     *  CSWP Related constant static variables
    */
    public static final String ON_AFTER_INSERT = 'onAfterInsert';
    public static final String ON_AFTER_UPDATE = 'onAfterUpdate';
    public static final String ON_BEFORE_INSERT = 'onBeforeInsert';
    public static final String ON_BEFORE_UPDATE = 'onBeforeUpdate';
    public static final String ON_AFTER_DELETE = 'onAfterDelete';

    public static final String CLIENTLIBRARYFOLDER_KEY = 'ClientLibraryFolder';
    public static final String PRODUCTLIBRARYFOLDER_KEY = 'ProductLibraryFolder';
    public static final String CLIENTWORKSPACE_RECORDTYPE= 'cswp_ClientDocument';
    public static final String PRODUCTWORKSPACE_RECORDTYPE = 'cswp_ProductDocument';
    public static final String ROOT_FOLDER_PRODUCT_LIB = 'RootFolder';
    public static final String CSWP_APP_MANAGER_PERM = 'CSWPAppUser';
    public static final String CSWP_APP_CUSTOMIZER_PERM = 'CSWPCustomizeApplication';
    public static final String CLIENT_DOCUMENT_RECORD_TYPE = 'Document Sharing';
    public static final String PRODUCT_DOCUMENT_RECORD_TYPE = 'Technical Documents';
    public static final String APPROVED = 'Approved';
    public static final String CSWP_SITE_PREFIX = '/cswp';

    public static final Map<String,String> GetConfigKeyByWorkspaceRecordType = new Map<String,String>{
        CLIENTWORKSPACE_RECORDTYPE => CLIENTLIBRARYFOLDER_KEY,
        PRODUCTWORKSPACE_RECORDTYPE => PRODUCTLIBRARYFOLDER_KEY
    };
    private static String cswpCommunityId {get;private set;}
    private static final String CSWP_PORTAL = 'CSWP Portal';
    private static Map<Id,RecordTypeInfo> recordTypes;

    public static void debugLog(Object message){
        System.debug(LoggingLevel.DEBUG, String.format('[CSWP] - [{0}] - {1}', new Object[]{Datetime.now().format(), message}));
    }

    public static void errorLog(Object message){
        System.debug(LoggingLevel.ERROR, String.format('[CSWP] - [{0}] - {1}', new Object[]{Datetime.now().format(), message}));
    }

    public static String getCswpCommunityId(){
        if(String.isNotBlank(cswpCommunityId)){
            return cswpCommunityId;
        }
        ConnectApi.CommunityPage cPage = ConnectApi.Communities.getCommunities();
        for(ConnectApi.Community comm : cPage.communities){
            if(comm.name == CSWP_PORTAL){
                cswpCommunityId = comm.id;
            }
        }
        return cswpCommunityId;
    }

    public static Object getCommunityDetail(){
        String networkId = '';
        networkId = Network.getNetworkId();
        if(String.isBlank(networkId)){
            return null;
        }

        return ConnectApi.Communities.getCommunity(networkId);
    }

    /* @Deprecated
    public static String getWorkspaceRootPath(String recordTypeName){
        if(!GetConfigKeyByWorkspaceRecordType.containsKey(recordTypeName)){
            return null;
        }
        CSWPConfigurationManager config = CSWPConfigurationManager.getInstance();
        return (String)config.getConfigValue(GetConfigKeyByWorkspaceRecordType.get(recordTypeName));
    }*/

    /* @Deprecated
    public static String getRecordTypeNameById(Schema.SObjectType objectType, String recordTypeId){
        if(recordTypes == null){
            recordTypes = objectType.getDescribe().getRecordTypeInfosById(); 
        }
        return recordTypes.get(recordTypeId).developername;
    }*/

    public static String encodeUrlPath(String path){
        if(String.isBlank(path)){
            return '';
        }
        List<String> splitted = path.split('/');
        for (Integer i = 0; i < splitted.size(); i++) {
            splitted[i] = EncodingUtil.urlEncode(splitted[i], 'utf-8').replaceAll('\\+','%20');
        }
        return String.join(splitted, '/');
    }

    public static Decimal convertByteToGigabyte(Long byteValue) {
        return 
            Decimal.valueOf(byteValue).divide(Math.pow(1024, 3), 2,RoundingMode.HALF_EVEN);
    }
    public static Decimal convertByteToMegabyte(Long byteValue) {
        return 
            Decimal.valueOf(byteValue).divide(Math.pow(1024, 2), 2, RoundingMode.HALF_EVEN);

    }
    public static Decimal convertByteToKilobyte(Long byteValue) {
        return Decimal.valueOf(byteValue).divide(1024,2,RoundingMode.HALF_EVEN);
    }
    /* @Deprecated
    public static void cacheValueInOrg(String key, Object value){
        if(String.isBlank(key)){
            return;
        }
        Cache.Org.put('local.CSWP.'+ removeNonAlphaNumericChars(key), value);    
    }
    @Deprecated
    public static Object getOrgCacheValue(String key){
        String cswpLocalKey = 'local.CSWP.' + removeNonAlphaNumericChars(key);
        if(Cache.Org.contains(cswpLocalKey)){
            return Cache.Org.get(cswpLocalKey);
        }else{
            return null;
        }
    }

    @Deprecated 
    public static void cacheValueInSession(String key, Object value){
        if(String.isBlank(key)){
            return;
        }
        Cache.Session.put('local.CSWP.'+ removeNonAlphaNumericChars(key), value);    
    }

    @Deprecated 
    public static Object getSessionCacheValue(String key){
        
        String cswpLocalKey = 'local.CSWP.' + removeNonAlphaNumericChars(key);
        if(Cache.Session.contains(cswpLocalKey)){
            return Cache.Session.get(cswpLocalKey);
        }else{
            return null;
        }
    }*/

    /* @Deprecated
    static String removeNonAlphaNumericChars(String key){
        Pattern nonAlphanumericPattern = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumericPattern.matcher(key);
        if(matcher.find()){
            key = matcher.replaceAll('');
        }
        return key;
    }*/

    public static String getSessionInfoByName(String authSessionKey){
        Map<String,String> session =  Test.isRunningTest() ? null : Auth.SessionManagement.getCurrentSession();
        if(session!=null && session.containsKey(authSessionKey)){
            return session.get(authSessionKey);
        }
        return null;
    }

    public static Boolean  isCSWPAdminUser(){
        return (FeatureManagement.checkPermission(CSWP_APP_MANAGER_PERM) || FeatureManagement.checkPermission(CSWP_APP_CUSTOMIZER_PERM));
    }

    public static Boolean isCommunityUser(){
        String userType = UserInfo.getUserType();
        return (userType == 'CustomerSuccess' || userType == 'PowerCustomerSuccess');
    }

    @AuraEnabled(cacheable=true)
    public static String getBaseURL() {
        return URL.getOrgDomainUrl().toExternalForm();
    }

    public static Datetime convertToUserTime(Datetime dt){
        if(dt == null) {
            return null;
        }
        Integer offset = UserInfo.getTimeZone().getOffset(dt);
        DateTime convertedDateTime = dt.addSeconds( offset/ 1000);
        return convertedDateTime;
    }

    public static String formatDateTime (DateTime dt) {
        if(dt == null) {
            return null;
        }
        return dt.format('dd MMM yyyy, HH:mm', UserInfo.getTimeZone().getID());
    }

    public static List<List<String>> splitArrayIntoChunks(List<String> originalList, Integer chunkSize){ 
        if(originalList==null || originalList.isEmpty() || chunkSize < 1 ) {
            return null;
        }
        List<String> copyList = originalList.clone();
        List<List<String>> result = new List<List<String>>();
        Integer numberOfChunks = Decimal.valueOf(originalList.size()).divide(chunkSize,2).round(System.RoundingMode.CEILING).intValue();
        integer loopn = 0;
        integer counter = 0;
        // System.debug(LoggingLevel.DEBUG, 'numberOfChunks:>>' +numberOfChunks );
        // System.debug(LoggingLevel.DEBUG, 'copyList.size():>>' +copyList.size() );
        for (Integer i = 0; i < numberOfChunks; i++) {   
            List<String> arr = new List<String>();
            while ( (loopn < chunkSize) && (counter <= copyList.size() -1)  ) {
                arr.add(copyList[counter]);
                loopn++;
                counter++;
            }
            result.add(arr);
            loopn = 0;
        }
        return result;
    }
    @future(callout = true)
    public static void handleFeedItemBody(Set<Id> feedIds){
        Set<Id> caseIds = new Set<Id>();
        Map<Id,String> caseToFeedBody = new Map<Id,String>();
        Map<Id,Id> caseToFeedOwner = new Map<Id,Id> ();

        List<FeedItem> updatedFeedItems = new List<FeedItem>();
        List<FeedItem> feedItemList = [SELECT Id,ParentId,Body,CreatedById FROM FeedItem WHERE ID IN : feedIds];
        for(FeedItem fi : feedItemList) {
            //if(fi.Body.contains('[JIRA]')) {
                fi.Body = fi.Body.replaceAll('<i>(.*?)</i>', '');
                Map<String, String> params = new Map<String,String>();
                params.put('rendererType','atlassian-wiki-renderer');
                params.put('unrenderedMarkup',fi.Body.stripHTMLTags());
                params.put('issueKey','');
                String body = JSON.serialize(params);
                HttpRequest request = new HttpRequest();
                request.setHeader('Content-Type', 'application/json');
                request.setEndpoint('callout:Advantest_Jira/rest/api/1.0/render');
                request.setMethod('POST');
                request.setBody(body);
                Http ht = new Http();
                Httpresponse response = ht.send(request);
                if(response.getStatusCode() == 200 && response.getStatus() == 'OK'){
                    fi.Body = response.getBody().escapeHtml4() +'#jiracomment';
                    updatedFeedItems.add(fi);
                    caseIds.add(fi.ParentId);
                    caseToFeedBody.put(fi.ParentId,response.getBody());
                    caseToFeedOwner.put(fi.ParentId,fi.CreatedById);
                }
            //}
        }

        if(!updatedFeedItems.isEmpty()) {
            update updatedFeedItems;
            sendNotificationToCaseOwner(caseIds,caseToFeedBody,caseToFeedOwner);
        }
    }
    
    public static void sendNotificationToCaseOwner(Set<Id> caseIds , Map<Id,String> caseToFeedBody,Map<Id,Id> caseToFeedOwner ) {
        if(!caseIds.isEmpty() && !caseToFeedBody.isEmpty() && !caseToFeedOwner.isEmpty()) {
            List<orgwideemailaddress> orgWideList = [Select id from orgwideemailaddress limit 1];
            Id senderId = null;
            Id communityProfileId = null;

            if(!orgWideList.isEmpty()) {
                senderId = orgWideList[0].Id;
            }

            List<Profile> cswpProfileList = [SELECT Id FROM Profile WHERE NAME = 'CSWP Community User' LIMIT 1];

            if(!cswpProfileList.isEmpty()) {
                communityProfileId = cswpProfileList[0].Id;
            }

            List<Site> cswpSiteList = [SELECT Id, Name, Subdomain, UrlPathPrefix, GuestUserId FROM Site where UrlPathPrefix = 'cswp'];

            if(!cswpSiteList.isEmpty() && senderId != null && communityProfileId != null) {
                Site s = cswpSiteList[0];
                String communityUrl = [SELECT Id, DurableId, IsRegistrationEnabled, SecureUrl FROM SiteDetail WHERE DurableId =: s.Id].SecureUrl;
                for( Case c : [SELECT id,Contact.Name, Subject,CaseNumber, OwnerId,owner.Email , owner.ProfileId FROM case WHERE id in : caseIds AND Cswp_Jira_Project__c != null]) {
                    if(c.Owner.profileId == communityProfileId && c.OwnerId != caseToFeedOwner.get(c.id)) {
                        String htmlBody = '<p>Hi'+ c.Contact.Name+',</p><p>A new comment is available:</p>'+caseToFeedBody.get(c.Id)+'<p>Case Number : #'+c.CaseNumber+'</p><p>View Details : '+communityUrl+'/s/case/'+c.id+'</p>';
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.OrgWideEmailAddressId = senderId;
                        message.toAddresses = new String[] { c.Owner.Email};
                        message.optOutPolicy = 'FILTER';
                        message.subject = 'You have a new comment #' +c.CaseNumber ;
                        message.HtmlBody = htmlBody;
                        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
                        if (results[0].success) {
                            System.debug('The email was sent successfully.');
                        } else {
                            System.debug('The email failed to send: ' + results[0].errors[0].message);
                        }
                    }
                }
            }
        }
    }
}