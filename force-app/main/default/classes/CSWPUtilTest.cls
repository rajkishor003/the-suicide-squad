@IsTest
private class CSWPUtilTest {
    @IsTest
    private static void utilTests(){
        List<String> testList = new List<String>();
        testList.add('String 1');
        testList.add('String 2');
        testList.add('String 3');
        testList.add('String 4');
        testList.add('String 5');
        testList.add('String 6');
        System.assertEquals(3, CSWPUtil.splitArrayIntoChunks(testList,2).size());
        System.assertEquals('/test%20util/path%20test', CSWPUtil.encodeUrlPath('/test util/path test'),'Path tested.');
        System.assertEquals(1.00, CSWPUtil.convertByteToGigabyte((Long)Math.pow(1024,3)));
        System.assertEquals(1.00, CSWPUtil.convertByteToMegabyte(1024*1024));
        System.assertEquals(1.00, CSWPUtil.convertByteToKilobyte(1024));
        System.assertEquals(null, CSWPUtil.getSessionInfoByName('SourceIp'));
        System.assertEquals(false, CSWPUtil.isCommunityUser());
        System.assertNotEquals(null, CSWPUtil.getBaseURL(),'Not null');
        System.assertNotEquals(null, CSWPUtil.convertToUserTime(Datetime.now() ),'Not null');
        System.assertNotEquals(null, CSWPUtil.formatDateTime(Datetime.now()),'Not null');
        CSWPUtil.isCSWPAdminUser();
    }
}