/*************************************************************************************************************
 * @name			CSWPWorkspaceManagerController
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			02 / 12 / 2020
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-12-02		oguzalp					Changes desription
 *
 **************************************************************************************************************/
public with sharing class CSWPWorkspaceManagerController {
  
  public static Map<String, Object> userInfoObject;

  @AuraEnabled(cacheable=true)
  public static List<SObject> getWorkspaces(String workspaceType) {
    List<cswp_Workspace__c> workspaces = new List<cswp_Workspace__c>();
    try {
      if (String.isBlank(workspaceType)) {
        return workspaces;
      }
      return CSWPSobjectSelector.getWorkspacesByType(workspaceType);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled //(cacheable=true)
  public static List<CSWPContent.DriveItem> getWorkspaceItems( String workspaceId, String workspaceType ) {
    List<CSWPContent.DriveItem> items;
    try {
      if (workspaceType == CSWPUtil.PRODUCT_DOCUMENT_RECORD_TYPE) {
        items = CSWPSobjectSelector.getFilesByWorkspaceId(workspaceId);
      }
      if (workspaceType == CSWPUtil.CLIENT_DOCUMENT_RECORD_TYPE) {
        items = CSWPSobjectSelector.getClientDocumentItems(workspaceId);
      }
      // for(CSWPContent.DriveItem item : items){
      //   item.createdDate = CSWPUtil.convertToUserTime(item.createdDate);
      // }
      return items;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  //@AuraEnabled(cacheable=true)
  public static List<CSWPContent.DriveItem> getFolderItems(String folderId) {
    try {
      List<CSWPContent.DriveItem> folderItems;
      folderItems = CSWPSobjectSelector.getItemsByFolder(folderId);
      return folderItems;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  //ParentId might be cswpFolderId or cswpWorkspaceId
  @AuraEnabled
  public static List<Object> uploadFilesToRemote( String parentId, List<String> documentIds ) {
    try {
      List<ContentVersion> files = CSWPSobjectSelector.getContentFilesByDocumentId(
        documentIds
      );
      List<cswp_File__c> cswpFiles;
      if (!files.isEmpty()) {
        cswpFiles = CSWPRepositoryManager.uploadFilesToFolderOrWorkspace(
          Id.valueOf(parentId),
          files
        );
      }
      return cswpFiles;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static List<CSWPContent.DriveItem> searchWorkspaceItems( String workspaceId, String searchText ) {
    List<CSWPContent.DriveItem> result = new List<CSWPContent.DriveItem>();
    try {
      if (String.isBlank(searchText) || String.isBlank(workspaceId)) {
        return result;
      }
      Id workId = Id.valueOf(workspaceId);
      result.addAll(
        CSWPSObjectSelector.searcFoldersInWorkspace(workspaceId, searchText)
      );
      result.addAll(
        CSWPSObjectSelector.searchFilesInWorkspace(workspaceId, searchText)
      );
      return result;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static List<CSWPContent.DriveItem> recentlyViewedItems() {
    try {
      List<CSWPContent.DriveItem> recentItems = new List<CSWPContent.DriveItem>();
      Set<Id> folderIds = new Set<Id>();
      Set<Id> fileIds = new Set<Id>();

      for (RecentlyViewed rv : [
        SELECT Id, Name, Type
        FROM RecentlyViewed
        WHERE
          LastViewedDate != NULL
          AND Type IN ('cswp_Folder__c', 'cswp_File__c')
        ORDER BY LastViewedDate, Type DESC, Name ASC
        LIMIT 20
      ]) {
        if (rv.Type == 'cswp_Folder__c') {
          folderIds.add(rv.Id);
        } else {
          //cswp_File__c
          fileIds.add(rv.Id);
        }
      }
      for (
        CSWP_Folder__c folder : CSWPSobjectSelector.getRecentFolders(folderIds)
      ) {
        recentItems.add(new CSWPContent.DriveItem(folder));
      }
      for (
        CSWP_File__c file : CSWPSobjectSelector.getCswpFilesByIdSet(fileIds)
      ) {
        recentItems.add(new CSWPContent.DriveItem(file));
      }
      return recentItems;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static Boolean eventLog(CSWPActivity.EventMessage message) {
    try {
      if (!CSWPUtil.isCommunityUser()) {
        return false;
      }
      //This will publish cswp Activity Log Event and flow will create CSWP_Activity__c record
      Database.SaveResult saveResult = new CSWPActivity(message).publishEvent();
      return saveResult.isSuccess();
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static Map<String, Object> getCurrentUserInfo( String workspaceId, String workspaceType ) {
    CSWPContent.Permission perm = initAdminPermission();
    try {
      userInfoObject = new Map<String, Object>{
        'isAdmin' => CSWPUtil.isCSWPAdminUser(),
        'isCustomer' => CSWPUtil.isCommunityUser(),
        'userId' => UserInfo.getUserId(),
        'userType' => UserInfo.getUserType(),
        'community' => null
      };
      if (CSWPUtil.isCommunityUser()) {
        userInfoObject.put('community', CSWPUtil.getCommunityDetail());
        if (
          String.isNotBlank(workspaceType) &&
          workspaceType == CSWPUtil.PRODUCT_DOCUMENT_RECORD_TYPE
        ) {
          // if the workspace is product document, community users can only download
          perm.canDownload = true;
          perm.canCreateFolder = false;
          perm.canDelete = false;
          perm.canCreateSubFolder = false;
          perm.canRename = false;
          perm.canUpload = false;
          userInfoObject.put('permissions', perm);
          //Break and return here!
          return userInfoObject;
        }
        if (String.isNotBlank(workspaceId)) {
          userInfoObject.put(
            'permissions',
            CSWPSobjectSelector.getUserPermissionsByWorkspaceId(workspaceId)
          );
        }
      } else if (CSWPUtil.isCSWPAdminUser()) {
        //Give the full access for now!TBD??
        userInfoObject.put('permissions', perm);
      }
      return userInfoObject;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static Map<String, Object> userPermissionByFile(String fileId) {
    try {
      String workspaceId;
      cswp_File__c file = CSWPSobjectSelector.getCswpFilesByIdSet(
          new Set<Id>{ Id.valueOf(fileId) }
        )
        .get(0);
      workspaceId = String.isNotBlank(file.cswp_Workspace__c)
        ? file.cswp_Workspace__c
        : file.cswp_Folder__r.cswp_Workspace__c;
      return CSWPWorkspaceManagerController.getCurrentUserInfo(
        workspaceId,
        CSWPUtil.CLIENT_DOCUMENT_RECORD_TYPE
      );
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static String createUploadSession( String cswpFileId, Decimal fileSize ) {
    try {
      String externalId;
      cswp_File__c file = CSWPSobjectSelector.getCswpFilesByIdSet(
          new Set<Id>{ Id.valueOf(cswpFileId) }
        )
        .get(0);
      SharepointService service = SharepointService.init();
      if (String.isBlank(file.cswp_ExternalId__c)) {
        SharepointService.DriveItem item = service.getDriveItemByPath(
          file.cswp_FilePath__c
        );
        if (item == null && String.isBlank(item.id)) {
          throw new AuraHandledException('Original file could not be found!');
        }
        externalId = item.id;
      } else {
        externalId = file.cswp_ExternalId__c;
      }
      String url = service.createUploadSession(externalId, file.Name, fileSize);
      CSWPUtil.debugLog(' UploadSessionURL => ' + url);
      return url;
    } catch (Exception e) {
      throw e;
      //throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=true)
  public static List<SharepointService.FileVersion> getFileVersions( String fileId ) {
    List<SharepointService.FileVersion> versions;
    try {
      String fileExternalId = CSWPRepositoryManager.getFileExternalId(fileId);
      if (String.isNotBlank(fileExternalId)) {
        SharepointService service = SharepointService.init();
        versions = service.getItemVersionList(fileExternalId);
      }
      return versions;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static String getFileVersionDownloadUrl( String fileId, String versionNo ) {
    String downlodUrl = '';
    try {
      String externalId = CSWPRepositoryManager.getFileExternalId(fileId);
      if (String.isNotBlank(externalId)) {
        SharepointService service = SharepointService.init();
        if (versionNo != null && String.isNotBlank(versionNo)) {
          downlodUrl = service.getItemVersionDownloadUrl(externalId, versionNo);
        } else {
          downlodUrl = service.getDownloadUrl(externalId);
        }
      }
      return downlodUrl;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  private static CSWPContent.Permission initAdminPermission() {
    CSWPContent.Permission adminPerm = new CSWPContent.Permission();
    adminPerm.canDelete = true;
    adminPerm.canCreateFolder = true;
    adminPerm.canCreateSubFolder = true;
    adminPerm.canDownload = true;
    adminPerm.canRename = true;
    adminPerm.canUpload = true;

    return adminPerm;
  }

   
	@AuraEnabled
	public static void handleWorkspaceSubscription(String userId, String workspaceId, String operation) {
		try {
			if(operation == 'Create') {
				Cswp_Workspace_Subscription__c wssub = new Cswp_Workspace_Subscription__c();
				wssub.Cswp_Subscriber__c = userId;
				wssub.Cswp_Workspace__c = workspaceId;
				database.insert(wssub);
			}
			else {
				List<Cswp_Workspace_subscription__c> wsSubList = CSWPSobjectSelector.getWorkspaceSubscriptions(userId, workspaceId);
				delete wsSubList;
			}      
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static Boolean getWorkspaceSynched(String workspaceId) {
		try {
			CSWPUtil.debugLog('check Sync Workspace ==> '+workspaceId);
			List<CSWP_Workspace__c> wsList = CSWPSobjectSelector.getWorkspacesByIdSet(new Set<Id> {workspaceId} );
			return (wsList.size()>0 && wsList[0].cswp_IsSync__c == true && wsList[0].cswp_ExternalId__c != null && wsList[0].cswp_LastSyncTime__c != null && wsList[0].cswp_HubItemId__c != null);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static void handleWorkspaceSync(String workspaceId) {
		try {
			CSWPRepositoryManager.createRepositorFolderByWorkspaceAsync(new Set<ID> {workspaceId});
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}