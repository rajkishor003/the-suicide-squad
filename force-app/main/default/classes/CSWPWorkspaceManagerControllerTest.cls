@isTest
private with sharing class CSWPWorkspaceManagerControllerTest {
    @TestSetup
    static void makeData(){
        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];
        List<Account> accList = new List<Account>();
        User testuser;
        GroupMember member;
        cswp_Group__c cswpGroup;
        System.runAs(adminUsers[0]){
            Account clientAcc = new Account(Name = 'Test Client Account');
            Account productAcc = new Account(Name = 'Test Product Account');
            accList.add(clientAcc);
            accList.add(productAcc);
            insert accList;
            
            testuser = CSWPTestUtil.createCommunityUser(accList[0]);
            insert testuser;
        
            cswpGroup = CSWPTestUtil.createCswpGroup('TestGroup',accList[0],'Test Description');
            insert cswpGroup;
            cswp_Group__c cswpGroup2 = CSWPTestUtil.createCswpGroup('TestGroup2',accList[0],'Test Description');
            insert cswpGroup2;
            cswp_Group__c cswpGrp = [SELECT Id,cswp_PublicGroupId__c,Group_name__c FROM cswp_Group__c WHERE Group_name__c = 'TestGroup' lIMIT 1];
        
            member                      = new GroupMember();
            member.UserOrGroupId        = testuser.Id;
            member.GroupId              = cswpGrp.cswp_PublicGroupId__c;
            insert member;
        
            cswp_GroupPermission__c cswpGrpPerm = CSWPTestUtil.createCswpGroupPermission(true,true,true,true,member.Id,'Regular',true);
            insert cswpGrpPerm;
            cswp_User_Permission__c cswpUserPerm = new cswp_User_Permission__c();
            cswpUserPerm.cswp_Group__c = cswpGroup.Id;
            cswpUserPerm.cswp_User__c = testuser.Id;
            cswpUserPerm.CSWP_Group_Permission__c = cswpGrpPerm.Id;
            insert cswpUserPerm;
        }
        List<cswp_Workspace__c> workSpaceList = new List<cswp_Workspace__c>();
        Id wsRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Document Sharing' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        Id wsProductRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Technical Documents' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        cswp_Workspace__c cswpClientWorkSpace = CSWPTestUtil.createCswpWorkspace('TestClient Workspace',accList[0]);
        cswpClientWorkSpace.RecordTypeId = wsRecordTypeId;
        cswpClientWorkSpace.cswp_Group__c = cswpGroup.Id;
        workSpaceList.add(cswpClientWorkSpace);
        cswp_Workspace__c cswpProductWorkSpace = CSWPTestUtil.createCswpWorkspace('TestProduct Workspace',accList[1]);
        cswpProductWorkSpace.RecordTypeId = wsProductRecordTypeId;
        workSpaceList.add(cswpProductWorkSpace);
        insert workSpaceList;
        List<cswp_Folder__c> folderList = new List<cswp_Folder__c>();
        cswp_Folder__c cswpClientFolder = CSWPTestUtil.createCswpFolder('TestClient Folder',2.5,workSpaceList[0]);
        folderList.add(cswpClientFolder);
        cswp_Folder__c cswpProductFolder = CSWPTestUtil.createCswpFolder('TestProduct Folder',1.5,workSpaceList[1]);
        folderList.add(cswpProductFolder);
        insert folderList;
    
        List<cswp_AppConfig__c>  cswpConfigList = new List<cswp_AppConfig__c> ();
        cswp_AppConfig__c cswpConfig0 = CSWPTestUtil.createAppConfig('SharepointSiteId','String','Test SiteId');
        cswpConfig0.Value__c = 'testmyadvantest.sharepoint.com';
        cswpConfigList.add(cswpConfig0);
        cswp_AppConfig__c cswpConfig1 = CSWPTestUtil.createAppConfig('SharepointSiteHost','String','Test SiteHost');
        cswpConfig1.Value__c = 'testmyadvantest.com';
        cswpConfigList.add(cswpConfig1);
        cswp_AppConfig__c cswpConfig2 = CSWPTestUtil.createAppConfig('SharepointLibraryPath','String','Test LibraryPath');
        cswpConfig2.Value__c = 'sites/testSalesforcefilerepositorydevtest';
        cswpConfigList.add(cswpConfig2);
        cswp_AppConfig__c cswpConfig3 = CSWPTestUtil.createAppConfig('CswpDriveId','String','Test CSWPDriveId');
        cswpConfig3.Value__c = 'b!testDriveId';
        cswpConfigList.add(cswpConfig3);
        cswp_AppConfig__c cswpConfig4 = CSWPTestUtil.createAppConfig('CswpLibraryName','String','Test CswpLibraryName');
        cswpConfig4.Value__c = 'testcswp-library';
        cswpConfigList.add(cswpConfig4);
        cswp_AppConfig__c cswpConfig5 = CSWPTestUtil.createAppConfig('ProductLibraryFolder','String','Test ProductLibraryFolder');
        cswpConfig5.Value__c = 'testproduct-documents';
        cswpConfigList.add(cswpConfig5);
        cswp_AppConfig__c cswpConfig6 = CSWPTestUtil.createAppConfig('ClientLibraryFolder','String','Test ClientLibraryFolder');
        cswpConfig6.Value__c = 'testclient-documents';
        cswpConfigList.add(cswpConfig6);
        cswp_AppConfig__c cswpConfig7 = CSWPTestUtil.createAppConfig('TestInteger','Integer','Added for Test Coverage');
        cswpConfig7.Value__c = '2';
        cswpConfigList.add(cswpConfig7);
        cswp_AppConfig__c cswpConfig8 = CSWPTestUtil.createAppConfig('TestDecimal','Decimal','Added for Test Coverage');
        cswpConfig8.Value__c = '2.22';
        cswpConfigList.add(cswpConfig8);
        cswp_AppConfig__c cswpConfig9 = CSWPTestUtil.createAppConfig('TestBoolean','Boolean','Added for Test Coverage');
        cswpConfig9.Value__c = 'true';
        cswpConfigList.add(cswpConfig9);
        cswp_AppConfig__c cswpConfig10 = CSWPTestUtil.createAppConfig('TestDate','Date','Added for Test Coverage');
        cswpConfig10.Value__c = '2020-10-01';
        cswpConfigList.add(cswpConfig10);
        cswp_AppConfig__c cswpConfig11 = CSWPTestUtil.createAppConfig('TestDateTime','DateTime','Added for Test Coverage');
        cswpConfig11.Value__c = String.valueOf(System.now());
        cswpConfigList.add(cswpConfig11);
        cswp_AppConfig__c cswpConfig12 = CSWPTestUtil.createAppConfig('TestAnotherType','AnotherType','Added for Test Coverage');
        cswpConfig12.Value__c = 'TestAnotherType';
        cswpConfigList.add(cswpConfig12);
        insert cswpConfigList;

        
        cswp_File__c cswpFile = CSWPTestUtil.createCswpFile('TestCSWPFile.pdf',workSpaceList[0],folderList[0],adminUsers[0]);
        cswpFile.cswp_FilePath__c = 'client-documents/V93000/TestCSWPFile.pdf';
        insert cswpFile;
        cswp_AccountPreference__c accPref = CSWPTestUtil.createAccountPreference(accList[0]);
        accPref.cswp_StorageLimit__c = 100.00;
        accPref.cswp_Current_Storage__c = 10.00;
        insert accPref;
        List<ContentVersion> cvList = new List<ContentVersion> ();
        ContentVersion contentVersion = new ContentVersion(
            Title = 'TestDoc',
            PathOnClient = 'TestDoc.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,
            FirstPublishLocationId = folderList[0].Id
        );
        cvList.add(contentVersion);
        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'TestDoc2',
            PathOnClient = 'TestDoc2.pdf',
            VersionData = Blob.valueOf('Test Contentt'),
            IsMajorVersion = true,
            FirstPublishLocationId = folderList[0].Id
        );
        cvList.add(contentVersion2);
        insert cvList;
        System.debug('cvList =>>' + cvList);
        Id conDocId = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Title = 'TestDoc' LIMIT 1].ContentDocumentId;
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = accList[0].id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        insert cdl;
        
    }

    @isTest
    private static void workspaceManager(){
        Id wsRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Document Sharing' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        Id wsProductRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Technical Documents' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        List<cswp_AccountPreference__c> ap = [SELECT Id, cswp_Account__r.Name, cswp_Active__c FROM cswp_AccountPreference__c];
        cswp_Workspace__c wsClient = [SELECT Id,Name,cswp_InternalParentPath__c FROM cswp_Workspace__c WHERE Name = 'TestClient Workspace' AND RecordTypeId = :wsRecordTypeId LIMIT 1];
        cswp_Workspace__c wsProduct = [SELECT Id,Name,cswp_InternalParentPath__c FROM cswp_Workspace__c WHERE Name = 'TestProduct Workspace' AND RecordTypeId =:wsProductRecordTypeId LIMIT 1];
        cswp_Folder__c clientFolder = [SELECT Id FROM cswp_Folder__c WHERE Name = 'TestClient Folder' LIMIT 1];
        cswp_File__c cswpFile = [SELECT Id FROM cswp_File__c WHERE Name = 'TestCSWPFile.pdf' LIMIT 1];
        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];
        List<String> docIds = new List<String> ();
        List<ContentVersion> cvList = [SELECT Id,Title,ContentDocumentId,PathOnClient FROM ContentVersion];
        for(ContentVersion cv : cvList){
            docIds.add(cv.ContentDocumentId);
        }
        Test.startTest();
        CSWPWorkspaceManagerController.getWorkspaces('Document Sharing');
        CSWPWorkspaceManagerController.getWorkspaceItems(wsClient.Id,'Document Sharing');
        CSWPWorkspaceManagerController.getWorkspaceItems(wsProduct.Id,'Technical Documents');
        CSWPWorkspaceManagerController.getFolderItems(clientFolder.Id);
        CSWPWorkspaceManagerController.searchWorkspaceItems(wsClient.Id,'TestClient');
        CSWPWorkspaceManagerController.recentlyViewedItems();
        String uploadFileErrMessage = null;
        User testCommunityUser = [SELECT Id,profile.name FROM User where profile.name = 'CSWP Community User' LIMIT 1];
        User commTestUser = [SELECT Id,profile.name,username FROM User where username = 'test@uniquedomain.com' LIMIT 1];
        System.runAs(adminUsers[0]){
            Id myPermissionSetId = [select id , name from PermissionSet where name ='CSWP_PortalUser' LIMIT 1].Id;
            PermissionSetAssignment psa = new PermissionSetAssignment
            (PermissionSetId = myPermissionSetId, AssigneeId = commTestUser.Id);
            insert psa;  
        }

        wsProduct.OwnerId = commTestUser.Id;
        update wsProduct;
        System.runAs(commTestUser){
            
            CSWPActivity.EventMessage em = new CSWPActivity.EventMessage();
            em.itemName = cvList[0].title;
            em.itemId = cvList[0].Id;
            em.parentItemName = cvList[0].PathOnClient;
            em.message = 'Test event message';
            em.actionName = 'Delete';
            Boolean isEventLog = CSWPWorkspaceManagerController.eventLog(em);
            String userInfoErrorMessage = null;
            CSWPWorkspaceManagerController.getCurrentUserInfo(wsClient.Id,'Technical Documents');
            CSWPWorkspaceManagerController.getCurrentUserInfo(wsClient.Id,'');
           
        }
        CSWPWorkspaceManagerController.userPermissionByFile(cswpFile.Id);
       
        Test.stopTest();
    }

    @isTest
    private static void getFileVersionsTest(){
        cswp_File__c cswpFile = [SELECT Id,Name,cswp_ExternalId__c FROM cswp_File__c WHERE Name = 'TestCSWPFile.pdf' LIMIT 1];
        cswpFile.cswp_ExternalId__c = '01NOCE2SIMKVIFV';
        update cswpFile;
        SharepointService.FileVersion version = new SharepointService.FileVersion();
        version.id = '1.0';
        version.size = 13;
        version.lastModifiedDateTime = Datetime.now().format();
        Map<String,Object> bodyMap = new Map<String,Object>{
            'value'  => new List<SharepointService.FileVersion>{ version }
        };
        Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(JSON.serialize(bodyMap),200));
        Test.startTest();
        CSWPWorkspaceManagerController.getFileVersions(cswpFile.Id);
        Test.stopTest();
    }

    @isTest
    private static void getFileVersionDownloadUrlTest(){
        cswp_File__c cswpFile = [SELECT Id,Name,cswp_ExternalId__c FROM cswp_File__c WHERE Name = 'TestCSWPFile.pdf' LIMIT 1];
        cswpFile.cswp_ExternalId__c = '01NOCE2SIMKVIFV';
        update cswpFile;
        Test.startTest();
        CSWPWorkspaceManagerController.getFileVersionDownloadUrl(cswpFile.Id, '1.0');
        Test.stopTest();
    }
    

    @isTest
    private static void checkNullParameters(){
        Id wsRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Document Sharing' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        cswp_Workspace__c wsClient = [SELECT Id ,Name FROM cswp_Workspace__c WHERE Name = 'TestClient Workspace' AND RecordTypeId =:wsRecordTypeId LIMIT 1];
        cswp_File__c cswpFile = [SELECT Id,cswp_ExternalId__c FROM cswp_File__c WHERE Name = 'TestCSWPFile.pdf' LIMIT 1];
        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];
        cswp_AppConfig__c siteIdConfig = [SELECT Id,Name,Value__c FROM cswp_AppConfig__c WHERE Name = 'SharepointSiteId' LIMIT 1];
        siteIdConfig.Value__c = '';
        update siteIdConfig;
        cswp_AppConfig__c cswpDriveId = [SELECT Id,Name,Value__c FROM cswp_AppConfig__c WHERE Name = 'CswpDriveId' LIMIT 1];
        cswpDriveId.Value__c = '';
        update cswpDriveId;
        Test.startTest();
        CSWPWorkspaceManagerController.getWorkspaces(null);
        CSWPWorkspaceManagerController.searchWorkspaceItems('','test');
        CSWPActivity.EventMessage em = new CSWPActivity.EventMessage();
            em.itemName = 'Test';
            em.itemId = null;
            em.parentItemName = '';
            em.message = 'Not Community User';
            em.actionName = 'Delete';
            Boolean isEventLog = CSWPWorkspaceManagerController.eventLog(em);
            System.runAs(adminUsers[0]){
                CSWPWorkspaceManagerController.getCurrentUserInfo(wsClient.Id,'Technical Documents');
            }
        Test.stopTest();
    }

    @isTest
    private static void createUploadSessionTest(){
        cswp_File__c cswpFile = [SELECT Id,cswp_ExternalId__c FROM cswp_File__c WHERE Name = 'TestCSWPFile.pdf' LIMIT 1];
        cswp_Workspace__c wsClient = [SELECT Id ,Name FROM cswp_Workspace__c WHERE Name = 'TestClient Workspace' LIMIT 1];
        String jsonBody = CSWPTestUtil.createDriveFolderForTest(wsClient.Name,'01NOCE2SIMKVIFV','');
        Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(jsonBody,200));
        Test.startTest();
        CSWPWorkspaceManagerController.createUploadSession(cswpFile.Id,2.3);
        Test.stopTest();

    }


    @isTest
    private static void updateCSWPFile(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(CSWPTestUtil.createDriveFolderForTest('test-cswpworkspacefolder-1','01NOCE2SIMKVIFV',''),200));
        cswp_Group__c cswpGroup2 = [SELECT Id,cswp_PublicGroupId__c, Group_name__c FROM cswp_Group__c  WHERE Group_name__c = 'TestGroup2' LIMIT 1];
        Id wsRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Document Sharing' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        List<User> adminUsers = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = true];
        cswp_Workspace__c wsClient = [SELECT Id,Name FROM cswp_Workspace__c WHERE Name = 'TestClient Workspace' AND RecordTypeId =:wsRecordTypeId LIMIT 1];            
        cswp_File__c cswpFile = [SELECT Id,Name,cswp_ExternalId__c,cswp_Status__c FROM cswp_File__c WHERE Name = 'TestCSWPFile.pdf' LIMIT 1];
        cswpFile.cswp_Status__c = 'Rejected';
        cswpFile.Name = 'UpdatedTestCSWPFile.pdf';
        cswpFile.cswp_FileExtension__c='pdf';
        update cswpFile;
        try{
            delete [SELECT Id,Name,cswp_ExternalId__c FROM cswp_File__c WHERE Name = 'UpdatedTestCSWPFile.pdf' LIMIT 1];
        }catch(Exception ex){
            CSWPUtil.errorLog(ex.getMessage());
        }
        cswp_Folder__c cswpClientFolder = [SELECT Id,Name,cswp_Path__c FROM cswp_Folder__c WHERE Name = 'TestClient Folder' LIMIT 1];
        cswpClientFolder.cswp_Path__c = 'client-documents/V93000';
        cswpClientFolder.Name = 'UpdatedTestClient Folder';
        
        
        update cswpClientFolder;
        cswp_Folder__c cswpProdFolder = [SELECT Id,Name,cswp_Path__c FROM cswp_Folder__c WHERE Name = 'TestProduct Folder' LIMIT 1];
        cswp_File__c cswpFile2 = CSWPTestUtil.createCswpFile('TestCSWPFile2.pdf',wsClient,cswpClientFolder,adminUsers[0]);
        cswpFile2.cswp_FilePath__c = 'client-documents/V93000/TestCSWPFile2.pdf';
        cswpFile2.cswp_FileSize__c = 8;
        insert cswpFile2;
        try{
            delete [SELECT Id,cswp_Path__c FROM cswp_Folder__c WHERE Name = 'UpdatedTestClient Folder' LIMIT 1];
        }catch(Exception e){
            CSWPUtil.errorLog(e.getMessage());
        }
        wsClient.Name = 'UpdatedTestClient Workspace';
        wsClient.cswp_Group__c = cswpGroup2.Id;
        cswpClientFolder.cswp_Path__c = 'client-documents';
        update wsClient;
        //HttpResponse responseFriends = new HttpResponse();
        //String body = '{"name" : "UpdatedTestClient Workspace"}';
        //Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(body,200));
        Test.stopTest();
    }

    @isTest
    private static void testSubscription() {
        Test.startTest();
        cswp_Workspace__c wsClient = [SELECT Id ,Name FROM cswp_Workspace__c WHERE Name = 'TestClient Workspace' LIMIT 1];
        User testCommunityUser = [SELECT Id,profile.name FROM User where profile.name = 'CSWP Community User' LIMIT 1];

        CSWPWorkspaceManagerController.handleWorkspaceSubscription(testCommunityUser.Id, wsClient.Id, 'Create');
        CSWPWorkspaceManagerController.handleWorkspaceSubscription(testCommunityUser.Id, wsClient.Id, 'Delete');
        Test.stopTest();
    }

}