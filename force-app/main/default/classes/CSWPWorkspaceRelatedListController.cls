public with sharing class CSWPWorkspaceRelatedListController {
    public static final String FIELDS_PARAM = 'fields';
    public static final String NUMBER_OF_RECORDS_PARAM = 'numberOfRecords';
    public static final String RECORD_ID_PARAM = 'recordId';
    public static final String SOBJECT_API_NAME_PARAM = 'sobjectApiName';
    public static final String SOBJECT_LABEL_PARAM = 'sobjectLabel';
    public static final String SOBJECT_LABEL_PLURAL_PARAM = 'sobjectLabelPlural';
    public static final String PARENT_RELATIONSHIP_API_NAME_PARAM = 'parentRelationshipApiName';
    public static final String RELATED_FIELD_API_NAME_PARAM = 'relatedFieldApiName';
    public static final String SORTED_DIRECTION_PARAM = 'sortedDirection';
    public static final String SORTED_BY_PARAM = 'sortedBy';
    public static final String CD_RECORDS_PARAM = 'cdrecords';
    public static final String TD_RECORDS_PARAM = 'tdrecords';
    public static final String ICON_NAME_PARAM = 'iconName';

    @AuraEnabled
    public static String initData(String jsonData){
        system.debug(jsonData);
        Map<String, Object> requestMap = (Map<String, Object>)JSON.deserializeUntyped(jsonData);  
        Map<String, Object> responseMap = new Map<String, Object>();
        String recordId = (String)requestMap.get(RECORD_ID_PARAM);
        responseMap.put(CD_RECORDS_PARAM, getCdRecords(jsonData, recordId));
        responseMap.put(TD_RECORDS_PARAM, getTdRecords(jsonData, recordId));
        
        //String sobjectApiName = 'cswp_Workspace__c';
        String sobjectApiName = (String)requestMap.get(SOBJECT_API_NAME_PARAM);
        responseMap.put(ICON_NAME_PARAM, getIconName(sobjectApiName));
        String relatedFieldApiName = (String)requestMap.get(RELATED_FIELD_API_NAME_PARAM);
        responseMap.put(PARENT_RELATIONSHIP_API_NAME_PARAM, getParentRelationshipName(recordId, sobjectApiName, relatedFieldApiName));

        Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(sobjectApiName);
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
        responseMap.put(SOBJECT_LABEL_PARAM, sobjectDescribe.getLabel());
        responseMap.put(SOBJECT_LABEL_PLURAL_PARAM, sobjectDescribe.getLabelPlural());
        return JSON.serialize(responseMap);
    }
    
	@AuraEnabled
    public static List<Sobject> getCdRecords(String jsonData, String groupId){
		Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
        Set<Id> groupIds = new Set<Id>{groupId};
        groupIds.addAll(CSWPWorkspaceRelatedListController.getLinkedGroups(groupId));
		Integer numberOfRecords = 10;
        String cdrecordId = 'cswp_ClientDocument';        
        String query = 'Select Id, Name, cswp_WorkspaceType__c, RecordType.DeveloperName, cswp_Group__c From cswp_Workspace__c WHERE cswp_Group__c IN: groupIds AND RecordType.DeveloperName = :cdrecordId ORDER BY ID DESC Limit :numberOfRecords';
        return Database.query(String.escapeSingleQuotes(query));
	}

    @AuraEnabled
    public static List<Sobject> getTdRecords(String jsonData, String groupId){
		Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
        Set<Id> groupIds = new Set<Id>{groupId};
        groupIds.addAll(CSWPWorkspaceRelatedListController.getLinkedGroups(groupId));
		Integer numberOfRecords = 10;
        String tdrecordId = 'cswp_ProductDocument';        
        String query = 'Select Id, Name, cswp_WorkspaceType__c, RecordType.DeveloperName, cswp_Group__c From cswp_Workspace__c WHERE cswp_Group__c IN: groupIds AND RecordType.DeveloperName = :tdrecordId ORDER BY ID DESC Limit :numberOfRecords';
        return Database.query(String.escapeSingleQuotes(query));
	}

    private static Set<Id> getLinkedGroups(String cswpGroupId) {
        Set<Id> linkedgroupMasterIds = new Set<Id>();
        List<cswp_Group_links__c> masterlinks = CSWPSobjectSelector.getCswpGroupLinksOfLinkedGroup(cswpGroupId);
        for(cswp_Group_links__c gl : masterlinks) {
            linkedgroupMasterIds.add(gl.Cswp_Master_Group__c);
        }
        return linkedgroupMasterIds;
    }
        
    private static String getParentRelationshipName(Id recordId, String childSobjectApiName, String relatedFieldApiName){
        Schema.DescribeSObjectResult descrRes = recordId.getSObjectType().getDescribe();
        String name;
        for (Schema.ChildRelationship cr: descrRes.getChildRelationships()){ 
            if(cr.getChildSObject().getDescribe().getName() == childSobjectApiName
            && cr.getField().getDescribe().getName() == relatedFieldApiName){
          	 	name = cr.getRelationshipName();
                break;
            }
        }     
        return name;
    }      
    
    @AuraEnabled
    public static String getIconName(String sobjectApiName){
        String iconName;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.DescribeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();

        for(Schema.DescribeTabSetResult tsr : tabSetDesc) { 
            tabDesc.addAll(tsr.getTabs()); 
        }

        for(Schema.DescribeTabResult tr : tabDesc) {
            if( sobjectApiName == tr.getSobjectName() ) {
                if( tr.isCustom() ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    iconName = 'standard:' + sobjectApiName.toLowerCase();
                }
            }
        }
        for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml'){
                iconName = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                break;
            }
        }
        return iconName;
    }    
    
}