@isTest
public with sharing class CSWPWorkspaceRelatedListController_Test {
    @isTest public static void testInit(){
        Account acc = new Account(Name = 'test');
        insert acc;
        Contact cnt = new Contact(LastName = 'test', AccountId = acc.Id);
        insert cnt;
        cswp_GroupPermission__c groupPermission = CSWPTestUtil.createCswpGroupPermission(true, true, true, true, 'Test', 'Test', true);
        insert groupPermission;
        PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
        cswp_Group__c cswpGroup = CSWPTestUtil.createCswpGroup('Test Group', acc, 'Test Description');
        cswpGroup.cswp_GroupPermission__c = groupPermission.Id;

        insert cswpGroup;
        CSWP_Assigned_Permission_Set__c assignedPermSet = CSWPTestUtil.createAssignedPermissionSet('testAssignedPermission',cswpgroup,permSet.Id);
        insert assignedPermSet;
        Id recordTypeId = Schema.SObjectType.cswp_Workspace__c.getRecordTypeInfosByDeveloperName().get('cswp_ClientDocument').getRecordTypeId();

        cswp_Workspace__c workspace = CSWPTestUtil.createCswpWorkspace('test',acc);
        workspace.cswp_Group__c = cswpGroup.Id;
        workspace.RecordTypeId = recordTypeId;
        insert workspace;
   
        Map<String, Object> requestMap = new Map<String, Object>(); 
        requestMap.put(CSWPWorkspaceRelatedListController.FIELDS_PARAM, 'Name');
        requestMap.put(CSWPWorkspaceRelatedListController.RELATED_FIELD_API_NAME_PARAM, 'cswp_Group__c');
        requestMap.put(CSWPWorkspaceRelatedListController.RECORD_ID_PARAM, cswpGroup.Id);
        requestMap.put(CSWPWorkspaceRelatedListController.NUMBER_OF_RECORDS_PARAM, 1);
        requestMap.put(CSWPWorkspaceRelatedListController.SOBJECT_API_NAME_PARAM, 'cswp_Workspace__c');
        requestMap.put(CSWPWorkspaceRelatedListController.SORTED_BY_PARAM, 'Name');
        requestMap.put(CSWPWorkspaceRelatedListController.SORTED_DIRECTION_PARAM, 'ASC');
                
        String jsonData = CSWPWorkspaceRelatedListController.initData(JSON.serialize(requestMap));
        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
        List<Object> records = (List<Object>)responseMap.get(CSWPWorkspaceRelatedListController.CD_RECORDS_PARAM);
        System.assert(!records.isEmpty());
        String iconName = (String)responseMap.get(CSWPWorkspaceRelatedListController.ICON_NAME_PARAM);
        System.assert(String.isNotBlank(iconName));
    }
}