public with sharing class CSWP_AssignPermissionSetController {
    
    // @AuraEnabled (cacheable=true)
    // public static List<PermissionSetWrapper> getPermissionSets(String cswpGroupId) {
    //     Set<String> permissionIdSet = new Set<String> ();
    //     for (CSWP_Assigned_Permission_Set__c assignedPermissionSet : [SELECT Id, cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Set__c WHERE cswp_Group__c = :cswpGroupId AND cswp_Permission_Set_Id__c != null]) {
    //         permissionIdSet.add(assignedPermissionSet.cswp_Permission_Set_Id__c);
    //     }

    //     List<PermissionSetWrapper> wrapperList = new List<PermissionSetWrapper> ();
    //     for(PermissionSet permissionSet : [SELECT Id, Label FROM PermissionSet WHERE (Id NOT IN :permissionIdSet) AND (NOT Label LIKE '00%') AND License.Name = 'Customer Community Plus' ORDER BY Label ASC]) {
    //         wrapperList.add(new PermissionSetWrapper(permissionSet.Label, permissionSet.Id));
    //     }
    //     if(Test.isRunningTest()) {
    //         for(PermissionSet permissionSet : [SELECT Id, Label FROM PermissionSet WHERE Name = 'CSWP_PortalUser']) {
    //             wrapperList.add(new PermissionSetWrapper(permissionSet.Label, permissionSet.Id));
    //         }
    //     }
    //     return wrapperList;
    // }

    @AuraEnabled (cacheable=true)
    public static List<PermissionSet> getPermissionSetList(String cswpGroupId) {
        Set<String> permissionIdSet = new Set<String> ();
        for (CSWP_Assigned_Permission_Set__c assignedPermissionSet : [SELECT Id, cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Set__c WHERE cswp_Group__c = :cswpGroupId AND cswp_Permission_Set_Id__c != null]) {
            permissionIdSet.add(assignedPermissionSet.cswp_Permission_Set_Id__c);
        }

        List<PermissionSet> psList = new List<PermissionSet> ();
        for(PermissionSet permissionSet : [SELECT Id, Label, Name FROM PermissionSet WHERE (Id NOT IN :permissionIdSet) AND (NOT Label LIKE '00%') AND (License.Name = 'Customer Community Plus') AND (NAME LIKE 'CSWP_%') ORDER BY Label ASC]) {
            psList.add(permissionSet);
        }
        return psList;
    }

    @AuraEnabled (cacheable=true)
    public static List<PermissionSet> getPermissionSetListAfterSave(String cswpGroupId, List<String> apsList) {
        Set<String> permissionIdSet = new Set<String> ();
        for (CSWP_Assigned_Permission_Set__c assignedPermissionSet : [SELECT Id, cswp_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Set__c WHERE cswp_Group__c = :cswpGroupId AND cswp_Permission_Set_Id__c != null]) {
            permissionIdSet.add(assignedPermissionSet.cswp_Permission_Set_Id__c);
        }
        for(String aps : apsList) {
            permissionIdSet.add(aps);
        }

        List<PermissionSet> psList = new List<PermissionSet> ();
        for(PermissionSet permissionSet : [SELECT Id, Label, Name FROM PermissionSet WHERE (Id NOT IN :permissionIdSet) AND (NOT Label LIKE '00%') AND (License.Name = 'Customer Community Plus') AND (NAME LIKE 'CSWP_%') ORDER BY Label ASC]) {
            psList.add(permissionSet);
        }
        return psList;
    }




    @AuraEnabled
    public static List<String> assignPermissionSet(String selectedPermissionSets, String cswpGroupId) {

        List<Object> lst_JsonParse = (List<Object>)Json.deserializeUntyped(selectedPermissionSets);
        Set<String> psIdSet = new Set<String>();
        Set<String> apsIdSet = new Set<String>();

        for(Object obj : lst_JsonParse) {
            psIdSet.add((String)((Map<String,Object>)obj).get('Id'));
        }

        List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE Id IN:psIdSet];
        cswp_Group__c cswpGroup = [SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c WHERE Id = :cswpGroupId];
        Set<String> userIdSet = new Set<String> ();
        if(String.isBlank(cswpGroup.cswp_PublicGroupId__c)) {
            return null;
        }

        for(GroupMember user : [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId = :cswpGroup.cswp_PublicGroupId__c]) {
            userIdSet.add(user.UserOrGroupId);
        }

        if(Test.isRunningTest()) {
            User user = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com'];
            userIdSet.add(user.Id);
        }

        List<CSWP_Assigned_Permission_Set__c> apsToInsert = new List<CSWP_Assigned_Permission_Set__c>();
        for(PermissionSet ps : permissionSetList) {

            CSWP_Assigned_Permission_Set__c assignedPermissionSet = new CSWP_Assigned_Permission_Set__c(
                cswp_Permission_Set_Id__c = ps.Id,
                Name = ps.Label,
                cswp_Group__c = cswpGroupId
            );

            apsToInsert.add(assignedPermissionSet);
        }       

        insert apsToInsert;
        for(CSWP_Assigned_Permission_Set__c aps : apsToInsert) {
            apsIdSet.add(aps.Id);
        }

        // if(!userIdSet.isEmpty()) {
        //     assignPermissions(psIdSet, userIdSet);
        // }
        return new List<String>(apsIdSet);
    }



    // public class PermissionSetWrapper {
    //     @AuraEnabled
    //     public String psName {get;set;}
    //     @AuraEnabled
    //     public String psId {get;set;}

    //     public PermissionSetWrapper(String psName, String psId) {
    //         this.psName = psName;
    //         this.psId = psId;
    //     }
    // }

    // private static void assignPermissions(Set<String> permissionSetIds, Set<String> userIdSet) {
    //     List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment> ();
    //     for(String userId : userIdSet) {
    //         for(String psId : permissionSetIds) {
    //             assignmentList.add(new PermissionSetAssignment(
    //                 PermissionSetId = psId,
    //                 AssigneeId = userId
    //             ));
    //         }            
    //     }
    //     if(!assignmentList.isEmpty()) {
    //         Database.insert(assignmentList, false);
    //     }
    // }
}