@IsTest
public with sharing class CSWP_AssignPermissionSetController_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name ='Test Account');
        insert acc;
        User testuser = CSWPTestUtil.createCommunityUser(acc);
        insert testuser;
        cswp_Group__c cswpgroup = CSWPTestUtil.createCswpGroup('test group',acc,'test group');
        insert cswpgroup;
    }

    @IsTest
    public static void testGetPermissionSets() {
        CSWP_Group__c cswpGroup = [SELECT Id, Name,Group_name__c FROM CSWP_Group__c WHERE Group_name__c = 'test group'] ;
        PermissionSet permissionSet = [SELECT Id, Label FROM PermissionSet WHERE Name = 'CSWP_PortalUser' LIMIT 1];
        List<PermissionSet> wrapperList = CSWP_AssignPermissionSetController.getPermissionSetList(cswpGroup.Id);
    }

    @IsTest
    public static void testAssignPermissionSet() {
        CSWP_Group__c cswpGroup = [SELECT Id, Name,Group_name__c FROM CSWP_Group__c WHERE Group_name__c = 'test group'];
        PermissionSet permissionSet = [SELECT Id, Label FROM PermissionSet WHERE Name = 'CSWP_PortalUser' LIMIT 1];
        Test.startTest();
        String psIdJson =  '[{"Label":"CSWP Portal User","Id":"'+permissionSet.Id+'"}]';
        List<String> assignedPermissionSetId = CSWP_AssignPermissionSetController.assignPermissionSet(psIdJson, cswpGroup.Id);

        Test.stopTest();

        CSWP_Assigned_Permission_Set__c assignedPermissionSet = [SELECT Id, cswp_Permission_Set_Id__c, Name, cswp_Group__c FROM CSWP_Assigned_Permission_Set__c WHERE Id IN:assignedPermissionSetId];
        System.assertEquals(assignedPermissionSet.Name, permissionSet.Label);
        System.assertEquals(assignedPermissionSet.cswp_Permission_Set_Id__c, permissionSet.Id);
        System.assertEquals(assignedPermissionSet.cswp_Group__c, cswpGroup.Id);
    }
}