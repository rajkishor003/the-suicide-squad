/*************************************************************************************************************
 * @name			CSWP_AssignedPermissionSet_Handler
 * @author			ozguneser <o.eser@emakina.com.tr>
 * @created			06 / 01 / 2021
 * @description		Trigger Handler for CSWP Assigned Permission Set Object
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2021-01-06		oeser					Removed Permission Set when Assigned Permission Set is deleted
 *              1.1     2021-09-09      eakkus                  Assigned Permission set to Users of CSWP Group when assigned Permission set record is created.
 *
**************************************************************************************************************/

public with sharing class CSWP_AssignedPermissionSet_Handler implements Triggers.Handler, Triggers.AfterDelete, Triggers.AfterInsert {
    public Boolean criteria(Triggers.context context) {
        return true;
    }

    public void afterDelete(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWP_AssignedPermissionSet_Handler.class.getName(), CSWPUtil.ON_AFTER_DELETE);
        if(isEnabled){
            then(context);
        }
    }

    public void afterInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWP_AssignedPermissionSet_Handler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    private void then(Triggers.context context) {
        if(context.props.isAfter && context.props.isDelete) {
            List<CSWP_Assigned_Permission_Set__c> cswpAssignedPermissionSetList = (List<CSWP_Assigned_Permission_Set__c>) context.props.oldList;
            //Map<String, Set<String>> permissionSetIdToUserIdMap = new Map<String, Set<String>> ();
            Set<String> permissionIdSet = new Set<String> ();
            Set<String> cswpGroupIdSet = new Set<String> ();
            Set<String> publicGroupIdSet = new Set<String> ();
            Set<String> userIdSet = new Set<String> ();
            Map<String, Set<String>> publicGroupIdToUserSetMap = new Map<String, Set<String>> ();
            Set<String> permissionSetAssignmentIdSet = new Set<String> ();
            String testUserId = '';
            if(Test.isRunningTest()) {
                testUserId = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com'].Id;
            }
            for(CSWP_Assigned_Permission_Set__c assignedPermissionSet : cswpAssignedPermissionSetList) {
                cswpGroupIdSet.add(assignedPermissionSet.cswp_Group__c);
                permissionIdSet.add(assignedPermissionSet.cswp_Permission_Set_Id__c);
            }
            Map<Id, cswp_Group__c> cswpGroupMap = new Map<Id, cswp_Group__c> ([SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c WHERE Id IN :cswpGroupIdSet AND cswp_PublicGroupId__c != null]);
            for(cswp_Group__c cswpGroup : cswpGroupMap.values()) {
                publicGroupIdSet.add(cswpGroup.cswp_PublicGroupId__c);
            }

            for(GroupMember user : [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN :publicGroupIdSet]) {
                if(!publicGroupIdToUserSetMap.containsKey(user.GroupId)) {
                    publicGroupIdToUserSetMap.put(user.GroupId, new Set<String> ());
                }
                publicGroupIdToUserSetMap.get(user.GroupId).add(user.UserOrGroupId);
                userIdSet.add(user.UserOrGroupId);
            }

            if(Test.isRunningTest()) {
                for(cswp_Group__c cswpGroup : cswpGroupMap.values()) {
                    publicGroupIdToUserSetMap.put(cswpGroup.cswp_PublicGroupId__c, new Set<String> {testUserId});
                }
                userIdSet.add(testUserId);
            }

            for(PermissionSetAssignment permissionSetAssignment : [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId IN :permissionIdSet OR AssigneeId = :userIdSet]) {
                for(CSWP_Assigned_Permission_Set__c assignedPermissionSet : cswpAssignedPermissionSetList) {
                    cswp_Group__c cswpGroup = cswpGroupMap.get(assignedPermissionSet.cswp_Group__c);
                    if(permissionSetAssignment.PermissionSetId == assignedPermissionSet.cswp_Permission_Set_Id__c && publicGroupIdToUserSetMap.get(cswpGroup.cswp_PublicGroupId__c).contains(permissionSetAssignment.AssigneeId)) {
                        permissionSetAssignmentIdSet.add(permissionSetAssignment.Id);
                    }
                }
            }

            if(!permissionSetAssignmentIdSet.isEmpty()) {
                deletePermissionSetAssignments(permissionSetAssignmentIdSet);
            }
        }
        if(context.props.isAfter && context.props.isInsert) {
            Map<Id,CSWP_Assigned_Permission_Set__c> cswpAssignedPermissionSetMap = (Map<Id,CSWP_Assigned_Permission_Set__c>) context.props.newMap;
            insertPermissionSetAssignments(cswpAssignedPermissionSetMap.keySet());
        }
        context.stop();
    }

    @future
    private static void deletePermissionSetAssignments(Set<String> permissionSetAssignmentIdSet) {
        delete [SELECT Id FROM PermissionSetAssignment WHERE Id IN :permissionSetAssignmentIdSet];
    }

    @future // Created Future Method Because MIXED_DML inserted CSWP_Assigned_Permission_Set then inserted PermissionSetAssignment
    private static void insertPermissionSetAssignments(Set<Id> permissionSetAssignmentIdSet) {
        List<CSWP_Assigned_Permission_Set__c> cswpAssignedPermissionSetList = [SELECT Id, Name, cswp_Permission_Set_Id__c, cswp_Group__c FROM CSWP_Assigned_Permission_Set__c WHERE ID IN: permissionSetAssignmentIdSet];
        Set<Id> Aps_cswpGroupIds = new Set<Id>();
        Map<String, List<User>> group_UserListMap = new Map<String, List<User>>();
        for(CSWP_Assigned_Permission_Set__c aps : cswpAssignedPermissionSetList) {
            Aps_cswpGroupIds.add(aps.cswp_Group__c);
        }

        Map<String, List<String>> cswpGroup_UsersList_Map = CSWPSobjectSelector.getUsersOfCswpGroupByIdSet(Aps_cswpGroupIds);
        List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment> ();
        for(CSWP_Assigned_Permission_Set__c aps : cswpAssignedPermissionSetList) {
            if(cswpGroup_UsersList_Map.get(aps.cswp_Group__c)==null) continue;
            for(String userId : cswpGroup_UsersList_Map.get(aps.cswp_Group__c)) {
                assignmentList.add(new PermissionSetAssignment(
                    PermissionSetId = aps.cswp_Permission_Set_Id__c,
                    AssigneeId = userId
                ));            
            }
        }
        if(!assignmentList.isEmpty()) {
            Database.SaveResult[] srList = Database.insert(assignmentList, false);
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        //Cannot Create Log Because Of MIXED_DML
                        //CSWPLogManagement.createLogForCSWP(err.getMessage(), 'insertPermissionSetAssignments', false, 'PermissionSetAssignment', null );
                    }
                }
            }
        }
    }
}