@isTest
private with sharing class CSWP_AssignedPermissionSet_Handler_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name ='Test Account');
        insert acc;
        User testuser = CSWPTestUtil.createCommunityUser(acc);
        insert testuser;
        cswp_Group__c cswpgroup = CSWPTestUtil.createCswpGroup('test group',acc,'test group');
        insert cswpgroup;
        PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
        CSWP_Assigned_Permission_Set__c assignedPermSet = CSWPTestUtil.createAssignedPermissionSet('testAssignedPermission',cswpgroup,permSet.Id);
        insert assignedPermSet;
        
    }
    @isTest
    static void testdelete(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com' LIMIT 1];
        cswp_Group__c cswpgroup = [SELECT Id FROM cswp_Group__c LIMIT 1];
        PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser'];
        CSWP_Assigned_Permission_Set__c assignedPermSet = [SELECT Id, Name, CSWP_Permission_Set_Id__c FROM CSWP_Assigned_Permission_Set__c];
        createPermissionSetAssignment(testuser.Id, permSet.Id);
        

        Test.startTest();
        delete assignedPermSet;
        Test.stopTest(); 
    }

    @future
    private static void createPermissionSetAssignment(Id userId, Id permissionSetId) {
        insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permissionSetId);
    }
}