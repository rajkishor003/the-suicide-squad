public with sharing class CSWP_Batch_AccountFileStorage implements Database.Batchable<sObject>, Schedulable {
    
    String query;

    public CSWP_Batch_AccountFileStorage() {
        query = 'SELECT Id, (SELECT Id, cswp_TotalItemSize__c FROM Workspaces__r WHERE cswp_TotalItemSize__c != NULL AND cswp_TotalItemSize__c > 0),'
        + '(SELECT Id, cswp_Current_Storage__c FROM AccountPreferences__r WHERE cswp_Active__c = TRUE ORDER BY LastModifiedDate DESC NULLS LAST LIMIT 1) FROM Account';
    }

    public CSWP_Batch_AccountFileStorage(String filter) {
        query = 'SELECT Id, (SELECT Id, cswp_TotalItemSize__c FROM Workspaces__r WHERE cswp_TotalItemSize__c != NULL AND cswp_TotalItemSize__c > 0), (SELECT Id, cswp_Current_Storage__c FROM AccountPreferences__r WHERE cswp_Active__c = TRUE ORDER BY LastModifiedDate DESC NULLS LAST LIMIT 1) FROM Account WHERE ' + filter;
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(new CSWP_Batch_AccountFileStorage());
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<Account> accountList = (List<Account>) scope;
        List<cswp_AccountPreference__c> accountPreferenceList = new List<cswp_AccountPreference__c> ();
        for(Account account : accountList) {
            if(account.Workspaces__r.isEmpty() || account.AccountPreferences__r.isEmpty()) {
                continue;
            }
            Long totalSize = 0;
            for(cswp_Workspace__c workspace : account.Workspaces__r) {
                totalSize += (Long)workspace.cswp_TotalItemSize__c;
            }
            cswp_AccountPreference__c accountPreference = account.AccountPreferences__r[0];
            //TODO: Convert it to GB later on.
            accountPreference.cswp_Current_Storage__c = CSWPUtil.convertByteToGigabyte(totalSize);
            accountPreferenceList.add(accountPreference);
        }
        update accountPreferenceList;
    }

    public void finish (Database.BatchableContext bc) {}
}