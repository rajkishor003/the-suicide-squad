@IsTest
public class CSWP_Batch_AccountFileStorage_Test {

    @TestSetup
    static void createTestData(){
        CSWPFolderTriggerHandler.isEnabled = false;
        Account account = CSWPTestUtil.createAccount();
        insert account;

        cswp_AccountPreference__c accountPreference = CSWPTestUtil.createAccountPreference(account);
        insert accountPreference;

        cswp_Workspace__c workspace = CSWPTestUtil.createCswpWorkspace('Test Workspace', account);
        insert workspace;

        cswp_Folder__c folder = CSWPTestUtil.createCswpFolder('Test Folder', 107341824, workspace);
        insert folder;

        User user = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        cswp_File__c cswpFile = CSWPTestUtil.createCswpFile('Test File', workspace, folder, user);
        cswpFile.cswp_HubItemId__c = 'TestFile';
        cswpFile.cswp_FileSize__c = 107341824;
        insert cswpFile;
    }
    
    @IsTest
    public static void testAccountFileStorageBatch() {
        List<cswp_AccountPreference__c> ap = [SELECT Id, cswp_Account__r.Name, cswp_Active__c FROM cswp_AccountPreference__c];
        Lİst<cswp_Workspace__c> ws = [SELECT Id, cswp_Account__r.Name, cswp_TotalItemSize__c FROM cswp_Workspace__c];
        Test.startTest();
        CSWP_Batch_AccountFileStorage batchClass = new CSWP_Batch_AccountFileStorage();
        String jobId = System.schedule('Test Schedule', '0 0 23 * * ?', batchClass);
        SchedulableContext sc = null;
        batchClass.execute(sc);
        Test.stopTest();

        cswp_AccountPreference__c accountPreference = [SELECT Id, cswp_Current_Storage__c FROM cswp_AccountPreference__c];
        System.assertEquals(accountPreference.cswp_Current_Storage__c, CSWPUtil.convertByteToGigabyte(107341824));
    }

    @IsTest
    public static void testAccountFileStorageWithFilter() {
        Test.startTest();
        Database.executeBatch(new CSWP_Batch_AccountFileStorage('Name = \'Test Account\''));
        Test.stopTest();

        cswp_AccountPreference__c accountPreference = [SELECT Id, cswp_Current_Storage__c FROM cswp_AccountPreference__c];
        System.assertEquals(accountPreference.cswp_Current_Storage__c, CSWPUtil.convertByteToGigabyte(107341824));
    }
    
}