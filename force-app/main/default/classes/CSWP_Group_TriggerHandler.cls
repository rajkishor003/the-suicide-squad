public with sharing class CSWP_Group_TriggerHandler implements Triggers.Handler, Triggers.AfterInsert, Triggers.AfterUpdate, Triggers.AfterDelete {
    
    public Boolean criteria(Triggers.context context) {
        return true;
    }

    public void afterInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWP_Group_TriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void afterUpdate(Triggers.context context) {
        if(CSWPConfigurationManager.IsTriggerEnabled(CSWP_Group_TriggerHandler.class.getName(), CSWPUtil.ON_AFTER_UPDATE)) {
            then(context);
        }
    }

    public void afterDelete(Triggers.context context) {
        if(CSWPConfigurationManager.IsTriggerEnabled(CSWP_Group_TriggerHandler.class.getName(), CSWPUtil.ON_AFTER_DELETE)) {
            then(context);
        }
    }

    private void then(Triggers.context context) {
        if(context.props.isAfter && context.props.isInsert) {
            List<cswp_Group__c> cswpGroupList = (List<cswp_Group__c>) context.props.newList;
            Map<String, Group> nameToGroupMap = new Map<String, Group> ();
            for(cswp_Group__c cswpGroup : cswpGroupList) {
                Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
                String trimmedGroupName = (cswpGroup.Name).deleteWhitespace();
                Matcher matcher = nonAlphanumeric.matcher(trimmedGroupName);
                trimmedGroupName = matcher.replaceAll('');
                if(cswpGroup.Name.length() >= 40) {
                    nameToGroupMap.put(cswpGroup.Name, new Group(Name = cswpGroup.Name.substring(0,38) + '..', DeveloperName = trimmedGroupName));
                }
                else {
                    nameToGroupMap.put(cswpGroup.Name, new Group(Name = cswpGroup.Name, DeveloperName = trimmedGroupName));
                }
            }
            insert nameToGroupMap.values();
            List<cswp_Group__c> cgList = [select id ,Name, cswp_PublicGroupId__c from cswp_Group__c where Name IN : nameToGroupMap.keySet()];
            for(cswp_Group__c cg : cgList) {
                cg.cswp_PublicGroupId__c = nameToGroupMap.get(cg.Name).Id;
            }
            update cgList;
        } else if(context.props.isAfter && context.props.isUpdate) {
            Map<Id, cswp_Group__c> newGroupMap = (Map<Id, cswp_Group__c>) context.props.newMap;
            Map<Id, cswp_Group__c> oldGroupMap = (Map<Id, cswp_Group__c>) context.props.oldMap;
            Map<String, Set<String>> groupIdToAddedUserId = new Map<String, Set<String>> ();
            Map<String, Set<String>> groupIdToRemovedUserId = new Map<String, Set<String>> ();

            List<Id> changeGroupIdSet = context.props.filterChanged(cswp_Group__c.cswp_Users__c);
            //System.debug('changeGroupIdSet -----> ' + changeGroupIdSet);
            for(Id groupId : changeGroupIdSet) {
                Set<String> addedUserIdSet = new Set<String> ();
                Set<String> removedUserIdSet = new Set<String> ();
                //System.debug('groupId -----> ' + groupId);
                cswp_Group__c newGroup = newGroupMap.get(groupId);
                //System.debug('newGroup -----> ' + newGroup);
                cswp_Group__c oldGroup = oldGroupMap.get(groupId);
                //System.debug('oldGroup -----> ' + oldGroup);

                List<String> newGroupUserList = String.isNotBlank(newGroup.cswp_Users__c) ? newGroup.cswp_Users__c.split(',') : new List<String> ();
                List<String> oldGroupUserList = String.isNotBlank(oldGroup.cswp_Users__c) ? oldGroup.cswp_Users__c.split(',') : new List<String> ();

                for(String newGroupUser : newGroupUserList) {
                    if(!oldGroupUserList.contains(newGroupUser)) {
                        addedUserIdSet.add(newGroupUser);
                    }
                }
                //System.debug('addedUserIdSet -----> ' + addedUserIdSet);

                for(String oldGroupUser : oldGroupUserList) {
                    if(!newGroupUserList.contains(oldGroupUser)) {
                        removedUserIdSet.add(oldGroupUser);
                    }
                }

                //System.debug('removedUserIdSet -----> ' + removedUserIdSet);

                if(!addedUserIdSet.isEmpty()) {
                    groupIdToAddedUserId.put(groupId, addedUserIdSet);
                }

                if(!removedUserIdSet.isEmpty()) {
                    groupIdToRemovedUserId.put(groupId, removedUserIdSet);
                }
            }

            Map<String, Set<String>> groupIdToPermissionSets = CSWPSobjectSelector.getAssignedPermissionSetsByGroupId(changeGroupIdSet);
            //System.debug('groupIdToPermissionSets -----> ' + groupIdToPermissionSets);
            //System.debug('groupIdToAddedUserId -----> ' + groupIdToAddedUserId);
            //System.debug('groupIdToRemovedUserId -----> ' + groupIdToRemovedUserId);

            System.enqueueJob(new CSWPAssignPermissionQueueable(groupIdToPermissionSets, groupIdToAddedUserId, groupIdToRemovedUserId));
        }
        else if(context.props.isAfter && context.props.isDelete) {
            Map<Id, cswp_Group__c> oldGroupMap = (Map<Id, cswp_Group__c>) context.props.oldMap;
            Set<Id> pgIds = new Set<Id>();
            for(cswp_Group__c cswpg : oldGroupMap.values()) {
                pgIds.add(cswpg.cswp_PublicGroupId__c);
            }            
            if(pgIds.size() > 0) deletePublicGroupsWithIdsFuture(pgIds);
        }
        context.stop();
    }

    @future
    private static void deletePublicGroupsWithIdsFuture(Set<Id> pgIds) {
        Map<Id, Group> pgMap = CSWPSobjectSelector.getPublicGroupsByIdSet(pgIds);
        List<Cswp_Workspace__Share> sharedWsList = CSWPSobjectSelector.getSharedWorkspacesByPublicGroupIds(pgIds);
        List<Cswp_Calibration_Box_Data__Share> sharedCalBoxDataList = CSWPSobjectSelector.getSharedCalBoxDataByPublicGroupIds(pgIds);

        delete sharedWsList;
        delete sharedCalBoxDataList;
        delete (pgMap.values());
    }
}