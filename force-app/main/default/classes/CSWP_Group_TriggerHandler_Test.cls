@isTest
private with sharing class CSWP_Group_TriggerHandler_Test {
    @isTest
    private static void testinsert(){
        Account acc = CSWPTestUtil.createAccount();
        insert acc;

        User testUser = CSWPTestUtil.createCommunityUser(acc);
        insert testUser;

        User testUser2 = CSWPTestUtil.createCommunityUser(acc);
        testUser2.Username = 'test@email.com';
        testUser2.CommunityNickname = 'test@email.com';
        insert testUser2;

        cswp_Group__c cswpgroup = CSWPTestUtil.createCswpGroup('test group',acc,'test group');
        cswpgroup.cswp_Users__c = testUser2.Id;

        Database.SaveResult svr = database.insert(cswpgroup , false);
        
        cswp_Group__c testGroup = [SELECT Name,Group_Name__c, cswp_PublicGroupId__c, cswp_Users__c FROM cswp_Group__c WHERE Group_Name__c = 'test group' LIMIT 1]; 
        System.assertEquals(true, svr.isSuccess(),'cswp group insert successful');
        System.assertEquals(testGroup.Group_Name__c,'test group', 'test group was inserted');
        System.assertNotEquals(testGroup.cswp_PublicGroupId__c, null, 'public group Id is assigned by trigger');

        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        PermissionSet permissionSet = [SELECT Id, Label FROM PermissionSet WHERE Name = 'CSWP_PortalUser' LIMIT 1];
        CSWP_Assigned_Permission_Set__c aps = CSWPTestUtil.createAssignedPermissionSet(permissionSet.Label, cswpGroup, permissionSet.Id);
        insert aps;
        GroupMember member;
        PermissionSetAssignment psa;
        System.runAs(currentUser) {
            member = new GroupMember(
                UserOrGroupId = testUser.Id,
                GroupId = testGroup.cswp_PublicGroupId__c
            );
            insert member;
            
            psa = new PermissionSetAssignment (
                PermissionSetId = permissionSet.Id,
                AssigneeId = testUser2.Id
            );
            insert psa;
        }

        Test.startTest();
        CSWPAssignPermissionSetSchedule scheduleClass = new CSWPAssignPermissionSetSchedule(60);
        String jobId = System.schedule('Test Schedule', '0 0 23 * * ?', scheduleClass);
        SchedulableContext sc = null;
        scheduleClass.execute(sc);
        Test.stopTest();
    }
}