@isTest 
class CSWP_SharepointServiceTest {
    static String hubItemId = 'item:L3NpdGVzL1NhbGVzZm9yY2VmaWxlcmVwb3NpdG9yeWRldnRlc3Q:6b996eec-9fe1-4150-bb52-2f824c53dd1a:7a464605-7c6b-432e-b4c0-e1b89b00d9cd:99';
    static String allowedItemId = 'item:L3NpdGVzL1NhbGVzZm9yY2VmaWxlcmVwb3NpdG9yeWRldnRlc3Q:6b996eec-9fe1-4150-bb52-2f824c53dd1a:7a464605-7c6b-432e-b4c0-e1b89b00d9cd:0x0101';
    static cswp_Workspace__c tempWorkSpace;

    static final String WorkspaceName = 'test-cswpworkspacefolder-1';
    static final String WorkspaceExternalId = '01NOCE2SIMKVIFV45RVZDZ3EISFE6FOHUXTESTWORKSPACEID';
    static final String FolderName = 'test-cswpfolder-1';
    static final String FolderExternalId = '01NOCE2SIMKVIFV45RVZDZ3EISFE6FOHUXTESTFOLDERID';
    private static Account acc;

    @isTest(SeeAllData=true)
    private static void createWorkspaceDriveItemTest(){
        
        contentApiData();
        String jsonBody = CSWPTestUtil.createDriveFolderForTest(workspaceName,workspaceExternalId,'');        
        Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(jsonBody,201));
        CSWP_Workspace__c workspace;
        Test.startTest();
        
        workspace = createWorkspace();
        CSWPUtil.debugLog('CREATED WORKSPACE:>>>' + JSON.serializePretty(workspace));
        //workspace = [Select Id, Name, cswp_ExternalId__c, cswp_HubItemId__c,cswp_ParentHubItemID__c From CSWP_Workspace__c Where Id=:workspace.Id Limit 1];
        
        Test.stopTest();
        CSWP_Folder__c folder = createFolderTest(workspace);

    }

    @isTest(SeeAllData=true)
    private static void uploadfileToWorkspace(){
        contentApiData();
        String jsonBody = CSWPTestUtil.createDriveFolderForTest(workspaceName,workspaceExternalId,'');                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(jsonBody,200));
        try {
            CSWP_Workspace__c workspace = createWorkspace();
            uploadFileTest(workspace.Id);
        } catch (Exception e) {
            CSWPUtil.errorlog('ERRor.uploadfileToWorkspace:>' + e.getMessage());    
        }
        Test.stopTest();
    }

    @isTest
    private static void getSiteId(){
        Test.startTest();
        SharepointService service = SharepointService.init();
        System.assertEquals(SharepointService.TEST_SITE_ID, service.siteId);
        Test.stopTest();
    }


    private static CSWP_Workspace__c createWorkspace(){ 
        acc = new Account(Name = 'TestAcc1');
        insert acc;
        Id wsRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Document Sharing' AND SobjectType = 'cswp_Workspace__c' LIMIT 1].Id;
        
        tempWorkSpace = CSWPTestUtil.createCswpWorkspace(workspaceName,acc);
        tempWorkSpace.RecordTypeId = wsRecordTypeId;
        tempWorkSpace.cswp_InternalParentPath__c = 'client-documents';
        tempWorkSpace.cswp_ParentHubItemID__c = hubItemId;
        tempWorkSpace.cswp_IsSync__c = false;            
        tempWorkSpace.cswp_Approver__c = [Select Id From User Where IsActive=TRUE and ProfileId IN (Select Id From Profile Where PermissionsCustomizeApplication=true ) Limit 1].Id;
        insert tempWorkSpace;
        
        return tempWorkSpace;

    }
    private static CSWP_Folder__c createFolderTest(cswp_Workspace__c parentWorkspace){
        //CSWPUtil.debugLog('createFolderTest.workspaceExternalId:>>>>' + parentWorkspace.cswp_ExternalId__c);
        String jsonBody = CSWPTestUtil.createDriveFolderForTest( FolderName,FolderExternalId, '/' + WorkspaceName );
        Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(jsonBody,200));
        cswp_Folder__c cswpClientFolder = CSWPTestUtil.createCswpFolder(FolderName,2.5,parentWorkspace);

        insert cswpClientFolder;
        return cswpClientFolder;
    }
    
    private static void uploadFileTest(String parentId){
        List<String> docIds = new List<String>();

        ContentVersion contentVersion = new ContentVersion(
            Title = 'testfile',
            PathOnClient = 'testfile.txt',
            VersionData = Blob.valueOf('Test file txt'),
            IsMajorVersion = true,
            FirstPublishLocationId = parentId
        );
        insert contentVersion;
        List<ContentVersion> cvList = [SELECT Id,Title,ContentDocumentId,FileExtension,ContentSize,PathOnClient FROM ContentVersion Where Id=:contentVersion.Id LIMIT 1 ];
        System.debug(LoggingLevel.DEBUG, 'ContentVersion:>>>>>>>>>>>' + JSON.serializePretty( cvList ) );
        docIds.add(cvList[0].ContentDocumentId);
        cswp_File__c theFile;
        
        CSWPWorkspaceManagerController.uploadFilesToRemote(tempWorkSpace.Id,docIds);

        theFile = [Select Id,cswp_Status__c, Name From cswp_File__c Where Name='testfile.txt' Limit 1];
        CSWPUtil.debugLog('theFile=:>>>' + theFile);
        theFile.cswp_Status__c = 'Approved';
        update theFile;
    
        //Update the file name 
        theFile.Name = 'testfile_updated.txt';
        update theFile;
        
        //Delete the file;
        //delete theFile;

    }

    
    private static void contentApiData(){
        ContentHubRepository chRepo = [SELECT Id, DeveloperName FROM ContentHubRepository WHERE DeveloperName = 'Sharepoint_Data_Source' LIMIT 1];
        Network net  = [SELECT ID,name FROM Network where name = 'CSWP Portal' LIMIT 1];

        ConnectApi.RepositoryFolderSummary testFolderSummary = new ConnectApi.RepositoryFolderSummary ();
        testFolderSummary.name = 'test-cswpworkspacefolder-1';
        ConnectApi.RepositoryFolderItem rfItem = new ConnectApi.RepositoryFolderItem ();
        rfItem.file = null;
        rfItem.folder = testFolderSummary;
        List<ConnectApi.RepositoryFolderItem > rfItemList = new List<ConnectApi.RepositoryFolderItem >();
        rfItemList.add(rfItem);
        ConnectApi.RepositoryFolderItemsCollection itemCollection = new ConnectApi.RepositoryFolderItemsCollection();
        itemCollection.items = rfItemList;
        ConnectApi.ContentHubProviderType contenHubProvType = new ConnectApi.ContentHubProviderType();
        contenHubProvType.type = 'ContentHubSharepointOffice365';
        ConnectApi.ContentHubRepositoryCollection testCol = new ConnectApi.ContentHubRepositoryCollection ();
        ConnectApi.ContentHubRepository chr = new ConnectApi.ContentHubRepository();
        chr.label = 'Sharepoint Data Source';
        chr.name = 'Sharepoint_Data_Source';
        chr.id = chRepo.Id;
        chr.providerType = contenHubProvType;
        ConnectApi.ContentHubAllowedItemTypeCollection itemTypeCollection = new ConnectApi.ContentHubAllowedItemTypeCollection();
        ConnectApi.ContentHubItemTypeSummary itemSummary  = new ConnectApi.ContentHubItemTypeSummary();
        itemSummary.id = allowedItemId;
        itemSummary.description = 'Create a new document';
        itemSummary.displayName = 'Document';

        itemTypeCollection.allowedItemTypes = new List<ConnectApi.ContentHubItemTypeSummary>{
            itemSummary
        };

        List<ConnectApi.ContentHubRepository> hubList = new List<ConnectApi.ContentHubRepository>();
        
        hubList.add(chr);
        
        testCol.repositories = hubList;

        ConnectApi.ContentHub.setTestGetAllowedItemTypes(chRepo.Id,hubItemId,itemTypeCollection);
        ConnectApi.ContentHub.setTestGetRepositoryFolderItems(chRepo.Id,hubItemId,itemCollection);
        ConnectApi.ContentHub.setTestGetRepositories(net.Id,testCol);
    }

}