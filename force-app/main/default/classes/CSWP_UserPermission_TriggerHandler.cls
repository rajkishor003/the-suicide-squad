public with sharing class CSWP_UserPermission_TriggerHandler implements 
    Triggers.Handler, Triggers.BeforeInsert, Triggers.BeforeUpdate {
    
    public Boolean criteria(Triggers.context context) {
        if(System.isBatch() || System.isFuture() ){
            return false;
        }
        return true;
    }

    public void beforeInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWP_UserPermission_TriggerHandler.class.getName(), CSWPUtil.ON_BEFORE_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void beforeUpdate(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CSWP_UserPermission_TriggerHandler.class.getName(), CSWPUtil.ON_BEFORE_UPDATE);
        if(isEnabled){
            then(context);
        }
    }

    public void then(Triggers.context context) {
        if(context.props.isBefore && (context.props.isInsert || context.props.isUpdate)) {
            List<CSWP_User_Permission__c> userPermissionList = (List<CSWP_User_Permission__c>) context.props.newList;
            Set<String> groupIdSet = new Set<String> ();
            Set<String> publicGroupIdSet = new Set<String> ();
            Map<String, Set<String>> publicGroupIdToUserIdSetMap = new Map<String, Set<String>> ();
            for(CSWP_User_Permission__c userPermission : userPermissionList) {
                if(String.isNotBlank(userPermission.CSWP_Group__c)) {
                    groupIdSet.add(userPermission.CSWP_Group__c);
                }
            }

            Map<Id, cswp_Group__c> cswpGroupMap = new Map<Id, cswp_Group__c> ([SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c WHERE Id IN :groupIdSet]);
            for(cswp_Group__c cswpGroup : cswpGroupMap.values()) {
                if(String.isNotBlank(cswpGroup.cswp_PublicGroupId__c)) {
                    publicGroupIdSet.add(cswpGroup.cswp_PublicGroupId__c);
                }
            }

            for(GroupMember groupMember : [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN :publicGroupIdSet]) {
                if(!publicGroupIdToUserIdSetMap.containsKey(groupMember.GroupId)) {
                    publicGroupIdToUserIdSetMap.put(groupMember.GroupId, new Set<String> ());
                }
                publicGroupIdToUserIdSetMap.get(groupMember.GroupId).add(groupMember.UserOrGroupId);
            }

            for(CSWP_User_Permission__c userPermission : userPermissionList) {
                if(publicGroupIdToUserIdSetMap.isEmpty() || publicGroupIdToUserIdSetMap == null) {
                    userPermission.addError('The user is not a member of public group that specified on CSWP Group record.');
                    continue;
                }
                if(String.isNotBlank(userPermission.CSWP_Group__c) && cswpGroupMap.containsKey(userPermission.CSWP_Group__c)) {
                    cswp_Group__c cswpGroup = cswpGroupMap.get(userPermission.CSWP_Group__c);
                    if(String.isNotBlank(cswpGroup.cswp_PublicGroupId__c) && publicGroupIdToUserIdSetMap.containsKey(cswpGroup.cswp_PublicGroupId__c)) {
                        Set<String> userIdSet = publicGroupIdToUserIdSetMap.get(cswpGroup.cswp_PublicGroupId__c);
                        if(!userIdSet.contains(userPermission.CSWP_User__c) || userIdSet == null || userIdSet.isEmpty()) {
                            userPermission.addError('The user is not a member of public group that specified on CSWP Group record.');
                        }
                    }
                }
            }
        }
    }
}