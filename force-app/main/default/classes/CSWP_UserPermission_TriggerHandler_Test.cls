@isTest
private without sharing class CSWP_UserPermission_TriggerHandler_Test {
    @TestSetup
    static void makeData(){
       Account acc = CSWPTestUtil.createAccount();
       insert acc;
       User user = CSWPTestUtil.createCommunityUser(acc);
       insert user;
    }

    @isTest
    private static void testInsertAndUpdate(){
        User testuser = [SELECT Id FROM User WHERE Username = 'test@uniquedomain.com'];
        Account acc = [SELECT Id FROM Account LIMIT 1];

        // Group publicGroup = new Group();
        // publicGroup.name = 'Test Group1';
        // publicGroup.Type = 'Regular'; 
        // insert publicGroup; 

        cswp_Group__c cswpGroup = CSWPTestUtil.createCswpGroup('test group',acc,'test group');
        //cswpGroup.cswp_PublicGroupId__c = publicGroup.Id;
        insert cswpGroup;

        cswp_Group__c testGroup = [SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c WHERE Id = :cswpGroup.Id];

        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        GroupMember groupMember;

        System.runAs(currentUser) {
            groupMember = new GroupMember();
            groupMember.UserOrGroupId = testuser.Id;
            groupMember.GroupId = testGroup.cswp_PublicGroupId__c;
            insert groupMember;
        }
        

        cswp_GroupPermission__c cswpGroupPermission = CSWPTestUtil.createCswpGroupPermission(true,true,true,true,groupMember.Id,'type',true);
        insert cswpGroupPermission;

        CSWP_User_Permission__c userPermission = new CSWP_User_Permission__c(
            Name='TestFullAccessPerm', 
            cswp_Group__c = cswpGroup.Id, 
            cswp_Group_Permission__c = cswpGroupPermission.Id, 
            CSWP_User__c= testuser.Id);

        Test.startTest();
        Database.SaveResult svrinsert = database.insert(userPermission, true);
        userPermission.Name = 'TestFullAccessPermission';
        Database.SaveResult svrupdate = database.update(userPermission, true);
        Test.stopTest(); 

        System.assertEquals(true, svrinsert.isSuccess(),'userpermission insert successful');
        System.assertEquals(true, svrupdate.isSuccess(),'userpermission update successful');
        System.assertEquals(userPermission.Name,'TestFullAccessPermission', 'user permission name field was updated sucessfully');
    }
}