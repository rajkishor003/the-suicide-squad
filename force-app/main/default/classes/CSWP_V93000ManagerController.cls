public with sharing class CSWP_V93000ManagerController {

    @AuraEnabled (cacheable = true)
    public static List<V93000LinkWrapper> getV93000Links() {
        Id V93000RecordId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('CSWP_V93000_Links').getRecordTypeId();

        List<V93000LinkWrapper> v93000List = new List<V93000LinkWrapper>();
        for(Knowledge__kav v93000 : [SELECT Id, Title, CSWP_Licence_URL__c, cswp_Sequence__c, CSWP_Image__c FROM Knowledge__kav WHERE RecordTypeId = :V93000RecordId AND Language = :UserInfo.getLanguage() AND PublishStatus = 'Online' ORDER BY cswp_Sequence__c ASC NULLS LAST LIMIT 5]){
            v93000List.add(new V93000LinkWrapper(v93000.Id, v93000.Title, v93000.CSWP_Licence_URL__c, v93000.CSWP_Image__c));
        }
        return v93000List;
    }

    @AuraEnabled (cacheable = true)
    public static List<V93000LinkWrapper> getV93000LinksDetail() {
        Id V93000RecordId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('CSWP_V93000_Links').getRecordTypeId();

        List<V93000LinkWrapper> v93000List = new List<V93000LinkWrapper>();
        for(Knowledge__kav v93000 : [SELECT Id, Title, CSWP_Licence_URL__c, cswp_Sequence__c, CSWP_Image__c FROM Knowledge__kav WHERE RecordTypeId = :V93000RecordId AND Language = :UserInfo.getLanguage() AND PublishStatus = 'Online' ORDER BY cswp_Sequence__c ASC NULLS LAST]){
            v93000List.add(new V93000LinkWrapper(v93000.Id, v93000.Title, v93000.CSWP_Licence_URL__c, v93000.CSWP_Image__c));
        }
        return v93000List; 
    }

    public class V93000LinkWrapper {
        @AuraEnabled
        public String v93000Title {get;set;}
        @AuraEnabled
        public String v93000URL {get;set;}
        @AuraEnabled
        public String v93000Id {get;set;}
        @AuraEnabled
        public String imageURL {get;set;}

        public V93000LinkWrapper(String v93000Id, String title, String url, String imageURL) {
            this.v93000Title = title;
            this.v93000URL = url;
            this.v93000Id = v93000Id;
            this.imageURL = imageURL;
        }
    }

}