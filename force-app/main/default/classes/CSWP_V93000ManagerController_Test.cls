@isTest
private without sharing class CSWP_V93000ManagerController_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='Test Account');
        insert acc;

        String dataCategory;
        String dataCategoryName;
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        describeCategoryResult = Schema.describeDataCategoryGroups(new List<String> {'KnowledgeArticleVersion'});
        List<DataCategoryGroupSobjectTypePair> pairList = new List<DataCategoryGroupSobjectTypePair> ();
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult) {
            DataCategoryGroupSobjectTypePair pair = new DataCategoryGroupSobjectTypePair();
            pair.setSobject(singleResult.getSobject());
            pair.setDataCategoryGroupName(singleResult.getName());
            pairList.add(pair);
        }

        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairList, false);

        for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult) {
            DataCategory[] topLevelCategories = singleResult.getTopCategories();
            for(DataCategory topLevelCategory : topLevelCategories) {
                for(DataCategory category : topLevelCategory.getChildCategories()) {
                    if(String.isBlank(dataCategory)) {
                        dataCategory = category.getName();
                        dataCategoryName = category.getName();
                        continue;
                    } else {
                        break;
                    }
                } 
            }
        }

        Id V93000RecordId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('CSWP_V93000_Links').getRecordTypeId();
        List<Knowledge__kav> v93000Links = new List<Knowledge__kav>();
        v93000Links.add(new Knowledge__kav(Title='Title 1', UrlName='V93000-100', CSWP_Licence_URL__c='www.url.com', cswp_Sequence__c=1, CSWP_Image__c='www.url.com/img.jpg',RecordTypeId=V93000RecordId, CSWP_Data_Category__c = dataCategory, Language = UserInfo.getLanguage()));
        v93000Links.add(new Knowledge__kav(Title='Title 2', UrlName='V93000-200', CSWP_Licence_URL__c='www.url.com', cswp_Sequence__c=2, CSWP_Image__c='www.url.com/img.jpg',RecordTypeId=V93000RecordId, CSWP_Data_Category__c = dataCategory, Language = UserInfo.getLanguage()));
        v93000Links.add(new Knowledge__kav(Title='Title 3', UrlName='V93000-300', CSWP_Licence_URL__c='www.url.com', cswp_Sequence__c=3, CSWP_Image__c='www.url.com/img.jpg',RecordTypeId=V93000RecordId, CSWP_Data_Category__c = dataCategory, Language = UserInfo.getLanguage()));
        v93000Links.add(new Knowledge__kav(Title='Title 4', UrlName='V93000-400', CSWP_Licence_URL__c='www.url.com', cswp_Sequence__c=4, CSWP_Image__c='www.url.com/img.jpg',RecordTypeId=V93000RecordId, CSWP_Data_Category__c = dataCategory, Language = UserInfo.getLanguage()));
        v93000Links.add(new Knowledge__kav(Title='Title 5', UrlName='V93000-500', CSWP_Licence_URL__c='www.url.com', cswp_Sequence__c=5, CSWP_Image__c='www.url.com/img.jpg',RecordTypeId=V93000RecordId, CSWP_Data_Category__c = dataCategory, Language = UserInfo.getLanguage()));
        insert v93000Links;
        User knowledgeUser = [SELECT Id FROM User WHERE UserPermissionsKnowledgeUser = true AND IsActive = true LIMIT 1];
        System.runAs(knowledgeUser){
            for(Knowledge__kav v93000Link : [SELECT Id, KnowledgeArticleId FROM Knowledge__kav]) {
                KbManagement.PublishingService.publishArticle(v93000Link.KnowledgeArticleId, true);
            }
        }
        
    }
    @isTest 
    static void testgetV93000LinksAndDetail(){
        List<Account> testaccount = [SELECT Id FROM Account LIMIT 1];
        List<CSWP_V93000ManagerController.V93000LinkWrapper> v93000Links = new List<CSWP_V93000ManagerController.V93000LinkWrapper>();
        List<CSWP_V93000ManagerController.V93000LinkWrapper> v93000LinksDetail = new List<CSWP_V93000ManagerController.V93000LinkWrapper>();
        v93000Links = CSWP_V93000ManagerController.getV93000Links();
        v93000LinksDetail = CSWP_V93000ManagerController.getV93000LinksDetail();
        System.assertNotEquals(v93000Links.size(),0,'links are retrieved');
        System.assertEquals(v93000Links.size(),5, '5 links are retrieved as expected');
        System.assertNotEquals(v93000LinksDetail.size(),0, 'links and details are retrieved');
        System.assertEquals(v93000LinksDetail.size(),5, '5 links are retrieved as expected');
    }
}