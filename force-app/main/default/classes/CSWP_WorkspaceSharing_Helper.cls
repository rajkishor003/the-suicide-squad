public with sharing class CSWP_WorkspaceSharing_Helper {

    @TestVisible private static final String ACCESS_LEVEL_EDIT = 'Edit';
    public static void addPermission(List<cswp_Workspace__c> workspaceList) {
        Set<String> groupIdSet = new Set<String> ();
        Group allUsers = getAllUsersGroup();
        for(cswp_Workspace__c workspace : workspaceList) {
            if(String.isNotBlank(workspace.cswp_Group__c)) {
                groupIdSet.add(workspace.cswp_Group__c);
            }
        }

        Map<Id, cswp_Group__c> groupMap = new Map<Id, cswp_Group__c> ([SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c WHERE Id IN :groupIdSet]);

        List<cswp_Workspace__Share> workspaceShareList = new List<cswp_Workspace__Share> ();
        
        for(cswp_Workspace__c workspace : workspaceList) {
            if(!groupMap.containsKey(workspace.cswp_Group__c)) {
                continue;
            }
            cswp_Group__c cswpGroup = groupMap.get(workspace.cswp_Group__c);
            if(String.isBlank(cswpGroup.cswp_PublicGroupId__c)) {
                continue;
            }
            // If 'is external url' true, this workspace should be visible to All Users
            Id publicGroupID;
            if(workspace.cswp_Is_External_URL__c && allUsers != null ){
                publicGroupID = allUsers.Id;
            }
            else {
                publicGroupID = cswpGroup.cswp_PublicGroupId__c;
            }
            
            workspaceShareList.add(new cswp_Workspace__Share(
                ParentId = workspace.Id,
                UserOrGroupId = publicGroupID,
                RowCause = Schema.cswp_Workspace__Share.RowCause.Workspace_Share__c,
                AccessLevel = ACCESS_LEVEL_EDIT
            ));
        }

        if(!workspaceShareList.isEmpty()) {
            Database.insert(workspaceShareList, false);
        }
    }

    public static void removePermission(Map<Id, cswp_Workspace__c> workspaceMap) {
        Set<String> groupIdSet = new Set<String> ();
        Set<String> groupMemberIdSet = new Set<String> ();
        Group allUsers = getAllUsersGroup();
        for(cswp_Workspace__c workspace : workspaceMap.values()) {
            if(String.isNotBlank(workspace.cswp_Group__c)) {
                groupIdSet.add(workspace.cswp_Group__c);
            }
            if(workspace.cswp_Is_External_URL__c && allUsers != null){
                groupMemberIdSet.add(allUsers.Id);
            }
        }

        for(cswp_Group__c cswpGroup : [SELECT Id, cswp_PublicGroupId__c FROM cswp_Group__c WHERE Id IN :groupIdSet]) {
            if(String.isNotBlank(cswpGroup.cswp_PublicGroupId__c)) {
                groupMemberIdSet.add(cswpGroup.cswp_PublicGroupId__c);
            }
        }

        List<cswp_Workspace__Share> workspaceShareList = [SELECT Id FROM cswp_Workspace__Share WHERE 
            UserOrGroupId IN :groupMemberIdSet 
            AND ParentId IN :workspaceMap.keySet() 
            AND RowCause = :Schema.cswp_Workspace__Share.RowCause.Workspace_Share__c];

        if(!workspaceShareList.isEmpty()){
            Database.delete(workspaceShareList, false);
        } 
    }

    public static Group getAllUsersGroup(){
        return [SELECT Id FROM Group WHERE DeveloperName = 'All_Users' LIMIT 1];
    }
}