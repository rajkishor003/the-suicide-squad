public with sharing class CswpFeedItemTriggerHandler implements Triggers.Handler,Triggers.BeforeInsert,Triggers.AfterInsert {
    public Boolean criteria(Triggers.Context context) {
        if (System.isFuture() || System.isQueueable() || System.isBatch()) {
          return false;
        }
        return true;
    }

    public void beforeInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CswpFeedItemTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void afterInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(CswpFeedItemTriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void then(Triggers.context context) { 
        List<FeedItem> newFeedItemList = (List<FeedItem>) context.props.newList;
        String internalCommentTag = Label.Internal_Comment_Tag;
        if(context.props.isBefore) {
            if(context.props.isInsert) {
                for(FeedItem fi : newFeedItemList) {
                    if(fi.Type == 'TextPost') {
                        fi.Visibility = 'AllUsers';
                        if(fi.Body.contains(internalCommentTag)) {
                            fi.Visibility = 'InternalUsers';
                        }
                        
                    } 
                }
            }
        }
        if(context.props.isAfter) {
            if(context.props.isInsert) {
                Set<Id> feedItemIds = new Set<Id>();
                Set<Id> caseIds = new Set<Id>();
                Map<Id,String> caseToFeedBody = new Map<Id,String>();
                Map<Id,Id> caseToFeedOwner = new Map<Id,Id> ();
                List<NamedCredential> credentialList = [SELECT DeveloperName,Endpoint,Id,MasterLabel,PrincipalType FROM NamedCredential WHERE DeveloperName = 'Advantest_Jira'];
                for(FeedItem fi : newFeedItemList) {
                    if(String.valueOf(fi.ParentId).startsWith('500') && !fi.Body.contains(internalCommentTag)) {
                        if(fi.Body.contains('JIRA')) {
                            System.debug('containsss jira');
                            feedItemIds.add(fi.Id);
                        }
                        else{
                            caseIds.add(fi.ParentId);
                            caseToFeedBody.put(fi.ParentId,fi.Body);
                            caseToFeedOwner.put(fi.ParentId,fi.CreatedById);
                        }
                    }
                }
                // Handle Feed Item Body and Send an Email to Case Owner
                if(!feedItemIds.isEmpty() && !credentialList.isEmpty()){
                    CSWPUtil.handleFeedItemBody(feedItemIds);
                }
                if(!caseIds.isEmpty() && !caseToFeedBody.isEmpty() && !caseToFeedOwner.isEmpty()) {
                    List<orgwideemailaddress> orgWideList = [Select id from orgwideemailaddress limit 1];
                    Id senderId = null;
                    Id communityProfileId = null;
    
                    if(!orgWideList.isEmpty()) {
                        senderId = orgWideList[0].Id;
                    }

                    List<Profile> cswpProfileList = [SELECT Id FROM Profile WHERE NAME = 'CSWP Community User' LIMIT 1];

                    if(!cswpProfileList.isEmpty()) {
                        communityProfileId = cswpProfileList[0].Id;
                    }

                    List<Site> cswpSiteList = [SELECT Id, Name, Subdomain, UrlPathPrefix, GuestUserId FROM Site where UrlPathPrefix = 'cswp'];

                    if(!cswpSiteList.isEmpty() && senderId != null && communityProfileId != null) {
                        Site s = cswpSiteList[0];
                        String communityUrl = [SELECT Id, DurableId, IsRegistrationEnabled, SecureUrl FROM SiteDetail WHERE DurableId =: s.Id].SecureUrl;
                        for( Case c : [SELECT id,Contact.Name, Subject,CaseNumber, OwnerId,owner.Email , owner.ProfileId FROM case WHERE id in : caseIds AND Cswp_Jira_Project__c != null]) {
                            if(c.Owner.profileId == communityProfileId && c.OwnerId != caseToFeedOwner.get(c.id)) {
                                String htmlBody = '<p>Hi'+ c.Contact.Name+',</p><p>A new comment is available:</p>'+caseToFeedBody.get(c.Id)+'<p>Case Number : #'+c.CaseNumber+'</p><p>View Details : '+communityUrl+'/s/case/'+c.id+'</p>';
                                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                message.OrgWideEmailAddressId = senderId;
                                message.toAddresses = new String[] { c.Owner.Email};
                                message.optOutPolicy = 'FILTER';
                                message.subject = 'You have a new comment #' +c.CaseNumber ;
                                message.HtmlBody = htmlBody;
                                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    
                                if (results[0].success) {
                                    System.debug('The email was sent successfully.');
                                } else {
                                    System.debug('The email failed to send: ' + results[0].errors[0].message);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}