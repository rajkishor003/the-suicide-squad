public without sharing class CswpTicketDetailController {
    
    @AuraEnabled(cacheable=false)
    public static List<FeedItem> getFeedItems(String currentCaseId){
        try {
            if(String.isNotBlank(currentCaseId)) {
                List<FeedItem> feedItemList = [SELECT Id, (SELECT Id, FeedEntityId, Type, RecordId, Title, Value FROM FeedAttachments WHERE RecordId != NULL) , CreatedDate,Title, Body, Status, RelatedRecordId, ParentId,CreatedBy.Name,CreatedBy.FirstName, CreatedBy.LastName,Type FROM FeedItem WHERE ParentId = :currentCaseId AND (Type = 'ContentPost' OR Type = 'TextPost') AND Visibility = 'AllUsers' ORDER BY CreatedDate desc];
                if(!feedItemList.isEmpty()) {
                    return feedItemList;
                }
            }
            return null;
  
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled(Cacheable=false)
    public static List<FeedItem> createFeedItem(String currentCaseId, String value){
        List<FeedItem> newFeedList = new List<FeedItem>();
        try {
            FeedItem fi = new FeedItem();
            fi.Visibility = 'AllUsers';
            fi.Body = value;
            //fi.Body = email.htmlBody;
            //fi.isRichText = false;
            fi.parentId = Id.valueOf(currentCaseId);
            insert fi ;
            newFeedList.add(fi);
            
            return newFeedList;
  
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getFiles(String recordId) {
        List<ContentDocumentLink> contentDocumentList = [SELECT ContentDocumentId, LinkedEntityId 
                                                            FROM   ContentDocumentLink 
                                                            WHERE  LinkedEntityId =: recordId];
        Set<Id> contentDocumentId = new Set<Id>();
            
        for(ContentDocumentLink cdl : contentDocumentList){
            contentDocumentId.add(cdl.ContentDocumentId);
        }
            
        List<ContentVersion> contentVersionList = [SELECT Id, VersionData, FileType, Title, FileExtension,
                                                    ContentDocument.CreatedBy.Name, ContentDocument.ContentSize,
                                                    CreatedDate, ContentDocumentId, ContentDocument.FileType
                                                    FROM   ContentVersion 
                                                    WHERE  ContentDocumentId IN : contentDocumentId ORDER BY CreatedDate DESC];
        return JSON.serialize(contentVersionList);
    }

    @AuraEnabled
    public static String deleteFile(String fileID) {
        try{
            delete [SELECT Id FROM ContentDocument WHERE Id =: fileID];
            return 'SUCCESS';
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    @AuraEnabled
    public static String uploadFile(String base64, String filename, String recordId) {
          ContentVersion cv = createContentVersion(base64, filename);
          ContentDocumentLink cdl = createContentLink(cv.Id, recordId);
          if (cv == null || cdl == null) { return null; }
          return cdl.Id;
    }
    private static ContentVersion createContentVersion(String base64, String filename) {
        ContentVersion cv = new ContentVersion();
        cv.VersionData = EncodingUtil.base64Decode(base64);
        cv.Title = filename;
        cv.PathOnClient = filename;
        try {
          insert cv;
          return cv;
        } catch(DMLException e) {
          System.debug(e);
          return null;
        }
    }
    private static ContentDocumentLink createContentLink(String contentVersionId, String recordId) {
        if (contentVersionId == null || recordId == null) { return null; }
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [
            SELECT ContentDocumentId 
            FROM ContentVersion 
            WHERE Id =: contentVersionId
            ].ContentDocumentId;
            cdl.LinkedEntityId = recordId;
            // ShareType is either 'V', 'C', or 'I'
            // V = Viewer, C = Collaborator, I = Inferred
            cdl.ShareType = 'V';
            try {
            insert cdl;
            return cdl;
            } catch(DMLException e) {
            System.debug(e);
            return null;
            }
    }

}