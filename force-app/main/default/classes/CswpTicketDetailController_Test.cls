@IsTest
public class CswpTicketDetailController_Test {
    @IsTest
    static void testTicketDetailMethods() {
        Account acc = CSWPTestUtil.createAccount();
        insert acc;
        Contact c = new Contact(Lastname = 'Doe', Firstname = 'John', AccountId = acc.Id);
        insert c;
        Case testCase = new Case (ContactId = c.Id, Subject = 'Test', Description = 'Test Description');
        insert testCase;

        FeedItem fi = new FeedItem();
        fi.visibility = 'AllUsers';
        fi.Body = '<i>[JIRA] Test comment...</i>Test feed body';
        fi.Type ='TextPost';
        fi.isRichText = true;
        fi.parentId = testCase.Id;
        insert fi ;
        CswpTicketDetailController.getFeedItems(testCase.Id);
        String newFeed = 'New Feed Item';
        CswpTicketDetailController.createFeedItem(testCase.Id, newFeed);

        ContentVersion cv = new Contentversion();
        cv.title='ABC';
        cv.PathOnClient ='test';
        Blob b = Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        List<ContentVersion> cvList = [Select Id,ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = testCase.id;
        contentlink.contentdocumentid = cvList[0].contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        CswpTicketDetailController.getFiles(testCase.Id);
        CswpTicketDetailController.uploadFile('Unit Test Attachment Body','test',testCase.Id);
    }
}