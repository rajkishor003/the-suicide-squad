public with sharing class CswpTicketManagerController {
    @AuraEnabled(cacheable=true)
    public static List<Case> getOpenTickets(String currentCaseId, Integer caseOffset){
        try {
            if(String.isNotBlank(currentCaseId)) {
                List<Case> openTicketList = [SELECT Id, Subject, CaseNumber,Status FROM Case WHERE Id != :currentCaseId AND Status != 'Closed' AND Cswp_Jira_Project__c != NULL  ORDER BY LastModifiedDate desc LIMIT 5 OFFSET :caseOffset];
                if(!openTicketList.isEmpty()) {
                    return openTicketList;
                }
            }
            return null;
  
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled(cacheable=true)
    public static List<Case> getMyTickets(String currentCaseId , Integer caseOffset){
        String ownId = (String)UserInfo.getUserId();
        try {
            if(String.isNotBlank(currentCaseId)) {
                List<Case> openTicketList = [SELECT Id, Subject, CaseNumber,Status FROM Case WHERE Id != :currentCaseId AND OwnerId = :ownId AND Cswp_Jira_Project__c != NULL ORDER BY LastModifiedDate desc LIMIT 5 OFFSET :caseOffset];
                if(!openTicketList.isEmpty()) {
                    return openTicketList;
                }
            }
            return null;
  
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static Case getTicketDetail(String currentCaseId){
        try {
            if(String.isNotBlank(currentCaseId)) {
                List<Case> currentTicket = [SELECT Id, Subject, CaseNumber,Status FROM Case WHERE Id = :currentCaseId LIMIT 1];
                if(!currentTicket.isEmpty()) {
                    return currentTicket[0];
                }
            }
            return null;
  
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}