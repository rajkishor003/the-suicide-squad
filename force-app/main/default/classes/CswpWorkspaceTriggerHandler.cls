/*************************************************************************************************************
 * @name			CswpWorkspaceTriggerHandler
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			25 / 11 / 2020
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-25		oguzalp					Changes desription
 *
 **************************************************************************************************************/
public with sharing class CswpWorkspaceTriggerHandler implements Triggers.Handler, Triggers.AfterInsert, Triggers.AfterUpdate {
  public Boolean criteria(Triggers.Context context) {
    if (System.isBatch() || System.isFuture()) {
      return false;
    }
    return true;
  }

  public void afterInsert(Triggers.Context context) {
    Boolean isAfterInsertEnabled = CSWPConfigurationManager.IsTriggerEnabled(
      CswpWorkspaceTriggerHandler.class.getName(),
      CSWPUtil.ON_AFTER_INSERT
    );
    if (isAfterInsertEnabled) {
      thenAfterInsert(context);
    }
  }

  public void afterUpdate(Triggers.Context context) {
    Boolean isAfterUpdateEnabled = CSWPConfigurationManager.IsTriggerEnabled(
      CswpWorkspaceTriggerHandler.class.getName(),
      CSWPUtil.ON_AFTER_UPDATE
    );
    if (!isAfterUpdateEnabled) {
      return;
    }
    Boolean isParentItemChanged = context.props.isChanged( Schema.CSWP_Workspace__c.cswp_ParentHubItemID__c );
    /*isAfterUpdateEnabled &&*/
    if (isParentItemChanged) {
      CSWPRepositoryManager.getHubItemIdByParentAsync( context.props.newMap.keySet() );
    }
    //If the workspace name is changed
    Boolean isNameChanged = context.props.isChanged(CSWP_Workspace__c.Name);
    if (isNameChanged) {
      //Update external name
      updateWorkspaceExternalName(context);
    }
  }

    public void thenAfterInsert(Triggers.Context context){
        Set<Id> eligibleIds = new Set<Id>();
        for (cswp_Workspace__c library : (List<cswp_Workspace__c>)context.props.newList) {
            //Internal parent path is required!
            if(String.isNotBlank(library.cswp_InternalParentPath__c) 
                && !library.cswp_IsSync__c //Check if it is already synced!
                && !library.cswp_Is_External_URL__c //Check if it is an external URL!
                /* &&library.cswp_CreateOnExternal__c*/ ){ //if it is createn on external site true
                    eligibleIds.add(library.Id); 
            }
        }
        try {
            CSWPRepositoryManager.createRepositorFolderByWorkspaceAsync(eligibleIds);
        } catch (Exception ex) {
            CSWPUtil.errorLog('ERROR CswpWorkspaceTriggerHandler ====== \n' + ex.getCause() + '\n' + ex.getMessage());
        }
    }

  public void updateWorkspaceExternalName(Triggers.Context context) {
    Map<Id, SObject> oldMap = context.props.oldMap;
    for (SObject space : context.props.newList) {
      cswp_Workspace__c oldOne = (cswp_Workspace__c) oldMap.get(space.Id);
      cswp_Workspace__c newOne = (cswp_Workspace__c) space;
      //External Callout: Limit 100 callout
      if (
        String.isNotBlank(oldOne.cswp_Path__c) && String.isNotBlank(newOne.Name)
      ) {
        CSWPRepositoryManager.updateExternalItemName(
          oldOne.cswp_Path__c,
          newOne.Name
        );
      }
    }
  }
}