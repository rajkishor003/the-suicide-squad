/*************************************************************************************************************
 * @name			Cswp_CalBoxFileUploadController.cls
 * @author			emre akkus <e.akkus@emakina.com.tr>
 * @created			12 / 11 / 2020
 * @description		Cswp_CalBoxFileUploadController class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-12		emreakkus				Initial Version.
**************************************************************************************************************/
public with sharing class Cswp_CalBoxFileUploadController {
    public Cswp_CalBoxFileUploadController() {}

    public static final String calboxdatafileextension = '.cdc';
    public static final String calboxdatasignaturefileextension = '.sign';
    public static final String openingTagFirstChars = '[';
    public static final String closingTagFirstChars = '[/';
    public static final String tagEndingChars = ']';

    public static Map<String, Cswp_Cal_Box_Data_File_Obj_Mapping__mdt> ObjMapping = new Map<String, Cswp_Cal_Box_Data_File_Obj_Mapping__mdt>();
    public static Map<String, Map<String, String>> objSourceFieldMap = new Map<String, Map<String, String>>();
    public static List<String> TagList = new List<String>();
    public static List<String> CloseTagList = new List<String>();
    private static List<String> InstrumentSNList = new List<String>();

    public static List<TagWrapper> activeTagWrappers = new List<TagWrapper>();

    public static Map<String, List<TagWrapper>> Tag_WrapperMap = new Map<String, List<TagWrapper>>();
    public static Map<Integer, TagWrapper> TagWrapperMap = new Map<Integer, TagWrapper>();
    public static Set<Id> documentIds = new Set<Id>();
    public static String FileStructureVersion = '';
    public static Map<String, String> returnMap = new Map<String, String>();
    public static Decimal latestCertificateVersion = 0; // Default Value
    public static Boolean certDownloadable = false;
    public static String AccountOfUploader = '';
    public static Boolean calibratedByAdvantest = false;
    public static Boolean wrongInstallBaseInfo = false;
    public static Boolean checkSumValid = true;
    public static String calBoxDataFileName;
    public static Boolean OIB_SysNo_Valid = false;
    public static Boolean OIB_AccNo_Valid = false;
    public static Boolean Cdc_Structure_Valid = true;
    public static String systemSerialNumber = '';
    public static String calibrationGenerationDate;
    public static String selectedFSECustomerName;
    public static String loggedInUserProfile;
    //public static Map<String, Schema.DescribeFieldResult> wrapper_ParentFieldMap = new Map<String, Schema.DescribeFieldResult>();
    public static Map<String, String> obj_parentFldMap = new Map<String, String> {'Cswp_Service_Box__c'=>'Cswp_Calibration_Box_Data__c',
                                                                                  'Cswp_System__c'=>'Cswp_Calibration_Box_Data__c',
                                                                                  'Cswp_Task__c'=>'Cswp_Calibration_Box_Data__c',
                                                                                  'Cswp_Instrument__c'=>'Cswp_Service_Box__c',
                                                                                  'Cswp_Resource__c'=>'Cswp_Task__c'
                                                                                };

    @AuraEnabled
    public static Map<String,String> ProcessUploadedFile(Object[] files, String fseCustomerName) {
        try {
            Boolean zipContainsDataFile = false;
            Boolean zipContainsSignFile = false;
            List<Zippex> fileList = new List<Zippex>();
            for(Object o : files) {
                Map<Object,Object> fileMap = (Map<Object,Object>)o;
                documentIds.add(String.valueOf(fileMap.get('documentId')));
            }

            Map<String, List<ContentVersion>> ContentDocVerMap = new Map<String, List<ContentVersion>>();
            List<ContentVersion> cvList = cswp_ContentVersionSelector.GetContentVersionWithDocumentId(documentIds);

            for(ContentVersion cv : cvList) {
                if(ContentDocVerMap.keySet().contains(cv.ContentDocumentId)) {
                    ContentDocVerMap.get(cv.ContentDocumentId).add(cv);
                }
                else {
                    ContentDocVerMap.put(cv.ContentDocumentId, new List<ContentVersion>());
                    ContentDocVerMap.get(cv.ContentDocumentId).add(cv);
                }
            }
            
            for(String docId : documentIds) {
                for(ContentVersion cv : ContentDocVerMap.get(docId)) {
                    Zippex zipFile = new Zippex(cv.VersionData);
                    fileList.add(zipFile);
                }
            }
            if(fseCustomerName != null ) selectedFSECustomerName = fseCustomerName;
            Blob CalBoxDataFile = null;
            Blob CalBoxSignatureFile = null;
            for(Zippex zipFile : fileList) {
                for(String fileName : zipFile.getFileNames()) {
                    if(fileName.endsWith(calboxdatafileextension)&&CalBoxDataFile==null) {
                        zipContainsDataFile = true;
                        CalBoxDataFile = zipFile.getFile(fileName);
                    }
                    if(fileName.endsWith(calboxdatasignaturefileextension)&&CalBoxSignatureFile==null) {
                        zipContainsSignFile = true;
                        CalBoxSignatureFile = zipFile.getFile(fileName);
                    }
                }
            }
            if(!zipContainsDataFile) throw new Cswp_Custom_Exception(Label.cswp_calbox_file_not_found_exception);
            if(!zipContainsSignFile) throw new Cswp_Custom_Exception(Label.cswp_calbox_sign_file_not_found_exception);
            //Parse And Validate Data File
            try{
                parseCalBoxDataFile(CalBoxdataFile);
            }
            catch(Exception ex){
                throw new Cswp_Custom_Exception(Label.Cswp_Cdc_Structure_Not_Valid);
            }
            returnMap.put('CreatedEntityName', calBoxDataFileName);
            //Parse And Validate Data File
            if(checkIfCalibrationDataAlreadyExist(calBoxDataFileName)) {
                throw new Cswp_Custom_Exception(Label.Cswp_Calibration_Already_Exists);
            }
            checkSumValid = validateDataFile(CalBoxdataFile, CalBoxSignatureFile); //Checksum Valid needs to be completed before CreateSObjectsForTagWrapper because checksum valid checkbox value assigned there.
            CheckSerialNumberExistOnInstallBase(systemSerialNumber, calBoxDataFileName); //CheckSerialNumberExistOnInstallBase needs to be completed before CreateSObjectsForTagWrapper because OIB checkboxes value assigned there.
            CreateSObjectsForTagWrapper(TagWrapperMap);
            if(!checkSumValid) { throw new Cswp_Custom_Exception(Label.Cswp_File_Not_Validated); }
            else if(!OIB_SysNo_Valid && !loggedInUserProfile.contains('Factory')) {
                throw new Cswp_Custom_Exception(Label.Cswp_Valid_File_Wrong_Install_Base);
            }
            else if(!OIB_AccNo_Valid && loggedInUserProfile.contains('Community')) {
                throw new Cswp_Custom_Exception(Label.Cswp_Valid_File_Wrong_Install_Base); 
            }
            //else if(wrongInstallBaseInfo) { throw new Cswp_Custom_Exception(Label.Cswp_Valid_File_Wrong_Install_Base); }
            returnMap.put('Result', Label.cswp_Success_Label);
            return returnMap;
        } catch (Exception e) {
            CSWPLogManagement.createLogForCSWP(e.getMessage(), e.getStackTraceString(), true, e.getTypeName(), calBoxDataFileName);
            returnMap.put('Result', e.getMessage());
            if(e.getMessage() == Label.Cswp_Valid_File_Wrong_Install_Base) { returnMap.put('Warning','true'); }
            if(e.getMessage() == Label.Cswp_File_Not_Validated) { returnMap.put('Warning','true'); }
            return returnMap;
        }
    }

    public static Boolean validateDataFile(Blob DataFile, Blob SignatureFile) {
        String pKey = Cswp_ConfigurationSelector.GetConfigurationWithName('Cswp_Data_File_Validation_Public_Key').get(0).Cswp_Value__c;
        Blob keyData = EncodingUtil.base64Decode(pKey);
        return Crypto.verify('RSA-SHA256', DataFile, SignatureFile, keyData);
    }

    private static Boolean checkIfCalibrationDataAlreadyExist(String calBoxDataFileName) {
        //split file name - left side will be system number, right side will be timestamp
        if(calBoxDataFileName.contains('/')) calBoxDataFileName = calBoxDataFileName.split('/')[1];
        List<String> splittedFileName = calBoxDataFileName.split('_');
        if(splittedFileName.size() > 2) { //Sometimes SerialNumber can contain underscore
            for(Integer i=0; i<splittedFileName.size()-1; i++) {
                if(i != 0) { systemSerialNumber += ' '; }
                systemSerialNumber      += splittedFileName[i];
            }
            calibrationGenerationDate    = splittedFileName[splittedFileName.size()-1].split('\\.')[0]; //take timestamp and getrid of file extension
        } else if(splittedFileName.size() == 2) {
            systemSerialNumber          += splittedFileName[0];
            calibrationGenerationDate    = splittedFileName[1].split('\\.')[0]; //take timestamp and getrid of file extension
        }
        List<Cswp_System__c> sysList = CSWPSobjectSelectorWithoutSharing.getSystemsWithSerialNumber(systemSerialNumber); // Ordered by Version Number
        if(sysList?.size() > 0) { latestCertificateVersion = sysList[0].Cswp_Calibration_Box_Data__r.Cswp_Certificate_Version__c; }
        for(Cswp_System__c sys : sysList) {
            if(sys.Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c == getDateTimeofValue(calibrationGenerationDate)) {
                return true;
            }
        }
        return false;
    }

    private static void CheckSerialNumberExistOnInstallBase(String serialNumber, String dataFileName) {
        User loggedInUser = CSWPSobjectSelector.getUser(UserInfo.getUserId());
        calibratedByAdvantest = CheckIfUserIsAdvantestEmployee(loggedInUser);
        AccountOfUploader = loggedInUser.Contact?.AccountId;
        loggedInUserProfile = loggedInUser.Profile.Name;
        boolean createLog = false;
        List<Cswp_Install_Base_System_Data__c> ibData = CSWPSobjectSelector.getInstallBaseSystemWithSerialNumber(serialNumber);
        //if(ibData == null || ibData.size() == 0) { throw new Cswp_Custom_Exception(Label.Cswp_System_Not_In_Install_Base); }
        if(loggedInUserProfile.contains('Factory') && checkSumValid ) certDownloadable = true;
        if(ibData.size() > 0) {
            OIB_SysNo_Valid = true;
            if(loggedInUserProfile.contains('FSE') && checkSumValid ) certDownloadable = true;
            //Check if Account Numbers Match in System
            String AccountID                  = String.valueOf(ibData[0].Cswp_Owner_Party_ID__c);
            //String GlobalAccountID          = String.valueOf(ibData[0].Cswp_Global_Owner_Party_ID__c);
            String InstalledAtAccountID       = String.valueOf(ibData[0].Cswp_Installed_At_Party_ID__c);

            List<Account> accList = CSWPSobjectSelector.getAccountWithAccountNumber(AccountID);
            //List<Account> globalAccList = CSWPSobjectSelector.getAccountWithAccountNumber(GlobalAccountID);
            List<Account> installedAccList = CSWPSobjectSelector.getAccountWithAccountNumber(InstalledAtAccountID);

            //if( (acclist == null||globalAccList == null||installedAccList == null)||(loggedInUser.Contact.AccountId != accList[0].Id&&loggedInUser.Contact.AccountId != globalAccList[0].Id&&loggedInUser.Contact.AccountId != installedAccList[0].Id)   ) { throw new Cswp_Custom_Exception(Label.Cswp_Not_Allowed_To_Upload_Data_File); }        }
            if( (acclist?.size()>0&&loggedInUser.Contact.AccountId == accList[0].Id)||(installedAccList?.size()>0&&loggedInUser.Contact.AccountId == installedAccList[0].Id)) {
                OIB_AccNo_Valid = true;
                if(loggedInUserProfile.contains('Community') && checkSumValid ) certDownloadable = true;
            }
            else {
                createLog = true;
            }
        }
        if(ibData.size() == 0||createLog) {
            //CSWPLogManagement.createLogForCalBox(Label.Cswp_Valid_File_Wrong_Install_Base, 'CheckSerialNumberExistOnInstallBase', true, 'Error', dataFileName);
            wrongInstallBaseInfo = true;
        }
    }

    private static boolean CheckIfUserIsAdvantestEmployee(User u) {
        Set<Id> groupIds = new Set<Id>();
        List<Group> gList = new List<Group>();
        List<Cswp_Group__c> cswpGroupList = new List<Cswp_Group__c>();
        gList = CSWPSobjectSelector.getPublicGroupsOfUser(u.Id);
        for(Group g : gList) { groupIds.add(g.Id); }
        cswpGroupList = CSWPSobjectSelector.getCswpGroupsOfPublicGroups(groupIds);
        for(cswp_Group__c g : cswpGroupList) { if(g.Cswp_Advantest_Employees__c) {return true;} }
        return false;
    }

    private static void parseCalBoxDataFile(Blob CalBoxdataFile) {
        String rawString = CalBoxDataFile.toString();
        // Initialize Metadata Records And Tag Lists
        List<Cswp_Cal_Box_Data_File_Obj_Mapping__mdt> mappingList = cswp_CalBoxDataFileMappingSelector.GetCalBoxDataFileMapping();
        for(Cswp_Cal_Box_Data_File_Obj_Mapping__mdt objMdt : mappingList) {
            TagList.add(openingTagFirstChars+objMdt.Cswp_Source_Tag__c+tagEndingChars);
            CloseTagList.add(closingTagFirstChars+objMdt.Cswp_Source_Tag__c+tagEndingChars);
            ObjMapping.put(objMdt.Cswp_Source_Tag__c, objMdt);

            if(objSourceFieldMap.get(objMdt.Cswp_Target_Object__c) == null) {
                objSourceFieldMap.put(objMdt.Cswp_Target_Object__c, new Map<String,String>());
            }            
            for(Cswp_Cal_Box_Data_File_Field_Mapping__mdt fldMDt : objMdt.Cswp_Cal_Box_Data_File_Field_Mappings__r) {
                objSourceFieldMap.get(objMdt.Cswp_Target_Object__c).put(fldMdt.Cswp_Source_Field__c, fldMdt.Cswp_Target_Field__c);
            }
        }
        openTagProcess('Master');

        List<String> lines = rawString.split('\n');
        for(Integer i=0; i<lines.size(); i++) {
            String thisLine = lines[i];
            List<String> splittedStr = thisLine.split('\\s++');
            if(i == 0) GetCdcVersion(splittedStr);
            //--Functionality Start
            if(checkIsCommentOrBlank(thisLine)) { continue; } //Comment line - ignored
            List<String> simplifiedStr = trimAndSimplifyLine(splittedStr); // Getting rid of unnecessary blank spaces.        
            parseLine(simplifiedStr);
            //--Functionality End
        }
    }

    private static void GetCdcVersion(List<String> lineStr) {
        FileStructureVersion = lineStr[2].remove(',');
    }

    private static List<String> trimAndSimplifyLine(List<String> rawSplittedLine) {
        if(rawSplittedLine.size() > 1 && String.isEmpty(rawSplittedLine[0])) {
            rawSplittedLine.remove(0);
        }
        return rawSplittedLine;
    }
    
    private static Boolean checkIsCommentOrBlank(String lineStr) {
        return (lineStr.length() < 4) || (lineStr.startsWith('#')) || (lineStr.split('\\s++').size() == 0)||(lineStr.deleteWhitespace().startsWith('#'));
    }

    private static void parseLine(List<String> splittedLine) {
        String objTag = '';
        if(TagList.contains(splittedLine[0])) { //Tag Opened
            objTag = splittedLine[0].substringBetween(openingTagFirstChars, tagEndingChars);
            openTagProcess(objTag);
        } else if(CloseTagList.contains(splittedLine[0])) { //Tag Closed
            objTag = splittedLine[0].substringBetween(closingTagFirstChars, tagEndingChars);
            closeTagProcess(objTag);
        } else {
            // Field Mapping
            TagWrapper activeWrapper = activeTagWrappers.get(activeTagWrappers.size()-1); // Reading Active Tag
            if(splittedLine.size() < 2) { return; }
            Map<String, String> srcFields = objSourceFieldMap.get(activeWrapper.ObjType);            
            if(srcFields.keySet().contains(splittedLine[0])) {
                if(splittedLine[1].contains('"')) {
                    String mergedValue = splittedLine[1];
                    //Value Contains Double Quote
                    for(integer i=2; i<splittedLine.size(); i++) {
                        if(splittedLine[i].contains('#')) { break; }
                        mergedValue = mergedValue + ' ' + splittedLine[i];
                        if(splittedLine[i].contains('"')) break;
                    }
                    splittedLine[1] = mergedValue;
                }                
            }
            // Setting target Field and Value For Fields on Tag
            // Setting System Serial Number And CDC Generation Date
            if(activeWrapper.TagName == 'thisFile' && splittedLine[0] == 'GenerationDate') { calBoxDataFileName = splittedLine[1].replace('"',''); }
            if(activeWrapper.TagName == 'System' && splittedLine[0] == 'SerialNumber') { calBoxDataFileName = splittedLine[1].replace('"','') + '_' +calBoxDataFileName;  }
            if(activeWrapper.Level == 0 && activeWrapper.TagName == 'Master') {splittedLine[0] = 'Cdc'; splittedLine[1] = FileStructureVersion;}
            activeWrapper.addField(srcFields.get(splittedLine[0]), splittedLine[1]);          
        }
    }

    private static void openTagProcess(String objTag) {
        TagWrapper tw;
        if(activeTagWrappers.size() > 0) {
            tw = new TagWrapper(objTag, activeTagWrappers.get(activeTagWrappers.size()-1).TagId);
        } else {
            tw = new TagWrapper(objTag);
        }
        TagWrapperMap.put(tw.TagId, tw);
        activeTagWrappers.add(tw);
    }

    private static void closeTagProcess(String objTag) {
        activeTagWrappers.remove(activeTagWrappers.size() - 1);
    }

    private static void CreateSObjectsForTagWrapper(Map<Integer, TagWrapper> wrapperMap) {
        Map<Integer, List<TagWrapper>> level_WrapperMap = new Map<Integer, List<TagWrapper>>();
        for( Integer tagId : wrapperMap.keySet()) {
            if(level_WrapperMap.get(wrapperMap.get(tagId).Level) == null) {
                level_WrapperMap.put(wrapperMap.get(tagId).Level , new List<TagWrapper>());
                level_WrapperMap.get(wrapperMap.get(tagId).Level).add(wrapperMap.get(tagId));
            } else {
                level_WrapperMap.get(wrapperMap.get(tagId).Level).add(wrapperMap.get(tagId));
            }
        }

        // Creating SObjects Level By Level
        List<SObject> createdSObjects = new List<SObject>();
        Map<String, List<Sobject>> createdObjTypeMap = new Map<String, List<Sobject>>();
        Map<Integer, Sobject> tagId_SobjMap = new Map<Integer, Sobject>();

        String existingSbId = CheckServiceBoxAlreadyExist(InstrumentSNList);
        for(Integer level : level_WrapperMap.keySet()) {
            for(TagWrapper tw : level_WrapperMap.get(level)) {
                if(String.isNotBlank(existingSbId) && (tw.ObjType == 'Cswp_Instrument__c' || tw.ObjType == 'Cswp_Service_Box__c')) { continue;}
                //system.debug('*** Emre tagWrappers ==> '+tw);
                SObject newObj;
                Schema.SObjectType objType = Schema.getGlobalDescribe().get(tw.ObjType);
                Map<String, Schema.SObjectField> fieldMap = objType.getDescribe().fields.getMap();
                if(!tw.isMultiple && createdObjTypeMap.get(tw.ObjType) != null) {
                    newObj = createdObjTypeMap.get(tw.ObjType).get(0);
                }
                else {
                    newObj = objType.newSObject();
                }
                for(String fName : tw.FieldMap.keySet()) {
                    newObj.put(fName, AssignValueWithType(fieldMap, fName, tw.FieldMap.get(fName)));
                }
                
                //Assign new Version For Cal-Box Record
                if(objType == Cswp_Calibration_Box_Data__c.getSObjectType()) {
                    newObj.put('Cswp_Created_From_Cswp_Portal__c', true);
                    newObj.put('Cswp_Certificate_Version__c', latestCertificateVersion + 1 );
                    newObj.put('Cswp_Sync_With_Install_Base__c', certDownloadable);
                    newObj.put('Cswp_Calibrated_By_Advantest__c', calibratedByAdvantest);
                    newObj.put('Cswp_Account__c', AccountOfUploader);
                    newObj.put('Cswp_CheckSum_Valid__c', checkSumValid);
                    newObj.put('Cswp_Validation_OIB_System__c', OIB_SysNo_Valid);
                    newObj.put('Cswp_Validation_OIB_Account__c', OIB_AccNo_Valid);
                    newObj.put('Cswp_FSE_Customer_Name__c', selectedFSECustomerName);
                    if(String.isNotBlank(existingSbId)) {newObj.put('Cswp_Service_Box__c', existingSbId);}
                }

                if(tw.ParentTagId != null && obj_parentFldMap.get(tw.ObjType) != null) {
                    TagWrapper parentTag = TagWrapperMap.get(tw.ParentTagId);
                    if(tw.ObjType != parentTag.ObjType) newObj.put(obj_parentFldMap.get(tw.ObjType), String.valueOf(tagId_SobjMap.get(parentTag.TagId).get('ID')));
                }

                //system.debug('***getCpuTime()*** After '+Limits.getCpuTime());
                tagId_SobjMap.put(tw.TagId, newObj);                
                if(tw.isMultiple) {
                    if(createdObjTypeMap.get(tw.ObjType) == null) {
                        createdObjTypeMap.put(tw.ObjType, new List<SObject>());
                        createdObjTypeMap.get(tw.ObjType).add(newObj);
                    } else {
                        createdObjTypeMap.get(tw.ObjType).add(newObj);
                    }
                } else {
                    createdObjTypeMap.put(tw.ObjType, new List<SObject>{ newObj });
                }                
                //createdObjTypeMap.put(tw.ObjType, newObj);
                createdSObjects.add(newObj);
            }
            // upsert objects on this level
            List<Sobject> objsToUpsert = new List<Sobject>();          
            for(String objType : createdObjTypeMap.keySet()) {
                objsToUpsert.addAll(createdObjTypeMap.get(objType));
            }
            /*for(Sobject s : objsToUpsert) {
                System.debug('*** sObj ==> '+s);
            }*/
            database.upsert(objsToUpsert);
        }
        // Link ContentDocument To Box Data File Object
        List<ContentDocumentLink> linkList = new List<ContentDocumentLink>();
        for(Id docId : documentIds) {
            ContentDocumentLink thislink = new ContentDocumentLink();
            thislink.ContentDocumentId = docId;
            thislink.LinkedEntityID = createdObjTypeMap.get(ObjMapping.get('Master').Cswp_Target_Object__c).get(0).Id;
            thislink.ShareType = 'V';
            thislink.Visibility = 'AllUsers';
            linkList.add(thislink);
        }
        returnMap.put('CalBoxDataID', String.valueOf(createdObjTypeMap.get(ObjMapping.get('Master').Cswp_Target_Object__c).get(0).Id));
        database.insert(linkList);
    }

    private static Object AssignValueWithType(Map<String, Schema.SObjectField> fieldMap, String fName, String val) {
        if(fieldMap.get(fName) == null) return null;
        Schema.DisplayType fldType = fieldMap.get(fName).getDescribe().getType();
        if(val.contains('"')) { val = val.remove('"'); }
        try {
            if (fldType == Schema.DisplayType.DOUBLE) {
                return Decimal.valueOf(val);
            }
            else if(fldType == Schema.DisplayType.DATE) {
                return getDateofValue(val);
                //ConvertToDate();
            }
            else if(fldType == Schema.DisplayType.DATETIME) {
                return getDateTimeofValue(val);
                //ConvertToDateTime();
            }
            else if(fldType == Schema.DisplayType.BOOLEAN) {
                return Boolean.valueOf(val);
            }
            else {
                return val;
            }
        } catch(Exception ex) {
            return null;
        }
    }

    private static DateTime getDateTimeofValue(String val) { // returns datetime for YYYYMMDDHHMMSS format
        return Datetime.newInstanceGMT(Integer.valueOf(val.subString(0,4)), Integer.valueOf(val.subString(4,6)), Integer.valueOf(val.subString(6,8)), Integer.valueOf(val.subString(8,10)), Integer.valueOf(val.subString(10,12)), Integer.valueOf(val.subString(12,14)));
    }

    private static Date getDateofValue(String val) { // returns date for YYYYMMDD format
        switch on val.length() {
            when 10 {
                return Date.newInstance(Integer.valueOf(val.subString(0,4)), Integer.valueOf(val.subString(5,7)), Integer.valueOf(val.subString(8,10)));
            }
            when else {
                return Date.newInstance(Integer.valueOf(val.subString(0,4)), Integer.valueOf(val.subString(4,6)), Integer.valueOf(val.subString(6,8)));
            }
        }
    }

    public class TagWrapper {
        public Integer TagId;
        public String TagName;
        public String ObjType;
        public Integer Level;
        public Integer ParentTagId;
        public Map<String, String> FieldMap = new Map<String, String>();
        public boolean isMultiple;

        public TagWrapper(String tagName) {
            this.TagId      = Integer.valueof((Math.random() * 10000000));
            this.TagName    = tagName;
            this.ObjType    = String.valueOf(ObjMapping.get(TagName).Cswp_Target_Object__c);
            this.Level      = activeTagWrappers.size();
            this.isMultiple = ObjMapping.get(TagName).Cswp_Is_Multiple__c;
        }

        public TagWrapper(String tagName, Integer parentTagId) {
            this.TagId       = Integer.valueof((Math.random() * 10000000));
            this.TagName     = tagName;
            this.ObjType     = String.valueOf(ObjMapping.get(TagName).Cswp_Target_Object__c);
            this.Level       = activeTagWrappers.size();
            this.ParentTagId = parentTagId;
            this.isMultiple  = ObjMapping.get(TagName).Cswp_Is_Multiple__c;
        }

        public void addField(String fieldName, String val) {
            //Storing Instrument Serial Numbers For Existing Service Box Check
            if(this.ObjType == 'Cswp_Instrument__c' && fieldName == 'Cswp_Serial_Number__c') { 
                InstrumentSNList.add(val.replace('"',''));
            }
            this.FieldMap.put(fieldName, val);
        }
    }

    @AuraEnabled
    public static void StoreCertificate(String linkID, string pageLanguage, String entityID, Boolean fullRes, String calBoxDataId, String certTemplate) {
        try {
            Blob pdfData;
            if(Test.isRunningTest()) { 
                pdfData = blob.valueOf('Unit.Test');
            } else {
                pdfData = new PageReference('/apex/cswpCalBox'+ certTemplate +'Certificate?linkID='+linkID+'&calBoxDataId='+calBoxDataId+'&lang='+pageLanguage+'&fullRes='+fullRes).getContent();
            }

            Cswp_Calibration_Box_Data__c cbd = [SELECT ID,Cswp_Report_Number__c FROM Cswp_Calibration_Box_Data__c WHERE ID =:calBoxDataId LIMIT 1];
            String contentDocTitle = 'Certificate'+'_'+cbd.Cswp_Report_Number__c + '_' + DateTime.now().format('yyyyMMddhhmmss','GMT');

            ContentVersion ContVerFile = new ContentVersion();
            ContVerFile.VersionData = pdfData;
            ContVerFile.Title = contentDocTitle; 
            ContVerFile.ContentLocation= 's';
            ContVerFile.PathOnClient=contentDocTitle + '.pdf';
            insert ContVerFile;
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:ContVerFile.Id].ContentDocumentId;
            ContentDocumentLink thislink = new ContentDocumentLink();
            thislink.ContentDocumentId = conDoc;
            thislink.LinkedEntityID = entityID;
            thislink.ShareType = 'V';
            thislink.Visibility = 'AllUsers';
            insert thislink;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void ProcessUploadedSignature(String ContentDocID) {
        try {
            User loggedInUser = CSWPSobjectSelector.getUser(UserInfo.getUserId());
            List<ContentVersion> cv_List = new List<ContentVersion>();
            if(loggedInUser != null && loggedInUser.ContactID != null) {

                //Check If User Already Has Signature
                List<ContentDocument> cdList = CSWPSobjectSelector.GetContentDocumentWithVersions(loggedInUser.ContactID, 'Signature');
                //Override Existing Signature
                List<String> docIds = new List<String>();
                for(ContentDocument cd : cdList) 
                {
                    docIds.add(cd.Id);
                }                
                
                deleteOldDocuments(docIds);
                ContentDocumentLink thislink = new ContentDocumentLink();
                thislink.ContentDocumentId = ContentDocID;
                thislink.LinkedEntityID = loggedInUser.ContactID;
                thislink.ShareType = 'V';
                thislink.Visibility = 'AllUsers';
                insert thislink;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @future
    public static void deleteOldDocuments(List<String> documentIds) {
        List<ContentDocument> cdList = [SELECT ID From ContentDocument Where ID IN:documentIds];
        delete cdList;
    }

    private static String CheckServiceBoxAlreadyExist(List<String> insSNList) {
        List<Cswp_Instrument__c> existingInsList = CSWPSobjectSelector.getInstrumentsWithSerialNumber(insSNList);
        Map<String, Set<Id>> ins_SbMap = new Map<String, Set<Id>>();
        for(Cswp_Instrument__c ins : existingInsList) {
            if(ins_SbMap.get(ins.Cswp_Serial_Number__c) == null) {
                ins_SbMap.put(ins.Cswp_Serial_Number__c, new Set<Id>());
                ins_SbMap.get(ins.Cswp_Serial_Number__c).add(ins.Cswp_Service_Box__c);
            }
            else {
                ins_SbMap.get(ins.Cswp_Serial_Number__c).add(ins.Cswp_Service_Box__c);
            }
        } 

        for(String serialNumber : insSNList) {
            if(ins_SbMap.get(serialNumber) == null) { return '';}
        }

        for(integer i=0; i<existingInsList.size()-1; i++) {
            ins_SbMap.get(existingInsList[i].Cswp_Serial_Number__c).retainAll(ins_SbMap.get(existingInsList[i+1].Cswp_Serial_Number__c));
        }

        if(ins_SbMap.get(existingInsList[0].Cswp_Serial_Number__c) != null ) {
            List<Id> finalList = new List<Id>(ins_SbMap.get(existingInsList[0].Cswp_Serial_Number__c));
            return finalList[0];
        }
        return '';
    }
}