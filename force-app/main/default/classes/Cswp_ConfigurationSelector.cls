/*************************************************************************************************************
 * @name			Cswp_ConfigurationSelector.cls
 * @author			emre akkus <e.akkus@emakina.com.tr>
 * @created			1 / 12 / 2020
 * @description		Cswp_ConfigurationSelector class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-12-01  	emreakkus				Initial Version.
**************************************************************************************************************/
public with sharing class Cswp_ConfigurationSelector {
    public static List<Cswp_Configuration__mdt> GetConfigurationWithName (String configName) {
        return [Select ID, Cswp_Value__c From Cswp_Configuration__mdt Where DeveloperName =: configName ];
    }
}