/*************************************************************************************************************
 * @name			Cswp_SystemSelector.cls
 * @author			emre akkus <e.akkus@emakina.com.tr>
 * @created			1 / 12 / 2020
 * @description		Cswp_SystemSelector class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-12-02  	emreakkus				Initial Version.
**************************************************************************************************************/
public with sharing class Cswp_SystemSelector {
    
    @AuraEnabled(cacheable=true)
    public static List<Cswp_System__c> getSystemData() {
        List<Cswp_System__c> systemList = new List<Cswp_System__c>();
        systemList = [SELECT Id, Name, Cswp_As_Delivered_Condition__c, Cswp_Calibration_Box_Data__c, Cswp_Host_Name__c,
                      Cswp_Rhel_Version__c, Cswp_Serial_Number__c, Cswp_Smt_Version__c, Cswp_Tasks_Start_Date__c,
                      Cswp_Calibration_Box_Data__r.Cswp_Certificate_Version__c, Cswp_Calibration_Box_Data__r.Cswp_Operator_Name__c, Cswp_Calibration_Box_Data__r.Cswp_Temperature__c, Cswp_Calibration_Box_Data__r.Cswp_Humidity__c,
                      Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c, Cswp_Calibration_Box_Data__r.Cswp_Is_Archived__c, Cswp_Calibration_Box_Data__r.Cswp_Days_Left_To_Calibration_Expire__c, 
                      Cswp_Calibration_Box_Data__r.Cswp_Calibration_Due_Date__c, Cswp_Calibration_Box_Data__r.Cswp_Sync_With_Install_Base__c, Cswp_Calibration_Box_Data__r.Cswp_CheckSum_Valid__c,
                      Cswp_Calibration_Box_Data__r.Cswp_Validation_OIB_Account__c, Cswp_Calibration_Box_Data__r.Cswp_Validation_OIB_System__c,
                      Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c
                      FROM Cswp_System__c ];
        return systemList;
    }

    @AuraEnabled(cacheable=true)
    public static List<Map<String,String>> getSystemColumns() {
        // sObject types to describe
        String[] types = new String[]{'Cswp_System__c'};
        // Make the describe call
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        Map<String,Schema.SObjectField> mFields = results[0].fields.getMap();

        List<Map<String,String>>  columnList = new List<Map<String,String>>();
        for(String f : mFields.keySet()) {
            Schema.DescribeFieldResult dfr = mFields.get(f).getDescribe();
            if(dfr.isCustom()||dfr.isNameField()) {
                Map<String, String> kv_Map = new Map<String, String>();
                kv_Map.put('label', dfr.getLabel());
                kv_Map.put('fieldName', dfr.getName());
                kv_Map.put('type', String.valueOf(dfr.getType()));
                kv_Map.put('sortable', String.valueOf(true));
                columnList.add(kv_Map);
            }
        }
        return columnList;
    }
}