/*************************************************************************************************************
 * @name			HttpUtil.cls
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			04 / 11 / 2020
 * @description		HttpUtil class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-04		oguzalp					Initial Version.
 **************************************************************************************************************/
public class HttpUtil implements HttpCalloutMock {
  public String endpoint;
  public String namedCredential;
  public HTTPVerb method;
  public HttpRequest request;
  @TestVisible HttpResponse response;
  Map<String, String> headers = new Map<String, String>();
  Map<String, String> urlParams = new Map<String, String>();
  static final Http http;

  public enum HTTPVerb {
    GET,
    POST,
    PUT,
    PATCH,
    DEL,
    HEAD
  }

  static {
    http = new Http();
	} 
 // For unit tests
 public HttpResponse respond(HttpRequest req){
  req.setEndpoint('/fake/service/endpoint');
	this.response = new HttpResponse();
	this.response.setStatus('OK');
	this.response.setStatusCode(200);
  this.response.setHeader('content-type', 'application/json');
	String jsonBody = JSON.serialize(new Map<String,Object>{ 'success'=>true, 'status'=>'OK' }); 
	this.response.setBody(jsonBody);
	return this.response;
 }

  public HttpUtil() {
    this.method = HttpVerb.GET;
    this.request = new HttpRequest();
    this.headers = new Map<String, String>();
  }

  public HttpUtil(String url) {
    this();
    this.endpoint = url;
  }

  public HttpUtil body(String body) {
    request.setBody(body);
    return this;
  }

  public HttpUtil get() {
    this.method = HTTPVerb.GET;
    return this;
  }

  public HttpUtil post() {
    this.method = HTTPVerb.POST;
    return this;
  }

  public HttpUtil put() {
    this.method = HTTPVerb.PUT;
    return this;
  }

  public HttpUtil patch() {
    this.method = HTTPVerb.PATCH;
    return this;
  }

  public HttpUtil del() {
    this.method = HTTPVerb.DEL;
    return this;
  }

  public HttpUtil head() {
    this.method = HttpVerb.HEAD;
    return this;
  }

  public HttpUtil addHeader(String key, String value) {
    headers.put(key, value);
    return this;
  }

  public HttpUtil addHeaders(Map<String, String> allHeaders) {
    if (allHeaders == null || allHeaders.isEmpty()) {
      return this;
    }
    headers.putAll(allHeaders);
    return this;
  }

  public HttpUtil call() {
    try {
      setEnpointUrl();
      request.setMethod(
        this.method == HttpVerb.DEL ? 'DELETE' : this.method.name()
      );

      if (!headers.isEmpty()) {
        for (String key : this.headers.keyset()) {
          request.setHeader(key, headers.get(key));
        }
      }

      response = http.send(request);
    } catch (Exception ex) {
      System.debug('Unknown error occured ' + ex.getMessage());
    }
    return this;
  }

  private void setEnpointUrl() {
    String encodedUrlParams = encodeParams(this.urlParams);
    if (String.isEmpty(encodedUrlParams)) {
      request.setEndpoint(this.endpoint);
      return;
    }
    String url = this.endpoint;
    if (url.contains('?')) {
      url = url + encodedUrlParams;
    } else {
      url = url + '?' + encodedUrlParams;
    }
    request.setEndpoint(url);
  }

  public static String encodeParams(Map<String, String> params) {
    if (params == null && params.isEmpty()) {
      return '';
    }
    List<String> allParams = new List<String>();
    for (String k : params.keySet()) {
      String theVal = '';
      String value = params.get(k);
      value = EncodingUtil.urlEncode(value, 'utf-8');
      theVal = k + '=' + value;
      allParams.add(theVal);
    }
    return String.join(allParams, '&');
  }

  public HttpUtil setTimeout(Integer timeOut) {
    if (timeOut != null && timeOut > 0) {
      request.setTimeout(timeOut);
    }
    return this;
  }

  public HttpResponse getResponse() {
    return this.response;
  }

  public HttpUtil setBodyAsBlob(Blob data){
    this.request.setBodyAsBlob(data);
    return this;
  }

  public String getResponseBody() {
    if(this.response!=null){
      return this.response.getBody();
    }else{
      return null;
    }
	}
	
	public Dom.Document getBodyAsDocument(){
		return this.response.getBodyDocument();
	}
	
	public Blob getBodyAsBlob(){
		return this.response.getBodyAsBlob();
	}

  public Map<String, String> getHeaders() {
    return this.headers;
  }

  public String status() {
    return this.response.getStatus();
  }

  public Integer getStatusCode(){
    if(this.response!=null){
      return this.response.getStatusCode();
    }else{
      return 500;
    }
  }

  public HttpUtil setUrlParam(String key, String value) {
    if (urlParams == null) {
      urlParams = new Map<String, String>();
    }
    urlParams.put(key, value);
    return this;
  }

  public HttpUtil setAllUrlParams(Map<String, String> allParams) {
    if (allParams != null && !allParams.isEmpty()) {
      urlParams.putAll(allParams);
    }
    return this;
  }
}