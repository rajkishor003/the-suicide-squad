@isTest
private class HttpUtilTest {
   
    static HttpUtil util;
    static HttpUtil initHttp(){
        return (new HttpUtil('/fake/service/endpoint'));
    }
    
    static Map<String,Object> response = new Map<String,Object>{ 'success'=>true, 'status'=>'OK' };
    static Map<String,String> urlParams = new Map<String,String>{
        'param1' => '1',
        'param2' => '2'
    };
    @isTest
    static void testGetMethod(){
        //test with named credentials
        Test.setMock(HttpCalloutMock.class, new HttpUtil());   
        util = HttpUtilTest.initHttp()
            .get()
            .addHeader('Authorization', 'Bearer xxxxxxxxxxxxx')
            .setAllUrlParams(urlParams)
            .setTimeout(12000)
            .call();
        
        System.assertEquals('OK', util.status());
        System.assertEquals(200, util.getStatusCode());
    }
    @isTest
    static void testPutMethod(){
        Test.setMock(HttpCalloutMock.class, new HttpUtil());
        util = HttpUtilTest.initHttp().put().addHeader('Authorization', 'Bearer xxxxxxxxxxxxx').call();
        HttpResponse res = util.getResponse();
        System.assertEquals('OK', util.status());
        System.assertEquals(200, util.getStatusCode());
        System.assertEquals(JSON.serialize(response), util.getResponseBody());
    }

    @isTest
    static void testPostMethod(){
        Test.setMock(HttpCalloutMock.class, new HttpUtil());
        util = HttpUtilTest.initHttp().post().addHeader('Authorization', 'Bearer xxxxxxxxxxxxx').call();
        System.assertEquals(JSON.serialize(response), util.getResponseBody());
    }

    @isTest
    static void testPatchMethod(){
        Test.setMock(HttpCalloutMock.class, new HttpUtil());
        util = HttpUtilTest.initHttp().patch().addHeader('Authorization', 'Bearer xxxxxxxxxxxxx').call();
        System.assertEquals(JSON.serialize(response), util.getResponseBody());
    }

    @isTest
    static void testHeadMethod(){
        Test.setMock(HttpCalloutMock.class, new HttpUtil());
        util = HttpUtilTest.initHttp().head().addHeader('Authorization', 'Bearer xxxxxxxxxxxxx').call();
        System.assertEquals(Blob.valueOf(JSON.serialize(response)), util.getBodyAsBlob());
    }

    @isTest
    static void testDeleteMethod(){
        Test.setMock(HttpCalloutMock.class, new HttpUtil());
        util = HttpUtilTest.initHttp().del().addHeader('Authorization', 'Bearer xxxxxxxxxxxxx').call();
        try{
            util.getBodyAsDocument();
        }catch(Exception e){
            System.debug(Logginglevel.ERROR, e.getMessage());
        }
    }
}