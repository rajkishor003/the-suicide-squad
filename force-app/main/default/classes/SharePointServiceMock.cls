@isTest
public class SharePointServiceMock implements HttpCalloutMock {
    private HttpResponse res;
    private String jsonBody;
    private Integer statusCode;
    public SharePointServiceMock(String jsonBody,Integer statusCode) {
        this.jsonBody = jsonBody; 
        this.statusCode = statusCode;
    }
    public HTTPResponse respond(HTTPRequest req) {
        this.res = new HttpResponse();
        this.res.setHeader('Content-Type', 'application/json');
        this.res.setStatusCode(this.statusCode);
        this.res.setStatus('OK');
        this.res.setBody(jsonBody);
        return this.res;
    }
}