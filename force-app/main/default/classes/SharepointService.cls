/*************************************************************************************************************
 * @name			SharepointService
 * @author			oguzalp <oguz.alp@emakina.com.tr>
 * @created			22 / 11 / 2020
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-22		oguzalp					Changes desription
 *
 **************************************************************************************************************/
public inherited sharing class SharepointService {
  public final static String SITE_HOST_CONFIG_KEY = 'SharepointSiteHost';
  public final static String SITE_LIBRARY_PATH_KEY = 'SharepointLibraryPath';
  public final static String NAMED_CALLOUT = 'callout:GraphApiV1';
  public final static String SITE_ID_KEY = 'SharepointSiteId';
  public final static String CSWP_DRIVEID_KEY = 'CswpDriveId';
  public final static String CSWP_LIBRARY_KEY = 'CswpLibraryName';

  @TestVisible 
  private final static String TEST_SITE_ID = 'myadvantest.sharepoint.com,c4b945eb-25fa-4899-9377-c8fedd73f7d4,6b996eec-9fe1-4150-bb52-2f824c53dd1a';
  @TestVisible 
  private final static String TEST_DRIVE_ID = 'b!60W5xPolmUiTd8j-3XP31OxumWvhn1BBu1IvgkxT3RoFRkZ6a3wuQ7TA4bibANnN';

  public static final Map<String, String> CommonHeaders = new Map<String, String>{
    'Content-Type' => 'application/json',
    'Accept' => 'text/html,application/json'
  };

  @TestVisible private CSWPConfigurationManager configService;
  @TestVisible private static SharepointService serviceInstance = null;
  public String siteId { get; private set; }
  public String driveId { get; private set; }
  @TestVisible private String siteHost { get; private set; }
  @TestVisible private String siteRelativePath { get; private set; }
  @TestVisible private static String sharepointRepoId { get; private set; }

  @TestVisible 
  private SharepointService() {
    configService = CSWPConfigurationManager.getInstance();
    if ( String.isBlank(this.siteHost) || String.isBlank(this.siteRelativePath)) {
      this.siteHost = (String) configService.getConfigValue(SITE_HOST_CONFIG_KEY);
      this.siteRelativePath = (String) configService.getConfigValue(SITE_LIBRARY_PATH_KEY);
      this.siteId = this.getSiteId();
      this.driveId = this.getCswpDriveId();
    }
  }

  public static SharepointService init() {
    if (serviceInstance == null) {
      return new SharepointService();
    } else {
      return serviceInstance;
    }
  }

  public String getSiteId() {
    String siteId = (String) configService.getConfigValue(SITE_ID_KEY);
    if (String.isNotBlank(siteId)) {
      return siteId;
    }
    if(Test.isRunningTest()){
      SharepointService.Site theSite = new SharepointService.Site();
      theSite.id = TEST_SITE_Id;
      theSite.name='myadvantest.sharepoint.com';
      Test.setMock(HttpCalloutMock.class, new SharePointServiceMock(JSON.serialize(theSite),200));
    }
    String endpoint = NAMED_CALLOUT + '/sites/' + this.siteHost + ':/' + this.siteRelativePath;
    String response = new HttpUtil(endpoint)
        .get()
        .addHeaders(commonHeaders)
        .call()
        .getResponseBody();
  
    CSWPUtil.debugLog(response);
    Site theSite = (Site) JSON.deserialize(response, Site.class);
    System.debug(loggingLevel.debug, 'theSite=>' + theSite);
    if (theSite != null && String.isNotBlank(theSite.id)) {
      CSWP_AppConfig__c siteIdConfig = CSWP_AppConfig__c.getInstance( SITE_ID_KEY );
      if(siteIdConfig!=null){
        siteIdConfig.Value__c = theSite.Id;
        update (siteIdConfig);
      }     
    }
    return theSite.id;
  }

  public String getCswpDriveId() {
    String driveId = (String) configService.getConfigValue(CSWP_DRIVEID_KEY);
    if (String.isNotBlank(driveId)) {
      return driveId;
    }
    if(Test.isRunningTest()){
      Map<String,String> jsmap = new Map<String,String>{ 'id' => TEST_DRIVE_ID };
      Test.setMock(HttpCalloutMock.class, new SharePointServiceMock( JSON.serialize(jsmap),200));
    }
    String cswpLibraryName = (String) configService.getConfigValue(CSWP_LIBRARY_KEY);
    String siteId = this.getSiteId();
    String driveEndpoint = NAMED_CALLOUT + '/sites/' + siteId + '/lists/' + cswpLibraryName + '/drive';
    System.debug(LoggingLevel.DEBUG, 'driveEndpoint:>>>' + driveEndpoint);
    String response = new HttpUtil(driveEndpoint)
      .get()
      .addHeaders(commonHeaders)
      .call()
      .getResponseBody();

    Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped( response );
    CSWPUtil.debugLog('result ===> ' + result);
    if (result != null && String.isNotBlank((String) result.get('id'))) {
      driveId = (String) result.get('id');
      CSWP_AppConfig__c driveConfig = CSWP_AppConfig__c.getInstance( CSWP_DRIVEID_KEY );
      if(driveConfig!=null){
        driveConfig.Value__c = driveId;
        update (driveConfig);
      }
      return driveId;
    }
    return '';
  }

  public DriveItem getDriveItemByPath(String path) {
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/root:/' + CSWPUtil.encodeUrlPath(path);
    DriveItem item;
    if(Test.isRunningTest()){
      item = (DriveItem) JSON.deserialize(
        CSWPTestUtil.createDriveFolderForTest('test-cswpworkspacefolder-1','01NOCE2SIMKVIFV','')
       ,DriveItem.class);
       return item;
    }
    HttpUtil callout = new HttpUtil(url).get().addHeaders(commonHeaders).call();
    if (callout.getStatusCode() == 200 ) {
      item = (DriveItem) JSON.deserialize( callout.getResponseBody() ,DriveItem.class);
      CSWPUtil.debugLog( 'DRIVE ITEM OBJECT ==> \n' + JSON.serializePretty(item) ); 
    }
    else {
      CSWPLogManagement.createLogForCSWP('Connection to Sharepoint Cannot be Established or Path Cannot be found on Sharepoint', 'SharepointService.getDriveItemByPath', false, 'CalloutException', NAMED_CALLOUT);
    }
    return item;
  }

  public DriveItem updateItemByGivenPath(String path, String name) {
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/root:/' + CSWPUtil.encodeUrlPath(path);
    String body = JSON.serializePretty( new Map<String, String>{ 'name' => name } );
    HttpUtil callout = new HttpUtil(url)
      .patch()
      .addHeaders(commonHeaders)
      .body(body)
      .call();
    String response = callout.getResponseBody();
    CSWPUtil.debugLog('UPDATE.ITEM.RESULT>>>' + response);
    if (callout.getStatusCode() == 200 || Test.isRunningTest()) {
      DriveItem item = (DriveItem) JSON.deserialize(response, DriveItem.class);
      return item;
    } else {
      ResponseError result = (ResponseError) JSON.deserialize( response, ResponseError.class );
      CSWPUtil.errorLog( ' UPDATE ITEM CODE => ' + result.error.code + ' MESSAGE => ' + result.error.message );
      return null;
    }
  }

  public DriveItem createFolder(String folderName, String parentItemId) {
    String endpoint = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + parentItemId + '/children';
    Map<String,Object> body = new Map<String,Object>{
      'name' => folderName,
      'folder'=> new Map<String,Object>{},
      '@microsoft.graph.conflictBehavior' => 'rename'
    };
    if(Test.isRunningTest()){
      return (DriveItem)JSON.deserialize(CSWPTestUtil.createDriveFolderForTest(folderName,'01NOCE2SIMKVIFV',''),DriveItem.class);
    }
    HttpUtil callout = new HttpUtil(endpoint).post().addHeaders(commonHeaders).body(JSON.serialize(body, true)).call();
    //201: Created
    String response = callout.getResponseBody();
    if (callout.getStatusCode() == 201 ) {
      CSWPUtil.debugLog( 'CREATE FOLDER RESPONSE ==> \n' + response + 'STATUSCODE => ' + callout.getStatusCode() );
      DriveItem folderItem = (DriveItem) JSON.deserialize( response, DriveItem.class );
      return folderItem;
    } else {
      ResponseError result = (ResponseError) JSON.deserialize( response, ResponseError.class );
      CSWPUtil.errorLog( ' CREATEFOLDER ERROR CODE => ' + result.error.code + ' MESSAGE => ' + result.error.message);
      CSWPLogManagement.createLogForCSWP('CREATEFOLDER ERROR => '+result.error.message, 'SharepointService.createFolder', false, 'CalloutException', folderName);
      return null;
    }
  }

  //Returns uploadURL
  //returned url is required for resumable upload 
  //It is being used for upload of file version
  public String createUploadSession(String itemId, String fileName,Decimal fileSize){ 
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + itemId +  '/createUploadSession';
    Map<String,Object> body = new Map<String,Object>{
      'name'=> fileName,
      'fileSize'=> fileSize
    };
    String uploadUrl;
    if(Test.isRunningTest()){
      uploadUrl = '/uploadFile';
    }
    else{
      HttpUtil callout = new HttpUtil(url).post().body(JSON.serializePretty(body)).addHeaders(commonHeaders).call();
      CSWPUtil.debugLog('createUploadSession.Body =>> '+ callout.getResponseBody());
      if(callout.getStatusCode() == 200){
        String response = callout.getResponseBody();
        Map<String,Object> result = (Map<String,Object>)JSON.deserializeUntyped(response);
        if(result.containsKey('uploadUrl')){
          uploadUrl = (String)result.get('uploadUrl');
        }
      }
    }
    
    return uploadUrl;
  }
  
  public List<DriveItem> getChildItemsByParentItemId(String externalItemId){
    List<DriveItem> childItems;
    String cswpLibraryName = (String) configService.getConfigValue(CSWP_LIBRARY_KEY);
    String endpoint = NAMED_CALLOUT + '/sites/' + this.siteId + '/lists/' + cswpLibraryName + '/drive/items/' + externalItemId + '/children';
    HttpUtil callout = new HttpUtil(endpoint).get().addHeaders(commonHeaders).call();
    String response = callout.getResponseBody();
    if(callout.getStatusCode() == 200){
      Map<String,Object> result = (Map<String,Object>)JSON.deserializeUntyped(response);
      List<Object> values = (List<Object>)result.get('value');
      childItems = (List<DriveItem>)JSON.deserialize( JSON.serialize(values), List<DriveItem>.class);
      System.debug('getChildItemsByParentItemId.CHILD_ARRAY' + childItems);
    }else{
      ResponseError result = (ResponseError) JSON.deserialize( response, ResponseError.class );
      CSWPUtil.errorLog( ' SharepointService.getChildItemsByParentItemId.ERROR => ' + result.error.code + ' MESSAGE => ' + result.error.message );
    }
    return childItems;
  }

  /**
   * Batch request to fetch child Items
   * !!! only 20 request in batch is allowed.
   */
   public Map<String,List<DriveItem>> getChildItemsBatchable(List<String> externalIdsList){
    String endpoint = NAMED_CALLOUT + '/$batch';
    Map<String,List<DriveItem>> resultMap = new Map<String,List<DriveItem>>();
    if(externalIdsList==null || externalIdsList.isEmpty()){
      //return empty map
      return resultMap;
    }
    String requestBody = generateBatchRequestBody(externalIdsList);
    System.debug(LoggingLevel.DEBUG, 'CHILDITEM_BATCH_REQUEST >>>>>>' + requestBody);

    HttpUtil callout = new HttpUtil(endpoint)
        .post()
        .body(requestBody)
        .addHeaders(commonHeaders)
        .call();

    String response = callout.getResponseBody();
    if(callout.getStatusCode() == 200){
      Map<String,Object> result = (Map<String,Object>)JSON.deserializeUntyped(response);
      List<Object> responsesArray = (List<Object>)result.get('responses');
      for(Object eachRes: responsesArray){
        Map<String,Object> res = (Map<String,Object>)eachRes;
        if(res.get('status') == 200){
          List<Object> values = (List<Object>)(((Map<String,Object>)res.get('body')).get('value'));
          List<DriveItem> eachChildItems = (List<DriveItem>)JSON.deserialize(JSON.serialize(values), List<DriveItem>.class);
          resultMap.put((String)res.get('id'), eachChildItems );
        }
      }
    }else{
      ResponseError result = (ResponseError) JSON.deserialize( response, ResponseError.class );
      CSWPUtil.errorLog( ' SharepointService.childItemBatch.ERROR => ' + result.error.code + ' MESSAGE => ' + result.error.message );
    }
    return resultMap;
   } 
   /**
    * List<String> externalIdsList: List of sharepoint item Id.
    */
    private String generateBatchRequestBody(List<String> externalIdsList){
      List<Object> requestsAsArray = new List<Object>();
      for (String itemId : externalIdsList) {
        Map<String,Object> req = new Map<String,Object>{
          'url' => '/sites/' + this.siteId + '/lists/' + 
            ((String)configService.getConfigValue(CSWP_LIBRARY_KEY)) 
            + '/drive/items/' + itemId  + '/children',
          'id' => itemId, 
          'method' => 'GET', 
          'headers' => CommonHeaders
        };
        requestsAsArray.add(req);
      }
      Map<String,Object> requestBody = new Map<String,Object>{
        'requests' => requestsAsArray
      };
      return JSON.serialize(requestBody);
    }

  /**
   * @Deprecated   
  public DriveItem getDriveItemById(String itemId) {
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + itemId;
    HttpUtil callout = new HttpUtil(url).get().addHeaders(commonHeaders).call();
    String response = callout.getResponseBody();
    if (callout.getStatusCode() == 200) {
      DriveItem item = (DriveItem) JSON.deserialize(response, DriveItem.class);
      return item;
    } else {
      //TODO: Save as content activity
      ResponseError result = (ResponseError) JSON.deserialize(
        response,
        ResponseError.class
      );
      CSWPUtil.errorLog(
        ' CREATEFOLDE ERROR CODE => ' +
        result.error.code +
        ' MESSAGE => ' +
        result.error.message
      );
      return null;
    }
  }
*/
  public Boolean deleteDriveItem(String itemId) {
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + itemId;
    if(!Test.isRunningTest()) {
      HttpUtil callout = new HttpUtil(url).del().addHeaders(CommonHeaders).call();
      if (callout.getStatusCode() == 204) {
        return true;
      } else {
        return false;
      }
    }
    else{
      return false;
    }
  }

  public String getItemVersionDownloadUrl(String itemId, String versionId) {
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + itemId + '/versions/' + versionId + '/content';
    String downloadUrl = '';
    //Mock Download Url (Emre)
    if(Test.isRunningTest()) {
      downloadUrl = 'product-documents/UAT Product Line 1/PL1 file jpg.jpg';
    }
    else{
      HttpUtil callout = new HttpUtil(url).get().addHeaders(CommonHeaders).call();
      if (callout.getStatusCode() == 302) {
        downloadUrl = callout.getResponse().getHeader('Location');
        System.debug('downloadUrl => ' + downloadUrl);
      }
    }
    return downloadUrl;
  }

  public String getDownloadUrl(String externalItemId){
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + externalItemId +  '/content';
    String downloadUrl = '';
    
    HttpUtil callout = new HttpUtil(url).get().addHeaders(CommonHeaders).call();
    System.debug(LoggingLevel.DEBUG, 'getDownloadUrl.response:>' + callout.getResponseBody());
    if (callout.getStatusCode() == 302) {
      downloadUrl = callout.getResponse().getHeader('Location');
      System.debug('downloadUrl => ' + downloadUrl);
    }
    return downloadUrl;
  }

  public List<FileVersion> getItemVersionList(String itemId) {
    //Mock Response (Emre)
    Map<String, Object> response = null;
    List<FileVersion> result = new List<FileVersion>();
    String url = NAMED_CALLOUT + '/sites/' + this.siteId + '/drives/' + this.driveId + '/items/' + itemId + '/versions/';

    HttpUtil callout = new HttpUtil(url).get().addHeaders(CommonHeaders).call();
    if (callout.getStatusCode() == 200) {
      response = (Map<String, Object>) JSON.deserializeUntyped( callout.getResponseBody() );
    }else {
      return result;
    }
    
    List<Object> versionList = (List<Object>) response.get('value');
    for (Object version : versionList) {
      String versionJson = JSON.serializePretty(version, true);
      FileVersion fileVersion = (FileVersion) JSON.deserialize(
        versionJson,
        fileVersion.class
      );
      fileVersion.size = CSWPUtil.convertByteToKilobyte((Long)fileVersion.size);
      result.add(fileVersion);
    }
    return result;
  }

  //TODO: Description value can also be added if necessary
  public static ConnectApi.RepositoryFileSummary uploadFileAsRepoItem(
      String repoFolderId,
      ConnectApi.BinaryInput fileBinaryInput ) {

      String repoId = getContentHubRepositoryId();
      CSWPUtil.debugLog( 'uploadFileAsRepoItem.repoId:>>>>'  +repoId);
      CSWPUtil.debugLog( 'uploadFileAsRepoItem.repoFolderId:>>>>'  +repoFolderId);
      //String communityId = CSWPUtil.getCswpCommunityId();
      String allowedItemId = getAllowedItemId(repoId, repoFolderId);
      //CSWPUtil.debugLog('SharepointService ' + '.allowedItemId: ' + allowedItemId);
      ConnectApi.ContentHubItemInput fileInput = new ConnectApi.ContentHubItemInput();
      fileInput.itemTypeId = allowedItemId;
      ConnectApi.ContentHubFieldValueInput fieldValueInput = new ConnectApi.ContentHubFieldValueInput();
      fieldValueInput.name = 'name';
      //CSWPUtil.debugLog('FILE_NAME==>' + fileBinaryInput.getFilename());
      fieldValueInput.value = fileBinaryInput.getFilename();
      fileInput.fields = new List<ConnectApi.ContentHubFieldValueInput>{
        fieldValueInput
    };
    
    
    CSWPUtil.debugLog( 'uploadFileAsRepoItem.fileInput:>>>>'  + fileInput );
    CSWPUtil.debugLog( 'uploadFileAsRepoItem.fileBinaryInput:>>>>'  +fileBinaryInput);
    if(Test.isRunningTest()){
        ConnectApi.RepositoryFolderItem docItem = new ConnectApi.RepositoryFolderItem ();
        ConnectApi.RepositoryFileSummary fileSummary = new ConnectApi.RepositoryFileSummary(); 
        fileSummary.title = fileBinaryInput.getFilename();
        fileSummary.contentSize = fileBinaryInput.getBlobValue().size();
        fileSummary.mimeType = fileBinaryInput.getContentType();
        fileSummary.id = repoFolderId+'1';
        //fileSummary.versionId='1';

      docItem.file = fileSummary;
      ConnectApi.ContentHub.setTestAddRepositoryItem(repoId, repoFolderId, fileInput, fileBinaryInput, docItem);
    }
    ConnectApi.RepositoryFolderItem folderFileItem = ConnectApi.ContentHub.addRepositoryItem(
      //communityId,
      repoId,
      repoFolderId,
      fileInput,
      fileBinaryInput
    );
    return folderFileItem.file;
  }
 
  /**
   * @Deprecated
   * 
  public static List<Object> getFolderChildItems(
    String repositoryFolderId,
    Integer pageParam,
    Integer pageSize
  ) {
    String repoId = getContentHubRepositoryId();
    ConnectApi.RepositoryFolderItemsCollection itemsCollection = ConnectApi.Contenthub.getRepositoryFolderItems(
      repoId,
      repositoryFolderId,
      pageParam,
      pageSize
    );
    CSWPUtil.debugLog(
      '===== ITEMS Collection ======\n' + JSON.serializePretty(itemsCollection)
    );
    return itemsCollection.items;
  }*/

  public static ConnectApi.RepositoryFolderItem getHubItemByParentId(
    String parentHubExternalId,
    String itemName,
    String itemType
  ) {
    ConnectApi.RepositoryFolderItem item;
    try {
      item = getHubItem( parentHubExternalId, itemName, itemType, 0);
    } catch (Exception ex) {
      System.debug(LoggingLevel.ERROR , 'An error occured in getHubItemByParentId:>>' + ex.getMessage());
    }
    return item;
    // try{
    
    // }catch(Exception ex){
    //   CSWPUtil.errorLog( 'An error occured in SharepintService.getHubItemdByParentId ==> ' + ex.getMessage() );
    //   return null;
    // }
    // CSWPUtil.debugLog('getHubItemByParentId.parentHubExternalId:>>>'+ parentHubExternalId);
    // CSWPUtil.debugLog('getHubItemByParentId.itemName:>>>'+ itemName);
    // CSWPUtil.debugLog('getHubItemByParentId.itemType:>>>'+ itemType);
    // String repoId = SharepointService.getContentHubRepositoryId();
    // ConnectApi.RepositoryFolderItemsCollection folderCollection;
    // ConnectApi.RepositoryFolderItem folderItem;
    // try {
    //   folderCollection = ConnectApi.Contenthub.getRepositoryFolderItems(
    //     repoId,
    //     parentHubExternalId
    //   );
    //   if (folderCollection == null && folderCollection.items.isEmpty()) {
    //     throw new SharepointServiceException('Folder Items could not be found');
    //   }
    //   for (ConnectApi.RepositoryFolderItem item : folderCollection.items) {
    //     if (item == null && !itemType.equalsIgnoreCase(item.type.name())) {
    //       continue;
    //     }
    //     if (itemType.equalsIgnoreCase('file') && item.file.name == itemName) {
    //       folderItem = item;
    //       break;
    //     }
    //     if (
    //       itemType.equalsIgnoreCase('folder') && item.folder.name == itemName
    //     ) {
    //       folderItem = item;
    //       break;
    //     }
    //   }
    //   return folderItem;
    // } catch (Exception ex) {
    //   CSWPUtil.errorLog(
    //     'An error occured in SharepintService.getHubItemdByParentId ==> ' +
    //     ex.getMessage()
    //   );
    //   return null;
    // }
  }
  private static final String repoId = SharepointService.getContentHubRepositoryId();
  public static ConnectApi.RepositoryFolderItem folderItem;

  public static ConnectApi.RepositoryFolderItem getHubItem(
    String parentHubExternalId,
    String itemName,
    String itemType,
    Integer pageNo
  ) {
    CSWPUtil.debugLog('getHubItemByParentId.parentHubExternalId:>>>'+ parentHubExternalId);
    CSWPUtil.debugLog('getHubItemByParentId.itemName:>>>'+ itemName);
    CSWPUtil.debugLog('getHubItemByParentId.itemType:>>>'+ itemType);
    System.debug(LoggingLevel.DEBUG, 'pageNo:>>>' + pageNo);
    // ConnectApi.RepositoryFolderItem folderItem;
   
    ConnectApi.RepositoryFolderItemsCollection folderCollection = ConnectApi.Contenthub.getRepositoryFolderItems(
      repoId,
      parentHubExternalId,
      pageNo, 
      25
    ); 
    //System.debug(LoggingLevel.DEBUG, 'folderCollection:>>' + JSON.serializePretty(folderCollection));

    if (folderCollection == null && folderCollection.items.isEmpty()) {
        return folderItem;
    }
    for (ConnectApi.RepositoryFolderItem item : folderCollection.items) {
      if (item == null && !itemType.equalsIgnoreCase(item.type.name())) {
        continue;
      }
      if (itemType.equalsIgnoreCase('file') && item?.file?.name == itemName) {
        folderItem = item;
        break;
      }
      if ( itemType.equalsIgnoreCase('folder') && item?.folder?.name == itemName) {
        folderItem = item;
        break;
      }
    }
    System.debug(LoggingLevel.DEBUG, 'folderITEM:>>>' + folderItem);
    if(folderItem != null){
      return folderItem;
    }
    if(folderCollection.nextPageUrl!=null){
      pageNo++;
      System.debug(LoggingLevel.DEBUG, 'Folder item null....| next Page No :>' + pageNo);
      getHubItem(parentHubExternalId,itemName,itemType,pageNo);
    }
    return folderItem;
  }

  /*public static ConnectApi.RepositoryFileDetail renameFile(
    String parentFolderId,
    String repositoryFileId,
    String newFileName
  ) {
    String repoId = SharepointService.getContentHubRepositoryId();
    ConnectApi.ContentHubItemInput updatedFile = new ConnectApi.ContentHubItemInput();
    updatedFile.itemTypeId = getAllowedItemId(repoId, parentFolderId);
    updatedFile.fields = new List<ConnectApi.ContentHubFieldValueInput>();
    ConnectApi.ContentHubFieldValueInput fieldValueInput = new ConnectApi.ContentHubFieldValueInput();
    fieldValueInput.name = 'name';
    fieldValueInput.value = newFileName;
    updatedFile.fields.add(fieldValueInput);
    ConnectApi.RepositoryFileDetail fileDetail = ConnectApi.ContentHub.updateRepositoryFile(
      repoId,
      repositoryFileId,
      updatedFile
    );
    return fileDetail;
  }*/

  public static String getAllowedItemId(
    String repositoryId,
    String repoFolderId
  ) {
    System.debug(LoggingLevel.DEBUG, 'GETALLLOWEDITEMID.REpoID:>> ' + repoFolderId);
    //String communityId = CSWPUtil.getCswpCommunityId();
    ConnectApi.ContentHubAllowedItemTypeCollection allowedItemTypesColl = ConnectApi.ContentHub.getAllowedItemTypes(
      //communityId,
      repositoryId,
      repoFolderId
    );
    System.debug(LoggingLevel.DEBUG, 'ContentHubItemTypeSummary.allowedFileItemType:>>>>' + JSON.serialize(allowedItemTypesColl));

    List<ConnectApi.ContentHubItemTypeSummary> allowedItemTypes = allowedItemTypesColl.allowedItemTypes;
    String allowedFileItemTypeId = null;
    if (allowedItemTypes.size() > 0) {
      ConnectApi.ContentHubItemTypeSummary allowedItemTypeSummary = allowedItemTypes.get(
        0
      );
      allowedFileItemTypeId = allowedItemTypeSummary.id;
    }
    return allowedFileItemTypeId;
  }
  /*********************************************************************************************************
   * @author			oguzalp <oguz.alp@emakina.com.tr>
   * @created			23 / 11 / 2022
   * @return			String: It returns contenthub repositoryId
   **********************************************************************************************************/
  public static String getContentHubRepositoryId() {
    if (String.isNotBlank(sharepointRepoId)) {
      return sharepointRepoId;
    }
    ConnectApi.ContentHubRepositoryCollection hubCollection = ConnectApi.ContentHub.getRepositories(
      CSWPUtil.getCswpCommunityId()
    );
    List<ConnectApi.ContentHubRepository> repos = hubCollection.repositories;
    for (ConnectApi.ContentHubRepository repo : repos) {
      if (repo.providerType.type == 'ContentHubSharepointOffice365') {
        sharepointRepoId = repo.id;
      }
    }
    return sharepointRepoId;
  }

  public class DriveItem {
    @TestVisible 
    public String name { get; private set; }
    @TestVisible 
    public String id { get; private set; }
    @TestVisible 
    public Integer size { get; private set; }
    @TestVisible 
    public FileFacet file { get; private set; }
    @TestVisible 
    public FolderFacet folder { get; private set; }
    @TestVisible 
    public ParentFacet parentReference { get; private set; }
    @TestVisible 
    public DateTime lastModifiedDateTime { get; private set; }
    @TestVisible 
    public DateTime createdDateTime { get; private set; }
    @TestVisible
    public String webUrl {get;private set;}
  }

  public class Site {
    @TestVisible
    public String id { get; private set; }
    @TestVisible
    public String name { get; private set; }
  }

  public class FileFacet {
    @TestVisible
    public String mimeType { get; private set; }
    @TestVisible
    public FileHashes hashes { get; private set; }
  }

  public class ParentFacet {
    @TestVisible
    public String driveId { get; private set; }
    @TestVisible
    public String driveType { get; private set; }
    @TestVisible
    public String id { get; private set; }
    @TestVisible
    public String path { get; private set; }
  }

  public class ResponseError {
    @TestVisible
    public ErrorFacet error { get; private set; }
  }

  class ErrorFacet {
    @TestVisible
    public String code { get; private set; }
    @TestVisible
    public String message { get; private set; }
  }

  @TestVisible 
  public class FolderFacet {
    public Integer childCount { get; set; }
  }

  public class FileHashes {
    public String quickXorHash { get; set; }
  }

  public class FileVersion {
    @AuraEnabled
    public String id { get; set; }
    @AuraEnabled
    public Decimal size { get; set; }
    @AuraEnabled
    public String lastModifiedDateTime {get; set;}
  }

  public class SharepointServiceException extends Exception {}
}