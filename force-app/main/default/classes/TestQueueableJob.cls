public with sharing class TestQueueableJob implements Queueable {

    public Integer invokeNumber {get;set;}
    public TestQueueableJob(Integer invokeNumber){
        this.invokeNumber = invokeNumber;
    }
    public void execute(QueueableContext context) {
        // Your processing logic here       
        System.debug(LoggingLevel.DEBUG, 'TestQueueableJob.execute.invokeNumber:>>>>>>>>>>' + this.invokeNumber);
        System.debug(LoggingLevel.DEBUG, ' LONG RUNNING JOB.... WEB CALLOUT..... ');
        // Chain this job to next job by submitting the next job
        this.invokeNumber ++;

        for (Integer i = 0; i < 5; i++) {
            if(this.invokeNumber==3){
                break;
            }
            System.enqueueJob(new TestQueueableJob(this.invokeNumber));    
        }
        
    }
}