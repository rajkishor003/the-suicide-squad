public with sharing class cswpCalBoxCertificateController {
    public String displayText {get; set;}
    public String linkID {get; set;}
    public String ReportNumber {get; set;}
    public String ProductDescription {get; set;}
    public String SystemSerialNumber {get; set;}
    public String CustomerName {get; set;}
    public String CalibrationDateTime {get; set;}
    public String TemperatureHumidity {get; set;}
    public String SoftwareVersion {get; set;}
    public String ResultFile {get; set;}
    public String SystemController {get; set;}
    public String AsDeliveredCondition {get; set;}
    public String OperatorName {get; set;}
    public String OperatorTitle {get; set;}
    public String OperatorRemark {get; set;}
    public String OperatorFailedRemark {get; set;}
    public String IssueDate {get; set;}
    public String ImageURL {get; set;}
    public User loggedInUser {get; set;}
    public String loggedInUserID {get; set;}
    public String pageLanguage {get; set;}
    public Boolean fullRes {get; set;}
    public boolean calibratedByAdvantest {get; set;}
    public String CalBoxDataID {get; set;}
    public String contactUserName {get; set;}
    public String contactUserTitle {get; set;}
    public String operatorRemarkSpecialRequirements {get; set;}

    public List<Cswp_Instrument__c> instrumentList {get; set;}
    public List<Cswp_Task__c> taskList {get; set;}
    public List<Cswp_Resource__c> resourceList {get; set;}
    public User createrUser {get; set;}

    public cswpCalBoxCertificateController() {
        pageLanguage = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('lang'));
        linkID       = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('linkID'));
        fullRes      = Boolean.valueOf(String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('fullRes')));
        CalBoxDataID = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('calBoxDataID'));
        loggedInUserID = UserInfo.getUserId();
        getSignature();
        setProperties();
    }

    public void setProperties() {
        //Set <String> calBoxDataIDs = CSWPSobjectSelector.getLinkedEntityIDOfContentDocument(linkID);
        Set<String> calBoxDataIDs = new Set<String>{ this.CalBoxDataID };
        String sbId;
        List<Cswp_Calibration_Box_Data__c> calBoxDataList = CSWPSobjectSelector.getCalBoxDataWithIdSet(calBoxDataIDs);
        List<Cswp_Service_Box__c> sbList = calBoxDataList[0].Cswp_Service_Boxes__r;

        String downloadingFileName = 'Certificate'+'_'+calBoxDataList[0].Cswp_Report_Number__c + '_' + DateTime.now().format('yyyyMMddhhmmss','GMT');
        Apexpages.currentPage().getHeaders().put( 'content-disposition', 'inline; filename=' + downloadingFileName );
        
        if(sbList.size()==0) {
            sbId = calBoxDataList[0].Cswp_Service_Box__c;
        } else {
            sbId = sbList[0].Id;
        }
        List<Cswp_System__c> sysList = calBoxDataList[0].Cswp_Systems__r;
        
        Set<Id> taskIds = new Set<Id>();
        for(Cswp_Task__c t : calBoxDataList[0].Cswp_Tasks__r) {
            taskIds.add(t.ID);
        }

        instrumentList = CSWPSobjectSelector.getInstrumentsWithServiceBoxIDs(new Set<ID>{sbId});
        if(fullRes) { resourceList = CSWPSobjectSelector.getResourcesWithTaskIDs(taskIds); }

        ReportNumber = calBoxDataList[0].Cswp_Report_Number__c;
        IssueDate = Datetime.Now().format('dd-MMM-yyyy');
        OperatorName = calBoxDataList[0].Cswp_Operator_Name__c;
        OperatorTitle = calBoxDataList[0].Cswp_Operator_Title__c;
        OperatorRemark = calBoxDataList[0].Cswp_Operator_Remark__c;
        OperatorFailedRemark = calBoxDataList[0].Cswp_Operator_Failed_Remark__c;
        
        String formattedTemp = calBoxDataList[0].Cswp_Temperature__c;
        String formattedHum = calBoxDataList[0].Cswp_Humidity__c;
        
        if(calBoxDataList[0].Cswp_Temperature__c.contains('.')) {
            Integer dotIdx = calBoxDataList[0].Cswp_Temperature__c.IndexOf('.');
            Integer blankIdx = calBoxDataList[0].Cswp_Temperature__c.IndexOf(' ');
            formattedTemp = calBoxDataList[0].Cswp_Temperature__c.substring(0, dotIdx + 2) + calBoxDataList[0].Cswp_Temperature__c.substring(blankIdx, calBoxDataList[0].Cswp_Temperature__c.length());
        }

        if(calBoxDataList[0].Cswp_Humidity__c.contains('.')) {
            Integer dotIdx = calBoxDataList[0].Cswp_Humidity__c.IndexOf('.');
            Integer blankIdx = calBoxDataList[0].Cswp_Humidity__c.IndexOf(' ');
            formattedHum = calBoxDataList[0].Cswp_Humidity__c.substring(0, dotIdx + 2) + calBoxDataList[0].Cswp_Humidity__c.substring(blankIdx, calBoxDataList[0].Cswp_Humidity__c.length());
        }        
        TemperatureHumidity = formattedTemp + ' / ' + formattedHum;
        
        
        ResultFile = calBoxDataList[0].Cswp_Local_Path__c;
        //CalibrationDateTime = String.valueOf(calBoxDataList[0].Cswp_Generation_Date__c);
        CalibrationDateTime = calBoxDataList[0].Cswp_Generation_Date__c.format('dd-MMM-yyyy HH:mm:ss');

        ProductDescription = Label.Cswp_V93000_Label;
        calibratedByAdvantest = calBoxDataList[0].Cswp_Calibrated_By_Advantest__c;
        AsDeliveredCondition = sysList[0].Cswp_As_Delivered_Condition__c;
        operatorRemarkSpecialRequirements = '';
        if(OperatorRemark != null && OperatorRemark.length() > 1) {
            operatorRemarkSpecialRequirements = OperatorRemark + '. ';
        }
        if(OperatorFailedRemark != null && OperatorFailedRemark.length() > 1) {
            operatorRemarkSpecialRequirements = operatorRemarkSpecialRequirements + OperatorFailedRemark + '.';
        }
        SystemController = sysList[0].Cswp_Host_Name__c;
        SoftwareVersion = sysList[0].Cswp_Smt_Version__c;
        SystemSerialNumber = sysList[0].Cswp_Serial_Number__c;

        createrUser = CSWPSobjectSelector.getUser(calBoxDataList[0].CreatedByID);

        //Setting Customer Name ACP3-23
        if(createrUser.Profile.Name.contains('Factory')) {
            CustomerName = 'Advantest Corporation';
        }
        else if(createrUser.Profile.Name.contains('FSE')) {
            CustomerName = CSWPSobjectSelector.getAccountWithIdSet(new Set<Id>{calBoxDataList[0].Cswp_FSE_Customer_Name__c})[0].Name;
        }
        else if(createrUser.Profile.Name.contains('Community')) {
            List<Group> pgList = CSWPSobjectSelector.getPublicGroupsOfUser(createrUser.Id);
            Set<Id> pgIds = new Set<Id>();
            for(Group pg : pgList) {
                pgIds.add(pg.Id);
            }
            List<Cswp_Group__c> csGroups= CSWPSobjectSelector.getCswpGroupsOfPublicGroups(pgIds);
            CustomerName = csGroups[0].Name;
        }

        contactUserName = createrUser.Contact.Name;
        contactUserTitle = createrUser.Contact.Title;
    }

    public void getSignature() {
        loggedInUser = CSWPSobjectSelector.getUser(loggedInUserID);
        List<ContentDocument> cd_List = new List<ContentDocument>();
        List<ContentVersion> cv_List = new List<ContentVersion>();

        if(loggedInUser != null && loggedInUser.ContactID != null) {
            cd_List = CSWPSobjectSelector.GetContentDocumentWithVersions(loggedInUser.ContactID, 'Signature');
            if(cd_List.size() > 0) {
                ImageURL ='/sfc/servlet.shepherd/version/download/'+cd_List[0].ContentVersions[0].Id;
            }
        }
    }
}