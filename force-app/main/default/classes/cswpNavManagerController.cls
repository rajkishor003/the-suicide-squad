public with sharing class cswpNavManagerController {

    @AuraEnabled (cacheable = true)
    public static List<navItemWrapper> getNavItemList() {
        List<navItemWrapper> navItemWrapperList = new List<navItemWrapper>();
        Integer counter = 0;
        for(CSWP_Home_Navigation_Item__mdt navItem :[SELECT Id, CSWP_Custom_Permission__c, CSWP_Description__c, CSWP_Image__c, CSWP_Link__c, CSWP_Link_Target__c, CSWP_Title__c, cswp_Sequence__c FROM CSWP_Home_Navigation_Item__mdt WHERE cswp_Is_Active__c = true ORDER BY cswp_Sequence__c ASC NULLS LAST]) {
            if(FeatureManagement.checkPermission(navItem.CSWP_Custom_Permission__c)) {
                String userLanguage = UserInfo.getLanguage();
                if(userLanguage.contains('en')) {
                    userLanguage = 'en';
                }
                navItemWrapperList.add(new navItemWrapper(parseTranslation(navItem.CSWP_Title__c, userLanguage), navItem.CSWP_Link__c, navItem.CSWP_Link_Target__c, navItem.CSWP_Image__c, String.valueOf(counter), parseTranslation(navItem.CSWP_Description__c, userLanguage)));
                counter++;
            }
        }
        return navItemWrapperList;
    }

    public class navItemWrapper {
        @AuraEnabled
        public String title {get;set;}
        @AuraEnabled
        public String link {get;set;}
        @AuraEnabled
        public String linkTarget {get;set;}
        @AuraEnabled
        public String image {get;set;}
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String text {get;set;}

        public navItemWrapper(String title, String link, String linkTarget, String image, String id, String text) {
            this.title = title;
            this.link = link;
            this.linkTarget = linkTarget;
            this.image = image;
            this.id = id;
            this.text = text;
        }
    }

    private static String parseTranslation(String textJSON, String userLanguage) {
        Map<String, Object> textMap = (Map<String, Object>) JSON.deserializeUntyped(textJSON);
        String translation = (String) textMap.get(userLanguage);
        return String.isNotBlank(translation) ? translation : (String) textMap.get('en');
    }
}