@isTest
private with sharing class cswpNavManagerController_Test {
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name='Test Account');
        insert acc;
    }
    @isTest
    static void testgetNavItemList(){
        List<Account> testaccount = [SELECT Id FROM Account LIMIT 1];
        User testuser = CSWPTestUtil.createCommunityUser(testaccount[0]);
        insert testuser;
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'CSWP_PortalUser_FAQ'];
        insert new PermissionSetAssignment(AssigneeId = testuser.id, PermissionSetId = ps.Id);
        List<cswpNavManagerController.navItemWrapper> navItems = new List<cswpNavManagerController.navItemWrapper>();
        System.runAs(testuser){
            navItems = cswpNavManagerController.getNavItemList();
        }
        System.assertNotEquals(navItems.size(),0,'navitems are retrieved');
    }
}