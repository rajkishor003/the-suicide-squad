/*************************************************************************************************************
 * @name			cswp_CalBoxDataFileMappingSelector.cls
 * @author			emre akkus <e.akkus@emakina.com.tr>
 * @created			12 / 11 / 2020
 * @description		cswp_CalBoxDataFileMappingSelector class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-16		emreakkus				Initial Version.
**************************************************************************************************************/
public with sharing class cswp_CalBoxDataFileMappingSelector {
    
    public static List<cswp_Cal_Box_Data_File_Obj_Mapping__mdt> GetCalBoxDataFileMapping() {
        return [Select Id, Label, cswp_source_tag__c, cswp_Target_Object__c, cswp_Is_Multiple__c, (Select Id, Label, Cswp_Cal_Box_Data_File_Obj_Mapping__c, Cswp_Source_Field__c, Cswp_Target_Field__c From Cswp_Cal_Box_Data_File_Field_Mappings__r) From cswp_Cal_Box_Data_File_Obj_Mapping__mdt];
    }
}