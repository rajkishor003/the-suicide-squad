/*************************************************************************************************************
 * @name			cswp_ContentVersionSelector.cls
 * @author			emre akkus <e.akkus@emakina.com.tr>
 * @created			12 / 11 / 2020
 * @description		cswp_ContentVersionSelector class
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-11-16		emreakkus				Initial Version.
**************************************************************************************************************/
public with sharing class cswp_ContentVersionSelector {
    public static List<ContentVersion> GetContentVersionWithDocumentId (Set<Id> documentIds) {
        return [Select ContentDocumentId,VersionData From ContentVersion Where ContentDocumentId IN: documentIds];
    }
}