public with sharing class cswp_Workspace_TriggerHandler 
    implements Triggers.Handler, Triggers.AfterInsert, Triggers.AfterUpdate {

    public Boolean criteria(Triggers.context context) {
        if(System.isBatch() || System.isFuture() || System.isQueueable()){
            return false;
        }
        return true;
    }

    public void afterInsert(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(cswp_Workspace_TriggerHandler.class.getName(), CSWPUtil.ON_AFTER_INSERT);
        if(isEnabled){
            then(context);
        }
    }

    public void afterUpdate(Triggers.context context) {
        Boolean isEnabled = CSWPConfigurationManager.IsTriggerEnabled(cswp_Workspace_TriggerHandler.class.getName(), CSWPUtil.ON_AFTER_UPDATE);
        if(isEnabled){
            then(context);
        }
    }

    public void then(Triggers.context context) {
        if(context.props.isAfter) {
            List<cswp_Workspace__c> newWorkspaceList = (List<cswp_Workspace__c>) context.props.newList;
            if(context.props.isInsert) {
                CSWP_WorkspaceSharing_Helper.addPermission(newWorkspaceList);
            } else if (context.props.isUpdate) {
                Map<Id, cswp_Workspace__c> oldWorkspaceMap = (Map<Id, cswp_Workspace__c>) context.props.oldMap;
                List<cswp_Workspace__c> workspaceToAddPermissionList = new List<cswp_Workspace__c> ();
                Map<Id, cswp_Workspace__c> workspaceToRemovePermissionMap = new Map<Id, cswp_Workspace__c> ();

                for(cswp_Workspace__c workspace : newWorkspaceList) {
                    if(workspace.cswp_Group__c != oldWorkspaceMap.get(workspace.Id).cswp_Group__c) {
                        workspaceToAddPermissionList.add(workspace);
                        workspaceToRemovePermissionMap.put(workspace.Id, oldWorkspaceMap.get(workspace.Id));
                    }
                    // If the is external url field changed update permissions
                    if(workspace.cswp_Is_External_URL__c != oldWorkspaceMap.get(workspace.Id).cswp_Is_External_URL__c) {
                        if(workspace.cswp_Is_External_URL__c){
                            if(!workspaceToAddPermissionList.contains(workspace)){
                                workspaceToAddPermissionList.add(workspace);
                            }
                        }
                        else {
                            if(!workspaceToRemovePermissionMap.keySet().contains(workspace.Id)){
                                workspaceToRemovePermissionMap.put(workspace.Id, oldWorkspaceMap.get(workspace.Id));
                            }
                        }
                    }

                    // End of external url field change
                }

                if(!workspaceToRemovePermissionMap.isEmpty()) {
                    CSWP_WorkspaceSharing_Helper.removePermission(workspaceToRemovePermissionMap);
                }
                if(!workspaceToAddPermissionList.isEmpty()) {
                    CSWP_WorkspaceSharing_Helper.addPermission(workspaceToAddPermissionList);
                }
            }
        }
    }
}