/**
 * File:        osf_AttachModel.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jan 13, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Model class for uploading a file
  ************************************************************************
 * History:
 */

public without sharing class osf_AttachModel {
    public String orderEncId {get;set;}
    public String attachmentId {get;set;}
    public String fileName {get;set;}
    public String contentType {get;set;}
    public String base64BlobValue {get;set;}
    public String cartEncId {get; set;}

    public osf_AttachModel(String orderEncId, String attachmentId, String fileName, String contentType, String base64BlobValue, String cartEncId) {
        this.orderEncId = orderEncId;
        this.attachmentId = attachmentId;
        this.fileName = fileName;
        this.contentType = contentType;
        this.base64BlobValue = base64BlobValue;
        this.cartEncId = cartEncId;
    }
}