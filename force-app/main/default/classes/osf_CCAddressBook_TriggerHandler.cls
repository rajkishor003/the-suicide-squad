/**
* File:        osf_CCAddressBook_TriggerHandler.cls
* Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
* Date:        June 10, 2020
* Created By:  Shikhar Srivastava
************************************************************************
* Description: Trigger handler for osf_CCAddressBook_Trigger
************************************************************************
* History:
*/
public class osf_CCAddressBook_TriggerHandler {
    public static Boolean isFirstTime = true;
    /**
    * @author Shikhar Srivastava
    * @date June 10, 2020
    * @name afterInsertUpdate
    * @description Logic to Manage Default Address Book
    * @see ccrz.ccLog
    * @param Map<Id, ccrz__E_AccountAddressBook__c>
    * @return void
    */
    public static void afterInsertUpdate(Map<Id, ccrz__E_AccountAddressBook__c> addressBookMap) {  
        Set<Id> accId = new Set<Id>();
        List<ccrz__E_AccountAddressBook__c> updateAddr = new List<ccrz__E_AccountAddressBook__c>();
        Boolean setDefaultShipping = false;
        Boolean setDefaultBilling = false;    
        
        try {
            for (ccrz__E_AccountAddressBook__c addr : addressBookMap.values()) {
                if (addr.ccrz__Default__c) {
                    if (addr.ccrz__AddressType__c == osf_constant_strings.ADDRESS_TYPE_SHIPPING) {
                        setDefaultShipping = true;
                        accId.add(addr.ccrz__Account__c);
                    } else if (addr.ccrz__AddressType__c == osf_constant_strings.ADDRESS_TYPE_BILLING) {
                        setDefaultBilling = true;
                        accId.add(addr.ccrz__Account__c);
                    }
                }             
            }
            if (!accId.isEmpty()) {
                for (ccrz__E_AccountAddressBook__c oldAddr : [SELECT Id, ccrz__AddressType__c, ccrz__Account__c, ccrz__Default__c FROM ccrz__E_AccountAddressBook__c WHERE Id != :addressBookMap.keySet() AND ccrz__Account__c = :accId AND ccrz__Default__c = true]) {
                    if ((setDefaultShipping && oldAddr.ccrz__AddressType__c == osf_constant_strings.ADDRESS_TYPE_SHIPPING)
                        || (setDefaultBilling && oldAddr.ccrz__AddressType__c == osf_constant_strings.ADDRESS_TYPE_BILLING)) {
                            oldAddr.ccrz__Default__c = false;
                            updateAddr.add(oldAddr);
                        }            
                }
                
                if (!updateAddr.isEmpty()) {
                    update updateAddr;
                } 
            }
        } catch(Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:CCAddressBook:TriggerHandler.afterInsertUpdate:Error', e.getMessage() + 'in line number: ' + e.getLineNumber());      
        }        
    }    
}