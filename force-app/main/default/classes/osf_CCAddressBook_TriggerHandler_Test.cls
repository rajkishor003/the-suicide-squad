/**
* File:        osf_CCAddressBook_TriggerHandler_Test.cls
* Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
* Date:        June 10, 2020
* Created By:  Shikhar Srivastava
************************************************************************
* Description: Test class for osf_CCAddressBook_TriggerHandler
************************************************************************
* History:
*/
@isTest
public without sharing class osf_CCAddressBook_TriggerHandler_Test {
    /**
    * @author Shikhar Srivastava
    * @date June 10, 2020
    * @name createTestData
    * @description Test method to create data
    * @return void
    */
    @TestSetup
    static void createTestData(){
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c shipTo1 = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo1);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        ccrz__E_ContactAddr__c billTo1 = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo1);
        Account acc = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(acc);
        Contact c1 = osf_testHelper.createContact('John', 'Doe', acc, 'test@email.com', '0000000000');
        database.insert(c1);
        User sysadmin = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true LIMIT 1];
        ccrz__E_AccountAddressBook__c shipping1 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_SHIPPING, shipTo, sysadmin);
        shipping1.ccrz__Default__c = true;
        ccrz__E_AccountAddressBook__c shipping2 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_SHIPPING, shipTo1, sysadmin);
        ccrz__E_AccountAddressBook__c billing1 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_BILLING, billTo, sysadmin);
        billing1.ccrz__Default__c = true;
        ccrz__E_AccountAddressBook__c billing2 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_BILLING, billTo1, sysadmin);
        Database.insert(new List<ccrz__E_AccountAddressBook__c> {shipping1, shipping2, billing1, billing2});
    }
    
    /**
    * @author Shikhar Srivastava
    * @date June 10, 2020
    * @name testMakeAddressDefaultShipping
    * @description Test Method to check only one Shipping Address can set as Default
    * @return void
    */
    @isTest   
    public static void testMakeAddressDefaultShipping() {
        User sysadmin = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true LIMIT 1];
        system.runAs(sysadmin) {
            List<ccrz__E_AccountAddressBook__c> shipAddr = [SELECT Name, ccrz__AddressType__c, ccrz__Default__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__AddressType__c = :osf_constant_strings.ADDRESS_TYPE_SHIPPING AND ccrz__Default__c = false];
            shipAddr[0].ccrz__Default__c = true;
            Update shipAddr[0];           
        }
        List<ccrz__E_AccountAddressBook__c> shipAddrRes = [SELECT ccrz__AddressType__c, ccrz__Default__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__AddressType__c = :osf_constant_strings.ADDRESS_TYPE_SHIPPING AND ccrz__Default__c = true];
        system.assert(shipAddrRes.size() == 1);      
    }
    
    /**
    * @author Shikhar Srivastava
    * @date June 10, 2020
    * @name testMakeAddressDefaultBilling
    * @description Test Method to check only one Billing Address can set as Default
    * @return void
    */
    @isTest   
    public static void testMakeAddressDefaultBilling() {
        User sysadmin = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true LIMIT 1];
        system.runAs(sysadmin) {
            List<ccrz__E_AccountAddressBook__c> billAddr = [SELECT Name, ccrz__AddressType__c, ccrz__Default__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__AddressType__c = :osf_constant_strings.ADDRESS_TYPE_BILLING AND ccrz__Default__c = false];
            billAddr[0].ccrz__Default__c = true;
            Update billAddr[0];           
        }
        List<ccrz__E_AccountAddressBook__c> billAddrRes = [SELECT ccrz__AddressType__c, ccrz__Default__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__AddressType__c = :osf_constant_strings.ADDRESS_TYPE_BILLING AND ccrz__Default__c = true];
        system.assert(billAddrRes.size() == 1);      
    }
}