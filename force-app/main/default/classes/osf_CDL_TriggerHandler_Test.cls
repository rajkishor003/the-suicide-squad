@isTest
public class osf_CDL_TriggerHandler_Test {
    
    @isTest
    public static void testContentDocumentLinkTrigger() {
        Account acc = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(acc);

        osf_quote__c quote = osf_testHelper.createQuote(200, acc);
        insert quote;

        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        insert contentVersion;

        List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument];

        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, contentDocumentList[0].Id);
        insert contentDocumentLink;

        System.assertEquals(osf_constant_strings.VISIBILITY_ALL_USERS, contentDocumentLink.Visibility);
    }

}