/**
* File:        osf_Contact_TriggerHandler
* Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
* Date:        Jan 06, 2020
* Created By:  Alina Craciunel
************************************************************************
* Description: Trigger handler class for Contact object
************************************************************************
* History:
*/

public without sharing class osf_Contact_TriggerHandler {
    //Static variable to skip handler in test classes.
    @TestVisible private static Boolean isEnabled = true;
    
    /**********************************************************************************************
    * @Name                 : createCommunityUsers
    * @Description          : creates community users based on the contacts created by DataSpider
    * @Created By           : Alina Craciunel
    * @Created Date         : Jan 06, 2020
    * @param lstContacts    : newly inserted contact records
    * @Return               : 
    *********************************************************************************************/
    public static void createCommunityUsers(Map<Id, Contact> mapNewContacts) {
        List<Contact> lstContactsAddedByDataSpider = new List<Contact>();
        Map<Id, Id> mapContactUserIds = new Map<Id, Id>();
        List<User> lstUsersToBeCreated = new List<User>();
        List<Profile> advanTestProfile = [SELECT Id FROM Profile WHERE Name =: osf_constant_strings.ADVANTEST_B2B_USER_PROFILE LIMIT 1];
        
        if (!advanTestProfile.isEmpty()) {
            for (User u : [SELECT Id, ContactId FROM User WHERE ContactId IN: mapNewContacts.keySet()]) {
                mapContactUserIds.put(u.ContactId, u.Id);
            }
            
            for (Contact c : mapNewContacts.values()) {
                if (c.osf_created_by_data_spider__c && c.osf_is_active__c && !mapContactUserIds.containsKey(c.Id)) {
                    lstContactsAddedByDataSpider.add(c);
                }
            }
            
            for (Contact c : lstContactsAddedByDataSpider) {
                User u = createUser(advanTestProfile[0], c);
                if (u == null) { continue; }
                lstUsersToBeCreated.add(u);
            }
            
            if (!lstUsersToBeCreated.isEmpty()) {
                Database.SaveResult[] saveResult = Database.insert(lstUsersToBeCreated, false);

                for(Database.SaveResult saveRes : saveResult) {
                    if (!saveRes.isSuccess()) {
                        for(Database.Error err : saveRes.getErrors()) {
                            throw new osf_Contact_TriggerHandlerException (err.getMessage());
                        }
                    }
                }
            }
        }
        
    }
    /**********************************************************************************************
    * @Name                 : createUser
    * @Description          : creates community user based on a contact
    * @Created By           : Alina Craciunel
    * @Created Date         : Jan 06, 2020
    * @param p              : user profile
    * @param c              : contact
    * @Return               : 
    *********************************************************************************************/
    private static User createUser(Profile p, Contact c) {
        User u = new User();
        u.ProfileId = p.Id;
        u.ContactId = c.Id;  
        u.IsActive = true;
        u.FirstName = c.FirstName;
        u.LastName = c.LastName;
        if(String.isBlank(c.Email)) return null;
        u.Username = c.Email;
        u.Email = c.Email;
        // Integer index = u.Email.indexOf('@');
        // if(index == -1) return null;
        u.CommunityNickname = u.Email;
        u.Alias = String.isNotBlank(c.FirstName) ? c.FirstName.substring(0,1) : '';
        u.Alias += c.LastName.substring(0,1);
        u.EmailEncodingKey = osf_constant_strings.CHARSET_UTF_8;
        String currencyCode, localeSidKey, languageLocaleKey, timeZone;
        if (c.MailingCountry != null) {
            currencyCode = osf_constant_strings.COUNTRY_CURRENCY_MAP.get(c.MailingCountry);
            localeSidKey = osf_constant_strings.COUNTRY_LOCALE_SID_MAP.get(c.MailingCountry);
            languageLocaleKey = osf_constant_strings.COUNTRY_LANGUAGE_LOCALE_MAP.get(c.MailingCountry);
            timeZone = osf_constant_strings.COUNTRY_TIMEZONE_MAP.get(c.MailingCountry);
        } else {
            currencyCode = osf_constant_strings.DEFAULT_CURRENCY_ISO_CODE;
            localeSidKey = osf_constant_strings.DEFAULT_LOCALE_SID;
            languageLocaleKey = osf_constant_strings.DEFAULT_LANGUAGE_LOCALE;
            timeZone = osf_constant_strings.DEFAULT_TIMEZONE;
        }
        u.ccrz__CC_CurrencyCode__c = currencyCode != null ? currencyCode : osf_constant_strings.DEFAULT_CURRENCY_ISO_CODE; 
        u.CurrencyIsoCode =  currencyCode != null ? currencyCode : osf_constant_strings.DEFAULT_CURRENCY_ISO_CODE; 
        u.LocaleSidKey = localeSidKey != null ? localeSidKey : osf_constant_strings.DEFAULT_LOCALE_SID; 
        u.LanguageLocaleKey = languageLocaleKey != null ? languageLocaleKey : osf_constant_strings.DEFAULT_LANGUAGE_LOCALE; 
        u.TimeZoneSidKey = timeZone != null ? timeZone : osf_constant_strings.DEFAULT_TIMEZONE; 
        return u;
    }
    public class osf_Contact_TriggerHandlerException extends Exception {}
}