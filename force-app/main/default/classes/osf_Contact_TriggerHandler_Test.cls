/**
 * File:        osf_Contact_TriggerHandler_Test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jan 06, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_Contact_TriggerHandler
  ************************************************************************
 * History:
 */
@isTest 
public without sharing class osf_Contact_TriggerHandler_Test {
    private static final String USERNAME = 'api_user@myadvantest.com';

    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        Account acc = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(acc);
    }

    /**********************************************************************************************
    * @Name         : testCreateCommunityUser
    * @Description  : Test createCommunityUser method
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 06, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCreateCommunityUser() {
        User apiUSer = [SELECT Id FROM User WHERE Username =: USERNAME];
        system.runAs(apiUSer) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            Contact c1 = osf_testHelper.createContact('John', 'Doe', acc, 'testJohnDoe@email.com', '0000000000');
            c1.MailingCountry = 'United States';
            Test.startTest();
            Database.insert(c1);
            Test.stopTest();
            List<User> lstUsers = [SELECT Id FROM User WHERE ContactId =: c1.Id];
            System.assertEquals(0, lstUsers.size());
        }
    }

    /**********************************************************************************************
    * @Name         : testCreateCommunityUser_createdByDataSpider
    * @Description  : Test createCommunityUser method
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 06, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCreateCommunityUser_createdByDataSpider() {
        User apiUSer = [SELECT Id FROM User WHERE Username =: USERNAME];
        system.runAs(apiUSer) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            Contact c2 = osf_testHelper.createContact('John', 'Toe', acc, 'testJohnToe@email.com', '1111111111');
            c2.MailingCountry = 'United States';
            c2.osf_created_by_data_spider__c = true;
            c2.osf_is_active__c = true;
            Test.startTest();
            Database.insert(c2);
            Test.stopTest();
            List<User> lstUsers = [SELECT Id FROM User WHERE ContactId =: c2.Id];
            System.assertEquals(1, lstUsers.size());
        }
    }

    /**********************************************************************************************
    * @Name         : testCreateCommunityUser_withoutCountry
    * @Description  : Test createCommunityUser method
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 06, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCreateCommunityUser_withoutCountry() {
        User apiUSer = [SELECT Id FROM User WHERE Username =: USERNAME];
        system.runAs(apiUSer) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            Contact c3 = osf_testHelper.createContact('John', 'Zoe', acc, 'testJohnZoe@email.com', '2222222222');
            c3.osf_created_by_data_spider__c = true;
            c3.MailingCountry = null;
            c3.osf_is_active__c = true;
            Test.startTest();
            Database.insert(c3);
            Test.stopTest();
            List<User> lstUsers = [SELECT Id, ccrz__CC_CurrencyCode__c, CurrencyIsoCode, LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey FROM User WHERE ContactId =: c3.Id];
            System.assertEquals(1, lstUsers.size());
            System.assertEquals(osf_constant_strings.DEFAULT_CURRENCY_ISO_CODE, lstUsers[0].ccrz__CC_CurrencyCode__c);
            System.assertEquals(osf_constant_strings.DEFAULT_CURRENCY_ISO_CODE, lstUsers[0].CurrencyIsoCode);
            System.assertEquals(osf_constant_strings.DEFAULT_LOCALE_SID, lstUsers[0].LocaleSidKey);
            System.assertEquals(osf_constant_strings.DEFAULT_LANGUAGE_LOCALE, lstUsers[0].LanguageLocaleKey);
            System.assertEquals(osf_constant_strings.DEFAULT_TIMEZONE, lstUsers[0].TimeZoneSidKey);
        }
    }

    /**********************************************************************************************
    * @Name         : testCreateCommunityUser_inactive
    * @Description  : Test createCommunityUser method
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 06, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCreateCommunityUser_inactive() {
        User apiUSer = [SELECT Id FROM User WHERE Username =: USERNAME];
        system.runAs(apiUSer) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            Contact c4 = osf_testHelper.createContact('John', 'Boe', acc, 'testJohnBoe@email.com', '3333333333');
            c4.MailingCountry = 'United States';
            c4.osf_created_by_data_spider__c = true;
            c4.osf_is_active__c = false;
            Test.startTest();
            Database.insert(c4);
            Test.stopTest();
            List<User> lstUsers = [SELECT Id FROM User WHERE ContactId =: c4.Id];
            System.assertEquals(0, lstUsers.size());
        }
    }

    /**********************************************************************************************
    * @Name         : testCreateCommunityUser_updateToActive
    * @Description  : Test createCommunityUser method
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 06, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCreateCommunityUser_updateToActive() {
        User apiUSer = [SELECT Id FROM User WHERE Username =: USERNAME];
        system.runAs(apiUSer) {
            Account acc = [SELECT Id FROM Account LIMIT 1];
            Contact c4 = osf_testHelper.createContact('John', 'Boe', acc, 'testJohnBoe@email.com', '3333333333');
            c4.MailingCountry = 'United States';
            c4.osf_created_by_data_spider__c = true;
            c4.osf_is_active__c = false;
            Database.insert(c4);
            c4.osf_is_active__c = true;
            Test.startTest();
            Database.update(c4);
            Test.stopTest();
            List<User> lstUsers = [SELECT Id FROM User WHERE ContactId =: c4.Id];
            System.assertEquals(1, lstUsers.size());
        }
    }
    
    /**********************************************************************************************
    * @Name         : testExceptionOnCreateCommunityUser
    * @Description  : Test Negative senario on createCommunityUser method
    * @Created By   : Shikhar Srivastava
    * @Created Date : July 01, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testExceptionOnCreateCommunityUser() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Contact c1 = osf_testHelper.createContact('John', 'Doe', acc, 'testJohnDoe@email.com', '0000000000');
        c1.MailingCountry = 'United States';
        c1.osf_created_by_data_spider__c = true;
        c1.osf_is_active__c = true;
        Database.insert(c1);
        List<User> lstUsers = [SELECT Id FROM User WHERE ContactId =: c1.Id];
        System.assertEquals(1, lstUsers.size());

        try {
            Contact c2 = osf_testHelper.createContact('JohnNew', 'ToeNew', acc, 'testJohnDoe@email.com', '1111111111');
            c2.MailingCountry = 'United States';
            c2.osf_created_by_data_spider__c = true;
            c2.osf_is_active__c = true;
            Database.insert(c2);            
        } catch (Exception e) {
            system.assert(e.getMessage() != null);
        }        
    }
}