/**
* Trigger Handler class for Content Document Link
* 
* @author Ozgun Eser
* @version 1.0
*/

public with sharing class osf_ContentDocumentLink_TriggerHandler {
    // Static variable to disable the handler for unit testing
    @TestVisible private static Boolean isEnabled = true;

    /**
    * Method for handling Before Insert
    *
    * @param contentDocumentLinkList, List of inserted ContentDocumentLink records.
    */
    public static void doBeforeInsert(List<ContentDocumentLink> contentDocumentLinkList) {
        if(!isEnabled) { return; }
        for(ContentDocumentLink contentDocumentLink : contentDocumentLinkList) {
            contentDocumentLink.Visibility = osf_constant_strings.VISIBILITY_ALL_USERS;
        }
    }
}