/**
 * File:        osf_myCartsController.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 23, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Controller class for osf_myCarts.page
  ************************************************************************
 * History:
 */

global with sharing class osf_DownloadLinksController {

    /**********************************************************************************************
    * @Name         : getAttachmentsMedias
    * @Description  : Insert a log when the attachment is downloaded
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @param ctx    : remote action context
    * @param attachId: the attachment id
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult getAttachmentsMedias(final ccrz.cc_RemoteActionContext ctx, String attachId) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
		res.inputContext = ctx;
        try {
            Attachment attach = [SELECT Id, ParentId, Name FROM Attachment WHERE Id =: attachId LIMIT 1];
            ccrz__E_ProductMedia__c media = [SELECT Id, ccrz__MediaType__c, ccrz__Product__r.ccrz__SKU__c FROM ccrz__E_ProductMedia__c WHERE Id =: attach.ParentId];
            
            osf_AttachmentDownloadLog__c log = new osf_AttachmentDownloadLog__c(
                osf_AttachmentId__c = attachId,
                osf_IPAddress__c = getUserIPAddress(ccrz.cc_CallContext.currUserId),
                osf_ProductMedia__c = attach.ParentId,
                osf_FileName__c = attach.Name,
                osf_FilePath__c = Site.getBaseUrl() + osf_constant_strings.SLASH + osf_constant_strings.DOWNLOAD_ATTACHMENT_PAGE + attachId,
                osf_media_type__c = media.ccrz__MediaType__c,
                osf_sku__c = media.ccrz__Product__r.ccrz__SKU__c
            );
            Database.insert(log);
            res.success = true; 
        } catch (Exception e) {
            res.data = e.getStackTraceString();
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:PDP:getAttachmentsMedias:Error', e);
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : getAttachmentsFromStaticResources
    * @Description  : Insert a log when the category attachment is downloaded
    * @Created By   : Alina Craciunel
    * @Created Date : JAn 29, 2020
    * @param ctx    : remote action context
    * @param attachName: the attachment name
    * @param attachURL: the attachment URL
    * @param categId: the category ID
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult getAttachmentsFromStaticResources(final ccrz.cc_RemoteActionContext ctx, String attachName, String attachURL, String categId) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
		res.inputContext = ctx;
        try {
            osf_AttachmentDownloadLog__c log = new osf_AttachmentDownloadLog__c(
                osf_IPAddress__c = getUserIPAddress(ccrz.cc_CallContext.currUserId),
                osf_FileName__c = attachName,
                osf_FilePath__c = Site.getBaseUrl().replace(osf_constant_strings.SLASH + osf_constant_strings.STOREFRONT_NAME, osf_constant_strings.EMPTY_STRING) + attachURL, 
                osf_category__c = categId
            );
            Database.insert(log);
            res.success = true; 
        } catch (Exception e) {
            res.data = e.getStackTraceString();
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:PLP:getAttachmentsFromStaticResources:Error', e);
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : getUserIPAddress
    * @Description  : Insert a log when the attachment is downloaded
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @param userId : currently logged in user id
    * @Return       : the ip address as a string
    *********************************************************************************************/
    private static String getUserIPAddress(String userId) {
	    string ReturnValue = '';
        List<AuthSession> lstAuthSessions = new List<AuthSession>([SELECT Id, SourceIp FROM AuthSession WHERE UsersId =: userId ORDER BY CreatedDate DESC]);
	    return lstAuthSessions.isEmpty() ? '' : lstAuthSessions[0].SourceIp;
    }
}