/**
 * File:        osf_DownloadLinksController_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 23, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_DownloadLinksController
  ************************************************************************
 * History:
 */

@isTest
public with sharing class osf_DownloadLinksController_test {

    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(account);
        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        database.insert(contact);
        User user = osf_testHelper.createCommunityUser(contact);
        database.insert(user);
        ccrz__E_Product__c prod = osf_testHelper.createCCProduct(osf_testHelper.PRODUCT_SKU, osf_testHelper.PRODUCT_NAME);
        Database.insert(prod);
        ccrz__E_ProductMedia__c media = osf_testHelper.createProductMedia(prod, osf_testHelper.LOCALE, osf_constant_strings.MEDIA_TYPE_THUBMNAIL);
        Database.insert(media);
        Attachment attach = osf_testHelper.createAttachment(media.Id, osf_testHelper.ATTACHMENT_NAME, osf_testHelper.ATTACHMENT_BODY);
        Database.insert(attach);
        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        Database.insert(cart);
        ccrz__E_Category__c categ = osf_testHelper.createCCCategory();
        Database.insert(categ);
    }

    /**********************************************************************************************
    * @Name         : testGetAttachmentsMedias
    * @Description  : Test method for getAttachmentsMedias method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
    public static void testGetAttachmentsMedias() {
    	osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        Attachment attach = [SELECT Id FROM Attachment WHERE Name =: osf_testHelper.ATTACHMENT_NAME LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_DownloadLinksController.getAttachmentsMedias(ctx, attach.Id);
        }
        Test.stopTest();
        List<osf_AttachmentDownloadLog__c> lstLogs = [SELECT Id FROM osf_AttachmentDownloadLog__c];
        System.assertEquals(true, res.success);
        System.assertEquals(true, lstLogs.size() > 0);
    }

    /**********************************************************************************************
    * @Name         : testGetAttachmentsFromStaticResources
    * @Description  : Test method for getAttachmentsFromStaticResources method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
    public static void testGetAttachmentsFromStaticResources() {
    	osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz__E_Category__c categ = [SELECT Id FROM ccrz__E_Category__c LIMIT 1];
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_DownloadLinksController.getAttachmentsFromStaticResources(ctx, 'test.txt', '/test.txt', '' + categ.Id);
        }
        Test.stopTest();
        List<osf_AttachmentDownloadLog__c> lstLogs = [SELECT Id FROM osf_AttachmentDownloadLog__c];
        System.assertEquals(true, res.success);
        System.assertEquals(true, lstLogs.size() > 0);
    }
}