/**
* Trigger Handler class for Chatter Feed Item.
* 
* @author Ozgun Eser
* @version 1.0
*/

public with sharing class osf_FeedItem_TriggerHandler {
    // Static variable to disable the handler for unit testing
    @TestVisible private static Boolean isEnabled = true;

     /**
    * Method for handling After Insert
    *
    * @param feedItemMap, Map of inserted Chatter Feed Item records.
    */
    public static void doAfterInsert(Map<Id, FeedItem> feedItemMap) {
        if(!isEnabled) { return; }
        if(System.isBatch() || System.isFuture() || System.isQueueable()) {
            sendEmail(feedItemMap.keySet());
        } else {
            sendEmailFuture(feedItemMap.keySet());
        }
    }

    @future
    private static void sendEmailFuture(Set<Id> feedItemIdSet) {
        sendEmail(feedItemIdSet);
    }

    /**********************************************************************************************
    * @Name         : sendEmail
    * @Description  : creates and send emails for RFQs and Orders chatter entries
    * @Created By   : Alina Craciunel (refactored Ozgun's code)
    * @Created Date : Nov 22, 2019
    * @param feedItemList: list of the feed items
    * @Return       : 
    *********************************************************************************************/
    private static void sendEmail(Set<Id> feedItemIdSet) {
        List<FeedItem> feedItemList = [SELECT Id, Body, ParentId, InsertedById, Parent.Type FROM FeedItem 
                                        WHERE (Parent.Type = :osf_constant_strings.REQUEST_FOR_QUOTE_API_NAME OR Parent.Type = :osf_constant_strings.ORDER_API_NAME) 
                                        AND Id IN :feedItemIdSet AND Type != 'AdvancedTextPost'];
        Set<String> requestForQuoteIdSet = new Set<String> ();
        Set<String> orderIdSet = new Set<String> ();
        for(FeedItem feedItem : feedItemList) {
            if (feedItem.Parent.Type == osf_constant_strings.REQUEST_FOR_QUOTE_API_NAME) {
                requestForQuoteIdSet.add(feedItem.ParentId);
            } else {
                orderIdSet.add(feedItem.ParentId);
            }
        }
        Map<String, User> mapParentObjectCreatedBy = new Map<String, User>();
        List<osf_request_for_quote__c> lstRFQs = new List<osf_request_for_quote__c>([SELECT Id, CreatedBy.Email, Name, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.LocaleSidKey, CreatedById FROM osf_request_for_quote__c WHERE Id IN :requestForQuoteIdSet]);
        List<ccrz__E_Order__c> lstOrders = new List<ccrz__E_Order__c>([SELECT Id, CreatedBy.Email, Name, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.LocaleSidKey, CreatedById FROM ccrz__E_Order__c WHERE Id IN :orderIdSet]);
        for (osf_request_for_quote__c rfq : lstRFQs) {
            mapParentObjectCreatedBy.put(rfq.Id, rfq.CreatedBy);
        }
        for (ccrz__E_Order__c order : lstOrders) {
            mapParentObjectCreatedBy.put(order.Id, order.CreatedBy);
        }
        Map<Id, osf_request_for_quote__c> requestForQuoteMap = new Map<Id, osf_request_for_quote__c>(lstRFQs);
        createAndSendEmails(feedItemList, mapParentObjectCreatedBy, requestForQuoteMap);
    }

    /**********************************************************************************************
    * @Name         : createAndSendEmails
    * @Description  : creates and send emails based on feed items
    * @Created By   : Alina Craciunel (refactored Ozgun's code)
    * @Created Date : Nov 22, 2019
    * @param feedItemList: list of the feed items
    * @param mapParentObjectCreatedBy: the map formed by parent object and user that created it
    * @Return       : 
    *********************************************************************************************/
    private static void createAndSendEmails(List<FeedItem> feedItemList, Map<String, User> mapParentObjectCreatedBy, Map<Id, osf_request_for_quote__c> requestForQuoteMap) {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage> ();
        EmailTemplate emailTemplateRFQ = [SELECT Id, Subject, HtmlValue FROM EmailTemplate WHERE Name = :osf_constant_strings.CHATTER_NOTIFICATION_EMAIL_TEMPLATE_NAME];
        EmailTemplate emailTemplateOrder = [SELECT Id FROM EmailTemplate WHERE Name = :osf_constant_strings.CHATTER_NOTIFICATION_EMAIL_TEMPLATE_NAME_ORDER];
        for(FeedItem feedItem : feedItemList) {
            if(!mapParentObjectCreatedBy.containsKey(feedItem.ParentId))  { continue; }
            User createdBy = mapParentObjectCreatedBy.get(feedItem.ParentId);
            if(feedItem.InsertedById == createdBy.Id) { continue; }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setCharset(osf_constant_strings.CHARSET_UTF_8);
            mail.setToAddresses(new List<String> {createdBy.Email});
            mail.setTargetObjectId(createdBy.Id); 
            mail.setUseSignature(false); 
            mail.setBccSender(false); 
            mail.setSaveAsActivity(false); 
            if (feedItem.parent.Type == osf_constant_strings.REQUEST_FOR_QUOTE_API_NAME) {
                osf_request_for_quote__c requestForQuote = requestForQuoteMap.get(feedItem.ParentId);
                mail.setTemplateID(emailTemplateRFQ.Id); 
                String subject = emailTemplateRFQ.Subject;
                subject = subject.replace(osf_constant_strings.REPLACE_REQUEST_FOR_QUOTE_NAME, requestForQuote.Name);
                String htmlBody = emailTemplateRFQ.HtmlValue;
                htmlBody = htmlBody.replace(osf_constant_strings.REPLACE_REQUEST_FOR_QUOTE_CONTACT_NAME, requestForQuote.CreatedBy.FirstName + osf_constant_strings.EMPTY_SPACE + requestForQuote.CreatedBy.LastName);
                htmlBody = htmlBody.replace(osf_constant_strings.REPLACE_REQUEST_FOR_QUOTE_NAME, requestForQuote.Name);
htmlBody = htmlBody.replace('{!$Label.osf_community_link}', Label.osf_community_link);
htmlBody = htmlBody.replace('{!osf_request_for_quote__c.Id}', requestForQuote.Id);


                mail.setSubject(subject);
                mail.setHtmlBody(htmlBody);
            } else {
                mail.setTemplateID(emailTemplateOrder.Id); 
            }
            mail.setWhatId(feedItem.ParentId);
            mailList.add(mail);
        }
        if(!mailList.isEmpty()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(mailList);
            if (results[0].success) {
                System.debug('The email was sent successfully');
            } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
            }
        }
    }
}