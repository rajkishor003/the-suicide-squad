@isTest
public class osf_FeedItem_TriggerHandler_Test {

    @isTest
    public static void testFeedItemTrigger() {
       insert osf_testHelper.createPageLabel(osf_constant_strings.QUOTE_IN_PROGRESS_CHATTER_MESSAGE_LABEL, 'In Progress');

        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Cart__c cart; 
        osf_request_for_quote__c requestForQuote;
        System.runAs(user) {
            cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
            insert cart;
            requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Quote Requested');
            insert requestForQuote;
        }

        // osf_request_for_quote__c rfq = [SELECT Id, osf_status__c FROM osf_request_for_quote__c];
        // rfq.osf_status__c = 'In Progress';
        // Test.startTest();
        // update rfq;
        // Test.stopTest();

        insert new FeedItem(
            ParentId = requestForQuote.Id,
            Body = 'Test Message',
            Type = 'TextPost',
            Status = osf_constant_strings.FEED_ITEM_STATUS_PUBLISHED,
            Visibility = osf_constant_strings.VISIBILITY_ALL_USERS
        );
    }
}