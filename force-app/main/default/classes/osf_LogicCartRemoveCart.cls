/**
 * File:        osf_LogicCartRemoveCart.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        June 17, 2020
 * Created By:  Shikhar Srivastava
  ************************************************************************
 * Description: Deletes cart
  ************************************************************************
 * History:
 * Date:                Modified By:            Description:
 */
global without sharing class osf_LogicCartRemoveCart extends ccrz.ccLogicCartRemoveCart {
    /* 
    * @description  : Delete Cart
    * @author       : Shikhar Srivastava
    * @createdDate  : June 17, 2020
    * @param        : Map<String, Object> inputData, B2B Commerce OOTB Input Data
    * @see          : ccrz.ccLog.log
    * @return       : Map<String, Object> output data
    */
    global override Map<String, Object> removeCart(Map<String, Object> inputData) {
        Map<String, Object> outputData = new Map<String, Object>();
        try{
            String cartId = (String) inputData.get(ccrz.ccAPICart.CART_ID);
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c, ccrz__ActiveCart__c, OwnerId, ccrz__CartStatus__c, osf_converted_from__c, osf_converted_from_rfq__c FROM ccrz__E_Cart__c WHERE Id = :cartId LIMIT 1];
            
            cart.ccrz__ActiveCart__c = false;
            cart.ccrz__CartStatus__c = osf_constant_strings.CART_CLOSED;
            update cart;
            
            outputData = inputData;
            outputData.put(ccrz.ccApi.SUCCESS, true);

        }catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:LogicCartRemoveCart:removeCart:Error', e);
        }        
        return outputData;
    }
}