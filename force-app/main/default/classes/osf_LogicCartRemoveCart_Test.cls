/**
 * File:        osf_LogicCartRemoveCart_Test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        June 17, 2020
 * Created By:  Shikhar Srivastava
   ************************************************************************
 * Description: Test Class For osf_LogicCartRemoveCart
  ************************************************************************
 * History:
 * Date:                Modified By:            Description:
*/
@IsTest
public class osf_LogicCartRemoveCart_Test {
    
    /* 
    * @description  : TestSetup
    * @author       : Shikhar Srivastava
    * @createdDate  : June 17, 2020
    * @Description  : Test Data creation
    */
    @TestSetup
    public static void createTestData() { 
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        insert product;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        cart.ccrz__Name__c = osf_constant_strings.CONVERTED_FROM + 'Test';
        insert cart;
        
        ccrz__E_Cart__c newCart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        newCart.ccrz__Name__c = 'NewCart';
        insert newCart;
        
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, osf_constant_strings.QUOTE_STATUS_QUOTE_REQUESTED);
        insert requestForQuote;

        ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
        cartItem.ccrz__AdjustmentAmount__c = -100;
        insert cartItem;
        
        ccrz__E_CartItem__c newCartItem = osf_testHelper.createCartItem(product, newCart, 1, 100);
        newCartItem.ccrz__AdjustmentAmount__c = -100;
        insert newCartItem;

        cart.osf_converted_from__c = requestForQuote.Id;
        update cart;        
    }
    
    /* 
    * @description  : testRemoveCartRFQ
    * @author       : Shikhar Srivastava
    * @createdDate  : June 17, 2020
    * @Description  : Method to test Delete RFQ Coverted Cart
    */
    @IsTest
    public static void testRemoveCartRFQ() {
        User user = [SELECT Id FROM User WHERE Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c, ccrz__Name__c FROM ccrz__E_Cart__c WHERE osf_converted_from_rfq__c = True AND ccrz__User__c = :user.Id LIMIT 1];
        osf_LogicCartRemoveCart logic = new osf_LogicCartRemoveCart();
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPICart.CART_ID => cart.Id,
            ccrz.ccAPI.SUCCESS => false,
            ccrz.ccApi.API_VERSION => 11,
            ccrz.ccLogic.ORIGINAL_INPUT => new Map<String, Object> {
              ccrz.ccAPICart.CART_ID => cart.Id,
              ccrz.ccApi.API_VERSION => 11              
            }                
        };
        Test.startTest();
        	Map<String, Object> outputData = logic.removeCart(inputData);
            System.assertEquals(outputData.get(ccrz.ccAPI.SUCCESS), true);
        Test.stopTest();
        ccrz__E_Cart__c delCart = [SELECT Id, ccrz__CartStatus__c, ccrz__EncryptedId__c, ccrz__Name__c FROM ccrz__E_Cart__c WHERE osf_converted_from_rfq__c = True AND ccrz__User__c = :user.Id LIMIT 1];
        System.assert(delCart.Id != Null);
        System.assertEquals(delCart.ccrz__CartStatus__c, osf_constant_strings.CART_CLOSED);
    }
}