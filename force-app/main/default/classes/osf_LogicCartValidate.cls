/**
 * File:        osf_LogicCartValidate.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 22, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: override logic provider to allow or prevent proceeding to Checkout 
  ************************************************************************
 * History:
 */
global with sharing class osf_LogicCartValidate extends ccrz.ccLogicCartValidate {
    /**********************************************************************************************
    * @Name         : processValidate
    * @Description  : override validate method and check osf_allow_checkout__c from contact
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 22, 2019
    * @param inputData: input data
    * @Return       : Map<String, Object>, inputData
    *********************************************************************************************/
    global override Map<String, Object> processValidate(Map<String, Object> inputData) {
        String userId = (String)inputData.get(ccrz.ccAPICart.BYOWNER);
        User u = [SELECT Id, Contact.osf_allow_checkout__c FROM User WHERE Id =: userId LIMIT 1];
        Map<String, Object> skipSz = new Map<String, Object>(inputData);
        skipSz.put(ccrz.ccApi.SIZING, new Map<String, Object> {
            ccrz.ccApiCart.ENTITYNAME => new Map<String, Object> { 
                ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_S,
                ccrz.ccApi.SZ_ASSC => FALSE,
                ccrz.ccApi.SZ_SKIPTRZ => FALSE
         }});
        Map<String,Object> fetchRes = ccrz.ccApiCart.fetch(skipSz);
        inputData.put(ccrz.ccApiCart.ALLOW_CHECKOUT, u.Contact.osf_allow_checkout__c);
        inputData.putAll(ccrz.ccApiCart.fetch(inputData));
        return inputData;
    }
}