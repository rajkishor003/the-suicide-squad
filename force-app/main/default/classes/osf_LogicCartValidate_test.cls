/**
 * File:        osf_LogicCartValidate_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 25, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_LogicCartValidate
  ************************************************************************
 * History:
 */

@isTest
public without sharing class osf_LogicCartValidate_test {
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        ccrz__E_Product__c prod = osf_testHelper.createCCProduct(osf_testHelper.PRODUCT_SKU, osf_testHelper.PRODUCT_NAME);
        Database.insert(prod);
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        User sysadmin = [SELECT Id, LanguageLocaleKey, UserRoleId FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true AND UserRoleId != null LIMIT 1];
        Account acc;
        Contact c1;
        system.runAs(sysadmin) {
            acc = osf_testHelper.createAccount('Test Company', '0000000000');
            database.insert(acc);
            c1 = osf_testHelper.createContact('John', 'Doe', acc, 'test@email.com', '0000000000');
            database.insert(c1);
            User user = osf_testHelper.createCommunityUser(c1);
            database.insert(user);
        }
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND ContactId = :c1.Id LIMIT 1];
        system.runAs(u1) {
            ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 10, osf_testHelper.TAX, osf_testHelper.TAX, acc, u1, c1);
            Database.insert(cart);
            ccrz__E_CartItem__c ci1 = osf_testHelper.createCartItem(prod, cart, 1, osf_testHelper.PRICE);
            Database.insert(ci1);
            ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
            Database.insert(order);
            ccrz__E_OrderItem__c orderItem = osf_testHelper.createCCOrderItem(order, prod, 1, osf_testHelper.PRICE);
            Database.insert(orderItem);
        }
    }

    /**********************************************************************************************
    * @Name         : testProcessValidate
    * @Description  : Test method for processValidate method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testProcessValidate() {
        osf_LogicCartValidate logicCartvalidate = new osf_LogicCartValidate();
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        c1.osf_allow_checkout__c = true;
        Database.update(c1);
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND ContactId = :c1.Id LIMIT 1];
        Map<String, Object> outputData;
        system.runAs(u1) {
            ccrz__E_Cart__c cart = [SELECT Id FROM ccrz__E_Cart__c LIMIT 1];
            Map<String, Object> inputData = new Map<String, Object> {
                ccrz.ccAPICart.BYOWNER => u1.Id,
                ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
                ccrz.ccAPICart.CART_ID => cart.Id
            };
            Test.startTest();
            outputData = logicCartvalidate.processValidate(inputData);
            Test.stopTest();
        }
        System.assertEquals(true, outputData.get(ccrz.ccApiCart.ALLOW_CHECKOUT));
    }

    /**********************************************************************************************
    * @Name         : testProcessValidate_doesntAllow
    * @Description  : Test method for processValidate method, doesn't allow checkout
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testProcessValidate_doesntAllow() {
        osf_LogicCartValidate logicCartvalidate = new osf_LogicCartValidate();
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND ContactId = :c1.Id LIMIT 1];
        Map<String, Object> outputData;
        system.runAs(u1) {
            ccrz__E_Cart__c cart = [SELECT Id  FROM ccrz__E_Cart__c LIMIT 1];
            Map<String, Object> inputData = new Map<String, Object> {
                ccrz.ccAPICart.BYOWNER => u1.Id,
                ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
                ccrz.ccAPICart.CART_ID => cart.Id
            };
            Test.startTest();
            outputData = logicCartvalidate.processValidate(inputData);
            Test.stopTest();
        }
        System.assertEquals(false, outputData.get(ccrz.ccApiCart.ALLOW_CHECKOUT));
    }
}