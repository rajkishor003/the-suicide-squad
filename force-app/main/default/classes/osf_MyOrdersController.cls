/**
 * File:        osf_MyOrdersController.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 4, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Controller class for osf_MyOrders.page
  ************************************************************************
 * History:
 */
global with sharing class osf_MyOrdersController {

    /**********************************************************************************************
    * @Name         : getPurchaseOrders
    * @Description  : Get all available purchase orders
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 06, 2019
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult getPurchaseOrders(final ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
		res.inputContext = ctx;
        try {
            Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
            res.data = new List<osf_PurchaseOrder__c>([SELECT Id, Name, osf_available_amount__c, osf_OriginalAmount__c, osf_ValidUntil__c, osf_PurchaseOrderId__c FROM osf_PurchaseOrder__c 
                                                WHERE osf_Contact__c =: (String) ccrz.cc_CallContext.currContact.Id AND osf_ValidUntil__c >=: Date.today() AND osf_available_amount__c > 0 AND osf_Status__c =: osf_constant_strings.ACTIVE AND RecordTypeId =: openPORecordTypeId]);
            res.success = true; 
        } catch(Exception e) {
			res.data = e;
            res.success = false;
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:osf_MyOrdersController:getPurchaseOrders:Error', e);
		} 
        return res;
    }
  
   /**********************************************************************************************
    * @Name         : makeAddressDefault
    * @Description  : make the address default
    * @Created By   : Alina Craciunel
    * @Created Date :Apr 08, 2020
    * @param ctx    : remote action context
    * @param ctx    : addressBookId
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult makeAddressDefault(final ccrz.cc_RemoteActionContext ctx, String addressId, String addressType) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
	    res.inputContext = ctx;
        try {
            List<ccrz__E_AccountAddressBook__c> lstAddressBooks = new List<ccrz__E_AccountAddressBook__c>([SELECT Id, ccrz__AddressType__c, ccrz__E_ContactAddress__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__Account__c =: ccrz.cc_CallContext.currAccountId]); 
            ccrz__E_AccountAddressBook__c selectedAddressBook;
            List<ccrz__E_AccountAddressBook__c> lstAddressBooksToUpdate = new List<ccrz__E_AccountAddressBook__c>();
            if (!lstAddressBooks.isEmpty()) {
                for (ccrz__E_AccountAddressBook__c addressBook : lstAddressBooks) {
                    if (addressBook.ccrz__E_ContactAddress__c == addressId && addressBook.ccrz__AddressType__c == addressType) {
                        selectedAddressBook = addressBook;
                        addressBook.ccrz__Default__c = true;
                        lstAddressBooksToUpdate.add(addressBook);
                    }
                }
                update lstAddressBooksToUpdate;
            }
            res.success = true; 
        } catch(Exception e) {
            res.data = e;
            res.success = false;
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:osf_MyOrdersController:makeAddressDefault:Error', e);
		}
        return res;
    }
}