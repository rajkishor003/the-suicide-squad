/**
 * File:        osf_MyOrdersController_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 25, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_MyOrdersController
  ************************************************************************
 * History:
 */
@isTest
public without sharing class osf_MyOrdersController_test {
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c shipTo1 = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo1);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        ccrz__E_ContactAddr__c billTo1 = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo1);
        User sysadmin = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true LIMIT 1];
        Account acc = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(acc);
        Contact c1 = osf_testHelper.createContact('John', 'Doe', acc, 'test@email.com', '0000000000');
        database.insert(c1);
        User user = osf_testHelper.createCommunityUser(c1);
        database.insert(user);
        osf_quote__c quote = osf_testHelper.createQuote(200, acc);
        Database.insert(quote);
        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion);
        List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument];
        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, contentDocumentList[0].Id);
        Database.insert(contentDocumentLink);
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, acc, u1, c1);
        Database.insert(cart);
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.osf_quote__c = quote.Id;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_PO_IN_PROCESS_STATUS;
        order.ccrz__Account__c = acc.Id;
        order.ccrz__Contact__c = c1.Id;
        Database.insert(order);
        osf_sales_order_acknowledgement__c soa = osf_testHelper.createSoa(acc);
        Database.insert(soa);
        ContentVersion contentVersion2 = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion2);
        contentVersion2 = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion2.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion2.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkSoa = osf_testHelper.createContentDocumentLink(soa.Id, lstContentDocuments[0].Id);
        Database.insert(contentDocumentLinkSoa);
        ccrz__E_AccountAddressBook__c shipping1 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_SHIPPING, shipTo, u1); 
        ccrz__E_AccountAddressBook__c shipping2 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_SHIPPING, shipTo1, u1);
        ccrz__E_AccountAddressBook__c billing1 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_BILLING, billTo, u1);
        ccrz__E_AccountAddressBook__c billing2 = osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_BILLING, billTo1, u1);
        Database.insert(new List<ccrz__E_AccountAddressBook__c> {shipping1, shipping2, billing1, billing2});
        Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        osf_PurchaseOrder__c openPO = osf_testHelper.createPurchaseOrder(c1, 1000, osf_testHelper.CURRENCY_ISO_CODE_USD, '001');
        openPO.RecordTypeId = openPORecordTypeId;
        Database.insert(openPO);
        ContentVersion contentVersion3 = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion3);
        contentVersion3 = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion3.Id LIMIT 1];
        lstContentDocuments = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion3.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkPo = osf_testHelper.createContentDocumentLink(openPO.Id, lstContentDocuments[0].Id);
        Database.insert(contentDocumentLinkPo);
    }

    /**********************************************************************************************
    * @Name         : testGetPurchaseOrders
    * @Description  : Test method for getPurchaseOrders method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetPurchaseOrders() {
        Contact c1 = [SELECT Id FROM Contact Limit 1];
        Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        Id newPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.NEW_PO_RECORD_TYPE).getRecordTypeId();
        osf_PurchaseOrder__c openPO = osf_testHelper.createPurchaseOrder(c1, 1000, osf_testHelper.CURRENCY_ISO_CODE_USD, '003');
        openPO.RecordTypeId = openPORecordTypeId;
        Database.insert(openPO);

        osf_PurchaseOrder__c newPO = osf_testHelper.createPurchaseOrder(c1, 1000, osf_testHelper.CURRENCY_ISO_CODE_USD, '002');
        newPO.RecordTypeId = newPORecordTypeId;
        Database.insert(newPO);

        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Order__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ctx.queryParams.put(osf_constant_strings.ORDER_QUERYSTRING_PARAM, order.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_MyOrdersController.getPurchaseOrders(ctx);
        }
        Test.stopTest();
        List<osf_PurchaseOrder__c> poList = (List<osf_PurchaseOrder__c>)res.data;
        System.assertEquals(2, poList.size());
    }

/**********************************************************************************************
    * @Name         : testMakeAddressDefault
    * @Description  : Test method for makeAddressDefault method
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 08, 2020
    * @Param        : 
    * @Return       : 
 *********************************************************************************************/
  @isTest   
	public static void testMakeAddressDefault() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_AccountAddressBook__c shipping1 = [SELECT Id, ccrz__E_ContactAddress__c, ccrz__AddressType__c FROM ccrz__E_AccountAddressBook__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_RemoteActionResult res;
        system.runAs(u1) {
            Test.startTest();
            res = osf_MyOrdersController.makeAddressDefault(ctx, shipping1.ccrz__E_ContactAddress__c, shipping1.ccrz__AddressType__c);
            Test.stopTest();
        }
        List<ccrz__E_AccountAddressBook__c> lstAddressBooks = [SELECT Id, ccrz__Default__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__Default__c = true];
        system.assertEquals(true, res.success);
        system.assertEquals(1, lstAddressBooks.size());
    }
}