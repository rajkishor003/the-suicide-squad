global without sharing class osf_OrderViewController {
    
    /**********************************************************************************************
    * @Name         : getOrderInfo
    * @Description  : Get order info
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 6, 2019
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult getOrderInfo(final ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.inputContext = ctx;
        res.data = false;
        String orderEncId = ctx.queryParams.get(osf_constant_strings.ORDER_QUERYSTRING_PARAM);
        try {
            List<ccrz__E_Order__c> lstOrders = new List<ccrz__E_Order__c>([SELECT Id, ccrz__OrderStatus__c, osf_PurchaseOrder__c, osf_PurchaseOrder__r.osf_Status__c 
                                                                        FROM ccrz__E_Order__c WHERE ccrz__EncryptedId__c =: orderEncId]);
            if (!lstOrders.isEmpty()) {
                OrderModel orderModel = new OrderModel(lstOrders[0]); 
                orderModel.quoteAttachmentId = osf_utility.getQuoteAttachId(orderEncId);
                orderModel.poAttachmentId = osf_utility.getPoAttachId(orderEncId);
                orderModel.soaAttachmentId = osf_utility.getSoaAttachId(orderEncId);
                orderModel.showDownloadQuote = orderModel.quoteAttachmentId != null;
                orderModel.showDownloadPo = orderModel.poAttachmentId != null;
                orderModel.showDownloadSoa = orderModel.soaAttachmentId != null;
                res.data = orderModel;
            }
        } catch(Exception e) {
            res.data = false;
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:orderViewController.getOrderInfo:Error', e.getMessage() + 'in line number: ' + e.getLineNumber());
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : rejectQuote
    * @Description  : reject quote
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 7, 2019
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult rejectQuote(final ccrz.cc_RemoteActionContext ctx, String message) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.inputContext = ctx;
        String orderEncId = ctx.queryParams.get(osf_constant_strings.ORDER_QUERYSTRING_PARAM);
        try {
            List<ccrz__E_Order__c> lstOrders = new List<ccrz__E_Order__c>([SELECT Id, ccrz__Account__r.OwnerId, ccrz__Account__r.Owner.Email 
                                                                        FROM ccrz__E_Order__c WHERE ccrz__EncryptedId__c =: orderEncId]);
            if (!lstOrders.isEmpty()) {
                lstOrders[0].ccrz__OrderStatus__c = osf_constant_strings.ORDER_CANCELLED_STATUS;
                Database.update(lstOrders[0]);
                FeedItem feedItem = osf_utility.createFeedItem(lstOrders[0].Id, message, false);
                Database.insert(feedItem);
                createAndSendRejectQuoteEmail(lstOrders[0].ccrz__Account__r.Owner.Email, lstOrders[0].ccrz__Account__r.OwnerId, lstOrders[0].Id);
            }
            res.success = true;
        } catch(Exception e) {
            res.data = e;
            res.success = false;
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:orderViewController.rejectQuote:Error', e.getMessage() + 'in line number: ' + e.getLineNumber());
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : createAndSendRejectQuoteEmail
    * @Description  : creates and send reject quote email to SA
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 28, 2020
    * @param 
    * @Return       : 
    *********************************************************************************************/
    private static void createAndSendRejectQuoteEmail(String accountOwnerEmail, Id accountOwnerId, Id orderId) {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage> ();
        EmailTemplate emailTemplateOrder = [SELECT Id FROM EmailTemplate WHERE Name = :osf_constant_strings.REJECT_QUOTE_EMAIL_TEMPLATE];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setCharset(osf_constant_strings.CHARSET_UTF_8);
        mail.setToAddresses(new List<String> {accountOwnerEmail});
        mail.setTargetObjectId(accountOwnerId); 
        mail.setUseSignature(false); 
        mail.setBccSender(false); 
        mail.setSaveAsActivity(false); 
        mail.setTemplateID(emailTemplateOrder.Id); 
        mail.setWhatId(orderId);
        mailList.add(mail);
        if(!mailList.isEmpty()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(mailList);
        }
    }

    /**********************************************************************************************
    * @Name         : acceptQuote
    * @Description  : accept quote
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 7, 2019
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult acceptQuote(final ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.inputContext = ctx;
        String orderEncId = ctx.queryParams.get(osf_constant_strings.ORDER_QUERYSTRING_PARAM);
        try {
            List<ccrz__E_Order__c> lstOrders = new List<ccrz__E_Order__c>([SELECT Id FROM ccrz__E_Order__c WHERE ccrz__EncryptedId__c =: orderEncId]);
            if (!lstOrders.isEmpty()) {
                lstOrders[0].ccrz__OrderStatus__c = osf_constant_strings.ORDER_ACCEPTD_STATUS;
                Database.update(lstOrders[0]);
            }
            res.success = true;
        } catch(Exception e) {
            res.data = e;
            res.success = false;
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : attachBlob
    * @Description  : attach files to a PO set as parameter
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 7, 2019
    * @param attach : serialized attachment model
    * @Return       : the attachment id
    *********************************************************************************************/
    @RemoteAction
    global static String attachBlob(String attach){
        return osf_utility.attachBlob(attach);
    }

    public class OrderModel {
        public Boolean showRejectQuote {get;set;} //cancel order process
        public Boolean showUploadPO {get;set;}
        public Boolean showAcceptQuote {get;set;}
        public Boolean showDownloadQuote {get;set;}
        public Boolean showDownloadPo {get;set;}
        public Boolean showDownloadSoa {get;set;}
        public String quoteAttachmentId {get; set;}
        public String poAttachmentId {get; set;}
        public String soaAttachmentId {get; set;}

        public OrderModel(ccrz__E_Order__c order) {
            showRejectQuote = showUploadPO = showAcceptQuote = showDownloadQuote = showDownloadPo = showDownloadSoa = false;
            if(String.isBlank(order.ccrz__OrderStatus__c)) return;
            if (order.ccrz__OrderStatus__c.toLowerCase() == osf_constant_strings.ORDER_WAITING_PO_STATUS.toLowerCase()) { 
                showUploadPO = true;
                showRejectQuote = true;
            } 
        }
    }
}