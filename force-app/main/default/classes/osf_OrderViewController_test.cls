/**
 * File:        osf_OrderViewController_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 25, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_OrderViewController
  ************************************************************************
 * History:
 */
@isTest
public without sharing class osf_OrderViewController_test {
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        Account acc = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(acc);
        Contact c1 = osf_testHelper.createContact('John', 'Doe', acc, 'test@email.com', '0000000000');
        database.insert(c1);
        User u1 = osf_testHelper.createCommunityUser(c1);
        database.insert(u1);
        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, acc, u1, c1);
        insert cart;
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        
        osf_quote__c quote = osf_testHelper.createQuote(200, acc);
        Database.insert(quote);
        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion);
        contentVersion = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments1 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion.ContentDocumentId];
        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, lstContentDocuments1[0].Id);
        Database.insert(contentDocumentLink);

        osf_sales_order_acknowledgement__c soa = osf_testHelper.createSoa(acc);
        Database.insert(soa);
        ContentVersion contentVersion2 = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion2);
        contentVersion2 = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion2.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments2 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion2.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkSoa = osf_testHelper.createContentDocumentLink(soa.Id, lstContentDocuments2[0].Id);
        Database.insert(contentDocumentLinkSoa);
        
        Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        osf_PurchaseOrder__c po = osf_testHelper.createPurchaseOrder(c1, osf_testHelper.AVAILABLE_PO_AMOUNT, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        po.RecordTypeId = openPORecordTypeId;
        po.osf_BlockedAmount__c = 1000;
        Database.insert(po);
        ContentVersion contentVersion3 = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion3);
        contentVersion3 = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion3.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments3 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion3.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkPo = osf_testHelper.createContentDocumentLink(po.Id, lstContentDocuments3[0].Id);
        Database.insert(contentDocumentLinkPo);

        ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_WAITING_PO_STATUS;
        order.ccrz__Account__c = acc.Id;
        order.osf_quote__c = quote.Id;
        order.ccrz__Contact__c = c1.Id;
        order.osf_PurchaseOrder__c = po.Id;
        order.osf_sales_order_acknowledgement__c = soa.Id;
        order.ccrz__OriginatedCart__c = cart.Id;
        Database.insert(order);
        
    }

    /**********************************************************************************************
    * @Name         : testGetOrderInfo
    * @Description  : Test method for getOrderInfo method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetOrderInfo() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c, osf_sales_order_acknowledgement__c, osf_quote__c, osf_PurchaseOrder__c FROM ccrz__E_Order__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ctx.queryParams.put(osf_constant_strings.ORDER_QUERYSTRING_PARAM, order.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_OrderViewController.getOrderInfo(ctx);
        }
        Test.stopTest();
        osf_OrderViewController.OrderModel orderModel = (osf_OrderViewController.OrderModel)res.data;
        System.assertEquals(true, orderModel.showRejectQuote);
        System.assertEquals(true, orderModel.showDownloadQuote);
        System.assertEquals(false, orderModel.showAcceptQuote);
        System.assertEquals(true, orderModel.showUploadPO);
        
        ContentDocumentLink cdl = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: order.osf_sales_order_acknowledgement__c];
        ContentVersion cv = [SELECT PathOnClient, VersionData, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: cdl.ContentDocumentId];
        System.assertEquals(cv.Id, orderModel.soaAttachmentId);

        cdl = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: order.osf_quote__c];
        cv = [SELECT PathOnClient, VersionData, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: cdl.ContentDocumentId];
        System.assertEquals(cv.Id, orderModel.quoteAttachmentId);

        cdl = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: order.osf_PurchaseOrder__c];
        cv = [SELECT PathOnClient, VersionData, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: cdl.ContentDocumentId];
        System.assertEquals(cv.Id, orderModel.poAttachmentId);
    }

    /**********************************************************************************************
    * @Name         : testRejectQuote
    * @Description  : Test method for rejectQuote method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testRejectQuote() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Order__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ctx.queryParams.put(osf_constant_strings.ORDER_QUERYSTRING_PARAM, order.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_OrderViewController.rejectQuote(ctx, 'test rejection message');
        }
        Test.stopTest();
        order = [SELECT Id, ccrz__OrderStatus__c FROM ccrz__E_Order__c LIMIT 1];
        System.assertEquals(true, res.success);
        System.assertEquals(osf_constant_strings.ORDER_CANCELLED_STATUS, order.ccrz__OrderStatus__c);
    }

    /**********************************************************************************************
    * @Name         : testAcceptQuote
    * @Description  : Test method for acceptQuote method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testAcceptQuote() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Order__c LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ctx.queryParams.put(osf_constant_strings.ORDER_QUERYSTRING_PARAM, order.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_OrderViewController.acceptQuote(ctx);
        }
        Test.stopTest();
        order = [SELECT Id, ccrz__OrderStatus__c FROM ccrz__E_Order__c LIMIT 1];
        System.assertEquals(true, res.success);
        System.assertEquals(osf_constant_strings.ORDER_ACCEPTD_STATUS, order.ccrz__OrderStatus__c);
    }

    /**********************************************************************************************
    * @Name         : testAttachBlob
    * @Description  : Test method for attachBlob method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testAttachBlob() {
        osf_testHelper.setupStorefront();
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c LIMIT 1];
        String attachmentId;
         osf_AttachModel attachModel = new osf_AttachModel(order.ccrz__EncryptedId__c, null, 'test.txt', null, 'body', null);
        Test.startTest();
        attachmentId = osf_OrderViewController.attachBlob(JSON.serialize(attachModel));
        Test.stopTest();
        System.assertNotEquals(null, attachmentId);
        order = [SELECT Id, osf_PurchaseOrder__c, osf_PurchaseOrder__r.RecordTypeId FROM ccrz__E_Order__c LIMIT 1];
        System.assertNotEquals(null, order.osf_PurchaseOrder__c);
        Id newPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        System.assertEquals(newPORecordTypeId, order.osf_PurchaseOrder__r.RecordTypeId);

        ContentDocumentLink cdl = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: order.osf_PurchaseOrder__c LIMIT 1];
        System.assertNotEquals(null, cdl);

        ContentVersion contentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: cdl.ContentDocumentId];
        System.assertNotEquals(null, contentVersion);
    }

}