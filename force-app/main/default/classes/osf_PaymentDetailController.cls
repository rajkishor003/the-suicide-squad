/**
 * File:        osf_PaymentDetailController.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 28, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Controller class for osf_newPOPayment.page
  ************************************************************************
 * History:
 */
global with sharing class osf_PaymentDetailController {
    
    /**********************************************************************************************
    * @Name         : checkCreditLimit
    * @Description  : Check the credit limit for the current user
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 28, 2019
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult getPurchaseOrders(final ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
		res.inputContext = ctx;
        try {
            List<ccrz__E_Cart__c> lstCarts = new List<ccrz__E_Cart__c>([SELECT Id, ccrz__TotalAmount__c, ccrz__CurrencyISOCode__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: (String) ccrz.cc_CallContext.currCartId]);
            if (!lstCarts.isEmpty()) {
                Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
                res.data = new PurchaseOrderModel(new List<osf_PurchaseOrder__c>([SELECT Id, Name, osf_PurchaseOrderId__c FROM osf_PurchaseOrder__c 
                                            WHERE osf_Status__c =: osf_constant_strings.ACTIVE AND osf_ValidUntil__c >=: Date.today() AND osf_available_amount__c >: lstCarts[0].ccrz__TotalAmount__c AND osf_CurrencyISOCode__c =: lstCarts[0].ccrz__CurrencyISOCode__c
                                            AND osf_Contact__c =: (String)ccrz.cc_CallContext.currContact.Id AND RecordTypeId =: openPORecordTypeId]));
            }
            res.success = true; 
        } catch(Exception e) {
			res.data = e;
			res.success = false;
		}
        return res;
    }

    /**********************************************************************************************
    * @Name         : attachBlob
    * @Description  : attach files to a PO set as parameter
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 28, 2020
    * @param attach : serialized attachment model
    * @Return       : the attachment id
    *********************************************************************************************/
    @RemoteAction
    global static String attachBlob(String attach){
        return osf_utility.attachBlob(attach);
    }

    public class PurchaseOrderModel {
        public List<osf_PurchaseOrder__c> purchaseOrdersList { get; set; }
        public Boolean displayExistingPOs { get; set; }
        public PurchaseOrderModel(List<osf_PurchaseOrder__c> purchaseOrdersList) {
            this.purchaseOrdersList = purchaseOrdersList;
            this.displayExistingPOs = purchaseOrdersList.isEmpty() ? false : true;
        }
    }
}