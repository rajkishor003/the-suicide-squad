/**
 * File:        osf_PaymentDetailController_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 1, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_PaymentDetailController
  ************************************************************************
 * History:
 */

@isTest
public with sharing class osf_PaymentDetailController_test {
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        User sysadmin = [SELECT Id, LanguageLocaleKey, UserRoleId FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true AND UserRoleId != null LIMIT 1];
        system.runAs(sysadmin) {
            Account account = osf_testHelper.createAccount('Test Company', '0000000000');
            database.insert(account);
            Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
            database.insert(contact);
            User user = osf_testHelper.createCommunityUser(contact);
            database.insert(user);
            ccrz__E_Product__c product = osf_testHelper.createCCProduct('111', 'test prod');
            database.insert(product);
        }
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        system.runAs(u1) {
            Account acc = [SELECT Id FROM Account LIMIT 1];  
            Contact c = [SELECT Id FROM Contact LIMIT 1];
            ccrz__E_Product__c p =[SELECT Id FROM ccrz__E_Product__c LIMIT 1];
            ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, acc, u1, c);
            database.insert(cart);
            ccrz__E_CartItem__c  cartItem = osf_testHelper.createCartItem(p, cart, 1, 100);
            database.insert(cartItem);
        }
    }

    /**********************************************************************************************
    * @Name         : testGetPurchaseOrders_NoPurchaseOrders
    * @Description  : Test method for getPurchaseOrders method, no purchase orders
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 1, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetPurchaseOrders_NoPurchaseOrders() {
        osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_RemoteActionResult res;
        system.runAs(u1) {
            Test.startTest();
            res = osf_PaymentDetailController.getPurchaseOrders(ctx);
            Test.stopTest();
        }
        System.assertEquals(true, res.success);
        System.assertEquals(true, ((osf_PaymentDetailController.PurchaseOrderModel)res.data).purchaseOrdersList.isEmpty());
    }

    /**********************************************************************************************
    * @Name         : testGetPurchaseOrders_InsufficientAmount
    * @Description  : Test method for getPurchaseOrders method, insufficient available amount
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 1, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetPurchaseOrders_InsufficientAmount() {
        osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        osf_PurchaseOrder__c po = osf_testHelper.createPurchaseOrder(c1, 0, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        Database.insert(po);
        ccrz.cc_RemoteActionResult res;
        system.runAs(u1) {
            Test.startTest();
            res = osf_PaymentDetailController.getPurchaseOrders(ctx);
            Test.stopTest();
        }
        System.assertEquals(true, res.success);
        System.assertEquals(true, ((osf_PaymentDetailController.PurchaseOrderModel)res.data).purchaseOrdersList.isEmpty());
    }

    /**********************************************************************************************
    * @Name         : testGetPurchaseOrders_DifferentContact
    * @Description  : Test method for getPurchaseOrders method, the existing purchase order has another contact
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 1, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetPurchaseOrders_DifferentContact() {
        osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        Account acc = [SELECT Id FROM Account LIMIT 1];  
        Contact c2 = osf_testHelper.createContact('Johnny', 'Dooe', acc, 'test1@email.com', '1111111111');
        database.insert(c2);
        osf_PurchaseOrder__c po = osf_testHelper.createPurchaseOrder(c2, 100, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        Database.insert(po);
        ccrz.cc_RemoteActionResult res;
        system.runAs(u1) {
            Test.startTest();
            res = osf_PaymentDetailController.getPurchaseOrders(ctx);
            Test.stopTest();
        }
        System.assertEquals(true, res.success);
        System.assertEquals(true, ((osf_PaymentDetailController.PurchaseOrderModel)res.data).purchaseOrdersList.isEmpty());
    }

    /**********************************************************************************************
    * @Name         : testGetPurchaseOrders
    * @Description  : Test method for getPurchaseOrders method, valid purchase order
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 1, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetPurchaseOrders() {
        osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c, ccrz__TotalAmount__c, ccrz__CurrencyISOCode__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        ccrz.cc_RemoteActionResult res;
        Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        Id newPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.NEW_PO_RECORD_TYPE).getRecordTypeId();
        system.runAs(u1) {
            osf_PurchaseOrder__c openPO = osf_testHelper.createPurchaseOrder(c1, 1000, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
            openPO.RecordTypeId = openPORecordTypeId; 
            Database.insert(openPO);
            osf_PurchaseOrder__c newPO = osf_testHelper.createPurchaseOrder(c1, 1000, osf_testHelper.CURRENCY_ISO_CODE_USD, '2');
            newPO.RecordTypeId = newPORecordTypeId;
            Database.insert(newPO);
            Test.startTest();
            res = osf_PaymentDetailController.getPurchaseOrders(ctx);
            Test.stopTest();
        }
        System.assertEquals(true, res.success);
        System.assertEquals(1, ((osf_PaymentDetailController.PurchaseOrderModel)res.data).purchaseOrdersList.size());
    }

    /**********************************************************************************************
    * @Name         : testAttachBlob
    * @Description  : Test method for attachBlob method
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 28, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testAttachBlob() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        String attachmentId;
         osf_AttachModel attachModel = new osf_AttachModel(null, null, 'test.txt', null, 'body', cart.ccrz__EncryptedId__c);
        Test.startTest();
        attachmentId = osf_OrderViewController.attachBlob(JSON.serialize(attachModel));
        Test.stopTest();
        System.assertNotEquals(null, attachmentId);
        cart = [SELECT Id, ccrz__EncryptedId__c, osf_PurchaseOrder__c, osf_PurchaseOrder__r.RecordTypeId FROM ccrz__E_Cart__c LIMIT 1];
        System.assertNotEquals(null, cart.osf_PurchaseOrder__c);
        Id newPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.NEW_PO_RECORD_TYPE).getRecordTypeId();
        System.assertEquals(newPORecordTypeId, cart.osf_PurchaseOrder__r.RecordTypeId);
        ContentDocumentLink cdl = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: cart.osf_PurchaseOrder__c];
        System.assertNotEquals(null, cdl);
        ContentVersion contentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: cdl.ContentDocumentId];
        System.assertNotEquals(null, contentVersion);
    }
}