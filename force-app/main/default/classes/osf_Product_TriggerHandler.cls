/**
 * File:        osf_Product_TriggerHandler.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        May 26, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Trigger Handler for CC Product trigger
  ************************************************************************
 * History:
 */

public with sharing class osf_Product_TriggerHandler {
    
    /**********************************************************************************************
    * @Name         : runCategoriesJob
    * @Description  : Run categories job only if something significant has been changed in the product definition
    * @Created By   : Alina Craciunel
    * @Created Date : May 26, 2020
    * @param        : Map<Id, ccrz__E_Product__c> newProductsMap
    * @param        : Map<Id, ccrz__E_Product__c> oldProductsMap
    * @param        : Boolean isUpdate
    * @Return       : 
    *********************************************************************************************/
    public static void runCategoriesJob(Map<Id, ccrz__E_Product__c> newProductsMap, Map<Id, ccrz__E_Product__c> oldProductsMap, Boolean isUpdate) {
        Boolean runJob = false;
        if (isUpdate) {
            for(ccrz__E_Product__c product : newProductsMap.values()) {
                ccrz__E_Product__c oldProduct = oldProductsMap.get(product.Id);
                if(product.ccrz__EndDate__c != oldProduct.ccrz__EndDate__c || product.ccrz__StartDate__c != oldProduct.ccrz__StartDate__c || product.ccrz__Storefront__c != oldProduct.ccrz__Storefront__c || product.ccrz__ProductStatus__c != oldProduct.ccrz__ProductStatus__c) {
                    runJob = true;
                    break;
                }
            }
        } else {
            runJob = true;
        }
        if (runJob) {
            Database.executeBatch(new osf_allowedCategoryJob(osf_constant_strings.STOREFRONT_NAME, osf_constant_strings.CURRENCY_LIST, osf_constant_strings.COUNTRY_LOCALE_SID_MAP.values()), 10);
        }
    }
}