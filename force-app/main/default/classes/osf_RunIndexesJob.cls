/**
 * File:        osf_RunIndexesJob
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jan 30, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Class for refreshing the indexes
  ************************************************************************
 * History:
 */

public class osf_RunIndexesJob {
    
    /**********************************************************************************************
    * @Name         : runRefreshIndexes
    * @Description  : refresh everything
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    public static void runRefreshIndexes() {
        refreshProductIndex();
        refreshProductSpecs();
        refreshCategoryTreeCache();
        refreshSiteIndex();
        refreshMenuIndex();
        refreshPageLabelIndex();
    }

    /**********************************************************************************************
    * @Name         : refreshProductIndex
    * @Description  : refresh the products indexes
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void refreshProductIndex() {
        Map<String, Object> res = ccrz.ccProductIndexBuilder.build(new Map<String, Object> {
            ccrz.ccAPIProductIndex.LOCALES => new Set<String>{osf_constant_strings.US_LOCALE, osf_constant_strings.EN_LANGUAGE, 
                                                            osf_constant_strings.JP_LOCALE, osf_constant_strings.JA_LANGUAGE, 
                                                            osf_constant_strings.CN_LOCALE, osf_constant_strings.ZH_LANGUAGE },
            ccrz.ccAPIProductIndex.CLEAN_ONLY => TRUE});
    }

    /**********************************************************************************************
    * @Name         : refreshProductSpecs
    * @Description  : refresh the product specs indexes
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void refreshProductSpecs() {
        Database.executeBatch(new ccrz.cc_job_LoadProductSpecIndex('SELECT Id, ParentProduct__c, ccrz__ProductId__c, ccrz__SKU__c, FilterData__c FROM E_Product__c'));
    }

    /**********************************************************************************************
    * @Name         : refreshCategoryTreeCache
    * @Description  : refresh the categories indexes
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void refreshCategoryTreeCache() {
        if(!Test.isRunningTest()) {
            (new ccrz.ccCategoryCacheBuilder()).build(new Map<String, Object> {ccrz.ccCategoryCacheBuilder.STOREFRONT => osf_constant_strings.STOREFRONT_NAME});

        }
    }

    /**********************************************************************************************
    * @Name         : refreshSiteIndex
    * @Description  : refresh the site indexes
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void refreshSiteIndex() {
        if(!Test.isRunningTest()) {
            ccrz.ccSiteIndexBuilder.build(new Map<String, Object> {ccrz.ccSiteIndexBuilder.STOREFRONT => osf_constant_strings.STOREFRONT_NAME});
        }
    }

    /**********************************************************************************************
    * @Name         : refreshMenuIndex
    * @Description  : refresh the menu indexes
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void refreshMenuIndex() {
        if(!Test.isRunningTest()) {
            (new ccrz.ccMenuCacheBuilder()).build(new Map<String,Object>{ccrz.ccMenuCacheBuilder.STOREFRONT => osf_constant_strings.STOREFRONT_NAME});
        }
    }

    /**********************************************************************************************
    * @Name         : refreshPageLabelIndex
    * @Description  : refresh the page labels indexes
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void refreshPageLabelIndex() {
        (new ccrz.ccPageLabelCacheBuilder()).build(new Map<String,Object>()); 
    }
}