/**
 * File:        osf_ShareCartController.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 07, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Controller class for osf_CartHeader.page
  ************************************************************************
 * History:
 */

global without sharing class osf_ShareCartController {

    /**********************************************************************************************
    * @Name         : getRelatedUsers
    * @Description  : Get the users under the same account as the current user
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult getRelatedUsers(final ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
		res.inputContext = ctx;
        try {
            List<Id> lstContactIds = new List<Id>();
            if ((String)ccrz.cc_CallContext.currAccountId != null && (String)ccrz.cc_CallContext.currUserId != null) {
                for (Account a : [SELECT Id, (SELECT Id FROM Contacts) FROM Account WHERE Id =: (String)ccrz.cc_CallContext.currAccountId]) {
                    for (Contact c : a.Contacts) {
                        lstContactIds.add(c.Id);
                    }
                }
                if (!lstContactIds.IsEmpty()) {
                    res.data = new List<User>([SELECT Id, Name FROM User WHERE ContactId IN: lstContactIds AND Id !=: (String)ccrz.cc_CallContext.currUserId]);
                    res.success = true; 
                }
            }
        } catch(Exception e) {
			res.data = e;
			res.success = false;
		} 
        return res;
    }

    /**********************************************************************************************
    * @Name         : getRelatedUsers
    * @Description  : Share the current cart with the selected users
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @param ctx, remote action context
    * @param sUserIds, serialized selected users list
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult share(final ccrz.cc_RemoteActionContext ctx, String sUserIds) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.success = false; 
		res.inputContext = ctx;
        String cartId = (String) ccrz.cc_CallContext.currCartId;
        if (String.isBlank(cartId) || String.isBlank(sUserIds)) {
			res.success = false;
            return res;
        }
        List<String> lstUserIds = (List<String>)System.JSON.deserialize(sUserIds, List<String>.class);
        List<User> lstUsers = new List<User>([SELECT Id, Username, ContactId, Email FROM User WHERE Id IN: lstUserIds]);
        List<ccrz__E_Cart__c> lstCarts = new List<ccrz__E_Cart__c>([
            SELECT Id, Name, ccrz__Name__c, ccrz__ShipDiscountAmount__c, ccrz__AdjustmentAmount__c, osf_converted_from__c, ccrz__TaxAmount__c, ccrz__TotalDiscount__c, (SELECT Id, ccrz__AdjustmentAmount__c, ccrz__SubAmount__c, ccrz__OriginalItemPrice__c, ccrz__Price__c, ccrz__AbsoluteDiscount__c, ccrz__PercentDiscount__c, ccrz__RecurringPrice__c, ccrz__RecurringPriceSubAmt__c, ccrz__Product__c FROM ccrz__E_CartItems__r), (SELECT Id, ccrz__Coupon__r.ccrz__CouponCode__c FROM ccrz__E_CartCoupons__r)  FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: cartId
        ]);
        if (lstUsers.isEmpty() || lstCarts.isEmpty()) {
			res.success = false;
            return res;
        }
        Savepoint sp = Database.setSavepoint();
        Boolean wasSuccessful = false;
        try {
            List<String> lstClonedCartsIds = new List<String>();
            wasSuccessful = cloneCart(lstCarts[0].Id, lstUsers, lstClonedCartsIds);
            if (wasSuccessful) {
                List<ccrz__E_Cart__c> lstClonedCarts = transferCartsAmounts(lstClonedCartsIds, lstCarts[0]);
                if (lstCarts[0].ccrz__E_CartCoupons__r != null && !lstCarts[0].ccrz__E_CartCoupons__r.isEmpty()) {
                    applyCoupons(lstCarts[0].ccrz__E_CartCoupons__r[0], lstClonedCartsIds);
                }
                transferCartsOwner(lstClonedCarts, lstUsers, lstCarts[0].osf_converted_from__c); 
                activateAndRenameOriginalCart(lstCarts[0]);
            }
            res.success = wasSuccessful;
        } catch(Exception e) {
			res.data = e;
			res.success = false; 
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:cart:share:Error', e);
            res.messages.add(osf_utility.createBeanMessage(e));
		} 
        if(!wasSuccessful){
            Database.rollback(sp);
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : cloneCart
    * @Description  : Clone the current cart
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @param cartId : current cart id
    * @param lstUsers: selected users list
    * @param lstClonedCartsIds: cloned carts ids list, output param
    * @Return       : Boolean ccrz.ccApi.SUCCESS
    *********************************************************************************************/
    @TestVisible 
    private static Boolean cloneCart(String cartId, List<User> lstUsers, List<String> lstClonedCartsIds) {
        Map<String, Object> inputData = new Map<String,Object> {
            ccrz.ccApiCart.CART_ID => cartId,
            ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION
        };
        for (User u : lstUsers) {
            Map<String, Object> outputData = ccrz.ccAPICart.cloneCart(inputData);
            lstClonedCartsIds.add((String)outputData.get(ccrz.ccApiCart.CART_ID));
            if (!(Boolean)outputData.get(ccrz.ccApi.SUCCESS)) {
                return false;
            }
        }
        return true;
    }

    /**********************************************************************************************
    * @Name         : applyCoupons
    * @Description  : Apply coupon to cloned carts
    * @Created By   : Alina Craciunel
    * @Created Date : Dec 06, 2019
    * @param lstClonedCartsIds: cloned carts ids list
    * @param cartCoupon: original cart coupon code
    * @Return       : 
    *********************************************************************************************/
    private static void applyCoupons(ccrz__E_CartCoupon__c cartCoupon, List<String> lstClonedCartsIds) {
        for (String cartId : lstClonedCartsIds) {
            Map<String, Object> inputData = new Map<String,Object>{
                ccrz.ccApi.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
                ccrz.ccApiCart.CART_ID => cartId,
                ccrz.ccApiCoupon.CODE => cartCoupon.ccrz__Coupon__r.ccrz__CouponCode__c
            };
            Map<String, Object> outputData = ccrz.ccAPICoupon.apply(inputData);
        }
    }

     /**********************************************************************************************
    * @Name         : transferCartsAmounts
    * @Description  : Transfer the cloned carts amounts
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @param lstClonedCartsIds: cloned carts ids list
    * @param currentCart: current cart
    * @Return       : the cloned carts
    *********************************************************************************************/
    @TestVisible 
    private static List<ccrz__E_Cart__c> transferCartsAmounts(List<String> lstClonedCartsIds, ccrz__E_Cart__c currentCart) {
        String siteUrl = getMyCartsUrl();
        Map<Id, ccrz__E_CartItem__c> mapProductCartItem = new Map<Id, ccrz__E_CartItem__c>();
        for (ccrz__E_CartItem__c cartItem : currentCart.ccrz__E_CartItems__r) {
            mapProductCartItem.put(cartItem.ccrz__Product__c, cartItem);
        }
        List<ccrz__E_Cart__c> lstCarts = new List<ccrz__E_Cart__c>([SELECT Id, Name, ccrz__EncryptedId__c, (SELECT Id, ccrz__Product__c FROM ccrz__E_CartItems__r) 
                                                                    FROM ccrz__E_Cart__c WHERE Id IN: lstClonedCartsIds]);
        List<ccrz__E_CartItem__c> lstCartItems = new List<ccrz__E_CartItem__c>();
        if (!lstCarts.isEmpty()) {
            for (Integer i=0; i<lstCarts.size(); i++) {
                lstCarts[i].ccrz__ActiveCart__c = false;
                lstCarts[i].ccrz__Name__c = osf_constant_strings.SHARED_BY + ccrz.cc_CallContext.currUser.Username + osf_constant_strings.EMPTY_SPACE + lstCarts[i].Name;
                lstCarts[i].osf_cartURL__c = siteUrl;
                lstCarts[i].ccrz__AdjustmentAmount__c = currentCart.ccrz__AdjustmentAmount__c;
                lstCarts[i].ccrz__TaxAmount__c = currentCart.ccrz__TaxAmount__c;
                lstCarts[i].ccrz__TotalDiscount__c = currentCart.ccrz__TotalDiscount__c;
                for (ccrz__E_CartItem__c cartItem : lstCarts[i].ccrz__E_CartItems__r) {
                    cartItem.ccrz__AdjustmentAmount__c = mapProductCartItem.get(cartItem.ccrz__Product__c).ccrz__AdjustmentAmount__c;
                    cartItem.ccrz__SubAmount__c = mapProductCartItem.get(cartItem.ccrz__Product__c).ccrz__SubAmount__c;
                    cartItem.ccrz__OriginalItemPrice__c = mapProductCartItem.get(cartItem.ccrz__Product__c).ccrz__OriginalItemPrice__c;
                    cartItem.ccrz__Price__c = mapProductCartItem.get(cartItem.ccrz__Product__c).ccrz__Price__c;
                    cartItem.ccrz__RecurringPrice__c = mapProductCartItem.get(cartItem.ccrz__Product__c).ccrz__RecurringPrice__c;
                    cartItem.ccrz__RecurringPriceSubAmt__c = mapProductCartItem.get(cartItem.ccrz__Product__c).ccrz__RecurringPriceSubAmt__c;
                    lstCartItems.add(cartItem);
                }
            }
            Database.update(lstCarts, false);
            Database.update(lstCartItems, false);
        }
        return lstCarts;
    }

    /**********************************************************************************************
    * @Name         : transferCartsOwner
    * @Description  : Transfer the cloned carts to appropriate users & clear bill to and ship to addresses
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @param lstCarts: cloned carts list
    * @param lstUsers: selected users list
    * @param convertedFrom: original RFQ
    * @Return      
    *********************************************************************************************/
    @TestVisible 
    private static void transferCartsOwner(List<ccrz__E_Cart__c> lstCarts, List<User> lstUsers, Id convertedFrom) {
        if (!lstCarts.isEmpty()) {
            for (Integer i=0; i<lstCarts.size(); i++) {
                lstCarts[i].ccrz__User__c = lstUsers[i].Id;
                lstCarts[i].ccrz__Contact__c = lstUsers[i].ContactId;
                lstCarts[i].OwnerId = lstUsers[i].Id;
                lstCarts[i].ccrz__ShipTo__c = null;
                lstCarts[i].ccrz__BillTo__c = null;
                lstCarts[i].osf_sharedCart__c = true;
                lstCarts[i].osf_converted_from__c = convertedFrom;
            }
            Database.update(lstCarts, false);
        }
    }

    /**********************************************************************************************
    * @Name         : getMyCartsUrl
    * @Description  : Get the URL for My Carts page
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @Return       : MyCarts location from Account
    *********************************************************************************************/
    private static String getMyCartsUrl() { 
        List<Site> lstSites = new List<Site>([SELECT GuestUserId, Name, Subdomain, OptionsRequireHttps, UrlPathPrefix 
                                                FROM Site WHERE Status =: osf_constant_strings.ACTIVE AND UrlPathPrefix !=: osf_constant_strings.EMPTY_STRING]);
        List<Domain> lstDomains = new List<Domain>([SELECT Domain FROM Domain]);
        String siteFullUrl = osf_constant_strings.HTTP;
        if(!lstSites.isEmpty() && !lstDomains.isEmpty()) {
            if(lstSites[0].Subdomain == osf_constant_strings.ADVANTEST) {
                if(lstSites[0].OptionsRequireHttps == true) {
                    siteFullUrl = osf_constant_strings.HTTPS;
                }
                siteFullUrl += lstDomains[0].Domain + osf_constant_strings.SLASH + lstSites[0].UrlPathPrefix + osf_constant_strings.SLASH + osf_constant_strings.CCRZ_MYACCOUNT_URL;
            }
        }  
        return siteFullUrl;
    }

    /**********************************************************************************************
    * @Name         : activateAndRenameOriginalCart
    * @Description  :  Activate and rename the current cart
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 07, 2019
    * @param currentCart: current cart
    * @Return       : 
    *********************************************************************************************/
    @TestVisible 
    private static void activateAndRenameOriginalCart(ccrz__E_Cart__c currentCart) {
        Map<String, Object> inputDataToActivateOriginalCart = new Map<String, Object>{
            ccrz.ccApiCart.CART_ID => currentCart.Id,
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPICart.BYOWNER => ccrz.cc_CallContext.currUserId
        };
        ccrz.ccAPICart.setActive(inputDataToActivateOriginalCart);
        String cartName = osf_constant_strings.SHARED_BY + ccrz.cc_CallContext.currUser.Username + osf_constant_strings.EMPTY_SPACE + currentCart.Name;
        if (cartName != currentCart.ccrz__Name__c) {
            currentCart.ccrz__Name__c = cartName;
            Database.update(currentCart);
        }
    }
}