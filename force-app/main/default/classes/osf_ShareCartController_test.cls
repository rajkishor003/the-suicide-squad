/**
 * File:        osf_ShareCartController_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 17, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_ShareCartController
  ************************************************************************
 * History:
 */

@isTest 
public with sharing class osf_ShareCartController_test {

    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        User sysadmin = [SELECT Id, LanguageLocaleKey, UserRoleId FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true AND UserRoleId != null LIMIT 1];
        system.runAs(sysadmin) {
            Account account = osf_testHelper.createAccount(osf_testHelper.ACCOUNT_NAME1, '0000000000');
            database.insert(account);
            Contact contact1 = osf_testHelper.createContact('John', 'Doe', account, osf_testHelper.USER_EMAIL1, '0000000000');
            database.insert(contact1);
            User user1 = osf_testHelper.createCommunityUser(contact1);
            user1.ccrz__CC_CurrencyCode__c = osf_testHelper.CURRENCY_ISO_CODE_USD;
            database.insert(user1);
            Contact contact2 = osf_testHelper.createContact('Johnny', 'Doey', account, osf_testHelper.USER_EMAIL2, '1111111111');
            database.insert(contact2);
            User user2 = osf_testHelper.createCommunityUser(contact2);
            database.insert(user2);
            ccrz__E_Product__c product = osf_testHelper.createCCProduct('111', 'test prod');
            database.insert(product);
        }
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        system.runAs(u1) {
            Account acc = [SELECT Id FROM Account LIMIT 1];  
            Contact c = [SELECT Id FROM Contact LIMIT 1];
            ccrz__E_Product__c p =[SELECT Id FROM ccrz__E_Product__c LIMIT 1];
            ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 10, 100, 10, acc, u1, c);
            database.insert(cart);
            ccrz__E_CartItem__c  cartItem = osf_testHelper.createCartItem(p, cart, 1, osf_testHelper.PRICE);
            database.insert(cartItem);
        }
    }

    /**********************************************************************************************
    * @Name         : testGetRelatedUsers
    * @Description  : Test method for getRelatedUsers method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testGetRelatedUsers() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_RemoteActionResult res;
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        system.runAs(u1) {
            Test.startTest();
            res = osf_ShareCartController.getRelatedUsers(ctx);
            Test.stopTest();
        }
        System.assertEquals(true, res.success);
        System.assertEquals(u1.Id, ccrz.cc_CallContext.currUserId);
        System.assertEquals(acc.Id, ccrz.cc_CallContext.currAccountId);
    }

    /**********************************************************************************************
    * @Name         : testShare
    * @Description  : Test method for share method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testShare() {
        osf_testHelper.setupStorefront();
        User u2 = [SELECT Id, LanguageLocaleKey FROM User WHERE Username =: osf_testHelper.USER_EMAIL2 LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionResult res;
        System.runAs(u1) {
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
            ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
            Test.startTest();
            res = osf_ShareCartController.share(ctx, JSON.serialize(new List<String>{u2.Id}));
            Test.stopTest();
        }
        System.assertEquals(true, res.success);
    }

    /**********************************************************************************************
    * @Name         : testClone
    * @Description  : Test method for clone method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testClone() {
        osf_testHelper.setupStorefront();
        User u2 = [SELECT Id, LanguageLocaleKey FROM User WHERE Username =: osf_testHelper.USER_EMAIL2 LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
        List<String> lstClonedCartsIds = new List<String>();
        system.runAs(u1) {
            Test.startTest();
            osf_ShareCartController.cloneCart(cart.Id, new List<User>{u2}, lstClonedCartsIds);
            Test.stopTest();
        }
        System.assertEquals(1, lstClonedCartsIds.size());
    }

    /**********************************************************************************************
    * @Name         : testTransferAmounts
    * @Description  : Test method for transferAmounts method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testTransferAmounts() {
        osf_testHelper.setupStorefront();
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        Contact c1 = [SELECT Id FROM Contact WHERE Email =: osf_testHelper.USER_EMAIL1];
        User u1 = [SELECT Id, LanguageLocaleKey, ContactId FROM User WHERE Username =: osf_testHelper.USER_EMAIL1 LIMIT 1];
        User u2 = [SELECT Id, LanguageLocaleKey, ContactId FROM User WHERE Username =: osf_testHelper.USER_EMAIL2 LIMIT 1];
        ccrz__E_Product__c prod =[SELECT Id FROM ccrz__E_Product__c LIMIT 1];
        List<ccrz__E_Cart__c> lstTransferedCarts;
        ccrz__E_Cart__c cart1;
        system.runAs(u1) {
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
            ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
            ccrz__E_Cart__c cart2 = osf_testHelper.createCart(null, null, 10, 100, 100, acc, u1, c1);
            Database.insert(cart2);
            ccrz__E_CartItem__c ci2 = osf_testHelper.createCartItem(prod, cart2, 1, 100);
            Database.insert(ci2);
            cart1 = [SELECT Id, Name, ccrz__EncryptedId__c, ccrz__TotalAmount__c, ccrz__AdjustmentAmount__c, ccrz__TaxAmount__c, ccrz__TotalDiscount__c, osf_converted_from__c,
                        (SELECT Id, ccrz__Product__c, ccrz__AdjustmentAmount__c, ccrz__SubAmount__c, ccrz__OriginalItemPrice__c, ccrz__Price__c, ccrz__RecurringPrice__c, ccrz__RecurringPriceSubAmt__c FROM ccrz__E_CartItems__r) 
                        FROM ccrz__E_Cart__c WHERE Id =: cart.Id LIMIT 1];
            Test.startTest();
            lstTransferedCarts = osf_ShareCartController.transferCartsAmounts(new List<String>{cart2.Id}, cart1);
            Test.stopTest();
        }
        ccrz__E_Cart__c transferedCart = [SELECT Id, ccrz__TaxAmount__c FROM ccrz__E_Cart__c WHERE Id =: lstTransferedCarts[0].Id LIMIT 1];
        System.assertEquals(cart1.ccrz__TaxAmount__c, transferedCart.ccrz__TaxAmount__c);
    }

    /**********************************************************************************************
    * @Name         : testTransferOwner
    * @Description  : Test method for transferOwner method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testTransferOwner() {
        osf_testHelper.setupStorefront();
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        Contact c1 = [SELECT Id FROM Contact WHERE Email =: osf_testHelper.USER_EMAIL1];
        User u1 = [SELECT Id, LanguageLocaleKey, ContactId FROM User WHERE Username =: osf_testHelper.USER_EMAIL1 LIMIT 1];
        User u2 = [SELECT Id, LanguageLocaleKey, ContactId FROM User WHERE Username =: osf_testHelper.USER_EMAIL2 LIMIT 1];
        ccrz__E_Product__c prod =[SELECT Id FROM ccrz__E_Product__c LIMIT 1];
        List<ccrz__E_Cart__c> lstTransferedCarts;
        ccrz__E_Cart__c cart1;
        ccrz__E_Cart__c cart2;
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c, ccrz__Account__c, ccrz__Contact__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Quote Requested');
        insert requestForQuote;
        System.runAs(u1) {
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
            ccrz.cc_CallContext.currPageParameters.put('currCartId', cart.ccrz__EncryptedId__c);
            cart2 = osf_testHelper.createCart(null, null, 10, 100, 100, acc, u1, c1);
            Database.insert(cart2);
            ccrz__E_CartItem__c ci2 = osf_testHelper.createCartItem(prod, cart2, 1, 100);
            Database.insert(ci2);
            cart1 = [SELECT Id, Name, ccrz__EncryptedId__c, ccrz__TotalAmount__c, ccrz__AdjustmentAmount__c, ccrz__TaxAmount__c, ccrz__TotalDiscount__c,
                        (SELECT Id, ccrz__Product__c, ccrz__AdjustmentAmount__c, ccrz__SubAmount__c, ccrz__OriginalItemPrice__c FROM ccrz__E_CartItems__r) 
                        FROM ccrz__E_Cart__c WHERE Id =: cart.Id LIMIT 1];
            Test.startTest();
            osf_ShareCartController.transferCartsOwner(new List<ccrz__E_Cart__c>{cart2}, new List<User>{u2}, requestForQuote.Id);
            Test.stopTest();
        }
        ccrz__E_Cart__c transferedCart = [SELECT Id, OwnerId, ccrz__ShipTo__c, ccrz__BillTo__c, osf_converted_from__c, osf_sharedCart__c FROM ccrz__E_Cart__c WHERE Id =: cart2.Id LIMIT 1];
        System.assertEquals(u2.Id, transferedCart.OwnerId);
        System.assert(String.isBlank(transferedCart.ccrz__ShipTo__c));
        System.assert(String.isBlank(transferedCart.ccrz__BillTo__c));
        System.assertEquals(requestForQuote.Id, transferedCart.osf_converted_from__c);
        System.assert(transferedCart.osf_sharedCart__c);
    }

    /**********************************************************************************************
    * @Name         : testActivateAndRenameOriginalCart
    * @Description  : Test method for ActivateAndRenameOriginalCart method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 17, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testActivateAndRenameOriginalCart() {
        osf_testHelper.setupStorefront();
        User u2 = [SELECT Id, LanguageLocaleKey FROM User WHERE Username =: osf_testHelper.USER_EMAIL2 LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        String cartName;
        ccrz__E_Cart__c cart1;
        System.runAs(u1) {
            cart1 = [SELECT Id, ccrz__EncryptedId__c, Name, ccrz__Name__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart1);
            ccrz.cc_CallContext.currPageParameters.put('currCartId', cart1.ccrz__EncryptedId__c);
            cartName = osf_constant_strings.SHARED_BY + ccrz.cc_CallContext.currUser.Username + osf_constant_strings.EMPTY_SPACE + cart1.Name;
            Test.startTest();
            osf_ShareCartController.activateAndRenameOriginalCart(cart1);
            Test.stopTest();
            cart1 = [SELECT Id, Name, ccrz__Name__c FROM ccrz__E_Cart__c WHERE Id =: cart1.Id LIMIT 1];
        }
        System.assertEquals(cartName, cart1.ccrz__Name__c);
    }
}