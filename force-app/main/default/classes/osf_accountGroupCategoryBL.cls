/**
 * File:        osf_accountGroupCategoryBL.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Mar 20, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: class for handling account group categories business logic
  ************************************************************************
 * History:
 */

public with sharing class osf_accountGroupCategoryBL {
    
    /* Description:  Be very careful with the number of account groups and currencies passed in.  Five SOQL will be performed for each combination*/    
    /**********************************************************************************************
    * @Name                 : createAccountGroupAllowedCategories
    * @Description          : This method refreshes the allowed categories for the passed Account Groups based on product availability within each category
                            Be very careful with the number of account groups and currencies passed in
                            Five/three SOQL will be performed for each combination
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : List<ccrz__E_AccountGroup__c> acctGrps
    * @param                : String storefront
    * @param                : String[] plCurrencies
    * @param                : String storefront
    * @Return               : 
    *********************************************************************************************/
    public static void createAccountGroupAllowedCategories(List<ccrz__E_AccountGroup__c> acctGrps, String[] plCurrencies, String storefront) {
        List<osf_account_group_category__c> allowedCategories = new List<osf_account_group_category__c>();        
        Set<Id> agIds = new Set<Id>();       
        Map<Id, List<ccrz__E_PriceList__c>> mapAccountGroupPricelists = getAccountGroupPricelistsMap(acctGrps);
        for(ccrz__E_AccountGroup__c ag : acctGrps) {
            if (mapAccountGroupPricelists.get(ag.Id) == null) continue;          
            agIds.add(ag.Id); 
            for(String curr : plCurrencies) { 
                for(Id catId : determineAllowedCategories(new Map<Id,ccrz__E_PriceList__c>(mapAccountGroupPricelists.get(ag.Id)), ag.Id, storefront, curr)) {                    
                    allowedCategories.add(new osf_account_group_category__c(
                        osf_category__c = catId,                                    
                        osf_currency__c = curr,                                    
                        osf_storefront__c = storefront,                                    
                        osf_account_group__c = ag.Id)
                    );
                }            
            }        
        }
        Savepoint sp = Database.setSavepoint();              
        List<osf_account_group_category__c> lstAccountGroupCategoriesToDelete = new List<osf_account_group_category__c>([SELECT Id FROM osf_account_group_category__c WHERE osf_account_group__c IN :agIds AND osf_storefront__c = :storefront AND osf_currency__c IN :plCurrencies]);
        try {
            if (!lstAccountGroupCategoriesToDelete.isEmpty()) {
                Database.delete(lstAccountGroupCategoriesToDelete);
            }
            Database.insert(allowedCategories);                  
        } catch(Exception e) {                         
            Database.rollback(sp);                  
        }
    }

    /**********************************************************************************************
    * @Name                 : getAccountGroupPricelistsMap
    * @Description          : build accoutGroup Price lists map
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : List<ccrz__E_AccountGroup__c> acctGrps
    * @Return               : List<ccrz__E_PriceList__c>> mapAccountGroupPricelists
    *********************************************************************************************/
    private static Map<Id, List<ccrz__E_PriceList__c>> getAccountGroupPricelistsMap(List<ccrz__E_AccountGroup__c> acctGrps) {
        Date today = Date.today();
        Map<Id, ccrz__E_AccountGroup__c> mapAccountGroups = new Map<Id, ccrz__E_AccountGroup__c>(acctGrps);
        List<ccrz__E_AccountGroupPriceList__c> lstAccountGroupPriceLists = new List<ccrz__E_AccountGroupPriceList__c>([SELECT ccrz__AccountGroup__c, ccrz__Pricelist__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c in :mapAccountGroups.keySet() AND ccrz__Pricelist__r.ccrz__StartDate__c <= :today AND ccrz__Pricelist__r.ccrz__EndDate__c >= :today]);
        Set<Id> setPriceListsIds = new Set<Id>();
        for (ccrz__E_AccountGroupPriceList__c agpl : lstAccountGroupPriceLists) {
            setPriceListsIds.add(agpl.ccrz__Pricelist__c);
        }
        Map<Id, ccrz__E_PriceList__c> mapAllowedPricelists = new Map<Id, ccrz__E_PriceList__c>([SELECT Id FROM ccrz__E_PriceList__c WHERE Id IN :setPriceListsIds]);
        Map<Id, List<ccrz__E_PriceList__c>> mapAccountGroupPriceLists = new Map<Id, List<ccrz__E_PriceList__c>>();
        for (ccrz__E_AccountGroupPriceList__c agpl : lstAccountGroupPriceLists) {
            List<ccrz__E_PriceList__c> lstPriceLists;
            if (!mapAccountGroupPriceLists.containsKey(agpl.ccrz__AccountGroup__c)) {
                lstPriceLists = new List<ccrz__E_PriceList__c>();
            } else {
                lstPriceLists = mapAccountGroupPriceLists.get(agpl.ccrz__AccountGroup__c);
            }
            lstPriceLists.add(mapAllowedPricelists.get(agpl.ccrz__Pricelist__c));
            mapAccountGroupPriceLists.put(agpl.ccrz__AccountGroup__c, lstPriceLists);
        }
        return mapAccountGroupPricelists;
    }
        
    /**********************************************************************************************
    * @Name                 : determineAllowedCategories
    * @Description          : Query method that, given an Account Group id, storefront, and currency returns the list of categories that have products visible to the end user
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : Id agId
    * @param                : String storefront
    * @param                : String plCurrency
    * @Return               : List<ccrz__E_Category__c>
    *********************************************************************************************/
    private static Set<Id> determineAllowedCategories(Map<Id,ccrz__E_PriceList__c> allowedPricelists, Id agId, String storefront, String plCurrency) {        
        Set<Id> catIds = new Set<Id>();  
        Set<String> setValidProductStatuses = new Set<String>{osf_constant_Strings.RELEASED, osf_constant_Strings.NOT_ORDERABLE};           
        if (allowedPricelists.size() > 0) {
            for(AggregateResult ag : [SELECT count(ccrz__Product__r.Id), ccrz__Category__c FROM ccrz__E_ProductCategory__c WHERE 
                ccrz__Product__c IN 
                    (SELECT ccrz__Product__c FROM ccrz__E_PriceListItem__c WHERE ccrz__Pricelist__c IN :allowedPricelists.values() AND ccrz__PriceList__r.ccrz__CurrencyISOCode__c = :plCurrency AND ccrz__PriceList__r.ccrz__StartDate__c <= TODAY AND ccrz__PriceList__r.ccrz__EndDate__c >= TODAY AND ccrz__PriceList__r.ccrz__Storefront__c INCLUDES (:storefront) AND ccrz__StartDate__c <= TODAY AND ccrz__EndDate__c >= TODAY) 
                AND ccrz__Product__r.ccrz__StartDate__c <= TODAY AND ccrz__Product__r.ccrz__EndDate__c >= TODAY AND ccrz__Product__r.ccrz__Storefront__c INCLUDES (:storefront) AND ccrz__Product__r.ccrz__ProductStatus__c IN :setValidProductStatuses AND ccrz__StartDate__c <= TODAY AND ccrz__EndDate__c >= TODAY AND ccrz__Category__r.ccrz__StartDate__c <= TODAY AND ccrz__Category__r.ccrz__EndDate__c >= TODAY
                GROUP BY ccrz__Category__c HAVING count(ccrz__Product__r.Id) > 0]) {                
                catIds.add((Id)ag.get(osf_constant_strings.CATEGORY_OBJ_API_NAME));            
            }
            if (!catIds.isEmpty()) {                
                Set<Id> firstRunParents = walkUpTree(catIds);   
                catIds.addAll(firstRunParents);                
                if(!firstRunParents.isEmpty()) {                    
                    Set<Id> secondRunParents = walkUpTree(firstRunParents);  
                    catIds.addAll(secondRunParents);                
                }            
            }   
        }          
        return catIds;
    }

    /**********************************************************************************************
    * @Name                 : walkUpTree
    * @Description          : get the category tree cache
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : Set<Id> catIdSet
    * @Return               : Set<Id> set with all children categories
    *********************************************************************************************/
    private static Set<Id> walkUpTree(Set<Id> catIdSet) {        
        final Set<Id> retIds = new Set<Id>();
        for(ccrz__E_Category__c cat : [SELECT ccrz__ParentCategory__c, ccrz__ParentCategory__r.ccrz__ParentCategory__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__c, ccrz__StartDate__c, ccrz__EndDate__c,
        ccrz__ParentCategory__r.ccrz__StartDate__c, ccrz__ParentCategory__r.ccrz__EndDate__c,
        ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__StartDate__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__EndDate__c,
        ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__StartDate__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__EndDate__c,
        ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__StartDate__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__EndDate__c,
        ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__StartDate__c, ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__ParentCategory__r.ccrz__EndDate__c 
        FROM ccrz__E_Category__c WHERE Id IN :catIdSet ]) {            
            recurseParentTree(cat, retIds);
        }
        return retIds;
    }
    
    /**********************************************************************************************
    * @Name                 : recurseParentTree
    * @Description          : create parent list for a categ
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : ccrz__E_Category__c cat
    * @param                : Set<Id> catIdSet
    * @Return               : 
    *********************************************************************************************/
    private static void recurseParentTree(ccrz__E_Category__c cat, Set<Id> catIdSet) {
        try {
            if(cat.ccrz__ParentCategory__r != null && cat.ccrz__ParentCategory__r.ccrz__StartDate__c <= system.today() && cat.ccrz__ParentCategory__r.ccrz__EndDate__c >= system.today()) {
                catIdSet.add(cat.ccrz__ParentCategory__r.Id);
                recurseParentTree(cat.ccrz__ParentCategory__r, catIdSet);
            }
        } catch(Exception ex){}
    }

    /**********************************************************************************************
    * @Name                 : refreshCategoryCaches
    * @Description          : delete the existing and insert new osf_category_tree_cache__c records
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 23, 2020
    * @param                : List<ccrz__E_AccountGroup__c> acctGrps
    * @param                : List<String> plCurrencies
    * @param                : String storefront
    * @param                : List<String> locales
    * @Return               : 
    *********************************************************************************************/
    public static void refreshCategoryCaches(List<ccrz__E_AccountGroup__c> acctGrps, List<String> plCurrencies, String storefront, List<String> locales) {        
        final List<String> actLocales = new List<String>(locales);                
        List<osf_category_tree_cache__c> toInsert = new List<osf_category_tree_cache__c>();
        Set<Id> agIds = new Set<Id>();
        for(ccrz__E_AccountGroup__c ag : acctGrps) {
            agIds.add(ag.Id);            
            for(String plCurr : plCurrencies) {   
                List<osf_category_tree_cache__c> lst = createCategoryTreeCache(ag.Id, storefront, plCurr, actLocales);
                toInsert.addAll(lst);            
            }        
        }
        Savepoint sp = Database.setSavepoint();        
        Boolean doRollback = true;
        try {    
            List<osf_category_tree_cache__c> lstCategoriesCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c WHERE osf_account_group__c IN :agIds AND osf_storefront__c = :storefront]);
            if (!lstCategoriesCache.isEmpty()) {
                Database.delete(lstCategoriesCache);            
            }
            Database.insert(toInsert);            
            doRollback = false;        
        } finally {            
            if(doRollback) {                
                Database.rollback(sp);            
            }        
        }    
    }

    /**********************************************************************************************
    * @Name                 : createCategoryTreeCache
    * @Description          : create categories tree cache
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 23, 2020
    * @param                : Id agId
    * @param                : String storefront
    * @param                : String plCurrency
    * @param                : List<String> locales
    * @Return               : List<osf_category_tree_cache__c>
    *********************************************************************************************/
    private static List<osf_category_tree_cache__c> createCategoryTreeCache(Id agId, String storefront, String plCurrency, List<String> locales) {
        Map<Id,ccrz__E_Category__c> allowedCats = new Map<Id,ccrz__E_Category__c>([SELECT Id, Name, ccrz__Sequence__c, ccrz__ParentCategory__r.Id, ccrz__SEOId__c, 
            (SELECT Name, ccrz__Locale__c, ccrz__SEOId__c FROM ccrz__CategoryI18Ns__r WHERE ccrz__Locale__c IN :locales) 
            FROM ccrz__E_Category__c WHERE Id IN ( SELECT osf_category__c FROM osf_account_group_category__c WHERE osf_account_group__c = :agId AND osf_storefront__c = :storefront AND osf_currency__c = :plCurrency) /*AND ccrz__ParentCategory__r.ccrz__ParentCategory__c != NULL*/  AND ccrz__CategoryID__c !=: osf_constant_strings.ROOT_CATEGORY ORDER BY ccrz__Sequence__c ]);
        Map<String,List<TreeElement>> theTrees = new Map<String,List<TreeElement>>();        
        Map<Id,Map<String,TreeElement>> elementLookup = new Map<Id,Map<String,TreeElement>>();
        for(ccrz__E_Category__c cat : allowedCats.values()) {        
            recurseCacheBuild(cat, allowedCats, theTrees, elementLookup, locales );        
        }
        List<osf_category_tree_cache__c> caches = new List<osf_category_tree_cache__c>();
        for(String loc : theTrees.keySet()) {            
            caches.add(new osf_category_tree_cache__c(
                osf_account_group__c = agId,                    
                osf_storefront__c = storefront,                    
                osf_currency__c = plCurrency,                    
                osf_locale__c = loc,                    
                osf_cache_data__c = JSON.serialize(theTrees.get(loc))            
                )
            );        
        }
        return caches;    
    }

    /**********************************************************************************************
    * @Name                 : recurseCacheBuild
    * @Description          : go through categories tree
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 23, 2020
    * @param                : ccrz__E_Category__c currCat
    * @param                : Map<Id,ccrz__E_Category__c> catMap
    * @param                : Map<String,List<TreeElement>> dataCache
    * @param                : Map<Id,Map<String,TreeElement>> elementLookup
    * @param                : List<String> locales
    * @Return               : 
    *********************************************************************************************/
    private static void recurseCacheBuild( ccrz__E_Category__c currCat, Map<Id,ccrz__E_Category__c> catMap, Map<String,List<TreeElement>> dataCache, Map<Id,Map<String,TreeElement>> elementLookup, List<String> locales) {        
        if(currCat != null) { 
            ccrz__E_Category__c currCatParent = catMap.get(currCat.ccrz__ParentCategory__r.Id);            
            if(currCatParent != null) {
                recurseCacheBuild(currCatParent, catMap, dataCache, elementLookup, locales);            
            }
            if(!elementLookup.containsKey(currCat.id)) {               
                 Map<String,String> mappedNames = new Map<String,String>();  
                Map<String,String> mappedSeoIds = new Map<String,String>();  
                 for(ccrz__E_CategoryI18N__c i18nCat:currCat.ccrz__CategoryI18Ns__r) {                    
                    mappedNames.put(i18nCat.ccrz__Locale__c,i18nCat.Name);
                    mappedSeoIds.put(i18nCat.ccrz__Locale__c,i18nCat.ccrz__SEOId__c);
                }                
                Map<String,TreeElement> localeElemMap = new Map<String,TreeElement>();                
                for(String loc : locales) {                    
                    TreeElement elem = new TreeElement();                    
                    elem.sfid = currCat.Id;   
                    if(currCat.ccrz__Sequence__c != null) {                        
                        elem.sequence = currCat.ccrz__Sequence__c.intValue();                    
                    } else {                        
                        elem.sequence = 0;                    
                    }
                    if(mappedNames.containsKey(loc)) {                        
                        elem.name = mappedNames.get(loc);                    
                    } else {                        
                        elem.name = currCat.name;                    
                    } 
                    if(mappedSeoIds.containsKey(loc)) {                        
                        elem.friendlyUrl = mappedSeoIds.get(loc);                    
                    } else {                        
                        elem.friendlyUrl = currCat.ccrz__SEOId__c;                    
                    } 
                    localeElemMap.put(loc,elem);                
                }
                if(currCatParent != null) {       
                    Map<String,TreeElement> parentTrees = elementLookup.get(currCatParent.Id);                    
                    for(String loc : parentTrees.keySet()) {                        
                        TreeElement pElem = parentTrees.get(loc);                        
                        TreeElement cElem = localeElemMap.get(loc);                        
                        pElem.children.add(cElem);                        
                        pElem.children.sort();                    
                    }                
                } else {
                    //Top level element                    
                    for(String loc : locales) {                        
                        List<TreeElement> topList = dataCache.get(loc);                        
                        if(topList == null) {                            
                            topList = new List<TreeElement>();                            
                            dataCache.put(loc,topList);                        
                        }                        
                        topList.add(localeElemMap.get(loc));                        
                        topList.sort();                    
                    }                
                }                
                elementLookup.put(currCat.Id, localeElemMap);            
            }        
        }
    }     

    public class TreeElement implements Comparable {        
        public String sfid {get; set;}        
        public String name {get; set;}        
        transient public Integer sequence {get; set;} 
        public String friendlyUrl {get; set;}
        public List<TreeElement> children {get; set;}
        public TreeElement() {            
            children = new List<TreeElement>();        
        }        
        public Integer compareTo(Object obj) {            
            return sequence - ((TreeElement)obj).sequence;        
        }    
    }
}