/**
 * File:        osf_allowedCategoryJob.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Mar 20, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: batch class for processing the category tree cache
  ************************************************************************
 * History:
 */

global with sharing class osf_allowedCategoryJob  implements Database.Batchable<sObject>, Database.Stateful {
    private String storefront;    
    private String[] plCurrencies;    
    private String[] locales;
    
    /**********************************************************************************************
    * @Name                 : osf_allowedCategoryJob
    * @Description          : constructor
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : String storefront
    * @param                : String[] plCurrencies
    * @param                : String[] locales
    * @Return               : 
    *********************************************************************************************/
    global osf_allowedCategoryJob(String storefront, String[] plCurrencies, String[] locales) {        
        this.storefront = storefront;        
        this.plCurrencies = plCurrencies;        
        this.locales = locales;    
    }

    /**********************************************************************************************
    * @Name                 : start
    * @Description          : start method of the batchable class
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : Database.BatchableContext BC
    * @Return               : Database.QueryLocator
    *********************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {        
        String query = 'SELECT Id FROM ccrz__E_AccountGroup__c WHERE Id in (' + 'SELECT ccrz__AccountGroup__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__Pricelist__r.ccrz__Storefront__c INCLUDES (\'' + storefront + '\')' + ')';   
        return Database.getQueryLocator(query);    
    }

    /**********************************************************************************************
    * @Name                 : execute
    * @Description          : execute method of the batchable class
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : Database.BatchableContext BC
    * @param                : List<sObject> scope
    * @Return               : 
    *********************************************************************************************/
    global void execute(Database.BatchableContext BC, List<sObject> scope) {  
        osf_accountGroupCategoryBL.createAccountGroupAllowedCategories(scope, plCurrencies, storefront);        
        osf_accountGroupCategoryBL.refreshCategoryCaches(scope, plCurrencies, storefront, locales);    
    }

    global void finish(Database.BatchableContext BC) {    }
}