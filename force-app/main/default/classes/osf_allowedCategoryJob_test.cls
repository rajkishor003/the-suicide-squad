/**
 * File:        osf_allowedCategoryJob_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        May 13, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_allowedCategoryJob
  ************************************************************************
 * History:
 */
@isTest 
public class osf_allowedCategoryJob_test {
    
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : May 13, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData() {
        ccrz__E_Product__c prod1 = osf_testHelper.createCCProduct('001', 'Product1');
        ccrz__E_Product__c prod2 = osf_testHelper.createCCProduct('002', 'Product2');
        ccrz__E_Product__c prod3 = osf_testHelper.createCCProduct('003', 'Product3');
        insert new List<ccrz__E_Product__c> {prod1, prod2, prod3};

        ccrz__E_Category__c categ1 = osf_testHelper.createCCCategory('categ1', '1');
        ccrz__E_Category__c categ11 = osf_testHelper.createCCCategory('categ11', '11');
        ccrz__E_Category__c categ111 = osf_testHelper.createCCCategory('categ111', '111');
        ccrz__E_Category__c categ112 = osf_testHelper.createCCCategory('categ112', '112');
        ccrz__E_Category__c categ113 = osf_testHelper.createCCCategory('categ113', '113');
        ccrz__E_Category__c categ12 = osf_testHelper.createCCCategory('categ12', '12');
        ccrz__E_Category__c categ13 = osf_testHelper.createCCCategory('categ13', '13');
        ccrz__E_Category__c categ2 = osf_testHelper.createCCCategory('categ2', '2');
        ccrz__E_Category__c categ211 = osf_testHelper.createCCCategory('categ211', '211');
        ccrz__E_Category__c categ4 = osf_testHelper.createCCCategory('categ4', '4');
        insert new List<ccrz__E_Category__c> {categ1, categ11, categ111, categ112, categ113, categ12, categ13, categ2, categ211, categ4};

        categ11.ccrz__ParentCategory__c = categ1.Id;
        categ12.ccrz__ParentCategory__c = categ1.Id;
        categ13.ccrz__ParentCategory__c = categ1.Id;
        categ111.ccrz__ParentCategory__c = categ11.Id;
        categ112.ccrz__ParentCategory__c = categ11.Id;
        categ113.ccrz__ParentCategory__c = categ11.Id;
        categ211.ccrz__ParentCategory__c = categ2.Id;
        update new List<ccrz__E_Category__c> {categ11, categ12, categ13, categ111, categ112, categ113, categ211};

        ccrz__E_ProductCategory__c pc1 = osf_testHelper.createProductCategory(prod1, categ111);
        ccrz__E_ProductCategory__c pc2 = osf_testHelper.createProductCategory(prod2, categ12);
        ccrz__E_ProductCategory__c pc3 = osf_testHelper.createProductCategory(prod3, categ211);
        insert new List<ccrz__E_ProductCategory__c> {pc1, pc2, pc3};

        ccrz__E_Pricelist__c pricelistUSD = osf_testHelper.createCCPricelist('Test Pricelist USD', 'USD');
        ccrz__E_Pricelist__c pricelistJPY = osf_testHelper.createCCPricelist('Test Pricelist JPY', 'JPY');
        ccrz__E_Pricelist__c pricelistCNY = osf_testHelper.createCCPricelist('Test Pricelist CNY', 'CNY');
        insert new List<ccrz__E_Pricelist__c> {pricelistUSD, pricelistJPY, pricelistCNY};

        ccrz__E_PriceListItem__c pli1 = osf_testHelper.createPriceListItem(pricelistUSD, prod1);
        ccrz__E_PriceListItem__c pli2 = osf_testHelper.createPriceListItem(pricelistJPY, prod2);
        insert new List<ccrz__E_PriceListItem__c> {pli1, pli2};

        ccrz__E_AccountGroup__c accountGroup = osf_testHelper.createCCAccountGroup('Test Account Group');
        insert accountGroup;

        ccrz__E_AccountGroupPricelist__c accountGroupPricelistUSD = osf_testHelper.createCCAccountGroupPricelist(accountGroup, pricelistUSD);
        ccrz__E_AccountGroupPricelist__c accountGroupPricelistJPY = osf_testHelper.createCCAccountGroupPricelist(accountGroup, pricelistJPY);
        insert new List<ccrz__E_AccountGroupPricelist__c> {accountGroupPricelistUSD, accountGroupPricelistJPY};
    }

    /**********************************************************************************************
    * @Name         : testBatchJob
    * @Description  : Test osf_allowedCategoryJob method
    * @Created By   : Alina Craciunel
    * @Created Date : May 13, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testBatchJob() {
        List<osf_category_tree_cache__c> lstCategoryTreeCache;
        List<osf_account_group_category__c> lstAccountGroupCategories;
        Test.startTest();
        Database.executeBatch(new osf_allowedCategoryJob(osf_constant_strings.STOREFRONT_NAME, osf_constant_strings.CURRENCY_LIST, osf_constant_strings.COUNTRY_LOCALE_SID_MAP.values()), 10);
        Test.stopTest();
        lstAccountGroupCategories = new List<osf_account_group_category__c>([SELECT Id, osf_category__r.Name, osf_currency__c, osf_storefront__c, osf_account_group__c FROM osf_account_group_category__c WHERE osf_storefront__c =: osf_constant_strings.STOREFRONT_NAME]);
        system.assertEquals(5, lstAccountGroupCategories.size());
        lstCategoryTreeCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c]);
        system.assertEquals(6, lstCategoryTreeCache.size());
    }

    /**********************************************************************************************
    * @Name         : testInsertProductCategory
    * @Description  : Test osf_allowedCategoryJob method when a new product category is inserted
    * @Created By   : Alina Craciunel
    * @Created Date : May 13, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testInsertProductCategory() {
        List<osf_category_tree_cache__c> lstCategoryTreeCache;
        List<osf_account_group_category__c> lstAccountGroupCategories;
        ccrz__E_Category__c categ3 = osf_testHelper.createCCCategory('categ3', '3');
        insert categ3;
        ccrz__E_Product__c prod1 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product1'];
        Test.startTest(); 
        insert osf_testHelper.createProductCategory(prod1, categ3);
        Test.stopTest();
        lstAccountGroupCategories = new List<osf_account_group_category__c>([SELECT Id FROM osf_account_group_category__c WHERE osf_storefront__c =: osf_constant_strings.STOREFRONT_NAME]);
        system.assertEquals(6, lstAccountGroupCategories.size());
        lstCategoryTreeCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c]);
        system.assertEquals(6, lstCategoryTreeCache.size());
    }

    /**********************************************************************************************
    * @Name         : testInsertPriceListItem
    * @Description  : Test osf_allowedCategoryJob method when a new price list item is inserted
    * @Created By   : Alina Craciunel
    * @Created Date : May 13, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testInsertPriceListItem() {
        List<osf_category_tree_cache__c> lstCategoryTreeCache;
        List<osf_account_group_category__c> lstAccountGroupCategories;
        ccrz__E_Category__c categ3 = osf_testHelper.createCCCategory('categ3', '3');
        insert categ3;
        ccrz__E_Product__c prod1 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product1'];
        insert osf_testHelper.createProductCategory(prod1, categ3);
        ccrz__E_Pricelist__c pricelistUSD = [SELECT Id FROM ccrz__E_Pricelist__c WHERE Name = 'Test Pricelist USD'];
        ccrz__E_Product__c prod3 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product3'];
        Test.startTest(); 
        insert osf_testHelper.createPriceListItem(pricelistUSD, prod3);
        Test.stopTest();
        lstAccountGroupCategories = new List<osf_account_group_category__c>([SELECT Id FROM osf_account_group_category__c WHERE osf_storefront__c =: osf_constant_strings.STOREFRONT_NAME]);
        system.assertEquals(8, lstAccountGroupCategories.size());
        lstCategoryTreeCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c]);
        system.assertEquals(6, lstCategoryTreeCache.size());
    }

    /**********************************************************************************************
    * @Name         : testInsertCategory
    * @Description  : Test osf_allowedCategoryJob method when a new category is inserted
    * @Created By   : Alina Craciunel
    * @Created Date : May 13, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testInsertCategory() {
        List<osf_category_tree_cache__c> lstCategoryTreeCache;
        List<osf_account_group_category__c> lstAccountGroupCategories;
        ccrz__E_Category__c categ3 = osf_testHelper.createCCCategory('categ3', '3');
        insert categ3;
        ccrz__E_Product__c prod1 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product1'];
        insert osf_testHelper.createProductCategory(prod1, categ3);
        ccrz__E_Pricelist__c pricelistUSD = [SELECT Id FROM ccrz__E_Pricelist__c WHERE Name = 'Test Pricelist USD'];
        ccrz__E_Product__c prod3 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product3'];
        insert osf_testHelper.createPriceListItem(pricelistUSD, prod3);
        ccrz__E_Category__c categ2 = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'categ2'];
        ccrz__E_Category__c categ211 = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'categ211'];
        ccrz__E_Category__c categ21 = osf_testHelper.createCCCategory('categ21', '21');
        categ21.ccrz__ParentCategory__c = categ2.Id;
        insert categ21;
        categ211.ccrz__ParentCategory__c = categ21.Id;
        Test.startTest(); 
        update categ211;
        Test.stopTest();
        lstAccountGroupCategories = new List<osf_account_group_category__c>([SELECT Id FROM osf_account_group_category__c WHERE osf_storefront__c =: osf_constant_strings.STOREFRONT_NAME]);
        system.assertEquals(9, lstAccountGroupCategories.size());
        lstCategoryTreeCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c]);
        system.assertEquals(6, lstCategoryTreeCache.size());
    }

    /**********************************************************************************************
    * @Name         : testInsertAccountGroupPriceList
    * @Description  : Test osf_allowedCategoryJob method when a new account group price list is inserted
    * @Created By   : Alina Craciunel
    * @Created Date : May 13, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testInsertAccountGroupPriceList() {
        List<osf_category_tree_cache__c> lstCategoryTreeCache;
        List<osf_account_group_category__c> lstAccountGroupCategories;
        ccrz__E_Category__c categ3 = osf_testHelper.createCCCategory('categ3', '3');
        insert categ3;
        ccrz__E_Product__c prod1 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product1'];
        insert osf_testHelper.createProductCategory(prod1, categ3);
        ccrz__E_Pricelist__c pricelistUSD = [SELECT Id FROM ccrz__E_Pricelist__c WHERE Name = 'Test Pricelist USD'];
        ccrz__E_Product__c prod3 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product3'];
        insert osf_testHelper.createPriceListItem(pricelistUSD, prod3);
        ccrz__E_Category__c categ2 = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'categ2'];
        ccrz__E_Category__c categ211 = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'categ211'];
        ccrz__E_Category__c categ21 = osf_testHelper.createCCCategory('categ21', '21');
        categ21.ccrz__ParentCategory__c = categ2.Id;
        insert categ21;
        categ211.ccrz__ParentCategory__c = categ21.Id;
        update categ211;
        ccrz__E_Pricelist__c pricelistCNY = [SELECT Id FROM ccrz__E_Pricelist__c WHERE Name = 'Test Pricelist CNY'];
        insert osf_testHelper.createPriceListItem(pricelistCNY, prod1);
        ccrz__E_AccountGroup__c accountGroup = [SELECT Id FROM ccrz__E_AccountGroup__c LIMIT 1];
        Test.startTest(); 
        insert osf_testHelper.createCCAccountGroupPricelist(accountGroup, pricelistCNY);
        Test.stopTest();
        lstAccountGroupCategories = new List<osf_account_group_category__c>([SELECT Id FROM osf_account_group_category__c WHERE osf_storefront__c =: osf_constant_strings.STOREFRONT_NAME]);
        system.assertEquals(13, lstAccountGroupCategories.size());
        lstCategoryTreeCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c]);
        system.assertEquals(9, lstCategoryTreeCache.size());
    }

    /**********************************************************************************************
    * @Name         : testUpdateProduct
    * @Description  : Test osf_allowedCategoryJob method when a product has been edited and its startDate is in the future, so it should not be displayed in storefront
    * @Created By   : Alina Craciunel
    * @Created Date : May 26, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testUpdateProduct() {
        List<osf_category_tree_cache__c> lstCategoryTreeCache;
        List<osf_account_group_category__c> lstAccountGroupCategories;
        ccrz__E_Category__c categ3 = osf_testHelper.createCCCategory('categ3', '3');
        insert categ3;
        ccrz__E_Product__c prod1 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product1'];
        insert osf_testHelper.createProductCategory(prod1, categ3);
        ccrz__E_Pricelist__c pricelistUSD = [SELECT Id FROM ccrz__E_Pricelist__c WHERE Name = 'Test Pricelist USD'];
        ccrz__E_Product__c prod3 = [SELECT Id FROM ccrz__E_Product__c WHERE Name = 'Product3'];
        insert osf_testHelper.createPriceListItem(pricelistUSD, prod3);
        ccrz__E_Category__c categ2 = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'categ2'];
        ccrz__E_Category__c categ211 = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'categ211'];
        ccrz__E_Category__c categ21 = osf_testHelper.createCCCategory('categ21', '21');
        categ21.ccrz__ParentCategory__c = categ2.Id;
        insert categ21;
        categ211.ccrz__ParentCategory__c = categ21.Id;
        update categ211;
        ccrz__E_Pricelist__c pricelistCNY = [SELECT Id FROM ccrz__E_Pricelist__c WHERE Name = 'Test Pricelist CNY'];
        insert osf_testHelper.createPriceListItem(pricelistCNY, prod1);
        ccrz__E_AccountGroup__c accountGroup = [SELECT Id FROM ccrz__E_AccountGroup__c LIMIT 1];
        insert osf_testHelper.createCCAccountGroupPricelist(accountGroup, pricelistCNY);
        Test.startTest(); 
        prod3.ccrz__StartDate__c = Date.today().addDays(10);
        update prod3;
        Test.stopTest();
        lstAccountGroupCategories = new List<osf_account_group_category__c>([SELECT Id FROM osf_account_group_category__c WHERE osf_storefront__c =: osf_constant_strings.STOREFRONT_NAME]);
        system.assertEquals(10, lstAccountGroupCategories.size());
        lstCategoryTreeCache = new List<osf_category_tree_cache__c>([SELECT Id FROM osf_category_tree_cache__c]);
        system.assertEquals(9, lstCategoryTreeCache.size());
    }
}