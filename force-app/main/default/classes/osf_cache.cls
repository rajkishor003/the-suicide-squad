/**
 * File:        osf_cache.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Feb 04, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Class for managing the cache
  ************************************************************************
 * History:
 */

public with sharing class osf_cache {

    /**********************************************************************************************
    * @Name         : setPriceVisibility
    * @Description  : Set the price visibility to cache
    * @Created By   : Alina Craciunel
    * @Created Date : Feb 04, 2020
    * @param        : priceIsVisible
    * @Return       : 
    *********************************************************************************************/
    public static void setPriceVisibility(Boolean priceIsVisible) {
        Cache.SessionPartition sessionPart = Cache.Session.getPartition(osf_constant_strings.NAMESPACE_CACHE);
        ccrz.ccLog.log(LoggingLevel.DEBUG, 'osf:ctrl:header:setPriceVisibility:Debug', priceIsVisible);
        sessionPart.put(osf_constant_strings.PRICE_IS_VISIBLE_CACHE, priceIsVisible);         
    }

    /**********************************************************************************************
    * @Name         : getPriceVisibility
    * @Description  : Get the price visibility from cache
    * @Created By   : Alina Craciunel
    * @Created Date : Feb 04, 2020
    * @Return       : the value stored in the session cache
    *********************************************************************************************/
    public static Boolean getPriceVisibility() {
        Cache.SessionPartition sessionPart = Cache.Session.getPartition(osf_constant_strings.NAMESPACE_CACHE);
        if (sessionPart.contains(osf_constant_strings.PRICE_IS_VISIBLE_CACHE)) {
            ccrz.ccLog.log(LoggingLevel.DEBUG, 'osf:ctrl:header:getPriceVisibility:Debug',sessionPart.get(osf_constant_strings.PRICE_IS_VISIBLE_CACHE));
            return (Boolean)sessionPart.get(osf_constant_strings.PRICE_IS_VISIBLE_CACHE);
        }
        return null;
    }

    /**********************************************************************************************
    * @Name         : setAvailableCurrencies
    * @Description  : Set the available currencies to cache
    * @Created By   : Alina Craciunel
    * @Created Date : Feb 04, 2020
    * @param        : setAvailableCurrencies
    * @Return       : 
    *********************************************************************************************/
    public static void setAvailableCurrencies(Set<String> setAvailableCurrencies) {
        Cache.SessionPartition sessionPart = Cache.Session.getPartition(osf_constant_strings.NAMESPACE_CACHE);
        ccrz.ccLog.log(LoggingLevel.DEBUG, 'osf:ctrl:header:setAvailableCurrencies:Debug', setAvailableCurrencies);
        sessionPart.put(osf_constant_strings.AVAILABLE_CURRENCIES_CACHE, setAvailableCurrencies);         
    }

    /**********************************************************************************************
    * @Name         : getAvailableCurrencies
    * @Description  : Get the available currencies from cache
    * @Created By   : Alina Craciunel
    * @Created Date : Feb 04, 2020
    * @Return       : the values stored in the session cache
    *********************************************************************************************/
    public static Set<String> getAvailableCurrencies() {
        Cache.SessionPartition sessionPart = Cache.Session.getPartition(osf_constant_strings.NAMESPACE_CACHE);
        if (sessionPart.contains(osf_constant_strings.AVAILABLE_CURRENCIES_CACHE)) {
            ccrz.ccLog.log(LoggingLevel.DEBUG, 'osf:ctrl:header:getAvailableCurrencies:Debug',sessionPart.get(osf_constant_strings.AVAILABLE_CURRENCIES_CACHE));
            return (Set<String>)sessionPart.get(osf_constant_strings.AVAILABLE_CURRENCIES_CACHE);
        }
        return null;
    }

}