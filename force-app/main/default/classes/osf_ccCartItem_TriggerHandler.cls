/**
 * File:        osf_ccCartItem_TriggerHandler
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Dec 11, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Trigger handler class for CC Cart Item object
  ************************************************************************
 * History:
 */

public with sharing class osf_ccCartItem_TriggerHandler {
    //Static variable to skip handler in test classes.
    @TestVisible private static Boolean isEnabled = true;

    /**********************************************************************************************
    * @Name                 : doBeforeDelete
    * @Description          : before cc cart item is deleted
    * @Created By           : Ozgun Eser
    * @Created Date         : Dec 11, 2019
    * @param cartItemList   : deleted cart item records
    * @Return               : 
    *********************************************************************************************/
    public static void doBeforeDelete(List<ccrz__E_CartItem__c> cartItemList) {
        Set<String> cartIdSet = new Set<String> ();
        List<ccrz__E_CartItem__c> cartItemListToPreventDelete = new List<ccrz__E_CartItem__c> ();
        for(ccrz__E_CartItem__c cartItem : cartItemList) {
            cartIdSet.add(cartItem.ccrz__Cart__c);
        }
        Map<Id, ccrz__E_Cart__c> cartMap = new Map<Id, ccrz__E_Cart__c> ([SELECT Id, osf_converted_from_rfq__c FROM ccrz__E_Cart__c WHERE Id IN :cartIdSet]);
        for(ccrz__E_CartItem__c cartItem : cartItemList) {
            if(!cartMap.containsKey(cartItem.ccrz__Cart__c)) { continue; }
            if(cartMap.get(cartItem.ccrz__Cart__c).osf_converted_from_rfq__c) {
                cartItem.addError(osf_constant_strings.CART_ITEM_DELETE_ERROR_MESSAGE);
            }
        }
    }
}