@IsTest
public class osf_ccCartItem_TriggerHandler_Test {
    
    @TestSetup
    public static void createTestData(){
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        insert product;

        ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
        insert cartItem;
    }

    @IsTest
    public static void testBeforeDelete() {
        ccrz__E_Cart__c cart = [SELECt Id, ccrz__Name__c, ccrz__Account__c, ccrz__Contact__c FROM ccrz__E_Cart__c];
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, osf_constant_strings.QUOTE_STATUS_QUOTE_REQUESTED);
        insert requestForQuote;
        cart.ccrz__Name__c = 'Converted From RFQ-000000';
        cart.osf_converted_from__c = requestForQuote.Id;
        update cart;
        List<Database.DeleteResult> deleteResultList = Database.delete([SELECT Id FROM ccrz__E_CartItem__c], false);
        System.assertEquals(1, deleteResultList.size());
        System.assert(!deleteResultList[0].isSuccess());
        List<Database.Error> errorList = deleteResultList[0].getErrors();
        System.assertEquals(1, errorList.size());
        System.assertEquals(osf_constant_strings.CART_ITEM_DELETE_ERROR_MESSAGE, errorList[0].getMessage());
    }
}