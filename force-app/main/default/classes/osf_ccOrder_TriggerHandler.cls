/**
 * File:        osf_ccOrder_TriggerHandler
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 08, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Trigger handler class for CC Order object
  ************************************************************************
 * History:
 */
public without sharing class osf_ccOrder_TriggerHandler {
    
    @TestVisible private static Boolean isEnabled = true;

    /**********************************************************************************************
    * @Name         : doAfterInsert
    * @Description  : after cc order is inserted
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 08, 2019
    * @param lstOrders: newly inserted orders
    * @Return       : 
    *********************************************************************************************/
    public static void doAfterInsert(List<ccrz__E_Order__c> lstOrders) {
        if(!isEnabled) { return; }
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c> ();
        for(ccrz__E_Order__c order : lstOrders) {
            if(String.isNotBlank(order.ccrz__OrderStatus__c)) {
                orderList.add(order);
            }
        }
        osf_utility.createChatterMessageForOrders(orderList);
        osf_utility.autoFollowChatterOrders(lstOrders);
    }

    /**********************************************************************************************
    * @Name         : doAfterUpdate
    * @Description  : after cc order is updated
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 08, 2019
    * @param mapNewOrders: newly updated orders
    * @param mapOldOrders: old orders values
    * @Return       : 
    *********************************************************************************************/
    public static void doAfterUpdate(Map<Id, ccrz__E_Order__c> mapNewOrders, Map<Id, ccrz__E_Order__c> mapOldOrders) {
        if(!isEnabled) { return; }
        List<ccrz__E_Order__c> lstOrders = new List<ccrz__E_Order__c> ();
        List<Id> lstOrdersWithSOA = new List<Id> ();
        Set<Id> setSoaIds = new Set<Id>();
        for(ccrz__E_Order__c order : mapNewOrders.values()) {
            if(String.isNotBlank(order.ccrz__OrderStatus__c) && order.ccrz__OrderStatus__c != mapOldOrders.get(order.Id).ccrz__OrderStatus__c) {
                lstOrders.add(order);
                if (order.ccrz__OrderStatus__c.toLowercase() == osf_constant_strings.ORDER_PROCESSED_STATUS.toLowercase()) {
                    lstOrdersWithSOA.add(order.Id);
                    if (order.osf_sales_order_acknowledgement__c != null) {
                        setSoaIds.add(order.osf_sales_order_acknowledgement__c);
                    }
                }
            }
        }
        if(!lstOrders.isEmpty()) {
            osf_utility.processPurchaseOrderList(lstOrders);
            osf_utility.createChatterMessageForOrders(lstOrders);
        }
        if (!lstOrdersWithSOA.isEmpty()) {
            sendSOAEmailNotification(lstOrdersWithSOA, setSoaIds);
        }
    }

    /**********************************************************************************************
    * @Name         : checkSalesOrderAcknowledgement
    * @Description  : after cc order is updated, check if there is any Sales Order Acknowledgement 
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2019
    * @param mapNewOrders: newly updated orders
    * @param mapOldOrders: old orders values
    * @Return       : 
    *********************************************************************************************/
    public static void checkSalesOrderAcknowledgement(Map<Id, ccrz__E_Order__c> mapNewOrders, Map<Id, ccrz__E_Order__c> mapOldOrders) {
        Set<Id> setOrderIds = new Set<Id>();
        Set<Id> setSoaIds = new Set<Id>();
        for(ccrz__E_Order__c order : mapNewOrders.values()) {
            if (String.isBlank(order.ccrz__OrderStatus__c)) {
                continue;
            }
            if(order.ccrz__OrderStatus__c.toLowercase() == osf_constant_strings.ORDER_PROCESSED_STATUS.toLowercase() && order.ccrz__OrderStatus__c != mapOldOrders.get(order.Id).ccrz__OrderStatus__c) {
                setOrderIds.add(order.Id);
                if (order.osf_sales_order_acknowledgement__c != null) {
                    setSoaIds.add(order.osf_sales_order_acknowledgement__c);
                }
            }
        }
        if (setOrderIds.isEmpty()) return;
        Map<String, String> mapSoaIdAttachmentIds = new Map<String, String>();
        if (!setSoaIds.isEmpty()) {
            for(ContentDocumentLink attachment : [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :setSoaIds]) {
                mapSoaIdAttachmentIds.put(attachment.LinkedEntityId, attachment.ContentDocumentId); 
            }
        }
        for (ccrz__E_Order__c order : mapNewOrders.values()) {
            if (setOrderIds.contains(order.Id) != null && (order.osf_sales_order_acknowledgement__c == null || mapSoaIdAttachmentIds.get(order.osf_sales_order_acknowledgement__c) == null)) {
                order.addError(osf_constant_strings.NO_SOA_ERROR);
            }
        }
    }

    /**********************************************************************************************
    * @Name         : sendSOAEmailNotification
    * @Description  : send account owner notification when a order with SOA is Processed
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2019
    * @param        : List<Id> lstOrdersWithSOA
    * @param        : Set<Id> setSoaIds
    * @Return       : 
    *********************************************************************************************/
    private static void sendSOAEmailNotification(List<Id> lstOrdersWithSOA, Set<Id> setSoaIds) {
        EmailTemplate emailTemplate = [SELECT Subject, HtmlValue FROM EmailTemplate WHERE Name = :osf_constant_strings.SOA_EMAIL_TEMPLATE_NAME];
        Map<String, List<String>> mapSoaIdAttachmentIds = new Map<String, List<String>>();
        List<String> lstContentDocumentIds = new List<String>();
        if (!setSoaIds.isEmpty()) {
            for(ContentDocumentLink attachment : [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :setSoaIds]) {
                List<String> lstContentDocuments;
                if (mapSoaIdAttachmentIds.get(attachment.LinkedEntityId) == null) {
                    lstContentDocuments = new List<String>();
                } else {
                    lstContentDocuments = mapSoaIdAttachmentIds.get(attachment.LinkedEntityId);
                }
                lstContentDocuments.add(attachment.ContentDocumentId);
                mapSoaIdAttachmentIds.put(attachment.LinkedEntityId, lstContentDocuments);
                lstContentDocumentIds.add(attachment.ContentDocumentId);
            }
        }
        Map<String, ContentVersion> contentDocumentIdToContentVersionMap = new Map<String, ContentVersion>();
        for(ContentVersion contentVersion : [SELECT Id, ContentDocumentId, VersionData, Title, FileType, FileExtension FROM ContentVersion WHERE ContentDocumentId IN :lstContentDocumentIds]) {
            contentDocumentIdToContentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
        }
        List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>();
        for (ccrz__E_Order__c order : [SELECT Id, ccrz__Account__r.OwnerId, ccrz__Account__r.Owner.Email, osf_sales_order_acknowledgement__c FROM ccrz__E_Order__c WHERE Id IN: lstOrdersWithSOA]) {
            Messaging.SingleEmailMessage email = createEmail(emailTemplate, order);
            email.setFileAttachments(getAttachments(mapSoaIdAttachmentIds.get(order.osf_sales_order_acknowledgement__c), contentDocumentIdToContentVersionMap));
            lstEmails.add(email);
        }
        if(!lstEmails.isEmpty()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(lstEmails);
        }
    }

    /**********************************************************************************************
    * @Name         : createEmail
    * @Description  : create email message
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2019
    * @param        : EmailTemplate emailTemplate
    * @param        : ccrz__E_Order__c order
    * @Return       : Messaging.SingleEmailMessage
    *********************************************************************************************/
    private static Messaging.SingleEmailMessage createEmail(EmailTemplate emailTemplate, ccrz__E_Order__c order) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateID(emailTemplate.Id); 
        email.setWhatId(order.Id);
        email.setCharset(osf_constant_strings.CHARSET_UTF_8);
        email.setToAddresses(new List<String> {order.ccrz__Account__r.Owner.Email});
        email.setTargetObjectId(order.ccrz__Account__r.OwnerId); 
        email.setUseSignature(false); 
        email.setBccSender(false); 
        email.setSaveAsActivity(false);
        return email;
    }

    /**********************************************************************************************
    * @Name         : getAttachments
    * @Description  : create list of attachments
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2019
    * @param        : List<String> lstContentDocumentIds
    * @param        : Map<String, ContentVersion> contentDocumentIdToContentVersionMap
    * @Return       : List<Messaging.EmailFileAttachment>
    *********************************************************************************************/
    private static List<Messaging.EmailFileAttachment> getAttachments(List<String> lstContentDocumentIds, Map<String, ContentVersion> contentDocumentIdToContentVersionMap) {
        List<Messaging.EmailFileAttachment> lstAttachments = new List<Messaging.EmailFileAttachment> ();
        for (String contentDocumentId : lstContentDocumentIds){
            ContentVersion contentVersion = contentDocumentIdToContentVersionMap.get(contentDocumentId);
            Messaging.EmailFileAttachment att = new Messaging.EmailFileAttachment();
            att.setBody(contentVersion.VersionData);
            att.setFileName(contentVersion.Title + '.' + contentVersion.FileExtension);
            //att.setContentType(contentVersion.FileType);
            lstAttachments.add(att);
        }
        return lstAttachments;
    }
}