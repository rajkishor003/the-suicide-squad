/**
 * File:        osf_ccOrder_TriggerHandler_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 25, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_ccOrder_TriggerHandler
  ************************************************************************
 * History:
 */
@isTest
public without sharing class osf_ccOrder_TriggerHandler_test {
    private static Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
    private static Id newPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.NEW_PO_RECORD_TYPE).getRecordTypeId();

    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        Account acc = osf_testHelper.createAccount('Test Company', '0000000000');
        database.insert(acc);
        Contact c1 = osf_testHelper.createContact('John', 'Doe', acc, 'test@email.com', '0000000000');
        database.insert(c1);
        User user = osf_testHelper.createCommunityUser(c1);
        database.insert(user);
        Database.insert(new List<ccrz__E_PageLabel__c> {
                        osf_testHelper.createPageLabel(osf_constant_strings.ORDER_REQUESTED_CHATTER_MESSAGE_LABEL, osf_constant_strings.ORDER_REQUESTED_STATUS),
                        osf_testHelper.createPageLabel(osf_constant_strings.ORDER_IN_PROGRESS_CHATTER_MESSAGE_LABEL, osf_constant_strings.ORDER_IN_PROGRESS_STATUS)      
        });
        //List<ccrz__E_PageLabel__c> pageLabeList = [SELECT Id, Name, ccrz__PageName__c, ccrz__ValueRT__c, ccrz__Storefront__c]
        osf_PurchaseOrder__c openPO = osf_testHelper.createPurchaseOrder(c1, osf_testHelper.AVAILABLE_PO_AMOUNT, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        openPO.RecordTypeId = openPORecordTypeId;
        Database.insert(openPO);
        osf_PurchaseOrder__c newPO = osf_testHelper.createPurchaseOrder(c1, null, osf_testHelper.CURRENCY_ISO_CODE_USD, '2');
        newPO.RecordTypeId = newPORecordTypeId;
        Database.insert(newPO);
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 10, osf_testHelper.TAX, osf_testHelper.TAX, acc, user, c1);
        Database.insert(cart);
        ccrz__E_Product__c prod = osf_testHelper.createCCProduct(osf_testHelper.PRODUCT_SKU, osf_testHelper.PRODUCT_NAME);
        Database.insert(prod);
        ccrz__E_CartItem__c ci1 = osf_testHelper.createCartItem(prod, cart, 1, osf_testHelper.PRICE);
        Database.insert(ci1);
        osf_quote__c quote = osf_testHelper.createQuote(osf_testHelper.PRICE, acc);
        Database.insert(quote);
    }

    /**********************************************************************************************
    * @Name         : createnewOrder
    * @Description  : Test inserting&updating a new order
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void createNewOrder() {
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        order.ccrz__Account__c = acc.Id;
        Database.insert(order);

        List<FeedItem> feedItemList = [SELECT Id, Body FROM FeedItem WHERE ParentId = :order.Id];
        System.assertEquals(1, feedItemList.size());

        List<EntitySubscription> entitySubscriptionList = new List<EntitySubscription>([SELECT Id FROM EntitySubscription WHERE ParentId =: order.Id]);
        System.assertEquals(1, entitySubscriptionList.size());

        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_IN_PROGRESS_STATUS;
        Database.update(order);
        feedItemList = [SELECT Id, Body FROM FeedItem WHERE ParentId = :order.Id];
        System.assertEquals(2, feedItemList.size());
    }

    /**********************************************************************************************
    * @Name         : cancelOrder
    * @Description  : Test po when an order is cancelled
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void cancelOrderOpenPO() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        osf_PurchaseOrder__c po = [SELECT Id FROM osf_PurchaseOrder__c WHERE RecordTypeId =: openPORecordTypeId LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        ccrz__E_Order__c order1 = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        order1.ccrz__Account__c = acc.Id;
        order1.osf_PurchaseOrder__c = po.Id;
        order1.ccrz__OriginatedCart__c = cart.Id;
        Database.insert(order1);

        order1 = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c FROM ccrz__E_Order__c WHERE Id =: order1.Id LIMIT 1];
        po.osf_BlockedAmount__c = cart.ccrz__TotalAmount__c;
        Database.update(po);
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(true, po.osf_available_amount__c != po.osf_OriginalAmount__c);
        System.assertEquals(po.osf_OriginalAmount__c - po.osf_BlockedAmount__c, po.osf_available_amount__c);
        System.assertNotEquals(0, po.osf_BlockedAmount__c);


        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_CANCELLED_STATUS;
        Database.update(order1);
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(po.osf_OriginalAmount__c, po.osf_available_amount__c);
        System.assertEquals(0, po.osf_BlockedAmount__c);
    }

    /**********************************************************************************************
    * @Name         : cancelOrder
    * @Description  : Test po when an order is cancelled
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 27, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void cancelOrderNewPO() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        osf_PurchaseOrder__c po = [SELECT Id FROM osf_PurchaseOrder__c WHERE RecordTypeId =: newPORecordTypeId LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        ccrz__E_Order__c order1 = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        order1.ccrz__Account__c = acc.Id;
        order1.osf_PurchaseOrder__c = po.Id;
        order1.ccrz__OriginatedCart__c = cart.Id;
        Database.insert(order1);

        order1 = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c FROM ccrz__E_Order__c WHERE Id =: order1.Id LIMIT 1];
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(0, po.osf_available_amount__c );
        System.assertEquals(null, po.osf_BlockedAmount__c);


        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_CANCELLED_STATUS;
        Database.update(order1);
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(0, po.osf_available_amount__c);
        System.assertEquals(null, po.osf_BlockedAmount__c);
    }

    /**********************************************************************************************
    * @Name         : acceptOrder
    * @Description  : Test po when an order is accepted
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void acceptOrderAcceptedPO() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        osf_PurchaseOrder__c po = [SELECT Id FROM osf_PurchaseOrder__c WHERE RecordTypeId =: openPORecordTypeId LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        osf_quote__c quote = [SELECT Id, osf_amount__c FROM osf_quote__c LIMIT 1];
        ccrz__E_Order__c order1 = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        order1.ccrz__Account__c = acc.Id;
        order1.osf_PurchaseOrder__c = po.Id;
        order1.ccrz__OriginatedCart__c = cart.Id;
        order1.osf_quote__c = quote.Id;
        Database.insert(order1);

        order1 = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c FROM ccrz__E_Order__c WHERE Id =: order1.Id LIMIT 1];
        po.osf_BlockedAmount__c = cart.ccrz__TotalAmount__c;
        Database.update(po);
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(true, po.osf_available_amount__c != po.osf_OriginalAmount__c);
        System.assertEquals(po.osf_OriginalAmount__c - po.osf_BlockedAmount__c, po.osf_available_amount__c);
        System.assertNotEquals(0, po.osf_BlockedAmount__c);


        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_ACCEPTD_STATUS;
        Database.update(order1);
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c, osf_UsedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(order1.ccrz__TotalAmount__c, po.osf_UsedAmount__c);
        System.assertEquals(0, po.osf_BlockedAmount__c);
        System.assertEquals(po.osf_OriginalAmount__c - po.osf_UsedAmount__c, po.osf_available_amount__c);

    }

    /**********************************************************************************************
    * @Name         : acceptOrder
    * @Description  : Test po when an order is accepted
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void acceptOrderNewPO() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        osf_PurchaseOrder__c po = [SELECT Id FROM osf_PurchaseOrder__c WHERE RecordTypeId =: newPORecordTypeId LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        osf_quote__c quote = [SELECT Id, osf_amount__c FROM osf_quote__c LIMIT 1];
        ccrz__E_Order__c order1 = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        order1.ccrz__Account__c = acc.Id;
        order1.osf_PurchaseOrder__c = po.Id;
        order1.ccrz__OriginatedCart__c = cart.Id;
        order1.osf_quote__c = quote.Id;
        Database.insert(order1);

        order1 = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c FROM ccrz__E_Order__c WHERE Id =: order1.Id LIMIT 1];
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(0, po.osf_available_amount__c);
        System.assertEquals(null, po.osf_BlockedAmount__c);


        order1.ccrz__OrderStatus__c = osf_constant_strings.ORDER_ACCEPTD_STATUS;
        Database.update(order1);
        po = [SELECT Id, osf_available_amount__c, osf_OriginalAmount__c, osf_BlockedAmount__c, osf_UsedAmount__c FROM osf_PurchaseOrder__c WHERE Id =: order1.osf_PurchaseOrder__c];
        System.assertEquals(null, po.osf_UsedAmount__c);
        System.assertEquals(null, po.osf_BlockedAmount__c);
        System.assertEquals(0, po.osf_available_amount__c);

    }

    /**********************************************************************************************
    * @Name         : checkSalesOrderAcknowledgement_no_SOA
    * @Description  : Test checkSalesOrderAcknowledgement, no soa
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void checkSalesOrderAcknowledgement_no_SOA() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__Account__c = acc.Id;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        insert order;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_PROCESSED_STATUS;
        String errorMessage;
        test.startTest();
        try {
            update order;
        } catch(Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        system.assert(errorMessage.contains(osf_constant_strings.NO_SOA_ERROR));
    }

    /**********************************************************************************************
    * @Name         : checkSalesOrderAcknowledgement_noSoaAttach
    * @Description  : Test checkSalesOrderAcknowledgement, no soa attachment
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void checkSalesOrderAcknowledgement_noSoaAttach() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__Account__c = acc.Id;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        insert order;
        osf_sales_order_acknowledgement__c soa = osf_testHelper.createSoa(acc);
        Database.insert(soa);
        order.osf_sales_order_acknowledgement__c = soa.Id;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_PROCESSED_STATUS;
        String errorMessage;
        test.startTest();
        try {
            update order;
        } catch(Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        system.assert(errorMessage.contains(osf_constant_strings.NO_SOA_ERROR));
    }

    /**********************************************************************************************
    * @Name         : checkSalesOrderAcknowledgement
    * @Description  : Test checkSalesOrderAcknowledgement
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 27, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void checkSalesOrderAcknowledgement() {
        List<ccrz__E_ContactAddr__c> lstAddresses = [SELECT Id FROM ccrz__E_ContactAddr__c];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(lstAddresses[0], lstAddresses[1], true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__Account__c = acc.Id;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        insert order;
        osf_sales_order_acknowledgement__c soa = osf_testHelper.createSoa(acc);
        Database.insert(soa);
        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion);
        contentVersion = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkSoa = osf_testHelper.createContentDocumentLink(soa.Id, lstContentDocuments[0].Id);
        Database.insert(contentDocumentLinkSoa);
        order.osf_sales_order_acknowledgement__c = soa.Id;
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_PROCESSED_STATUS;
        String errorMessage;
        test.startTest();
        try {
            update order;
        } catch(Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        system.assertEquals(null, errorMessage);
        order = [SELECT Id, ccrz__OrderStatus__c FROM ccrz__E_Order__c WHERE Id =: order.Id LIMIT 1];
        system.assertEquals(osf_constant_strings.ORDER_PROCESSED_STATUS, order.ccrz__OrderStatus__c);
    }
}