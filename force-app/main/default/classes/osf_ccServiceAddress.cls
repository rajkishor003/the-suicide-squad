/**
* File:        osf_ccServiceAddress.cls
* Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
* Date:        June 24, 2020
* Created By:  Shikhar Srivastava
************************************************************************
* Description:  Update Address Field mapping
************************************************************************
* History:
* Date:                Modified By:            Description:
*/
global with sharing class osf_ccServiceAddress extends ccrz.ccServiceAddress{
    
    @TestVisible private static final List<String> FIELD_LIST = new List<String> {'osf_Country_Code__c'};
        
        /* 
        * @description  : getFieldsMap
        * @author       : Shikhar Srivastava
        * @createdDate  : June 24, 2020
        * @param        : Map<String, Object> inputData, B2B Commerce OOTB Input Data
        * @see          : ccrz.ccService
        * @return       : Map<String, Object> output data
        */
        global override Map<String, Object> getFieldsMap(Map<String, Object> inputData) {
            Map<String, Object> outputData = super.getFieldsMap(inputData);
            String fields = (String) outputData.get(ccrz.ccService.OBJECTFIELDS);
            fields += osf_constant_strings.COMMA + String.join(FIELD_LIST, osf_constant_strings.COMMA);
            outputData.put(ccrz.ccService.OBJECTFIELDS, fields);
            return outputData;
        }
}