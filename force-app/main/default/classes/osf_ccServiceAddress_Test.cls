/**
 * File:        osf_ccServiceAddress_Test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        June 24, 2019
 * Created By:  Shikhar Srivastava
  ************************************************************************
 * Description: Test class for osf_ccServiceAddress
  ************************************************************************
 * History:
 */
@isTest
public without sharing class osf_ccServiceAddress_Test {

    /**********************************************************************************************
    * @Name         : testGetFieldsMap
    * @Description  : Test method for GetFieldsMap
    * @Created By   : Shikhar Srivastava
    * @Created Date : June 24, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @IsTest
    public static void testGetFieldsMap() {
        osf_ccServiceAddress logic = new osf_ccServiceAddress();
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        Map<String, Object> inputData = new Map<String, Object>();
        inputData.put(ccrz.ccAPIAddress.ADDRESSLIST, new List<Id>{shipTo.Id});
        inputData.put(ccrz.ccAPI.API_VERSION, ccrz.ccAPI.CURRENT_VERSION);
        Map<String, Object> outputData = logic.getFieldsMap(inputData);
        System.assert(!outputData.isEmpty());
        
    }
}