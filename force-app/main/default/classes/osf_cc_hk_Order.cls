/**
 * File:        osf_cc_hk_Order.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 31, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Extension for Order hook class
  ************************************************************************
 * History:
 */
global without sharing class osf_cc_hk_Order extends ccrz.cc_hk_Order {
    
    /**********************************************************************************************
    * @Name         : place
    * @Description  : Place the order
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 31, 2019
    * @param Map<String, Object> inputData; the input data
    * @Return       : inputData
    *********************************************************************************************/
    global override Map<String, Object> place(Map<String, Object> inputData) {
        String currentStep = (String) inputData.get(ccrz.cc_hk_Order.PARAM_PLACE_STEP);
        if (ccrz.cc_hk_Order.STEP_END.equals(currentStep)) {
            updateOrderRelatedData(inputData);
        }
        try {
            if(currentStep == ccrz.cc_hk_Order.STEP_CREATE_ORDER_PRE) {
                ccrz__E_Cart__c ccCart = (ccrz__E_Cart__c) inputData.get(ccrz.cc_hk_Order.PARAM_CART);
                ccCart = [SELECT osf_converted_from__c, osf_converted_from__r.osf_quote__c FROM ccrz__E_Cart__c WHERE Id = :ccCart.Id];
                if(String.isNotBlanK(ccCart.osf_converted_from__c) && String.isNotBlanK(ccCart.osf_converted_from__r.osf_quote__c)) {
                    ccrz__E_Order__c ccOrder = (ccrz__E_Order__c) inputData.get(ccrz.cc_hk_Order.PARAM_ORDER);
                    ccOrder.osf_quote__c = ccCart.osf_converted_from__r.osf_quote__c;
                    inputData.put(ccrz.cc_hk_Order.PARAM_ORDER, ccOrder);
                }
            } else if (currentStep == ccrz.cc_hk_Order.STEP_END) {
                osf_utility.createEmptyCart();
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:hk_Order:place:Error', e);
        }
        return inputData; 
    }

    /**********************************************************************************************
    * @Name         : fetchOrderHistory
    * @Description  : Overriding the method for getting past orders, and returning their prices as 0
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 19, 2019
    * @param Map<String, Object> inputData; the input data
    * @Return       : outputData Map<String, Object>, the map containing the past orders data.
    *********************************************************************************************/
    global override Map<String, Object> fetchOrderHistory(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.fetchOrderHistory(inputData);
        try {
            if(!osf_logicProductPricing.showPrices()) {
                this.hidePricesInMockOrder((List<Object>) outputData.get(ccrz.cc_hk_order.PARAM_ORDERS));
            }
            List<osf_MockOrder> lstOrders = setPropertiesOnOrder((List<Object>) outputData.get(ccrz.cc_hk_order.PARAM_ORDERS));
            outputData.put(ccrz.cc_hk_order.PARAM_ORDERS, lstOrders);
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.Error, 'osf:hk_Order:fetchOrderHistory:Error', e);
        }
        return outputData;
    }

    /**********************************************************************************************
    * @Name         : updateOrderRelatedData
    * @Description  : Update PurchaseOrder, Cart and Order objects
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 31, 2019
    * @param inputData: the input data
    * @Return       : 
    *********************************************************************************************/
    private void updateOrderRelatedData(Map<String, Object> inputData) {
        try {
            Map<String, Object> mapTransactionData = (Map<String, Object>)JSON.deserializeUntyped((String)inputData.get(ccrz.cc_hk_Order.PARAM_TRANSACTION_DATA));
            String storedPaymentSelectionId = mapTransactionData.get(osf_constant_strings.STORED_PAYMENT_SELECTION) == null ? null : (String)mapTransactionData.get(osf_constant_strings.STORED_PAYMENT_SELECTION);
            ccrz__E_Order__c currentOrder = (ccrz__E_Order__c)inputData.get(ccrz.cc_hk_Order.PARAM_ORDER);
            ccrz__E_Cart__c currentCart = (ccrz__E_Cart__c)inputData.get(ccrz.cc_hk_Order.PARAM_CART);
            currentCart = [SELECT Id, osf_PurchaseOrder__c, osf_contracting_entity__c, ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE Id =: currentCart.Id];

            if (String.isNotBlank(storedPaymentSelectionId)) {
                currentCart.osf_PurchaseOrder__c = storedPaymentSelectionId;
                Database.update(currentCart);
                currentOrder.osf_PurchaseOrder__c = storedPaymentSelectionId;
                osf_utility.orderPlacedProcessPO(currentCart, currentOrder);
            } else {
                currentOrder.osf_PurchaseOrder__c = currentCart.osf_PurchaseOrder__c;
            }
            currentOrder.osf_contracting_entity__c = currentCart.osf_contracting_entity__c;
            currentOrder.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
            Database.update(currentOrder);

        } catch(Exception e) {
            ccrz.ccLog.log(LoggingLevel.Error, 'osf:hk_Order:updateOrderRelatedData:Error', e);
        }
    }

    /**********************************************************************************************
    * @Name         : hidePricesInMockOrder
    * @Description  : Returning Pricing fields as 0 for order history
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 19, 2019
    * @param inputData: the input data
    * @Return       : 
    *********************************************************************************************/
    @TestVisible private void hidePricesInMockOrder(List<Object> orderList) {
        for(Object orderObject : orderList) {
            ccrz.cc_bean_MockOrder order = (ccrz.cc_bean_MockOrder) orderObject;
            order.shipAmount = 0;
            order.subTotalAmount = 0;
            order.taxAmount = 0;
            order.totalAmount = 0;
            order.totalSurcharge = 0;
        }
    }

    /**********************************************************************************************
    * @Name         : setPropertiesOnOrder
    * @Description  : Returns canCancel field for order history
    * @Created By   : Alina Craciunel.
    * @Created Date : Dec 03, 2019
    * @param inputData: the input data
    * @Return       : 
    *********************************************************************************************/
    @TestVisible private List<osf_MockOrder> setPropertiesOnOrder(List<Object> orderList) {
        List<osf_MockOrder> lstOrders = new List<osf_MockOrder>();
        List<String> lstOrderIds = new List<String>();
        for(Object orderObject : orderList) {
            ccrz.cc_bean_MockOrder order = (ccrz.cc_bean_MockOrder) orderObject;
            lstOrderIds.add(order.sfid);
        }
        Map<Id, Id> mapOrderHasQuoteAttachment = new Map<Id, Id>();
        Map<Id, Id> mapOrderHasPOAttachment = new Map<Id, Id>();
        Map<Id, Id> mapOrderHasSOAAttachment = new Map<Id, Id>();
        osf_utility.ordersHaveAttachments(lstOrderIds, mapOrderHasQuoteAttachment, mapOrderHasPOAttachment, mapOrderHasSOAAttachment);
        for(Object orderObject : orderList) {
            ccrz.cc_bean_MockOrder order = (ccrz.cc_bean_MockOrder) orderObject;
            if (order.status == osf_constant_strings.ORDER_PO_IN_PROCESS_STATUS || order.status == osf_constant_strings.ORDER_PO_ACCEPTED_STATUS 
                || order.status == osf_constant_strings.ORDER_ACCEPTD_STATUS || order.status == osf_constant_strings.ORDER_PROCESSED_STATUS
                || order.status == osf_constant_strings.ORDER_CANCELLED_STATUS) {
                order.canCancel = false;
            }
            osf_MockOrder customMock = new osf_MockOrder(order);
            customMock.poAttachmentId = mapOrderHasPOAttachment.get(order.sfid);
            customMock.quoteAttachmentId = mapOrderHasQuoteAttachment.get(order.sfid);
            customMock.soaAttachmentId = mapOrderHasSOAAttachment.get(order.sfid);
            customMock.hasPO = customMock.poAttachmentId != null;
            customMock.hasQuote = customMock.quoteAttachmentId != null;
            customMock.hasSOA = customMock.soaAttachmentId != null;
            lstOrders.add(customMock);
        }
        return lstOrders;
    }

    public class osf_MockOrder {
        public Boolean hasPO {get; set;}
        public Boolean hasQuote {get; set;}
        public Boolean hasSOA {get; set;}
        public Id poAttachmentId {get; set;}
        public Id quoteAttachmentId {get; set;}
        public Id soaAttachmentId {get; set;}
        public String poNumber {get; set;}
        public String name {get; set;}
        public Double shipAmount {get; set;}
        public Double subTotalAmount {get; set;}
        public Date orderDate {get; set;}
        public String orderDateStr {get; set;}
        public String encryptedId {get; set;}
        public String status {get; set;}
        public Decimal orderNumber {get; set;}
        public String orderNumberAsString {get; set;}
        public String sfid {get; set;}
        public String ownerName {get; set;}
        public Double taxAmount {get; set;}
        public Double totalAmount {get; set;}
        public Double totalSurcharge {get; set;}
        public String cartId {get; set;}
        public String currencyIsoCode {get; set;}
        public Boolean canCancel {get; set;}
        public Boolean canAmend {get; set;}
        public Boolean canReOrder {get; set;}
        public String orderShipAmount {get; set;}
        public String orderSubTotal {get; set;}
        public String orderTotalAmount {get; set;}
        public String orderTotalSurcharge {get; set;}
        public osf_MockOrder(ccrz.cc_bean_MockOrder mockOrder) {
            poNumber = mockOrder.poNumber;
            name = mockOrder.name;
            shipAmount = mockOrder.shipAmount;
            subTotalAmount = mockOrder.subTotalAmount;
            orderDate = mockOrder.orderDate;
            orderDateStr = mockOrder.orderDateStr;
            encryptedId = mockOrder.encryptedId;
            status = mockOrder.status;
            orderNumber = mockOrder.orderNumber;
            orderNumberAsString = mockOrder.orderNumberAsString;
            sfid = mockOrder.sfid;
            ownerName = mockOrder.ownerName;
            taxAmount = mockOrder.taxAmount;
            totalAmount = mockOrder.totalAmount;
            totalSurcharge = mockOrder.totalSurcharge;
            cartId = mockOrder.cartId;
            currencyIsoCode = mockOrder.currencyIsoCode;
            canCancel = mockOrder.canCancel;
            canAmend = mockOrder.canAmend;
            canReOrder = mockOrder.canReOrder;
            orderShipAmount = mockOrder.orderShipAmount;
            orderSubTotal = mockOrder.orderSubTotal;
            orderTotalAmount = mockOrder.orderTotalAmount;
            orderTotalSurcharge = mockOrder.orderTotalSurcharge;
        }
    }
    
}