/**
 * File:        osf_cc_hk_Order_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 1, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_cc_hk_Order
  ************************************************************************
 * History:
 */

@isTest
public without sharing class osf_cc_hk_Order_test {
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        List<User> userList = [SELECT Id, UserRoleId FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true AND ccrz__dataId__c =: osf_testHelper.USER_DATA_ID AND UserRoleId != null];
        User sysadmin; 
        if(userList.isEmpty()) {
            Profile sysadminUserProfile = [SELECT Id FROM Profile WHERE Name =: osf_testHelper.SYS_ADMIN_PROFILE LIMIT 1];
            UserRole r = osf_testHelper.createUserRole();
            Database.insert(r);
            sysadmin = osf_testHelper.createUser(sysadminUserProfile, osf_testHelper.USER_DATA_ID, osf_testHelper.USER_EMAIL, osf_testHelper.USER_FIRST_NAME, osf_testHelper.USER_LAST_NAME, osf_testHelper.USER_DATA_ID);
            sysadmin.UserRoleId = r.Id;
            Database.insert(sysadmin);
        } else {
            sysadmin = userList[0];
        }
        System.runAs(sysadmin) {
            Account account = osf_testHelper.createAccount('Test Company', '0000000000');
            database.insert(account);
            Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
            database.insert(contact);
            User user = osf_testHelper.createCommunityUser(contact);
            database.insert(user);
        }
    }

    /**********************************************************************************************
    * @Name         : testPlace
    * @Description  : Test method for place method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 1, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testPlaceOpenPO() {
        osf_cc_hk_Order hook = new osf_cc_hk_Order();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE LIMIT 1];
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Product__c prod = osf_testHelper.createCCProduct(osf_testHelper.PRODUCT_SKU, osf_testHelper.PRODUCT_NAME);
        Database.insert(prod);
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        osf_PurchaseOrder__c po = osf_testHelper.createPurchaseOrder(c1, osf_testHelper.AVAILABLE_PO_AMOUNT, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        po.RecordTypeId = openPORecordTypeId;
        Database.insert(po);
        ccrz__E_Cart__c cart;
        ccrz__E_Order__c order;
        cart = osf_testHelper.createCart(null, null, 10, osf_testHelper.TAX, osf_testHelper.TAX, acc, u1, c1);
        Database.insert(cart);
        ccrz__E_CartItem__c ci1 = osf_testHelper.createCartItem(prod, cart, 1, osf_testHelper.PRICE);
        Database.insert(ci1);
        //system.runAs(u1) {
        order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1, osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        Database.insert(order);
        ccrz__E_OrderItem__c orderItem = osf_testHelper.createCCOrderItem(order, prod, 1, osf_testHelper.PRICE);
        Database.insert(orderItem);
        cart = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id LIMIT 1];
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.cc_hk_Order.PARAM_CART_ID => cart.Id,
            ccrz.cc_hk_Order.PARAM_CART => cart,
            ccrz.cc_hk_Order.PARAM_CART_ITEMS => new List<ccrz__E_CartItem__c> {ci1},
            ccrz.cc_hk_Order.PARAM_ORDER => order,
            ccrz.cc_hk_Order.PARAM_ORDER_ITEMS => new List<ccrz__E_OrderItem__c> {orderItem},
            ccrz.cc_hk_Order.PARAM_PLACE_STEP => ccrz.cc_hk_Order.STEP_END,
            ccrz.cc_hk_Order.PARAM_TRANSACTION_DATA => JSON.serialize(new Map<String, Object>{osf_constant_strings.STORED_PAYMENT_SELECTION => po.Id})
        };
        Test.startTest();
        hook.place(inputData);
        Test.stopTest();
        //}
        ccrz__E_Cart__c currentCart = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c, ccrz__Account__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id LIMIT 1];
        ccrz__E_Order__c currentOrder = [SELECT Id, osf_PurchaseOrder__c FROM ccrz__E_Order__c WHERE Id =: order.Id LIMIT 1];
        osf_PurchaseOrder__c currentPO = [SELECT Id, osf_BlockedAmount__c, osf_available_amount__c FROM osf_PurchaseOrder__c WHERE Id =: po.Id LIMIT 1];
        System.assertEquals(po.Id, currentCart.osf_PurchaseOrder__c);
        System.assertEquals(po.Id, currentOrder.osf_PurchaseOrder__c);
        System.assertEquals(currentCart.ccrz__TotalAmount__c, currentPO.osf_BlockedAmount__c);
        System.assertEquals(osf_testHelper.AVAILABLE_PO_AMOUNT-currentCart.ccrz__TotalAmount__c, currentPO.osf_available_amount__c);
    }

    @IsTest
    public static void testPlaceWithQuote() {
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND IsActive = true LIMIT 1];
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Product__c prod = osf_testHelper.createCCProduct(osf_testHelper.PRODUCT_SKU, osf_testHelper.PRODUCT_NAME);
        Database.insert(prod);
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        osf_PurchaseOrder__c po = osf_testHelper.createPurchaseOrder(c1, osf_testHelper.AVAILABLE_PO_AMOUNT, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        Database.insert(po);
        ccrz__E_Cart__c cart;
        ccrz__E_Order__c order;
        cart = osf_testHelper.createCart(null, null, 10, osf_testHelper.TAX, osf_testHelper.TAX, acc, u1, c1);
        Database.insert(cart);
        ccrz__E_CartItem__c ci1 = osf_testHelper.createCartItem(prod, cart, 1, osf_testHelper.PRICE);
        Database.insert(ci1);
        osf_cc_hk_Order hook = new osf_cc_hk_Order();
        order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1, osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        Database.insert(order);
        ccrz__E_OrderItem__c orderItem = osf_testHelper.createCCOrderItem(order, prod, 1, osf_testHelper.PRICE);
        Database.insert(orderItem);
        osf_quote__c quote = osf_testHelper.createQuote(200, acc);
        insert quote;
        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion);
        contentVersion = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments1 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion.ContentDocumentId];
        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, lstContentDocuments1[0].Id);
        Database.insert(contentDocumentLink);
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Available');
        requestForQuote.osf_quote__c = quote.Id;
        requestForQuote.osf_status__c = 'Available';
        insert requestForQuote;
        requestForQuote = [SELECT Id, osf_quote__c, Name FROM osf_request_for_quote__c WHERE Id = :requestForQuote.Id];
        cart = [SELECT Id, ccrz__TotalAmount__c, osf_PurchaseOrder__c, ccrz__Name__c, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id LIMIT 1];
        cart.ccrz__Name__c = osf_constant_strings.CONVERTED_FROM + requestForQuote.Name;
        cart.osf_converted_from__c = requestForQuote.Id;
        cart.osf_sharedCart__c = false;
        update cart;
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.cc_hk_Order.PARAM_CART_ID => cart.Id,
            ccrz.cc_hk_Order.PARAM_CART => cart,
            ccrz.cc_hk_Order.PARAM_ORDER => order,
            ccrz.cc_hk_Order.PARAM_PLACE_STEP => ccrz.cc_hk_Order.STEP_CREATE_ORDER_PRE,
            ccrz.cc_hk_Order.PARAM_ENC_CART_ID => cart.ccrz__EncryptedId__c
        };
        Test.startTest();
        hook.place(inputData);
        Test.stopTest();
    }

    /**********************************************************************************************
    * @Name         : testPlaceWithContractingEntity
    * @Description  : Test method for place an order from a cart with a contracting entity
    * @Created By   : Alina Craciunel
    * @Created Date : May 12, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @IsTest
    public static void testPlaceWithContractingEntity() {
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND IsActive = true LIMIT 1];
        Contact c1 = [SELECT Id FROM Contact LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_Product__c prod = osf_testHelper.createCCProduct(osf_testHelper.PRODUCT_SKU, osf_testHelper.PRODUCT_NAME);
        Database.insert(prod);
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, 'USA', osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, 'USA', osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        insert new List<ccrz__E_ContactAddr__c> {shipTo, billTo};
        osf_contracting_entity__c ce_USA = osf_testHelper.createContractEntity('Doe', 'address2', 'USA');
        ce_USA.osf_active__c = true;
        insert ce_USA;
        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 10, osf_testHelper.TAX, osf_testHelper.TAX, acc, u1, c1);
        cart.osf_contracting_entity__c = ce_USA.Id;
        Database.insert(cart);
        ccrz__E_CartItem__c ci1 = osf_testHelper.createCartItem(prod, cart, 1, osf_testHelper.PRICE);
        Database.insert(ci1);
        osf_cc_hk_Order hook = new osf_cc_hk_Order();
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1, osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        Database.insert(order);
        ccrz__E_OrderItem__c orderItem = osf_testHelper.createCCOrderItem(order, prod, 1, osf_testHelper.PRICE);
        Database.insert(orderItem);
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.cc_hk_Order.PARAM_CART_ID => cart.Id,
            ccrz.cc_hk_Order.PARAM_CART => cart,
            ccrz.cc_hk_Order.PARAM_ORDER => order,
            ccrz.cc_hk_Order.PARAM_PLACE_STEP => ccrz.cc_hk_Order.STEP_END,
            ccrz.cc_hk_Order.PARAM_ENC_CART_ID => cart.ccrz__EncryptedId__c,
            ccrz.cc_hk_Order.PARAM_TRANSACTION_DATA => JSON.serialize(new Map<String, Object>{osf_constant_strings.STORED_PAYMENT_SELECTION => ''})
        };
        Test.startTest();
        hook.place(inputData);
        Test.stopTest();
        order = [SELECT Id, osf_contracting_entity__c FROM ccrz__E_Order__c WHERE Id =: order.Id];
        System.assertEquals(ce_USA.Id, order.osf_contracting_entity__c);
    }

    /**********************************************************************************************
    * @Name         : testSubscribeToOrderChatterFeed
    * @Description  : Test method for subscribeToOrderChatterFeed method
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 4, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testSubscribeToOrderChatterFeed() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1, osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__Account__c = acc.Id;
        
        Test.startTest();
        Database.insert(order);
        Test.stopTest();

        List<EntitySubscription> lstEntitySubscriptions = new List<EntitySubscription>([SELECT Id, SubscriberId, ParentId FROM EntitySubscription WHERE ParentId =: order.Id LIMIT 10]);
        System.assertEquals(false, lstEntitySubscriptions.isEmpty());
    }

    @isTest
    public static void testFetchOrderHistory() {
        osf_testHelper.setupStorefront();

        Account account = [SELECT Id, osf_prices_visible__c FROM Account WHERE Name = 'Test Company'];
        account.osf_prices_visible__c = false;
        update account;

        ccrz.cc_CallContext.currAccountId = account.Id;
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;

        Contact contact = [SELECT Id FROM Contact WHERE AccountId = :account.Id];
        User user = [SELECT Id FROM User WHERE ContactId = :contact.Id];
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        
        osf_quote__c quote = osf_testHelper.createQuote(200, account);
        Database.insert(quote);
        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion);
        contentVersion = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments1 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion.ContentDocumentId];
        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, lstContentDocuments1[0].Id);
        Database.insert(contentDocumentLink);

        osf_sales_order_acknowledgement__c soa = osf_testHelper.createSoa(account);
        Database.insert(soa);
        ContentVersion contentVersion2 = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion2);
        contentVersion2 = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion2.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments2 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion2.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkSoa = osf_testHelper.createContentDocumentLink(soa.Id, lstContentDocuments2[0].Id);
        Database.insert(contentDocumentLinkSoa);
        
        Id openPORecordTypeId = Schema.SObjectType.osf_PurchaseOrder__c.getRecordTypeInfosByDeveloperName().get(osf_constant_strings.OPEN_PO_RECORD_TYPE).getRecordTypeId();
        osf_PurchaseOrder__c po = osf_testHelper.createPurchaseOrder(contact, osf_testHelper.AVAILABLE_PO_AMOUNT, osf_testHelper.CURRENCY_ISO_CODE_USD, '1');
        po.RecordTypeId = openPORecordTypeId;
        Database.insert(po);
        ContentVersion contentVersion3 = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        Database.insert(contentVersion3);
        contentVersion3 = [SELECT Id, ContentDOcumentId FROM ContentVersion WHERE Id =: contentVersion3.Id LIMIT 1];
        List<ContentDocument> lstContentDocuments3 = [SELECT Id FROM ContentDocument WHERE Id =: contentVersion3.ContentDocumentId];
        ContentDocumentLink contentDocumentLinkPo = osf_testHelper.createContentDocumentLink(po.Id, lstContentDocuments3[0].Id);
        Database.insert(contentDocumentLinkPo);

        System.runAS(user) {
            osf_cc_hk_Order hook = new osf_cc_hk_Order();

            ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1, osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
            order.ccrz__Account__c = account.Id;
            order.ccrz__Contact__c = contact.Id;
            order.ccrz__User__c = user.Id;
            order.ccrz__CurrencyISOCode__c = 'USD';
            order.osf_quote__c = quote.Id;
            order.osf_PurchaseOrder__c = po.Id;
            order.osf_sales_order_acknowledgement__c = soa.Id;
            order.ccrz__OrderStatus__c = osf_testHelper.ORDER_STATUS;
            Database.insert(order);
            ccrz__E_Order__c searchOrder = [SELECT Id, Name, ccrz__OrderNumber__c FROM ccrz__E_Order__c WHERE Id = :order.Id];
            Map<String, Object> inputData = new Map<String, Object> {
                ccrz.cc_hk_Order.PARAM_SEARCH_ORDER_STATUS => osf_testHelper.ORDER_STATUS
            };

            Test.startTest();
            Map<String, Object> outputData = hook.fetchOrderHistory(inputData);
            Test.stopTest();
            System.assert(!outputData.isEmpty());
            System.assert(outputData.containsKey(ccrz.cc_hk_Order.PARAM_ORDERS));
            List<osf_cc_hk_Order.osf_MockOrder> lstOrders = (List<osf_cc_hk_Order.osf_MockOrder>)outputData.get(ccrz.cc_hk_order.PARAM_ORDERS);
            System.assert(lstOrders.size() > 0);
            System.assert(lstOrders[0].hasPO);
            System.assert(lstOrders[0].hasQuote);
            System.assert(lstOrders[0].hasSOA);
        }
    }

    @IsTest
    public static void testHidePricesInMockOrderAndSetCanCancelOrder() {
        osf_cc_hk_Order hook = new osf_cc_hk_Order();
        ccrz.cc_bean_MockOrder order = new ccrz.cc_bean_MockOrder();
        order.status = osf_constant_strings.ORDER_CANCELLED_STATUS;
        hook.hidePricesInMockOrder(new List<Object> {order});
        System.assertEquals(0, order.shipAmount);
        System.assertEquals(0, order.subTotalAmount);
        System.assertEquals(0, order.taxAmount);
        System.assertEquals(0, order.totalAmount);
        System.assertEquals(0, order.totalSurcharge);
        hook.setPropertiesOnOrder(new List<Object> {order});
        System.assert(!order.canCancel);
    }
}