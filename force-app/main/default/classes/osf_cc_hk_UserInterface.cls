global with sharing class osf_cc_hk_UserInterface extends ccrz.cc_hk_UserInterface.v004 {
    
    public static final String OSF_THEME_RESOURCE = 'OSF_CCThemeCapricorn';
    public static final String FAVICON_PATH = 'images/favicon';

    global virtual override String metaContent(){
        String link = '';
        try{
            link = super.metaContent();
            link += '<link rel="icon" type="image/png" href="' + resourcePath( OSF_THEME_RESOURCE, FAVICON_PATH) + '/thumb.png" />';
            link += '<title>myAdvantest Shop</title>';
            return link;
        }catch(Exception ex){
            ccrz.cclog.log('exceptie: - ' + ex);
            return link;
        }
    } 
}