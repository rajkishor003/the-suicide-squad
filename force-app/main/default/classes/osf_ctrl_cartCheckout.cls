/**
* Controller class for osf_CartCheckout component
* 
* @author Ozgun Eser
* @version 1.0
*/

global with sharing class osf_ctrl_cartCheckout {
    
    /* Disables current cart and creates a new cart
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult createNewCart(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            String newCartId = osf_constant_strings.EMPTY_STRING;
            List<ccrz__E_Cart__c> cartListToUpdate = new List<ccrz__E_Cart__c> ();
            List<ccrz__E_Cart__c> cartList = [SELECT Id, ccrz__EncryptedId__c, ccrz__ActiveCart__c FROM ccrz__E_Cart__c WHERE ccrz__TotalQuantity__c = 0 AND ccrz__CartType__c = :osf_constant_strings.CART_TYPE_CART AND ccrz__CurrencyISOCode__c = :ccrz.cc_CallContext.userCurrency AND ccrz__Storefront__c = :ctx.Storefront AND ccrz__CartStatus__c = :osf_constant_strings.OPEN AND ccrz__User__c = :ccrz.cc_CallContext.currUserId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(!cartList.isEmpty()) {
                newCartId = cartList[0].ccrz__EncryptedId__c;
                String cartId = (String) ccrz.cc_CallContext.currCartId;
                if(String.isNotBlank(cartId)) {
                    List<ccrz__E_Cart__c> currentCartList = [SELECT Id, ccrz__ActiveCart__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :cartId];
                    if(!currentCartList.isEmpty()) {
                        currentCartList[0].ccrz__ActiveCart__c = false;
                        cartListToUpdate.add(currentCartList[0]);
                    }
                    cartList[0].ccrz__ActiveCart__c = true;
                    cartListToUpdate.add(cartList[0]);
                    update cartListToUpdate;
                }
            } else {
                Map<String, Object> outputData = ccrz.ccAPICart.create(new Map<String, Object> {
                    ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
                    ccrz.ccApiCart.CART_OBJLIST => new List<Map<String, Object>>{new Map<String, Object>{
                        osf_constant_strings.ACTIVE_CART => true, 
                        osf_constant_strings.CART_TYPE => osf_constant_strings.CART_TYPE_CART, 
                        osf_constant_strings.STOREFRONT => ccrz.cc_CallContext.storefront,
                        osf_constant_strings.CURRENCY_ISO_CODE => ccrz.cc_CallContext.userCurrency}
                	}
                });
                newCartId = (String) outputData.get(ccrz.ccAPICart.CART_ENCID);
            } 

            data.put(osf_constant_strings.NEW_CART_ID, newCartId);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:cartCheckout:createNewCart:Error', e.getMessage() + ' in line ' + e.getLineNumber());
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }
}