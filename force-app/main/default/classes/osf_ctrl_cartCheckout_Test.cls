@IsTest
public class osf_ctrl_cartCheckout_Test {
    
    @TestSetup
    public static void createTestData(){
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        cart.ccrz__Name__c = 'Converted From RFQ-000000';
        insert cart;
    }

    @IsTest
    public static void testCreateNewCart() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        String cartId = cart.Id;
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        System.runAs(user) {
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_cartCheckout.createNewCart(ctx);
            Test.stopTest();
            System.assert(result.success);
            Map<String, Object> data = (Map<String, Object>) result.data;
            System.assertEquals(1, data.size());
            System.assert(data.containsKey(osf_constant_strings.NEW_CART_ID));
            List<ccrz__E_Cart__c> cartList = [SELECT Id, ccrz__ActiveCart__c, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
            for(ccrz__E_Cart__c ccCart : cartList) {
                if(ccCart.Id == cart.Id) {
                    System.assert(!ccCart.ccrz__ActiveCart__c);
                } else {
                    System.assert(ccCart.ccrz__ActiveCart__c);
                    System.assertEquals(ccCart.ccrz__EncryptedId__c, (String) data.get(osf_constant_strings.NEW_CART_ID));
                }
            }
        }
    }

    @IsTest
    public static void testCreateNewCartWithExisting() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        Contact contact = [SELECT Id FROM Contact WHERE Email = 'test@email.com'];
        Account account = [SELECT Id FROM Account WHERE Name = 'Test Company'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        String cartId = cart.Id;
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        System.runAs(user) {
            ccrz__E_Cart__c existingCart = osf_testHelper.createCart(null, null, 50, 200, 20, account, user, contact);
            insert existingCart;
            existingCart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE Id = :existingCart.Id];
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_cartCheckout.createNewCart(ctx);
            Test.stopTest();
            System.assert(result.success);
            Map<String, Object> data = (Map<String, Object>) result.data;
            System.assertEquals(1, data.size());
            System.assert(data.containsKey(osf_constant_strings.NEW_CART_ID));
            String newCartId = (String) data.get(osf_constant_strings.NEW_CART_ID);
            System.assertEquals(newCartId, existingCart.ccrz__EncryptedId__c);
        }
    }
}