/*
* Controller class for osf_chat Visualforce Component
*
* @author Ozgun Eser
* @date 06 Nov 2019
*/

global without sharing class osf_ctrl_chat {
    public static Boolean allowsPagination {get; set;}
    
    /**********************************************************************************************
    * @Name         : getParentId
    * @Description  : get parent id based on the query string param
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 14, 2019
    * @param queryParams: the map of query string params
    * @Return       : parent id as string
    *********************************************************************************************/
    private static String getParentId(Map<String, String> queryParams) {
        String quoteId = queryParams.get(osf_constant_strings.REQUEST_FOR_QUOTE_ID);
        if (!String.isBlank(quoteId)) {
            return quoteId;
        }
        String orderEncId = queryParams.get(osf_constant_strings.ORDER_QUERYSTRING_PARAM);
        if (!String.isBlank(orderEncId)) {
            List<ccrz__E_Order__c> lstOrders = new List<ccrz__E_Order__c>([SELECT Id FROM ccrz__E_Order__c  WHERE ccrz__EncryptedId__c =: orderEncId]);
            if (!lstOrders.isEmpty()) {
                return lstOrders[0].Id;
            }
        }
        return null;
    }

    /* Get Chatter Messages for Request For Quote
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getMessages(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            Map<String, String> queryParams = ccrz.cc_CallContext.currPageParameters;
            String parentId = getParentId(queryParams);
            List<Map<String, Object>> feedItemList = osf_utility.getChatterMessages(parentId);
            if(!feedItemList.isEmpty()) {
                data.put(osf_constant_strings.MESSAGES, feedItemList);
            }
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:chat:getMessages:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /* Create Chatter Message
    *
    * @param ctx, Remote Action Context
    * @param message, String, message of customer.
    * @return result, Remote Action Result
    *
    */
    @RemoteAction
    global static ccrz.cc_RemoteActionResult sendMessage(ccrz.cc_RemoteActionContext ctx, String message) {
        ccrz.cc_remoteActionResult result = ccrz.cc_CallContext.init(ctx);
        try {
            if(String.isNotBlank(message)) {
                Map<String, String> queryParams = ccrz.cc_CallContext.currPageParameters;
                String parentId = getParentId(queryParams);
                insert osf_utility.createFeedItem(parentId, message, false);
            }
            result.success = true;  
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:chat:sendMessage:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

}