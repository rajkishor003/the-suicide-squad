@isTest
public without sharing class osf_ctrl_chat_Test {
    
    @TestSetup
    public static void createTestData() {
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;

        Database.insert(new List<ccrz__E_PageLabel__c> {
                osf_testHelper.createPageLabel(osf_constant_strings.ORDER_REQUESTED_CHATTER_MESSAGE_LABEL, osf_constant_strings.ORDER_REQUESTED_STATUS),
                osf_testHelper.createPageLabel(osf_constant_strings.ORDER_IN_PROGRESS_CHATTER_MESSAGE_LABEL, osf_constant_strings.ORDER_IN_PROGRESS_STATUS),
                osf_testHelper.createPageLabel(osf_constant_strings.QUOTE_REQUESTED_CHATTER_MESSAGE_LABEL, osf_constant_strings.QUOTE_STATUS_QUOTE_REQUESTED)      
        });

        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;

        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, osf_constant_strings.QUOTE_STATUS_QUOTE_REQUESTED);
        insert requestForQuote;

        //Account account = [SELECT Id FROM Account LIMIT 1];
        ccrz__E_ContactAddr__c shipTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY); 
        Database.insert(shipTo);
        ccrz__E_ContactAddr__c billTo = osf_testHelper.createContactAddress(osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.ADDRESS, osf_testHelper.CITY, osf_testHelper.STATE, osf_testHelper.COUNTRY, osf_testHelper.CURRENCY_ISO_CODE_USD, osf_testHelper.POSTAL_CODE, osf_testHelper.COMPANY);
        Database.insert(billTo);
        ccrz__E_Order__c order = osf_testHelper.createCCOrder(shipTo, billTo, true, osf_testHelper.CONTACT_FIRST_NAME1, osf_testHelper.CONTACT_LAST_NAME1, osf_testHelper.CONTACT_EMAIL1,osf_testHelper.CONTACT_PHONE1, '', osf_testHelper.TAX);
        order.ccrz__OrderStatus__c = osf_constant_strings.ORDER_REQUESTED_STATUS;
        order.ccrz__Account__c = account.Id;
        Database.insert(order);
    }

    @isTest
    public static void testGetMessages() {
        osf_request_for_quote__c requestForQuote = [SELECT Id FROM osf_request_for_quote__c];
        ccrz.cc_CallContext.currPageParameters.put(osf_constant_strings.REQUEST_FOR_QUOTE_ID, requestForQuote.Id);
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_chat.getMessages(ctx);
        Test.stopTest();
        System.assert(result.success);
        Map<String, Object> data = (Map<String, Object>) result.data;
        System.assert(!data.isEmpty());
        List<Map<String, Object>> messageList = (List<Map<String, Object>>) data.get(osf_constant_strings.MESSAGES);
        System.assert(!messageList.isEmpty());
        System.assertEquals(1, messageList.size());
        
        Map<String, Object> messageMap = messageList[0];
        System.assertEquals('Quote Requested', (String) messageMap.get(osf_constant_strings.CHATTER_MESSAGE_BODY));
        System.assertEquals(requestForQuote.Id, (String) messageMap.get(osf_constant_strings.PARENT_ID));
        System.assertEquals(String.valueOf(System.today()), (String) messageMap.get(osf_constant_strings.CREATED_DATE));
    }

    @isTest
    public static void testSendMessage() {
        osf_request_for_quote__c requestForQuote = [SELECT Id FROM osf_request_for_quote__c];
        ccrz.cc_CallContext.currPageParameters.put(osf_constant_strings.REQUEST_FOR_QUOTE_ID, requestForQuote.Id);
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_chat.sendMessage(ctx, 'Test Comment');
        Test.stopTest();
        System.assert(result.success);

        List<FeedItem> feedItemList = [SELECT Id, Body FROM FeedItem WHERE ParentId = :requestForQuote.Id ORDER BY CreatedDate DESC];
        System.assert(!feedItemList.isEmpty());
        System.assertEquals(2, feedItemList.size());
        System.assertEquals('Test Comment', feedItemList[0].Body);
    }

    @isTest
    public static void testSendMessageCatchBlock() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_chat.sendMessage(ctx, 'Test Comment');
        Test.stopTest();
        System.assert(!result.success);
    }

    /**********************************************************************************************
    * @Name         : testGetMessagesForOrders
    * @Description  : Test getMessages for orders
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
    public static void testGetMessagesForOrders() {
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Order__c LIMIT 1];
        ccrz.cc_CallContext.currPageParameters.put(osf_constant_strings.ORDER_QUERYSTRING_PARAM, order.ccrz__EncryptedId__c);
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_chat.getMessages(ctx);
        Test.stopTest();
        System.assert(result.success);
        Map<String, Object> data = (Map<String, Object>) result.data;
        System.assert(!data.isEmpty());
        List<Map<String, Object>> messageList = (List<Map<String, Object>>) data.get(osf_constant_strings.MESSAGES);
        System.assert(!messageList.isEmpty());
        System.assertEquals(1, messageList.size());
        
        Map<String, Object> messageMap = messageList[0];
        System.assertEquals('Order Requested', (String) messageMap.get(osf_constant_strings.CHATTER_MESSAGE_BODY));
        System.assertEquals(order.Id, (String) messageMap.get(osf_constant_strings.PARENT_ID));
        System.assertEquals(String.valueOf(System.today()), (String) messageMap.get(osf_constant_strings.CREATED_DATE));
    }

    /**********************************************************************************************
    * @Name         : testSendMessageForOrders
    * @Description  : Test sendMessage for orders
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 25, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
    public static void testSendMessageForOrders() {
        ccrz__E_Order__c order = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Order__c LIMIT 1];
        ccrz.cc_CallContext.currPageParameters.put(osf_constant_strings.ORDER_QUERYSTRING_PARAM, order.ccrz__EncryptedId__c);
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_chat.sendMessage(ctx, 'Test Comment');
        Test.stopTest();
        System.assert(result.success);

        List<FeedItem> feedItemList = [SELECT Id, Body FROM FeedItem WHERE ParentId = :order.Id ORDER BY CreatedDate DESC];
        System.assert(!feedItemList.isEmpty());
        System.assertEquals(2, feedItemList.size());
        System.assertEquals('Test Comment', feedItemList[0].Body);
    }
}