/**
 * File:        osf_ctrl_contactus.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 7, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Controller class for osf_subscr_contactus Subscriber page.
  ************************************************************************
 * History:
 * Date:                Modified By:            Description:
 */

//Described as without sharing since the email should be sent to account owner in some cases but the user cannot see Internal User information.
global without sharing class osf_ctrl_contactus {

    /**********************************************************************************************

    * @Name         : createCase
    * @Description  : The Method will be called when submit form button is clicked. Will create Case.
    * @Created By   : Ozgun Eser
    * @Created Date : Oct 7, 2019
    * @Param        : ccrz.cc_RemoteActionContext ctx, Map<String, Object> formData
    * @Return       : ccrz.cc_RemoteActionResult
     
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult sendEmailNotification(ccrz.cc_RemoteActionContext ctx, Map<String, Object> formData) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            String topic = formData.containsKey(osf_constant_strings.TOPIC) ? (String) formData.get(osf_constant_strings.TOPIC) : osf_constant_strings.EMPTY_STRING;
            String feedback = formData.containsKey(osf_constant_strings.FEEDBACK) ? (String) formData.get(osf_constant_strings.FEEDBACK) : osf_constant_strings.EMPTY_STRING;
            Boolean sendToTopicOwner = false;
            osf_contact_us_email_configuration__c emailConfiguration = osf_contact_us_email_configuration__c.getValues(topic);
            if(emailConfiguration == null || emailConfiguration.osf_email__c == osf_constant_strings.ACCOUNT_OWNER) {
                sendToTopicOwner = true;
            }
            User topicOwner;
            Account account = (Account) ccrz.cc_CallContext.currAccount;
            User currentUser = (User) ccrz.cc_CallContext.currUser;
            List<String> emailAddressList = new List<String> ();
            if(sendToTopicOwner) {
                topicOwner = [SELECT FirstName, LastName, Email FROM User WHERE Id = :account.OwnerId];
                emailAddressList.add(topicOwner.Email);       
            } else {
                emailAddressList.add(emailConfiguration.osf_email__c);
            }
            EmailTemplate emailTemplate = [SELECT Subject, HtmlValue FROM EmailTemplate WHERE Name = :osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_NAME];

            String subject = emailTemplate.Subject;
            subject = subject.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_TOPIC_NAME, topic);

            String htmlBody = emailTemplate.HtmlValue;
            htmlBody = htmlBody.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_OWNER_FIRST_NAME, sendToTopicOwner ? topicOwner.FirstName : osf_constant_strings.EMPTY_STRING);
            htmlBody = htmlBody.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_OWNER_LAST_NAME, sendToTopicOwner ? topicOwner.LastName : osf_constant_strings.EMPTY_STRING);
            htmlBody = htmlBody.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_TOPIC_NAME, topic);
            htmlBody = htmlBody.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_SENDER_EMAIL, currentUser.Email);       
            htmlBody = htmlBody.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_ACCOUNT_NAME, account.Name);
            htmlBody = htmlBody.replace(osf_constant_strings.CONTACT_US_EMAIL_TEMPLATE_TOPIC_MESSAGE, feedback);

            String accountId = (String) ccrz.cc_CallContext.currAccountId;

            List<Attachment> attachmentList = [SELECT Id, Body, Name, ContentType FROM Attachment WHERE Name LIKE 'cusAtt-%' AND ParentId = :accountId];

            List<Messaging.EmailFileAttachment> attList = new List<Messaging.EmailFileAttachment> ();
            for(Attachment attachment : attachmentList) {
                Messaging.EmailFileAttachment att = new Messaging.EmailFileAttachment();
                att.setBody(attachment.Body);
                att.setFileName(attachment.Name.removeStart(osf_constant_strings.CONTACT_US_FORM_ATTACHMENT));
                att.setContentType(attachment.ContentType);
                attList.add(att);
            }

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setCharset(osf_constant_strings.CHARSET_UTF_8);
            email.setToAddresses(emailAddressList);
            email.setSubject(subject);
            email.setHtmlBody(htmlBody);

            if(!attList.isEmpty()) {
                email.setFileAttachments(attList);
            }
            List<Messaging.SendEmailResult> sendEmailResultList = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
            delete attachmentList;    

            for(Messaging.SendEmailResult sendEmailResult : sendEmailResultList) {
                if(!sendEmailResult.isSuccess()) {
                    throw new ContactUsException(sendEmailResult.getErrors()[0].getMessage());
                }
            }

            result.success = true;
        } catch (Exception e) {
            delete [SELECT Id, Body, Name, ContentType FROM Attachment WHERE Name LIKE 'cusAtt-%' AND ParentId = :ccrz.cc_CallContext.currAccountId];
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:contactus:createCase:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /**********************************************************************************************
    * @Name                 : attachBlob
    * @Description          : attach files to the Account
    * @Created By           : Ozgun Eser
    * @Created Date         : Nov 12, 2019
    * @param attach         : serialized attachment model
    * @Return               : the attachment id
    *********************************************************************************************/
    @RemoteAction
    global static String attachBlob(String attach) {
        osf_AttachModel attachModel = (osf_AttachModel)JSON.deserialize(attach, osf_AttachModel.class);

        Attachment attachment;
        if(String.isBlank(attachModel.attachmentId)) {
            attachment = new Attachment(
                ParentId = (String) ccrz.cc_CallContext.currAccountId,
                Body = EncodingUtil.Base64Decode(attachModel.base64BlobValue),
                Name = osf_constant_strings.CONTACT_US_FORM_ATTACHMENT + attachModel.fileName,
                ContentType = attachModel.contentType
            );
            insert attachment;
            return attachment.Id;
        } else {
            attachment = [SELECT Id, Body FROM Attachment WHERE Id = :attachModel.attachmentId LIMIT 1];
            attachment.Body = EncodingUtil.Base64Decode(EncodingUtil.Base64Encode(attachment.Body) + attachModel.base64BlobValue);
            update attachment;
            return attachModel.attachmentId;
        }
    }

    public class ContactUsException extends Exception {}
}