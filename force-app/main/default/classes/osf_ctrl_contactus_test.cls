@isTest
public class osf_ctrl_contactus_test {
    @TestSetup
    public static void createTestData(){
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;
    }

    @isTest
    public static void testContactForm() {
        User user = [SELECT Id, LanguageLocaleKey, Email FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        Account account = [SELECT Id, Name, OwnerId FROM Account WHERE Name = 'Test Company'];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_CallContext.currAccount = account;
        ccrz.cc_CallContext.currAccountId = account.Id;
        ccrz.cc_CallContext.currUser = user;
        ccrz.cc_CallContext.isGuest = false;
        osf_AttachModel attachModel = new osf_AttachModel(null, null, 'test_file.txt', 'text/plain', 'Test File Content', null);
        String attachmentId = osf_ctrl_contactus.attachBlob(JSON.serialize(attachModel));
        System.assert(String.isNotBlank(attachmentId));
        List<Attachment> attachment = [SELECT Id FROM Attachment WHERE Id = :attachmentId];
        System.assertEquals(1, attachment.size());
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_contactus.sendEmailNotification(ctx, new Map<String, Object> {
            osf_constant_strings.TOPIC => 'Test Topic',
            osf_constant_strings.FEEDBACK => 'Test Feedback'
        });
        Test.stopTest();
        System.assert(result.success);
        attachment = [SELECT Id FROM Attachment WHERE Id = :attachmentId];
        System.assert(attachment.isEmpty());
    }
}