/**
* Controller class for osf_header page
* 
* @author Ozgun Eser
* @version 1.0
*/

global with sharing class osf_ctrl_header {
    
    /* Check if Prices are Visible for the Current Account
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult checkPriceVisibility(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try{
            Boolean isVisible = osf_logicProductPricing.showPrices();
            data.put(osf_constant_strings.PRICES_VISIBLE, isVisible);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:header:checkPriceVisibility:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    } 

    /* Fetches available currencies
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchAvailableCurrencies(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        if(ccrz.cc_CallContext.isGuest) {
            data.put(osf_constant_strings.AVAILABLE_CURRENCIES, new Set<String>{});
            data.put(osf_constant_strings.DEFAULT_CURRENCY, '');
            result.success = true;
            return result;
        }
        try {
            Set<String> setAvailableCurrencies = osf_cache.getAvailableCurrencies();
            if (setAvailableCurrencies == null) {
                setAvailableCurrencies = osf_utility.fetchCurrencyByCustomer(ccrz.cc_CallContext.currAccount);
                osf_cache.setAvailableCurrencies(setAvailableCurrencies);
            } 
            data.put(osf_constant_strings.AVAILABLE_CURRENCIES, setAvailableCurrencies);
            data.put(osf_constant_strings.DEFAULT_CURRENCY, ccrz.cc_CallContext.currUser.ccrz__CC_CurrencyCode__c);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:header:fetchAvailableCurrencies:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /* Change Currency
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeCurrency(ccrz.cc_RemoteActionContext ctx, String selectedCurrency) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
		Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        ccrz__E_Cart__c newCart = new ccrz__E_Cart__c();
        String encryptedId;
        try {
            User user = [SELECT Id, ccrz__CC_CurrencyCode__c FROM User WHERE Id = :ccrz.cc_CallContext.currUserId];
            user.ccrz__CC_CurrencyCode__c = selectedCurrency;
            update user;
            
            List<ccrz__E_Cart__c> existCart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__CartStatus__c = :osf_constant_strings.OPEN
                           						AND ccrz__User__c = :ccrz.cc_CallContext.currUserId  AND ccrz__CurrencyISOCode__c = :selectedCurrency  
                           						AND ccrz__CartType__c = :osf_constant_strings.CART_TYPE_CART ORDER BY LastModifiedDate DESC LIMIT 1];
            if (!existCart.isEmpty()) {
                encryptedId = existCart[0].ccrz__EncryptedId__c;
            } else {
                Map<String, Object> outputData = ccrz.ccAPICart.create(new Map<String, Object> {
                    ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
                    ccrz.ccApiCart.CART_OBJLIST => new List<Map<String, Object>> {	
                        new Map<String, Object>
                         	{
                              osf_constant_strings.ACTIVE_CART => true, 
                              osf_constant_strings.CART_TYPE => osf_constant_strings.CART_TYPE_CART, 
                              osf_constant_strings.STOREFRONT => ccrz.cc_CallContext.storefront,
                              osf_constant_strings.CURRENCY_ISO_CODE => selectedCurrency
                            }
                        }
                });
                encryptedId = (String) outputData.get(ccrz.ccAPICart.CART_ENCID);
                ccrz__E_Cart__c updateCart = [SELECT CurrencyIsoCode FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :encryptedId LIMIT 1];
                updateCart.CurrencyIsoCode = selectedCurrency;
                update updateCart;
            } 
            data.put(osf_constant_strings.CART_ENCRYPTED_ID, encryptedId);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(loggingLevel.ERROR, 'osf:ctrl:header:changeCurrency:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /**********************************************************************************************
    * @Name         : deactivateCurrentUser
    * @Description  : get parent id based on the query string param
    * @Created By   : Alina Craciunel
    * @Created Date : Dec 04, 2019
    * @Param ctx    : remote action context
    * @Return result: Remote Action Result
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult deactivateCurrentUser(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        result.success = false;
        try {
            User user = [SELECT Id FROM User WHERE Id = :ccrz.cc_CallContext.currUserId];
            user.IsActive = false;
            Database.update(user);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:header:deactivateCurrentUser:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    
    /**********************************************************************************************
    * @Name         : checkLockedCart
    * @Description  : return Boolean value of if the Cart is locked or not
    * @Created By   : Ozgun Eser
    * @Created Date : Dec 17, 2019
    * @Param        : ccrz.cc_RemoteActionContext ctx
    * @Return       : ccrz.cc_RemoteActionResult, containing the cart locked Boolean data
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult checkLockedCart(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            Boolean lockedCart = false;
            String cartId = (String) ctx.currentCartId;
            if(String.isNotBlank(cartId)) {
                List<ccrz__E_Cart__c> cartList = [SELECT osf_converted_from_rfq__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :cartId];
                if(!cartList.isEmpty()) {
                    lockedCart = cartList[0].osf_converted_from_rfq__c;
                }
            }
            data.put(osf_constant_strings.LOCKED_CART, lockedCart);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(loggingLevel.ERROR, 'osf:ctrl:header:checkLockedCart:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }
}