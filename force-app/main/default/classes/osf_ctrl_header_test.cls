@IsTest
public class osf_ctrl_header_test {
    
    @TestSetup
    public static void createTestData(){
        ccrz__E_AccountGroup__c accountGroup = osf_testHelper.createCCAccountGroup('Test Account Group');
        insert accountGroup;

        ccrz__E_Pricelist__c pricelistUSD = osf_testHelper.createCCPricelist('Test Pricelist USD', 'USD');
        ccrz__E_Pricelist__c pricelistJPY = osf_testHelper.createCCPricelist('Test Pricelist JPY', 'JPY');
        insert new List<ccrz__E_Pricelist__c> {pricelistUSD, pricelistJPY};

        ccrz__E_AccountGroupPricelist__c accountGroupPricelistUSD = osf_testHelper.createCCAccountGroupPricelist(accountGroup, pricelistUSD);
        ccrz__E_AccountGroupPricelist__c accountGroupPricelistJPY = osf_testHelper.createCCAccountGroupPricelist(accountGroup, pricelistJPY);
        insert new List<ccrz__E_AccountGroupPricelist__c> {accountGroupPricelistUSD, accountGroupPricelistJPY};

        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        account.osf_prices_visible__c = true;
        account.ccrz__E_AccountGroup__c = accountGroup.Id;
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'johndoe@testemail.com','0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        user.ccrz__CC_CurrencyCode__c = 'USD';
        user.CurrencyISOCode = 'USD';
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        cart.ccrz__Name__c = osf_constant_strings.CONVERTED_FROM + 'RFQ-000001';
        cart.ccrz__ActiveCart__c = true;
        insert cart;
        
        ccrz__E_Cart__c cartJPY = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        cartJPY.ccrz__Name__c = osf_constant_strings.CONVERTED_FROM + 'RFQ-000002';
        cartJPY.ccrz__ActiveCart__c = true;
        cartJPY.ccrz__CurrencyISOCode__c = 'JPY';
        insert cartJPY;

        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, osf_constant_strings.QUOTE_STATUS_QUOTE_REQUESTED);
        insert requestForQuote;

        cart.osf_converted_from__c = requestForQuote.Id;
        update cart;
    }

    @IsTest
    public static void testCheckPriceVisiblity() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'johndoe@testemail.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'USD' LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        System.runAs(user) {
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_header.checkPriceVisibility(ctx);
            Test.stopTest();
            System.assert(result.success);
            Map<String, Object> data = (Map<String, Object>) result.data;
            System.assertEquals(1, data.size());
            System.assert(data.containsKey(osf_constant_strings.PRICES_VISIBLE));
            System.assert((Boolean) data.get(osf_constant_strings.PRICES_VISIBLE));
        }
    }

    @IsTest
    public static void testFetchAvailableCurrencies() {
        User user = [SELECT Id, LanguageLocaleKey, ccrz__CC_CurrencyCode__c FROM User WHERE Username = 'johndoe@testemail.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'USD' LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Account account = [SELECT Id, ccrz__E_AccountGroup__c FROM Account WHERE Name = 'Test Company'];
        List<ccrz__E_AccountGroupPriceList__c> agpl = [SELECT ccrz__Pricelist__c FROM ccrz__E_AccountGroupPriceList__c  WHERE ccrz__AccountGroup__c = :account.ccrz__E_AccountGroup__c AND ccrz__StartDate__c <= TODAY AND ccrz__EndDate__c >= TODAY AND ccrz__Enable__c = TRUE];
        System.debug('agpl ----> ' + agpl);
        System.debug('agpl.size() -----> ' + agpl.size());
        ccrz.cc_CallContext.currUser = user;
        ccrz.cc_CallContext.currAccount = account;
        //System.runAs(user) {
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_header.fetchAvailableCurrencies(ctx);
            result = osf_ctrl_header.fetchAvailableCurrencies(ctx); //duplicate call in order to cover cache class
            Test.stopTest();
            System.assert(result.success);
            Map<String, Object> data = (Map<String, Object>) result.data;
            System.assertEquals(2, data.size());
            System.assert(data.containsKey(osf_constant_strings.AVAILABLE_CURRENCIES));
            Set<String> currencySet = (Set<String>) data.get(osf_constant_strings.AVAILABLE_CURRENCIES);
            System.assertEquals(2, currencySet.size());
            System.assert(currencySet.contains('USD'));
            System.assert(currencySet.contains('JPY'));
            System.assert(data.containsKey(osf_constant_strings.DEFAULT_CURRENCY));
            System.assertEquals('USD', (String) data.get(osf_constant_strings.DEFAULT_CURRENCY));
        //}
    }

    @IsTest
    public static void testChangeCurrency() {
        User user = [SELECT Id, LanguageLocaleKey, ccrz__CC_CurrencyCode__c FROM User WHERE Username = 'johndoe@testemail.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'USD' LIMIT 1];
        
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);

        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_header.changeCurrency(ctx, 'JPY');
        Test.stopTest();
        System.assert(result.success);
        User changedUser = [SELECT ccrz__CC_CurrencyCode__c FROM User WHERE Id = :user.Id];
        System.assertEquals('JPY', changedUser.ccrz__CC_CurrencyCode__c);
    }
    
    /**********************************************************************************************
    * @Name         : testNewChangeCurrency
    * @Description  : Test ChangeCurrency method to create new cart
    * @Created By   : Shikhar Srivastava
    * @Created Date : June 16, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @IsTest
    public static void testNewChangeCurrency() {
        User user = [SELECT Id, LanguageLocaleKey, ccrz__CC_CurrencyCode__c FROM User WHERE Username = 'johndoe@testemail.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'USD' LIMIT 1];
        
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        system.runAs(user) {
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_header.changeCurrency(ctx, 'EUR');
            Test.stopTest();
            System.assert(result.success);
        }
        User changedUser = [SELECT ccrz__CC_CurrencyCode__c FROM User WHERE Id = :user.Id];
        ccrz__E_Cart__c newCart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'EUR' LIMIT 1];
        System.assertEquals('EUR', changedUser.ccrz__CC_CurrencyCode__c);
        System.assert(newCart.Id != Null);
    }

    /**********************************************************************************************
    * @Name         : testDeactivateCurrentUser
    * @Description  : Test deactivateCurrentUser
    * @Created By   : Alina Craciunel
    * @Created Date : Dec 04, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @IsTest
    public static void testDeactivateCurrentUser() {
        User user = [SELECT Id, LanguageLocaleKey, ccrz__CC_CurrencyCode__c FROM User WHERE Username = 'johndoe@testemail.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'USD' LIMIT 1];
        
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        System.runAs(user) {
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_header.deactivateCurrentUser(ctx);
            Test.stopTest();
            System.assert(result.success);
            User changedUser = [SELECT Id, IsActive FROM User WHERE Id = :user.Id];
            System.assertEquals(false, changedUser.IsActive);
        }
    }

    @IsTest
    public static void testCheckLockedCart() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'johndoe@testemail.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c, osf_converted_from_rfq__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__CurrencyISOCode__c = 'USD' LIMIT 1];
        System.debug('cart -----> ' + cart);
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_header.checkLockedCart(ctx);
        Test.stopTest();
        System.assert(result.success);
        Map<String, Object> data = (Map<String, Object>) result.data;
        System.assert(data.containsKey(osf_constant_strings.LOCKED_CART));
        System.assert((Boolean) data.get(osf_constant_strings.LOCKED_CART));
    }
}