/**
* Controller class for osf_my_request_for_quote component
* 
* @author Ozgun Eser
* @version 1.0
*/

global without sharing class osf_ctrl_myquotes {

    /* Get Requests For Quotes of the Current Account
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getRequestsForQuotes(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            User currentUser = (User) ccrz.cc_CallContext.currUser;
            Set<String> quoteIdSet = new Set<String> ();
            Map<String, String> quoteIdToAttachmentIdMap = new Map<String, String> ();
            List<osf_request_for_quote__c> requestForQuoteList = [SELECT Id, Name, CreatedDate, osf_cart__r.ccrz__TotalAmount__c, osf_status__c, osf_quote__c, CurrencyIsoCode FROM osf_request_for_quote__c WHERE CreatedById = :currentUser.Id /*AND CurrencyISOCode = :currentUser.ccrz__CC_CurrencyCode__c*/ ORDER BY CreatedDate DESC];

            for(osf_request_for_quote__c requestForQuote : requestForQuoteList) {
                if(String.isNotBlank(requestForQuote.osf_quote__c)) {
                    quoteIdSet.add(requestForQuote.osf_quote__c);
                }
            }            

            if(!quoteIdSet.isEmpty()) {
                for(ContentDocumentLink attachment : [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :quoteIdSet]) {
                    quoteIdToAttachmentIdMap.put(attachment.LinkedEntityId, attachment.ContentDocumentId); 
                }
            }
            
            List<Map<String, Object>> quoteDataMap = transformQuoteData(requestForQuoteList, quoteIdToAttachmentIdMap);
            data.put(osf_constant_strings.REQUESTS_FOR_QUOTES, quoteDataMap);
            data.put(osf_constant_strings.PRICES_VISIBLE, osf_logicProductPricing.showPrices());
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:myquotes:getRequestsForQuotes', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /* Transforms Request For Quote Data to Map<String, Object> to send to the frontend Backbone model.
	*
	* @param requestForQuotelist
	* @param quoteIdToAttachmentIdSet
    * @return List<Map<String, Object>>
    *
	*/
    private static List<Map<String, Object>> transformQuoteData(List<osf_request_for_quote__c> requestForQuoteList, Map<String, String> quoteIdToAttachmentIdMap) {
        Map<String, ContentVersion> contentDocumentIdToContentVersionMap = new Map<String, ContentVersion> ();
        List<Map<String, Object>> quoteDataMap = new List<Map<String, Object>> ();
        for(osf_request_for_quote__c requestForQuote : requestForQuoteList) {
            Map<String, Object> quoteMap = new Map<String, Object> {
                osf_constant_strings.SFDC_NAME => requestForQuote.Name,
                osf_constant_strings.STATUS => requestForQuote.osf_status__c,
                osf_constant_strings.REQUEST_DATE => String.valueOf(requestForQuote.CreatedDate.Date()),
                osf_constant_strings.SF_ID => requestForQuote.Id,
                osf_constant_strings.QUOTE_AMOUNT => osf_logicProductPricing.showPrices() ? requestForQuote.osf_cart__r.ccrz__TotalAmount__c : 0,
                osf_constant_strings.RFQ_CURRENCY => requestForQuote.CurrencyIsoCode
            };
            if(!quoteIdToAttachmentIdMap.isEmpty()) {
                for(ContentVersion contentVersion : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :quoteIdToAttachmentIdMap.values()]) {
                    contentDocumentIdToContentVersionMap.put(contentVersion.ContentDocumentId, contentVersion);
                }
            }

            if(String.isNotBlank(requestForQuote.osf_quote__c) && quoteIdToAttachmentIdMap.containsKey(requestForQuote.osf_quote__c) && !contentDocumentIdToContentVersionMap.isEmpty() && contentDocumentIdToContentVersionMap.containsKey(quoteIdToAttachmentIdMap.get(requestForQuote.osf_quote__c))) {
                ContentVersion contentVersion = contentDocumentIdToContentVersionMap.get(quoteIdToAttachmentIdMap.get(requestForQuote.osf_quote__c));
                quoteMap.put(osf_constant_strings.ATTACHMENT_ID, contentVersion.Id);
            }
            quoteDataMap.add(quoteMap);
        }
        return quoteDataMap;
    }    
}