@isTest
public class osf_ctrl_myquotes_Test {
    @TestSetup
    public static void createTestData() {
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        user.ccrz__CC_CurrencyCode__c = 'USD';
        insert user;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        insert product;

        ccrz__E_ProductMedia__c productMedia = osf_testHelper.createProductMedia(product, 'en_US', osf_constant_strings.MEDIA_TYPE_THUBMNAIL);
        insert productMedia;

        Attachment attachment = osf_testHelper.createAttachment(productMedia.Id, 'Test Media', 'Test Media Image');
        insert attachment;

        osf_quote__c quote = osf_testHelper.createQuote(200, account);
        quote.osf_contact__c = contact.Id;
        quote.osf_account__c = account.Id;
        insert quote;

        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        insert contentVersion;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, documents[0].Id);
        insert contentDocumentLink;

        System.runAs(user) {
            ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
            insert cart;

            ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
            insert cartItem;

            osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Available');
            requestForQuote.osf_quote__c = quote.Id;
            requestForQuote.CurrencyISOCode = 'USD';
            insert requestForQuote;
        }
    }

    @isTest
    public static void testGetRequestsForQuotes() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__Account__c, ccrz__Contact__c, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        osf_request_for_quote__c requestForQuote = [SELECT Id, Name FROM osf_request_for_quote__c];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        System.runAs(user) {
            Test.startTest();
            ccrz.cc_RemoteActionResult result = osf_ctrl_myquotes.getRequestsForQuotes(ctx);
            Test.stopTest();
            System.assert(result.success);
            Map<String, Object> data = (Map<String, Object>) result.data;
            System.assert(!data.isEmpty());
            List<Map<String, Object>> quoteDataMap = (List<Map<String, Object>>) data.get(osf_constant_strings.REQUESTS_FOR_QUOTES);
            System.assert(!quoteDataMap.isEmpty());
            System.assertEquals(1, quoteDataMap.size());
            Map<String, Object> quoteMap = quoteDataMap[0];
            System.assertEquals(requestForQuote.Name, (String) quoteMap.get(osf_constant_strings.SFDC_NAME));
            System.assertEquals('Available', (String) quoteMap.get(osf_constant_strings.STATUS));        
            System.assertEquals(String.valueOf(System.today()), (String) quoteMap.get(osf_constant_strings.REQUEST_DATE));        
            System.assertEquals(requestForQuote.Id, (String) quoteMap.get(osf_constant_strings.SF_ID));         
        }
    }
}