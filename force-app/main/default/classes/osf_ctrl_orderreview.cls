/**
* Controller class for osf_orderreview component
* 
* @author Ozgun Eser
* @version 1.0
*/

global without sharing class osf_ctrl_orderreview {

    /**********************************************************************************************

    * @Name         : createRequestForQuote
    * @Description  : The Method will be called when "Request for Quote" button is clicked. Will create Request For Quote Object.
    * @Created By   : Ozgun Eser
    * @Created Date : May 5
    * @Param        : ccrz.cc_RemoteActionContext ctx, String shippingAddressId, String billingAddressId
    * @Return       : ccrz.cc_RemoteActionResult
     
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult createRequestForQuote(ccrz.cc_RemoteActionContext ctx, String shippingAddressId, String billingAddressId) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        try {
            String cartId = (String) ccrz.cc_CallContext.currCartId;
            Contact contact = (Contact) ccrz.cc_CallContext.currContact;
            Account account = (Account) ccrz.cc_CallContext.currAccount;

            ccrz__E_Cart__c cart = [SELECT Id, ccrz__ActiveCart__c, ccrz__CartStatus__c, ccrz__CurrencyISOCode__c, ccrz__BillTo__c, ccrz__ShipTo__c, osf_contracting_entity__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :cartId];
            
            Map<Id, ccrz__E_ContactAddr__c> contactAddressMap = new Map<Id, ccrz__E_ContactAddr__c> ([SELECT ccrz__AddressFirstline__c, ccrz__AddressSecondline__c, ccrz__AddressThirdline__c, 
                                                                                                      ccrz__City__c, ccrz__CompanyName__c, ccrz__Country__c, ccrz__CountryISOCode__c,
                                                                                                      ccrz__Email__c, ccrz__FirstName__c, ccrz__LastName__c, ccrz__PostalCode__c, ccrz__State__c, ccrz__StateISOCode__c 
                                                                                                      FROM ccrz__E_ContactAddr__c WHERE Id IN (:billingAddressId, :shippingAddressId)
                                                                                                     ]);
            
            ccrz__E_ContactAddr__c billAddress = contactAddressMap.get(billingAddressId).clone();
            ccrz__E_ContactAddr__c shipAddress = contactAddressMap.get(shippingAddressId).clone();
            insert billAddress;
            insert shipAddress;
            
            cart.ccrz__BillTo__c = billAddress.Id;
            cart.ccrz__ShipTo__c = shipAddress.Id;
            update cart;

            osf_request_for_quote__c requestForQuote = new osf_request_for_quote__c (
                osf_cart__c = cart.Id,
                osf_contact__c = contact.Id,
                osf_account__c = account.Id,
                osf_status__c = osf_constant_strings.QUOTE_REQUESTED,
                OwnerId = account.OwnerId,
                CurrencyISOCode = cart.ccrz__CurrencyISOCode__c,
                osf_contracting_entity__c = cart.osf_contracting_entity__c
            );
            insert requestForQuote;

            cart.ccrz__ActiveCart__c = false;
            cart.ccrz__CartStatus__c = osf_constant_strings.CART_CLOSED;
            update cart;
            osf_utility.createEmptyCart();
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:cart:createRequestForQuote:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /**********************************************************************************************
    * @Name         : fetchContractingEntitiesAddresses
    * @Description  : get contracting entities 
    * @Created By   : Alina Craciunel
    * @Created Date : May 06, 2020
    * @param        : ccrz.cc_RemoteActionContext ctx
    * @Return       : ccrz.cc_RemoteActionResult result
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchContractingEntitiesAddresses(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            Set<String> setCodes = getAccountAddressesCountryCodes(ccrz.cc_CallContext.currAccountId);
            Map<String, osf_contracting_entity__c> mapContractingEntitiesByCode = getContractingEntitiesIsoCode(setCodes);
            data.put(osf_constant_strings.CONTRACTING_ENTITIES, mapContractingEntitiesByCode);
            Boolean validCreditCardLimit = checkCreditLimit((String)ccrz.cc_CallContext.currAccountId, (String)ccrz.cc_CallContext.currUser.ccrz__CC_CurrencyCode__c, (String) ccrz.cc_CallContext.currCartId);
            data.put(osf_constant_strings.VALID_CREDIT_LIMIT, validCreditCardLimit);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:orderreview:fetchContractingEntitiesAddresses:Error', e + ' in line ' + e.getLineNumber());
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /**********************************************************************************************
    * @Name         : getAccountAddressesCountryCodes
    * @Description  : get country iso codes for cc account address books for an account
    * @Created By   : Alina Craciunel
    * @Created Date : May 06, 2020
    * @param        : accountId
    * @Return       : the result as Set of country iso codes
    *********************************************************************************************/
    private static Set<String> getAccountAddressesCountryCodes(String accountId) {
        Set<String> setCodes = new Set<String>();
        Map<String,Object> inputData = (new Map<String,Object>{
            ccrz.ccApi.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPIAddressBook.ACCOUNTID => accountId
        });
        Map<String, Object> outputData = ccrz.ccAPIAddressBook.fetch(inputData);
        if (outputData.get(ccrz.ccAPIAddressBook.ADDRESSBOOKS) != null) {
            List<Map<String, Object>> addressBookList = (List<Map<String, Object>>)outputData.get(osf_constant_strings.ADDRESS_LIST);
            for (Map<String, Object> addressBook : addressBookList) {
                if (addressBook.get(osf_constant_strings.COUNTRY_ISO_CODE) != null) {
                    setCodes.add((String)addressBook.get(osf_constant_strings.COUNTRY_ISO_CODE));
                }
            }
        }
        return setCodes;
    }

    /**********************************************************************************************
    * @Name         : getContractingEntitiesIsoCode
    * @Description  : get contracting entity map based on country iso codes
    * @Created By   : Alina Craciunel
    * @Created Date : May 06, 2020
    * @param        : Set<String> setCodes
    * @Return       : the result as Map<String, osf_contracting_entity__c>
    *********************************************************************************************/
    private static Map<String, osf_contracting_entity__c> getContractingEntitiesIsoCode(Set<String> setCodes) {
        Map<String, osf_contracting_entity__c> mapContractingEntitiesByCode = new Map<String, osf_contracting_entity__c>();
        for (osf_contracting_entity__c ce : [SELECT Id, osf_name__c, osf_address__c, osf_country_iso_code__c FROM osf_contracting_entity__c WHERE osf_country_iso_code__c IN: setCodes AND osf_active__c = true ORDER BY CreatedDate DESC]) {
            if (!mapContractingEntitiesByCode.containsKey(ce.osf_country_iso_code__c)) {
                mapContractingEntitiesByCode.put(ce.osf_country_iso_code__c, ce);
            }
        }
        return mapContractingEntitiesByCode;
    }

    /**********************************************************************************************
    * @Name         : saveContractingEntity
    * @Description  : save selected contract entity on cart
    * @Created By   : Alina Craciunel
    * @Created Date : May 06, 2020
    * @param        : ccrz.cc_RemoteActionContext ctx
    * @param        : String contractingEntityId
    * @Return       : ccrz.cc_RemoteActionResult result
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult saveContractingEntity(ccrz.cc_RemoteActionContext ctx, String contractingEntityId) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        try {
            String cartId = (String) ccrz.cc_CallContext.currCartId;
            ccrz__E_Cart__c cart = [SELECT Id FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: cartId];
            cart.osf_contracting_entity__c = contractingEntityId;
            update cart;
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:orderreview:saveContractingEntity:Error', e + ' in line ' + e.getLineNumber());
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

        /**********************************************************************************************
    * @Name         : checkCreditLimit
    * @Description  : Check the cerdit limit for the current user
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 28, 2019
    * @param        : String accountId
    * @param        : String currencyCode
    * @param        : String cartId
    * @Return       : Boolean credit is valid
    *********************************************************************************************/
    @TestVisible
    private static Boolean checkCreditLimit(String accountId, String currencyCode, String cartId) {
        List<osf_CreditLimit__c> lstCreditLimits = [SELECT osf_Amount__c FROM osf_CreditLimit__c WHERE osf_Account__c =: accountId AND osf_CurrencyISOCode__c =: currencyCode];
        if (lstCreditLimits.isEmpty()) {
            return true;
        } else {
            List<ccrz__E_Cart__c> lstCarts = new List<ccrz__E_Cart__c>([SELECT Id, ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: cartId]);
            if (!lstCarts.isEmpty()) {
                return lstCarts[0].ccrz__TotalAmount__c > lstCreditLimits[0].osf_Amount__c ? false : true;
            }
            return true;
        }

    }
}