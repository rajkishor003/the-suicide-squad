@IsTest
public class osf_ctrl_orderreview_test {
    public static final Decimal CREDIT_LIMIT_AMOUNT1 = 10;
    public static final Decimal CREDIT_LIMIT_AMOUNT2 = 1000;
    
    @TestSetup
    public static void createTestData() {
        User sysadmin = [SELECT Id, LanguageLocaleKey, UserRoleId FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true AND UserRoleId != null LIMIT 1];
        system.runAs(sysadmin) {
            Account account = osf_testHelper.createAccount(osf_testHelper.ACCOUNT_NAME1, '0000000000');
            database.insert(account);
            Contact contact1 = osf_testHelper.createContact('John', 'Doe', account, osf_testHelper.USER_EMAIL1, '0000000000');
            database.insert(contact1);
            User user1 = osf_testHelper.createCommunityUser(contact1);
            user1.ccrz__CC_CurrencyCode__c = osf_testHelper.CURRENCY_ISO_CODE_USD;
            database.insert(user1);
            Contact contact2 = osf_testHelper.createContact('Johnny', 'Doey', account, osf_testHelper.USER_EMAIL2, '1111111111');
            database.insert(contact2);
            User user2 = osf_testHelper.createCommunityUser(contact2);
            database.insert(user2);
            ccrz__E_Product__c product = osf_testHelper.createCCProduct('111', 'test prod');
            database.insert(product);
        }
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        system.runAs(u1) {
            Account acc = [SELECT Id FROM Account LIMIT 1];  
            Contact c = [SELECT Id FROM Contact LIMIT 1];
            ccrz__E_Product__c p =[SELECT Id FROM ccrz__E_Product__c LIMIT 1];
            ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 10, 100, 10, acc, u1, c);
            database.insert(cart);
            ccrz__E_CartItem__c  cartItem = osf_testHelper.createCartItem(p, cart, 1, osf_testHelper.PRICE);
            database.insert(cartItem);

            ccrz__E_ContactAddr__c shipTo =  osf_testHelper.createContactAddress('John', 'Doe', 'Test address 1', 'Izmir', 'Izmir', 'Turkey', 'TR', '01234', 'Test Company');
            ccrz__E_ContactAddr__c billTo =  osf_testHelper.createContactAddress('John', 'Test', 'Test address 2', 'Chicago', 'Chicago', 'USA', 'USA', '01234', 'Test Company');
            insert new List<ccrz__E_ContactAddr__c> {shipTo, billTo};
            osf_contracting_entity__c ce_TR = osf_testHelper.createContractEntity('John', 'address1', 'TR');
            ce_TR.osf_active__c = false;
            osf_contracting_entity__c ce_USA = osf_testHelper.createContractEntity('Doe', 'address2', 'USA');
            ce_USA.osf_active__c = true;
            insert new List<osf_contracting_entity__c> {ce_TR, ce_USA};
            insert osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_SHIPPING, shipTo, u1);
            insert osf_testHelper.createCCAccountAddressBook(acc, osf_constant_strings.ADDRESS_TYPE_SHIPPING, billTo, u1);
            
            cart.osf_contracting_entity__c = ce_USA.Id;
            update cart;
        }
    }

    @isTest
	public static void testCreateRequestForQuote() {
        Account acc = [SELECT Id FROM Account WHERE Name = :osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Username =: osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionResult res;
        ccrz__E_ContactAddr__c contactAddressShipping;
        ccrz__E_ContactAddr__c contactAddressBilling;
        Test.startTest();
        system.runAs(u1) {
            contactAddressShipping = [SELECT Id FROM ccrz__E_ContactAddr__c WHERE ccrz__CountryISOCode__c =: 'TR' LIMIT 1];
            contactAddressBilling = [SELECT Id FROM ccrz__E_ContactAddr__c WHERE ccrz__CountryISOCode__c =: 'USA' LIMIT 1];
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
            res = osf_ctrl_orderreview.createRequestForQuote(ctx, contactAddressShipping.Id, contactAddressBilling.Id);
        }
        Test.stopTest();
        System.assertEquals(true, res.success);
        System.assertEquals(u1.Id, ccrz.cc_CallContext.currUserId);
        System.assertEquals(acc.Id, ccrz.cc_CallContext.currAccountId);

        List<osf_request_for_quote__c> rfqList = [SELECT Id, osf_status__c, osf_account__c, osf_cart__r.ccrz__ShipTo__c, osf_cart__r.ccrz__BillTo__c, osf_cart__r.ccrz__BillTo__r.ccrz__CountryISOCode__c, osf_contracting_entity__c FROM osf_request_for_quote__c];
        System.assert(!rfqList.isEmpty());
        System.assertEquals(1, rfqList.size());
        System.assertEquals(osf_constant_strings.QUOTE_REQUESTED, rfqList[0].osf_status__c);
        System.assertEquals(acc.Id, rfqList[0].osf_account__c);
        System.assert(contactAddressShipping.Id != null);
        System.assert(contactAddressBilling.Id != null);
        osf_contracting_entity__c cs = [SELECT Id FROM osf_contracting_entity__c WHERE osf_country_iso_code__c =: rfqList[0].osf_cart__r.ccrz__BillTo__r.ccrz__CountryISOCode__c];
        System.assertEquals(cs.Id, rfqList[0].osf_contracting_entity__c);
    }

    /**********************************************************************************************
    * @Name         : testFetchContractingEntitiesAddresses
    * @Description  : Test method for fetchContractingEntitiesAddresses method
    * @Created By   : Alina Craciunel
    * @Created Date : May 12, 2020
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testFetchContractingEntitiesAddresses() {
        Account acc = [SELECT Id FROM Account WHERE Name = :osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Username =: osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionResult res;
        system.runAs(u1) {
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
            ccrz.cc_CallContext.currAccountId = acc.Id;
            ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
            Test.startTest();
            res = osf_ctrl_orderreview.fetchContractingEntitiesAddresses(ctx);
            Test.stopTest();
        }
        System.assert(res.success);
        Map<String, osf_contracting_entity__c> mapContractingEntitiesByCode = (Map<String, osf_contracting_entity__c>)((Map<String, Object>)res.data).get(osf_constant_strings.CONTRACTING_ENTITIES);
        System.assertEquals(1, mapContractingEntitiesByCode.size());
        System.assert(mapContractingEntitiesByCode.containsKey('USA'));
        System.assert(!mapContractingEntitiesByCode.containsKey('TR'));
    }

    /**********************************************************************************************
    * @Name         : testSaveContractingEntity
    * @Description  : Test method for saveContractingEntity method
    * @Created By   : Alina Craciunel
    * @Created Date : May 12, 2020
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testSaveContractingEntity() {
        Account acc = [SELECT Id FROM Account WHERE Name = :osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Username =: osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionResult res;
        osf_contracting_entity__c ce = [SELECT Id FROM osf_contracting_entity__c WHERE osf_active__c = true LIMIT 1];
        system.runAs(u1) {
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
            Test.startTest();
            res = osf_ctrl_orderreview.saveContractingEntity(ctx, String.valueOf(ce.Id));
            Test.stopTest();
        }
        System.assert(res.success);
        ccrz__E_Cart__c cart = [SELECT Id, osf_contracting_entity__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
        System.assertEquals(ce.Id, cart.osf_contracting_entity__c);
    }

        /**********************************************************************************************
    * @Name         : testCheckCreditLimit_limitExceeded
    * @Description  : Test method for checkCreditLimit method, the credit limit is exceeded
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 29, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCheckCreditLimit_limitExceeded() {
        osf_testHelper.setupStorefront();
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        osf_CreditLimit__c creditlimit = osf_testHelper.createCreditLimit(CREDIT_LIMIT_AMOUNT1, acc);
        Database.insert(creditlimit);
        Boolean validCreditLimit;
        System.runAs(u1) {
            ccrz__E_Cart__c cart1 = [SELECT Id, ccrz__EncryptedId__c, ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :u1.Id LIMIT 1];
            ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart1);
            ccrz.cc_CallContext.currPageParameters.put('currCartId', cart1.ccrz__EncryptedId__c);
            Test.startTest();
            validCreditLimit = osf_ctrl_orderreview.checkCreditLimit(acc.Id, 'USD', cart1.ccrz__EncryptedId__c);
            Test.stopTest();
        }
        System.assertEquals(false, validCreditLimit);
    }

    /**********************************************************************************************
    * @Name         : testCheckCreditLimit
    * @Description  : Test method for checkCreditLimit method
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 29, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCheckCreditLimit() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart1 = [SELECT Id, ccrz__EncryptedId__c, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart1);
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart1.ccrz__EncryptedId__c);
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        osf_CreditLimit__c creditlimit = osf_testHelper.createCreditLimit(CREDIT_LIMIT_AMOUNT2, acc);
        Database.insert(creditlimit);
        Boolean validCreditLimit;
        system.runAs(u1) {
            Test.startTest();
            validCreditLimit = osf_ctrl_orderreview.checkCreditLimit(acc.Id, 'USD', cart1.ccrz__EncryptedId__c);
            Test.stopTest();
        } 
        System.assertEquals(true, validCreditLimit);
    }

    /**********************************************************************************************
    * @Name         : testCheckCreditLimit_noCreditLimit
    * @Description  : Test method for checkCreditLimit method, no credit limit defined
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 29, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCheckCreditLimit_noCreditLimit() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart1 = [SELECT Id, ccrz__EncryptedId__c, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart1);
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart1.ccrz__EncryptedId__c);
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        Boolean validCreditLimit;
        system.runAs(u1) {
            Test.startTest();
            validCreditLimit = osf_ctrl_orderreview.checkCreditLimit(acc.Id, 'USD', cart1.ccrz__EncryptedId__c);
            Test.stopTest();
        } 
        System.assertEquals(true, validCreditLimit);
    }

    /**********************************************************************************************
    * @Name         : testCheckCreditLimit_nullAmount
    * @Description  : Test method for checkCreditLimit method, null Amount
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 29, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testCheckCreditLimit_nullAmount() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart1 = [SELECT Id, ccrz__EncryptedId__c, ccrz__TotalAmount__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = :osf_testHelper.USER_EMAIL1 LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart1);
        ccrz.cc_CallContext.currPageParameters.put('currCartId', cart1.ccrz__EncryptedId__c);
        Account acc = [SELECT Id FROM Account WHERE Name =: osf_testHelper.ACCOUNT_NAME1 LIMIT 1];
        osf_CreditLimit__c creditlimit = osf_testHelper.createCreditLimit(null, acc);
        Database.insert(creditlimit);
        Boolean validCreditLimit;
        system.runAs(u1) {
            Test.startTest();
            validCreditLimit = osf_ctrl_orderreview.checkCreditLimit(acc.Id, 'USD', cart1.ccrz__EncryptedId__c);
            Test.stopTest();
        } 
        System.assertEquals(true, validCreditLimit);
    }
}