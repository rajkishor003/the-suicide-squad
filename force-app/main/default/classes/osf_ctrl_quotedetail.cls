/**
* Controller class for osf_my_quote_info page
* 
* @author Ozgun Eser
* @version 1.0
*/

global without sharing class osf_ctrl_quotedetail {
    
    /* Get Request For Quote By Id in the URL parameter.
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getRequestForQuote(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        try {
            osf_request_for_quote__c requestForQuote = getCurrentRequestForQuote();
            Map<String, Object> data = transformReqeustForQuoteData(requestForQuote); 
            if(String.isNotBlank(requestForQuote.osf_cart__c)) {
                Map<String, Object> outputData = fetchCartData(requestForQuote.osf_cart__c);
                Map<String, Object> productData = fetchMediaIdsFromProducts((List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST));    
                Map<String, Object> cartMap = transformCartData((List<Map<String, Object>>) outputData.get(ccrz.ccAPICart.CART_OBJLIST), productData);         
                data.put(osf_constant_strings.QUOTE_CART, cartMap);
            }
            if(String.isNotBlank(requestForQuote.osf_quote__c) && requestForQuote.osf_status__c == osf_constant_strings.QUOTE_STATUS_AVAILABLE) {
                data.put(osf_constant_strings.ATTACHMENT_ID, getQuoteAttachmentId(requestForQuote.osf_quote__c));
            }
            result.data = data;
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:quotedetail:getRequestForQuote:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /* Convert Request For Quote To Cart
	*
	* @param ctx, remote action context
	* @return result, Remote Action Result
    *
	*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult convertToCart(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        try {
            Map<String, String> queryParams = ccrz.cc_CallContext.currPageParameters;
            String quoteId = queryParams.get(osf_constant_strings.REQUEST_FOR_QUOTE_ID);
            osf_request_for_quote__c requestForQuote = [SELECT osf_cart__c, Name, CurrencyIsoCode FROM osf_request_for_quote__c WHERE Id = :quoteId LIMIT 1];
            Map<String, Object> outputData = ccrz.ccAPICart.cloneCart(new Map<String, Object> {
                ccrz.ccAPICart.CART_ID => requestForQuote.osf_cart__c,
                ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
            });
            if(!(Boolean) outputData.get(ccrz.ccAPI.SUCCESS)) {
                ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:quotedetail:outputData', outputData);
                throw new QuoteDetailException('Cart clone is failed');
            }

            String newCartId = (String) outputData.get(ccrz.ccAPICart.CART_ID);
            Map<Id, ccrz__E_Cart__c> cartMap = new Map<Id, ccrz__E_Cart__c> ([SELECT Id, Name, ccrz__Name__c, ccrz__ShipDiscountAmount__c, ccrz__AdjustmentAmount__c, ccrz__TaxAmount__c, ccrz__TotalDiscount__c, osf_converted_from__c, (SELECT Id, ccrz__AdjustmentAmount__c, ccrz__SubAmount__c, ccrz__OriginalItemPrice__c, ccrz__Price__c, ccrz__AbsoluteDiscount__c, ccrz__PercentDiscount__c, ccrz__RecurringPrice__c, ccrz__RecurringPriceSubAmt__c, ccrz__Product__c FROM ccrz__E_CartItems__r), (SELECT Id, ccrz__Coupon__r.ccrz__CouponCode__c FROM ccrz__E_CartCoupons__r) FROM ccrz__E_Cart__c WHERE Id = :requestForQuote.osf_cart__c OR Id = :newCartId]);
            ccrz__E_Cart__c cart = cartMap.get(requestForQuote.osf_cart__c);
            ccrz__E_Cart__c newCart = cartMap.get(newCartId);

            copyAdjustments(newCart, cart);
            if(!cart.ccrz__E_CartCoupons__r.isEmpty()) {
                copyCoupons(cart.ccrz__E_CartCoupons__r[0], newCartId);
            }
            newCart.ccrz__Name__c = osf_constant_strings.CONVERTED_FROM + requestForQuote.Name;
            newCart.osf_converted_from__c = quoteId;
            update newCart;            

            String cartId = (String) ccrz.cc_CallContext.currCartId;
            if(String.isNotBlank(cartId)) {
                List<ccrz__E_Cart__c> cartList = [SELECT Id, ccrz__ActiveCart__c, ccrz__CartStatus__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: cartId];
                if(!cartList.isEmpty()) {
                    cartList[0].ccrz__ActiveCart__c = false;
                    update cartList[0];
                }
            }
            data.put(osf_constant_strings.CART_ENCRYPTED_ID, (String) outputData.get(ccrz.ccAPICart.CART_ENCID));
            if (requestForQuote.CurrencyIsoCode != ccrz.cc_CallContext.userCurrency) {
                changeCurrency(requestForQuote.CurrencyIsoCode, ccrz.cc_CallContext.currUserId);
            }
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:quotedetail:convertToCart:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
            result.success = false;
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    /**********************************************************************************************
    * @Name         : copyAdjustments
    * @Description  : Copy Adjustments from Original Cart to Converted Cart
    * @Created By   : Ozgun Eser
    * @Created Date : Dec 18, 2019
    * @param        : ccrz__E_Cart__c newCart, converted cart
    * @param        : ccrz__E_Cart__c cart, original cart
    * @Return       : 
    *********************************************************************************************/
    private static void copyAdjustments(ccrz__E_Cart__c newCart, ccrz__E_Cart__c cart) {
        Map<String, ccrz__E_CartItem__c> productIdToCartItemMap = new Map<String, ccrz__E_CartItem__c> ();
        for(ccrz__E_CartItem__c cartItem : cart.ccrz__E_CartItems__r) {
            productIdToCartItemMap.put(cartItem.ccrz__Product__c, cartItem);
        }
        List<ccrz__E_CartItem__c> newCartItemList = new List<ccrz__E_CartItem__c> ();
        newCart.ccrz__AdjustmentAmount__c = cart.ccrz__AdjustmentAmount__c;
        newCart.ccrz__TotalDiscount__c = cart.ccrz__TotalDiscount__c;
        newCart.ccrz__TaxAmount__c = cart.ccrz__TaxAmount__c;
        for(ccrz__E_CartItem__c newCartItem : newCart.ccrz__E_CartItems__r) {
            if(!productIdToCartItemMap.containsKey(newCartItem.ccrz__Product__c)) { continue; }
            ccrz__E_CartItem__c cartItem = productIdToCartItemMap.get(newCartItem.ccrz__Product__c);
            newCartItem.ccrz__AdjustmentAmount__c = cartItem.ccrz__AdjustmentAmount__c;
            newCartItem.ccrz__SubAmount__c = cartItem.ccrz__SubAmount__c;
            newCartItem.ccrz__OriginalItemPrice__c = cartItem.ccrz__OriginalItemPrice__c;
            newCartItem.ccrz__Price__c = cartItem.ccrz__Price__c;
            newCartItem.ccrz__RecurringPrice__c = cartItem.ccrz__RecurringPrice__c;
            newCartITem.ccrz__RecurringPriceSubAmt__c = cartItem.ccrz__RecurringPriceSubAmt__c;
            newCartItemList.add(newCartItem);
        }
        update newCart;
        update newCartItemList;
    }

    /**********************************************************************************************
    * @Name         : copyCoupons
    * @Description  : Copy Coupons from Original Cart to Converted Cart
    * @Created By   : Ozgun Eser
    * @Created Date : Dec 18, 2019
    * @param        : ccrz__E_CartCoupon__c cartCoupon, cart coupon of the original cart
    * @param        : String newCartId, Id of the converted cart
    * @Return       : 
    *********************************************************************************************/
    private static void copyCoupons(ccrz__E_CartCoupon__c cartCoupon, String newCartId) {
        Map<String, Object> outputData = ccrz.ccAPICoupon.apply(new Map<String, Object> {
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPICart.CART_ID => newCartId,
            ccrz.ccAPICoupon.CODE => cartCoupon.ccrz__Coupon__r.ccrz__CouponCode__c
        });
    }

    /**********************************************************************************************
    * @Name         : getCurrentRequestForQuote
    * @Description  : Get Request For Quote By The Id From Page URL and Current User's Currency
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @Return       : osf_request_for_quote__c requestForQuote
    *********************************************************************************************/
    private static osf_request_for_quote__c getCurrentRequestForQuote () {
        osf_request_for_quote__c requestForQuote;
        List<osf_request_for_quote__c> requestForQuoteList = [SELECT Id, Name, CreatedDate, osf_cart__c, osf_status__c, osf_quote__c, osf_cart__r.ccrz__TotalAmount__c, CurrencyIsoCode FROM osf_request_for_quote__c WHERE Id = :ccrz.cc_CallContext.currPageParameters.get(osf_constant_strings.REQUEST_FOR_QUOTE_ID)  LIMIT 1];
            if(!requestForQuoteList.isEmpty()) {
                requestForQuote = requestForQuoteList[0];
            }
        return requestForQuote;
    }

    /**********************************************************************************************
    * @Name         : transformReqeustForQuoteData
    * @Description  : Transform Request For Quote into a Map<String, Object>
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @param        : osf_request_for_quote__c requestForQuote
    * @Return       : Map<String, Object> transformed data
    *********************************************************************************************/
    private static Map<String, Object> transformReqeustForQuoteData(osf_request_for_quote__c requestForQuote) {
        return new Map<String, Object> {
            osf_constant_strings.SFDC_NAME => requestForQuote.Name,
            osf_constant_strings.STATUS => requestForQuote.osf_status__c,
            osf_constant_strings.REQUEST_DATE => String.valueOf(requestForQuote.CreatedDate.Date()),
            osf_constant_strings.SF_ID => requestForQuote.Id,
            osf_constant_strings.QUOTE_AMOUNT => requestForQuote.osf_cart__r.ccrz__TotalAmount__c,
            osf_constant_strings.RFQ_CURRENCY => requestForQuote.CurrencyIsoCode
        };
    }

    /**********************************************************************************************
    * @Name         : fetchCartData
    * @Description  : Fetch cart data by Id taken by request for quote
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @param        : String cartId
    * @Return       : Map<String, Object> cart output data
    *********************************************************************************************/
    private static Map<String, Object> fetchCartData(String cartId) {
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                ccrz.ccAPICart.ENTITYNAME => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                },
                ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                }
            },
            ccrz.ccAPICart.CART_IDLIST => new Set<String> {cartId},
            ccrz.ccAPICart.BYSTOREFRONT => ccrz.cc_CallContext.storefront
        };
        return ccrz.ccAPICart.fetch(inputData);
    }

    /**********************************************************************************************
    * @Name         : fetchMediaIdsFromProducts
    * @Description  : Fetch Media Ids From Products in Fetched Cart
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @param        : List<Map<String, Object>> productMapList, productMapList taken by outputData of fetchCartData method
    * @Return       : Map<String, Object> productData
    *********************************************************************************************/
    private static Map<String, Object> fetchMediaIdsFromProducts(List<Map<String, Object>> productMapList) {
        Map<String, Object> productIdToMediaMap = new Map<String, Object> ();
        Set<String> mediaIdSet = new Set<String> ();
        Map<String, String> productIdToNameMap = new Map<String, String> ();
        for(Map<String, Object> productMap : productMapList) {
            productIdToNameMap.put((String) productMap.get(osf_constant_strings.SF_ID), (String) productMap.get(osf_constant_strings.SFDC_NAME));
            if(productMap.containsKey(osf_constant_strings.PRODUCT_MEDIAS_REL)) {
            List<Map<String, Object>> mediaMap = (List<Map<String, Object>>) productMap.get(osf_constant_strings.PRODUCT_MEDIAS_REL);
                if(mediaMap != null && !mediaMap.isEmpty()) {
                    for(Map<String, Object> media : mediaMap) {
                        if((String) media.get(osf_constant_strings.MEDIA_TYPE) == osf_constant_strings.MEDIA_TYPE_THUBMNAIL) {
                            productIdToMediaMap.put((String) productMap.get(osf_constant_strings.SF_ID), media);
                            mediaIdSet.add((String) media.get(osf_constant_strings.SF_ID));
                            break;
                        }
                    }
                }
            }
        }
        return new Map<String, Object> {
            osf_constant_strings.PRODUCT_ID_TO_MEDIA_MAP => productIdToMediaMap,
            osf_constant_strings.MEDIA_ID_SET => mediaIdSet,
            osf_constant_strings.PRODUCT_ID_TO_NAME_MAP => productIdToNameMap
        };
    }

    /**********************************************************************************************
    * @Name         : fetchAttachmentData
    * @Description  : Fetch Attachment By CC Product Media Ids
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @param        : Set<String> mediaIdSet, Id Set of CC Product Media 
    * @Return       : Map<String, Object> mediaIdToAttachmentMap
    *********************************************************************************************/
    private static Map<String, Object> fetchAttachmentData(Set<String> mediaIdSet) {
        Map<String, Object> mediaIdToAttachmentMap = new Map<String, Object> ();
        for(Attachment attachment : [SELECT Id, ParentId, Name FROM Attachment WHERE ParentId IN :mediaIdSet]) {
            Map<String, Object> attachmentMap = new Map<String, Object> {
                osf_constant_strings.PARENT_ID => attachment.ParentId,
                osf_constant_strings.SFDC_NAME => attachment.Name,
                osf_constant_strings.SF_ID => attachment.Id 
            };
            List<Map<String, Object>> attachmentMapList = new List<Map<String, Object>> {attachmentMap};
            mediaIdToAttachmentMap.put(attachment.ParentId, attachmentMapList);
        }
        return mediaIdToAttachmentMap;
    }

    /**********************************************************************************************
    * @Name         : transformCartData
    * @Description  : Transform Cart Data To Map<String, Object> for backbone model
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @param        : Map<String, Object> cartMapList fetched Cart Map
    * @param        : Map<String, Object> productData
    * @Return       : Map<String, Object> cartMap, cart map that is added media data.
    *********************************************************************************************/
    private static Map<String, Object> transformCartData (List<Map<String, Object>> cartMapList, Map<String, Object> productData) {
        Map<String, Object> cartMap = cartMapList[0];
        List<Map<String, Object>> cartItemMapList = (List<Map<String, Object>>) cartMap.get(osf_constant_strings.CART_ITEMS_REL);
        Map<String, Object> mediaIdToAttachmentMap = fetchAttachmentData((Set<String>) productData.get(osf_constant_strings.MEDIA_ID_SET));
        Map<String, Object> productIdToMediaMap = (Map<String, Object>) productData.get(osf_constant_strings.PRODUCT_ID_TO_MEDIA_MAP);
        Map<String, String> productIdToNameMap = (Map<String, String>) productData.get(osf_constant_strings.PRODUCT_ID_TO_NAME_MAP);
        for(Map<String, Object> cartItemMap : cartItemMapList) {
            Map<String, Object> productMap = (Map<String, Object>) cartItemMap.get(osf_constant_strings.CART_ITEM_PRODUCT);
            String productId = (String) productMap.get(osf_constant_strings.SF_ID);
            productMap.put(osf_constant_strings.SFDC_NAME, productIdToNameMap.get(productId));
            if(productIdToMediaMap.containsKey(productId)) {
                Map<String, Object> mediaMap = (Map<String, Object>) productIdToMediaMap.get(productId);
                String mediaId = (String) mediaMap.get(osf_constant_strings.SF_ID);
                if(mediaIdToAttachmentMap.containsKey(mediaId)) {
                    List<Map<String, Object>> attMapList = (List<Map<String, Object>>) mediaIdToAttachmentMap.get(mediaId);
                    Map<String, Object> attMap = attMapList[0];
                    String attId = (String) attMap.get(osf_constant_strings.SF_ID);
                    mediaMap.put(osf_constant_strings.MEDIA_URI, attId);
                    mediaMap.put(osf_constant_strings.MEDIA_ATTACMENTS, attMapList);
                }
                productMap.put(osf_constant_strings.PRODUCT_MEDIAS_REL, mediaMap);
            }
        }
        return cartMap;
    }

    /**********************************************************************************************
    * @Name         : getQuoteAttachmentId
    * @Description  : Get Attachment Id From Quote Id
    * @Created By   : Ozgun Eser
    * @Created Date : Jan 13, 2020
    * @param        : String quoteId
    * @Return       : String contentVersionId
    *********************************************************************************************/
    private static String getQuoteAttachmentId(String quoteId) {
        String contentVersionId = osf_constant_strings.EMPTY_STRING;
        List<ContentDocumentLink> contentDocumentLinkList = [SELECT Id, ContentDocumentId, ContentDocument.createddate, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :quoteId ORDER BY ContentDocument.createddate desc];
        if(!contentDocumentLinkList.isEmpty()) {
            List<ContentVersion> contentVersionList = [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :contentDocumentLinkList[0].ContentDocumentId];
            if(!contentVersionList.isEmpty()) {
                contentVersionId = contentVersionList[0].Id;
            }
        }
        return contentVersionId;
    }

    /**********************************************************************************************
    * @Name         : changeCurrency
    * @Description  : change currency of the user according to clicked request for quote
    * @Created By   : Ozgun Eser
    * @Created Date : May 18, 2020
    * @param        : String quoteId
    * @param        : String userId
    * @Return       : 
    *********************************************************************************************/
    @TestVisible
	private static void changeCurrency(String quoteCurrency, String userId) {
        User user = [SELECT Id, ccrz__CC_CurrencyCode__c FROM User WHERE Id = :userId];
        user.ccrz__CC_CurrencyCode__c = quoteCurrency;
        update user;
    }

    public class QuoteDetailException extends Exception {}
}