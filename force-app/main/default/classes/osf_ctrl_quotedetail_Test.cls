@isTest
public class osf_ctrl_quotedetail_Test {
    @TestSetup
    public static void createTestData() {
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        user.ccrz__CC_CurrencyCode__c = 'USD';
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        insert product;

        ccrz__E_ProductMedia__c productMedia = osf_testHelper.createProductMedia(product, 'en_US', osf_constant_strings.MEDIA_TYPE_THUBMNAIL);
        insert productMedia;

        Attachment attachment = osf_testHelper.createAttachment(productMedia.Id, 'Test Media', 'Test Media Image');
        insert attachment;

        ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
        insert cartItem;

        osf_quote__c quote = osf_testHelper.createQuote(200, account);
        insert quote;

        ContentVersion contentVersion = osf_testHelper.createContentVersion('Test Document', 'TestDocument.pdf', 'Test Document');
        insert contentVersion;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink contentDocumentLink = osf_testHelper.createContentDocumentLink(quote.Id, documents[0].Id);
        insert contentDocumentLink;

        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Available');
        requestForQuote.osf_quote__c = quote.Id;
        requestForQuote.CurrencyISOCode = 'USD';
        insert requestForQuote;
    }

    @isTest
    public static void testGetRequestForQuoteCatchBlock() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_quotedetail.getRequestForQuote(ctx);
        Test.stopTest();
        System.assert(!result.success);
    }

    @isTest
    public static void testConvertToCartCatchBlock() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_quotedetail.convertToCart(ctx);
        Test.stopTest();
        System.assert(!result.success);
    }

    @isTest
    public static void testGetRequestForQuote() {
        osf_testHelper.setupStorefront();
        osf_request_for_quote__c requestForQuote = [SELECT Id FROM osf_request_for_quote__c];
        ccrz.cc_CallContext.currPageParameters.put(osf_constant_strings.REQUEST_FOR_QUOTE_ID, requestForQuote.Id);
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_quotedetail.getRequestForQuote(ctx);
        Test.stopTest();
        System.assert(result.success);
    }

    @isTest
    public static void testConvertToCart() {
        osf_testHelper.setupStorefront();
        osf_request_for_quote__c requestForQuote = [SELECT Id, Name FROM osf_request_for_quote__c];
        ccrz.cc_CallContext.currPageParameters.put(osf_constant_strings.REQUEST_FOR_QUOTE_ID, requestForQuote.Id);
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_ctrl_quotedetail.convertToCart(ctx);
        Test.stopTest();
        System.assert(result.success);
        String cartName = osf_constant_strings.CONVERTED_FROM + requestForQuote.Name;
        List<ccrz__E_Cart__c> cartList = [SELECT Id FROM ccrz__E_Cart__c WHERE ccrz__Name__c = :cartName];
        System.assert(!cartList.isEmpty());
        System.assertEquals(1, cartList.size());

        cart = [SELECT ccrz__ActiveCart__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id AND ccrz__Name__c != :cartName];
        System.assert(!cart.ccrz__ActiveCart__c);
    }

    @IsTest
    public static void testChangeCurrency() {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__Account__c, ccrz__Contact__c, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        osf_request_for_quote__c requestForQuote = [SELECT Id, Name FROM osf_request_for_quote__c];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        System.runAs(user) {
            Test.startTest();
            osf_ctrl_quotedetail.changeCurrency('JPY', user.Id);
            Test.stopTest();       
        }
        user = [SELECT ccrz__CC_CurrencyCode__c FROM User WHERE Username = 'test@email.com'];
        System.assertEquals('JPY', user.ccrz__CC_CurrencyCode__c);
    }
}