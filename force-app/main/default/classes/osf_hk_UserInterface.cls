global with sharing class osf_hk_UserInterface extends ccrz.cc_hk_UserInterface.v004 {
    
    public static final String OSF_THEME_RESOURCE = 'OSF_CCThemeCapricorn';
    public static final String FAVICON_PATH = 'images/favicon';

    public static final String ADVANTEST_JS_PATH = 'js/advantest.js';
    
    global virtual override Map<String,Object> endContent(Map<String,Object> inpData) {
        
        Map<String,Object> outPut = super.endContent(inpData);
        String pageKey = ccrz.cc_CallContext.currPageName;
        ccrz.cclog.log('OSF:endContentJS:pageKey',pageKey);
        String endContents= '';
        try{
            endContents += '<script type="text/javascript" src="' + resourcePath( OSF_THEME_RESOURCE, ADVANTEST_JS_PATH) + '"></script>\n';    
            String retContent = (String)outPut.get(EOB_CONTENT);
            retContent +=  endContents;
            outPut.put(EOB_CONTENT, retContent);
            return outPut;
        }catch(Exception ex){
            return outPut;
        }      
    }

    global virtual override String metaContent(){
        String link = '';
        try{
            link = super.metaContent();
            link += '<link rel="icon" type="image/png" href="' + resourcePath( OSF_THEME_RESOURCE, FAVICON_PATH) + '/thumb.png" />';
            link += '<title>myAdvantest Shop</title>';
            return link;
        }catch(Exception ex){
            ccrz.cclog.log('exceptie: - ' + ex);
            return link;
        }
    } 
}