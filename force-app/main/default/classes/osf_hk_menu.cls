/**
 * File:        osf_hk_menu.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jun 04, 2020
 * Created By:  Adrian Hildebrandt
  ************************************************************************
 * Description: Extension of cc_hk_menu to hide menu items that belong to
 * 				categories which should not be visible.
  ************************************************************************
 */

global with sharing class osf_hk_menu extends ccrz.cc_hk_menu {
    
    global static String ITEM_TYPE_CATEGORY = 'Category';
    
    /**********************************************************************************************
    * @Name                 : fetch
    * @Description          : fetch method of the cc_hk_menu class. Executes other methods to
    * 						extract the necessary menu items of the result.
    * @Created By           : Adrian Hildebrandt
    * @Created Date         : Jun 04, 2020
    * @param                : Map<String, Object> inputData
    * @Return               : Map<String, Object>
    *********************************************************************************************/
    global virtual override Map<String,Object> fetch(Map<String,Object> inputData) {
		Map<String,Object> returnValue = super.fetch(inputData);
        List<ccrz.cc_bean_MenuItem> menuTree = (List<ccrz.cc_bean_MenuItem>)returnValue.get(ccrz.cc_hk_menu.PARAM_MENU);

        if (menuTree != null) {
            Set<Id> categoryIds = this.fetchVisibleCategoryIds(ccrz.cc_CallContext.userCurrency, ccrz.cc_CallContext.storefront, ccrz.cc_CallContext.currAccountGroup.Id);
            List<ccrz.cc_bean_MenuItem> newMenuTree = this.removeItemsForHiddenCategories(menuTree, categoryIds);
            returnValue.put(ccrz.cc_hk_menu.PARAM_MENU, newMenuTree);
        }
        
        return returnValue;
    }
    
    /**********************************************************************************************
    * @Name                 : fetchVisibleCategoryIds
    * @Description          : fetches AccountGroupCategory records to identify which categories are
    * 						visible to the user.
    * @Created By           : Adrian Hildebrandt
    * @Created Date         : Jun 04, 2020
    * @param                : String userCurrency
    * @param				: String storefront
    * @param				: String accountGroupId
    * @Return               : Set<Id>					Set of CC Category Ids
    *********************************************************************************************/
    @testVisible
    private Set<Id> fetchVisibleCategoryIds(String userCurrency, String storefront, String accountGroupId) {
		List<osf_account_group_category__c> agcs = [SELECT osf_category__c FROM osf_account_group_category__c WHERE osf_currency__c = :userCurrency AND osf_storefront__c = :storefront AND osf_account_group__c = :accountGroupId];
        Set<Id> categoryIds = new Set<Id>();
        
        for (osf_account_group_category__c agc : agcs) {
            categoryIds.add(agc.osf_category__c);
        }
        
        return categoryIds;
    }
    
    /**********************************************************************************************
    * @Name                 : removeItemsForHiddenCategories
    * @Description          : builds and returns a new menu tree by removing all menu items that
    * 						belong to a category that is not in the categoryIds set. Calls itself
    * 						to also remove respective items from the children.
    * @Created By           : Adrian Hildebrandt
    * @Created Date         : Jun 04, 2020
    * @param                : List<ccrz.cc_bean_MenuItem> menuTree
    * @param				: Set<Id> categoryIds
    * @Return               : List<ccrz.cc_bean_MenuItem>			new menu item tree
    *********************************************************************************************/
    @testVisible
    private List<ccrz.cc_bean_MenuItem> removeItemsForHiddenCategories(List<ccrz.cc_bean_MenuItem> menuTree, Set<Id> categoryIds) {
        List<ccrz.cc_bean_MenuItem> newMenuTree = new List<ccrz.cc_bean_MenuItem>();
        
        for (ccrz.cc_bean_MenuItem menuTreeItem : menuTree) {
            String itemType = menuTreeItem.mType;
            String categoryId = menuTreeItem.linkURL;
            if (itemType != null && itemType == osf_hk_menu.ITEM_TYPE_CATEGORY && categoryId != null && !categoryIds.contains(categoryId)) {
                continue;
            }
            newMenuTree.add(menuTreeItem);
            
            List<ccrz.cc_bean_MenuItem> children = menuTreeItem.children;
            if (children != null) {
                menuTreeItem.children = this.removeItemsForHiddenCategories(children, categoryIds);
            }
        }
        
        return newMenuTree;
    }
}