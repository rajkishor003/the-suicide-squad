@isTest
private class osf_hk_menuTest {
    @isTest
    private static void testFetchVisibleCategoryIds() {
        // setup test data
        ccrz__E_AccountGroup__c accountGroup = new ccrz__E_AccountGroup__c();
        insert accountGroup;
        
        ccrz__E_Category__c category = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        insert category;
        
        String userCurrency = 'USD';
        String storefront = 'DefaultStore';
        
        osf_account_group_category__c agc = new osf_account_group_category__c(
            osf_account_group__c = accountGroup.Id,
        	osf_category__c = category.Id,
            osf_currency__c = userCurrency,
            osf_storefront__c = storefront
        );
        insert agc;
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute unit
        Test.startTest();
        Set<Id> categoryIds = menuHook.fetchVisibleCategoryIds(userCurrency, storefront, accountGroup.Id);
        Test.stopTest();
        
		// assertations
		System.assertEquals(1, categoryIds.size());
        System.assert(categoryIds.contains(category.Id));
    }
    
    @isTest
    private static void testFetchVisibleCategoryIds_hidden() {
        // setup test data
        ccrz__E_AccountGroup__c accountGroup = new ccrz__E_AccountGroup__c();
        insert accountGroup;
        
        ccrz__E_Category__c category = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        insert category;
        
        String userCurrency = 'USD';
        String storefront = 'DefaultStore';
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        Set<Id> categoryIds = menuHook.fetchVisibleCategoryIds(userCurrency, storefront, accountGroup.Id);
        Test.stopTest();
        
		// assertations
		System.assertEquals(0, categoryIds.size());
    }
    
    @isTest
    private static void testRemoveItemsForHiddenCategories_visibleCat() {
        // setup test data
        ccrz__E_Category__c category = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        insert category;
        
        List<ccrz.cc_bean_MenuItem> menuTree = new List<ccrz.cc_bean_MenuItem>{
            new ccrz.cc_bean_MenuItem('sfid', 'displayName', category.Id, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0)
        };
        Set<Id> categoryIds = new Set<Id>{
            category.Id
        };
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        List<ccrz.cc_bean_MenuItem> newMenuTree = menuHook.removeItemsForHiddenCategories(menuTree, categoryIds);
        Test.stopTest();
        
        // assertations
        System.assertEquals(1, newMenuTree.size());
        System.assertEquals(menuTree.get(0), newMenuTree.get(0));
    }
    
    @isTest
    private static void testRemoveItemsForHiddenCategories_hiddenCat() {
        // setup test data
        ccrz__E_Category__c category = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        insert category;
        
        List<ccrz.cc_bean_MenuItem> menuTree = new List<ccrz.cc_bean_MenuItem>{
            new ccrz.cc_bean_MenuItem('sfid', 'displayName', category.Id, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0)
        };
        Set<Id> categoryIds = new Set<Id>{};
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        List<ccrz.cc_bean_MenuItem> newMenuTree = menuHook.removeItemsForHiddenCategories(menuTree, categoryIds);
        Test.stopTest();
        
        // assertations
        System.assertEquals(0, newMenuTree.size());
    }
    
    @isTest
    private static void testRemoveItemsForHiddenCategories_catWithoutId() {
        // setup test data
        List<ccrz.cc_bean_MenuItem> menuTree = new List<ccrz.cc_bean_MenuItem>{
            new ccrz.cc_bean_MenuItem('sfid', 'displayName', null, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0)
        };
        Set<Id> categoryIds = new Set<Id>{};
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        List<ccrz.cc_bean_MenuItem> newMenuTree = menuHook.removeItemsForHiddenCategories(menuTree, categoryIds);
        Test.stopTest();
        
        // assertations
        System.assertEquals(1, newMenuTree.size());
        System.assertEquals(menuTree.get(0), newMenuTree.get(0));
    }
    
    private static void testRemoveItemsForHiddenCategories_itemForDifferentType() {
        // setup test data
        ccrz__E_Category__c category = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        insert category;
        
        List<ccrz.cc_bean_MenuItem> menuTree = new List<ccrz.cc_bean_MenuItem>{
            new ccrz.cc_bean_MenuItem('sfid', 'displayName', category.Id, false, 'OTHER TYPE', 0)
        };
        Set<Id> categoryIds = new Set<Id>{};
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        List<ccrz.cc_bean_MenuItem> newMenuTree = menuHook.removeItemsForHiddenCategories(menuTree, categoryIds);
        Test.stopTest();
        
        // assertations
        System.assertEquals(1, newMenuTree.size());
        System.assertEquals(menuTree.get(0), newMenuTree.get(0));
    }
    
    @isTest
    private static void testRemoveItemsForHiddenCategories_visibleChildren() {
        // setup test data
        ccrz__E_Category__c cat1 = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        ccrz__E_Category__c cat2 = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat2'
        );
        insert new List<ccrz__E_Category__c>{cat1, cat2};
        
        ccrz.cc_bean_MenuItem parentItem = new ccrz.cc_bean_MenuItem('sfid', 'displayName', cat1.Id, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0);
        parentItem.children = new List<ccrz.cc_bean_MenuItem>{
            new ccrz.cc_bean_MenuItem('sfid', 'displayName', cat2.Id, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0)
        };
        
        List<ccrz.cc_bean_MenuItem> menuTree = new List<ccrz.cc_bean_MenuItem>{parentItem};
        Set<Id> categoryIds = new Set<Id>{cat1.Id, cat2.Id};
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        List<ccrz.cc_bean_MenuItem> newMenuTree = menuHook.removeItemsForHiddenCategories(menuTree, categoryIds);
        Test.stopTest();
        
        // assertations
        ccrz.cc_bean_MenuItem newParentItem = newMenuTree.get(0);
        System.assertEquals(parentItem.children, newParentItem.children);
    }
    
    @isTest
    private static void testRemoveItemsForHiddenCategories_hiddenChildren() {
        // setup test data
        ccrz__E_Category__c cat1 = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat1'
        );
        ccrz__E_Category__c cat2 = new ccrz__E_Category__c(
        	ccrz__CategoryId__c = 'cat2'
        );
        insert new List<ccrz__E_Category__c>{cat1, cat2};
        
        ccrz.cc_bean_MenuItem parentItem = new ccrz.cc_bean_MenuItem('sfid', 'displayName', cat1.Id, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0);
        parentItem.children = new List<ccrz.cc_bean_MenuItem>{
            new ccrz.cc_bean_MenuItem('sfid', 'displayName', cat2.Id, false, osf_hk_menu.ITEM_TYPE_CATEGORY, 0)
        };
        
        List<ccrz.cc_bean_MenuItem> menuTree = new List<ccrz.cc_bean_MenuItem>{parentItem};
        Set<Id> categoryIds = new Set<Id>{cat1.Id};
        
        osf_hk_menu menuHook = new osf_hk_menu();
        
        // execute test
        Test.startTest();
        List<ccrz.cc_bean_MenuItem> newMenuTree = menuHook.removeItemsForHiddenCategories(menuTree, categoryIds);
        Test.stopTest();
        
        // assertations
        ccrz.cc_bean_MenuItem newParentItem = newMenuTree.get(0);
        System.assertEquals(0, newParentItem.children.size());
    }
}