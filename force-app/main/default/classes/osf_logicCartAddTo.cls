/**
 * File:        osf_logicCartAddTo.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Add To Cart Logic Provider
  ************************************************************************
 * History:
 */

global with sharing class osf_logicCartAddTo extends ccrz.ccLogicCartAddTo {

    /**********************************************************************************************
    * @Name         : addCurrentLineDataToCart
    * @Description  : Changes skipHidePrices value of osf_logicProductPricing before adding it to cart to prevent it to be 0 in database Cart Object.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/
    global override Map<String, Object> addCurrentLineDataToCart(Map<String, Object> inputData) {
        osf_logicProductPricing.skipHidePrices = true;
        Map<String, Object> outputData = super.addCurrentLineDataToCart(inputData);
        osf_logicProductPricing.skipHidePrices = false;
        return outputData;
    }
}