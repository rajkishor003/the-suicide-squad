@IsTest
public class osf_logicCartAddTo_Test {
    
    @IsTest
    public static void testAddCurrentLineDataToCart() {
        osf_logicCartAddTo logic = new osf_logicCartAddTo();

        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        insert product;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;

        ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
        insert cartItem;

        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPICart.WORKING_LINE_DATAS => new List<Map<String, Object>> {new Map<String, Object> {
                ccrz.ccAPICart.LINE_DATA_QUANTITY => cartItem.ccrz__Quantity__c,
                ccrz.ccAPICart.LINE_DATA_PRODUCT_SFID => cartItem.ccrz__Product__c
            }},
            ccrz.ccAPICart.CARTITEMSBYID => new Map<String, Object> {
                cart.Id => new List<Map<String, Object>> {new Map<String, Object> {
                    'attributes' => new Map<String, Object> {
                        'type' => 'ccrz__E_CartItem__c',
                        'url' => '/services/data/v47.0/sobjects/ccrz__E_CartItem__c/' + cartItem.Id
                    },
                    'Id' => cartItem.Id,
                    'Name' => cartItem.Name,
                    'ccrz__Cart__c' => cartItem.ccrz__Cart__c,
                    'ccrz__Product__c' => cartItem.ccrz__Product__c,
                    'ccrz__Quantity__c' => cartItem.ccrz__Quantity__c,
                    'ccrz__Price__c' => cartItem.ccrz__Price__c,
                    'ccrz__ProductType__c' => 'Product',
                    'ccrz__StoreID__c' => osf_testHelper.STOREFRONT,
                    'ccrz__PricingType__c' => 'auto',
                    'ccrz__ItemTotal__c' => cartItem.ccrz__ItemTotal__c,
                    'ccrz__cartItemType__c' => 'Major',
                    'ccrz__SubAmount__c' => cartItem.ccrz__SubAmount__c,
                    'ccrz__UnitOfMeasure__c' => cartItem.ccrz__UnitOfMeasure__c,
                    'ccrz__ItemStatus__c' => 'Available',
                    'ccrz__Is_Subscription_Selected__c' => false,
                    'ccrz__OriginalQuantity__c' => cartItem.ccrz__Quantity__c,
                    'ccrz__OriginalItemPrice__c' => cartItem.ccrz__Price__c,
                    'ccrz__RequestDate__c' => String.valueOf(Date.today()),
                    'CurrencyISOCode' => 'USD',
                    'ccrz__Cart__r' => new Map<String, Object> {
                        'attributes' => new Map<String, Object> {
                            'type' => 'ccrz__E_Cart__c',
                            'url' => '/services/data/v47.0/sobjects/ccrz__E_Cart__c/' + cart.Id
                        },
                        'Id' => cart.Id,
                        'Name' => cart.Name
                    },
                    'ccrz__Product__r' => new Map<String, Object> {
                        'attributes' => new Map<String, Object> {
                            'type' => 'ccrz__E_Product__c',
                            'url' => '/services/data/v47.0/sobjects/ccrz__E_Product__c/' + product.Id
                        },
                        'ccrz__SKU__c' => product.ccrz__SKU__c,
                        'ccrz__ProductStatus__c' => product.ccrz__ProductStatus__c,
                        'ccrz__StartDate__c' => product.ccrz__StartDate__c,
                        'ccrz__EndDate__c' => product.ccrz__EndDate__c,
                        'ccrz__ProductType__c' => product.ccrz__ProductType__c,
                        'Id' => product.Id,
                        'CurrencyISOCode' => 'USD'
                    }
                }},
                'cartHeader' => new Map<String, Object> {
                    'attributes' => new Map<String, Object> {
                        'type' => 'ccrz__E_Cart__c',
                        'url' => '/services/data/v47.0/sobjects/ccrz__E_Cart__c/' + cart.Id
                    },
                    'Id' => cart.Id,
                    'OwnerId' => cart.OwnerId,
                    'Name' => cart.Name,
                    'ccrz__EncryptedId__c' => cart.ccrz__EncryptedId__c,
                    'ccrz__ActiveCart__c' => true,
                    'ccrz__ValidationStatus__c' => 'CartAuthUserValidated',
                    'ccrz__CartStatus__c' => 'Open',
                    'ccrz__CartType__c' => 'Cart',
                    'LastModifiedDate' => String.valueOf(cart.LastModifiedDate),
                    'ccrz__SubtotalAmount__c' => cart.ccrz__SubtotalAmount__c,
                    'ccrz__Name__c' => cart.ccrz__Name__c,
                    'ccrz__TaxAmount__c' => cart.ccrz__TaxAmount__c,
                    'ccrz__TaxExemptFlag__c' => false,
                    'ccrz__CurrencyISOCode__c' => 'USD',
                    'ccrz__TaxSubTotalAmount__c' => cart.ccrz__TaxAmount__c,
                    'ccrz__TotalQuantity__c' => cartItem.ccrz__Quantity__c,
                    'ccrz__Account__c' => cart.ccrz__Account__c,
                    'ccrz__AnonymousId__c' => false,
                    'ccrz__BillTo__c' => cart.ccrz__BillTo__c,
                    'ccrz__BuyerEmail__c' => contact.Email,
                    'ccrz__BuyerFirstName__c' => contact.FirstName,
                    'ccrz__BuyerLastName__c' => contact.LastName,
                    'ccrz__BuyerPhone__c' => '+901111111111',
                    'ccrz__Contact__c' => cart.ccrz__Contact__c,
                    'ccrz__RepricedDate__c' => String.valueOf(Date.today()),
                    'ccrz__ShipComplete__c' => false,
                    'ccrz__ShipTo__c' => cart.ccrz__ShipTo__c,
                    'ccrz__Storefront__c' => osf_testHelper.STOREFRONT,
                    'ccrz__User__c' => cart.ccrz__User__c,
                    'CurrencyISOCode' => 'USD'
                },
                ccrz.ccAPICart.CART_ID => cart.Id,
                ccrz.ccAPIProduct.PRODUCTSELLERLIST => new List<Map<String, Object>>{},
                ccrz.ccAPICart.SELLERS_ENABLED => false,
                ccrz.ccAPICart.ADD_SEPARATE => false,
                ccrz.ccAPICart.ISREPRICE => false,
                ccrz.ccAPICart.ISSKIPPRICING => false,
                ccrz.ccAPICart.CARTTYPE => 'Cart',
                ccrz.ccAPICart.HAS_COUPON_CODE => false,
                ccrz.ccAPICart.HAS_LINE_DATA => true,
                ccrz.ccAPICart.HAS_ID => true,
                ccrz.ccAPICart.RESPONSE => new Map<String, Object> {
                    ccrz.ccAPICart.CART_ENCID => cart.ccrz__EncryptedId__c,
                    ccrz.ccAPICart.INCRERROR => new Map<String, Object> {},
                    ccrz.ccAPICart.INCRWARN => new Map<String, Object> {},
                    ccrz.ccAPICart.NONSTDPRODUCTS => new List<Map<String, Object>> {},
                    ccrz.ccAPICart.INELIGIBLEPRODUCTS => new List<Map<String, Object>> {},
                    ccrz.ccAPICart.PRODSADDEDTOCART => new Map<String, Object> {},
                    ccrz.ccAPI.SUCCESS => false
                },
                ccrz.ccAPICart.LINE_DATA => new List<Map<String, Object>> {new Map<String, Object> {
                    ccrz.ccAPICart.LINE_DATA_QUANTITY => cartItem.ccrz__Quantity__c,
                    ccrz.ccAPICart.LINE_DATA_PRODUCT_SFID => cartItem.ccrz__Product__c
                }},
                ccrz.ccAPICart.CART_ENCID => cart.ccrz__EncryptedId__c,
                ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
            }
        };


        Map<String, Object> outputData = logic.addCurrentLineDataToCart(inputData);
        System.assert(!outputData.isEmpty());
    }
}