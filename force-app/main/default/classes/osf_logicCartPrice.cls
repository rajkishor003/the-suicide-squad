/**
 * File:        osf_logicCartPrice.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Cart Pricing Logic Provider
  ************************************************************************
 * History:
 */

global with sharing class osf_logicCartPrice extends ccrz.ccLogicCartPrice {

    global override Map<String, Object> validateLastPriceDate(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.validateLastPriceDate(inputData);
        ccrz__E_Cart__c cart = (ccrz__E_Cart__c) outputData.get('cartToPrice');
        if(cart.osf_converted_from_rfq__c) {
            outputData.put(ccrz.ccApiCart.SKIP_CART_PRICING, true);
        }
        return outputData;
    }

    /**********************************************************************************************
    * @Name         : fetchPricingData
    * @Description  : Changes skipHidePrices value of osf_logicProductPricing before fetching Pricing Data to prevent it to be 0 in database Cart Object.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/
    global override Map<String, Object> fetchPricingData(Map<String, Object> inputData) {
        ccrz.ccLog.log(LoggingLevel.DEBUG, 'osf:logicCartPrice:fetchPricingData:inputData', inputData);
        osf_logicProductPricing.skipHidePrices = true;
        Map<String, Object> outputData = super.fetchPricingData(inputData);
        osf_logicProductPricing.skipHidePrices = false;
        return outputData;
    }

    /**********************************************************************************************
    * @Name         : prepareCartItems
    * @Description  : 
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/
    global override Map<String, Object> prepareCartItems(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepareCartItems(inputData);
        try {
            ccrz__E_Cart__c cart = (ccrz__E_Cart__c) outputData.get('cartToPrice');
            //if(!cart.ccrz__Name__c.startsWith(osf_constant_strings.CONVERTED_FROM)) return outputData;
            Set<String> cartItemIdSet = new Set<String> ();

            Map<Id, ccrz__E_CartItem__c> cartItemMap = new Map<Id, ccrz__E_CartItem__c> ([SELECT Id, ccrz__AdjustmentAmount__c, ccrz__cartItemType__c FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c = :cart.Id]);
            
            List<ccrz__E_CartItem__c> cartItemToPriceList = (List<ccrz__E_CartItem__c>) outputData.get('cartItemsToPrice');
            for(ccrz__E_CartItem__c cartItem : cartItemToPriceList) {
                if (cartItemMap.get(cartItem.Id).ccrz__cartItemType__c != osf_constant_strings.CART_ITEM_TYPE_COUPON) {
                    cartItem.ccrz__AdjustmentAmount__c = cartItemMap.get(cartItem.Id).ccrz__AdjustmentAmount__c;
                }
            }
            outputData.put('cartItemsToPrice', cartItemToPriceList);

            List<ccrz__E_CartItem__c> cartItemList = (List<ccrz__E_CartItem__c>) outputData.get(ccrz.ccAPICart.CILIST);
            for(ccrz__E_CartItem__c cartItem : cartItemList) {
                if (cartItemMap.get(cartItem.Id).ccrz__cartItemType__c != osf_constant_strings.CART_ITEM_TYPE_COUPON) {
                    cartItem.ccrz__AdjustmentAmount__c = cartItemMap.get(cartItem.Id).ccrz__AdjustmentAmount__c;
                }
            }

            outputData.put(ccrz.ccAPICart.CILIST, cartItemList);
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:logicCartPrice:prepareCartItems:Error', e);
        }
        return outputData;
    }
}