@IsTest
public class osf_logicCartPrice_Test {

    @TestSetup
    public static void createTestData() { 
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        insert product;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        cart.ccrz__Name__c = osf_constant_strings.CONVERTED_FROM + 'Test';
        insert cart;
        
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, osf_constant_strings.QUOTE_STATUS_QUOTE_REQUESTED);
        insert requestForQuote;

        ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
        cartItem.ccrz__AdjustmentAmount__c = -100;
        insert cartItem;

        cart.osf_converted_from__c = requestForQuote.Id;
        update cart;        
    }
    
    @IsTest
    public static void testFetchPricingData() {
        User user = [SELECT Id FROM User WHERE Username = 'test@email.com' LIMIT 1];
        ccrz__E_Product__c product = [SELECT Id FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'Test SKU' LIMIT 1];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c, ccrz__Name__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id LIMIT 1];
        ccrz__E_CartItem__c cartItem = [SELECT Id, ccrz__Price__c, ccrz__Product__c FROM ccrz__E_CartItem__c WHERE ccrz__Product__c = :product.Id AND ccrz__Cart__c = :cart.Id LIMIT 1];
        osf_logicCartPrice logic = new osf_logicCartPrice();
        Map<String, Object> inputData = new Map<String, Object>  {
            'pricingInputs' => new Map<String, Object> {
                'subProdTerms4PR' => new List<Map<String, Object>> {null},
                ccrz.ccAPIProduct.PARAM_INCLUDE_SELLERS => true,
                ccrz.ccAPIProduct.PRODUCTSELLERLIST => new List<Map<String, Object>> {null},
                ccrz.ccAPI.SIZING => new Map<String, Object> {
                    ccrz.ccAPIProduct.PRODUCT => new Map<String, Object> {
                        ccrz.ccAPI.SZ_REL => new List<Object> {'CompositeProducts__r'},
                        ccrz.ccAPI.SZ_ASSC => false,
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_S
                    }
                },
                ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
                ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Object> {product.Id},
                ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
            },
            'cartItemsToDelete' => new List<Object> {},
            'majorsWExtChild' => new List<Object> {},
            'externalCartItems' => new List<Object> {},
            'cartItemsToPrice' => new List<ccrz__E_CartItem__c> {cartItem},
            ccrz.ccAPICart.SKIP_CART_PRICING => false,
            ccrz.ccAPICart.CILIST => new List<ccrz__E_CartItem__c> {cartItem},
            'cartToPrice' => cart,
            ccrz.ccAPICart.CART_ID => cart.Id,
            'skpPreProc' => false,
            'cartItemsToUpdate' => null,
            ccrz.ccAPI.SUCCESS => false,
            ccrz.ccLogic.ORIGINAL_INPUT => new Map<String, Object> {
                ccrz.ccAPICart.FETCH_EXTERNAL_PRICING => false,
                ccrz.ccAPICart.CART_ENCID => cart.ccrz__EncryptedId__c,
                ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
            },
            ccrz.ccAPICart.FETCH_EXTERNAL_PRICING => false,
            ccrz.ccAPICart.CART_ENCID => cart.ccrz__EncryptedId__c,
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION         
        };
        Test.startTest();
        Map<String, Object> outputData = logic.fetchPricingData(inputData);
        Map<String, Object> cartItemData = logic.prepareCartItems(inputData);
        Test.stopTest();
        System.assert(!outputData.isEmpty());
        System.assert(!cartItemData.isEmpty());
    }

    @IsTest
    public static void testValidateLastPriceDate() {
        User user = [SELECT Id FROM User WHERE Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__c ccCart = [SELECT Id, osf_converted_from_rfq__c, ccrz__EncryptedId__c, ccrz__Name__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id LIMIT 1];
        osf_logicCartPrice logicCartPrice = new osf_logicCartPrice();
        Map<String, Object> inputData = new Map<String, Object> {
            'cartToPrice' => ccCart,
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPICart.SKIP_CART_PRICING => false
        };
        Test.startTest();
        Map<String, Object> outputData = logicCartPrice.validateLastPriceDate(inputData);
        Test.stopTest();
        System.assert(!outputData.isEmpty());
        System.assert((Boolean) outputData.get(ccrz.ccAPICart.SKIP_CART_PRICING));
    }
}