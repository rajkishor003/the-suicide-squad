/**
 * File:        osf_logicCategoryGetTree.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Mar 20, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: extension of ccLogicCategoryGetTree
  ************************************************************************
 * History:
 */

global with sharing class osf_logicCategoryGetTree extends ccrz.ccLogicCategoryGetTree {
    
    /**********************************************************************************************
    * @Name                 : process
    * @Description          : process method of the ccLogicCategoryGetTree class
    * @Created By           : Alina Craciunel
    * @Created Date         : Mar 20, 2020
    * @param                : Map<String, Object> inputData
    * @Return               : Map<String, Object>
    *********************************************************************************************/
    global virtual override Map<String, Object> process(Map<String, Object> inputData) {
        List<osf_category_tree_cache__c> caches = [SELECT osf_cache_data__c, osf_locale__c, osf_currency__c FROM osf_category_tree_cache__c 
            WHERE osf_storefront__c = :ccrz.cc_CallContext.storefront AND osf_account_group__c = :ccrz.cc_CallContext.currAccountGroup.Id AND osf_locale__c = :ccrz.cc_CallContext.userLocale AND osf_currency__c = :ccrz.cc_CallContext.userCurrency ORDER BY osf_locale__c ASC LIMIT 1 ];        
        //ccrz.ccLog.log(LoggingLevel.DEBUG,'@@','@@caches='+ caches);        
        // if no cache is found, fall back on the default full category tree    
        if(caches.isEmpty()) {          
            return super.process(inputData);        
        }        
        List<osf_bean_Category> theCategories = (List<osf_bean_Category>)JSON.deserialize(caches.get(0).osf_cache_data__c, List<osf_bean_Category>.class);        
        //ccrz.ccLog.log(LoggingLevel.DEBUG,'@@','@@theCategories='+ theCategories);
        Map<String, Object> ret = new Map<String,Object>{ccrz.ccAPICategory.CATEGORYTREE => theCategories};
        return ret;
    }
    
    public class osf_bean_Category {
        String sfid {get; set;}
        String name {get; set;}
        String friendlyUrl {get; set;}
        List<osf_bean_Category> children {get; set;}
        public osf_bean_Category(){}
    }

}