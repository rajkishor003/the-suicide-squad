/**
 * File:        osf_logicProductPricing.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Product Pricing Logic Provider
  ************************************************************************
 * History:
 */

global with sharing class osf_logicProductPricing extends ccrz.ccLogicProductPricing {

    public static Boolean skipHidePrices = false;
    private static Account currentAccount;
    

    /**********************************************************************************************
    * @Name         : showPrices
    * @Description  : Returning the Prices Visible Checkbox value of current Account unless skipHidePrices is true.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        :
    * @Return       : Boolean showPrices (if false prices will not be shown on storefront)
    *********************************************************************************************/
    public static Boolean showPrices() {
        if(ccrz.cc_CallContext.isGuest) {return false;}
        if(skipHidePrices) {
            return true;
        }
        Boolean isVisible = osf_cache.getPriceVisibility();
        if (isVisible != null) {
            return isVisible;
        }
        if(currentAccount == null) {
            List<Account> accountList = [SELECT osf_prices_visible__c FROM Account WHERE Id = :ccrz.cc_CallContext.currAccountId];
            if(accountList.isEmpty()) {
                osf_cache.setPriceVisibility(false);
                return false;
            }
            currentAccount = accountList[0];
        }
        osf_cache.setPriceVisibility(currentAccount.osf_prices_visible__c);
        return currentAccount.osf_prices_visible__c;
    }

    /**********************************************************************************************
    * @Name         : fetchPriceListsAndItems
    * @Description  : Return 0 for price if showPrices is false
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/
    global override Map<String, Object> fetchPriceListsAndItems(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.fetchPriceListsAndItems(inputData);
        try {
            Map<String, Object> priceListIdToItemsMap = (Map<String, Object>) outputData.get(ccrz.ccApiPriceList.PRICELISTITEMS);
            if(!showPrices()) {
                for(Map<String, Object> priceListItem : (List<Map<String, Object>>) priceListIdToItemsMap.values()) {
                    priceListItem.put(osf_constant_strings.PRICE, 0.0);
                }
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:logicProductPricing:fetchPriceListsAndItems:Error', e);
        }
        return outputData;
    }
}