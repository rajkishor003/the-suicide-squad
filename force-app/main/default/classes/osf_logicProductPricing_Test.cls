@IsTest
public class osf_logicProductPricing_Test {

    @IsTest
    public static void testProductPricingHidePrices() {
        SetupData setupData = new SetupData(false);
        Map<String, Object> inputData = setupData.testData;
        Map<String, Object> result = new Map<String, Object> {};
        osf_logicProductPricing logic = new osf_logicProductPricing();
        System.runAs(setupData.testUser) {
            Test.startTest();
            result = logic.fetchPriceListsAndItems(inputData);
            Test.stopTest();
        }
        System.assert(!result.isEmpty());
    }

    @IsTest
    public static void testProductPricingShowPrices() {
        SetupData setupData = new SetupData(true);
        Map<String, Object> inputData = setupData.testData;
        Map<String, Object> result = new Map<String, Object> {};
        osf_logicProductPricing logic = new osf_logicProductPricing();
        System.runAs(setupData.testUser) {
            ccrz.cc_CallContext.currAccountId = null;
            Test.startTest();
            osf_logicProductPricing.showPrices(); //duplicate call in order to cover cache class
            result = logic.fetchPriceListsAndItems(inputData);
            Test.stopTest();
        }
        System.assert(!result.isEmpty());
    }

    @IsTest
    public static void testProductPricingHidePricesSkipPriceHiding() {
        SetupData setupData = new SetupData(false);
        Map<String, Object> inputData = setupData.testData;
        Map<String, Object> result = new Map<String, Object> {};
        osf_logicProductPricing logic = new osf_logicProductPricing();
        osf_logicProductPricing.skipHidePrices = true;
        System.runAs(setupData.testUser) {
            Test.startTest();
            result = logic.fetchPriceListsAndItems(inputData);
            Test.stopTest();
        }
        osf_logicProductPricing.skipHidePrices = false;
        System.assert(!result.isEmpty());
    }

    public class SetupData {
        public User testUser;
        private Map<String, Object> testData;
        private ccrz__E_Product__c testProduct;

        public SetupData(Boolean isPriceVisible) {
            createTestData();
            testUser = createNewPortalUser(osf_testHelper.COMMUNITY_PROFILE, 1, isPriceVisible).get(0);
            testUser = [SELECT AccountId FROM User WHERE Id = :testUser.Id];
            createTestProduct();
            createTestProductPricingData();
        }

        private List<User> createNewPortalUser(String profileName, Integer quantity, Boolean isPriceVisible) {
            List<User> userList = new List<User> ();
            List<Account> accountList = createNewAccount(quantity, isPriceVisible);
            List<Contact> contactList = createNewContact(quantity, accountList);
            Id profileId = [SELECT Id FROM Profile WHERE Name = :profileName].Id;
            String uniqueName = osf_testHelper.generateUniqueName();
            for(Integer i = 0; i < quantity; i++) {
                User user = new User(
                    ProfileId = profileId,
                    EmailEncodingKey = 'ISO-8859-1',
                    LanguageLocaleKey = 'en_US',
                    TimeZoneSidKey = 'America/New_York',
                    LocaleSidKey = 'en_US',
                    FirstName = 'PortalUserFirst' + i,
                    LastName = 'PortalUserLast' + i,
                    CommunityNickName = 'testUser' + i,
                    Alias = 'test' + i,
                    Email = uniqueName + '@testemail.com' + i,
                    Username = uniqueName + '@testemail.com' + i,
                    IsActive = true,
                    ContactId = contactList.get(i).Id,
                    PortalRole = 'Worker'
                );
                userList.add(user);
            }
            insert userList;
            return userList;
        }

        private List<Account> createNewAccount(Integer quantity, Boolean isPriceVisible) {
            List<Account> accountList = new List<Account> ();
            for(Integer i = 0; i < quantity; i++) {
                Account account = new Account(
                    Name = 'Test Account' + i,
                    Phone = '000000000' + i,
                    osf_prices_visible__c = isPriceVisible
                );
                accountList.add(account);
            }
            insert accountList;
            return accountList;
        }

        private List<Contact> createNewContact(Integer quantity, List<Account> accountList) {
            List<Contact> contactList = new List<Contact> ();
            for(Integer i = 0; i < quantity; i++) {
                Contact contact = new Contact (
                    LastName = 'Test Contact Last Name' + i,
                    AccountId = accountList.get(i).Id,
                    Phone = '000000000' + i
                );
                contactList.add(contact);
            }
            insert contactList;
            return contactLİst;
        }

        private void createTestData () {
            Map<String, Map<String, Object>> testSetupMap = new Map<String, Map<String, Object>> {
                ccrz.ccAPITestData.STOREFRONT_SETTINGS => new Map<String, Object> {
                    osf_testHelper.STOREFRONT => new Map<String, Object> {
                        'Allow_Anonymous_Browsing__c' => true,
                        'Currencies__c' => 'USD;EUR',
                        'Languages__c' => 'en_US;jp_JP;zn_CN',
                        'Customer_Portal_Account_Name__c' => 'PortalAccount',
                        'CustomerPortalAcctGroupName__c' => 'PortalAccount',
                        'DefaultCurrencyCode__c' => 'USD',
                        'DefaultLocale__c' => 'en_US',
                        'InventoryCheckFlag__c' => false,
                        'Quoting_Enabled__c' => false,
                        'Skip_Tax_Calculation__c' => true,
                        'Filter_Orders_Based_on_Owner__c' => false
                    }
                },
                ccrz.ccAPITestData.SERVICE_SETTINGS => new Map<String, Object> {
                    osf_testHelper.STOREFRONT => new Map<String, Object> {
                        'ccServiceAccount' => 'ccServiceAccount'
                    }
                }
            };
            ccrz.ccAPITestData.setupData(testSetupMap);
        }

        private void createTestProduct() {
            this.testProduct = new ccrz__E_Product__C(
                Name = 'Test Product',
                ccrz__SKU__c = 'Test001'
            );
            insert this.testProduct;
        }

        private void createTestProductPricingData() {
            this.testData = new Map<String, Object> {
                'productPricingData' => new Map<String, Object> {
                    this.testProduct.Id => new Map<String, Object> {
                        'productPrice' => new Map<String, Object> {'price' => 100.00}
                    }
                }
            };
        }
    }
}