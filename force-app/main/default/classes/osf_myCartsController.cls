/**
 * File:        osf_myCartsController.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 23, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Controller class for osf_myCarts.page
  ************************************************************************
 * History:
 */
global with sharing class osf_myCartsController {

    /**********************************************************************************************
    * @Name         : changeCurrency
    * @Description  : get all the open carts for a user
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 06, 2020
    * @param ctx    : remote action context
    * @Return       : cc_RemoteActionResult, the result
    *********************************************************************************************/
    @RemoteAction
	global static ccrz.cc_RemoteActionResult changeCurrency(final ccrz.cc_RemoteActionContext ctx, String selectedCurrency) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx); 
        res.inputContext = ctx;
        try {
            User user = [SELECT Id, ccrz__CC_CurrencyCode__c FROM User WHERE Id = :ccrz.cc_CallContext.currUserId];
            user.ccrz__CC_CurrencyCode__c = selectedCurrency;
            Database.update(user);
            res.success = true;
        } catch(Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:osf_myCartsController:changeCurrency:Error', e.getMessage() + ' in line ' + e.getLineNumber());
            res.success = false;
            res.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(res);
        }
        return res;
    }

    /**********************************************************************************************
    * @Name         : cloneCartWithoutAdjustments
    * @Description  : Clones Cart. Override the standard clone functionality to prevent cart level adjustments to be carried to the new cart
    * @Created By   : Ozgun Eser
    * @Created Date : Dec 17, 2019
    * @param        : ccrz.cc_RemoteActionContext ctx
    * @Return       : cc_RemoteActionResult, result containing the new cart data to redirect cart page.
    *********************************************************************************************/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult cloneCartWithoutAdjustments (ccrz.cc_RemoteActionContext ctx, String cartId) {
        ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
        Map<String, Object> data = new Map<String, Object> ();
        result.data = data;
        Savepoint sp = Database.setSavePoint();
        try {
            Map<String, Object> outputData = ccrz.ccAPICart.cloneCart(new Map<String, Object> {
                ccrz.ccAPICart.CART_ID => cartId,
                ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
            });
            if(!(Boolean) outputData.get(ccrz.ccAPI.SUCCESS)) {
                ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:myCarts:cloneCartWithoutAdjustments:outputData', outputData);
                throw new osf_myCartsException ('Cart clone is failed!!!');
            }
            String newCartId = (String) outputData.get(ccrz.ccAPICart.CART_ID);
            ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c, ccrz__AdjustmentAmount__c FROM ccrz__E_Cart__c WHERE Id = :newCartId];
            cart.ccrz__AdjustmentAmount__c = null;
            update cart;
            data.put(osf_constant_strings.NEW_CART_ID, cart.ccrz__EncryptedId__c);
            result.success = true;
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:ctrl:myCarts:cloneCartWithoutAdjustments:Error', e);
            result.messages.add(osf_utility.createBeanMessage(e));
        } finally {
            ccrz.ccLog.close(result);
        }
        return result;
    }

    public class osf_myCartsException extends Exception {}

    public class CartsListWrapper{
        public List<CartWrapper> cartsListWrapper{get; set;}
        public CartsListWrapper(){
            cartsListWrapper = new List<CartWrapper>();
        }
    }
    public class CartWrapper {
        public String sfid{get; set;}
        public String sfdcName{get; set;}
        public Datetime lastModifiedDate{get; set;}
        public Decimal subTotal{get; set;}
        public String cartCurrency{get; set;}
        public String encId{get; set;}
        public CartWrapper(){}
    }
}