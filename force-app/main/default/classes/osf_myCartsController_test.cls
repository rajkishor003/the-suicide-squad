/**
 * File:        osf_myCartsController_test.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 23, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_myCartsController
  ************************************************************************
 * History:
 */

@isTest
public with sharing class osf_myCartsController_test {
    /**********************************************************************************************
    * @Name         : createTestData
    * @Description  : Test method for creating test data
    * @Created By   : Alina Craciunel
    * @Created Date : Oct 23, 2019
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @TestSetup
    static void createTestData(){
        User sysadmin = [SELECT Id, LanguageLocaleKey, UserRoleId FROM User WHERE Profile.Name =: osf_testHelper.SYS_ADMIN_PROFILE AND IsActive = true AND UserRoleId != null LIMIT 1];
        system.runAs(sysadmin) {
            Account account = osf_testHelper.createAccount('Test Company', '0000000000');
            database.insert(account);
            Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
            database.insert(contact);
            User user = osf_testHelper.createCommunityUser(contact);
            user.ccrz__CC_CurrencyCode__c = 'USD';
            database.insert(user);
            ccrz__E_Product__c product = osf_testHelper.createCCProduct('111', 'test prod');
            database.insert(product);
        }
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        system.runAs(u1) {
            Account acc = [SELECT Id FROM Account LIMIT 1];  
            Contact c = [SELECT Id FROM Contact LIMIT 1];
            ccrz__E_Product__c p =[SELECT Id FROM ccrz__E_Product__c LIMIT 1];
            ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, acc, u1, c);
            cart.ccrz__AdjustmentAmount__c = 100;
            database.insert(cart);
            ccrz__E_CartItem__c  cartItem = osf_testHelper.createCartItem(p, cart, 1, 100);
            database.insert(cartItem);
        }
    }

    /**********************************************************************************************
    * @Name         : testChangeCurrency
    * @Description  : Test method for changeCurrency method
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 06, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @isTest
	public static void testChangeCurrency() {
        osf_testHelper.setupStorefront();
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        User u1 = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(u1, osf_testHelper.STOREFRONT, cart);
        ccrz.cc_RemoteActionResult res;
        Test.startTest();
        system.runAs(u1) {
            res = osf_myCartsController.changeCurrency(ctx, 'EUR');
        }
        Test.stopTest();
        System.assertEquals(true, res.success);
        u1 = [SELECT Id, ccrz__CC_CurrencyCode__c FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        System.assertEquals('EUR', u1.ccrz__CC_CurrencyCode__c);
    }

    @isTest
    public static void testCloneCartWithoutAdjustments() {
        osf_testHelper.setupStorefront();
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Profile.Name =: osf_testHelper.COMMUNITY_PROFILE AND Username = 'test@email.com' LIMIT 1];
        ccrz__E_Cart__C cart = [SELECT Id, ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        ccrz.cc_RemoteActionContext ctx = osf_testHelper.getRemoteActionContext(user, osf_testHelper.STOREFRONT, cart);
        Test.startTest();
        ccrz.cc_RemoteActionResult result = osf_myCartsController.cloneCartWithoutAdjustments(ctx, cart.Id);
        Test.stopTest();
        System.assert(result.success);
        Map<String, Object> data = (Map<String, Object>) result.data;
        System.assert(data.containsKey(osf_constant_strings.NEW_CART_ID));
        String newCartId = (String) data.get(osf_constant_strings.NEW_CART_ID);
        cart = [SELECT ccrz__AdjustmentAmount__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :newCartId];
        System.assertEquals(null, cart.ccrz__AdjustmentAmount__c);        
    }
}