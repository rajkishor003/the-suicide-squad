/**
* Trigger Handler class for Request For Quote.
* 
* @author Ozgun Eser
* @version 1.0
*/

public without sharing class osf_requestForQuote_TriggerHandler {
    // Static variable to disable the handler for unit testing
    @TestVisible private static Boolean isEnabled = true;
    @TestVisible private static final String QUOTE_PDF_ERROR_MESSAGE = 'RFQ_Quote_Availability_Error';
    private static final String QUOTE_STATUS_AVAILABLE = 'Available';

    /**
    * Method for handling Before Insert
    *
    * @param newRequestForQuoteList, List of inserted Request For Quote records.
    */
    public static void doBeforeInsert(List<osf_request_for_quote__c> newRequestForQuoteList) {
        if(!isEnabled) {
            return;
        }
        List<osf_request_for_quote__c> requestForQuoteList = new List<osf_request_for_quote__c> ();
        for(osf_request_for_quote__c requestForQuote : newRequestForQuoteList) {
            if(requestForQuote.osf_status__c == QUOTE_STATUS_AVAILABLE) {
                requestForQuoteList.add(requestForQuote);
            }
        }
        if(!requestForQuoteList.isEmpty()) {
            checkIfQuotePDFAttached(requestForQuoteList);
        }
    }

    /**
    * Method for handling After Insert
    *
    * @param newRequestForQuoteList, List of inserted Request For Quote records.
    */
    public static void doAfterInsert(List<osf_request_for_quote__c> requestForQuoteList) {
        if(!isEnabled) {
            return;
        }
        osf_utility.createChatterMessageForRequestForQuote(requestForQuoteList);
        osf_utility.autoFollowChatterRequestForQuotes(requestForQuoteList);
    }

    /**
    * Method for handling Before Update
    *
    * @param newRequestForQuoteMap, Map of Request For Quote records containing data after update
    * @param oldRequestForQuoteMap, Map of Request For Quote records containing data before update
    */
    public static void doBeforeUpdate(Map<Id, osf_request_for_quote__c> newRequestForQuoteMap, Map<Id, osf_request_for_quote__c> oldRequestForQuoteMap) {
        if(!isEnabled) {
            return;
        }
        List<osf_request_for_quote__c> requestForQuoteList = new List<osf_request_for_quote__c> ();
        for(osf_request_for_quote__c requestForQuote : newRequestForQuoteMap.values()) {
            osf_request_for_quote__c oldRequestForQuote = oldRequestForQuoteMap.get(requestForQuote.Id);
            if((requestForQuote.osf_status__c != oldRequestForQuote.osf_status__c || requestForQuote.osf_quote__c != oldRequestForQuote.osf_quote__c) && requestForQuote.osf_status__c == QUOTE_STATUS_AVAILABLE) {
                requestForQuoteList.add(requestForQuote);
            }
        }
        if(!requestForQuoteList.isEmpty()) {
            checkIfQuotePDFAttached(requestForQuoteList);
        }
    }

    /**
    * Method for handling After Update
    *
    * @param newRequestForQuoteMap, Map of Request For Quote records containing data after update
    * @param oldRequestForQuoteMap, Map of Request For Quote records containing data before update
    */
    public static void doAfterUpdate(Map<Id, osf_request_for_quote__c> newRequestForQuoteMap, Map<Id, osf_request_for_quote__c> oldRequestForQuoteMap) {
        if(!isEnabled) {
            return;
        }
        List<osf_request_for_quote__c> requestForQuoteList = new List<osf_request_for_quote__c> ();
        for(osf_request_for_quote__c requestForQuote : newRequestForQuoteMap.values()) {
            if(String.isNotBlank(requestForQuote.osf_status__c) && requestForQuote.osf_status__c != oldRequestForQuoteMap.get(requestForQuote.Id).osf_status__c) {
                requestForQuoteList.add(requestForQuote);
            }
        }
        if(!requestForQuoteList.isEmpty()) {
            osf_utility.createChatterMessageForRequestForQuote(requestForQuoteList);
        }
    }

    private static void checkIfQuotePDFAttached(List<osf_request_for_quote__c> requestForQuoteList) {
        Set<Id> quoteIdSet = new Set<Id> (); 
        Map<String, ContentDocumentLink> quoteIdToAttachmentMap = new Map<String, ContentDocumentLink> ();
        for(osf_request_for_quote__c requestForQuote : requestForQuoteList) {
            if(String.isNotBlank(requestForQuote.osf_quote__c)) {
                quoteIdSet.add(Id.valueOf(requestForQuote.osf_quote__c));
            }
        }
        for(ContentDocumentLink contentDocumentLink : [SELECT LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :quoteIdSet]) {
            quoteIdToAttachmentMap.put(contentDocumentLink.LinkedEntityId, contentDocumentLink);
        }
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPII18N.PAGE_LABEL_NAMES => new Set<String> {QUOTE_PDF_ERROR_MESSAGE},
            ccrz.ccAPII18N.LOCALE => UserInfo.getLocale()
        };
        Map<String, Object> outputData = ccrz.ccAPII18N.fetchPageLabels(inputData);
        Map<String, Object> pageLabelData = (Map<String, Object>) outputData.get(ccrz.ccAPII18N.PAGE_LABELS);
        String errorMessage = (String) pageLabelData.get(QUOTE_PDF_ERROR_MESSAGE);
        for(osf_request_for_quote__c requestForQuote : requestForQuoteList) {
            if(String.isBlank(requestForQuote.osf_quote__c) || !quoteIdToAttachmentMap.containsKey(requestForQuote.osf_quote__c)) {
                requestForQuote.osf_status__c.addError(errorMessage);
            }
        }        
    }
}