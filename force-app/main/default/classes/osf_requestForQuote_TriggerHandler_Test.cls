@isTest
public class osf_requestForQuote_TriggerHandler_Test {
    @TestSetup
    public static void createTestData(){
        insert new List<ccrz__E_PageLabel__c> {osf_testHelper.createPageLabel(osf_constant_strings.QUOTE_REQUESTED_CHATTER_MESSAGE_LABEL, 'Quote Requested'),
                                               osf_testHelper.createPageLabel(osf_constant_strings.QUOTE_IN_PROGRESS_CHATTER_MESSAGE_LABEL, 'In Progress'),
                                               osf_testHelper.createPageLabel(osf_constant_strings.QUOTE_AVAILABLE_CHATTER_MESSAGE_LABEL, 'Available'),
                                               osf_testHelper.createPageLabel(osf_constant_strings.QUOTE_DECLINED_CHATTER_MESSAGE_LABEL, 'Rejected')        
        };

        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;

        osf_quote__c quote = osf_testHelper.createQuote(200, account);
        insert quote;
    }

    @isTest
    public static void testRFQTrigger() {
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        User user = [SELECT Id FROM user WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__Account__c, ccrz__Contact__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Quote Requested');
        insert requestForQuote;

        List<FeedItem> feedItemList = [SELECT Id, Body FROM FeedItem WHERE ParentId = :requestForQuote.Id ORDER BY CreatedDate DESC];
        System.assertEquals(1, feedItemList.size());
        System.assertEquals('Quote Requested', feedItemList[0].Body);

        requestForQuote.osf_status__c = 'In Progress';
        update requestForQuote;
        feedItemList = [SELECT Id, Body FROM FeedItem WHERE ParentId = :requestForQuote.Id ORDER BY CreatedDate DESC];
        System.assertEquals(2, feedItemList.size());
        System.assertEquals('In Progress', feedItemList[0].Body);
    }

    @IsTest
    public static void beforeInsertTest() {
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        User user = [SELECT Id FROM user WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__Account__c, ccrz__Contact__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        osf_quote__c quote = [SELECT Id FROM osf_quote__c LIMIT 1];
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Quote Requested');
        requestForQuote.osf_quote__c = quote.Id;
        requestForQuote.osf_status__c = 'Available';
        Database.SaveResult saveResult = Database.insert(requestForQuote, false);
        System.assert(!saveResult.isSuccess());
    }

    @IsTest
    public static void beforeUpdateTest() {
        ccrz.cc_CallContext.storefront = osf_testHelper.STOREFRONT;
        User user = [SELECT Id FROM user WHERE Username = 'test@email.com'];
        ccrz__E_Cart__c cart = [SELECT Id, ccrz__Account__c, ccrz__Contact__c FROM ccrz__E_Cart__c WHERE ccrz__User__c = :user.Id];
        osf_quote__c quote = [SELECT Id FROM osf_quote__c LIMIT 1];
        osf_request_for_quote__c requestForQuote = osf_testHelper.createRequestForQuote(cart, 'Quote Requested');
        insert requestForQuote;
        requestForQuote.osf_quote__c = quote.Id;
        requestForQuote.osf_status__c = 'Available';
        Database.SaveResult saveResult = Database.update(requestForQuote, false);
        System.assert(!saveResult.isSuccess());
    }
    
}