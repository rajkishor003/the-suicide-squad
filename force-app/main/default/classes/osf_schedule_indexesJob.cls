/**
 * File:        osf_schedule_indexesJob
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jan 30, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Schedulable calss for refreshing the indexes
  ************************************************************************
 * History:
 */

global class osf_schedule_indexesJob implements Schedulable {
    //private static final String CRON = '0 0 3 * * ? *'; 

    /**********************************************************************************************
    * @Name         : execute
    * @Description  : execute method of the schedulable class
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @param ctx    : SchedulableContext context
    * @Return       : 
    *********************************************************************************************/
    global void execute(SchedulableContext ctx) {
        //String jobID = System.schedule(osf_schedule_indexesJob.class.getName(), CRON, new osf_schedule_indexesJob());
        osf_RunIndexesJob.runRefreshIndexes();
    }
}