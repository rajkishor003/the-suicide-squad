/**
 * File:        osf_schedule_indexesJob_Test
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jan 30, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Test class for osf_schedule_indexesJob_Test
  ************************************************************************
 * History:
 */

@IsTest
public class osf_schedule_indexesJob_Test {
    public static final String CRON_EXP = '0 0 0 * * ? *';
    public static final Set<String> TEST_PRODUCT_INDEX_BATCH_CLASS_NAME = new Set<String> {'ccProductIndexBuildJob2', 'ccProductIndexDisableOldJob', 'ccProductIndexCleanupJob'};
    public static final Set<String> TEST_PRODUCT_SPEC_INDEX_BATCH_CLASS_NAME = new Set<String> {'cc_job_LoadProductSpecIndex'};
    public static final Set<String> TEST_SITE_INDEX_BATCH_CLASS_NAME = new Set<String> {'ccSiteIndexBuildJob', 'ccSiteIndexDisableOldJob', 'ccSiteIndexCleanupJob'};

    /**********************************************************************************************
    * @Name         : testScheduledJob
    * @Description  : Test method for execute method
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : 
    *********************************************************************************************/
    @IsTest
    public static void testScheduledJob() {
        osf_testHelper helper = new osf_testHelper();

        Test.startTest();
        System.schedule(osf_schedule_indexesJob.class.getName(), CRON_EXP, new osf_schedule_indexesJob());
        Test.stopTest();
        List<AsyncApexJob> lstJobs = queryForBatchApexJobs();
        Map<Id, String> mapClasses = queryForApexClassesByJobIds(lstJobs);
        System.assertEquals(true, lstJobs.size() > 0); 
    }

    /**********************************************************************************************
    * @Name         : queryForBatchApexJobs
    * @Description  : returns the AsyncApexJob list
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * @Return       : List<AsyncApexJob>
    *********************************************************************************************/
    private static List<AsyncApexJob> queryForBatchApexJobs() {
        return [SELECT ApexClassID, MethodName, CompletedDate, JobType, NumberOfErrors, Status FROM AsyncApexJob WHERE JobType = 'BatchApex' ORDER BY CompletedDate ASC];
    }

    /**********************************************************************************************
    * @Name         : queryForApexClassesByJobIds
    * @Description  : returns the apex classes list by jobs
    * @Created By   : Alina Craciunel
    * @Created Date : Jan 30, 2020
    * Param         : List<AsyncApexJob> lstJobs
    * @Return       : Map<Id, String> classes
    *********************************************************************************************/
    private static Map<Id, String> queryForApexClassesByJobIds(List<AsyncApexJob> lstJobs) {
        Map<Id, String> mapClasses = new Map<Id, String>();
        for(AsyncApexJob job : lstJobs) {
            mapClasses.put(job.ApexClassID, '');
        }
        List<ApexClass> lstClasses = [SELECT Id, Name FROM ApexClass WHERE Id IN : mapClasses.keySet()];
        for(ApexClass apexClass: lstClasses) {
            mapClasses.put(apexClass.Id, apexClass.Name);
        }
        return mapClasses;
    }
}