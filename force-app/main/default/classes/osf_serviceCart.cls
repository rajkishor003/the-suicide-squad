/**
 * File:        osf_serviceCart.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Service Provider of CC Cart Object
  ************************************************************************
 * History:
 */

global with sharing class osf_serviceCart extends ccrz.ccServiceCart {
    
    @TestVisible private static final List<String> FIELD_LIST = new List<String> {'osf_converted_from_rfq__c', 'Contact__r.osf_allow_checkout__c'};

    /**********************************************************************************************
    * @Name         : getFieldsMap
    * @Description  : Adding custom fields to the cart model.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/ 
    global override Map<String, Object> getFieldsMap(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.getFieldsMap(inputData);
        String fields = (String) outputData.get(ccrz.ccService.OBJECTFIELDS);
        fields += osf_constant_strings.COMMA + String.join(FIELD_LIST, osf_constant_strings.COMMA);
        outputData.put(ccrz.ccService.OBJECTFIELDS, fields);
        return outputData;
    }


    /**********************************************************************************************
    * @Name         : prepReturn
    * @Description  : Returning 0 for pricing field if Account's Prices Visible field is false.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/       
    global override Map<String, Object> prepReturn (Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepReturn(inputData);
        try {
            if(!osf_logicProductPricing.showPrices()) {
                this.hidePricesForCart((List<Object>) outputData.get(ccrz.ccAPICart.CART_OBJLIST));
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceCart:prepReturn:Error', e);
        }
        return outputData;
    }

    /**********************************************************************************************
    * @Name         : hidePricesForCart
    * @Description  : Changes pricing fields' value to 0 in CC Cart Model.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : List<Object> cartList
    * @Return       : 
    *********************************************************************************************/
    private void hidePricesForCart(List<Object> cartList) {
        osf_utility.hidePricesInModel(cartList, new List<String> {
            osf_constant_strings.SUB_TOTAL,
            osf_constant_strings.SUBTOTAL_AMOUNT,
            osf_constant_strings.TAX_AMOUNT,
            osf_constant_strings.CART_TOTAL_AMOUNT,
            osf_constant_strings.TOTAL_DISCOUNT,
            osf_constant_strings.TOTAL_SURCHARGE,
            osf_constant_strings.ADJUSTMENT_AMOUNT,
            osf_constant_strings.SHIP_AMOUNT,
            osf_constant_strings.SHIP_DISCOUNT_AMOUNT,
            osf_constant_strings.TAX_SUBTOTAL_AMOUNT
        });
    }

    /**********************************************************************************************
    * @Name         : getFilterMap
    * @Description  : adds custom filters to CC Cart Query's Where Clause.
    * @Created By   : Alina Craciunel
    * @param        : Map<String, Object>, inputData
    * @Return       : Map<String, Object>, outputData
    *********************************************************************************************/
    global override Map<String, Object> getFilterMap(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.getFilterMap(inputData);
        if (ccrz.cc_CallContext.currPageName.contains(osf_constant_strings.MY_ACCOUNT_PAGE)) {
            outputData.remove(ccrz.ccAPICart.CURRCODE);
        }
        return outputData;
    }
}