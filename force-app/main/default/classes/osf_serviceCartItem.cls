/**
 * File:        osf_serviceCartItem.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Service Provider of CC Cart Item Object
  ************************************************************************
 * History:
 */

global with sharing class osf_serviceCartItem extends ccrz.ccServiceCartItem {

    /**********************************************************************************************
    * @Name         : prepReturn
    * @Description  : Returning 0 for pricing field if Account's Prices Visible field is false.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/    
    global override Map<String, Object> prepReturn(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepReturn(inputData);
        try {
            if(!osf_logicProductPricing.showPrices()) {
                Map<String, Object> cartItemsById = (Map<String, Object>) outputData.get(ccrz.ccAPICart.CARTITEMSBYID);
                for(Object cartItemsObject : (List<Object>) cartItemsById.values()) {
                    this.hidePricesForCartItem((List<Object>) cartItemsObject);
                }
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceCartItem:prepReturn:Error', e);
        }
        return outputData;
    }

    /**********************************************************************************************
    * @Name         : hidePricesForCartItem
    * @Description  : Changes pricing fields' value to 0 in CC Cart Item Model.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : List<Object> cartItemList
    * @Return       : 
    *********************************************************************************************/
    private void hidePricesForCartItem(List<Object> cartItemList) {
        osf_utility.hidePricesInModel(cartItemList, new List<String> {
            osf_constant_strings.PRICE,
            osf_constant_strings.ITEM_TOTAL,
            osf_constant_strings.SUB_AMOUNT,
            osf_constant_strings.ORIGINAL_ITEM_PRICE
        });
    }
}