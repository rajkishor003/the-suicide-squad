@IsTest
public  class osf_serviceCart_Test {

    @IsTest
    public static void testPrepReturn() {
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        account.osf_prices_visible__c = false;
        insert account;
        ccrz.cc_CallContext.currAccountId = account.Id;
        osf_serviceCart service = new osf_serviceCart();
        Test.startTest();
        Map<String, Object> cartMap = new Map<String, Object> {
            osf_constant_strings.SUB_TOTAL => 100,
            osf_constant_strings.SUBTOTAL_AMOUNT => 100,
            osf_constant_strings.TAX_AMOUNT => 10,
            osf_constant_strings.CART_TOTAL_AMOUNT => 115,
            osf_constant_strings.TOTAL_DISCOUNT => 5,
            osf_constant_strings.TOTAL_SURCHARGE => 10,
            osf_constant_strings.ADJUSTMENT_AMOUNT => 10,
            osf_constant_strings.SHIP_AMOUNT => 20,
            osf_constant_strings.SHIP_DISCOUNT_AMOUNT => 10,
            osf_constant_strings.TAX_SUBTOTAL_AMOUNT => 5
        };
        List<Map<String, Object>> cartListMap = new List<Map<String, Object>> {cartMap};
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPICart.CART_OBJLIST => cartListMap,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                service.entityName => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                }
            },
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
        };
        Map<String, Object> outputData = service.prepReturn(inputData);
        Test.stopTest();
        System.assert(!outputData.isEmpty());
    }

    @IsTest
    public static void testGetFieldsMap() {
        osf_serviceCart serviceCart = new osf_serviceCart();
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                serviceCart.ENTITYNAME => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M
                }
            }
        };
        Test.startTest();
        Map<String, Object> outputData = serviceCart.getFieldsMap(inputData);
        Test.stopTest();
        String fields = (String) outputData.get(ccrz.ccService.OBJECTFIELDS);
        for(String field : osf_serviceCart.FIELD_LIST) {
            System.assert(fields.contains(field));
        }
    }

    /**********************************************************************************************
    * @Name         : testGetFilterMap
    * @Description  : Test method for getFilterMap method
    * @Created By   : Alina Craciunel
    * @Created Date : Apr 16, 2020
    * @Param        : 
    * @Return       : 
    *********************************************************************************************/
    @IsTest
    public static void testGetFilterMap() {
        osf_ServiceCart serviceCart = new osf_ServiceCart();
        ccrz.cc_CallContext.currPageName = osf_constant_strings.MY_ACCOUNT_PAGE;
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccService.BASEFIELDSMAP => new Map<String, Object> {
                ccrz.ccAPI.SZ_M => 'Id,OwnerId,Name,EncryptedId__c,ActiveCart__c,ShipStructure__c,ValidationStatus__c,CartStatus__c,CartType__c,LastModifiedDate,SubtotalAmount__c,Name__c,CurrencyISOCode__c'
            },
            ccrz.ccAPICart.ACTIVECART => false,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                serviceCart.ENTITYNAME => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M
                }
            },
            ccrz.ccAPICart.CARTSTATUS => 'Open',
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
        };
        Test.startTest();
        Map<String, Object> outputData = serviceCart.getFilterMap(inputData);
        Test.stopTest();
        System.assert(!outputData.containsKey(ccrz.ccAPICart.CURRCODE));
    }
}