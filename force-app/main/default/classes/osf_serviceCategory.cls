/**
 * File:        osf_serviceCategory.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Oct 16, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: CCRZ extension point Service class that's responsible for returning CC Category related fields.
  ************************************************************************
 * History:
 * Date:                Modified By:            Description:
 */

global with sharing class osf_serviceCategory extends ccrz.ccServiceCategory {
    
    global static final List<String> FIELDS_TO_ADD = new List<String> {'Osf_Contact_Us_Topic__c'};
    global override Map<String, Object> getSubQueryMap(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.getSubQueryMap(inputData);
        try {
            outputData.put(osf_constant_strings.CATEGORY_LOCALIZATION_RELATIONSHIP_NAME, osf_constant_strings.CATEGORY_LOCALIZATION_SUB_QUERY);
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceCategory:getSubQueryMap:Error', e);
        }
        return outputData;
    }

    global override Map<String, Object> prepReturn(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepReturn(inputData);
        try {
            String pageName = (String) ccrz.cc_CallContext.currPageName;
            if((pageName == osf_constant_strings.PLP_PAGE_NAME || pageName == osf_constant_strings.PLP_PAGE_NAME_WITH_PREFIX) || Test.isRunningTest()) {
                Set<String> categoryLocaleIdSet = new Set<String> ();
                Set<String> attachmentNameSet = new Set<String> ();
                Map<String, List<Attachment>> categoryIdToAttachmentListMap = new Map<String, List<Attachment>> ();
                Map<String, StaticResource> staticResourceNameToResourceMap = new Map<String, StaticResource> ();
                List<Map<String, Object>> categoryListMap = (List<Map<String, Object>>) outputData.get(ccrz.ccApiCategory.CATEGORYLIST);
                for(Map<String, Object> categoryMap : categoryListMap) {
                    if(!categoryMap.containsKey(osf_constant_strings.CATEGORY_LOCALE_NAME) && !Test.isRunningTest()) continue;
                    List<Map<String, Object>> categoryLocaleMapList = (List<Map<String, Object>>) categoryMap.get(osf_constant_strings.CATEGORY_LOCALE_NAME);
                    Map<String, Object> categoryLocaleMap = categoryLocaleMapList[0];
                    String attachmentName = (String) categoryLocaleMap.get(osf_constant_strings.OSF_ATTACHMENT_NAME);
                    if(String.isNotBlank(attachmentName)) {
                        attachmentNameSet.add(attachmentName);
                    }
                    //categoryLocaleIdSet.add((String) categoryLocaleMap.get(osf_constant_strings.SF_ID));
                }

                if(!attachmentNameSet.isEmpty()) {
                    for(StaticResource static_resource : [SELECT Id, Name FROM StaticResource WHERE NAME IN :attachmentNameSet]) {
                        staticResourceNameToResourceMap.put(static_resource.Name, static_resource);
                    }
                }

                for(Map<String, Object> categoryMap : categoryListMap) {
                    if(!categoryMap.containsKey(osf_constant_strings.CATEGORY_LOCALE_NAME) && !Test.isRunningTest()) { continue; }
                    List<Map<String, Object>> categoryLocaleMapList = (List<Map<String, Object>>) categoryMap.get(osf_constant_strings.CATEGORY_LOCALE_NAME);
                    Map<String, Object> categoryLocaleMap = categoryLocaleMapList[0];
                    String attachmentName = (String) categoryLocaleMap.get(osf_constant_strings.OSF_ATTACHMENT_NAME);
                    if(String.isNotBlank(attachmentName) && staticResourceNameToResourceMap.containsKey(attachmentName)) {
                        categoryLocaleMap.put(osf_constant_strings.CONTENT_EXIST, true);
                    } else {
                        categoryLocaleMap.put(osf_constant_strings.CONTENT_EXIST, false);
                    }
                }

                /*if(!categoryLocaleIdSet.isEmpty()) {
                    for(Attachment att : [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN :categoryLocaleIdSet]) {
                        if(!categoryIdToAttachmentListMap.containsKey(att.ParentId)) categoryIdToAttachmentListMap.put(att.ParentId, new List<Attachment> ());
                        categoryIdToAttachmentListMap.get(att.ParentId).add(att);
                    }
                }*/

                /*if(!categoryIdToAttachmentListMap.isEmpty()) {
                    for(Map<String, Object> categoryMap : categoryListMap) {
                        if(!categoryMap.containsKey(osf_constant_strings.CATEGORY_LOCALE_NAME)) continue;
                        List<Map<String, Object>> categoryLocaleMapList = (List<Map<String, Object>>) categoryMap.get(osf_constant_strings.CATEGORY_LOCALE_NAME);
                        Map<String, Object> categoryLocaleMap = categoryLocaleMapList[0];
                        //categoryLocaleIdSet.add((String) categoryLocaleMap.get(osf_constant_strings.SF_ID));
                        String categoryLocaleId = (String) categoryLocaleMap.get(osf_constant_strings.SF_ID);
                        List<Attachment> attList = categoryIdToAttachmentListMap.get(categoryLocaleId);
                        List<Map<String, Object>> attListMap = new List<Map<String, Object>> ();
                        for(Attachment att : attList) {
                            attListMap.add(new Map<String, Object> {
                                osf_constant_strings.ATTACHMENT_NAME => att.Name,
                                osf_constant_strings.ATTACHMENT_ID => att.Id
                            });
                        }
                        if(!attListMap.isEmpty()){
                            categoryLocaleMap.put(osf_constant_strings.ATTACHMENT_LIST, attListMap);
                        }   
                    }
                }*/
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceCategory:prepReturn:Error', e);
        }
        return outputData;
    }
    
    /**********************************************************************************************
    * @Name         : getFieldsMap
    * @Description  : Added custom Contact Us Topic field
    * @Created By   : Shikhar Srivastava
    * @Created Date : July 03, 2020
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> fields
    *********************************************************************************************/
    global virtual override Map<String, Object> getFieldsMap(Map<String, Object> inputData) {
        inputData = super.getFieldsMap(inputData);
        String objectFields = (String)inputData.get(ccrz.ccService.OBJECTFIELDS);
        objectFields += ',' + String.join(FIELDS_TO_ADD, ',');
        inputData.put(ccrz.ccService.OBJECTFIELDS, objectFields);
        return inputData;
    }
}