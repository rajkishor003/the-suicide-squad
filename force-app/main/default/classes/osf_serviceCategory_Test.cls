@isTest
public class osf_serviceCategory_Test {
	@isTest
	public static void testGetSubQueryMap() {
		osf_serviceCategory service = new osf_serviceCategory();
		Test.startTest();
		Map<String, Object> resultMap = service.getSubQueryMap(new Map<String, Object> {
			ccrz.ccAPI.SIZING => new Map<String, Object> {
				service.entityName => new Map<String, Object> {
					ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
				}
			}
		});
		Test.stopTest();
        System.assert(!resultMap.isEmpty());
        System.assert(resultMap.containsKey(osf_constant_strings.CATEGORY_LOCALIZATION_RELATIONSHIP_NAME));
        System.assertEquals(resultMap.get(osf_constant_strings.CATEGORY_LOCALIZATION_RELATIONSHIP_NAME), osf_constant_strings.CATEGORY_LOCALIZATION_SUB_QUERY);
	}

	@isTest
	public static void testPrepReturnMap() {
		osf_serviceCategory service = new osf_serviceCategory();
		Test.startTest();
		Map<String, Object> categoryMap = new Map<String, Object> {
			osf_constant_strings.OSF_ATTACHMENT_NAME => 'testAttachment'
		};
		List<Map<String, Object>> categoryMapList = new List<Map<String, Object>> {categoryMap};
		Map<String, Object> inputData = new Map<String, Object> {
			osf_constant_strings.CATEGORY_LOCALE_NAME => categoryMapList
		};
		List<Map<String, Object>> inputDataList = new List<Map<String, Object>> {inputData};
		Map<String, Object> resultMap = service.prepReturn(new Map<String, Object> {
			ccrz.ccApiCategory.CATEGORYLIST => inputDataList,
			ccrz.ccAPI.SIZING => new Map<String, Object> {
				service.entityName => new Map<String, Object> {
					ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
				}
			}
		});
		Test.stopTest();
		System.assert(!resultMap.isEmpty());
	}
    
    @isTest
    public static void testGetFieldsMap() {
        osf_serviceCategory service = new osf_serviceCategory();
        Test.startTest();
        Map<String, Object> inputData = service.getFieldsMap(new Map<String, Object> {
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                service.entityName => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M
                }
            }
        });
        Test.stopTest();
        String fields = (String) inputData.get(ccrz.ccService.OBJECTFIELDS);
        for(String field : osf_serviceCategory.FIELDS_TO_ADD) {
            System.assert(fields.contains(field));
        }
    }
}