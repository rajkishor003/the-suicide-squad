/**
 * File:        osf_serviceOrder.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Service Provider of CC Order Object
  ************************************************************************
 * History:
 */
 global with sharing class osf_serviceOrder extends ccrz.ccServiceOrder {

    global static final List<String> FIELDS_TO_ADD = new List<String> {'osf_PurchaseOrder__c', 'osf_PurchaseOrder__r.Name', 'osf_PurchaseOrder__r.osf_PurchaseOrderId__c', 'osf_contracting_entity__c', 'osf_contracting_entity__r.osf_name__c', 'osf_contracting_entity__r.osf_address__c'};

    /**********************************************************************************************
    * @Name         : prepReturn
    * @Description  : Returning 0 for pricing field if Account's Prices Visible field is false.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/ 
    global override Map<String, Object> prepReturn(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepReturn(inputData);
        try {
            if(!osf_logicProductPricing.showPrices()) {
                this.hidePricesForOrder((List<Object>) outputData.get(ccrz.ccAPIOrder.ORDERLIST));
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceOrder:prepReturn:Error', e);
        }
        return outputData;
    }
    
    /**********************************************************************************************
    * @Name         : hidePricesForOrder
    * @Description  : Changes pricing fields' value to 0 in CC Order Model.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : List<Object> orderList
    * @Return       : 
    *********************************************************************************************/
    private void hidePricesForOrder(List<Object> orderList) {
        osf_utility.hidePricesInModel(orderList, new List<String> {
            osf_constant_strings.SUB_TOTAL,
            osf_constant_strings.SUBTOTAL_AMOUNT,
            osf_constant_strings.TAX_AMOUNT,
            osf_constant_strings.CART_TOTAL_AMOUNT,
            osf_constant_strings.TOTAL_DISCOUNT,
            osf_constant_strings.TOTAL_SURCHARGE,
            osf_constant_strings.ADJUSTMENT_AMOUNT,
            osf_constant_strings.SHIP_AMOUNT,
            osf_constant_strings.SHIP_DISCOUNT_AMOUNT,
            osf_constant_strings.TAX_SUBTOTAL_AMOUNT
        });
    }

    /**********************************************************************************************
    * @Name         : getFieldsMap
    * @Description  : Added custom purchase order field
    * @Created By   : Alina Craciunel
    * @Created Date : Nov 27, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> fields
    *********************************************************************************************/
    global virtual override Map<String, Object> getFieldsMap(Map<String, Object> inputData) {
        inputData = super.getFieldsMap(inputData);
        String objectFields = (String)inputData.get(ccrz.ccService.OBJECTFIELDS);
        objectFields += ',' + String.join(FIELDS_TO_ADD, ',');
        inputData.put(ccrz.ccService.OBJECTFIELDS, objectFields);
        return inputData;
    }
}