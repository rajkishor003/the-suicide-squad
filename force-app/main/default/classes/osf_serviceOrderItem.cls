/**
 * File:        osf_serviceOrderItem.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 18, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Extension for Service Provider of CC Order Item Object
  ************************************************************************
 * History:
 */
global with sharing class osf_serviceOrderItem extends ccrz.ccServiceOrderItem {

    /*********************************************************************************************
    * @Name         : prepReturn
    * @Description  : Returning 0 for pricing field if Account's Prices Visible field is false.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : Map<String, Object> inputData
    * @Return       : Map<String, Object> outputData
    *********************************************************************************************/
    global override Map<String, Object> prepReturn (Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepReturn(inputData);
        try {
            if(!osf_logicProductPricing.showPrices()) {
                Map<String, Object> orderItemsById = (Map<String, Object>) outputData.get(ccrz.ccAPIOrder.ORDERITEMSBYID);
                for(Object orderItemObj : (List<Object>) orderItemsById.values()) {
                    this.hidePricesForOrderItem((List<Object>) orderItemObj);
                }
            }
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceOrderItem:prepReturn:Error', e);
        }
        return outputData;
    }

    /*********************************************************************************************
    * @Name         : hidePricesForOrderItem
    * @Description  : Changes pricing fields' value to 0 in CC Order Item Model.
    * @Created By   : Ozgun Eser
    * @Created Date : Nov 18, 2019
    * @param        : List<Object> orderItemList
    * @Return       : 
    *********************************************************************************************/
    private void hidePricesForOrderItem(List<Object> orderItemList) {
        osf_utility.hidePricesInModel(orderItemList, new List<String> {
            osf_constant_strings.PRICE,
            osf_constant_strings.ITEM_TOTAL,
            osf_constant_strings.SUB_AMOUNT,
            osf_constant_strings.ORIGINAL_ITEM_PRICE
        });
    }
}