@IsTest
public class osf_serviceOrderItem_Test {
    
    @IsTest
    public static void testPrepReturn() {
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        account.osf_prices_visible__c = false;
        insert account;
        ccrz.cc_CallContext.currAccountId = account.Id;
        osf_serviceOrderITem service = new osf_serviceOrderItem();
        Test.startTest();
        Map<String, Object> orderItemMap = new Map<String, Object> {
            osf_constant_strings.PRICE => 100,
            osf_constant_strings.ITEM_TOTAL => 100,
            osf_constant_strings.SUB_AMOUNT => 100,
            osf_constant_strings.ORIGINAL_ITEM_PRICE => 100
        };
        List<Object> orderItemMapList = new List<Object> {orderItemMap};
        Map<String, Object> orderItemByIdMap = new Map<String, Object> {'TestOrderItemId' => orderItemMapList};
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPIOrder.ORDERITEMSBYID => orderItemByIdMap,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                service.entityName => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                }
            },
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
        };
        Map<String, Object> outputData = service.prepReturn(inputData);
        Test.stopTest();
        System.assert(!outputData.isEmpty());
    }
}