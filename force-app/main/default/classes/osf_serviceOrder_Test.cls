@IsTest
public class osf_serviceOrder_Test {
    
    @IsTest
    public static void testPrepReturn() {
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        account.osf_prices_visible__c = false;
        insert account;
        ccrz.cc_CallContext.currAccountId = account.Id;
        osf_serviceOrder service = new osf_serviceOrder();
        Test.startTest();
        Map<String, Object> orderMap = new Map<String, Object> {
            osf_constant_strings.SUB_TOTAL => 100,
            osf_constant_strings.SUBTOTAL_AMOUNT => 100,
            osf_constant_strings.TAX_AMOUNT => 10,
            osf_constant_strings.CART_TOTAL_AMOUNT => 115,
            osf_constant_strings.TOTAL_DISCOUNT => 5,
            osf_constant_strings.TOTAL_SURCHARGE => 10,
            osf_constant_strings.ADJUSTMENT_AMOUNT => 10,
            osf_constant_strings.SHIP_AMOUNT => 20,
            osf_constant_strings.SHIP_DISCOUNT_AMOUNT => 10,
            osf_constant_strings.TAX_SUBTOTAL_AMOUNT => 5
        };
        List<Map<String, Object>> orderListMap = new List<Map<String, Object>> {orderMap};
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccAPIOrder.ORDERLIST => orderListMap,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                service.entityName => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                }
            },
            ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION
        };
        Map<String, Object> outputData = service.prepReturn(inputData);
        Test.stopTest();
        System.assert(!outputData.isEmpty());
    }

    @isTest
    public static void testGetFieldsMap() {
        osf_serviceOrder service = new osf_serviceOrder();
        Test.startTest();
        Map<String, Object> inputData = service.getFieldsMap(new Map<String, Object> {
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                service.entityName => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M
                }
            }
        });
        Test.stopTest();
        String fields = (String) inputData.get(ccrz.ccService.OBJECTFIELDS);
        for(String field : osf_serviceOrder.FIELDS_TO_ADD) {
            System.assert(fields.contains(field));
        }
    }
}