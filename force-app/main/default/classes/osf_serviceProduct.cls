/**
 * File:        osf_serviceProduct.cls
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Dec 17, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: CCRZ extension point Service class that's responsible for returning CC Product related fields.
  ************************************************************************
 * History:
 * Date:                Modified By:            Description:
 */
global with sharing class osf_serviceProduct extends ccrz.ccServiceProduct {
    
    global override Map<String, Object> prepReturn(Map<String, Object> inputData) {
        Map<String, Object> outputData = super.prepReturn(inputData);
        try {
            List<Map<String, Object>> productMapList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
            Set<String> productIdSet = new Set<String> ();
            Map<String, Decimal> productIdToQuantityMap = new Map<String, Decimal> ();
            for(Map<String, Object> productMap : productMapList) {
                productIdSet.add((String) productMap.get(osf_constant_strings.SF_ID));
            }
            for(ccrz__E_CartItem__c cartItem : [SELECT ccrz__Product__c, ccrz__Quantity__c FROM ccrz__E_CartItem__c WHERE ccrz__Product__c IN :productIdSet AND ccrz__Cart__r.ccrz__EncryptedId__c = :ccrz.cc_CallContext.currCartId]) {
                productIdToQuantityMap.put(cartItem.ccrz__Product__c, cartItem.ccrz__Quantity__c);
            }
            for(Map<String, Object> productMap : productMapList) {
                String productId = (String) productMap.get(osf_constant_strings.SF_ID);
                productMap.put(osf_constant_strings.QUANTITY_IN_CART, productIdToQuantityMap.containsKey(productId) ? productIdToQuantityMap.get(productId) : 0);
            }            
        } catch (Exception e) {
            ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:serviceProduct:prepReturn:Error', e);
        }
        return outputData;
    }
}