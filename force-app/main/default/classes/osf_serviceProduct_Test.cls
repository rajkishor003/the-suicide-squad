@isTest
public class osf_serviceProduct_Test {
    
    @isTest
    public static void testPrepReturn() {
        Account account = osf_testHelper.createAccount('Test Company', '0000000000');
        insert account;

        Contact contact = osf_testHelper.createContact('John', 'Doe', account, 'test@email.com', '0000000000');
        insert contact;

        User user = osf_testHelper.createCommunityUser(contact);
        insert user;

        ccrz__E_Product__c product = osf_testHelper.createCCProduct('Test SKU', 'Test Product');
        ccrz__E_Product__c product2 = osf_testHelper.createCCProduct('Test SKU 2', 'Test Product 2');
        insert new List<ccrz__E_Product__c> {product, product2};

        ccrz__E_Cart__c cart = osf_testHelper.createCart(null, null, 0, 100, 10, account, user, contact);
        insert cart;

        ccrz__E_CartItem__c cartItem = osf_testHelper.createCartItem(product, cart, 1, 100);
        insert cartItem;

        Test.startTest();
        ccrz__E_Cart__c newCart = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE Id = :cart.Id];
        ccrz.cc_CallContext.currCartId = newCart.ccrz__EncryptedId__c;
        osf_serviceProduct service = new osf_serviceProduct();
        Map<String, Object> resultMap = service.prepReturn(new Map<String, Object> {
            ccrz.ccAPIProduct.PRODUCTLIST => new List<Map<String, Object>> {
                new Map<String, Object> {
                    osf_constant_strings.SF_ID => product.Id
                },
                new Map<String, Object> {
                    osf_constant_strings.SF_ID => product2.Id
                }
            },
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                service.entityName => new Map<String, Object> {
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
                }
            }
        });
        Test.stopTest();
        System.assert(!resultMap.isEmpty());
        List<Map<String, Object>> productMapList = (List<Map<String, Object>>) resultMap.get(ccrz.ccAPIProduct.PRODUCTLIST);
        for(Map<String, Object> productMap : productMapList) {
            String productId = (String) productMap.get(osf_constant_strings.SF_ID);
            Decimal quantity = (Decimal) productMap.get(osf_constant_strings.QUANTITY_IN_CART);
            if(productId == product.Id) {
                System.assertEquals(1, quantity);
            } else {
                System.assertEquals(0, quantity);
            }
        }
    }
}