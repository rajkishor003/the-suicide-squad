<apex:component controller="osf_ctrl_header">
    <script id="osf_MiniQuickOrder-Desktop" type="text/template">
        <div class="panel panel-default cc_panel cc_mini_quick_order">
            <div class="panel-heading cc_heading">
                <h3 class="panel-title cc_title">{{pageLabelMap 'QuickOrder'}}
                    <span class="pull-right">
                        <span class="icon-toggle cc_filter_icon" role="button" data-toggle="collapse" data-target="#collapseMiniQuickOrder" aria-expanded="true" aria-controls="collapseMiniQuickOrder">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </span>
                    </span>
                </h3>
            </div>
            <div id="collapseMiniQuickOrder" class="panel-collapse collapse {{#ifNotMobile}}in{{/ifNotMobile}}">
                <div class="panel-body cc_body">
                    <p class="quickOrderQtyWarn">{{pageLabelMap 'QuickOrderQtyExceededWarning'}}</p>
                    <div id="quick_order" class="quick_order cc_quick_order"></div>
                    <p class="cc_caption">{{pageLabelMap 'QuickOrder_QuickOrderBoxCaption'}}</p>
                    <form id="skuQtyForm" class="form-horizontal cc_form_sku_qty">
                        <div class="quickOrderRow cc_quick_order_row">
                        </div>
                    </form>
                </div>
                <div class="panel-footer cc_footer">
                    <div class="form-group text-center cc_form_group">
                        <button class="btn btn-default btn-sm addToCart cc_addtocart btn-locked" type="submit">{{pageLabelMap 'QuickOrder_AddToCart'}}</button>
                        <button class="btn btn-default btn-sm addMore cc_addmore" type="submit">{{pageLabelMap 'QuickOrder_AddMore'}}</button>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <script>
        jQuery(function ($) {           
            CCRZ.subsc = _.extend(CCRZ.subsc || {});

            CCRZ.subsc.quickOrderRemoteActions = _.extend({
                className: "osf_ctrl_header",
                checkLockedCartAction: function (callback) {
                    this.invokeCtx("checkLockedCart", function (response) {
                        callback(response);
                    }, { buffer: false, nmsp: false, timeout: 120000 });
                }
            }, CCRZ.RemoteInvocation);
            
            CCRZ.pubSub.on('view:QuickOrderView:refresh', function (QuickOrderView) {
                QuickOrderView.addProductsToCart = function(event) {
                    var formData = form2js('skuQtyForm', '.', false, function(node) {}, false, true);
                    if (this.validateQuantities(formData.quickorder)) {
                        loading(jQuery('.addToCart'));
                        var dataSet = JSON.stringify(formData.quickorder);
                        $('.quickOrderQtyWarn').hide();
                        $('.quick_order').empty();
                        this.invokeCtx("addBulk", dataSet,
                            function(response) {
                                if (response && response.data) {
                                    var cartId = response.data.cartId;
                                    CCRZ.pagevars.currentCartID = cartId;
                                    CCRZ.console.log(response);
                                    //cart change will set cookie
                                    CCRZ.pubSub.trigger('cartChange', cartId);
                                    CCRZ.pubSub.trigger('pageMessage', response.data);
                                    CCRZ.pubSub.trigger('action:CartDetailView:refresh', response.data);

                                    doneLoading(jQuery('.addToCart'));
                                    //reset the sku and qty
                                    if (CCRZ.pagevars.pageConfig.isTrue('qo.clr')) {
                                        $(".sku-input").val('');
                                        $(".qty-input").val(1);
                                    }
                                    if (CCRZ.pagevars.pageConfig.isTrue('qo.g2c')) {
                                        cartDetails();
                                    }
                                } else {
                                    $('.quickOrderQtyWarn').show();
                                    $('.addToCart').show();
                                    $('.ccrz_loading_gif').hide();
                                }
                            });
                    } else {
                        CCRZ.pubSub.trigger("pageMessage", CCRZ.createPageMessage('WARN', "messagingSection-Warning", 'Invalid_Qty'));
                    }
                }

                QuickOrderView.addToCartOnEnterKey = function(event) {
                    var v = this;
                    if (window.event && window.event.keyCode == 13 || event.which == 13) {
                        v.addProductsToCart(event);
                        return false;
                    } else {
                        return true;
                    }
                }

                QuickOrderView.quantityEntryKey = function(event) {
                    var v = this;
                    if (window.event && window.event.keyCode == 13 || event.which == 13) {
                        v.addProductsToCart(event);
                        return false;
                    } else {
                        return CCRZ.util.isValidNumericInput(event); // todo, check qty field, not SKU
                    }
                }

                QuickOrderView.events["click .addToCart"] = "addProductsToCart";
                QuickOrderView.events["keypress .sku-input"] = "addToCartOnEnterKey";
                QuickOrderView.events["keypress .qty-input"] = "quantityEntryKey";
                QuickOrderView.delegateEvents();

                CCRZ.subsc.quickOrderRemoteActions.checkLockedCartAction(function (callback) {
                    if (callback.success) {
                        var lockedButton = callback.data["lockedCart"];
                        if (lockedButton) {
                            $('.btn-locked').attr('disabled','true');
                        }
                    } else {
                        console.error("callback -----> ", callback);
                    }
                });
            });
        });
    </script>
</apex:component>