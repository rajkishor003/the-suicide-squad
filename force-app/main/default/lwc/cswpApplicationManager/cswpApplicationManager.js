import { LightningElement } from 'lwc';
import cswpResources from '@salesforce/resourceUrl/cswp_resources';

export default class CswpApplicationManager extends LightningElement {
    iconApps = cswpResources + '/icons/icon_apps.svg';
    iconAcs = cswpResources + '/icons/icon_acs.png';
    iconAdv = cswpResources + '/icons/icon_adv.png';
}