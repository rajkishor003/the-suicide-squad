import { LightningElement, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import approveWithEmail from '@salesforce/apex/CSWPApprovalProcessController.ApproveFileWithFromWithCategory'
import { showSuccessToast } from "c/cswpUtil";


export default class CswpApproveWithEmailManager extends NavigationMixin(LightningElement) {

    currentPageReference = null; 
    urlStateParameters = null;

    /* Params from Url */
    urlId = null;
    urlARID = null;
    urlCategory = null;

    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
       if (currentPageReference) {
          this.urlStateParameters = currentPageReference.state;
          console.log(this.urlStateParameters);
          this.setParametersBasedOnUrl();
       }
    }
 
    setParametersBasedOnUrl() {
        this.urlId = this.urlStateParameters.recId || null;
        this.urlARID = this.urlStateParameters.ARID || null;
        this.urlCategory = this.urlStateParameters.cat || null;;
        console.log('this.urlId==> '+this.urlId);
        console.log('this.urlARID==> '+this.urlARID);
        console.log('this.urlCategory==> '+this.urlCategory);
       
        approveWithEmail({ recordId : this.urlId, category : this.urlCategory, arId : this.urlARID})
        .then(result => {
            console.log('result ==> '+result);
            let toastMessage = (this.urlCategory === 'Reject' ? 'Rejected' : 'Approved');
            showSuccessToast(this,{
                title: 'Success',
                message: 'Record '+toastMessage+' Successfully' // to be custom label later on.
            });

            this[NavigationMixin.Navigate]({
                type: 'comm__namedPage',
                attributes: {
                    name: 'Home'
                }
            });

        })
        .catch(error => {
           console.log(error);
        });
    }
}