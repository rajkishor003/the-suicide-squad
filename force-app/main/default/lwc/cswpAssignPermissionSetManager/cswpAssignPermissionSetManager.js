import { LightningElement, wire, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
// import getPermissionSets from '@salesforce/apex/CSWP_AssignPermissionSetController.getPermissionSets';
import getPermissionSetList from '@salesforce/apex/CSWP_AssignPermissionSetController.getPermissionSetList';
import getPermissionSetListAfterSave from '@salesforce/apex/CSWP_AssignPermissionSetController.getPermissionSetListAfterSave';
import assignPermissionSet from '@salesforce/apex/CSWP_AssignPermissionSetController.assignPermissionSet';

import NameLabel from '@salesforce/label/c.cswp_label_name';

export default class cswpAssignPermissionSetManager extends NavigationMixin(LightningElement) {
    @api cswpGroupId;
    @track selectedPermissionSets;
    // @track options = [];
    @track tableData;
    // @api assignedId;
    recordPageUrl;

    label = {
        NameLabel
    }

    columns = [
        { label: this.label.NameLabel, fieldName: 'Label', sortable:'true', type: 'Text'}
    ];

    // @wire(getPermissionSets, {cswpGroupId : '$cswpGroupId'})
    // wiredSets({error,data}) {
    //     if (data) {
    //         for(const ps of data) {
    //             const option = {
    //                 label: ps.psName,
    //                 value: ps.psId
    //             };
    //             this.options = [ ...this.options, option];
    //         }
    //     } else if (error) {
    //         console.error("error ----> ", error);
    //     }
    // }

    @wire(getPermissionSetList, {cswpGroupId : '$cswpGroupId'})
    wiredSetList({error,data}) {
        if (data) {
            this.tableData = data;
        } else if (error) {
            console.error("error ----> ", error);
        }
    }

    // handleChange(event) {
    //     this.selectedPermissionSet = event.detail.value;        
    // }

    getSelectedPermissionSets(event) {
        this.selectedPermissionSets = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log('Selected Permission Sets ==> '+this.selectedPermissionSets);
    }

    assignPermissioSet() {
        assignPermissionSet({
            selectedPermissionSets: JSON.stringify(this.selectedPermissionSets),
            cswpGroupId: this.cswpGroupId
        })
        .then(assignedObjectId => {
            if (assignedObjectId) {
                // this.assignedId = assignedObjectId;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'SUCCESS',
                        variant: 'success',
                        message: 'Permission Set Successfully Assigned to Public Group Members',
                    }),
                );
                getPermissionSetListAfterSave({ cswpGroupId : this.cswpGroupId, apsList : assignedObjectId })
                .then(result => {
                    this.tableData = result;
                    this.selectedPermissionSets = null;
                    this.template.querySelector('lightning-datatable').selectedRows=[];
                })
                .catch(error => {
                    console.log(error);
                });
            }
        }).catch(error => {
            console.log('error ----> ', error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'ERROR',
                    variant: 'error',
                    message: 'An error occured',
                }),
            );
        });
    }

    // navigateToRecordDetailPage() {
    //     this[NavigationMixin.Navigate]({
    //         type: 'standard_recordPage',
    //         attributes: {
    //             recordId: this.assignedId,
    //             objectApiName: 'CSWP_Assigned_Permission_Set__c',
    //             actionName: 'view'
    //         }
    //     });
    // }
}