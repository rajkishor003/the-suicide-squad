import { LightningElement, wire } from 'lwc';
import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import cswpResources from '@salesforce/resourceUrl/cswp_resources';
import getCurrentTicket from '@salesforce/apex/CswpTicketManagerController.getTicketDetail';

export default class CswpBreadcrumb extends NavigationMixin(LightningElement) {
    myAdvantest = cswpResources + '/logos/logo__myAdvantest.svg';

    @wire (CurrentPageReference)
    currentPage;

    ticketSubject = '';

    @wire(getCurrentTicket , {currentCaseId : '$recordId'})
    getSubject({error, data}) {
        if (data) {
            this.ticketSubject = data.Subject;
        } else if (error) {
            console.error('error ----> ', error);
        }
    }

    previousPage = {
        'name': '',
        'recordName': '',
        'urlName': '',
        'urlAlias': '',
        'recordId': '',
    }

    sfNamedPages = [
        'Home',
        'Topic_Catalog'
    ]

    name = '';
    recordName = '';
    urlName = '';
    urlAlias = '';
    recordId = '';
    profilePageLabel = '';
    urlPath = [];
    showRecords = false;
    showBreadcrumb = false;
    isProfilePage = false;
    isTicketDetailPage = false;

    manageLabels (label) {
        if (label) {
           label = label.replace(/__c/g, '').replace(/-|_/g,' ');
        }

        if (label && label.length > 30) {
            label = label.substr(0, 27) + '...';
        }

        return label;
    }

    manageLevels () {
        this.name = this.manageLabels (this.currentPage.attributes.name);
        this.recordName = this.manageLabels (this.currentPage.state.recordName);
        this.urlName = this.manageLabels (this.currentPage.attributes.urlName);
        this.urlAlias = this.manageLabels (this.currentPage.attributes.urlAlias);
        this.recordId = this.currentPage.attributes.recordId;
        this.urlPath = (new URL(document.location)).pathname.split('/');

        if (this.name && this.name !== 'Home') {
            this.previousPage.name = this.name;
        }

        if (this.recordName) {
            this.previousPage.recordName = this.recordName;
        }

        if (this.recordId) {
            this.previousPage.recordId = this.recordId;
        }

        if (this.currentPage.type !== 'comm__namedPage') {
            this.showRecords = true;
        } else {
            this.showRecords = false;
        }

        if (this.urlPath.includes('profile') || this.urlPath.includes('settings')) {
            this.isProfilePage = true;
            this.profilePageLabel = 'my ' + this.urlPath[3];
        } else if (this.urlPath.includes('detail')) {
            this.isProfilePage = true;
            this.profilePageLabel = 'My Account';
        } else {
            this.isProfilePage = false;
        }

        if (this.urlPath.includes('case')) {
            this.isTicketDetailPage = true;
            this.previousPage.name = 'AE Consultancy';
        } else {
            this.isTicketDetailPage = false;
            this.ticketSubject = '';
        }

        this.manageHistory (this.currentPage);
    }

    manageHistory (currentPage) {
        if (this.name === 'Home') {
            this.previousPage.name = '';
            this.previousPage.recordName = '';
        }

        if (this.urlAlias) {
            this.previousPage.recordName = '';
        }

        if (currentPage.type === 'standard__recordPage' && this.previousPage.name === 'News') {
            this.previousPage.name = '';
        }
    }

    navigateToPage (event) {

        var pageReference = {};
        var level = parseInt(event.target.dataset.level); // eslint-disable-line radix
        var name = event.target.name;
        var recordId = this.previousPage.recordId;

        event.preventDefault();

        switch (level) {
            case 0:
            case 1:
                name = name.replace(/ /g,'_');
                name = this.sfNamedPages.includes(name) ? name : name + '__c';
                pageReference = {
                    type: 'comm__namedPage',
                    attributes: {
                        name: name
                    }
                };
                break;
            case 2:
                pageReference = {
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: recordId,
                        actionName: 'view'
                    }
                };
                break;
            //No Default
        }

        this[NavigationMixin.GenerateUrl](pageReference)
        .then(this[NavigationMixin.Navigate](pageReference));
    }

    renderedCallback () {
        if (this.currentPage.attributes.name === 'Home') {
            this.showBreadcrumb = false;
        } else {
            this.showBreadcrumb = true
        }
        this.manageLevels();
    }
}