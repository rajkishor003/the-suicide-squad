/* eslint-disable */
import { LightningElement, api, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { NavigationMixin } from 'lightning/navigation';

//templates
import homeTemplate from './cswpBreadcrumbManager.html';
import mainTemplate from './cswpBreadcrumbManagerMain.html';
import faqTemplate from './cswpBreadcrumbManagerFAQ.html';
import caseTemplate from './cswpBreadcrumbManagerCase.html';

//logos
import cswpResources from '@salesforce/resourceUrl/cswp_resources';
import ae_consultancy from '@salesforce/label/c.cswp_ae_consultancy';
import home from '@salesforce/label/c.cswp_home';
import topic_catalog from '@salesforce/label/c.cswp_topic_catalog';
import cswp_action_search from '@salesforce/label/c.cswp_action_search';

export default class CswpBreadcrumbManager extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference)
    pageRef;
    @api pageName = '';
    @api pageDisplay = '';
    @api recordName = '';
    @api urlName = '';
    @api topicRecordId = '';
    @track urlNameValid = false;

    myAdvantest = cswpResources + '/logos/logo__myAdvantest.svg';

    label = {
        ae_consultancy,
        home,
        topic_catalog,
        cswp_action_search
    }
    render() {
        this.pageName = this.pageRef.attributes.name;
        console.log(this.pageRef);
        if (this.pageName) {
            if(this.pageName == 'Home') {
                this.modifyPageName(this.pageName)
                return homeTemplate;
            } else {
                this.modifyPageName(this.pageName)
                return mainTemplate;
            }
        } else if (this.pageRef.state.recordName) {
            this.recordName = this.pageRef.state.recordName;
            this.topicRecordId = this.pageRef.attributes.recordId;
            console.log('record id : ', this.topicRecordId);
            this.urlName = '';
            this.urlNameValid = false;
            this.modifyRecordName(this.recordName);
            if(this.topicRecordId.startsWith('500')) {
                return caseTemplate
            } else {
                return faqTemplate;
            }
        } else if (this.pageRef.attributes.urlName) {
            this.urlName = this.pageRef.attributes.urlName;
            this.urlNameValid = true;
            this.modifyUrlName(this.urlName)
            return faqTemplate;
        } else if (this.pageRef.type == 'standard__search') {
            this.modifyPageName(this.label.cswp_action_search);
            return mainTemplate;
        } else {
            return mainTemplate;
        }
    }

    modifyPageName(pageName) {
        if(pageName == 'AE_Consultancy__c') {
            this.pageDisplay =  this.label.ae_consultancy;
            return this.pageDisplay;
        }
        else {
            this.pageDisplay = pageName.replace(/__c/g, "").replace(/_/g, " ");
            return this.pageDisplay;
        }
    }

    modifyRecordName(recordName) {
        this.recordName = recordName.replace(/-/g, " ");
        return this.recordName;
    }

    modifyUrlName(urlName) {
        console.log('modifying');
        console.log(urlName);
        this.urlName = urlName.replace(/-/g, " ");
        var desiredLength = 30;
        if(this.urlName.length > desiredLength) {
            return this.urlName = this.urlName.substr(0, desiredLength-1) + '...';
        } else {
            return this.urlName;
        }
    }

    goPage(event) {
        var pageName = event.currentTarget.dataset.name;
        console.log(pageName);
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: pageName
            }
        });
    }

    goTopicPage(event) {
        if(this.topicRecordId) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.topicRecordId,
                    actionName: 'view'
                }
            });
        }
    }
}