import { LightningElement } from 'lwc';

export default class CswpCalBoxManager extends LightningElement {

    uploadIcon;
    searchIcon;
    connectedCallback() {
        this.uploadIcon = "utility:download";
        this.searchIcon = "utility:edit";
    }

}