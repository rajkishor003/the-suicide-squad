import { LightningElement, track, wire, api } from 'lwc';

import getSystemData from '@salesforce/apex/Cswp_SystemSelector.getSystemData';
import getSystemColumns from '@salesforce/apex/Cswp_SystemSelector.getSystemColumns';
import StoreCertificate from '@salesforce/apex/Cswp_CalBoxFileUploadController.StoreCertificate';
import GetContentDocumentWithVersions from '@salesforce/apex/CSWPSobjectSelector.GetContentDocumentWithVersions';
import getResourcesWithSystemID from '@salesforce/apex/CSWPSobjectSelector.getResourcesWithSystemID'; 
import getLatestCertificateForCalBoxData from '@salesforce/apex/CSWPSobjectSelector.getLatestCertificateForCalBoxData';
import getBaseURL from '@salesforce/apex/CSWPUtil.getBaseURL';
import getUser from '@salesforce/apex/CSWPSobjectSelector.getUser'
import { logActivity } from "c/cswpUtil";

import InstrumentLabel from '@salesforce/label/c.Cswp_Instrument_Label';
import SystemLabel from '@salesforce/label/c.Cswp_System_Label';
import ToCalibrateLabel from '@salesforce/label/c.Cswp_Cal_Box_To_Calibrate';
import ValidLabel from '@salesforce/label/c.Cswp_Cal_Box_Valid';
import OverdueLabel from '@salesforce/label/c.Cswp_Cal_Box_Overdue';
import GenerateCertificateLabel from '@salesforce/label/c.Cswp_Generate_Certification';
import LanguageEnglish from '@salesforce/label/c.Cswp_English';
import LanguageJapanese from '@salesforce/label/c.Cswp_Japanese';
import LanguageChinese from '@salesforce/label/c.Cswp_Chinese';
import LanguageChineseSimplified from '@salesforce/label/c.Cswp_ChineseSimplified';
import LanguageKorean from '@salesforce/label/c.Cswp_Korean';
import Language from '@salesforce/label/c.Cswp_Language';
import SelectLanguage from '@salesforce/label/c.Cswp_Select_Language';
import cswp_calboxmanager_title from '@salesforce/label/c.cswp_calboxmanager_title';
import cswp_calboxmanager_paper from '@salesforce/label/c.cswp_calboxmanager_paper';
import cswp_calboxmanager_paper_1 from '@salesforce/label/c.cswp_calboxmanager_paper_1';
import cswp_calboxmanager_systemsn from '@salesforce/label/c.cswp_calboxmanager_systemsn';
import cswp_calboxmanager_cert from '@salesforce/label/c.cswp_calboxmanager_cert';
import cswp_calbox_cancel_text from '@salesforce/label/c.cswp_calbox_cancel_text';
import cswp_calboxmanager_content_1 from '@salesforce/label/c.cswp_calboxmanager_content_1';
import cswp_calboxmanager_content_2 from '@salesforce/label/c.cswp_calboxmanager_content_2';
import cswp_calboxmanager_caldate from '@salesforce/label/c.cswp_calboxmanager_caldate';
import cswp_calboxmanager_calduedate from '@salesforce/label/c.cswp_calboxmanager_calduedate';
import cswp_calboxmanager_calstatus from '@salesforce/label/c.cswp_calboxmanager_calstatus';
import cswp_calboxmanager_operator from '@salesforce/label/c.cswp_calboxmanager_operator';
import cswp_calboxmanager_cert_2 from '@salesforce/label/c.cswp_calboxmanager_cert_2';
import cswp_calboxmanager_info_1 from '@salesforce/label/c.cswp_calboxmanager_info_1';
import cswp_calboxmanager_info_2 from '@salesforce/label/c.cswp_calboxmanager_info_2';
import cswp_calboxmanager_info_3 from '@salesforce/label/c.cswp_calboxmanager_info_3';
import cswp_calboxmanager_info_4 from '@salesforce/label/c.cswp_calboxmanager_info_4';
import cswp_calboxmanager_resources from '@salesforce/label/c.cswp_calboxmanager_resources';
import cswp_not_generated from '@salesforce/label/c.cswp_not_generated';
import Cswp_Customer_Name from '@salesforce/label/c.Cswp_Customer_Name';
import Cswp_As_Delivered_Condition from '@salesforce/label/c.Cswp_As_Delivered_Condition';
import cswp_calboxtable_close from '@salesforce/label/c.cswp_calboxtable_close';
import FullCalibrationResult from '@salesforce/label/c.cswp_Include_Full_Calibration_Result';

import SelectSize from '@salesforce/label/c.Cswp_Select_Size';
import Size from '@salesforce/label/c.Cswp_Size';
import A4 from '@salesforce/label/c.Cswp_A4';
import Letter from '@salesforce/label/c.Cswp_Letter';
import Html from '@salesforce/label/c.Cswp_Html';

import Id from '@salesforce/user/Id';
export default class cswpCalBoxManagerV2 extends LightningElement {
    @api keyword = "";    
    @api needsRendering;

    //Advanced Search Parameters
    @api snSearchParam = "";
    @api calDateStartParam = "";
    @api calDateEndParam = "";
    @api calDueDateStartParam = "";
    @api calDueDateEndParam = "";
    @api calStatusParam = "";
    @api operatorNameParam = "";
    
    @track selectedLanguage = "en_US";
    @track selectedSize = "A4";
    @track ContentDocumentLinkID;
    @track calBoxDataID;
    @track IncludeFullCalibrationResults = true;

    @track loggedInUserId = Id;
    @track loggedInUser;
    @track certificateTemplate = "";
    advancedFilterApplied = false;
    label = {
        SystemLabel,
        InstrumentLabel,
        GenerateCertificateLabel,
        LanguageEnglish,
        LanguageJapanese,
        LanguageChinese,
        LanguageChineseSimplified,
        LanguageKorean,
        Language,
        SelectLanguage,
        cswp_calboxmanager_title,
        cswp_calboxmanager_paper,
        cswp_calboxmanager_paper_1,
        cswp_calboxmanager_systemsn,
        cswp_calboxmanager_cert,
        cswp_calbox_cancel_text,
        cswp_calboxmanager_content_1,
        cswp_calboxmanager_content_2,
        cswp_calboxmanager_caldate,
        cswp_calboxmanager_calduedate,
        cswp_calboxmanager_calstatus,
        cswp_calboxmanager_operator,
        cswp_calboxmanager_cert_2,
        cswp_calboxmanager_info_1,
        cswp_calboxmanager_info_2,
        cswp_calboxmanager_info_3,
        cswp_calboxmanager_info_4,
        cswp_calboxmanager_resources,
        SelectSize,
        Size,
        A4,
        Letter,
        Html,
        cswp_not_generated,
        Cswp_Customer_Name,
        Cswp_As_Delivered_Condition,
        cswp_calboxtable_close,
        FullCalibrationResult
    };

    get languageOptions() {
        return [
            { label: LanguageEnglish, value: "en_US" },
            { label: LanguageJapanese, value: "ja_JP" },
            { label: LanguageKorean, value: "ko_KR"},
            { label: LanguageChinese, value: "zh_TW"},
            { label: LanguageChineseSimplified, value: "zh_CN"}
        ];
    }

    get sizeOptions() {
        return [
            { label: A4, value: "A4" },
            { label: Letter, value: "Letter" },
            { label: Html, value: "HTML" }
        ];
    }

    handleLanguageChange(event) {
        this.selectedLanguage = event.detail.value;
    }

    handleSizeChange(event) {
        this.selectedSize = event.detail.value;
    }

    generateBtnClicked(event) {
        if(this.selectedSize === 'A4') {
            //window.open(this.baseURL+"/apex/cswpCalBoxCertificate?linkID="+this.ContentDocumentLinkID+'&lang='+this.selectedLanguage+'&fullRes='+this.IncludeFullCalibrationResults+'&calBoxDataID='+this.calBoxDataID, '_blank');
            window.open("/cswp/cswpCalBox"+this.certificateTemplate+"Certificate?linkID="+this.ContentDocumentLinkID+'&lang='+this.selectedLanguage+'&fullRes='+this.IncludeFullCalibrationResults+'&calBoxDataID='+this.calBoxDataID, '_blank');
        }
        else if(this.selectedSize === 'HTML') {
            window.open("/cswp/cswpCalBoxCertificateHtml?linkID="+this.ContentDocumentLinkID+'&lang='+this.selectedLanguage+'&fullRes='+this.IncludeFullCalibrationResults+'&calBoxDataID='+this.calBoxDataID, '_blank');
        }
        else {
            window.open("/cswp/cswpCalBox"+this.certificateTemplate+"CertificateLetter?linkID="+this.ContentDocumentLinkID+'&lang='+this.selectedLanguage+'&fullRes='+this.IncludeFullCalibrationResults+'&calBoxDataID='+this.calBoxDataID, '_blank');
        }
        
        // TODO storeCertificate Method
        StoreCertificate({linkID : this.ContentDocumentLinkID , pageLanguage: this.selectedLanguage, entityID : this.calBoxDataID, fullRes : this.IncludeFullCalibrationResults, calBoxDataId : this.calBoxDataID, certTemplate : this.certificateTemplate })
        .then(result => {     
            logActivity({
                id: this.calBoxDataID,
                name: "Certificate-"+this.calBoxDataID
            },
            "generate",
            "Certificate " + this.ContentDocumentLinkID + " generated ");
        })
        .catch(error => {
            this.error = error;
            this.toastTitle = 'Error';
            this.toastMessage = error.body.message
            const toastEvent = new ShowToastEvent({
                "variant": this.toastTitle,
                "title": this.toastTitle,
                "message": this.toastMessage
            });
            this.dispatchEvent(toastEvent);
        });
    }

    searchFieldsMap =  [{"label": this.label.SystemLabel, "field": "Cswp_Serial_Number__c"},
                        {"label": this.label.InstrumentLabel, "field": "Cswp_Serial_Number__c"}];

    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;
    @track wrapperArray = [];
    @track sysWrapperArr = [];
    indexMap = {};
    filterMap = [];
    advancedFilteredData = [];
    distinctdata = [];
    cd_cv_Map = [];
    baseURL = '';

    @track data = []; //data to be displayed in the table
    @track systemData = []; //data to be displayed in the Modal-Table
    @track calBoxData = []; //data to be displayed in the Modal-Table
    @track alldata = []; //data to be stored
    @track columns = []; //data to be displayed in the table
    @track pageSize = 10; //default value we are assigning
    
    @wire(getBaseURL)
    getBaseURL({ error ,data }){
        if(data) {
            this.baseURL = data;
        }
        else if(error) {
            this.error = error;
        }
    }

    @wire(getSystemColumns)
    wiredSystemColumns({ error, data }) {
        if (data) {
            this.indexMap[this.label.SystemLabel] = this.wrapperArray.length;
            this.wrapperArray.push({key:this.label.SystemLabel, value: new datatableWrapper(1, 1, 0, 0, 1, null, null, data, null)});
        } else if (error) {
            this.error = error;
            this.SystemColumns = undefined;
        }
    }

    @wire(getSystemData)
    wiredSystemData({ error, data }) {
        if (data) { 
            let arrIdx = this.indexMap[this.label.SystemLabel];
            this.wrapperArray[arrIdx].value.totalRecordCount  = data.length;
            this.wrapperArray[arrIdx].value.totalPage         = Math.ceil(data.length / this.pageSize);
            this.wrapperArray[arrIdx].value.totalData         = data;
            this.wrapperArray[arrIdx].value.tempTotalData     = data;
            this.wrapperArray[arrIdx].value.pageData          = data.slice(0,this.pageSize);
            this.wrapperArray[arrIdx].value.endingRecord      = this.pageSize;
            this.wrapperArray = [...this.wrapperArray];

            getUser({ userID: this.loggedInUserId})
                .then((userResult) => {
                    this.loggedInUser = userResult;
                    //this.loggedInUser.Profile.Name = 'FSE';
                    if(this.loggedInUser.Profile.Name.includes('FSE')) {this.certificateTemplate = 'FSE';}
                    if(this.loggedInUser.Profile.Name.includes('Factory')) {this.certificateTemplate = 'Factory';}
                    if(this.loggedInUser.Profile.Name.includes('Community')) {this.certificateTemplate = 'Customer';}
                    getLatestCertificateForCalBoxData({ calBoxDataIds: data.map(x=> x.Cswp_Calibration_Box_Data__c) })
                    .then((result) => {
                        this.error = undefined;
                        this.cd_cv_Map = result;
                        for (let i = 0; i < data.length; i++) {
                            var row = data[i];
                            var sw = new systemWrapper(row.Cswp_Serial_Number__c, null, null, "Not Ready", null, null, null, null, null, null, null, null, null);
                            if (row.Cswp_Calibration_Box_Data__c&&row.Cswp_Calibration_Box_Data__r.Cswp_Is_Archived__c === false) {
                                //row.Cswp_Calibration_Box_Data__r.Cswp_Sync_With_Install_Base__c == true condition removed
                                // str1 format should be dd/mm/yyyy. Separator can be anything e.g. / or -. It wont effect
                                var year   = row.Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c.substring(0,4);
                                var month  = row.Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c.substring(5,7);
                                var dt     = row.Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c.substring(8,10);
        
                                sw.CalibrationDate      = year+ "-" +month+ "-" + dt;
                                sw.CalibrationDateTime  = row.Cswp_Calibration_Box_Data__r.Cswp_Generation_Date__c;
                                sw.Temperature          = row.Cswp_Calibration_Box_Data__r.Cswp_Temperature__c;
                                sw.Humidity             = row.Cswp_Calibration_Box_Data__r.Cswp_Humidity__c;
                                sw.AsDeliveredCondition = row.Cswp_As_Delivered_Condition__c;
                                sw.CertificateVersion   = row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Version__c;
                                sw.Certificate          = this.cd_cv_Map[row.Cswp_Calibration_Box_Data__c] === undefined ? "" : this.baseURL+"/sfc/servlet.shepherd/version/download/"+this.cd_cv_Map[row.Cswp_Calibration_Box_Data__c];
                                sw.CertificateLabel     = this.cd_cv_Map[row.Cswp_Calibration_Box_Data__c] === undefined ? this.label.cswp_not_generated : this.label.cswp_calboxmanager_cert_2;
                                sw.CalBoxDataId         = row.Cswp_Calibration_Box_Data__c;
                                sw.Disabled             = (!(row.Cswp_Calibration_Box_Data__r.Cswp_Sync_With_Install_Base__c)) ||
                                                          (row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c === undefined) ||
                                                          ((row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c !== undefined) && (!row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c.includes(this.certificateTemplate)));

                                //sw.Disabled             = (row.Cswp_Calibration_Box_Data__r.Cswp_CheckSum_Valid__c === false) || //Checksum Needs to be valid for all types
                                //                          (this.certificateTemplate === 'FSE' && (row.Cswp_Calibration_Box_Data__r.Cswp_Validation_OIB_System__c === false)) || //Only Sn validation need to be checked when user is FSE type
                                //                          (this.certificateTemplate === 'Customer' && (row.Cswp_Calibration_Box_Data__r.Cswp_Validation_OIB_System__c === false||row.Cswp_Calibration_Box_Data__r.Cswp_Validation_OIB_Account__c === false)) || // Sn and account validation need to be checked when user is Customer type
                                //                          ((!this.loggedInUser.Profile.Name.includes('Admin')) && (row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c === undefined)) || 
                                //                          ((!this.loggedInUser.Profile.Name.includes('Admin')) && (row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c !== undefined) && (!row.Cswp_Calibration_Box_Data__r.Cswp_Certificate_Template_Picklist__c.includes(this.certificateTemplate)));
        
                                var Dueyear   = row.Cswp_Calibration_Box_Data__r.Cswp_Calibration_Due_Date__c.substring(0,4);
                                var Duemonth  = row.Cswp_Calibration_Box_Data__r.Cswp_Calibration_Due_Date__c.substring(5,7);
                                var Duedt     = row.Cswp_Calibration_Box_Data__r.Cswp_Calibration_Due_Date__c.substring(8,10);
        
                                sw.CalibrationDueDate = Dueyear+ "-" +Duemonth+ "-" + Duedt;
                                sw.OperatorName    = row.Cswp_Calibration_Box_Data__r.Cswp_Operator_Name__c;
            
                                if(row.Cswp_Calibration_Box_Data__r.Cswp_Days_Left_To_Calibration_Expire__c > 15) {
                                    sw.CalibrationStatus = ValidLabel; sw.StatusClass = "c-valid";
                                }
                                else if(row.Cswp_Calibration_Box_Data__r.Cswp_Days_Left_To_Calibration_Expire__c > 0 && row.Cswp_Calibration_Box_Data__r.Cswp_Days_Left_To_Calibration_Expire__c < 15) {
                                    sw.CalibrationStatus = ToCalibrateLabel; sw.StatusClass = "c-calibrate";
                                }
                                else {                            
                                    sw.CalibrationStatus = OverdueLabel; sw.StatusClass = "c-overdue";
                                }                        
                                this.sysWrapperArr.push(sw);
                            }
                        }
                        this.alldata = this.sysWrapperArr;
                        this.data = this.getUnique(this.alldata);
                        this.distinctdata = this.data;
                        this.advancedFilteredData = this.data;
                    })
                    .catch((error) => {
                        this.error = error;
                        console.log(error);
                    });
                })
                .catch((error) => {
                    this.error = error;
                    console.log(error);
                });           
        } else if (error) {
            this.error = error;
        }
    }

    getUnique(array) {
        var unique = [];
        var distinct = [];
        for( let i = 0; i < array.length; i++ ){          
            if(!unique[array[i].SerialNumber]){
                distinct.push(array[i]);
                unique[array[i].SerialNumber] = 1;
            }
            else if(new Date(array[i].CalibrationDate)>new Date(distinct.filter(x=> x.SerialNumber === array[i].SerialNumber)[0].CalibrationDate)) {
                let pos = distinct.map(function(e) { return e.SerialNumber; }).indexOf(array[i].SerialNumber);
                distinct[pos] = array[i];
            }
        }
        return distinct;
    }

    columns = [
        { label: this.label.cswp_calboxmanager_systemsn, fieldName: 'systemsn', sortable:'true', type: 'customTypeSystemSn', typeAttributes: { systemSn: { fieldName: 'SerialNumber' } } },
        { label: this.label.cswp_calboxmanager_caldate, fieldName: 'CalibrationDate', sortable:'true', type:'Date'},
        { label: this.label.cswp_calboxmanager_calduedate, fieldName: 'CalibrationDueDate', sortable:'true', type:'Date'},
        { label: this.label.cswp_calboxmanager_calstatus, fieldName: 'CalibrationStatus', cellAttributes: { class: { fieldName: 'StatusClass' } } },
        { label: this.label.cswp_calboxmanager_operator, fieldName: 'OperatorName', sortable:'true', type:'Text'},
        { label: this.label.cswp_calboxmanager_cert_2, fieldName: 'calboxdataid', type: 'customTypeDownload', typeAttributes: { myta: { fieldName: 'CalBoxDataId' }, my2ta: { fieldName: 'Disabled' } }} 
    ];

    // CERTIFICATE
    @track isDownloadModalOpen = false;
    openDownloadModal() {
        this.isDownloadModalOpen = true;
    }

    closeDownloadModal() {
        this.isDownloadModalOpen = false;
    }

    handleCustomDownload(event) {
        this.calBoxDataID = event.detail.recordId;
        GetContentDocumentWithVersions({ calBoxDataID: this.calBoxDataID, type : 'Data File' })
        .then((result) => {
            console.log('result ==> '+result);
            this.ContentDocumentLinkID = result[0].Id;
            this.error = undefined;
        })
        .catch((error) => {
            this.error = error;
            console.log('error ==> '+error);
        });
        this.openDownloadModal();
    }
    
    // SYSTEM SN
    @track isSystemSnModalOpen = false;
    openSystemSnModal() {
        this.isSystemSnModalOpen = true;
    }

    closeSystemSnModal() {
        this.isSystemSnModalOpen = false;
    }

    @track modalSn = "";
    @track modalCalDate = "";
    @track modalCalDateTime = "";
    @track modalTemperature = "";
    @track modalHumidity = "";
    @track resourceData = [];

    handleCustomSystemSn(event) {
        this.modalSn = event.detail.recordId;
        this.openSystemSnModal();
        this.modalData        = this.alldata.filter(x=> x.SerialNumber === this.modalSn).sort(this.dynamicSort("-CertificateVersion"));
        this.modalCalDate     = this.modalData[0].CalibrationDate;
        this.modalCalDateTime = this.modalData[0].CalibrationDateTime;
        this.modalTemperature = this.modalData[0].Temperature;
        this.modalHumidity    = this.modalData[0].Humidity;
        this.modalData        = [...this.modalData];
        
        getResourcesWithSystemID({serialNumber : this.modalSn})
        .then(result => {
            this.resourceData = result;
        })
        .catch(error => {
            this.error = error;
        });
    }

    dynamicSort(property) {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            /* next line works with strings and numbers, 
             * and you may want to customize it to your needs
             */
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }

    renderedCallback() {

        if (this.needsRendering) {

            this.filterMap = [];
            if(this.snSearchParam != undefined && this.snSearchParam.length != "") {
                this.filterMap.push({label:"SerialNumber", value:this.snSearchParam, fieldType:"text"});
            }
            if(this.calDateStartParam != undefined && this.calDateStartParam.length != "") {
                this.filterMap.push({label:"CalibrationDate", value:this.calDateStartParam, fieldType:"date", operator:"gt"});
            }
            if(this.calDateEndParam != undefined && this.calDateEndParam.length != "") {
                this.filterMap.push({label:"CalibrationDate", value:this.calDateEndParam, fieldType:"date", operator:"lt"});
            }
            if(this.calDueDateStartParam != undefined && this.calDueDateStartParam.length != "") {
                this.filterMap.push({label:"CalibrationDueDate", value:this.calDueDateStartParam, fieldType:"date", operator:"gt"});
            }
            if(this.calDueDateEndParam != undefined && this.calDueDateEndParam.length != "") {
                this.filterMap.push({label:"CalibrationDueDate", value:this.calDueDateEndParam, fieldType:"date", operator:"lt"});
            }
            if(this.calStatusParam != undefined && this.calStatusParam.length != "") {
                this.filterMap.push({label:"CalibrationStatus", value:this.calStatusParam, fieldType:"text"});
            }
            if(this.operatorNameParam != undefined && this.operatorNameParam.length != "") {
                this.filterMap.push({label:"OperatorName", value:this.operatorNameParam, fieldType:"text"});
            }
            
            //Restore Data if criterias are cleared
            if(this.filterMap.length == 0) {
                this.advancedFilterApplied = false;
            }

            //apply advanced filter
            this.advancedFilteredData = this.distinctdata;
            for(let i=0; i<this.filterMap.length; i++) {
                this.advancedFilterApplied = true;
                let row = this.filterMap[i];
                if(row.fieldType == "text") {
                    this.advancedFilteredData = this.advancedFilteredData.filter(x=> x[row.label].toLowerCase() == row.value.toLowerCase());
                } else if(row.fieldType == "date") {
                    if(row.operator == "gt") {
                        this.advancedFilteredData = this.advancedFilteredData.filter(x=> new Date(x[row.label]) > new Date(row.value));
                    } else if(row.operator == "lt") {
                        this.advancedFilteredData = this.advancedFilteredData.filter(x=> new Date(x[row.label]) < new Date(row.value));
                    }
                }
            }
            if(this.advancedFilterApplied) { this.data = this.advancedFilteredData; }
            else {
                if(this.keyword !=undefined && this.keyword.length > 1) {
                    this.data = this.distinctdata.filter(x=> x.SerialNumber.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                        x.CalibrationDate.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                        x.CalibrationDueDate.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                        x.OperatorName.toLowerCase().includes(this.keyword.toLowerCase()));
                }
                else {
                    this.data = this.distinctdata;
                }
            }
            this.dispatchEvent(new CustomEvent('return'));
        }
    }

    // SAMPLE MODAL DATA
    modalData = [];
    

    modalColumns = [
        { label: this.label.cswp_calboxmanager_caldate, fieldName: 'CalibrationDate' },
        { label: this.label.cswp_calboxmanager_operator, fieldName: 'OperatorName' },
        { label: this.label.Cswp_As_Delivered_Condition, fieldName: 'AsDeliveredCondition' },
        { label: this.label.cswp_calboxmanager_cert_2, fieldName: 'Certificate', type: 'url', typeAttributes: {label: { fieldName: 'CertificateLabel' }, target: '_blank'}}
    ];
    handleFullCalibrationResultChange(event) {
        this.IncludeFullCalibrationResults = event.target.checked;
    }
}

class datatableWrapper {

    constructor(page, startingRecord, endingRecord, totalRecordCount, totalPage, totalData, pageData, columns, tempTotalData) {
        this.page             = page;
        this.startingRecord   = startingRecord;
        this.endingRecord     = endingRecord;
        this.totalRecordCount = totalRecordCount;
        this.totalPage        = totalPage;
        this.totalData        = totalData;
        this.pageData         = pageData;
        this.columns          = columns;
        this.tempTotalData    = tempTotalData;
    }
}

class systemWrapper {

    constructor(SerialNumber, CalibrationDate, CalibrationDueDate, CalibrationStatus, OperatorName, AsDeliveredCondition, CertificateVersion, CalibrationDateTime, Temperature, Humidity, Certificate, CertificateLabel, CalBoxDataId, StatusClass, Disabled) {
        this.SerialNumber         = SerialNumber;
        this.CalibrationDate      = CalibrationDate;
        this.CalibrationDueDate   = CalibrationDueDate;
        this.CalibrationStatus    = CalibrationStatus;
        this.OperatorName         = OperatorName;   
        this.AsDeliveredCondition = AsDeliveredCondition;
        this.CertificateVersion   = CertificateVersion;
        this.CalibrationDatetime  = CalibrationDateTime;
        this.Temperature          = Temperature;
        this.Humidity             = Humidity;
        this.Certificate          = Certificate;
        this.CertificateLabel     = CertificateLabel;
        this.CalBoxDataId         = CalBoxDataId;
        this.StatusClass          = StatusClass;
        this.Disabled             = Disabled;
    }
}