import { LightningElement, track, wire } from 'lwc';

import CalibrationBoxDataLabel from '@salesforce/label/c.Cswp_Calibration_Box_Data_Label';
import ServiceBoxLabel from '@salesforce/label/c.Cswp_Service_Box_Label';
import InstrumentLabel from '@salesforce/label/c.Cswp_Instrument_Label';
import SystemLabel from '@salesforce/label/c.Cswp_System_Label';
import TaskLabel from '@salesforce/label/c.Cswp_Task_Label';
import ResourceLabel from '@salesforce/label/c.Cswp_Resource_Label';
import SerialNumberLabel from '@salesforce/label/c.Cswp_Serial_Number';
import CalDateLabel from '@salesforce/label/c.Cswp_Calibration_Date';
import CalDueDateLabel from '@salesforce/label/c.Cswp_Calibration_Due_Date';
import CalStatusLabel from '@salesforce/label/c.Cswp_Status';
import OperatorNameLabel from '@salesforce/label/c.Cswp_Operator_Name';
import ToCalibrateLabel from '@salesforce/label/c.Cswp_Cal_Box_To_Calibrate';
import ValidLabel from '@salesforce/label/c.Cswp_Cal_Box_Valid';
import OverdueLabel from '@salesforce/label/c.Cswp_Cal_Box_Overdue';
import PleaseSelectLabel from '@salesforce/label/c.Cswp_Please_Select';

import getSystemData from '@salesforce/apex/Cswp_SystemSelector.getSystemData';
import getSystemColumns from '@salesforce/apex/Cswp_SystemSelector.getSystemColumns';
import cswp_calbox_search from '@salesforce/label/c.cswp_calbox_search';
import cswp_calbox_advanced_search from '@salesforce/label/c.cswp_calbox_advanced_search';
import cswp_calbox_status from '@salesforce/label/c.cswp_calbox_status';
import cswp_calbox_search_text from '@salesforce/label/c.cswp_calbox_search_text';
import cswp_calbox_cancel_text from '@salesforce/label/c.cswp_calbox_cancel_text';
import cswp_calboxtable_to from '@salesforce/label/c.cswp_calboxtable_to';
import cswp_calboxtable_close from '@salesforce/label/c.cswp_calboxtable_close';
import cswp_calboxtable_from from '@salesforce/label/c.cswp_calboxtable_from';
import cswp_calbox_iconmessage from '@salesforce/label/c.cswp_calbox_iconmessage';


export default class CcrzyCalibrationBoxSearch extends LightningElement {
    error;
    @track keyword;
    @track needsrendering = false;

    //Advanced Search Parameters
    snSearchParam = '';
    calDateStartParam = '';
    calDateEndParam = '';
    calDueDateStartParam = '';
    calDueDateEndParam = '';
    calStatusParam = '';
    operatorNameParam = '';

    label = {
        CalibrationBoxDataLabel,
        ServiceBoxLabel,
        InstrumentLabel,
        SystemLabel,
        TaskLabel,
        ResourceLabel,
        SerialNumberLabel,
        CalDateLabel,
        CalDueDateLabel,
        CalStatusLabel,
        OperatorNameLabel,
        ToCalibrateLabel,
        ValidLabel,
        OverdueLabel,
        PleaseSelectLabel,
        cswp_calbox_search,
        cswp_calbox_advanced_search,
        cswp_calbox_status,
        cswp_calbox_search_text,
        cswp_calbox_cancel_text,
        cswp_calboxtable_to,
        cswp_calboxtable_close,
        cswp_calboxtable_from,
        cswp_calbox_iconmessage
    };

    get statusOptions() {
        return [
            { label: ValidLabel, value: ValidLabel },
            { label: OverdueLabel, value: OverdueLabel },
            { label: ToCalibrateLabel, value: ToCalibrateLabel },
        ];
    }

    searchFieldsMap = [{"label": this.label.CalibrationBoxDataLabel, "field": "Cswp_Operator_Name__c"},
                       {"label": this.label.ServiceBoxLabel, "field": "Cswp_Serial_Number__c"},
                       {"label": this.label.InstrumentLabel, "field": "Cswp_Serial_Number__c"},
                       {"label": this.label.SystemLabel, "field": "Cswp_Serial_Number__c"},
                       {"label": this.label.TaskLabel, "field": "Name"},
                       {"label": this.label.ResourceLabel, "field": "Cswp_Serial_Number__c"}];

    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;
    @track wrapperArray = [];
    indexMap = {};
    @track selectedTab;
    @track data = []; //data to be displayed in the table
    @track pageSize = 5; //default value we are assigning

    @wire(getSystemColumns)
    wiredSystemColumns({ error, data }) {
        if (data) {
            this.indexMap[this.label.SystemLabel] = this.wrapperArray.length;
            this.wrapperArray.push({key:this.label.SystemLabel, value: new datatableWrapper(1, 1, 0, 0, 1, null, null, data, null)});
        } else if (error) {
            this.error = error;
            this.SystemColumns = undefined;
        }
    }

    @wire(getSystemData)
    wiredSystemData({ error, data }) {
        if (data) {
            let arrIdx = this.indexMap[this.label.SystemLabel];
            this.wrapperArray[arrIdx].value.totalRecordCount  = data.length;
            this.wrapperArray[arrIdx].value.totalPage         = Math.ceil(data.length / this.pageSize);
            this.wrapperArray[arrIdx].value.totalData         = data;
            this.wrapperArray[arrIdx].value.tempTotalData     = data;
            this.wrapperArray[arrIdx].value.pageData          = data.slice(0,this.pageSize);
            this.wrapperArray[arrIdx].value.endingRecord      = this.pageSize;
            this.wrapperArray = [...this.wrapperArray];
        } else if (error) {
            this.error = error;
        }
    }

    tabChanged(evt) {
        this.selectedTab =  evt.target.label; 
    }

    // Used to sort the 'Age' column
    sortBy(field, reverse, primer) {
        const key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        let arrIdx = this.indexMap[this.selectedTab];
        const { fieldName: sortedBy, sortDirection } = event.detail;
        const cloneData = [...this.wrapperArray[arrIdx].value.totalData];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.wrapperArray[arrIdx].value.totalData = cloneData;
        this.displayRecordPerPage(this.wrapperArray[arrIdx].value.page, arrIdx);
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    //clicking on previous button this method will be called
    /*previousHandler() {
        let arrIdx = this.indexMap[this.selectedTab];
        if (this.wrapperArray[arrIdx].value.page > 1) {
            this.wrapperArray[arrIdx].value.page = this.wrapperArray[arrIdx].value.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.wrapperArray[arrIdx].value.page, arrIdx);
        }
    }*/

    //clicking on next button this method will be called
    /*nextHandler() {
        let arrIdx = this.indexMap[this.selectedTab];
        if((this.wrapperArray[arrIdx].value.page<this.wrapperArray[arrIdx].value.totalPage) && this.wrapperArray[arrIdx].value.page !== this.wrapperArray[arrIdx].value.totalPage) {
            this.wrapperArray[arrIdx].value.page = this.wrapperArray[arrIdx].value.page + 1; //increase page by 1
            this.displayRecordPerPage(this.wrapperArray[arrIdx].value.page, arrIdx);     
        }
    }*/

    //this method displays records page by page
    displayRecordPerPage(page, arrIdx) {

        this.wrapperArray[arrIdx].value.startingRecord = ((page -1) * this.pageSize) ;
        this.wrapperArray[arrIdx].value.endingRecord   = (this.pageSize * page);

        this.wrapperArray[arrIdx].value.endingRecord   = (this.wrapperArray[arrIdx].value.endingRecord > this.wrapperArray[arrIdx].value.totalRecordCount) 
                            ? this.wrapperArray[arrIdx].value.totalRecordCount : this.wrapperArray[arrIdx].value.endingRecord; 

        this.wrapperArray[arrIdx].value.pageData       = this.wrapperArray[arrIdx].value.totalData.slice(this.wrapperArray[arrIdx].value.startingRecord, this.wrapperArray[arrIdx].value.endingRecord);

        this.wrapperArray[arrIdx].value.startingRecord = this.wrapperArray[arrIdx].value.startingRecord + 1;

        this.wrapperArray = [...this.wrapperArray];
    }

    setParentKeyword(event) {
        this.keyword = event.target.value;
        this.needsrendering = true;
    }

    returned() {
        this.needsrendering = false;
    }

    @track isAdvancedSearchModalOpen = false;
    openAdvancedSearchModal() {
        this.isAdvancedSearchModalOpen = true;
    }

    operateAdvancedSearchModal() {
        this.needsrendering = true;
        this.isAdvancedSearchModalOpen = false;
    }

    closeAdvancedSearchModal() {
        this.isAdvancedSearchModalOpen = false;
    }

    snParamChanged(event) {
        this.snSearchParam = event.target.value;
    }
    calDateStartParamChanged(event) {
        this.calDateStartParam = event.target.value;
    }
    calDateEndParamChanged(event) {
        this.calDateEndParam = event.target.value;
    }
    calDueDateStartParamChanged(event) {
        this.calDueDateStartParam = event.target.value;
    }
    calDueDateEndParamChanged(event) {
        this.calDueDateEndParam = event.target.value;
    }
    calStatusParamChanged(event) {
        this.calStatusParam = event.target.value;
    }
    operatorNameParamChanged(event) {
        this.operatorNameParam = event.target.value;
    }
}

class datatableWrapper  {

    constructor(page, startingRecord, endingRecord, totalRecordCount, totalPage, totalData, pageData, columns, tempTotalData) {
        this.page             = page;
        this.startingRecord   = startingRecord;
        this.endingRecord     = endingRecord;
        this.totalRecordCount = totalRecordCount;
        this.totalPage        = totalPage;
        this.totalData        = totalData;
        this.pageData         = pageData;
        this.columns          = columns;
        this.tempTotalData    = tempTotalData;
    }
}