import { LightningElement, api, track, wire } from 'lwc';
import ProcessUploadedFile from '@salesforce/apex/Cswp_CalBoxFileUploadController.ProcessUploadedFile';
import ProcessUploadedSignature from '@salesforce/apex/Cswp_CalBoxFileUploadController.ProcessUploadedSignature';
import StoreCertificate from '@salesforce/apex/Cswp_CalBoxFileUploadController.StoreCertificate';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import SuccessLabel from '@salesforce/label/c.cswp_Success_Label';
import ErrorLabel from '@salesforce/label/c.cswp_Error_Label';
import WarningLabel from '@salesforce/label/c.cswp_Warning_Label';
import ValidatedLabel from '@salesforce/label/c.Cswp_File_Validated';
import GenerateCertificateLabel from '@salesforce/label/c.Cswp_Generate_Certification';
import LanguageEnglish from '@salesforce/label/c.Cswp_English';
import LanguageJapanese from '@salesforce/label/c.Cswp_Japanese';
import LanguageChinese from '@salesforce/label/c.Cswp_Chinese';
import LanguageKorean from '@salesforce/label/c.Cswp_Korean';
import Language from '@salesforce/label/c.Cswp_Language';
import SelectLanguage from '@salesforce/label/c.Cswp_Select_Language';
import cswp_calbox_page_title from '@salesforce/label/c.cswp_calbox_page_title';
import cswp_calbox_content_1 from '@salesforce/label/c.cswp_calbox_content_1';
import cswp_calbox_content_2 from '@salesforce/label/c.cswp_calbox_content_2';
import cswp_calbox_content_3 from '@salesforce/label/c.cswp_calbox_content_3';
import cswpcalbox_text_1 from '@salesforce/label/c.cswpcalbox_text_1';
import cswpcalbox_text_2 from '@salesforce/label/c.cswpcalbox_text_2';
import cswp_upload_calibration_label from '@salesforce/label/c.cswp_upload_calibration_label';
import cswp_select_account_first from '@salesforce/label/c.cswp_select_account_first';
import { logActivity } from "c/cswpUtil";
import loggedInUserId from '@salesforce/user/Id';
import getUser from '@salesforce/apex/CSWPSobjectSelector.getUser'

export default class CcrzyCalibrationBoxUpload extends LightningElement {
    @api
    myRecordId;

    @track jsfiles;
    @track error;    
    @track toastTitle;
    @track toastMessage;
    @track isLoading = false;
    @track isGenerateButtonDisabled = true;
    @track ContentDocumentLinkID;
    @track selectedLanguage = "en_US";
    @track calBoxDataID;
    loggedInUser = {};
    isFSE = false;
    isUploadReady = true;
    selectedAccountId;

    label = {
        GenerateCertificateLabel,
        LanguageEnglish,
        LanguageJapanese,
        LanguageChinese,
        LanguageKorean,
        Language,
        SelectLanguage,
        cswp_calbox_page_title,
        cswp_calbox_content_1,
        cswp_calbox_content_2,
        cswp_calbox_content_3,
        cswpcalbox_text_1,
        cswpcalbox_text_2,
        cswp_select_account_first,
        cswp_upload_calibration_label
    };

    @wire(getUser, { userID: loggedInUserId}) 
    getLoggedInUser({error, data}) {
        if(data) {
            this.loggedInUser = data;
            if(this.loggedInUser.Profile.Name.includes('FSE')) { 
                this.isFSE = true; 
                this.isUploadReady = false;
            } 
        }
        else if(error) {
            console.log('loggedInUser error ==> '+error);
        }
    }
    
    get languageOptions() {
        return [
            { label: LanguageEnglish, value: "en_US" },
            { label: LanguageJapanese, value: "ja_JP" },
            { label: LanguageKorean, value: "ko_KR"},
            { label: LanguageChinese, value: "zh_TW"}
        ];
    }

    handleLanguageChange(event) {
        this.selectedLanguage = event.detail.value;
    }

    get acceptedFormats() {
        return ['.zip', '.ecr'];
    }

    get acceptedSignatureFormats() {
        return ['.png', '.jpeg', '.jpg'];
    }

    generateBtnClicked(event) {
        window.open("/apex/cswpCalBoxCertificate?linkID="+this.ContentDocumentLinkID+'&lang='+this.selectedLanguage, '_blank');        
        // TODO storeCertificate Method
        StoreCertificate({linkID : this.ContentDocumentLinkID , pageLanguage: this.pageLanguage, entityID : this.calBoxDataID})
        .then(result => {            
            console.log(result);
        })
        .catch(error => {
            this.error = error;
            this.toastTitle = ErrorLabel;
            this.toastMessage = error.body.message
            const toastEvent = new ShowToastEvent({
                "variant": this.toastTitle,
                "title": this.toastTitle,
                "message": this.toastMessage
            });
            this.dispatchEvent(toastEvent);
        });
    }

    handleUploadFinished(event) {
        this.isLoading = true;
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
        ProcessUploadedFile({files : uploadedFiles, fseCustomerName : this.selectedAccountId})
        .then(result => { 
            this.jsfiles = result;
            let toastMode = 'dismissible';
            if(result.Result == 'Success') {
                this.calBoxDataID = result.CalBoxDataID;
                this.ContentDocumentLinkID = uploadedFiles[0].documentId;
                this.toastTitle = SuccessLabel;
                this.toastMessage = ValidatedLabel;
                toastMode = 'dismissible';
            }
            else {
                this.toastTitle = ErrorLabel;
                this.toastMessage = result.Result;
                toastMode = 'sticky';
                if(result.Warning) { this.toastTitle = WarningLabel; }
            }
            logActivity({
                id: this.calBoxDataID,
                name: "DataFile-"+result.CreatedEntityName
            },
            "upload",
            "DataFile " + result.CreatedEntityName + " uploaded ");
            const toastEvent = new ShowToastEvent({
                "variant": this.toastTitle,
                "title": this.toastTitle,
                "message": this.toastMessage,
                "mode": toastMode
            });
            this.dispatchEvent(toastEvent);
            this.isLoading = false;
            this.isGenerateButtonDisabled = false;
        })
        .catch(error => {
        });
    }

    @track isSignatureModalOpen = false;
    openSignatureModal() {
        this.isSignatureModalOpen = true;
    }

    closeSignatureModal() {
        this.isSignatureModalOpen = false;
    }

    handleSignatureUploadFinished(event) {
        const uploadedSignatures = event.detail.files;
        ProcessUploadedSignature({ContentDocID : uploadedSignatures[0].documentId})
        .then(result => {
            this.isSignatureModalOpen = false;
        })
        .catch(error => {
            this.error = error;            
        });
    }

    handleAccountSelection(event)  {
        this.selectedAccountId = event.target.value;
        this.isUploadReady = (this.selectedAccountId != null && this.selectedAccountId != '');
    }
}