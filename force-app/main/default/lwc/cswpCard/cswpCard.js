import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { loadStyle } from 'lightning/platformResourceLoader';
import css from '@salesforce/resourceUrl/cswpCard'

export default class CswpCard extends NavigationMixin(LightningElement) {
    @api image = 'https://www.lightningdesignsystem.com/assets/images/carousel/carousel-03.jpg';
    @api title = 'Default Title';
    @api text = 'Default Text';
    @api link = 'Default Link';
    @api linkTarget;

    connectedCallback(){
        loadStyle(this, css);
    }

    handleClick(e) {
        switch (this.linkTarget) {
            case 'New Tab':
                window.open(this.link, '_blank');
                break;
            case 'Current Tab':
                window.open(this.link, '_self');
                break;
            case 'Community Page':
                this.navigateToPage(this.link);
                break;
        }
    }

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }
}