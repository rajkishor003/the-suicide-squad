import { LightningElement,wire,api,track } from 'lwc';
import getCurrentTicket from '@salesforce/apex/CswpTicketManagerController.getTicketDetail';
export default class CswpCaseManager extends LightningElement {
    @api recordId;
    @track currentTicket;

    @wire(getCurrentTicket , {currentCaseId : '$recordId'})
    wiredTickets({error,data}) {
        if (data) {
            this.currentTicket = data;
        } else if (error) {
            console.error("error ----> ", error);
        }
    }
    
}