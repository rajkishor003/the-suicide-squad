/* eslint-disable */
import { LightningElement, wire, track } from 'lwc';
// import { loadScript } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { refreshApex } from '@salesforce/apex';
import cswpResources from '@salesforce/resourceUrl/cswp_resources';

import cswp_subject from '@salesforce/label/c.cswp_subject';
import cswp_status from '@salesforce/label/c.Cswp_Status';
import cswp_type from '@salesforce/label/c.Cswp_Type';
import cswp_priority from '@salesforce/label/c.cswp_priority';
import cswp_reason from '@salesforce/label/c.cswp_reason';
import cswp_calbox_search_text from '@salesforce/label/c.cswp_calbox_search_text';
import cswp_calbox_iconmessage from '@salesforce/label/c.cswp_calbox_iconmessage';
import cswp_calbox_advanced_search from '@salesforce/label/c.cswp_calbox_advanced_search';
import cswp_createdby from '@salesforce/label/c.cswp_createdby';
import cswp_assignee from '@salesforce/label/c.cswp_assignee';
import PleaseSelectLabel from '@salesforce/label/c.Cswp_Please_Select';
import cswp_calbox_cancel_text from '@salesforce/label/c.cswp_calbox_cancel_text';
import cswp_calboxtable_close from '@salesforce/label/c.cswp_calboxtable_close';
import AE_Overview from '@salesforce/label/c.Cswp_AE_overview';
import AE_SubmitTicket from '@salesforce/label/c.Cswp_AE_submit_a_ticket';
import AE_ContractTerminated from '@salesforce/label/c.Cswp_AE_contract_terminated';
import AE_TotalHoursExceed from '@salesforce/label/c.Cswp_AE_total_hours_exceed';
import AE_TicketModalClose from '@salesforce/label/c.Cswp_AE_ticket_modal_close';
import AE_SubmitTicketHeader from '@salesforce/label/c.Cswp_AE_submit_a_ticket_header';
import AE_CancelButton from '@salesforce/label/c.Cswp_AE_Cancel_Button';
import AE_SubmitButton from '@salesforce/label/c.Cswp_AE_Submit_Button';
import AE_TimeSpent from '@salesforce/label/c.Cswp_AE_Time_Spent';
import AE_LimitExceed from '@salesforce/label/c.Cswp_AE_Limit_Exceed';
import AE_Remaining from '@salesforce/label/c.Cswp_AE_Remaining';
import AE_Of from '@salesforce/label/c.Cswp_AE_of';
import AE_ServiceHistory from '@salesforce/label/c.Cswp_AE_Service_History';
import AE_SearchForTickets from '@salesforce/label/c.Cswp_AE_Search_For_Tickets';
import AE_Please_Contact from '@salesforce/label/c.CSWP_AE_Please_Contact';
import AE_Support_Mail from '@salesforce/label/c.CSWP_AE_Support_Mail';
import AE_Extend_Contract from '@salesforce/label/c.CSWP_AE_To_Extend_Contract';
import CSWP_No_Jira_Project_Available from '@salesforce/label/c.CSWP_No_Jira_Project_Available';


import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import CASE_OBJECT from '@salesforce/schema/Case';
import STATUS_FIELD from '@salesforce/schema/Case.Status';
import TYPE_FIELD from '@salesforce/schema/Case.Type';
import PRIORITY_FIELD from '@salesforce/schema/Case.Priority';

import getConsultancyTickets from '@salesforce/apex/CSWPAEConsultancyController.getConsultancyTickets';

import getTicketOverview from '@salesforce/apex/CSWPAEConsultancyController.getTicketOverview';
import getJiraProjects from '@salesforce/apex/CSWPAEConsultancyController.getJiraProjects';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';
export default class CswpConsultancyManager extends NavigationMixin(LightningElement) {

    // isChartJsInitialized;
    chartConfiguration;
    modalInit = false;
    caseId;
    @track jiraProjects;
    @track displayProject = false;
    @track displayJiraProjectWarning;
    @track availableContract = true;
    @track singleProjectId;
    @track singleProjectAssignee;
    @track singleProjectAssigneeName;
    @track singleProjectJiraCustomer;
    @track totalEstimate = 0.00;
    @track timeSpent = 0.00;
    @track remainingEstimate = 0.00;
    @track formattedRemainingEstimate;
    @track formattedTotalEstimate;
    @track formattedTimeSpent;
    @track isTicketModalVisible = false;
    @track isAdvancedSearchModalOpen = false;
    @track sortBy;
    @track sortDirection;
    //advanced Search Parameters
    subjSearchParam = '';
    statusOptions = '';
    caseStatusParam = '';
    assigneeParam = '';
    typeOptions = '';
    caseTypeParam = '';
    priorityuOptions = '';
    casePriorityParam = '';
    createdParam = '';

    @track formCompleted = true;
    @track hasActiveContract = true;
    @track totalHoursExceed = false;
    //labels
    label = {
        cswp_subject,
        cswp_status,
        cswp_type,
        cswp_priority,
        cswp_reason,
        cswp_calbox_search_text,
        cswp_calbox_iconmessage,
        cswp_calbox_advanced_search,
        cswp_createdby,
        cswp_assignee,
        PleaseSelectLabel,
        cswp_calbox_cancel_text,
        cswp_calboxtable_close,
        AE_Overview,
        AE_SubmitTicket,
        AE_ContractTerminated,
        AE_TotalHoursExceed,
        AE_TicketModalClose,
        AE_SubmitTicketHeader,
        AE_CancelButton,
        AE_SubmitButton,
        AE_TimeSpent,
        AE_LimitExceed,
        AE_Remaining,
        AE_Of,
        AE_ServiceHistory,
        AE_SearchForTickets,
        AE_Please_Contact,
        AE_Support_Mail,
        AE_Extend_Contract,
        AE_mail_to: 'mailto:' + AE_Support_Mail,
        CSWP_No_Jira_Project_Available
    };

    //datatable properties
    @track advSearchAppliedConsultancyData;
    @track initialConsultancyData;
    @track consultancyData;
    @track keyword;
    consultancyColumns = [
        { label: this.label.cswp_subject, fieldName: 'CaseUrl', sortable:'true', type: 'url', typeAttributes: {label: { fieldName: 'Subject' }, target: '_self'}},
        { label: this.label.cswp_status, fieldName: 'Status',sortable:'true', type: 'Text' },
        { label: this.label.cswp_createdby, fieldName: 'CreatedByName',sortable:'true', type: 'Text'},
        { label: this.label.cswp_assignee, fieldName: 'Assignee', sortable:'true',type: 'Text'},
        {label: this.label.AE_TimeSpent, fieldName: 'TimeSpent', sortable:'true',type: 'Formula'}
    ];

    refreshTable;

    @wire(getObjectInfo, { objectApiName: CASE_OBJECT }) caseMetadata;

    @wire(getPicklistValues,
        {
            recordTypeId: '$caseMetadata.data.defaultRecordTypeId',
            fieldApiName: TYPE_FIELD
        }
    ) typePicklist;

    @wire(getPicklistValues,
        {
            recordTypeId: '$caseMetadata.data.defaultRecordTypeId',
            fieldApiName: STATUS_FIELD
        }
    ) statusPicklist;

    @wire(getPicklistValues,
        {
            recordTypeId: '$caseMetadata.data.defaultRecordTypeId',
            fieldApiName: PRIORITY_FIELD
        }
    ) priorityPicklist;
    @wire(getConsultancyTickets)
    wiredConsultancyTickets(result) {
        this.refreshTable = result;
        if (result.data) {
            var conWrapperList = [];
            var casePageUrl = window.location.origin + '/cswp/s/case/';
            for (var i = 0; i < result.data.length; i++) {

                let conWrapper = new consultancyWrapper(result.data[i].Subject, result.data[i].Status, result.data[i].CreatedBy.Name, result.data[i].Cswp_Assignee__c, result.data[i].Type, result.data[i].Priority, casePageUrl+result.data[i].Id, result.data[i].AE_Calculated_Time_Spent__c);
                conWrapperList.push(conWrapper);
            }
            this.consultancyData = conWrapperList;
            this.initialConsultancyData = conWrapperList;
        } else if (result.error) {
            console.log('error ==> '+result.error);
        }
    }
    @wire(getJiraProjects)
    wiredJiraProjects({ error, data }) {

        if (data) {
            if(data.length < 1) {
                this.displayJiraProjectWarning = true;
            }
            else {
                this.displayJiraProjectWarning = false;
                if(data.length == 1) {
                    this.singleProjectId = data[0].Id;
                    this.singleProjectAssignee = data[0].Cswp_Default_Assignee_ID__c;
                    this.singleProjectAssigneeName = data[0].Cswp_Default_Assignee_Name__c;
                    this.singleProjectJiraCustomer = data[0].Cswp_Jira_Customer__c;
                    this.displayProject = false;
                }
                else {
                    this.displayProject = true;
                }
                this.jiraProjects = data.map(row => ({
                    ...row,
                    Id: row.Id,
                    Name: row.Name,
                    label: row.Name,
                    value: row.Id,
                    assignee : row.Cswp_Default_Assignee_ID__c,
                    assigneeName : row.Cswp_Default_Assignee_Name__c,
                    jiraCustomer : row.Cswp_Jira_Customer__c
                }));
            }
        } else if (error) {
            console.log('error ==> '+error);
        } 
    }


    @wire(getTicketOverview)
    wiredOriginalEstimate({ error, data }) {
        if(!data) {
            this.availableContract = false;
        }
        if (data) {
            this.availableContract = true;
            if(data.hasActiveContract == false) {
                this.hasActiveContract = false;
            }

            if(data.totalHoursExceed == true) {
                this.totalHoursExceed = true;
                this.remainingEstimate = 0;
            }
            if(data.totalHours && data.timeSpent && !data.totalHoursExceed) {
                this.remainingEstimate = (data.totalHours  - data.timeSpent);
                this.remainingEstimateForProgressBar = (data.totalHours  - data.timeSpent) / data.totalHours * 100;
                this.formattedTotalEstimate = data.totalHours.toFixed(2) + ' hours';
                this.formattedTimeSpent = data.timeSpent.toFixed(2) + ' hours';
                this.formattedRemainingEstimate = this.remainingEstimate.toFixed(2) + ' hours';
            }
            else if (data.totalHours && !data.timeSpent && !data.totalHoursExceed) {
                this.remainingEstimate = data.totalHours;
                this.remainingEstimateForProgressBar = 100;
                this.formattedTotalEstimate = data.totalHours.toFixed(2) + ' hours';
                this.formattedTimeSpent = '0 hour';
                this.formattedRemainingEstimate = this.remainingEstimate.toFixed(2) + ' hours';
            }
            else if (!data.totalHours) {
                this.remainingEstimate = 0;
                this.remainingEstimateForProgressBar = 0;
                this.formattedTotalEstimate = '0 hour';
                this.formattedRemainingEstimate = this.remainingEstimate.toFixed(2) + ' hours';
            }
            else if (data.totalHours && data.timeSpent && data.totalHoursExceed) {
                this.remainingEstimate = 0;
                this.remainingEstimateForProgressBar = 0;
                this.formattedTotalEstimate = data.totalHours.toFixed(2) + ' hours';
                this.formattedTimeSpent = data.timeSpent.toFixed(2) + ' hours';
                this.formattedRemainingEstimate = this.remainingEstimate.toFixed(2) + ' hours';;
            }

        } else if (error) {
            console.log('error ==> '+error);
        }
    }

    secondsToWdhms(seconds) {
        seconds = Number(seconds);
        var h = seconds / 3600;
        var hDisplay = h >= 0 ? h + (h == 1 ? " hour " : " hours ") : "";
        return hDisplay;
    }

    showTicketModal() {
        this.isTicketModalVisible = true;
    }

    hideTicketModal() {
        this.isTicketModalVisible = false;
    }

    formLoad() {
        this.modalInit = true;
    }

    formSubmit(event){
        event.preventDefault();       // stop the form from submitting
        const fields = event.detail.fields;
        if(!this.singleProjectId && this.displayProject) {
            this.singleProjectId = this.template.querySelector(".jira-project").value;
        }
        if(!this.singleProjectAssignee && this.displayProject){
            this.singleProjectAssignee = this.jiraProjects.find(element => element.Id == this.template.querySelector(".jira-project").value).assignee;
        }
        if(!this.singleProjectAssigneeName && this.displayProject){
            this.singleProjectAssigneeName = this.jiraProjects.find(element => element.Id == this.template.querySelector(".jira-project").value).assigneeName;
        }
        if(!this.singleProjectJiraCustomer && this.displayProject){
            this.singleProjectJiraCustomer = this.jiraProjects.find(element => element.Id == this.template.querySelector(".jira-project").value).jiraCustomer;
        }
        if(this.singleProjectId) {
            fields.Cswp_Jira_Project__c = this.singleProjectId;
        }

        if(this.singleProjectAssignee) {
            fields.Cswp_Default_Assignee_ID__c = this.singleProjectAssignee;
        }
        if(this.singleProjectAssigneeName) {
            fields.Cswp_Assignee__c = this.singleProjectAssigneeName;
        }
        if(this.singleProjectJiraCustomer){
            fields.Cswp_Jira_Customer__c = this.singleProjectJiraCustomer;
        }
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }
    formSuccess(e) {
        this.formCompleted = false;
        this.caseId = e.detail.id;
        setTimeout(() => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Case created successfully',
                    message: 'Case created successfully',
                    variant: 'success',
                })
            );
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    objectApiName: 'Case',
                    actionName: 'view',
                    recordId: this.caseId,
                }
            });
            this.formCompleted = true;
        }, 3000);
        return refreshApex(this.refreshTable);
    }

    formError() {
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Case creation failed',
                message: error.message,
                variant: 'error',
            })
        );
    }

    // renderedCallback() {
    //     if (this.isChartJsInitialized) {
    //         return;
    //     }

    //     // load chartjs from the static resource
    //     Promise.all([loadScript(this, cswpResources + '/scripts/Chart.js')])
    //         .then(() => {
    //             this.isChartJsInitialized = true;
    //             const ctx = this.template.querySelector('canvas.pieChart').getContext('2d');
    //             // this.chart = new window.Chart(ctx, JSON.parse(JSON.stringify(config)));
    //             const data = {
    //                 labels: [
    //                     'In Progress',
    //                     'Pending',
    //                     'Done'
    //                 ],
    //                 datasets: [{
    //                     data: [10, 3, 21],
    //                     backgroundColor: [
    //                         '#0153cc',
    //                         '#e5e0e0',
    //                         '#8dcc9a'
    //                     ],
    //                 }]
    //             };
    //             const config = {
    //                 type: 'pie',
    //                 data: data,
    //                 options: {
    //                     title: {
    //                         display: true,
    //                         text: 'Resolution',
    //                         position: 'top'
    //                     },
    //                     legend: {
    //                         position: 'bottom',
    //                         labels: {
    //                             boxWidth: 10,
    //                             generateLabels: function (chart) {
    //                                 var data = chart.data;
    //                                 if (data.labels.length && data.datasets.length) {
    //                                     return data.labels.map(function (label, i) {
    //                                         var meta = chart.getDatasetMeta(0);
    //                                         var ds = data.datasets[0];
    //                                         var arc = meta.data[i];
    //                                         var custom = arc && arc.custom || {};
    //                                         var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
    //                                         var arcOpts = chart.options.elements.arc;
    //                                         var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
    //                                         var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
    //                                         var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
    //                                         var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];
    //                                         return {
    //                                             text: label + " : " + value,
    //                                             fillStyle: fill,
    //                                             strokeStyle: stroke,
    //                                             lineWidth: bw,
    //                                             hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
    //                                             index: i
    //                                         };
    //                                     });
    //                                 } else {
    //                                     return [];
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             };
    //             this.chart = new window.Chart(ctx, config);
    //         })
    //         .catch(error => {
    //             this.dispatchEvent(
    //                 new ShowToastEvent({
    //                     title: 'Error loading Chart',
    //                     message: error.message,
    //                     variant: 'error',
    //                 })
    //             );
    //         });
    // }

    setKeyword(event) {
        this.keyword = event.target.value;
        if(this.keyword !=undefined && this.keyword.length > 1) {
            this.consultancyData = this.initialConsultancyData.filter(x=> (x.Subject) && x.Subject.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                (x.Status)&&x.Status.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                (x.CreatedByName)&&x.CreatedByName.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                (x.TimeSpent)&&x.TimeSpent.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                (x.Assignee)&&x.Assignee.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                (x.Type)&&x.Type.toLowerCase().includes(this.keyword.toLowerCase()));
        }
        else {
            this.consultancyData = this.initialConsultancyData;
        }
    }

    openAdvancedSearchModal(event) {
        this.isAdvancedSearchModalOpen = true;

    }
    closeAdvancedSearchModal(event) {
        this.isAdvancedSearchModalOpen = false;
    }

    caseStatusParamChanged(event) {
        this.caseStatusParam = event.detail.value;
    }

    caseTypeParamChanged(event) {
        this.caseTypeParam = event.detail.value;
    }

    casePriorityParamChanged(event) {
        this.casePriorityParam = event.detail.value;
    }

    subjParamChanged(event) {
        this.subjSearchParam = event.detail.value;
    }
    assigneeParamChanged(event) {
        this.assigneeParam = event.detail.value;
    }
    createdParamChanged(event) {
        this.createdParam = event.detail.value;
    }

    operateAdvancedSearchModal(event) {
        this.advSearchAppliedConsultancyData = this.initialConsultancyData;
        this.filterMap = [];
        if(this.subjSearchParam != undefined && this.subjSearchParam.length != "") {
            this.filterMap.push({label:"Subject", value:this.subjSearchParam, fieldType:"text"});
        }
        if(this.caseStatusParam != undefined && this.caseStatusParam.length != "") {
            this.filterMap.push({label:"Status", value:this.caseStatusParam, fieldType:"text"});
        }
        if(this.assigneeParam != undefined && this.assigneeParam.length != "") {
            this.filterMap.push({label:"Assignee", value:this.assigneeParam, fieldType:"text"});
        }
        if(this.caseTypeParam != undefined && this.caseTypeParam.length != "") {
            this.filterMap.push({label:"Type", value:this.caseTypeParam, fieldType:"text"});
        }
        if(this.casePriorityParam != undefined && this.casePriorityParam.length != "") {
            this.filterMap.push({label:"Priority", value:this.casePriorityParam, fieldType:"text"});
        }
        if(this.createdParam != undefined && this.createdParam.length != "") {
            this.filterMap.push({label:"CreatedByName", value:this.createdParam, fieldType:"text"});
        }

        if(this.filterMap.length == 0) this.consultancyData = this.initialConsultancyData;
        for(let i=0; i<this.filterMap.length; i++) {
            //this.advancedFilterApplied = true;
            let row = this.filterMap[i];
            if(row.fieldType == "text") {
                this.advSearchAppliedConsultancyData = this.advSearchAppliedConsultancyData.filter(x=> (x[row.label]) && x[row.label].toLowerCase() == row.value.toLowerCase());
            }
        }
        this.consultancyData = this.advSearchAppliedConsultancyData;
    }

    doSorting(event) {
        if(event.detail.fieldName== 'CaseURL'){
            this.sortBy = 'Subject';
        }
        else{
            this.sortBy = event.detail.fieldName;
        }
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection,'list');
    }

    sortData(fieldname, direction,tablename) {
        let parseData;
        if(tablename == 'list') {
            parseData = JSON.parse(JSON.stringify(this.consultancyData));
        }

        // Return the value stored in the field
        let keyValue = (a) => {

            if(fieldname.includes("Url") ){
                return a['Subject'];
            }
            return a[fieldname];
        };
        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1: -1;
        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        if(tablename == 'list') {
            this.consultancyData = parseData;
        }
    }
}

class consultancyWrapper {

    constructor(Subject, Status, CreatedByName, Assignee, Type, Priority, CaseUrl, TimeSpent) {
        if(!Subject) Subject = ' ';
        this.Subject         = Subject;
        this.Status          = Status;
        this.CreatedByName   = CreatedByName;
        this.Assignee        = Assignee;
        this.Type            = Type;
        this.Priority        = Priority;
        this.CaseUrl         = CaseUrl;
        this.TimeSpent       = TimeSpent;
    }
}