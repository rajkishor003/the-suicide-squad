import { LightningElement, track, api,wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveRecord from '@salesforce/apex/CSWPContactUsController.saveCase';
import createContentDocumentLinks from '@salesforce/apex/CSWPContactUsController.createContentDocumentLinks';
import deleteContentDocument from '@salesforce/apex/CSWPContactUsController.deleteContentDocument';
import getNavItemList from '@salesforce/apex/cswpNavManagerController.getNavItemList';
const MAX_FILE_SIZE = 100000000; //10mb
//labels
import cswp_topic from '@salesforce/label/c.cswp_topic';
import cswp_select_topic from '@salesforce/label/c.cswp_select_topic';
import cswp_topic_general from '@salesforce/label/c.cswp_topic_general';
import cswp_topic_technical_documentation from '@salesforce/label/c.cswp_topic_technical_documentation';
import cswp_topic_document_sharing from '@salesforce/label/c.cswp_topic_document_sharing';
import cswp_topic_faq from '@salesforce/label/c.cswp_topic_faq';
import cswp_topic_calibration_box from '@salesforce/label/c.cswp_topic_calibration_box';
import cswp_topic_v93000_licensing from '@salesforce/label/c.cswp_topic_v93000_licensing';
import cswp_topic_support_request from '@salesforce/label/c.cswp_topic_support_request';
import cswp_topic_inventory_query from '@salesforce/label/c.cswp_topic_inventory_query';
import cswp_topic_order_management from '@salesforce/label/c.cswp_topic_order_management';
import cswp_feedback from '@salesforce/label/c.cswp_feedback';
import cswp_remove from '@salesforce/label/c.cswp_remove';
import cswp_submit from '@salesforce/label/c.cswp_submit';
import cswp_file_attachment_label from '@salesforce/label/c.cswp_file_attachment_label';
import cswp_contact_us_submit_success_title from '@salesforce/label/c.cswp_contact_us_submit_success_title';
import cswp_contact_us_submit_success_text from '@salesforce/label/c.cswp_contact_us_submit_success_text';
import cswp_contact_us_submit_error_title from '@salesforce/label/c.cswp_contact_us_submit_error_title';
import cswp_contact_us_submit_error_text from '@salesforce/label/c.cswp_contact_us_submit_error_text';

export default class CswpContactUs extends NavigationMixin(LightningElement) {
    label = {
        cswp_topic,
        cswp_select_topic,
        cswp_topic_technical_documentation,
        cswp_topic_document_sharing,
        cswp_topic_faq,
        cswp_topic_calibration_box,
        cswp_topic_v93000_licensing,
        cswp_topic_support_request,
        cswp_topic_inventory_query,
        cswp_topic_order_management,
        cswp_feedback,
        cswp_file_attachment_label,
        cswp_remove,
        cswp_submit,
        cswp_contact_us_submit_success_title,
        cswp_contact_us_submit_success_text,
        cswp_contact_us_submit_error_title,
        cswp_contact_us_submit_error_text,
        cswp_topic_general
    }
    @track topic;
    @track feedback;
    @track caseRecId;
    @track disableFileUpload = true;
    @track documentIds = [];
    @track uploadedFiles = [];
    @api isLoading = false;
    @track navitems;
    @wire(getNavItemList)
    wiredlinks({error, data}) {
        if(data) {
            this.navitems = data;
        } else if (error) {
            console.error("error -----> ", error);
        }
    }

    checkValid() {
        this.topic = this.template.querySelector("select").value;
        this.feedback = this.template.querySelector("textarea").value;
        this.disableFileUpload = !(this.topic && this.feedback);
    }

    async handleFileUpload(event) {
        const fileArray = event.detail.files;
        for (let i = 0; i < fileArray.length; i++) {
            const file = fileArray[i];
            if (file && file.hasOwnProperty("documentId")) {
                this.uploadedFiles.push(file);
                this.documentIds.push(file.documentId);
            }
        }
        if (this.documentIds.length > 0 && this.caseRecId) {
            let docIdsJson = JSON.stringify(documentIds);
            this.uploadFile();
        }
    }

    navigateToPage() {
        console.log('navigate to home');
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: "Home"
            }
        });
    }

    goHome() {
        setTimeout(() => {
            this.navigateToPage();
        }, 1000);
    }

    async uploadFile() {
        let contents = await createContentDocumentLinks({
            "contentDocumentIds": this.documentIds,
            "caseId": this.caseRecId
        });
        this.goHome();
    }

    saveRecord() {
        this.isLoading = true;
        this.topic = this.template.querySelector("select").value;
        this.feedback = this.template.querySelector("textarea").value;

        if (this.topic && this.feedback) {
            let caseRec = {
                'sobjectType': 'Case',
                'Status': 'New',
                'Origin': 'Web',
                'Priority': 'Medium',
                'Type': 'Feedback',
                'Subject': this.topic,
                'Description': this.feedback
            }
            saveRecord({
                    caseRec: caseRec
                })
                .then(caseId => {
                    if (caseId) {
                        this.caseRecId = caseId;
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: this.label.cswp_contact_us_submit_success_title,
                                variant: 'success',
                                message: this.label.cswp_contact_us_submit_success_text,
                            }),
                        );

                        if (this.documentIds.length > 0) {
                            this.uploadFile();
                        } else {
                            this.goHome();
                        }
                        this.isLoading = false;
                    }
                }).catch(error => {
                    console.log('error ', error);
                    this.isLoading = false;
                });
        } else {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: this.label.cswp_contact_us_submit_error_title,
                    variant: 'error',
                    message: this.label.cswp_contact_us_submit_error_text,
                }),
            );
            this.isLoading = false;
        }
    }

    removeFile(e) {
        let selectedItemId = e.target.dataset.item;
        for (let i = 0; i < this.uploadedFiles.length; i++) {
            if (selectedItemId == this.uploadedFiles[i].documentId) {
                let index = i;
                this.uploadedFiles.splice(index, 1);
            }
        }
        let docIdindex = this.documentIds.indexOf(selectedItemId);;
        this.documentIds.splice(docIdindex, 1);

        deleteContentDocument({
                documentId: selectedItemId
            })
            .catch(error => {
                console.log('error ', error);
            });
    }
}