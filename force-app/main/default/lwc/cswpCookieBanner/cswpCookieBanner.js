import { LightningElement, wire} from 'lwc';
import {  CurrentPageReference } from 'lightning/navigation';
import cswpResources from '@salesforce/resourceUrl/cswp_resources';
import { loadScript } from 'lightning/platformResourceLoader';
import saveUserCookies from '@salesforce/apex/CSWPCookieController.saveUserCookies';
import getUserCookies from '@salesforce/apex/CSWPCookieController.getUserCookies';
import getTrakingId from '@salesforce/apex/CSWPCookieController.getTrakingId';

const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay)); // eslint-disable-line @lwc/lwc/no-async-operation

export default class CswpCookieBanner extends   LightningElement {
    showCookies = false;
    trakingID = '';
    displayPreferences = false;
    displayStatistics = false;
    displayEsential = false;
    cookieLogo = cswpResources + '/images/advantest-cookie-logo.png';
    defaultStyle = '';
    visibleStyle = 'height: 100%; opacity: 1;';
    hiddenStyle = 'height: 0; opacity: 0;';
    allCookie = false;
    technicalCookie = false;
    cookieType;
    savedCookie = '';
    
    @wire (CurrentPageReference)
    currentPage;
    
    async togglePreferences () {
        this.animateBanner ();
        await sleep(500);
        this.handleViews();
    }
    acceptAllCookies(){
        this.allCookie = true;
        this.showCookies = false;
        this.handleCookies();
        this.googleAnalytics();
        this.hideCookies ();
      
    }
    acceptTechnicalCookies(){
        this.technicalCookie = true;
        this.handleCookies();
        this.hideCookies ();
    }
    animateBanner () {
        var cookieContainer = this.template.querySelector('[data-id="cookie-container"]');
        cookieContainer.classList.add('fadeOutDown');
        setTimeout(() => { cookieContainer.classList.remove('fadeOutDown'); }, 500); // eslint-disable-line @lwc/lwc/no-async-operation
    }

    handleViews () {
        var cookieBox = this.template.querySelector('[data-id="cookie-box"]');
        var cookiePreference = this.template.querySelector('[data-id="cookie-preference"]');

        if (this.displayPreferences) {
            cookieBox.style = this.visibleStyle;
            cookiePreference.style = this.hiddenStyle;
            this.displayPreferences = false;
        } else {
            cookieBox.style = this.hiddenStyle;
            cookiePreference.style = this.visibleStyle;
            this.displayPreferences = true;
        }
    }

    toggleDetailTable (event) {
        var tableType = event.target.dataset.tableType;
        var trigger = this.template.querySelector('[data-cookie-accordion-target="' + tableType + '"]');
        var content = this.template.querySelector('[data-cookie-accordion-parent="' + tableType + '"]');

        if (tableType === 'statistics') {
            if (this.displayStatistics) {
                trigger.innerHTML = 'Show Cookie-Information'; // eslint-disable-line @lwc/lwc/no-inner-html
                content.style.display= 'none';
                this.displayStatistics = false;
            } else {
                trigger.innerHTML = 'Hide Cookie-Information'; // eslint-disable-line @lwc/lwc/no-inner-html
                content.style.display= 'block';
                this.displayStatistics = true;
            }
        } else if (tableType === 'essential') {
            if (this.displayEsential) {
                trigger.innerHTML = 'Show Cookie-Information'; // eslint-disable-line @lwc/lwc/no-inner-html
                content.style.display = 'none';
                this.displayEsential = false;
            } else {
                trigger.innerHTML = 'Hide Cookie-Information'; // eslint-disable-line @lwc/lwc/no-inner-html
                content.style.display = 'block';
                this.displayEsential = true;
            }
        }
    }

    async hideCookies () { // call this when any type of cookies accepted
        this.animateBanner ();
        await sleep(500);
        this.showCookies = false;
    }

    connectedCallback () {
        this.checkCookies();
       
    }

    renderedCallback(){
        if(this.technicalCookie || this.allCookie){
            this.hideCookies();
        }
        else{
            this.checkCookies(); 
        }
        
    }
    googleAnalytics(){
        console.log('executed');
        Promise.all([loadScript(this, cswpResources + '/scripts/googleAnalytics.js')])
             .then(() => {
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script', cswpResources + '/scripts/ga.js','ga'); 
                    ga('create', this.trakingID, 'auto');ga('send', {
                        hitType: 'pageview',
                        page: location.pathname
                      }); 
                 window.dataLayer = window.dataLayer || [];
                 function gtag(){dataLayer.push(arguments);}
                 gtag('js', new Date());
            
                 //gtag('config', 'UA-209796869-1');
             });
    }
    handleType(){
        if(this.allCookie){
            this.cookieType = 'All';
        }else if(this.technicalCookie) {
            this.cookieType = 'Technical';
        } else {
            this.showCookies = true;
        }
    }
    handleCookies(){
        this.handleType();
        let cookietype = this.cookieType;
        console.log(cookietype);
        if(cookietype){
            saveUserCookies({
                cookieType: cookietype
            });
            } /*else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.cswp_contact_us_submit_error_title,
                        variant: 'error',
                        message: this.label.cswp_contact_us_submit_error_text,
                    }),
                );
                this.isLoading = false;
            }*/
       
    }
    @wire(getUserCookies)
    wiredCookies({error, data}) {
        if(data ) {
            console.log("data -----> ", data);
            this.savedCookie = data;
            this.checkCookies();
        } else if (error) {
            console.error("error -----> ", error);
        }
    }
    @wire(getTrakingId)
    wiredTrakingId({error, data}) {
        if(data ) {
            console.log("data -----> ", data);
            this.trakingID = data;
            
        } else if (error) {
            console.error("error -----> ", error);
        }
    }
    checkCookies(){
        if(this.savedCookie.includes('None')){
            this.showCookies = true;
        }
        else if(this.savedCookie.includes('All')){
            this.hideCookies();
            this.googleAnalytics();
            this.showCookies = false;
        }
        else{
            this.hideCookies();
            this.showCookies = false;
        }
        
    }
}