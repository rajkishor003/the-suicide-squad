import LightningDatatable from 'lightning/datatable';
import customTypeDownload from './cswpCustomTypeDownload'
import customTypeSystemSn from './cswpCustomTypeSystemSn'

export default class cswpCustomLightningDatatable extends LightningDatatable {
    static customTypes = {
        customTypeDownload: {
            template: customTypeDownload,
            typeAttributes: ['myta','my2ta']
        },
        customTypeSystemSn: {
            template: customTypeSystemSn,
            typeAttributes: ['systemSn']
        },
    }
}