import { LightningElement, api } from 'lwc';
import cswp_download_label from '@salesforce/label/c.cswp_download_label';
export default class cswpCustomTypeDownload extends LightningElement {
    label ={
        cswp_download_label
    };
    @api calboxdataid;
    @api isvisible;
    fireDownloadEvent() {
        console.log('download event created and modal opened');
        const event = new CustomEvent('customdownload', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                recordId: this.calboxdataid
            }
        });
        this.dispatchEvent(event);
    }
}