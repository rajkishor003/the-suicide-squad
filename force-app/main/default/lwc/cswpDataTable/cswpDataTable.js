import LightningDatatable from "lightning/datatable";
import cswpNameCell from "./cswpNameCell";

export default class CswpDataTable extends LightningDatatable {
    static customTypes = {
        cswpNameCell : {
            template: cswpNameCell,
            typeAttributes: ['id','type','icon','breadcrumb'] 
        }
    }
}