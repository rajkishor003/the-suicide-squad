import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { loadStyle } from 'lightning/platformResourceLoader';
import css from '@salesforce/resourceUrl/cswpCard'
import cswp_docsharing_title from '@salesforce/label/c.cswp_docsharing_title';
import cswp_docsharing_owner from '@salesforce/label/c.cswp_docsharing_owner';
import cswp_docsharing_lastModifiedDate from '@salesforce/label/c.cswp_docsharing_lastModifiedDate';

const data = [{
        id: 1,
        title: 'Sample File 1',
        owner: 'Zimetu',
        lastmd : '12-11-2020 13:37'
    },
    {
        id: 2,
        title: 'Sample File 2',
        owner: 'Zimetu',
        lastmd: '12-11-2020 13:37'
    },
    {
        id: 3,
        title: 'Sample File 3',
        owner: 'Zimetu',
        lastmd: '12-11-2020 13:37'
    },
    {
        id: 4,
        title: 'Sample File 4',
        owner: 'Zimetu',
        lastmd: '12-11-2020 13:37',
    },
    {
        id: 5,
        title: 'Sample File 5',
        owner: 'Zimetu',
        lastmd: '12-11-2020 13:37',
    },
];
label = {
    cswp_docsharing_title,
    cswp_docsharing_owner,
    cswp_docsharing_lastModifiedDate
}
const columns = [{
        label: this.label.cswp_docsharing_title,
        fieldName: 'title',
        sortable: true,
        cellAttributes: {
            alignment: 'left'
        },
    },
    {
        label: this.label.cswp_docsharing_owner,
        fieldName: 'owner',
        sortable: true,
        cellAttributes: {
            alignment: 'left'
        },
    },
    {
        label: this.label.cswp_docsharing_lastModifiedDate,
        fieldName: 'lastmd',
        sortable: true,
        cellAttributes: {
            alignment: 'left'
        },
    },
];

export default class CswpDocumentSharing extends NavigationMixin(LightningElement) {
    @api title;
    @api link;
    @api linkTarget;
    data = data;
    columns = columns;
    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;

    sortBy(field, reverse, primer) {
        const key = primer ?
            function (x) {
                return primer(x[field]);
            } :
            function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const {
            fieldName: sortedBy,
            sortDirection
        } = event.detail;
        const cloneData = [...this.data];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.data = cloneData;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    connectedCallback(){
        loadStyle(this, css);
    }

    handleClick(e) {
        switch (this.linkTarget) {
            case 'New Tab':
                window.open(this.link, '_blank');
                break;
            case 'Current Tab':
                window.open(this.link, '_self');
                break;
            case 'Community Page':
                this.navigateToPage(this.link);
                break;
        }
    }

    navigateToPage(apiName) {
        // Use the built-in 'Navigate' method
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }
}