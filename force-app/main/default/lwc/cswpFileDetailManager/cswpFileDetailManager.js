import { LightningElement,wire } from 'lwc';
import { CurrentPageReference,NavigationMixin } from 'lightning/navigation';
import getFileDetail from "@salesforce/apex/CSWPFileDetailController.getFileDetail";
import deleteFile from '@salesforce/apex/CSWPFileDetailController.deleteFile';
import cswp_download_label from '@salesforce/label/c.cswp_download_label';
import cswp_file_detail_action_edit from '@salesforce/label/c.cswp_file_detail_action_edit';
import cswp_table_action_delete from '@salesforce/label/c.cswp_table_action_delete';
import cswp_file_detail_action_update from '@salesforce/label/c.cswp_file_detail_action_update';
import cswp_file_detail_action_cancel from '@salesforce/label/c.cswp_file_detail_action_cancel';

import cswp_label_name from '@salesforce/label/c.cswp_label_name';
import cswp_label_foldername_c from '@salesforce/label/c.cswp_label_foldername_c';
import cswp_label_status_c from '@salesforce/label/c.cswp_label_status_c';
import cswp_label_filepath_c from '@salesforce/label/c.cswp_label_filepath_c';
import cswp_label_filesize_c from '@salesforce/label/c.cswp_label_filesize_c';
import cswp_label_description_c from '@salesforce/label/c.cswp_label_description_c';
import cswp_label_lastdownloadedtime_c from '@salesforce/label/c.cswp_label_lastdownloadedtime_c';
import cswp_label_uploadedbyname_c from '@salesforce/label/c.cswp_label_uploadedbyname_c';
import cswp_label_uploadeddatetime_c from '@salesforce/label/c.cswp_label_uploadeddatetime_c';
import cswp_label_fileextension_c from '@salesforce/label/c.cswp_label_fileextension_c';
import cswp_label_mimetype_c from '@salesforce/label/c.cswp_label_mimetype_c';
import cswp_label_lastversionnumber_c from '@salesforce/label/c.cswp_label_lastversionnumber_c';
import cswp_delete_file_confirm from '@salesforce/label/c.CSWP_Delete_File_Confirm';

import DOWNLOAD_TIME_FIELD from '@salesforce/schema/cswp_File__c.cswp_Last_Downloaded_Time__c';
import UPLOADED_TIME_FIELD from '@salesforce/schema/cswp_File__c.cswp_UploadedDateTime__c';
import ID_FIELD from '@salesforce/schema/cswp_File__c.Id';
import { updateRecord } from "lightning/uiRecordApi";
import { logActivity } from "c/cswpUtil";
import TIMEZONE from "@salesforce/i18n/timeZone";

export default class CswpFileDetailManager extends NavigationMixin(LightningElement) {

    fileId;
    viewFile;
    editForm;
    canDelete;
    canRename;
    canDownload;
    canUpload;
    fileDetail;
    uploadedTime;
    downloadedTime;
    userTimeZone = TIMEZONE;

    label = {
        cswp_download_label,
        cswp_file_detail_action_edit,
        cswp_table_action_delete,
        cswp_file_detail_action_update,
        cswp_file_detail_action_cancel,
        cswp_label_name,
        cswp_label_foldername_c,
        cswp_label_status_c,
        cswp_label_filepath_c,
        cswp_label_filesize_c,
        cswp_label_description_c,
        cswp_label_lastdownloadedtime_c,
        cswp_label_uploadedbyname_c,
        cswp_label_uploadeddatetime_c,
        cswp_label_fileextension_c,
        cswp_label_mimetype_c,
        cswp_label_lastversionnumber_c,
        cswp_delete_file_confirm
    }

    currentPageReference; 

    @wire(CurrentPageReference)
    setCurrentPageReference(page){
        this.currentPageReference = page;
        console.log('this.state: > %O', this.currentPageReference);
        if(this.currentPageReference.state){
            this.fileId = this.currentPageReference.state.recordId;
            this.viewFile = true;
            this.editForm = false;
        }
    }

    get downloadedTime() {
        return getFieldValue(this.fileDates.data, DOWNLOAD_TIME_FIELD);
    }

    get uploadedTime() {
        return getFieldValue(this.fieldDates.data, UPLOADED_TIME_FIELD);
    }

    @wire(getFileDetail, {fileId : '$fileId'})
    fileDetail({error, data}) {
        console.log('timezone -----> ', this.userTimeZone);
        if(data) {
            console.log('data -----> ', data);
            this.fileDetail = data;
            this.canDelete = data.canDelete;
            this.canDownload = data.canDownload;
            this.canRename = data.canRename;
            this.canUpload = data.canUpload;
            this.uploadedTime = data.uploadedTime;
            this.downloadedTime = data.downloadedTime;
        } else {
            console.error("error -----> ", error);
        }
    }

    deleteFile() {
        if(!window.confirm(this.label.cswp_delete_file_confirm)){
            return;
        }
        deleteFile({
            fileId: this.fileId
        })
        .then(isDeleted => {
            if (isDeleted) {
                this[NavigationMixin.Navigate]({
                    type: 'comm__namedPage',
                    attributes: {
                        name: "Home"
                    }
                });
            }
        }).catch(error => {
            console.log('error ', error);
        });
    }

    editFile() {
        this.viewFile = false;
        this.editForm = true;
    }

    cancelEdit() {
        this.viewFile = true;
        this.editForm = false;
    }

    async downloadItem() {
        //log the download activity
        logActivity({
          name: this.fileDetail.name,
          id: this.fileId,
          filepath: this.fileDetail.path
        },'Download', this.fileDetail.name + ' downloaded!');
        
        let url;
        let urlString = window.location.href;
        let baseURL = urlString.substring(0, urlString.indexOf('/s'));
        url = baseURL + this.fileDetail.downloadUrl;
        //Update the last downloaded Time Field
        this.updateDownloadedFile();
        //then redirect users to download page
        window.location.href = url;
      }
    
      updateDownloadedFile(){
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.fileId;
        fields[DOWNLOAD_TIME_FIELD.fieldApiName] = new Date().toISOString();
        updateRecord({ fields })
          .then( res => console.log(res) )
          .catch( err => console.error(err.body));
      }

      
}