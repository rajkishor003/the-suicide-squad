import { api, LightningElement } from "lwc";
import uploadFilesToRemote from '@salesforce/apex/CSWPWorkspaceManagerController.uploadFilesToRemote';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle } from 'lightning/platformResourceLoader';
import css from '@salesforce/resourceUrl/cswp_resources';
import cswp_FileUploadMessage from '@salesforce/label/c.cswp_FileUploadMessage';
import cswp_fileuploaded from "@salesforce/label/c.cswp_fileuploaded";
import cswp_contact_us_submit_success_title from "@salesforce/label/c.cswp_contact_us_submit_success_title";
import cswp_contact_us_submit_error_title from "@salesforce/label/c.cswp_contact_us_submit_error_title";
// import cswp_duplicate_file_name from "@salesforce/label/c.cswp_duplicate_file_name";
import cswp_item_exist_error from '@salesforce/label/c.cswp_item_exist_error';
import { logActivity } from "c/cswpUtil";

export default class CswpFileUpload extends LightningElement {
  @api recordId;
  //@api folderId;

  connectedCallback(){
      loadStyle(this, css + '/styles/fileupload.css');
  }
 
  async handleFileUpload(event) {
    const uploadedFiles = event.detail.files;
    let documentIds = [];
    for (let i = 0; i < uploadedFiles.length; i++) {
      const file = uploadedFiles[i];
      if (file && file.hasOwnProperty("documentId")) {
        documentIds.push(file.documentId);
      }
    }
    if(documentIds.length > 0) {
      let docIdsJson = JSON.stringify(documentIds);
      console.log(docIdsJson);
      this.uploadFiles(documentIds);
      /*let contents = await contentVersionByDocumentId({
        "contentDocumentIds": documentIds,
        "parentFolderOrWorkspaceId": this.recordId
      });*/
      if(documentIds){
        this.dispatchEvent(new ShowToastEvent({
          title : cswp_fileuploaded,
          message: cswp_FileUploadMessage,
          variant: 'info'
        }));
      }
      //console.log(JSON.stringify(contents));
    }
  }

  //docIds: List<String>
  uploadFiles(docIds){
    console.log('uploadFiles.docIOds');
    uploadFilesToRemote({
      parentId: this.recordId,
      documentIds: docIds
    })
    .then(res => {
      console.log('file upload result %0', res);
      //log the upload activity
      for (let i = 0; i < res.length; i++) {
        const fileObj = res[i];
        logActivity({
          name: fileObj.Name,
          id: fileObj.Id,
          filepath: fileObj.cswp_FilePath__c
        },'Upload', fileObj.Name + ' Uploaded!');
      }
      this.dispatchEvent(new ShowToastEvent({
        title : cswp_contact_us_submit_success_title,
        message : cswp_fileuploaded,
        variant: 'success'
      }));
      this.dispatchEvent(new CustomEvent('fileuploaddone'));
    }).catch(err=> {
      console.error(err.body);
      let errorMessage = '';
      if(err.body && 
          (err.body.message.indexOf('DUPLICATE_VALUE') > 0 || err.body.message.indexOf('cswp_FilePath__c'))) {
        errorMessage = cswp_item_exist_error;
      } else {
        errorMessage = err.body.message;
      }
      this.dispatchEvent(new ShowToastEvent({
        title : cswp_contact_us_submit_error_title,
        message : errorMessage,
        variant: 'error'
      }));
    });
  }
}