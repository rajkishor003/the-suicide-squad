import { LightningElement,api } from 'lwc';
import createUploadSession from "@salesforce/apex/CSWPWorkspaceManagerController.createUploadSession";
import { showSuccessToast, showErrorToast } from "c/cswpUtil";
import cswp_filerversion_upload_text from "@salesforce/label/c.cswp_filerversion_upload_text";
import cswp_fileversion_uploadmsg from "@salesforce/label/c.cswp_fileversion_uploadmsg";
import cswp_fileuploaded from "@salesforce/label/c.cswp_fileuploaded";
export default class CswpFileVersionUpload extends LightningElement {
    label = {
      cswp_filerversion_upload_text,
      cswp_fileversion_uploadmsg,
      cswp_fileuploaded
    };

    _cswpFileId;
    @api recordId;
    isProcessing = false;
    progress = 0;
  
    sizeLimit = 5 * 1024 * 1024; // MB: if the file is lower than 5MB upload use smallChunk,
    // otherwise use 2MB as chunksize
    chunksize = 2 * 1024 * 1024; // Testing with 4 MB;
    smallChunksize = 320 * 1024;
    position = 0; //starts position

    async handleFileChange(e){
        let file = e.target.files[0];
        console.log('selected File : %O', file);
        let reader = new FileReader();
        reader.onload = async() => {
           if(reader.readyState === reader.DONE){
               let fileBufferArray = reader.result;
               console.log('onload.reader.result ',fileBufferArray);
               let url = await this.getUploadUrl(file);
               console.log('uploadSessionUrl:>' , url);
               if(fileBufferArray.byteLength <= this.sizeLimit){
                 //await this.uploadAllAtOnce(url,fileBufferArray);
                 //320KB chunk
                 await this.uploadAsChunks(url,fileBufferArray,this.smallChunksize)
               }else{
                 //2MB chunk
                 await this.uploadAsChunks(url,fileBufferArray,this.chunksize);
               }
           }
        };
        reader.readAsArrayBuffer(file);        
    }

    /*async uploadAllAtOnce(url, buffer){
      let total = buffer.byteLength;
      this.isProcessing = true;
      this.progress = 1;
      let result = await this.putFileVersion(url,buffer,0,total-1,total);
      console.log('uploaded at once result : %O',result);
    }*/

    async uploadAsChunks(url,buffer,chunksize){
      this.isProcessing = true;
      let fileSize = buffer.byteLength;  
      let position = 0;
      let continued = true;
      while(continued){
        let start = position;
        let end = position + chunksize;      
        position = end;
        try{
          if(position >= fileSize){
            chunksize = fileSize - start;
            end = fileSize;
            continued = false;
            //break;
          }
          let slicedBuffer = buffer.slice(start,end);
          let chunk_result = await this.putFileVersion(url,slicedBuffer,start,end-1,fileSize);
          if(chunk_result.error){
            console.log('each_result: >' , chunk_result.error);
            showErrorToast(this,
              {"title":"Upload Failed!","message":chunk_result.error.message}
            );
            continued = false;
            break;
          }
          if(chunk_result.success &&  chunk_result.body.hasOwnProperty('id')){
            console.log('chunk_result:%O > ',chunk_result);    
            //Send success event
            this.dispatchSuccessEvent({ 
              id: chunk_result.body.id,
              size: chunk_result.body.size,
              name: chunk_result.body.name,
            });
          }          
        }catch(err){
          continued = false;
        }
      }
    }

    //range as string
    async putFileVersion(url,buffer,start,end,totalSize){
       let response = await fetch(url,{
          method: 'PUT',
          headers:{
            'Content-Length': buffer.byteLength,
            'Content-Range' : 'bytes ' + start +'-' + end + '/' + totalSize
          },
          body: buffer
       });
       let resJson = {};
       if(response.ok){
         let res = await response.json();
         resJson = { success:true, body:res, error:null};
         this.progress = Math.round( (end/totalSize) * 100);
         if(this.progress === 100){
            this._resetProgress();
            showSuccessToast(this,{
              title: this.label.cswp_fileuploaded,
              message: this.label.cswp_fileversion_uploadmsg
            });
         }
       }else{
          this._resetProgress();
          let body = await response.json();
          resJson = { success : false, body : null,  status : response.status,  error:  body.error };
       }
       return resJson;
    }

    dispatchSuccessEvent(detail){
      const event = new CustomEvent('fileversionuploaded',{
        bubbles: true, 
        detail: detail
      });
      this.dispatchEvent(event);
    }

    _resetProgress(){
      this.isProcessing = false;
      this.progress = 0;
    }

    async getUploadUrl(file){
      let result = await createUploadSession({
          cswpFileId: this.recordId,
          fileSize: file.size
      });
      return result;
  }
}