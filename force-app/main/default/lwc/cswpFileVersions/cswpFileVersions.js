import { LightningElement, wire,api } from 'lwc';
import getFileVersions from "@salesforce/apex/CSWPWorkspaceManagerController.getFileVersions";
import userPermissionByFile from "@salesforce/apex/CSWPWorkspaceManagerController.userPermissionByFile"
import getFileVersionDownloadUrl from "@salesforce/apex/CSWPWorkspaceManagerController.getFileVersionDownloadUrl";
import { showErrorToast, logActivity } from "c/cswpUtil";
import { refreshApex } from "@salesforce/apex";
import { getRecord } from "lightning/uiRecordApi";
import { updateRecord } from "lightning/uiRecordApi";
import cswp_nodatamsg from "@salesforce/label/c.cswp_nodatamsg";
import cswp_download_label from "@salesforce/label/c.cswp_download_label";
import cswp_fileversion_versionno from "@salesforce/label/c.cswp_fileversion_versionno";
import cswp_fileversion_filesize from "@salesforce/label/c.cswp_fileversion_filesize";
import cswp_fileversion_md from "@salesforce/label/c.cswp_fileversion_md";
import cswp_fileversion_toasttitle from "@salesforce/label/c.cswp_fileversion_toasttitle";
import cswp_fileversion_toastmsg from "@salesforce/label/c.cswp_fileversion_toastmsg";

import FILE_SIZE_FIELD from '@salesforce/schema/cswp_File__c.cswp_FileSize__c';
import VERSION_FIELD from '@salesforce/schema/cswp_File__c.cswp_LastVersionNumber__c';
import ID_FIELD from '@salesforce/schema/cswp_File__c.Id';

const FIELDS = [
  "cswp_File__c.Name",
  "cswp_File__c.cswp_FilePath__c"
];

export default class CswpFileVersions extends LightningElement {
    @api recordId;

    label = {
      cswp_nodatamsg,
      cswp_download_label,
      cswp_fileversion_versionno,
      cswp_fileversion_filesize,
      cswp_fileversion_md,
      cswp_fileversion_toasttitle,
      cswp_fileversion_toastmsg
    };

    wiredFileVersions = {};
    fileVersionLoaded = false;
    canDownload = false;

    error;
    noDataMessage = this.label.cswp_nodatamsg;
    actions = [{ label: this.label.cswp_download_label, name: 'download' }];
    columns = [
      { label: this.label.cswp_fileversion_versionno, fieldName :'id', type: 'text'},
      { label: this.label.cswp_fileversion_filesize, fieldName :'size', type: 'number'},
      { label: this.label.cswp_fileversion_md, fieldName:'lastModifiedDateTime',type:'date'},
      { label: this.label.cswp_download_label, type: 'action', fixedWidth: 90, typeAttributes: { rowActions: this.actions } }
    ];

    downloading = false;

    @wire(getRecord, {recordId : '$recordId', fields: FIELDS })
    theFile;

    @wire(getFileVersions,{fileId: '$recordId'})
    fileVersions(result){
      this.fileVersionLoaded = true;
      this.wiredFileVersions = result;
    }

    @wire(userPermissionByFile,{fileId: '$recordId'})
    userInfo({data}){
      console.log('userInfo.data :> ', JSON.stringify(data));
      if(data && data.permissions){
        this.canDownload = data.permissions.canDownload;
      }

    }
    
    async handleAction(event){
      const action = event.detail.action.name;
      const row =event.detail.row;
      if(action === 'download'){
          console.log('check permission : %O',JSON.stringify(this.userInfo));
          if(this.canDownload){
            this.downloading = true;
            try{
              let url = await this.getDownloadUrl(row.id);
              // console.log('downloadUrl: ', url);
              console.log('theFile %O',this.theFile);
              //No wait, log as a cswp dowload activity
              logActivity({
                  id: this.recordId,
                  name: this.theFile.data.fields.Name.value,
                  filepath:this.theFile.data.fields.cswp_FilePath__c.value
                },
                "download",
              "File Version " + row.id + " dowloaded ");
              //Open new page
              this.downloading = false;
              window.open(url);
            }catch(err){
              console.error(err);
              this.downloading = false;
            }
          }else{
            this.downloading = false;
            showErrorToast(this,{
              "title": this.label.cswp_fileversion_toasttitle,
              "message": this.label.cswp_fileversion_toastmsg
            });
        }
      }
    }
    get isfound(){
      return (this.wiredFileVersions.data && this.wiredFileVersions.data.length>0);
      //return true;
    }

    get versionData(){
      return this.wiredFileVersions.data;
    }

    async getDownloadUrl(version){
      try{
        console.log( 'versionData.list :> ' , JSON.stringify(this.versionData));
        let index = this.versionData.findIndex( item => item.id === version);
        let result = await getFileVersionDownloadUrl({
          fileId: this.recordId,
          //if the version is latest then dowload original file, otherwise download file version
          versionNo: index === 0 ? null : version
        });
        return result;
      }catch(error){
        return error;
      }
    }

    updateCswpFile(fileSize,versionNumber){
      const fields = {};
      fields[ID_FIELD.fieldApiName] = this.recordId;
      fields[FILE_SIZE_FIELD.fieldApiName] = fileSize
      fields[VERSION_FIELD.fieldApiName]= versionNumber;
      updateRecord({ fields })
        .then( res => {
          console.log('updated file size %i  and result is %O ', fileSize, res);
        })
        .catch( err => console.error(err.body));
    }

    handleFileUploaded(event){
      if(event.detail.id){
        refreshApex(this.wiredFileVersions).then(()=>{
          //Update the latest file version size
          this.updateCswpFile(event.detail.size, this.wiredFileVersions.data[0].id);
        });
      }
    }
}