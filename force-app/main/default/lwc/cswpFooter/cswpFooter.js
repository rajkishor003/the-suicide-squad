import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import footerIcons from '@salesforce/resourceUrl/cswp_footer_icons';


export default class CcrzyFooter extends NavigationMixin(LightningElement) {
    @api footerMessage = 'Default Message';
    iconFacebook = footerIcons + '/icon_facebook.png';
    iconTwitter = footerIcons + '/icon_twitter.png';

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }

    navigateContactUs(e) {
        this.navigateToPage('Contact_Us__c');
    }
}