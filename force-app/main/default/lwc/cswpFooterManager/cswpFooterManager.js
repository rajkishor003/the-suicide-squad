import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import cswpResources from '@salesforce/resourceUrl/cswp_resources';
import termsOfUse from '@salesforce/label/c.cswp_terms_of_use';
import contactUs from '@salesforce/label/c.cswp_contact_us';
import privacyPolicy from '@salesforce/label/c.cswp_privacy_policy';
import footerMessage from '@salesforce/label/c.cswp_footer_message';


export default class CswpFooterManager extends NavigationMixin(LightningElement) {
    label = {
        termsOfUse,
        contactUs,
        privacyPolicy,
        footerMessage
    }
    iconFacebook = cswpResources + '/icons/icon_facebook.png';
    iconTwitter = cswpResources + '/icons/icon_twitter.png';

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }

    navigateContactUs(e) {
        this.navigateToPage('Contact_Us__c');
    }
}