import { LightningElement, api, track } from 'lwc';
import {loadStyle} from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import relatedListResource from '@salesforce/resourceUrl/relatedListResource';
import getCswpGroupLinks from '@salesforce/apex/CSWPSobjectSelector.getCswpGroupLinks';
import getSelectableCswpGroupsForLink from '@salesforce/apex/CSWPSobjectSelector.getAvailableCswpGroupsToAddCswpGroupLink';
import SubmitSelectedCswpGroups from '@salesforce/apex/CSWPGroupController.AddCswpGroupLinkToPublicGroup';
import RemoveSelectedCswpGroups from '@salesforce/apex/CSWPGroupController.RemoveCswpGroupLinkFromPublicGroup';
//import { refreshApex } from '@salesforce/apex';

import NameLabel from '@salesforce/label/c.cswp_label_name';
import EmailLabel from '@salesforce/label/c.cswp_label_email';
import UsernameLabel from '@salesforce/label/c.cswp_label_username';
import CloseLabel from '@salesforce/label/c.cswp_calboxtable_close';
import PopUpTitleLabel from '@salesforce/label/c.cswp_label_addlinktogroup';
import PopUpTitleRemoveLabel from '@salesforce/label/c.cswp_label_removelinkfromgroup';
import SubmitLabel from '@salesforce/label/c.cswp_submit';
import SearchLabel from '@salesforce/label/c.cswp_calbox_search_text';
import LicenceWarningLabel from '@salesforce/label/c.cswp_licence_warning';
import SuccessLabel from '@salesforce/label/c.cswp_contact_us_submit_success_title';

export default class CswpGroupLinks extends LightningElement {

    @api recordId;
    @track recordCount = 0;
    @track records;
    @track isModalOpen = false;
    @track isRemoveModalOpen = false;
    @track cswpGroupSelectionData;
    @track cswpGroupSelectionDataBackup;
    @track removeCswpGroupSelectionData;
    @track removeCswpGroupSelectionDataBackup;
    @track selectedRows;
    @track selectedRowsRemove;
    @track isRefreshNeeded = false;
    @track keyword;
    @track removeKeyword;

    label = {
        NameLabel,
        EmailLabel,
        UsernameLabel,
        CloseLabel,
        PopUpTitleLabel,
        PopUpTitleRemoveLabel,
        SubmitLabel,
        SearchLabel,
        LicenceWarningLabel,
        SuccessLabel
    }

    columns = [
        { label: this.label.NameLabel, fieldName: 'LinkName', type: 'url', typeAttributes: {label: { fieldName: 'LinkedGroupName' }, target: '_blank'}}
    ];

    cswpGroupSelectionColumns = [
        { label: this.label.NameLabel, fieldName: 'Name', sortable:'true', type: 'Text'}
    ];
    removeCswpGroupSelectionColumns = [
        { label: this.label.NameLabel, fieldName: 'CSWP_Linked_Group_Name__c', sortable:'true', type: 'Text'}
    ];

    renderedCallback() {
        loadStyle(this, relatedListResource + '/relatedList.css')
        if(this.isRefreshNeeded) {this.getCswpLinks();}
        if(this.cswpGroupSelectionData == null) { this.getSelectionCswpGroups();}
        if(this.removeCswpGroupSelectionData == null) { this.getSelectionRemoveCswpGroups();}
    }

    connectedCallback() {
        this.getCswpLinks();
        this.getSelectionCswpGroups();
        this.getSelectionRemoveCswpGroups();
    }

    getCswpLinks() {
        getCswpGroupLinks({cswpGroupId : this.recordId})
        .then(result => {
            var data = [];
            for (let i = 0; i < result.length; i++) {
                var glw = new GroupLinkWrapper(
                    result[i].Name,
                    result[i].CSWP_Linked_Group__c,
                    result[i].CSWP_Master_Group__c,
                    result[i].CSWP_Linked_Group_Name__c,
                    result[i].CSWP_Master_Group_Name__c
                );
                data.push(glw);
            }
            this.records = data;
            this.recordCount = result.length;
        })
        .catch(error => {
            console.log(error);
        });
        this.isRefreshNeeded = false;
    };

    getSelectionCswpGroups() {
        getSelectableCswpGroupsForLink({cswpGroupId : this.recordId})
        .then(result => {
            this.cswpGroupSelectionData = result;
            this.cswpGroupSelectionDataBackup = this.cswpGroupSelectionData;
        })
        .catch(error => {
            console.log(error);
        });
    };

    getSelectionRemoveCswpGroups() {
        getCswpGroupLinks({cswpGroupId : this.recordId})
        .then(result => {
            this.removeCswpGroupSelectionData = result;
            this.removeCswpGroupSelectionDataBackup = this.removeCswpGroupSelectionData;
        })
        .catch(error => {
            console.log(error);
        });
    };

    getSelectedCswpGroups(event) {
        debugger;
        this.selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log(this.selectedRows);
    }

    getSelectedRemoveCswpGroups(event) {
        this.selectedRowsRemove = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log(this.selectedRowsRemove);
    }

    handleCreateRecord() {
        this.isModalOpen = true;
    }

    handleDeleteRecord() {
        this.isRemoveModalOpen = true;
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
        this.isRemoveModalOpen = false;
    }

    submitDetails() {
        SubmitSelectedCswpGroups({cswpGroupId:this.recordId, selectedCswpGroups:JSON.stringify(this.selectedRows)})
        .then(result => {
            this.isModalOpen = false;
            this.keyword = '';
            this.getCswpLinks();
            this.getSelectionCswpGroups();
            this.getSelectionRemoveCswpGroups();
            //this.records = [...this.records];
            this.isRefreshNeeded = true;          
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: this.label.SuccessLabel,
                }),
            );

        })
        .catch(error => {
            debugger;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Warning!',
                    variant: 'warning',
                    message: error.body.message,
                }),
            );
            console.log(error);
        });
    }

    removeCswpGroupsFromCswpGroupLink() {
        console.log('this.selectedRowsRemove ==> '+this.selectedRowsRemove);
        RemoveSelectedCswpGroups({cswpGroupId:this.recordId, selectedCswpGroups:JSON.stringify(this.selectedRowsRemove)})
        .then(result => {
            this.isRemoveModalOpen = false;
            this.removeKeyword = '';
            this.getCswpLinks();
            this.getSelectionCswpGroups();
            this.getSelectionRemoveCswpGroups();
            //this.records = [...this.records];
            this.isRefreshNeeded = true;

            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: this.label.SuccessLabel,
                }),
            );
        })
        .catch(error => {
            console.log(error);
        });
    }

    setKeyword(event) {
        this.keyword = event.target.value;
        if(this.keyword.length > 0) {
            this.cswpGroupSelectionData = this.cswpGroupSelectionDataBackup.filter(x=> x.Name.toLowerCase().includes(this.keyword.toLowerCase()));
        }
        else {
            this.cswpGroupSelectionData = this.cswpGroupSelectionDataBackup;
        }
    }

    setRemoveKeyword(event) {
        this.removeKeyword = event.target.value;
        if(this.removeKeyword.length > 0) {
            this.removeCswpGroupSelectionData = this.removeCswpGroupSelectionDataBackup.filter(x=> x.Name.toLowerCase().includes(this.removeKeyword.toLowerCase()));
        }
        else {
            this.removeCswpGroupSelectionData = this.removeCswpGroupSelectionDataBackup;
        }
    }
    
}

class GroupLinkWrapper {

    constructor(Name, LinkedGroupId, MasterGroupId, LinkedGroupName, MasterGroupName ) {
        this.Name              = Name;
        this.LinkedGroupId     = LinkedGroupId;
        this.MasterGroupId     = MasterGroupId;
        this.LinkedGroupName   = LinkedGroupName;
        this.MasterGroupName   = MasterGroupName;
        this.LinkName          = '/'+LinkedGroupId;
    }
}