import { LightningElement, api, track, wire } from 'lwc';
import {loadStyle} from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import relatedListResource from '@salesforce/resourceUrl/relatedListResource';
import getUsers from '@salesforce/apex/CSWPSobjectSelector.getCswpGroupUsersByCswpGroupIds';
import getCswpGroupLinks from '@salesforce/apex/CSWPSobjectSelector.getCswpGroupLinks';
import getSelectableUsers from '@salesforce/apex/CSWPSobjectSelector.getAvailableUsersToAddCswpGroup';
import SubmitSelectedUsers from '@salesforce/apex/CSWPGroupController.AddUsersToPublicGroup';
import RemoveSelectedUsers from '@salesforce/apex/CSWPGroupController.RemoveUsersFromPublicGroup';
//import { refreshApex } from '@salesforce/apex';

import NameLabel from '@salesforce/label/c.cswp_label_name';
import EmailLabel from '@salesforce/label/c.cswp_label_email';
import UsernameLabel from '@salesforce/label/c.cswp_label_username';
import CloseLabel from '@salesforce/label/c.cswp_calboxtable_close';
import PopUpTitleLabel from '@salesforce/label/c.cswp_label_addtogroup';
import PopUpTitleRemoveLabel from '@salesforce/label/c.cswp_label_removefromgroup';
import SubmitLabel from '@salesforce/label/c.cswp_submit';
import SearchLabel from '@salesforce/label/c.cswp_calbox_search_text';
import LicenceWarningLabel from '@salesforce/label/c.cswp_licence_warning';
import SuccessLabel from '@salesforce/label/c.cswp_contact_us_submit_success_title';
import CSWPGroupNameLabel from '@salesforce/label/c.cswp_group_name';
import IsActiveLabel from '@salesforce/label/c.cswp_is_active';

export default class CswpGroupUsers extends LightningElement {
    @api recordId;
    @track recordCount = 0;
    @track records;
    @track isModalOpen = false;
    @track isRemoveModalOpen = false;
    @track userSelectionData;
    @track userSelectionDataBackup;
    @track removeUserSelectionData;
    @track removeUserSelectionDataBackup;
    @track selectedRows;
    @track selectedRowsRemove;
    @track isRefreshNeeded = false;
    @track keyword;
    @track removeKeyword;
    @track showRelatedList = true;

    label = {
        NameLabel,
        EmailLabel,
        UsernameLabel,
        CloseLabel,
        PopUpTitleLabel,
        PopUpTitleRemoveLabel,
        SubmitLabel,
        SearchLabel,
        LicenceWarningLabel,
        SuccessLabel,
        CSWPGroupNameLabel,
        IsActiveLabel
    }

    relatedListColumns = [        
        { label: this.label.NameLabel, fieldName: 'Cswp_User_Name__c', sortable:'true', type: 'Text'}, 
        { label: this.label.EmailLabel, fieldName: 'Cswp_Email__c', sortable:'true', type:'Email'},
        { label: this.label.IsActiveLabel, fieldName: 'Cswp_Is_Active__c', sortable:'true', type:'boolean'}
    ];

    userSelectionColumns = [
        { label: this.label.NameLabel, fieldName: 'Name', sortable:'true', type: 'Text'},
        { label: this.label.UsernameLabel, fieldName: 'Username', sortable:'true', type:'Text'},
        { label: this.label.EmailLabel, fieldName: 'Email', sortable:'true', type:'Email'}
    ];
    removeUserSelectionColumns = [
        { label: this.label.NameLabel, fieldName: 'Cswp_User_Name__c', sortable:'true', type: 'Text'}, 
        { label: this.label.EmailLabel, fieldName: 'Cswp_Email__c', sortable:'true', type:'Email'}
    ];

    @wire(getCswpGroupLinks, { cswpGroupId: '$recordId' })
    wiredgetCswpGroupLinks({ error, data }) {
        if (data) {
            if(data.length>0) this.showRelatedList = false;
        } else if (error) {
            this.error = error;
        }
    }

    renderedCallback() {
        loadStyle(this, relatedListResource + '/relatedList.css')
        if(this.isRefreshNeeded) {this.getCswpUsers();}
        if(this.userSelectionData == null) { this.getSelectionUsers();}
        if(this.removeUserSelectionData == null) { this.getSelectionRemoveUsers();}
    }

    connectedCallback() {
        this.getCswpUsers();
        this.getSelectionUsers();
        this.getSelectionRemoveUsers();
    }

    getCswpUsers() {
        getCswpGroupLinks({ cswpGroupId: this.recordId })
        .then(result => {
            if(result.length>0) this.showRelatedList = false;
            else {
                getUsers({cswpGroupId : this.recordId})
                .then(result => {
                    this.records = result;
                    this.recordCount = result.length;
                })
                .catch(error => {
                    console.log(error);
                });
            }
        })
        .catch(error => {
            console.log(error);
        });
        this.isRefreshNeeded = false;
    };

    getSelectionUsers() {
        getSelectableUsers({cswpGroupId : this.recordId})
        .then(result => {
            this.userSelectionData = result;
            this.userSelectionDataBackup = this.userSelectionData;
        })
        .catch(error => {
            console.log(error);
        });
    };

    getSelectionRemoveUsers() {
        getUsers({cswpGroupId : this.recordId})
        .then(result => {
            this.removeUserSelectionData = result;
            this.removeUserSelectionDataBackup = this.removeUserSelectionData;
        })
        .catch(error => {
            console.log(error);
        });
    };

    getSelectedUsers(event) {
        this.selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log(this.selectedRows);
    }

    getSelectedRemoveUsers(event) {
        this.selectedRowsRemove = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log(this.selectedRowsRemove);
    }

    handleCreateRecord() {
        this.isModalOpen = true;
    }

    handleDeleteRecord() {
        this.isRemoveModalOpen = true;
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
        this.isRemoveModalOpen = false;
    }

    submitDetails() {
        SubmitSelectedUsers({cswpGroupId:this.recordId, selectedUser:JSON.stringify(this.selectedRows)})
        .then(result => {
            //this.userSelectionData = result;
            this.isModalOpen = false;
            this.keyword = '';
            this.getCswpUsers();
            this.getSelectionUsers();
            this.getSelectionRemoveUsers();
            //this.records = [...this.records];
            this.isRefreshNeeded = true;
            if(result.includes('user licence')) {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Warning!',
                        variant: 'warning',
                        message: this.label.LicenceWarningLabel,
                    }),
                );
            }
            else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: this.label.SuccessLabel,
                    }),
                );
            }
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Warning!',
                    variant: 'warning',
                    message: error.body.message,
                }),
            );
            console.log(error);
        });
    }

    removeUsersFromCswpGroup() {
        RemoveSelectedUsers({cswpGroupId:this.recordId, selectedUser:JSON.stringify(this.selectedRowsRemove)})
        .then(result => {
            //this.userSelectionData = result;
            this.isRemoveModalOpen = false;
            this.removeKeyword = '';
            this.getCswpUsers();
            this.getSelectionUsers();
            this.getSelectionRemoveUsers();
            //this.records = [...this.records];
            this.isRefreshNeeded = true;

            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: this.label.SuccessLabel,
                }),
            );
        })
        .catch(error => {
            console.log(error);
        });
    }

    setKeyword(event) {
        this.keyword = event.target.value;
        if(this.keyword.length > 0) {
            this.userSelectionData = this.userSelectionDataBackup.filter(x=> x.Name.toLowerCase().includes(this.keyword.toLowerCase()));
        }
        else {
            this.userSelectionData = this.userSelectionDataBackup;
        }
    }

    setRemoveKeyword(event) {
        this.removeKeyword = event.target.value;
        if(this.removeKeyword.length > 0) {
            this.removeUserSelectionData = this.removeUserSelectionDataBackup.filter(x=> x.Name.toLowerCase().includes(this.removeKeyword.toLowerCase()));
        }
        else {
            this.removeUserSelectionData = this.removeUserSelectionDataBackup;
        }
    }
}