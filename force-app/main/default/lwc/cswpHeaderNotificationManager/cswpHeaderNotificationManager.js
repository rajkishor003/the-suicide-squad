import { LightningElement, wire, api, track } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import cswpResources from '@salesforce/resourceUrl/cswp_resources';
import getUserNotifacations from '@salesforce/apex/CSWPNotificationController.getUserNotifacations';

import CSWP_No_Notification_Message from '@salesforce/label/c.CSWP_No_Notification_Message';
import CSWP_Notification_See_All from '@salesforce/label/c.CSWP_Notification_See_All';CSWP_Notification_Header
import CSWP_Notification_Header from '@salesforce/label/c.CSWP_Notification_Header';

export default class CswpHeaderNotificationManager extends NavigationMixin(LightningElement) {
    iconApps = cswpResources + '/icons/icon_apps.svg';
    iconAcs = cswpResources + '/icons/icon_acs.png';
    iconAdv = cswpResources + '/icons/icon_adv.png';
    notifications = new Array ();
    notificationNumber;
    isNotification;
    seeAll;

    label = {
        CSWP_No_Notification_Message,
        CSWP_Notification_See_All,
        CSWP_Notification_Header,
    }

    @wire(getUserNotifacations)
    wiredNotification({error, data}) {
        if(data) {
        //   console.log("-------data -----> ", data)
//            this.notifications = data;
            if(data.length > 5 ) {
                for(let i=0; i<5; i++){
                    this.notifications[i] = data[i];
                }
            }
            else {
                for(let i=0; i<data.length; i++){
                    this.notifications[i] = data[i];
                }
            }

            this.notificationNumber = data.length;
            this.isNotification = this.notificationNumber > 0 ? true : false;
            this.seeAll = this.notificationNumber > 5 ? true : false;

        } else if (error) {
            console.error("error -----> ", error);
        }
    }

    navigateToNotification(e) {
//        console.log("event id", e.currentTarget.dataset.id);
        var pageLink = `/cswp/s/cswp-notification-detail?recordId=${e.currentTarget.dataset.id}`;
        window.open(pageLink, '_self');
    }

    navigateToNotificationManager() {
        var pageLink = "/cswp/s/cswp-notification";
        window.open(pageLink, '_self');
/*         this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: 'CSWP_Notification__c'
            },
        }); */
    }
}