import { LightningElement,wire } from 'lwc';
import documentSharingPermission from '@salesforce/customPermission/ClientDocumentAccess';
import V93000LicensingPermission from '@salesforce/customPermission/V93000LicensingAccess';
import TechnicalDocumentPermission from '@salesforce/customPermission/TechnicalDocumentAccess';
import calBoxPermission from '@salesforce/customPermission/CalBoxAccess';

import document_sharing_overview from '@salesforce/label/c.cswp_document_sharing_overview';
import getNotifacationForLandingPage from '@salesforce/apex/CSWPNotificationController.getNotifacationForLandingPage';
import getNotificationDetail from '@salesforce/apex/CSWPNotificationDetailController.getNotificationDetail';

import getUserSystemNotifacations from '@salesforce/apex/CSWPNotificationController.getUserSystemNotifacations';
export default class CswpHomePageManager extends LightningElement {
    label = {
        document_sharing_overview
    }
    isModalOpen = false;
    message = {};
    serviceNotifictions= {};
    documentSharingAccess = documentSharingPermission;
    v93LicensesAccess = V93000LicensingPermission;
    technicalDocumentsAccess = TechnicalDocumentPermission;
    hasAccess = (documentSharingPermission || V93000LicensingPermission || TechnicalDocumentPermission || calBoxPermission);

    @wire(getNotifacationForLandingPage)
    wiredNotification({error, data}) {
        if(data ) {
            console.log("data -----> ", data);
            this.message = data;

        } else if (error) {
            console.error("error -----> ", error);
        }
    }

    @wire(getUserSystemNotifacations)
    wiredServiceNotification({error, data}) {
        if(data) {
            console.log(data);
            if (data.length > 0){
                this.isModalOpen = true;
                console.log("data -----> ", data);
                this.serviceNotifictions = data;
            }
            else {
                console.log("this.isModalOpen -----> ", this.isModalOpen);
                this.isModalOpen = false;
            }
        } else if (error) {
            console.error("error -----> ", error);
        }
    }

    renderedCallback() {
        let table = this.template.querySelector('c-cswp-last-modifiled-files-manager');
        if(table) table.refreshTable();

        let table2 = this.template.querySelector('c-cswp-last-added-files-manager');
        if(table2) table2.refreshTable();
    }

    closeModal() {
        this.isModalOpen = false;
/*         if(this.isModalOpen === false) {
            this.isModalOpen = true;
        } else {
            this.isModalOpen = false;
        }*/
    }
}