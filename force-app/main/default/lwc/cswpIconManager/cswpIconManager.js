import { LightningElement,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class CswpIconManager extends NavigationMixin(LightningElement) {
    @api iconName;
    @api alternativeText;
    @api title;
    @api pageName;
    @api size;

    navigatePage(event) {
        var page = event.target.dataset.pageName;
        
        if(page) {
            this[NavigationMixin.Navigate]({
                type: 'comm__namedPage',
                attributes: {
                    name: page
                }
            });
        }
    }
}