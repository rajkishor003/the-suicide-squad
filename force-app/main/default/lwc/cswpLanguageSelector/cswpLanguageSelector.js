import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import cswp_lang_english from '@salesforce/label/c.cswp_lang_english';
import cswp_lang_japanese from '@salesforce/label/c.cswp_lang_japanese';
import cswp_lang_label from '@salesforce/label/c.cswp_lang_label';
import cswp_lang_placeholder from '@salesforce/label/c.cswp_lang_placeholder';

export default class CswpLanguageSelector extends NavigationMixin(LightningElement) {
    url;
    value;
    langParam;

    label = {
        cswp_lang_label,
        cswp_lang_placeholder
    }

    get options() {
        return [{
                label: cswp_lang_english,
                value: 'en_US'
            },
            {
                label: cswp_lang_japanese,
                value: 'ja'
            },
        ];
    }

    getselectedLang() {
        this.langParam = window.location.search;
        this.langParam = this.langParam.split('=');
        this.value = this.langParam[1];
    }

    handleChange(event) {
        this.value = event.detail.value;
        this.url = window.location.origin + window.location.pathname + '?language=' + this.value;
        window.open(this.url,'_self');
    }

    connectedCallback() {
        this.getselectedLang();
    }
}