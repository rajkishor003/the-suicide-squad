import {LightningElement, track, wire, api } from 'lwc';
import recentlyAddedFiles from '@salesforce/label/c.cswp_recently_added_files';
import title from '@salesforce/label/c.cswp_title';
import owner from '@salesforce/label/c.cswp_owner';
import uploadTime from '@salesforce/label/c.cswp_upload_time';
import getLastlyAddedFiles from '@salesforce/apex/CSWPFilesController.getLastlyAddedFiles';
import getRefreshedLastlyAddedFiles from '@salesforce/apex/CSWPFilesController.getRefreshedLastlyAddedFiles';
import { NavigationMixin } from 'lightning/navigation';

export default class lastlyAddedFiles extends NavigationMixin(LightningElement) {
    label = {
        recentlyAddedFiles,
        title,
        owner,
        uploadTime
    }
    @track columns = [
        {
            label: this.label.title,
            fieldName: 'recordURL',
            type: 'url',
            typeAttributes: {label: { fieldName: 'fileName' },
            target: '_self'},
            sortable: false
        },
        {
            label: this.label.owner,
            fieldName: 'fileOwner',
            type: 'text',
            sortable: false
        },
        // {
        //     label: this.label.uploadTime,
        //     fieldName: 'fileDateTime',
        //     type: 'date',
        //     typeAttributes:{
        //         year: "numeric",
        //         month: "2-digit",
        //         day: "2-digit",
        //         hour: "2-digit",
        //         minute: "2-digit"
        //     }
        // }
        {
            label: this.label.uploadTime,
            fieldName:'fileDateTime',
            type: 'text',
            sortable: false
        }
    ];
    @track fileList = [];

    @wire(getLastlyAddedFiles)
    wiredlinks({error,data}) {
        if (data) {
            this.fileList = data;
        } else if (error) {
            console.error("error ----> ", error);
        }
    }

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }

    handleClick(e) {
        this.navigateToPage('Document_Sharing__c');
    }

    @api refreshTable() {
        getRefreshedLastlyAddedFiles()
        .then(result => {
            this.fileList = result;
        })
        .catch(error => {
            console.log(error);
        });
    }
}