import {LightningElement, track, wire, api} from 'lwc';
import recentlyDownloadedFiles from '@salesforce/label/c.cswp_recently_downloaded_files';
import title from '@salesforce/label/c.cswp_title';
import owner from '@salesforce/label/c.cswp_owner';
import downloadtime from '@salesforce/label/c.cswp_download_time';
import getLastlyDownloadedFiles from '@salesforce/apex/CSWPFilesController.getLastlyDownloadedFiles';
import getRefreshedLastlyDownloadedFiles from '@salesforce/apex/CSWPFilesController.getRefreshedLastlyDownloadedFiles';
import { NavigationMixin } from 'lightning/navigation';


export default class lastlyDownloadedFiles extends NavigationMixin(LightningElement) {
    label = {
        recentlyDownloadedFiles,
        title,
        owner,
        downloadtime
    }
    @track columns = [
        {
            label: this.label.title,
            fieldName: 'recordURL',
            type: 'url',
            typeAttributes: {label: { fieldName: 'fileName' },
            target: '_self'},
            sortable: false
        },
        {
            label: this.label.owner,
            fieldName: 'fileOwner',
            type: 'text',
            sortable: false
        },
        // {
        //     label: this.label.downloadtime,
        //     fieldName: 'fileDateTime',
        //     type: 'date',
        //     typeAttributes:{
        //         year: "numeric",
        //         month: "2-digit",
        //         day: "2-digit",
        //         hour: "2-digit",
        //         minute: "2-digit"
        //     }
        // }
        {
            label: this.label.downloadtime,
            fieldName: 'fileDateTime',
            type: 'text',
            sortable: false
        }
    ];
    @track fileList = [];

    @wire(getLastlyDownloadedFiles)
    wiredlinks({error,data}) {
        if (data) {
            this.fileList = data;
        } else if (error) {
            console.error("error ----> ", error);
        }
    }

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }

    handleClick(e) {
        this.navigateToPage('Document_Sharing__c');
    }

    @api refreshTable() {
        getRefreshedLastlyDownloadedFiles()
        .then(result => {
            this.fileList = result;
        })
        .catch(error => {
            console.log(error);
        });
    }
}