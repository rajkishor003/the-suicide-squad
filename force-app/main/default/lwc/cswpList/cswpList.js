import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class CswpList extends NavigationMixin(LightningElement) {
    @api link;
    @api title;
    @api titleLink = 'Default Link';
    @api linkTarget;
    @api item1;
    @api link1;
    @api item2;
    @api link2;
    @api item3;
    @api link3;
    @api item4;
    @api link4;
    @api item5;
    @api link5;

    handleClick(e) {
        switch (this.linkTarget) {
            case 'New Tab':
                window.open(this.titleLink, '_blank');
                break;
            case 'Current Tab':
                window.open(this.titleLink, '_self');
                break;
            case 'Community Page':
                this.navigateToPage(this.titleLink);
                break;
        }
    }

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }
}