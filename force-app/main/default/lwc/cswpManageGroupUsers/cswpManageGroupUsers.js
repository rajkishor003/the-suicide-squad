import { LightningElement, api, track } from 'lwc';
import {loadStyle} from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import relatedListResource from '@salesforce/resourceUrl/relatedListResource';
import getSelectableUsers from '@salesforce/apex/CSWPGroupController.getAvailableUsersToActivate';
import getSelectableDeactiveUsers from '@salesforce/apex/CSWPGroupController.getAvailableUsersToDeactivate';
import SubmitSelectedUsers from '@salesforce/apex/CSWPGroupController.MakeActiveCswpGroupUsers';
import GetCswpGroupUsers from '@salesforce/apex/CSWPGroupController.GetCswpGroupUsers';
import RemoveSelectedUsers from '@salesforce/apex/CSWPGroupController.MakeDeactiveCswpGroupUsers';
//import { refreshApex } from '@salesforce/apex';

import NameLabel from '@salesforce/label/c.cswp_label_name';
import EmailLabel from '@salesforce/label/c.cswp_label_email';
import UsernameLabel from '@salesforce/label/c.cswp_label_username';
import CloseLabel from '@salesforce/label/c.cswp_calboxtable_close';
import PopUpTitleLabel from '@salesforce/label/c.cswp_label_activateGroupUsers';
import CSWPGroupNameLabel from '@salesforce/label/c.cswp_group_name';
import IsActiveLabel from '@salesforce/label/c.cswp_is_active';
import PopUpTitleRemoveLabel from '@salesforce/label/c.cswp_label_deactivateGroupUsers';
import SubmitLabel from '@salesforce/label/c.cswp_submit';
import SearchLabel from '@salesforce/label/c.cswp_calbox_search_text';
import LicenceWarningLabel from '@salesforce/label/c.cswp_licence_warning';
import SuccessLabel from '@salesforce/label/c.cswp_contact_us_submit_success_title';

export default class CswpGroupUsers extends LightningElement {
    @api recordId;
    @track recordCount = 0;
    @track columns;
    @track records;
    @track recordsBackup;
    @track isModalOpen = false;
    @track isRemoveModalOpen = false;
    @track userSelectionColumns;
    @track userSelectionData;
    @track userSelectionDataBackup;
    @track removeUserSelectionColumns;
    @track removeUserSelectionData;
    @track removeUserSelectionDataBackup;
    @track selectedRows;
    @track selectedRowsRemove;
    @track isRefreshNeeded = false;
    @track keyword;
    @track tableKeyword;
    @track removeKeyword;
    @track sortBy;
    @track sortDirection;
    @track sortByOnActivateTable;
    @track sortDirectionOnActivateTable;
    @track sortByOnDeactivateTable;
    @track sortDirectionOnDeactivateTable;

    label = {
        NameLabel,
        EmailLabel,
        UsernameLabel,
        CloseLabel,
        PopUpTitleLabel,
        PopUpTitleRemoveLabel,
        SubmitLabel,
        SearchLabel,
        LicenceWarningLabel,
        SuccessLabel,
        CSWPGroupNameLabel,
        IsActiveLabel
    }

    columns = [
        { label: this.label.NameLabel, fieldName: 'Cswp_User_Name__c', sortable:'true', type: 'Text'},
        { label: this.label.CSWPGroupNameLabel, fieldName: 'Cswp_Group_Name__c', sortable:'true', type:'Text'},
      
        { label: this.label.EmailLabel, fieldName: 'Cswp_Email__c', sortable:'true', type:'Email'},
        { label: this.label.IsActiveLabel, fieldName: 'Cswp_Is_Active__c', sortable:'true', type:'boolean'}
        
    ];

    userSelectionColumns = [
        { label: this.label.NameLabel, fieldName: 'Cswp_User_Name__c', sortable:'true', type: 'Text'},
        { label: this.label.CSWPGroupNameLabel, fieldName: 'Cswp_Group_Name__c', sortable:'true', type:'Text'},
    
        { label: this.label.EmailLabel, fieldName: 'Cswp_Email__c', sortable:'true', type:'Email'}
    ];
    removeUserSelectionColumns = [
        { label: this.label.NameLabel, fieldName: 'Cswp_User_Name__c', sortable:'true', type: 'Text'},
        { label: this.label.CSWPGroupNameLabel, fieldName: 'Cswp_Group_Name__c', sortable:'true', type:'Text'},
      
        { label: this.label.EmailLabel, fieldName: 'Cswp_Email__c', sortable:'true', type:'Email'}
    ];
    renderedCallback() {
        loadStyle(this, relatedListResource + '/relatedList.css')
        if(this.isRefreshNeeded) {this.getCswpUsers();}
        if(this.userSelectionData == null) { this.getSelectionUsers();}
    }

    connectedCallback() {
        this.getCswpUsers();
        this.getSelectionUsers();
        this.getSelectionDeactiveUsers();
    }

    getCswpUsers() {
        GetCswpGroupUsers()
        .then(result => {
            this.records = result;
            this.recordsBackup = result;
            this.recordCount = result.length;
        })
        .catch(error => {
            console.log(error);
        });
        this.isRefreshNeeded = false;
    };

    getSelectionUsers() {
        getSelectableUsers()
        .then(result => {
            this.userSelectionData = result;
            this.userSelectionDataBackup = this.userSelectionData;
        })
        .catch(error => {
            console.log(error);
        });
    };

    getSelectionDeactiveUsers() {
        getSelectableDeactiveUsers()
        .then(result => {
            this.removeUserSelectionData = result;
            this.removeUserSelectionDataBackup = result;
        })
        .catch(error => {
            console.log(error);
        });
    };

    getSelectedUsers(event) {
        this.selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log(this.selectedRows);
    }

    getSelectedRemoveUsers(event) {
        this.selectedRowsRemove = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        console.log(this.selectedRowsRemove);
    }

    handleActivateRecord() {
        this.isModalOpen = true;
    }

    handleDeactivateRecord() {
        this.isRemoveModalOpen = true;
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
        this.isRemoveModalOpen = false;
    }

    submitDetails() {
        SubmitSelectedUsers({selectedUser:JSON.stringify(this.selectedRows)})
        .then(result => {
            //this.userSelectionData = result;
            this.isModalOpen = false;
            this.keyword = '';
            this.getCswpUsers();
            this.getSelectionUsers();
            this.getSelectionDeactiveUsers();
            //this.records = [...this.records];
            this.isRefreshNeeded = true;
            if(result.includes('user licence')) {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Warning!',
                        variant: 'warning',
                        message: this.label.LicenceWarningLabel,
                    }),
                );
            }
            else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: this.label.SuccessLabel,
                    }),
                );
            }
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Warning!',
                    variant: 'warning',
                    message: error.body.message,
                }),
            );
            console.log(error);
        });
    }

    removeUsersFromCswpGroup() {
        RemoveSelectedUsers({selectedUser:JSON.stringify(this.selectedRowsRemove)})
        .then(result => {
            //this.userSelectionData = result;
            this.isRemoveModalOpen = false;
            this.removeKeyword = '';
            this.getCswpUsers();
            this.getSelectionUsers();
            this.getSelectionDeactiveUsers();
            //this.records = [...this.records];
            this.isRefreshNeeded = true;

            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: this.label.SuccessLabel,
                }),
            );
        })
        .catch(error => {
            console.log(error);
        });
    }

    setKeyword(event) {
        this.keyword = event.target.value;
        console.log('Search Keyword ==> '+this.keyword);
        if(this.keyword.length > 0) {
            this.userSelectionData = this.userSelectionDataBackup.filter(x=> (x.Cswp_User_Name__c) && x.Cswp_User_Name__c.toLowerCase().includes(this.keyword.toLowerCase())||
                                                                        (x.Cswp_Group_Name__c)&&x.Cswp_Group_Name__c.toLowerCase().includes(this.keyword.toLowerCase()) ||
                                                                        (x.Cswp_Email__c)&&x.Cswp_Email__c.toLowerCase().includes(this.keyword.toLowerCase()) );
        }
        else {
            this.userSelectionData = this.userSelectionDataBackup;
        }
    }
    setTableKeyword(event) {
        this.tableKeyword = event.target.value;
        console.log('Search Keyword ==> '+this.tableKeyword);
        if(this.tableKeyword.length > 0) {
            this.records = this.recordsBackup.filter(x=> (x.Cswp_User_Name__c) && x.Cswp_User_Name__c.toLowerCase().includes(this.tableKeyword.toLowerCase())||
                                                                        (x.Cswp_Group_Name__c)&&x.Cswp_Group_Name__c.toLowerCase().includes(this.tableKeyword.toLowerCase()) ||
                                                                        (x.Cswp_Email__c)&&x.Cswp_Email__c.toLowerCase().includes(this.tableKeyword.toLowerCase()) );
        }
        else {
            this.records = this.recordsBackup;
        }
    }

    setRemoveKeyword(event) {
        this.removeKeyword = event.target.value;
        if(this.removeKeyword.length > 0) {
            this.removeUserSelectionData = this.removeUserSelectionDataBackup.filter(x=> (x.Cswp_User_Name__c) &&  x.Cswp_User_Name__c.toLowerCase().includes(this.removeKeyword.toLowerCase())||
            (x.Cswp_Group_Name__c)&& x.Cswp_Group_Name__c.toLowerCase().includes(this.removeKeyword.toLowerCase()) ||
            (x.Cswp_Email__c)&&x.Cswp_Email__c.toLowerCase().includes(this.removeKeyword.toLowerCase()) );
}
        else {
            this.removeUserSelectionData = this.removeUserSelectionDataBackup;
        }
    }

    doSorting(event) {
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection,'list');
    }
    doSortingOnActivateTable(event) {
        this.sortByOnActivateTable = event.detail.fieldName;
        this.sortDirectionOnActivateTable = event.detail.sortDirection;
        this.sortData(this.sortByOnActivateTable, this.sortDirectionOnActivateTable,'active');
    }
    doSortingOnDeactivateTable(event) {
        this.sortByOnDeactivateTable = event.detail.fieldName;
        this.sortDirectionOnDeactivateTable = event.detail.sortDirection;
        this.sortData(this.sortByOnDeactivateTable, this.sortDirectionOnDeactivateTable,'inactive');
    }
    sortData(fieldname, direction,tablename) {
        let parseData;
        if(tablename == 'list') {
            parseData = JSON.parse(JSON.stringify(this.records));
        }
        else if(tablename == 'active'){
            parseData = JSON.parse(JSON.stringify(this.userSelectionData));
        }
        else if(tablename == 'inactive'){
            parseData = JSON.parse(JSON.stringify(this.removeUserSelectionData));
        }
        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };
        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1: -1;
        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        if(tablename == 'list') {
            this.records = parseData;
        }
        else if(tablename == 'active'){
            this.userSelectionData = parseData;
        }
        else if(tablename == 'inactive'){
            this.removeUserSelectionData = parseData;
        }
    }  
}