import { LightningElement,wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import Nav_Item_Name_1 from '@salesforce/label/c.Nav_Item_Name_1';
import Nav_Item_Link_1 from '@salesforce/label/c.Nav_Item_Link_1';
import Nav_Item_Name_2 from '@salesforce/label/c.Nav_Item_Name_2';
import Nav_Item_Link_2 from '@salesforce/label/c.Nav_Item_Link_2';
import Nav_Item_Name_3 from '@salesforce/label/c.Nav_Item_Name_3';
import Nav_Item_Link_3 from '@salesforce/label/c.Nav_Item_Link_3';
import cswp_contact_us from '@salesforce/label/c.cswp_contact_us';
import cswp_topic_faq from '@salesforce/label/c.cswp_topic_faq';
import services from '@salesforce/label/c.cswp_services';

import getNavItemList from '@salesforce/apex/cswpNavManagerController.getNavItemList';

export default class CswpMenuManager extends NavigationMixin(LightningElement) {
    navitems = [];
    label = {
        Nav_Item_Name_1,
        Nav_Item_Link_1,
        Nav_Item_Name_2,
        Nav_Item_Link_2,
        Nav_Item_Name_3,
        Nav_Item_Link_3,
        cswp_contact_us,
        cswp_topic_faq,
        services
    }

    renderFAQ = false;

    @wire(getNavItemList)
    wiredlinks({error, data}) {
        var faqAccess;
        if(data) {
            this.navitems = data;
            this.navitems.forEach(function (item) {
                if (item.link === 'Topic_Catalog') {
                    faqAccess = true;
                }
            });
        } else if (error) {
            console.error("error -----> ", error);
        }
        this.renderFAQ = faqAccess;
    }

    goPage(event) {
        var pageName = event.currentTarget.dataset.name;
        var pageLink = '';
        if(pageName.includes('http')) {
            pageLink = event.currentTarget.dataset.name;
            window.open(pageLink, '_blank');
        } else {
            this[NavigationMixin.Navigate]({
                type: 'comm__namedPage',
                attributes: {
                    name: pageName
                }
            });
        }
    }
}