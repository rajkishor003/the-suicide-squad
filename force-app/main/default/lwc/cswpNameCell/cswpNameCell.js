import { LightningElement, api } from "lwc";

export default class CswpNameCell extends LightningElement {
  @api id;
  @api name;
  @api type;
  @api icon;
  @api breadcrumb;

  handleClick(evt) {
    evt.preventDefault();
    console.log("clicked!..." + this.id + " : " + this.name);
    this._dispatchDetailEvent();
    this._dispatchBreadcrumbEvent();
  }

  _dispatchDetailEvent() {
    let itemId;
    if (this.id.indexOf("-") > 0) {
      itemId = this.id.split("-")[0];
    }
    const customEvt = new CustomEvent("godetail", {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: {
        id: itemId,
        name: this.name,
        type: this.type
      }
    });
    this.dispatchEvent(customEvt);
  }

  _dispatchBreadcrumbEvent() {
    console.log("_dispatchedBreadCrumb evt path :> " + this.breadcrumb);
    const breadCrumbEvt = new CustomEvent("pathchanged", {
      composed: true,
      bubbles: true,
      detail: {
        path: this.breadcrumb
      }
    });
    this.dispatchEvent(breadCrumbEvt);
  }
}