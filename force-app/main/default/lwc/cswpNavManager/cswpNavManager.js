import { LightningElement,wire, api } from 'lwc';
import getNavItemList from '@salesforce/apex/cswpNavManagerController.getNavItemList';
import your_services from '@salesforce/label/c.cswp_your_services';

export default class CswpNavManager extends LightningElement {
    label = {
        your_services
    }
    navitems = [];
    @api hasAccess;
    @api message;

    @wire(getNavItemList)
    wiredlinks({error, data}) {
        if(data) {
            this.navitems = data;
            if(this.navitems.length>0){
                 this.hasAccess = true;
            }
           
        } else if (error) {
            console.error("error -----> ", error);
        }
    }
}