import { LightningElement,api } from 'lwc';
import cswp_noitemsfound from '@salesforce/label/c.cswp_noitemsfound';


export default class CswpNoRecord extends LightningElement {
    @api message;
    searchKey = '';
    messageFirst = '';
    messageLast = '';
    filtered = false;
    label = {
        cswp_noitemsfound
    };

    handleMessage(msg) {
        msg = msg.split('%');
        if(msg.length >= 3) {
            this.messageFirst = msg[0] + ' ';
            this.searchKey = msg[1] + ' ';
            this.messageLast = msg[2];
            this.filtered = true;
        } else {
            this.filtered = false;
        }
    }

    renderedCallback() {
        try {
            this.handleMessage(this.message);
        } catch(e) {
            console.log('ERROR : ', e.message);
        }
    }
}