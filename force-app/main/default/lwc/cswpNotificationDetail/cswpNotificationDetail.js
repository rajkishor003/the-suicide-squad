import { LightningElement,wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import getNotificationDetail from '@salesforce/apex/CSWPNotificationDetailController.getNotificationDetail';
import confirmNotification from '@salesforce/apex/CSWPConfirmedNotificationController.confirmNotification';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import CSWP_Confirm_Notification_Success_Title from '@salesforce/label/c.CSWP_Confirm_Notification_Success_Title';
import CSWP_Confirm_Notification_Success_Text from '@salesforce/label/c.CSWP_Confirm_Notification_Success_Text';
export default class CswpNotificationDetail extends NavigationMixin(LightningElement) {
    label = {


        CSWP_Confirm_Notification_Success_Title,
        CSWP_Confirm_Notification_Success_Text,

      

    }
    notificationDetail;
    content;
    title;
    date;
    notificationId;
    notificationUserMustConfirm;


    navigateToNotification(){
        console.log('navigate to home');
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: 'Home'
            },
        });
    }

    goHome() {
        setTimeout(() => {
            var pageLink = '/cswp/s';
            window.open(pageLink, '_self');
        }, 1000);
    }

    @wire(CurrentPageReference)
    setCurrentPageReference(page){
        this.currentPageReference = page;
        console.log('this.state: > %O', this.currentPageReference);
        if(this.currentPageReference.state){
            this.notificationId = this.currentPageReference.state.recordId;
        }
    }

    @wire(getNotificationDetail, {notificationId : '$notificationId'})
    notificationDetail({error, data}) {
        console.log('timezone -----> ', this.userTimeZone);
        if(data) {
            console.log('data -----> ', data);
            this.notificationDetail = data;
            this.content = data.notificationContent;
            this.title = data.notificationName;
            this.date = data.notificationStartDate;
            this.notificationUserMustConfirm = data.notificationUserMustConfirm;
        } else {
            console.error("error -----> ", error);
        }
    }

    confirmNotification(){
        let nId = this.notificationId;
        if(this.notificationId){
            confirmNotification({
                notificationId: nId
            }).then(confirmedNotificationId => {
                if (confirmedNotificationId) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: this.label.CSWP_Confirm_Notification_Success_Title,
                            variant: 'success',
                            message: this.label.CSWP_Confirm_Notification_Success_Text,
                        }),
                    );
                    this.goHome();
                   
                }
            }).catch(error => {
                console.log('error ', error);
            
            });
            } /*else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: this.label.cswp_contact_us_submit_error_title,
                        variant: 'error',
                        message: this.label.cswp_contact_us_submit_error_text,
                    }),
                );
                this.isLoading = false;
            }*/
       
    }
}