import { LightningElement,wire } from 'lwc';
import title from '@salesforce/label/c.cswp_title';
import getUserNotifacations from '@salesforce/apex/CSWPNotificationController.getUserNotifacations';

export default class CswpNotificationManager extends LightningElement {
    label = {
       
        title,
       
    }
    columns = [
        { label: this.label.title, 
            fieldName: 'recordURL',
            type: 'url',
            typeAttributes: {label: { fieldName: 'notificationName' },
            target: '_self'} },
        { label: 'Start Date', fieldName: 'notificationStartDate'},
    
    ];
    notifications = {};
    @wire(getUserNotifacations)
    wiredNotification({error, data}) {
        if(data) {
//            console.log("data -----> ", data)
            this.notifications = data;
        } else if (error) {
            console.error("error -----> ", error);
        }
    }
}