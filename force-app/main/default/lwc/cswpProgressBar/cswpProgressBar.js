import { LightningElement, wire, api } from 'lwc';
import storageLabel from '@salesforce/label/c.cswp_storage';
import gbUsed from '@salesforce/label/c.cswp_gb_used';
import of from '@salesforce/label/c.cswp_of';
import storageWarning from '@salesforce/label/c.cswp_Storage_90_Warning';
import getStorageInfo from '@salesforce/apex/CSWPProgressBarController.getStorageInfo';

export default class CswpProgressBar extends LightningElement {
    label = {
        storageLabel,
        gbUsed,
        of,
        storageWarning
    };
    @api progress = 0;
    storageLimit = 0;
    currentStorage = 0;
    showWarning;
    @wire(getStorageInfo)
    wiredlinks({error, data}) {
        if(data) {
            this.progress = data.progress;
            this.storageLimit = data.storageLimit;
            this.currentStorage = data.currentStorage;
            this.showWarning = this.progress >= 95;
        } else if (error) {
            console.error("error -----> ", error);
        }
    }
}