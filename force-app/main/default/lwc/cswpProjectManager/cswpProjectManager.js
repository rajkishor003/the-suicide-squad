import { LightningElement } from 'lwc';

let data = [
    { id: 1, dynamicIcon:'', title: 'Task 1', status: 'OPEN', remaining: 100 },
    { id: 2, dynamicIcon:'', title: 'Story 1', status: 'IN PROGRESS', remaining: 60 },
    { id: 4, dynamicIcon:'', title: 'Story 2', status: 'UNDER REVIEW', remaining: 20 },
    { id: 3, dynamicIcon:'', title: 'Task 3', status: 'DONE', remaining: 0 },
    { id: 5, dynamicIcon:'', title: 'Bug 1', status: 'CLOSED', remaining: 0 },
];

let columns = [
    { label: 'Title', fieldName: 'title', cellAttributes: { iconName: { fieldName: 'dynamicIcon' } } },
    { label: 'Status', fieldName: 'status' },
    { label: 'Remaining Hours', fieldName: 'remaining', type:'progress' },
];

export default class CswpProjectManager extends LightningElement {
    data = data;
    columns = columns;
}