import { LightningElement,track } from 'lwc';

export default class CswpProjectNew extends LightningElement {
    @track isModalOpen = false;
    openModal() {
        this.isModalOpen = true;
    }
    closeModal() { 
        this.isModalOpen = false;
    }
    submit() {
        console.log('submit BD stuff');
        this.isModalOpen = false;
    }
}