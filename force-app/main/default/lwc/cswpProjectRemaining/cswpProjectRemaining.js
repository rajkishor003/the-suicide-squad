import { LightningElement, api } from 'lwc';

export default class CswpProjectRemaining extends LightningElement {
    @api remainingHours;
}