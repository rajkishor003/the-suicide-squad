import LightningDatatable from 'lightning/datatable';
import cswpProgressControll from './cswpProgressControl.html';

export default class CswpProjectTable extends LightningDatatable  {
    static customTypes = {
        progress: {
            template: cswpProgressControll
        }
    };
}