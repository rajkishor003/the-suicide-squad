import { LightningElement } from 'lwc';
import cswp_system_handle from '@salesforce/label/c.cswp_system_handle';
import cswp_system_handle_helper from '@salesforce/label/c.cswp_system_handle_helper';
import cswp_or from '@salesforce/label/c.cswp_or';
import cswp_pin_code from '@salesforce/label/c.cswp_pin_code';
import cswp_submit from '@salesforce/label/c.cswp_submit';

export default class CswpSupportRequest extends LightningElement {
    label = {
        cswp_system_handle,
        cswp_system_handle_helper,
        cswp_or,
        cswp_pin_code,
        cswp_submit
    }
}