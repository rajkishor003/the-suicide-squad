import { LightningElement,wire } from 'lwc';
import title from '@salesforce/label/c.cswp_title';
import getSystemNotifacations from '@salesforce/apex/CSWPNotificationController.getUserSystemNotifacations';

export default class CswpSystemNotificationManager extends LightningElement {
    label = {
       
        title,
       
    }
    columns = [
        { label: this.label.title, 
            fieldName: 'recordURL',
            type: 'url',
            typeAttributes: {label: { fieldName: 'notificationName' },
            target: '_self'} },
        { label: 'Start Date', fieldName: 'notificationStartDate'},
        
    
    ];
    notifications = {};
    @wire(getSystemNotifacations)
    wiredNotification({error, data}) {
        if(data) {
            console.log("data -----> ", data)
            this.notifications = data;
        } else if (error) {
            console.error("error -----> ", error);
        }
    }
}