import {LightningElement, track, wire} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import technicalDocuments from '@salesforce/label/c.cswp_technical_documents';
import title from '@salesforce/label/c.cswp_title';
import getFolders from '@salesforce/apex/CSWPFilesController.getFolders';


export default class CswpTechDocManager extends NavigationMixin(LightningElement) {
    label = {
        technicalDocuments,
        title
    };
    @track columns = [
        {
            label: this.label.title,
            fieldName: 'recordURL',
            type: 'url',
            typeAttributes: {label: { fieldName: 'folderName' },
            target: '_self'},
            sortable: false
        }
    ];
    @track fileList = [];
    @wire(getFolders)
    wiredlinks({error,data}) {
        if (data) {
            this.fileList = data;
        } else if (error) {
            console.error("error ----> ", error);
        }
    }

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }

    handleClick(e) {
        this.navigateToPage('Technical_Documentation__c');
    }
}