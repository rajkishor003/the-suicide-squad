import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import desertSVG from '@salesforce/resourceUrl/desertSVG';
import getFeedItems from '@salesforce/apex/CswpTicketDetailController.getFeedItems';
import createFeedItem from '@salesforce/apex/CswpTicketDetailController.createFeedItem';
import getFiles from '@salesforce/apex/CswpTicketDetailController.getFiles';
import uploadFile from '@salesforce/apex/CswpTicketDetailController.uploadFile';
import deleteFileFromTicket from '@salesforce/apex/CswpTicketDetailController.deleteFile';
import LOCALE from '@salesforce/i18n/locale';

export default class CswpTicketDetail extends NavigationMixin(LightningElement) {

    // file related variables
    @api recordId;
    uploadedFiles;
    fileName;
    file;
    fileData;
    @track ticketComments = [];
    ticketFiles = [];
    desertSVG = '${desertSVG}#sfdesert'; //Placeholder image when ticket has no comments / files.
    showFiles = false;
    showComments = false;
    @track openDeleteModal = false;
    @track selectedFileID;
    @track selectedFileTitle;
    @track dontPreviewFile = false;
    @track attachFileID ;
    getBaseUrl(){
        let baseUrl = 'https://' + location.host + '/'; // eslint-disable-line no-restricted-globals
        return baseUrl;
    }

    getCapitalLetter(word){
        if(word.charAt(0) === word.charAt(0).toUpperCase()) {
            return word.charAt(0);
        }
        return null;
    }

    onFileUpload(event) {
        const file = event.target.files[0]
        var reader = new FileReader()
        reader.onload = () => {
            var base64 = reader.result.split(',')[1]
            this.fileData = {
                'filename': file.name,
                'base64': base64,
                'recordId': this.recordId
            }
            uploadFile({ base64 : this.fileData.base64, filename : this.fileData.filename, recordId : this.recordId }).then(result=>{
            if(result) {
                let title = this.fileData.filename + ' uploaded successfully!!'
                this.toast(title,'success')
                this.fileData = null
                this.handleFiles();
            }
        }) .catch(error => {
            console.error(error);
        })
        }
        reader.readAsDataURL(file)
    }

    handleComment() {
        const editor = this.template.querySelector('lightning-input-rich-text');

        if (!editor.value || editor.value === undefined) {
            this.toast('Please enter a valid comment', 'error');
        } else {
            createFeedItem({
                currentCaseId: this.recordId,
                value: editor.value
            })
            .then(() => {
                this.handleFeedItems();
                this.handleFiles();
                editor.value = '';
            })
            .catch((error) => {
                console.log('error' + error);
            });
        }
    }

    handleFeedItems() {
        getFeedItems({
                currentCaseId: this.recordId
            })
            .then((result) => {
                if (result) {
                    this.ticketComments = result.map(row => ({
                        ...row,
                        id: row.Id,
                        name: row.CreatedBy.Name,
                        initials: this.getCapitalLetter(row.CreatedBy.FirstName) + this.getCapitalLetter(row.CreatedBy.LastName),
                        date: this.calcCommentDateDifference (row.CreatedDate),
                        content:  this.handleFeedBody(row.Body) + this.addFeedAttachments(row.FeedAttachments)
                    }));
                }
            })
            .catch((error) => {
                console.log('error' + error);
            });
    }

    handleFeedBody(body){
        body = body.replaceAll('&lt;','<');
        body = body.replaceAll('&gt;','>');
        body = body.replaceAll('&quot;','"');
        body = body.replaceAll('&nbsp;',' ');
        body = body.replaceAll('#jiracomment','');
        body = body.replaceAll('#sf','');
        return body;
    }
    addFeedAttachments(feedAttachments){
        let attachmentURL = '';
        let elementWithAttachment = ''
        if(feedAttachments) {
            feedAttachments.forEach(
                element => attachmentURL += '<a href='+this.getBaseUrl() + 'cswp/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+element.RecordId+' ><img src='+this.getBaseUrl() + 'cswp/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+element.RecordId+'></img></a>'
            )
        }
        if(attachmentURL) {
            elementWithAttachment = '<div class="slds-grid slds-wrap">'+attachmentURL + '</div>';
        }
        return elementWithAttachment
    }
    
    handleFiles() {
        let imageExtensions = ['png', 'jpg', 'gif', 'jpeg'];
        let supportedIconExtensions = ['ai', 'attachment', 'audio', 'box_notes', 'csv', 'eps', 'excel', 'exe',
            'flash', 'folder', 'gdoc', 'gdocs', 'gform', 'gpres', 'gsheet', 'html', 'image', 'keynote', 'library_folder',
            'link', 'mp4', 'overlay', 'pack', 'pages', 'pdf', 'ppt', 'psd', 'quip_doc', 'quip_sheet', 'quip_slide',
            'rtf', 'slide', 'stypi', 'txt', 'text', 'unknown', 'video', 'visio', 'webex', 'word', 'xml', 'zip', 'mov'
        ];
        let nonPreviewableExtentions = ['mov', 'zip', 'video'];

        getFiles({
                recordId: this.recordId
            })
            .then(result => {
                let parsedData = JSON.parse(result);
                let stringifiedData = JSON.stringify(parsedData);
                let finalData = JSON.parse(stringifiedData);
                let baseUrl = this.getBaseUrl();

                finalData.forEach(file => {
                    let dt = new Date(file.CreatedDate);
                    file.date = this.calcDateDifference (dt);
                    file.name = file.Title;
                    file.fileID = file.ContentDocumentId;
                    file.downloadUrl = baseUrl + 'cswp/sfc/servlet.shepherd/document/download/' + file.ContentDocumentId;
                    file.fileUrl = baseUrl + 'cswp/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=' + file.Id;
                    file.CREATED_BY = file.ContentDocument.CreatedBy.Name;
                    file.size = this.formatBytes(file.ContentDocument.ContentSize, 2);
                    file.fileType = file.ContentDocument.FileType.toLowerCase();
                    let fileType = file.ContentDocument.FileType.toLowerCase();

                    if (nonPreviewableExtentions.includes(fileType)) {
                        file.isPreviewable = false;
                    } else {
                        file.isPreviewable = true;
                    }

                    if (imageExtensions.includes(fileType)) {
                        file.icon = 'doctype:image';
                    } else {
                        if (supportedIconExtensions.includes(fileType)) {
                            switch (fileType) {
                                case 'text':
                                    file.icon = 'doctype:txt';
                                    break;
                                case 'mov':
                                    file.icon = 'doctype:video';
                                    break;
                                default:
                                    file.icon = 'doctype:' + fileType;
                            }
                        } else {
                            file.icon = 'doctype:unknown';
                        }
                    }
                });
                this.ticketFiles = finalData;
            })
            .catch(error => {
                console.error('**** error **** \n ', error)
            })
    }

    formatBytes(bytes, decimals) {
        if (bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    calcDateDifference (cdate) {
        const options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
            hour12: true
        };
        let today = new Date;
        let initialDate = new Intl.DateTimeFormat(LOCALE, options).format(cdate);
        let ticketDate = new Date(initialDate.split(',')[0]);
        var daysAgo = (today.getTime() - ticketDate.getTime()) / (1000 * 3600 * 24);
        var parsedDaysAgo = parseInt(daysAgo); // eslint-disable-line radix

        if (parsedDaysAgo < 15 && parsedDaysAgo !== 0) {
            return parsedDaysAgo + ' days ago';
        } else if (parsedDaysAgo === 0) { // eslint-disable-line no-else-return
            return 'today';
        } else { // eslint-disable-line no-else-return
            return initialDate;
        }
    }

    calcCommentDateDifference(cdate) {
        let today = new Date;
        let initialDate = cdate
        let commentDate = new Date(cdate.slice(0, 10));
        let daysAgo = (today.getTime() - commentDate.getTime()) / (1000 * 3600 * 24);
        var parsedDaysAgo = parseInt(daysAgo); // eslint-disable-line radix

        if (parsedDaysAgo < 15 && parsedDaysAgo !== 0) {
            return parsedDaysAgo + ' days ago';
        } else if (parsedDaysAgo === 0) { // eslint-disable-line no-else-return
            return 'today';
        } else { // eslint-disable-line no-else-return
            return initialDate;
        }
    }

    previewFile(event){
        if(!this.dontPreviewFile){
            let isPreviewable = event.currentTarget.dataset.previewable;
            if (isPreviewable === 'true') {
                let url = event.currentTarget.dataset.fileurl;
                this[NavigationMixin.Navigate]({
                    type: 'standard__webPage',
                    attributes: {
                        url: url
                    },
                }).catch((error) => {
                    console.log('error' + error);
                });
            } else {
                this.toast ('No preview available.', 'error');
            }
        }
    }

    downloadFile(event){
        let url = event.target.dataset.downloadurl;
        this[NavigationMixin.Navigate]({
                type: 'standard__webPage',
                attributes: {
                    url: url
                }
            }, false
        );
    }

    deleteFile(event){
        this.dontPreviewFile = true;
        this.openDeleteModal = true;
        this.selectedFileID = event.target.dataset.fileid;
        this.selectedFileTitle = event.target.dataset.filename;
    }

    submitDeleteFile(){
        this.openDeleteModal = false;
        this.dontPreviewFile = false;
        deleteFileFromTicket({
            fileID : this.selectedFileID
        })
        .then((result) => {
            if(result == 'SUCCESS'){
                this.toast(this.selectedFileTitle + ' deleted successfully!', 'success');
                this.handleFiles();
                this.handleFeedItems();
            }
        })
        .catch((error) => {
            this.toast(error,'error');
        })
    }

    closeWarningModal(){
        this.selectedFileID = '';
        this.selectedFileTitle = '';
        this.openDeleteModal = false;
        this.dontPreviewFile = false;
    }
    connectedCallback () {
        this.handleFiles();
        this.handleFeedItems();
        this.showFiles = (this.ticketFiles ? true : false);
        this.showComments = (this.ticketComments ? true : false);
    }

    toast(title, variant){
        const toastEvent = new ShowToastEvent({
            title,
            variant: variant
        })

        this.dispatchEvent(toastEvent)
    }
}