import { LightningElement, api, track ,wire} from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import getOpenTickets from '@salesforce/apex/CswpTicketManagerController.getOpenTickets';
import getMyTickets from '@salesforce/apex/CswpTicketManagerController.getMyTickets';
import OpenTickets from '@salesforce/label/c.Cswp_ae_open_tickets';
import TicketsReportedByMe from '@salesforce/label/c.Cswp_AE_Tickets_Reporter_By_Me';
import NoMoreRecords from '@salesforce/label/c.Cswp_AE_no_more_records';
import {refreshApex} from '@salesforce/apex';
export default class CswpTicketManager extends LightningElement {

    label = {
        OpenTickets,
        TicketsReportedByMe,
        NoMoreRecords
    };

    @track ticketList = [];
    @track myTicketList = [];
    @track currentRecordId;
    @track caseOffset = 0;
    @track myCaseOffset = 0;
    @track showLoadMoreIcon = false;
    @track showLoadMoreIconForMyCases = false;
    @api recordId;
    @wire(CurrentPageReference)
    pageRef;

    @wire(getOpenTickets , {currentCaseId : '$recordId' , caseOffset : '$caseOffset'})
    wiredTickets({error,data}) {
        if (data) {
            if (data.length > 0) {
                if(!this.showLoadMoreIcon) {
                    this.showLoadMoreIcon = true;
                }
                this.ticketList = this.ticketList.concat(data);
            }
            else {
                this.showLoadMoreIcon = false;
            }
        } else if (error) {
            console.error("error ----> ", error);
        } else {
            this.showLoadMoreIcon = false;
        }
    }

    @wire(getMyTickets , {currentCaseId : '$recordId' , caseOffset : '$myCaseOffset'})
    wiredMyTickets({error,data}) {
        if (data) {
            if (data.length > 0) {
                if(!this.showLoadMoreIconForMyCases) {
                    this.showLoadMoreIconForMyCases = true;
                }
                this.myTicketList = this.myTicketList.concat(data);
            }
            else {
                this.showLoadMoreIconForMyCases = false;
            }

        } else if (error) {
            console.error("error ----> ", error);
        } else {
            this.showLoadMoreIconForMyCases = false;
        }
    }

    goToCase(event) {
        window.open(window.location.origin + '/cswp/s/case/' + event.target.dataset.id , "_self");
    }

    loadMoreCases() {
        this.caseOffset = this.caseOffset + 5 ;
        refreshApex(this.wiredTickets);
    }

    loadMoreMyCases() {
        this.myCaseOffset = this.myCaseOffset + 5 ;
        refreshApex(this.wiredMyTickets);
    }
}