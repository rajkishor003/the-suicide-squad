//import { LightningElement } from 'lwc';
import eventLog from "@salesforce/apex/CSWPWorkspaceManagerController.eventLog";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
const logActivity = async (itemInfo, actionName, customMessage) => {
  let result = await eventLog({
    message: {
      itemName: itemInfo.name,
      itemId: itemInfo.id,
      parentItemName: itemInfo.filepath,
      actionName: actionName,
      message: customMessage ? customMessage : ""
    }
  });
  console.log("logActivity result.Result :>", result);
  return result;
};

//detail: title, message
const showSuccessToast = (lwcElm,detail)=>{
  lwcElm.dispatchEvent(new ShowToastEvent(
    {...detail, variant:'success'}
  ));
}

//detail: title, message
const showErrorToast = (lwcElem,detail)=>{
  lwcElem.dispatchEvent(new ShowToastEvent(
    {...detail, variant:'error'}
  ));
}
    
export { logActivity, showSuccessToast,showErrorToast };