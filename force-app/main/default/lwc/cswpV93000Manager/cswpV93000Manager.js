import {LightningElement, track, wire} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getV93000Links from '@salesforce/apex/CSWP_V93000ManagerController.getV93000Links';
import v93000Licenses from '@salesforce/label/c.cswp_v93000_licenses';

export default class V93000Links extends NavigationMixin(LightningElement) {
    label = {
        v93000Licenses
    };
    @track columns = [
        {
            label: 'V93000 Licences',
            fieldName: 'v93000URL',
            type: 'url',
            typeAttributes: {label: { fieldName: 'v93000Title' },
            target: '_blank'},
            sortable: true
        }
    ];

    @track v93000Links = [];

    @wire(getV93000Links)
    wiredlinks({error,data}) {
        if (data) {
            this.v93000Links = data;
        } else if (error) {
            console.error("error ----> ", error);
        }
    }

    navigateToPage(apiName) {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: apiName
            }
        });
    }

    handleClick(e) {
        this.navigateToPage('V93000_Licensing__c');
    }
}