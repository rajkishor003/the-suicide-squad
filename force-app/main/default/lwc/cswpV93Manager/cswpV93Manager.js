import {LightningElement, track, wire} from 'lwc';
import getV93000LinksDetail from '@salesforce/apex/CSWP_V93000ManagerController.getV93000LinksDetail';
import topic_v93000_licensing from '@salesforce/label/c.cswp_topic_v93000_licensing';

export default class CswpV93Manager extends LightningElement {

    @track v93000Links;
    @wire(getV93000LinksDetail)
    wiredlinks({error,data}) {
        if (data) {
            this.v93000Links = data;
        } else if (error) {
            console.error("error ----> ", error);
            this.v93000Links = undefined;
        }
    }

    label = {
        topic_v93000_licensing
    }
}