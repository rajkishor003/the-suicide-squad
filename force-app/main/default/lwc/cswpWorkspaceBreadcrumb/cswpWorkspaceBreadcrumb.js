import { LightningElement, api, track } from "lwc";

export default class CswpWorkspaceBreadcrumb extends LightningElement {
  @api workspaceId;
  @track breadcrumbs;
  _path;
  
  @api get breadcrumb(){
    return this._path;
  }

  set breadcrumb(value) {
    this._path = value;
    this.setAttribute('breadcrumb', this._path);
    // console.log("set.breadcrumb.value >>> ", this._path);
    this._createBreadcrumbs(this._path); 
  }

  _createBreadcrumbs(value){
    this.breadcrumbs = []
    if (value) {
       value.split('/').forEach(p => {
          let arr = p.split(':');
          this.breadcrumbs.push({label: arr[0], name: arr[1], id: arr[1] });
       });
    } else {
      //this.breadcrumbs = [ {label: "Libraries", name: "Libraries", id: this.workspaceId }];
    }
  }
  handleNavigateTo(event) {
    event.preventDefault();
    // console.log("HandleNavigateTo.itemId>>>> ", event.target.name);
    let itemId = event.target.name;
    const navigateEvent = new CustomEvent('navigated',{
      bubbles: true, 
      detail:{
        itemId: itemId
      }
    });
    this.dispatchEvent(navigateEvent);
    this._refreshBreadcrumb(itemId);
  }
  _refreshBreadcrumb(itemId){
     // console.log('_RefreshBreadCrumb.itemId:>' , itemId);
    if(this.breadcrumbs && this.breadcrumbs.length>0){
      // console.log('array.breadcrumbs:>>%O', JSON.parse(JSON.stringify(this.breadcrumbs)));  
      let ind = this.breadcrumbs.findIndex( item=> item.name === itemId );
      // console.log('crumb.index:>>' + ind);
      const newPath = this.breadcrumbs.slice(0,ind+1).map(item=> item.label + ':' +item.name).join('/');
      this._path = newPath;
      this.setAttribute('breadcrumb',this._path);
      this._createBreadcrumbs(this._path);
      //this.breadcrumbs = Array.from( this.breadcrumbs.slice(0,ind+1));  
    }
  }
}