import { api, LightningElement, track, wire } from "lwc";
import { getRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getCurrentUserInfo from "@salesforce/apex/CSWPWorkspaceManagerController.getCurrentUserInfo";
import cswp_nav_item_shared_libraries from '@salesforce/label/c.cswp_nav_item_shared_libraries';
import cswp_nav_item_product_documents from '@salesforce/label/c.cswp_nav_item_product_documents';
import cswp_action_create_folder from '@salesforce/label/c.cswp_action_create_folder';
import cswp_action_search from '@salesforce/label/c.cswp_action_search';
import cswp_workspace_createnewfolder from '@salesforce/label/c.cswp_workspace_createnewfolder';
import cswp_Success_Label from '@salesforce/label/c.cswp_Success_Label';
import cswp_label_created from '@salesforce/label/c.cswp_label_created';

const FIELDS = [
  "cswp_Workspace__c.Name",
  "cswp_Workspace__c.cswp_Breadcrumb__c",
  "cswp_Workspace__c.cswp_ContentSize__c",
  "cswp_Workspace__c.cswp_Description__c"
];

export default class CswpWorkspaceManager extends LightningElement {
  @api workspaceType; //Product or Client Document? // Check the Workspace Type datasource
  
  @track workspaceId = null;
  @track path;

  docSharing;
  isCommunity;
  _workspaceFields;
  folderId;
  
  get isSearchable(){
    return (this.workspaceId && this.workspaceId !== 'recent');
  }

  labels = {
    cswp_nav_item_shared_libraries,
    cswp_action_create_folder,
    cswp_action_search,
    cswp_workspace_createnewfolder,
    cswp_Success_Label,	
    cswp_label_created
  }

  @wire(getCurrentUserInfo, {
    workspaceId: "$workspaceId",
    workspaceType: "$workspaceType"
  })
  userInfo;

  @wire(getRecord, { recordId: "$workspaceId", modes:"View", fields:FIELDS })
  workspace({ error, data }) {
    if (data && data.fields) {
      console.log(
        "data.fields.cswp_Breadcrumb__c.value: " +
          data.fields.cswp_Breadcrumb__c.value
      );
      this._workspaceFields = data.fields;
      this.path = data.fields.cswp_Breadcrumb__c.value;
    }
    if (error) {
      console.error(error);
    }
  }

  //workspaceChanged!
  handleSelection(event) {
    try{
      console.log("WorkspaceManager.handleSelection.event==> ", event.detail);
      //reset folderId;
      this.folderId = null;
      if(event.detail === 'recent'){        
        this.workspaceId = null;
      }else{
        this.workspaceId = event.detail;
      }
      //refresh SubscriptionButton
      let wstable = this.template.querySelector("c-cswp-workspace-subscription");
      if(wstable) wstable.refreshSubscriptionButton(event.detail);
      //fetch workspace Items
      const dataTable = this.template.querySelector("c-cswp-workspace-table");
      if(dataTable){
        dataTable.getWorkspaceItems(this.workspaceId);
      }
      //Set breadcrumb path again  
      const crumb = this.template.querySelector('c-cswp-workspace-breadcrumb');
      if(crumb && this._workspaceFields?.cswp_Breadcrumb__c?.value){
        crumb.breadcrumb = this.path = this._workspaceFields.cswp_Breadcrumb__c.value;
      } 
      //setFileUploadId   
      this.setFileUploadRecordId(this.workspaceId); 
    }catch(err){
      console.error(err);
    }
    
  }

  handlePathChanged(event) {
    console.log('cswpWorkspaceManager.handlePathChange:>>>>', event.detail.path);
    //this.path = event.detail.path;
    //sometimes this.path does not set the value somehow!!
    const cmp_breadcrump = this.template.querySelector('c-cswp-workspace-breadcrumb');
    if(cmp_breadcrump){
      cmp_breadcrump.breadcrumb = event.detail.path;
    }
    
  }

  handleNavigation(event) {
    let itemId = event.detail.itemId;
    console.log("handleNavigation.Id>>> ", itemId);
    if(itemId === this.workspaceId){
      console.log('itemId=workspaceId then reset folderId');
      this.folderId = null;
    }else{
      console.log('handleNavigation.FolderId.is:>' + this.folderId);
      this.folderId = itemId;
    }
    let dataTable = this.template.querySelector("c-cswp-workspace-table");
    dataTable.navigateToItem(itemId);
    this.setFileUploadRecordId(itemId);
  }

  handleSearch(evt){
    const searchTerm = evt.target.value;
    const table = this.template.querySelector('c-cswp-workspace-table');
    if(searchTerm.length >= 0 && searchTerm.length < 2 && table){
      table.refreshTable();
    }
    else if(table){
      table.searchInWorkspace(searchTerm);
    }    
  }

  resetSearhInput(){
    const searchInput = this.template.querySelector('lightning-input');
    if(searchInput){
      searchInput.value = '';
    }
  }

  //folder changed!
  handleSpaceChanged(event) {
    let type = event.detail.type;
    if (type === "folder") {
      //this.workspaceId = event.detail.id;
      this.folderId = event.detail.id;
      this.setFileUploadRecordId(this.folderId);
    }
  }

  setFileUploadRecordId(parentId){
    const cmpFileUpload = this.template.querySelector("c-cswp-file-upload");
    if(cmpFileUpload){
      cmpFileUpload.recordId = parentId; 
    }
  }

  openCreateModal() {
    let form = this.template.querySelector("c-edit-record-modal");
    form.objectApiName = "cswp_Folder__c";
    form.recordId = ""; //to be created!
    let fields = [
      { key: 0, name: "Name", value: "", disable: false, required: true },
      { key: 1, name: "cswp_Description__c", value: "", disable: false, required: false },
      { key: 2, name: "cswp_Workspace__c", value: this.workspaceId, disable: true },
      { key: 3, name: "cswp_ParentFolder__c", value: this.folderId, disable: true } ];

    form.fields = fields;
    form.header = this.labels.cswp_workspace_createnewfolder;
    form.showModal = true;
    //this.showCreate = true;
  }

  handleSuccessForm(event) {
    event.preventDefault();
    let record = event.detail;
    let form = this.template.querySelector("c-edit-record-modal");
    this.dispatchEvent(
      new ShowToastEvent({
        title: this.labels.cswp_Success_Label,
        message: record.fields.Name.value + ' ' + this.labels.cswp_label_created,
        variant: "success"
      })
    );
    form.showModal = false;
    let table = this.template.querySelector("c-cswp-workspace-table");
    table.refreshTable();
  }

  get isClient() {
    return this.workspaceType === "Document Sharing";
  }
  get isTechnical() {
    return this.workspaceType === "Technical Documents";
  }

  get navigationLabel() {
    return this.isClient ? cswp_nav_item_shared_libraries : cswp_nav_item_product_documents;
  }
  get canFileUpload() {
    let user = this.userInfo.data;
    return this.workspaceId && this.workspaceId !== 'recent' && user && user.permissions && user.permissions.canUpload;
  }
  get canCreateFolder() {
    let user = this.userInfo.data;
    return this.workspaceId && this.workspaceId !== 'recent' && user && user.permissions && user.permissions.canCreateFolder;
  }
  get canSubscribe() {
    return (this.workspaceId && this.workspaceId !== 'recent');
  }

  handleRowActionDone(event) {
    this.template.querySelector('c-cswp-last-modifiled-files-manager').refreshTable();
  }
  handleFileUploadDone(event) {
    this.template.querySelector('c-cswp-last-added-files-manager').refreshTable();
  }

  renderedCallback() {
    let table = this.template.querySelector('c-cswp-last-modifiled-files-manager');
    if(table) table.refreshTable();

    let table2 = this.template.querySelector('c-cswp-last-added-files-manager');
    if(table2) table2.refreshTable();
  }
}