import { api, LightningElement,track,wire } from 'lwc';

export default class CswpWorkspaceManager extends LightningElement {
    @api workspaceType; //Technical or Client Document?

    @track workspaceId;
    docSharing;
    isCommunity;

    connectedCallback() {
    }
    
    handleSelection(event){
        console.log('event handled ==> ', event.detail);
        this.workspaceId = event.detail;
    }
    get isClient(){
        return (this.workspaceType === 'Client Document');
    }
    get isTechnical(){
        return (this.workspaceType === 'Technical Document');
    }
}