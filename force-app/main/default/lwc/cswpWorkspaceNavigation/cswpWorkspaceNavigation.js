import { LightningElement,api, track, wire } from 'lwc';
import getWorkspaces from "@salesforce/apex/CSWPWorkspaceManagerController.getWorkspaces";
import cswp_nav_item_other from '@salesforce/label/c.cswp_nav_item_other';
import cswp_nav_item_recent from '@salesforce/label/c.cswp_nav_item_recent';
import cswp_nav_item_V93K_SOC from '@salesforce/label/c.cswp_nav_item_V93K_SOC';
import cswp_nav_item_V93K_SOC_URL from '@salesforce/label/c.cswp_nav_item_V93K_SOC_URL';
import cswp_nolibrary from '@salesforce/label/c.cswp_nolibrary';
import { CurrentPageReference,NavigationMixin } from 'lightning/navigation';


export default class CswpWorkspaceNavigation extends NavigationMixin(LightningElement) {
    @api label;
    @api isClient;
    @track workspaces;
    _workspacetype;
    selectedItem;
    isLoading=true;
    //error;

    labels = {
        cswp_nav_item_other,
        cswp_nav_item_recent,
        cswp_nolibrary,
        cswp_nav_item_V93K_SOC,
        cswp_nav_item_V93K_SOC_URL
    }

    @api get workspaceType(){
        return this._workspacetype;
    }
    set workspaceType(value){
        this.setAttribute('workspaceType',value);
        this._workspacetype = value;
        this.fetchWorkspaces();
    }

    _urlRecordId;
    _currentPage;
    
    @wire(CurrentPageReference)
    setCurrentPageReference(page){
        this._currentPage = page;
        //console.log('this._currentPage: > %O', this._currentPage);
        if(this._currentPage.state && this._currentPage.state.recordId){
            this._urlRecordId = this._currentPage.state.recordId;
        }
    }

    async fetchWorkspaces(){
        this.isLoading =true;
        let result=[];
        try {
            result = await getWorkspaces({workspaceType: this.workspaceType});
            if(result && result.length>0){
                this.workspaces = result;
                if(this._workspacetype === 'Document Sharing'){
                    this.selectedItem = 'recent';
                }else if(this._urlRecordId){
                    console.log('urlRecordId:>',this._urlRecordId);
                    this.selectedItem = this._urlRecordId;
                }else{
                    this.selectedItem = result[0].Id;
                }
            }                     
        } catch (error) {
            console.error('fetch workspaces error: >',error);            
        }
        this.isLoading = false;
    }

    /*handleWorkspaceClick(event){
        event.preventDefault();
        //this.selectedItem = event.detail.name;
        const selectedEvent = new CustomEvent('notify', {detail: event.target.dataset.id });
        this.dispatchEvent(selectedEvent);
    }*/

    handleSelect(event){
        event.preventDefault();
        if(event.detail && event.detail.name && this.workspaces){
            console.log('sending workspace change event:', JSON.stringify(event.detail));
            const selectedEvent = new CustomEvent('notify', {detail: event.detail.name });
            this.dispatchEvent(selectedEvent);
        }

    }

    goToURL(event) {
        var extURL = event.target.dataset.exturl;
        if(extURL) {
            const config = {
                type: 'standard__webPage',
                attributes: {
                    url: extURL
                }
            };
            this[NavigationMixin.Navigate](config);
        }
    }

}