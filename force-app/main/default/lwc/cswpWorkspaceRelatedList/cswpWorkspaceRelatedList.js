import { LightningElement, track, api, wire } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import RelatedListHelper from "./cswpWorkspaceRelatedListHelper";
import {loadStyle} from 'lightning/platformResourceLoader';
import relatedListResource from '@salesforce/resourceUrl/cswp_workspaceRelatedListResource';
import getObjectRecordTypes from '@salesforce/apex/CSWPSobjectSelector.getObjectRecordTypeInfosByDeveloperName';

export default class cswpWorkspaceRelatedList extends NavigationMixin(LightningElement) {
    @track state = {}
    @track objectInfo = {}
    @api sobjectApiName = 'cswp_Workspace__c';
    @api relatedFieldApiName = 'cswp_Group__c';
    @api rowActionHandler;
    @api recordTypeId;
    @api columns = [
        { label: 'Name', fieldName: 'LinkName', type: 'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}}
    ];
    @api customActions = [];
    helper = new RelatedListHelper()

    @wire(getObjectRecordTypes, { sObjectApiName: 'cswp_Workspace__c' })
    Function({error,data}){
        if(data){
            this.objectInfo = JSON.parse(data);
        }else if(error){
            console.log(error);
        }
    };

    renderedCallback() {
        loadStyle(this, relatedListResource + '/cswp_workspaceRelatedList.css')
    }

    @api
    get recordId() {
        return this.state.recordId;
    }

    set recordId(value) {
        this.state.recordId = value;
        this.init();
    }
    get hasCdRecords() {
        return this.state.cdrecords != null && this.state.cdrecords.length;
    }

    get hasTdRecords() {
        return this.state.tdrecords != null && this.state.tdrecords.length;
    }

    async init() {
        this.state.showRelatedList = (this.recordId != null);

        if (! (this.recordId
            && this.sobjectApiName
            && this.relatedFieldApiName
            && this.columns)) {
            this.state.cdrecords = [];
            this.state.tdrecords = [];
            return;
        }

        this.state.recordId= this.recordId
        this.state.sobjectApiName= this.sobjectApiName
        this.state.relatedFieldApiName= this.relatedFieldApiName
        this.state.customActions= this.customActions

        const data = await this.helper.fetchData(this.state);
        this.state.cdrecords = data.cdrecords;
        this.state.tdrecords = data.tdrecords;
        this.state.iconName = data.iconName;
        this.state.sobjectLabel = data.sobjectLabel;
        this.state.sobjectLabelPlural = data.sobjectLabelPlural;
        this.state.title = data.title;
        this.state.title2 = data.title2;
        this.state.parentRelationshipApiName = data.parentRelationshipApiName;
        this.state.columns = this.helper.initColumnsWithActions(this.columns, this.customActions)
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;

        if (this.rowActionHandler) {
            this.rowActionHandler.call()
        } else {
            switch (actionName) {
                case "delete":
                    this.handleDeleteRecord(row);
                    break;
                default:
            }
        }
    }

    handleGotoRelatedList() {
        this[NavigationMixin.Navigate]({
            type: "standard__recordRelationshipPage",
            attributes: {
                recordId: this.recordId,
                relationshipApiName: this.state.parentRelationshipApiName,
                actionName: "view",
                objectApiName: this.sobjectApiName
            }
        });
    }

    handleCreateRecord(event) {
        const newEditPopup = this.template.querySelector("c-cswp-workspace-related-list-new-popup");
        if((event.target.id).includes('cd')) {
            this.recordTypeId = this.objectInfo["cswp_ClientDocument"].recordTypeId;
        }
        else if ((event.target.id).includes('td')) {
            this.recordTypeId = this.objectInfo["cswp_ProductDocument"].recordTypeId;
        }
        newEditPopup.recordId = null
        newEditPopup.recordTypeId = this.recordTypeId;
        newEditPopup.recordName = null        
        newEditPopup.sobjectApiName = this.sobjectApiName;
        newEditPopup.sobjectLabel = this.state.sobjectLabel;
        newEditPopup.show();
    }

    handleDeleteRecord(row) {
        const deletePopup = this.template.querySelector("c-cswp-workspace-related-list-delete-popup");
        deletePopup.recordId = row.Id;
        deletePopup.recordName = row.Name;
        deletePopup.sobjectLabel = this.state.sobjectLabel;
        deletePopup.show();
    }

    handleRefreshData() {
        this.init();
    }
}