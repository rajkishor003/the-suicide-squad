import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle } from 'lightning/platformResourceLoader';
import relatedListResource from '@salesforce/resourceUrl/cswp_workspaceRelatedListResource';
import WS_NAME from '@salesforce/schema/cswp_Workspace__c.Name';
import WS_GROUP from '@salesforce/schema/cswp_Workspace__c.cswp_Group__c'
import WS_APPROVER from '@salesforce/schema/cswp_Workspace__c.cswp_Approver__c'
import WS_ACCOUNT from '@salesforce/schema/cswp_Workspace__c.cswp_Account__c'

export default class CswpWorkspaceRelatedListNewPopup extends LightningElement {
    showModal = false
    @api sobjectLabel
    @api sobjectApiName    
    @api recordId
    @api recordTypeId
    @api recordName

    wsName = WS_NAME;
    wsGroup = WS_GROUP;
    wsApprover = WS_APPROVER;
    wsAccount = WS_ACCOUNT;

    @api show() {
        this.showModal = true;
    }

    @api hide() {
        this.showModal = false;
    }
    handleClose() {
        this.showModal = false;     
    }
    handleDialogClose(){
        this.handleClose()
    }

    isNew(){
        return this.recordId == null
    }
    get header(){
        return this.isNew() ? 'New '+ this.sobjectLabel : 'Edit '+this.recordName;
    }

    handleSave(){
        this.template.querySelector('lightning-record-form').submit();
       
    }    
    handleSuccess(event){
        this.hide()
        let name = this.recordName
        if(this.isNew()){
            if(event.detail.fields.Name){
                name = event.detail.fields.Name.value
            }else if(event.detail.fields.LastName){
                name = [event.detail.fields.FirstName.value, event.detail.fields.LastName.value].filter(Boolean).join(" ")
            }
        } 
        name = name ? name : ''
        
        const message = this.sobjectLabel + name + ' was created.';
        const evt = new ShowToastEvent({
            title: message,
            variant: "success"
        });
        this.dispatchEvent(evt);
        this.dispatchEvent(new CustomEvent("refreshdata"));                  
    }    

    renderedCallback() {
        loadStyle(this, relatedListResource + '/cswpWorkspaceRelatedListNewPopup.css')
    }         
}