import { LightningElement, track, api } from 'lwc';
import Id from '@salesforce/user/Id';
import checkIfUserSubscribed from '@salesforce/apex/CSWPSobjectSelector.getWorkspaceSubscriptions'
import handleSubscription from '@salesforce/apex/CSWPWorkspaceManagerController.handleWorkspaceSubscription'

import SubscribeLabel from '@salesforce/label/c.cswp_subscribe';
import UnsubscribeLabel from '@salesforce/label/c.cswp_unsubscribe';

export default class CswpWorkspaceSubscription extends LightningElement {

    @api workspaceIdParam;
    @track isSubscribed;
    @api recordId;
    userId = Id;

    label = {
        SubscribeLabel,
        UnsubscribeLabel
    }

    connectedCallback() {
        //console.log('Subscription Connected Callback');
        if(this.workspaceIdParam) this.recordId = this.workspaceIdParam;
        this.checkSubscription();
    }

    checkSubscription() {
        //console.log('WorkspaceIDParam ==> '+this.workspaceIdParam);
        checkIfUserSubscribed({ userId : this.userId, workspaceId : this.recordId})
        .then(result => {
            this.isSubscribed = (result.length > 0);
            //console.log('Subscription checked for user : '+this.userId +' For workspace : '+this.recordId);
        })
        .catch(error => {
            console.log(error);
        });
    }

    handleSubscribe() {
        handleSubscription({userId : this.userId, workspaceId : this.recordId, operation : 'Create'})
        .then(result => {
            this.isSubscribed = true;
            //console.log('Subscription handled for user : '+this.userId +' For workspace : '+this.recordId+'With Create Operation');
        })
        .catch(error => {
            console.log(error);
        });
    }

    handleUnsubscribe() {
        handleSubscription({userId : this.userId, workspaceId : this.recordId, operation : 'Delete'})
        .then(result => {
            this.isSubscribed = false;
            //console.log('Subscription handled for user : '+this.userId +' For workspace : '+this.recordId+'With Delete Operation');
        })
        .catch(error => {
            console.log(error);
        });
    }

    @api refreshSubscriptionButton(value) {
        try{
            this.recordId = value;
            this.checkSubscription();
        }catch(err){
          console.error('refreshTable.Error: ', err);
        }
      }
}