import { LightningElement, api, track, wire } from 'lwc';
import Id from '@salesforce/user/Id';
import checkIfWorkspaceSynched from '@salesforce/apex/CSWPWorkspaceManagerController.getWorkspaceSynched'
import SyncWorkspace from '@salesforce/apex/CSWPWorkspaceManagerController.handleWorkspaceSync'
import SyncLabel from '@salesforce/label/c.Cswp_Sync';
import { getRecord } from 'lightning/uiRecordApi';
import { showSuccessToast } from "c/cswpUtil";

export default class CswpWorkspaceSynch extends LightningElement {

    @api workspaceIdParam;
    @api isSynched;
    @api recordId;
    userId = Id;

    label = {
        SyncLabel
    }

    @wire(getRecord, { recordId: '$recordId', fields: [ 'cswp_Workspace__c.Name' ] })
    getaccountRecord({ data, error }) {
        this.checkSynchronization();
    }

    connectedCallback() {
        console.log('Sync Connected Callback');
        //if(this.workspaceIdParam) this.recordId = this.workspaceIdParam;
        this.checkSynchronization();
    }

    checkSynchronization() {
        console.log('checkSynchronization ==> '+this.recordId);
        checkIfWorkspaceSynched({ workspaceId : this.recordId})
        .then(result => {
            this.isSynched = result;
            console.log('issynched result==> '+result);
        })
        .catch(error => {
            console.log(error);
        });
    }

    handleSync() {
        console.log('handleSync ==> '+this.recordId);
        SyncWorkspace({ workspaceId : this.recordId})
        .then(result => {
            showSuccessToast(this,{
                title: 'Message',
                message: 'Sync Process Started'
              });
        })
        .catch(error => {
            console.log(error);
        });
    }
}