import { api, LightningElement, track, wire } from "lwc";

import { tableColumns } from "./data";
import getWorkspaceItems from "@salesforce/apex/CSWPWorkspaceManagerController.getWorkspaceItems";
import getFolderItems from "@salesforce/apex/CSWPWorkspaceManagerController.getFolderItems";
import searchWorkspaceItems from "@salesforce/apex/CSWPWorkspaceManagerController.searchWorkspaceItems";
import recentlyViewedItems from "@salesforce/apex/CSWPWorkspaceManagerController.recentlyViewedItems";
import getFileVersionDownloadUrl from "@salesforce/apex/CSWPWorkspaceManagerController.getFileVersionDownloadUrl";

import { deleteRecord } from "lightning/uiRecordApi";
import { updateRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { NavigationMixin } from "lightning/navigation";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import WORKSPACE_OBJECT from "@salesforce/schema/cswp_Workspace__c";
import FOLDER_OBJECT from "@salesforce/schema/cswp_Folder__c";
import FILE_OBJECT from "@salesforce/schema/cswp_File__c";
import { logActivity,showErrorToast } from "c/cswpUtil";

import DOWNLOAD_TIME_FIELD from '@salesforce/schema/cswp_File__c.cswp_Last_Downloaded_Time__c';
import ID_FIELD from '@salesforce/schema/cswp_File__c.Id';

import cswp_table_column_actions from '@salesforce/label/c.cswp_table_column_actions';
import cswp_table_action_download from '@salesforce/label/c.cswp_table_action_download';
import cswp_table_action_delete from '@salesforce/label/c.cswp_table_action_delete';
import cswp_table_action_rename from '@salesforce/label/c.cswp_table_action_rename';
import cswp_noitemsfound from '@salesforce/label/c.cswp_noitemsfound';
import cswp_workspace_edit from '@salesforce/label/c.cswp_workspace_edit';
import cswp_workspace_file from '@salesforce/label/c.cswp_workspace_file';
import cswp_workspace_deleted from "@salesforce/label/c.cswp_workspace_deleted";
import cswp_deleted_success from "@salesforce/label/c.cswp_deleted_success";
import cswp_successfully_updated from "@salesforce/label/c.cswp_successfully_updated";
import cswp_nodownload from "@salesforce/label/c.cswp_nodownload";
import cswp_folder_notfound from "@salesforce/label/c.cswp_folder_notfound";
import cswp_delete_file_confirm from "@salesforce/label/c.CSWP_Delete_File_Confirm";
import cswp_search_message_1 from "@salesforce/label/c.cswp_search_message_1";
import cswp_search_message_2 from "@salesforce/label/c.cswp_search_message_2";
import cswp_label_status_c from "@salesforce/label/c.cswp_label_status_c";

export default class CswpWorkspaceTable extends NavigationMixin(
  LightningElement
) {

  label = {
    cswp_noitemsfound,
    cswp_workspace_edit,
    cswp_workspace_file,
    cswp_workspace_deleted,
    cswp_deleted_success,
    cswp_successfully_updated,
    cswp_nodownload,
    cswp_folder_notfound,
    cswp_delete_file_confirm,
    cswp_search_message_1,
    cswp_search_message_2,
    cswp_label_status_c
  };

  @api userInfo;
  @api workspaceType;

  @track columns;
  @track noRecord = true;
  @track searchNotFound = false;

  isLoaded = true;
  dataset;
  folderId;
  libraryId;
  searchMessage = '';
  
  @wire(getObjectInfo, { objectApiName: WORKSPACE_OBJECT })
  workspaceInfo;
  @wire(getObjectInfo, { objectApiName: FOLDER_OBJECT })
  folderInfo;


  @api refreshTable() {
    this.isLoaded = false;
    try{
      if (this.folderId) {
        this.fetchFolderItems(this.folderId);
      } else {
        this.fetchWorkspaceItems(this.libraryId);
      }
    }catch(err){
      console.error('refreshTable.Error: ', err);
    }
  }

  @api getWorkspaceItems(libraryId){
    this.isLoaded = false;
    //reset folderId
    try{
      this.fetchWorkspaceItems(libraryId);
    }catch(err){
      console.error('getWorkspaceItems.Error:>' , err);
    }
  }

  @api
  navigateToItem(placeId) {
    console.log("navigatedItemId.handled >>> ", placeId);
    //console.log(this.workspaceInfo);
    let wPrefix = this.workspaceInfo.data.keyPrefix;
    let fPrefix = this.folderInfo.data.keyPrefix;
    let IdPrefix = placeId.substring(0, 3);
    if (wPrefix === IdPrefix) {
      // then getWorkspaceItems
      this.fetchWorkspaceItems(placeId);
    }
    if (fPrefix === IdPrefix) {
      console.log("getFoldeInfo");
      this.fetchFolderItems(placeId);
    }
  }

  async fetchWorkspaceItems(libId) {
    this.libraryId = libId;
    console.log('fetchWorkspaceItems.libraryId=>',libId);
    console.log('fetchWorkspaceItems.workspaceType=>', this.workspaceType);
    //Reset folderId
    this.folderId = null;
    this.isLoaded = false;
    let items=[];    
    if(!this.libraryId){
      //get recentItems; 
      items = await recentlyViewedItems();
      console.log('recentItems:>> %O',items);
    } else{
      items = await getWorkspaceItems({
        workspaceId: this.libraryId,
        workspaceType: this.workspaceType
      });
    }
    //Get columns of workspace table along with their actions
    this.columns = this.getColumnsWithAction();

    if (items && items.length > 0) {
      this.dataset = items;     
      this.noRecord = false;
    } else {
      this.noRecord = true;
    }
    this.isLoaded = true;
  }

  @api searchInWorkspace(searchTerm){
    console.log('search in workspaceId :>', this.libraryId);
    console.log('searchTerm :> ', searchTerm);
    if(!searchTerm && searchTerm != ''){
      this.refreshTable();
    }
    this.isLoaded = false;
    searchWorkspaceItems({ workspaceId: this.libraryId, searchText:searchTerm })
      .then(res => {
        console.log('search result:>' , res); 
        if(res && res.length > 0){
          this.dataset = res;
          this.noRecord = false;  
          this.searchNotFound = false; 
        }else{
          this.noRecord = true;
          this.searchNotFound = true;
          if(searchTerm) {
            this.searchMessage = this.label.cswp_search_message_1 + '%' + searchTerm + '%' + this.label.cswp_search_message_2;
          } else {
            this.searchMessage = this.label.cswp_noitemsfound;
          }
        }
        this.isLoaded = true;      
      }).catch(err => {
        console.error('error in search :>',err);
        this.isLoaded = true;
      });
  }



  handleRowAction(event) {
    const actionName = event.detail.action.name;
    const row = event.detail.row;
    switch (actionName) {
      case "download":
        this.downloadItem(row);
        break;
      case "rename":
        this.renameItem(row);
        break;
      case "delete":
        this.deleteItem(row);
        break;
      default:
    }
  }

  async downloadItem(row) {
    //console.log(JSON.stringify(row));
    if(row.type === 'folder'){
      showErrorToast(this,{
        title: this.label.cswp_nodownload,
        message: this.label.cswp_folder_notfound
      });
      return;
    }
    //log the download activity
    logActivity({
      name: row.name,
      id: row.id,
      filepath: row.path
    },'Download', row.name + ' downloaded!');
    
    let url;
    if(row.downloadUrl){
      url = this.userInfo.community ? 
         (this.userInfo.community.siteUrl + row.downloadUrl) : 
         (window.location.origin + row.downloadUrl);
    }else{
      url = await getFileVersionDownloadUrl({ fileId: row.id , versionNo: null });
    }
    console.log('downloadUrl:>> ' + url);
    //Update the last downloaded Time Field
    this.updateDownloadedFile(row.id);
    //then redirect users to download page
    if(url){
      window.location.href = url;
    }else{
      showErrorToast(this,{
        title: this.label.cswp_nodownload,
        message: 'Something went wrong!'
      });
    }
  }

  updateDownloadedFile(fileId){
    const fields = {};
    fields[ID_FIELD.fieldApiName] = fileId;
    fields[DOWNLOAD_TIME_FIELD.fieldApiName] = new Date().toISOString();
    updateRecord({ fields })
      .then( res => {
        console.log('updatedRecord with downloadTime ==> '+res); 
        this.dispatchEvent(new CustomEvent('rowactiondone'));
      })
      .catch( err => console.error(err.body));
  }

  goInside(event) {
    event.preventDefault();
    let itemId = event.detail.id;
    let type = event.detail.type;
    console.log("event is handled id :> ", itemId);
    if (itemId && type === "folder") {
      this.fetchFolderItems(itemId);
    }
    if (itemId && type === "file") {
      //Download file or navigate
      if (this.userInfo.community) {
        if(this.workspaceType === 'Document Sharing'){
          this._navigateToFileInCommunity(itemId);
        }else{
         let row = this.dataset.find( el=> el.id === itemId );
         //console.log('found row =>', row);
         this.downloadItem(row); 
        }
      } else {
        this._navigateToFile(itemId);
      }
    }
  }

  async fetchFolderItems(itemId) {
    this.folderId = itemId;
    this.isLoaded = false;
    let result = await getFolderItems({ folderId: itemId });
    this.columns = this.getColumnsWithAction();
    if (result && result.length > 0) {
      this.noRecord = false;
      this.dataset = result;
    } else {
      this.noRecord = true;
    }
    this.isLoaded = true;
  }

  _navigateToFileInCommunity(fileId) {
    console.log("navigate file Id > ", fileId);
    this[NavigationMixin.Navigate]({
      type: "comm__namedPage",
      attributes: {
        name: 'CSWP_File_Detail__c'
      },
      state: {
        recordId: fileId
      }
    });
  }

  _navigateToFile(fileId) {
    // View a custom object record.
    this[NavigationMixin.Navigate]({
      type: "standard__recordPage",
      attributes: {
        recordId: fileId,
        objectApiName: "cswp_File__c",
        actionName: "view"
      }
    });
  }

  renameItem(row) {
    //console.log(JSON.stringify(row));
    //console.log("Rename functionality");
    let form = this.template.querySelector("c-edit-record-modal");
    const FormFields = [
      { key: 0, name: "Name", value:row.name, disable: false,required:true },
      { key: 1, name: "cswp_Description__c", value: row.description, disable: false, required:false }
    ];
    form.header = this.label.cswp_workspace_edit + " " + row.name;
    form.fields = FormFields;
    form.recordId = row.id;
    form.objectApiName = row.type === "file" ? FILE_OBJECT : FOLDER_OBJECT;
    form.showModal = true;
  }

  handleSuccessForm(event) {
    let record = event.detail;
    console.log("formSubmitted", event.detail);
    console.log("handleSuccessForm: ", this.folderId);
    let editForm = this.template.querySelector("c-edit-record-modal");
    this.showUserMessage(
      record.fields.Name.value,
      this.label.cswp_successfully_updated,
      "success"
    );
    editForm.showModal = false;
    this.refreshTable();
  }

  deleteItem(row) {
    if(!window.confirm(this.label.cswp_delete_file_confirm)){
      return;
    }
    console.log("delete item %O ", row);
    this.isLoaded = false;
    deleteRecord(row.id)
      .then(() => {
        this.showUserMessage(
          this.label.cswp_workspace_deleted,
          row.name + ' ' + this.label.cswp_deleted_success,
          "success"
        );
        //Log the deletion
        logActivity({ name: row.name, id: row.id, filepath: row.path },'Delete', row.name + ' deleted!');
        this.isLoaded = true;
        console.log('this.folderId:>>>', this.folderId);
        console.log('this.libraryId:>>>', this.libraryId);
        this.refreshTable();
      })
      .catch((err) => {
        this.showUserMessage("Error!", err.body.message, "error");
        this.isLoaded = true;
      });
  }

  getColumnsWithAction = () => {
    let cols = Array.from(tableColumns);
    if(!this.userInfo.community) {
      cols.splice(1, 0, {label: cswp_label_status_c, fieldName: 'status'});
    }
    let userPerms = (this.userInfo && this.userInfo.permissions) || {};
    console.log('UserPerms: > ',JSON.parse(JSON.stringify(userPerms)));
    if(userPerms){
      const columnActionObj = {
        label: cswp_table_column_actions,
        type: "action",
        typeAttributes: {rowActions: this.getActions(userPerms) }, 
        fixedWidth: 80
      };
      if(this.libraryId){
        cols.push(columnActionObj);
      }
    }    
    return cols;
  };

  getActions(userPerms){
    return (row,doneCallback) => {
      const actions = [];
      if(row.type === 'file' && userPerms.canDownload){
        actions.push({ label: cswp_table_action_download, name: "download" });
      }
      if (userPerms.canDelete) {
        actions.push({ label: cswp_table_action_delete, name: "delete" });
      }
      if (userPerms.canRename) {
        actions.push({ label: cswp_table_action_rename, name: "rename" });
      }
      doneCallback(actions);
    };
  }


  showUserMessage(title, message, variant) {
    
    this.dispatchEvent(
      new ShowToastEvent({
        title: title,
        message: message,
        variant: variant
      })
    );
  }
}