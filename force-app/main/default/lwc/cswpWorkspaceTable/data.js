import cswp_table_column_name from '@salesforce/label/c.cswp_table_column_name';
import cswp_table_column_description from '@salesforce/label/c.cswp_table_column_description';
import cswp_table_column_content_size from '@salesforce/label/c.cswp_table_column_content_size';
import cswp_table_column_created_date from '@salesforce/label/c.cswp_table_column_created_date';

export const tableColumns = [
    {
        label: cswp_table_column_name,
        fieldName: 'name',
        type:'cswpNameCell',
        initialWidth: 280,
        typeAttributes: { id: { fieldName: 'id' },type: { fieldName: 'type' }, icon: { fieldName: 'icon' }, breadcrumb: { fieldName: 'breadcrumb'} }
     },
     {
        label: cswp_table_column_description,
        fieldName:'description'
     },
    {
        label: cswp_table_column_content_size,
        fieldName: 'contentSize',
        type: 'number',
        initialWidth: 150
    },
    // {
    //     label: 'Status',
    //     fieldName: 'status'
    // },
    // {
    //     label: cswp_table_column_created_date,
    //     fieldName: 'createdDate',
    //     type:'date',
    //     initialWidth: 150,
    //     typeAttributes:{
    //         year: "numeric",
    //         month: "numeric",
    //         day: "numeric",
    //         hour: "2-digit",
    //         minute: "2-digit"
    //     },
    // },
    {
        label: cswp_table_column_created_date,
        fieldName: 'createdDateFormatted',
        type: 'text',
        initialWidth: 150
    }
    // {
    //     label: 'Owner',
    //     fieldName: 'createdBy'
    // }
];