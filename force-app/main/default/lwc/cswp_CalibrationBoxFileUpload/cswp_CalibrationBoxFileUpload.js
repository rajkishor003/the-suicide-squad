import { LightningElement, api, wire, track } from 'lwc';
import ProcessUploadedFile from '@salesforce/apex/Cswp_CalBoxFileUploadController.ProcessUploadedFile';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import SuccessLabel from '@salesforce/label/c.cswp_Success_Label';
import ErrorLabel from '@salesforce/label/c.cswp_Error_Label';
import ValidatedLabel from '@salesforce/label/c.Cswp_File_Validated';

export default class Cswp_CalibrationBoxFileUpload extends LightningElement {
    @api
    myRecordId;

    @track jsfiles;
    @track error;    
    @track toastTitle;
    @track toastMessage;
    @track isLoading = false;

    get acceptedFormats() {
        return ['.zip', '.ecr'];
    }

    handleUploadFinished(event) {
        this.isLoading = true;
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
        ProcessUploadedFile({files : uploadedFiles})
        .then(result => {
            this.jsfiles = result;
            this.toastTitle = SuccessLabel;
            this.toastMessage = ValidatedLabel;
            const toastEvent = new ShowToastEvent({
                "variant": this.toastTitle,
                "title": this.toastTitle,
                "message": this.toastMessage
            });
            this.dispatchEvent(toastEvent);
            this.isLoading = false;
        })
        .catch(error => {
            this.error = error;
            this.toastTitle = ErrorLabel;
            this.toastMessage = error.body.message
            const toastEvent = new ShowToastEvent({
                "variant": this.toastTitle,
                "title": this.toastTitle,
                "message": this.toastMessage
            });
            this.dispatchEvent(toastEvent);
            this.isLoading = false;
        });
    }
}