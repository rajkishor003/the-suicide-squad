import LightningDatatable from 'lightning/datatable';
import customTypeDownload from './customTypeDownload'
import customTypeSystemSn from './customTypeSystemSn'

export default class CustomLightningDatatable extends LightningDatatable {
    static customTypes = {
        customTypeDownload: {
            template: customTypeDownload,
            // typeAttributes: ['recordId']
        },
        customTypeSystemSn: {
            template: customTypeSystemSn,
            typeAttributes: ['systemSn']
        },
    }
}