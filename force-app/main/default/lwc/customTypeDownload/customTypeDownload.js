import { LightningElement, api, track } from 'lwc';
import cswp_download_label from '@salesforce/label/c.cswp_download_label';

export default class CustomTypeDownload extends LightningElement {

    label = {
        cswp_download_label
    };
    // @api recordId;

    fireDownloadEvent() {
        console.log('download event created and modal opened');
        const event = new CustomEvent('customdownload', {
            composed: true,
            bubbles: true,
            cancelable: true,
            // detail: {
            //     recordId: this.recordId,
            // },
        });
        this.dispatchEvent(event);
    }
}