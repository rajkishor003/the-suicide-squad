import { LightningElement, api, track } from 'lwc';

export default class CustomTypeSystemSn extends LightningElement {

    @api systemSn;

    fireSystemSnEvent() {
        console.log('system sn event created and modal opened');
        const event = new CustomEvent('customsystemsn', {
            composed: true,
            bubbles: true,
            cancelable: true,
            // detail: {
            //     recordId: this.recordId,
            // },
        });
        this.dispatchEvent(event);
    }
}