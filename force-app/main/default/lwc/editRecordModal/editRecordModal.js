import { LightningElement,api } from 'lwc';
import cswp_calbox_cancel_text from '@salesforce/label/c.cswp_calbox_cancel_text';
import cswp_submit from '@salesforce/label/c.cswp_submit';
import cswp_table_column_description from '@salesforce/label/c.cswp_table_column_description';
// import cswp_folder_name from '@salesforce/label/c.cswp_folder_name';
import cswp_name_label from '@salesforce/label/c.cswp_name_label';
import cswp_item_exist_error from '@salesforce/label/c.cswp_item_exist_error';
import cswp_create_folder_error from '@salesforce/label/c.cswp_create_folder_error';
import cswp_contact_us_submit_error_title from '@salesforce/label/c.cswp_contact_us_submit_error_title';

export default class EditRecordModal extends LightningElement {
    label = {
      cswp_calbox_cancel_text,
      cswp_submit,
      cswp_table_column_description,
      cswp_name_label,
      cswp_contact_us_submit_error_title,
      cswp_create_folder_error,
      cswp_item_exist_error
    };

    @api header;
    @api recordId;
    @api objectApiName;
    @api fields=[];
    openModal=false;

    isLoaded=true;
    isError = false;
    error;

    @api get showModal(){
      return this.openModal;
    }

    set showModal(value){
      this.openModal = value;
      this.setAttribute('showModal',this.openModel);
    }

    closeModal(){
      this.isError = false;
      this.isLoaded = true;
      this.openModal = false;
    }

    cancelSubmit(evt){
      this.isError = false;
      this.isLoaded = true;
      this.openModal = false;
    }

    onSubmit(evt){
      this.isLoaded = false;
      this.isError = false;
      evt.preventDefault();
      const fields = evt.detail.fields;
      this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    onSuccess(evt){
      evt.preventDefault();
      this.isLoaded = true;
      console.log('onSuccess.Form ', evt.detail);
      this.dispatchEvent(
        new CustomEvent('formsuccess',{
          bubbles:true,
          detail: {
            id: evt.detail.id,
            fields: evt.detail.fields
          }
        })
      );
    }

    onError(evt){
      //console.log(JSON.stringify(evt.detail.output));
      let err = evt.detail.detail;
      console.log('error message:>' , err);
      if( err && ( err.includes('duplicate') || err.includes('cswp_Path__c')) ){
        this.error = this.label.cswp_item_exist_error;
      }else{
        this.error= err;
      }
      this.isError = true;
      this.isLoaded = true;
    }

    onCancel(evt){
      this.isError = false;
      this.isLoaded = true;
      this.openModal = false;
    }
}