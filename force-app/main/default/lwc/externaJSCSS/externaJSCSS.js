import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import jqueryMinJS from '@salesforce/resourceUrl/jqueryminjsv1';
import demoCSS from '@salesforce/resourceUrl/demoCSSv1'

export default class ExternaJSCSS extends LightningElement {

    renderedCallback() {
        Promise.all([
            loadScript(this, jqueryMinJS),
            loadStyle(this, demoCSS)
        ]).then(() => {
            $(this.template.querySelector('div')).text("JQuery Loaded");
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading Jquery',
                    message: error,
                    variant: 'error'
                })
            );
        });
    }
}