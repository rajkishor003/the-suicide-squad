import { LightningElement, wire } from 'lwc';
import messageChannel from '@salesforce/messageChannel/test__c';
import {publish, MessageContext} from 'lightning/messageService'
export default class LmsPublisher extends LightningElement {
    @wire(MessageContext)
    messageContext;
    handleButtonClick() {
        let message = {messageText: 'This is a test!!!!!!'};
        publish(this.messageContext, messageChannel, message);
    }
}