import { LightningElement, wire } from 'lwc';
import messageChannel from '@salesforce/messageChannel/test__c';
import { subscribe, MessageContext } from 'lightning/messageService';

export default class LmsSubscriber extends LightningElement {

    msgText;

    subscription = null;

    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        this.handleSubscribe();
    }

    handleSubscribe() {
        if (this.subscription) {
            return;
        }
        this.subscription = subscribe(this.messageContext, messageChannel, (message) => {
            console.log(message.messageText);
            this.msgText = message.messageText;
        });
    }
}