<apex:page docType="html-5.0" sidebar="false" showHeader="false" standardStylesheets="false" applyHtmlTag="false" controller="osf_MyOrdersController">
    <c:osf_contact_info />
    <c:osf_my_address_book />
    <c:osf_my_carts />
    <c:osf_my_request_for_quote />
    <c:osf_my_orders />
    <c:osf_MyPurchaseOrders />
    <c:osf_my_wishlist_detail />
    <c:osf_my_wishlist />

    <script id="osf_MyAccount-Nav" type="text/template">
        <div class="panel panel-default cc_panel cc_myaccount_nav">
            <div class="panel-heading cc_heading">
                <h3 class="panel-title cc_title">{{pageLabelMap 'Component_SiteHeader_MyAccount'}}</h3>
            </div>
            <ul class="side_navigation_dropdown list-group cc_myaccount_nav_list" id="side_nav">
                {{#each this}}
                    {{#ifEquals this.index '0'}}
                        <li class="acctStep{{this.index}} acctStepNav list-group-item cc_acc_step_nav">
                            <a href="#" class="gotoSection cc_goto_section" data-index="{{this.index}}">{{{pageLabelMap this.title}}}</a>
                        </li>
                        <li class="list-group-item cc_acc_step_nav">
                            <a href="{{pageLabelMap 'MyAccount_Service_Membership_link'}}" class="cc_goto_section" target="_blank">{{pageLabelMap 'MyAccount_Service_Membership'}}</a>
                        </li>
                        <li class="list-group-item cc_acc_step_nav">
                            <a href="{{pageLabelMap 'MyAccount_ChangePassword_link'}}" class="cc_goto_section">{{pageLabelMap 'MyAccount_ChangePassword'}}</a>
                        </li>
                        <li class="list-group-item cc_acc_step_nav">
                            <a href="{{pageLabelMap 'MyAccount_MyLicenses_link'}}" class="cc_goto_section" target="_blank">{{pageLabelMap 'MyAccount_MyLicenses'}}</a>
                        </li>
                    {{else}}
                        <li class="acctStep{{this.index}} acctStepNav list-group-item cc_acc_step_nav">
                            <a href="#" class="gotoSection cc_goto_section" data-index="{{this.index}}">{{{pageLabelMap this.title}}}</a>
                        </li>
                    {{/ifEquals}}
                    
                {{/each}}
            </ul>
        </div>
    </script>

    <script>
        jQuery(function ($) {
            CCRZ.pubSub.on('view:myaccountHDRView:awaitingSubViewInit', function (view) {
                if (CCRZ.HDRMyAccount.contactInfo) {
                    CCRZ.HDRMyAccount.contactInfo.register(view);
                }
                if (CCRZ.HDRMyAccount.addressBooks) {
                    CCRZ.HDRMyAccount.addressBooks.register(view);
                }
                if (CCRZ.HDRMyAccount.myCarts) {
                    CCRZ.HDRMyAccount.myCarts.register(view);
                }

                if (CCRZ.HDRMyAccount.myRequestForQuote) {
                    CCRZ.HDRMyAccount.myRequestForQuote.register(view);
                }

                if (CCRZ.HDRMyAccount.myOrders) {
                    CCRZ.HDRMyAccount.myOrders.register(view);
                }

                if (CCRZ.HDRMyAccount.myWishlists) {
                    CCRZ.HDRMyAccount.myWishlists.register(view);
                }

                if (CCRZ.HDRMyAccount.myWallet) {
                    CCRZ.HDRMyAccount.myWallet.register(view);
                }

                CCRZ.pubSub.trigger("view:myaccountHDRView:subViewInit", true);
            });

            CCRZ.pubSub.on('view:myaccountView:awaitingSubViewInit', function (view) {
                if (CCRZ.MyAccount.contactInfo) {
                    CCRZ.MyAccount.contactInfo.register(view);
                }

                if (CCRZ.MyAccount.addressBooks) {
                    CCRZ.MyAccount.addressBooks.register(view);
                }

                if (CCRZ.MyAccount.myCarts) {
                    CCRZ.MyAccount.myCarts.register(view);
                }

                if (CCRZ.MyAccount.myRequestForQuote) {
                    CCRZ.MyAccount.myRequestForQuote.register(view);
                }

                if (CCRZ.MyAccount.myOrders) {
                    CCRZ.MyAccount.myOrders.register(view);
                }

                if (CCRZ.MyAccount.myWishlists) {
                    CCRZ.MyAccount.myWishlists.register(view);
                }

                if (CCRZ.MyAccount.myWallet) {
                    CCRZ.MyAccount.myWallet.register(view);
                }

                CCRZ.pubSub.trigger("view:myaccountView:subViewInit", true);
            });

            CCRZ.pubSub.on('view:myaccountView:refresh', function () {
                if (CCRZ.pagevars.queryParams.rfqh) {
                    $("[data-index='3']").trigger('click');
                }
            });

            CCRZ.subsc = _.extend(CCRZ.subsc || {});

            CCRZ.subsc.ordersRemoteActions = _.extend(
                {
                    className: "osf_MyOrdersController",
                    getPurchaseOrdersAction: function (callback) {
                        this.invokeCtx(
                            "getPurchaseOrders",
                            function (resp) {
                                callback(resp);
                            },
                            { buffer: false, nmsp: false }
                        );
                    }
                },
                CCRZ.RemoteInvocation
            );

            CCRZ.pubSub.on("view:myOrdersView:refresh", function (myOrdersView) {
                myOrdersView.downloadQuoteAttachAction = function (event) {
                    var url = CCRZ.pagevars.currSiteURL + 'sfc/servlet.shepherd/version/download/' + event.target.getAttribute('data-id');
                    window.open(url, '_blank');
                };

                myOrdersView.downloadSoaAttachAction = function (event) {
                    var url = CCRZ.pagevars.currSiteURL + 'sfc/servlet.shepherd/version/download/' + event.target.getAttribute('data-id');
                    window.open(url, '_blank');
                };

                myOrdersView.downloadPoAttachAction = function (event) {
                    var url = CCRZ.pagevars.currSiteURL + 'sfc/servlet.shepherd/version/download/' + event.target.getAttribute('data-id');
                    window.open(url, '_blank');
                };

                myOrdersView.events["click .downloadQuote"] = "downloadQuoteAttachAction";
                myOrdersView.events["click .downloadSoa"] = "downloadSoaAttachAction";
                myOrdersView.events["click .downloadPo"] = "downloadPoAttachAction";
                myOrdersView.delegateEvents();
            });

            Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
                return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
            });

        });
    </script>
</apex:page>