/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./frontend/assets/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./frontend/assets/js/index.js":
/*!*************************************!*\
  !*** ./frontend/assets/js/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n__webpack_require__(/*! ../sass/styles.scss */ \"./frontend/assets/sass/styles.scss\");\n\n__webpack_require__(/*! ./localSwitcher */ \"./frontend/assets/js/localSwitcher.js\");\n\nconsole.log(\"Welcome to Advantest Frontend\");\n\n//# sourceURL=webpack:///./frontend/assets/js/index.js?");

/***/ }),

/***/ "./frontend/assets/js/localSwitcher.js":
/*!*********************************************!*\
  !*** ./frontend/assets/js/localSwitcher.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nCCRZ.directLocation = function (lang) {\n  var locale = lang;\n  var queryString = \"?cclcl=\" + locale;\n\n  if (CCRZ.pagevars.queryParams) {\n    _.each(_.keys(CCRZ.pagevars.queryParams), function (queryParamKey) {\n      if (queryParamKey != \"cclcl\" && CCRZ.pagevars.queryParams[queryParamKey]) {\n        queryString += '&' + queryParamKey + '=' + CCRZ.pagevars.queryParams[queryParamKey];\n      }\n    });\n  } // Reload Current Page With New Locale Param\n\n\n  document.location = window.location.href.split('?')[0] + CCRZ.buildQueryString(queryString);\n};\n\njQuery(function ($) {\n  CCRZ.views.localeSwitcherView.prototype.setLocale = function (event) {\n    var objLink = $('#modalSecLocale .cc_tr_locale.active');\n    var locale = objLink.data(\"id\");\n    var v = this; //first check if a selection was made and its not the current locale\n\n    if (locale && '' != locale && this.model.attributes && this.model.attributes.locale && this.model.attributes.locale != locale) {\n      //with a valid locale, call the controller to set the locale\n      this.model.invokeContainerLoadingCtx($('body'), 'setLocale', locale, function (response, event) {\n        if (event.status && response && response.success && response.data) {\n          //if we got the same value back from the setLocale call, then set is successful,\n          //and page param can be updated and reload the page\n          if (response.data.locale && response.data.locale === locale) {\n            CCRZ.console.log('set locale succeeded, reload page'); // Call direction method with language parameter (es_ES)\n\n            CCRZ.directLocation(locale); //close the modal\n            // v.closeModal(event);\n          } else {\n            CCRZ.console.log('set locale failed');\n          }\n        } else {\n          if (response && response.messages && _.isArray(response.messages) && response.messages.length) {\n            CCRZ.console.log('got messages: ' + response.messages); // v.closeModal(event);\n          } else {\n            CCRZ.console.log('got error'); // v.closeModal(event);\n          }\n        }\n      }, {\n        buffer: false,\n        timeout: 120000\n      });\n    }\n  };\n});\n\n//# sourceURL=webpack:///./frontend/assets/js/localSwitcher.js?");

/***/ }),

/***/ "./frontend/assets/sass/styles.scss":
/*!******************************************!*\
  !*** ./frontend/assets/sass/styles.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./frontend/assets/sass/styles.scss?");

/***/ })

/******/ });