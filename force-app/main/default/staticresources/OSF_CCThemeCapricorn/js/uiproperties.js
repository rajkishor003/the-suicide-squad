//
// Theme driven uiProperties overrides.  If you want to override elements
// of uiProperties within this file then
// 1) Uncomment the code between //OVERRIDES START HERE and
//                               //OVERRIDES END HERE
// 2) Define the specific property(ies) that you wish to
//    override from the managed package.
//
// Note that you must override according to the full object path
// but you can only override specific values, i.e. you do not need
// to provide the whole object copy.
//
// OVERRIDES START HERE
CCRZ.uiProperties = $.extend(true, CCRZ.uiProperties, {
  headerView: {
    desktop: {
      tmpl: "osf_HeaderDesktop",
      partials: {
        changeCurrency: "changeCurrencyTemplate"
      }
    }
  },

  cartHeaderView: {
    desktop: {
      tmpl: "osf_CartHeaderBrowser"
    }
  },

  productDetailView: {
    desktop: {
      tmpl: "osf_ProductDetail"
    }
  },

  productListPageView: {
    header: {
      tmpl: "osf_Product-List-Header"
    },

    productItem: {
      list: {
        tmpl: "osf_Product-Item-Row"
      },
      grid: {
        tmpl: "osf_Product-Item-Grid"
      }
    }
  },

  productCompareTrayView: {
    desktop: {
      tmpl: "osf_Product-Compare-Tray-View"
    }
  },

  contentSectionView: {
    desktop: {
      tmpl: "osf_ProductDetail-ContentSection"
    }
  },

  mediaSectionView: {
    desktop: {
      tmpl: "osf_ProductDetail-MediaSection"
    }
  },

  requestForQuoteView: {
    desktop: {
      tmpl: "osf_MyAccount-MyRequestForQuote"
    }
  },

  quoteInfoView: {
    desktop: {
      tmpl: "osf_QuoteInfo"
    }
  },

  chatView: {
    desktop: {
      tmpl: "osf_Chat"
    }
  },

  spotlightView: {
    desktop: {
      tmpl: "osf_Spotlight"
    }
  },

  prodSectionView: {
    desktop: {
      main: {
        tmpl: "osf_ProductDetail-ProductsSection"
      },

      right: {
        tmpl: "osf_ProductDetail-ProductsSectionRight"
      }
    }
  },

  ReOrderView: {
    tmpl: "osf_ReOrder3",

    OrderedItemsView: {
      tmpl: "osf_ReOrder-IncludedItems"
    }
  },

  miniCartView: {
    desktop: {
      tmpl: "osf_MiniCart"
    }
  },

  contactInfoView: {
    desktop: {
      tmpl: "osf_MyAccount-ContactInformation"
    }
  },

  myAddressBookView: {
    desktop: {
      tmpl: "osf_MyAccount-AddressBook"
    }
  },

  myOrdersView: {
    desktop: {
      tmpl: "osf_MyAccount-MyOrders"
    }
  },

  wishlistDetailsView: {
    desktop: {
      tmpl: "osf_MyAccount-WishlistDetail-Desktop"
    }
  },

  myWishlistsView: {
    desktop: {
      tmpl: "osf_MyAccount-MyWishlist-Desktop"
    }
  },

  CartDetailView: {
    desktop: {
      tmpl: "osf_CartDetail-View-Desktop"
    }
  },

  myWalletView: {
    desktop: {
      tmpl: "osf_MyAccount-MyWallet"
    }
  },

  localeSwitcherView: {
    desktop: {
      tmpl: "osf_LocaleSwitcher-Desktop"
    }
  },

  myAccountNavView: {
    desktop: {
      tmpl: "osf_MyAccount-Nav"
    }
  },

  wishlistPickerModal: {
    desktop: {
      tmpl: "osf_AddtoWishlist-Desktop"
    }
  },

  CartOrderReviewViewV2: {
    desktop: {
      tmpl: "osf_Review-Cart-DesktopV2"
    }
  },

  myCartsView: {
    desktop: {
      tmpl: "osf_MyAccount-MyCarts"
    }
  },

  wishlistDetailsView: {
    desktop: {
      tmpl: "osf_MyAccount-WishlistDetail-Desktop"
    }
  },

  myWishlistsView: {
    desktop: {
      tmpl: "osf_MyAccount-MyWishlist-Desktop"
    }
  },

  CartDetailView: {
    desktop: {
      tmpl: "osf_CartDetail-View-Desktop"
    }
  },

  productCompareView: {
    desktop: {
      tmpl: "osf_ProductCompare-Desktop"
    }
  },

  QuickOrderView: {
    desktop: {
      tmpl: "osf_MiniQuickOrder-Desktop"
    }
  }
});

CCRZ.uiProperties.OrderReviewView.desktop.tmpl = "osf_Review-Desktop";
CCRZ.uiProperties.CheckoutNav.desktop.tmpl = "osf_CheckoutHeader-Desktop";

Handlebars.registerPartial(
  "changeCurrency",
  $("#changeCurrencyTemplate").html()
);
// OVERRIDES END HERE
