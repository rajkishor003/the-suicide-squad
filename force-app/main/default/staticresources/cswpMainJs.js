//----------FUNCTIONS----------//

// HEADER SEARCH INPUT
function searchStylingOverride() {

    //styles
    let transation = 'transition: width 0.8s;';
    let borderSolid = 'border:solid #90A4AE 1px !important;';
    let borderNone = 'border:none !important;';
    let widthFull = 'width: 100% !important;';
    let widthFixed = 'width: 38px !important;';

    //states
    function setHoverState() {
        $(".search-region").hover(function () {
            $(this).attr('style', widthFull + borderSolid + transation);
        }, function () {
            $(this).attr('style', widthFixed + borderNone + transation);
        });
    }

    function setFocusState() {
        $('.search-input').focus(function () {
            $('.search-region').attr('style', widthFull + borderSolid + transation);
            $('.search-region').off('mouseenter mouseleave');
        });

        $('.search-input').focusout(function () {
            $(this).val('');
            $('.search-region').attr('style', widthFixed + borderNone + transation);
            setHoverState();
        });
    }

    //set hover state
    setHoverState();

    //set focus state
    setFocusState();
}

$(document).ready(function () {
    //HEADER SEARCH INPUT
    searchStylingOverride();
});