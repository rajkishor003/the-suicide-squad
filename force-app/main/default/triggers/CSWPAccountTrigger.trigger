trigger CSWPAccountTrigger on Account (after insert) {
    Triggers.prepare()
        .afterInsert().bind(new CSWPAccountTriggerHandler())
        .execute();
}