trigger CSWPCalibrationBoxDataTrigger on Cswp_Calibration_Box_Data__c (after insert, after update, before insert) {
    Triggers.prepare()
    .afterUpdate().bind(new CSWPCalibrationBoxDataTriggerHandler())
    .beforeInsert().bind(new CSWPCalibrationBoxDataTriggerHandler())
    .afterInsert().bind(new CSWPCalibrationBoxDataTriggerHandler())    
    .execute();
}