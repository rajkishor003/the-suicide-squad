trigger CSWPCaseTrigger on Case (after insert,after update , after delete) {
    Triggers.prepare()
    .afterUpdate().bind(new CSWPCaseTriggerHandler())
    .afterInsert().bind(new CSWPCaseTriggerHandler())
    .afterDelete().bind(new CSWPCaseTriggerHandler())
    .execute();
}