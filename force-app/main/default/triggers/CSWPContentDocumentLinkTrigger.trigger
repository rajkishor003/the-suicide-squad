trigger CSWPContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    Triggers.prepare()
    .afterInsert().bind(new CSWPContentDocumentLinkTriggerHandler())
    .execute();
}