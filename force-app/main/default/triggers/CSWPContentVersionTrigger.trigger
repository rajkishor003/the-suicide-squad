trigger CSWPContentVersionTrigger on ContentVersion (after update,after insert) {
    Triggers.prepare()
        /*.afterUpdate() // moved it to the CSWPWorkspaceManagerController
            .bind(new CSWPContentVersionHandler())*/
        .afterInsert()
            .bind(new CSWPContentVersionHandler())
        .execute();
}