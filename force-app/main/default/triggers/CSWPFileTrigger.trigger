trigger CSWPFileTrigger on CSWP_File__c ( before update, after update, after delete, after insert) {
    Triggers.prepare()
        .beforeUpdate().bind(new CSWPFileTriggerHandler())
        .afterUpdate().bind(new CSWPFileTriggerHandler())
        .afterDelete().bind(new CSWPFileTriggerHandler())
        .afterInsert().bind(new CSWPFileTriggerHandler())
        .execute();
}