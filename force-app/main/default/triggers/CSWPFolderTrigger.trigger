trigger CSWPFolderTrigger on CSWP_Folder__c (after insert, after update,after delete) {
  Triggers.prepare()
      .afterInsert().bind(new CSWPFolderTriggerHandler())
      .afterUpdate().bind(new CSWPFolderTriggerHandler())
      .afterDelete().bind(new CSWPFolderTriggerHandler())
      .execute();
}