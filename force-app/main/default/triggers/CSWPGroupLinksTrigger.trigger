trigger CSWPGroupLinksTrigger on CSWP_Group_Links__c (after insert, after delete, after update) {
    Triggers.prepare()
      .afterInsert().bind(new CSWPGroupLinksTriggerHandler())
      .afterUpdate().bind(new CSWPGroupLinksTriggerHandler())
      .afterDelete().bind(new CSWPGroupLinksTriggerHandler())
    .execute();
}