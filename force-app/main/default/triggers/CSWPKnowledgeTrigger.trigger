trigger CSWPKnowledgeTrigger on Knowledge__kav (after insert, after update) {
    Triggers.prepare()
    .afterInsert().bind(new CSWPKnowledgeTriggerHandler())
    .afterUpdate().bind(new CSWPKnowledgeTriggerHandler())
    .execute();
}