trigger CSWPNotificationTrigger on cswp_Notification__c (before insert, before update) {
    List<cswp_Notification__c> notifications = Trigger.new;
    List<cswp_Notification__c> landingPageMessage = CSWPSobjectSelector.getLandinPageNotification();
    if(landingPageMessage.size()>0){
        Id recordTypeId = landingPageMessage.get(0).RecordTypeId;
        for (cswp_Notification__c n : notifications) {
            if(n.Id != landingPageMessage.get(0).Id){
                 if (n.RecordTypeId==recordTypeId ) {
                    n.addError(System.Label.cswp_landing_page_message_limit);
                 }
            }
           
        }
    }
  }