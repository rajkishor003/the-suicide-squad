trigger CSWPSystemTrigger on Cswp_System__c (after insert, after update)  {
    Triggers.prepare()
    .afterUpdate().bind(new CSWPSystemTriggerHandler())
    .afterInsert().bind(new CSWPSystemTriggerHandler())
    .execute();
}