trigger CSWP_AssignedPermissionSet_Trigger on CSWP_Assigned_Permission_Set__c (after delete, after insert) {
    Triggers.prepare()
            .afterDelete().bind(new CSWP_AssignedPermissionSet_Handler())
            .afterInsert().bind(new CSWP_AssignedPermissionSet_Handler())
            .execute();
}