trigger CSWP_Group_Trigger on cswp_Group__c (after insert, after update, after delete) {
  Triggers.prepare()
      .afterInsert().bind(new CSWP_Group_TriggerHandler())
      .afterUpdate().bind(new CSWP_Group_TriggerHandler())
      .afterDelete().bind(new CSWP_Group_TriggerHandler())
    .execute();
}