trigger CSWP_UserPermission_Trigger on CSWP_User_Permission__c (before insert, before update) {
    Triggers.prepare()
        .beforeInsert().bind(new CSWP_UserPermission_TriggerHandler())
        .beforeUpdate().bind(new CSWP_UserPermission_TriggerHandler())
    .execute();
}