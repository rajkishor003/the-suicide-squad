trigger CaseTrigger on Case (after insert,after update) {

    List<Case> toBeCreated = new List<Case>();
    if(Trigger.isAfter && Trigger.isInsert) {
        List<Jira_Configuration__mdt> jiraMappings = [SELECT MasterLabel, Jira_Issue_Type_ID__c, Jira_Project_ID__c FROM Jira_Configuration__mdt];
        if(!jiraMappings.isEmpty()){
            for(Jira_Configuration__mdt jc : jiraMappings) {
                for(Case c : Trigger.new) {
                    if(c.Type == jc.MasterLabel) {
                        toBeCreated.add(c);
                        JSFS.API.createJiraIssue(jc.Jira_Project_ID__c,jc.Jira_Issue_Type_ID__c,toBeCreated,null);
                    }
                }
            }
        }    
    }
    if(Trigger.isAfter && Trigger.isUpdate) {

        Map<Id,Id> caseToAccount = new Map<Id,Id>();
        Map<Id,Double> accountToTimeSpent = new Map<Id,Double>();
        Double totalTimeSpent = 0.0;
        for(Case c : Trigger.new) {
            caseToAccount.put(c.Id,c.AccountId);
        }
        if(!caseToAccount.isEmpty()) {
            List<Case> caseList = [SELECT Id,AccountId,AE_Time_Spent__c FROM Case WHERE AccountId IN :caseToAccount.values()];
            for(Case c : caseList) {
                if(c.AE_Time_Spent__c != null) {
                    totalTimeSpent += c.AE_Time_Spent__c;
                    accountToTimeSpent.put(c.AccountId,totalTimeSpent);
                }
            }
            List<CSWP_Contract__c> contractList = [SELECT AE_Total_Hours__c ,Cswp_Contract_Status__c,AE_Total_Time_Spent__c,Account__c FROM CSWP_Contract__c WHERE Account__c IN :caseToAccount.values()];
            if(!contractList.isEmpty()) {
                for(CSWP_Contract__c con : contractList) {
                    con.AE_Total_Time_Spent__c = accountToTimeSpent.get(con.Account__c);
                }
                update contractList;
            }
        }

        JSFS.API.pushUpdatesToJira(Trigger.new, null);
    
    }
}