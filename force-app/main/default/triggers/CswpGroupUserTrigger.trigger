trigger CswpGroupUserTrigger on cswp_group_user__c (after update) {
    if (Trigger.isAfter && Trigger.isUpdate) {
        Set<Id> userIds = new Set<Id>();
        Set<Id> groupIds = new Set<Id>();
        Map<Id,Set<Id>> userToPublicGroupIds = new Map<Id,Set<Id>>();
        List<cswp_group_user__c> permissionSetShouldBeRemoved = new List<cswp_group_user__c>();
        List<cswp_group_user__c> permissionSetShouldBeAdd = new List<cswp_group_user__c>();
        List<cswp_group_user__c> cguList = new List<cswp_group_user__c>();
        try {
            for(cswp_group_user__c cgu : Trigger.new) {
                System.debug('cgu === ' + cgu);
                if(!cgu.Cswp_Is_Active__c) {
                    userIds.add(cgu.Cswp_User__c);
                    groupIds.add(cgu.Cswp_Public_Group_ID__c);
                    permissionSetShouldBeRemoved.add(cgu);
                    
                }
                if(cgu.Cswp_Is_Active__c) {
                    cguList.add(cgu);
                    permissionSetShouldBeAdd.add(cgu);
                }
            }
            for(cswp_group_user__c cgu : Trigger.new) {
                System.debug('cgu ====> ' + cgu);
                if(userToPublicGroupIds.get(cgu.Cswp_User__c) == null){
                    userToPublicGroupIds.put(cgu.Cswp_User__c, new Set<Id>());
                }
                userToPublicGroupIds.get(cgu.Cswp_User__c).add(cgu.Cswp_Public_Group_ID__c);
            }
            
            if(!userIds.isEmpty() && !groupIds.isEmpty()) {
                List<GroupMember> toBeRemovedList = new List<GroupMember>();
                List<GroupMember> gmList = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember Where UserOrGroupId IN: userIds AND GroupId IN: groupIds];
                for(Id uId : userIds) {
                    for(GroupMember gm : gmList) {
                        if(gm.UserOrGroupId == uId && userToPublicGroupIds.get(uID).contains(gm.GroupId)) {
                            toBeRemovedList.add(gm);
                        }
                    }
                }
                CSWPCustomerAdminOperations.deleteGroupMembers(JSON.serialize(toBeRemovedList));
                CSWPCustomerAdminOperations.RemovePermissionSetFromCswpGrupUsers(JSON.serialize(permissionSetShouldBeRemoved));
                
            }

            if(!cguList.isEmpty()) {
                CSWPCustomerAdminOperations.createGroupMembers(JSON.serialize(cguList));
                CSWPCustomerAdminOperations.AddPermissionSetToCswpGrupUsers(JSON.serialize(permissionSetShouldBeAdd));
            }

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
    }
}