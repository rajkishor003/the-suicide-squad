trigger cswp_Workspace_Trigger on cswp_Workspace__c (after insert, after update) {
    Triggers.prepare()
        .afterInsert()
            .bind(new cswp_Workspace_TriggerHandler())
            .bind(new CswpWorkspaceTriggerHandler())
        .afterUpdate()
            .bind(new cswp_Workspace_TriggerHandler())
            .bind(new CswpWorkspaceTriggerHandler())
        .execute();
}