trigger feedCommentTrigger on FeedComment (before insert, before update, after insert) {
    if(Trigger.isBefore && Trigger.isInsert) {
        for(FeedComment fc : Trigger.new) {
            if(String.valueOf(fc.ParentId).startsWith('500')) {
                fc.CommentBody = fc.CommentBody.replaceAll('(?i)<(?!img|/img).*?>', '');
            }
        }
    }
}