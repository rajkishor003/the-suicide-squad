trigger feedItemTrigger on FeedItem (before insert, after insert) {
    Triggers.prepare()
    .beforeInsert().bind(new CswpFeedItemTriggerHandler())
    .afterInsert().bind(new CswpFeedItemTriggerHandler())
    .execute();
}