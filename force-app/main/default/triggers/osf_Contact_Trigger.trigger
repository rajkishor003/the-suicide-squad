/**
 * File:        osf_Contact_Trigger
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Jan 06, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Trigger for Contact object
  ************************************************************************
 * History:
 */

trigger osf_Contact_Trigger on Contact (after insert, after update) {
    if(Trigger.IsAfter) {
        if(Trigger.IsInsert || Trigger.IsUpdate) {
            try {
                osf_Contact_TriggerHandler.createCommunityUsers(Trigger.newMap);  
            } catch(exception e) {
                ccrz.ccLog.log(LoggingLevel.ERROR, 'osf:contact_triggerhandler.createCommunityUsers:Error', e.getMessage() + 'in line number: ' + e.getLineNumber());
                Trigger.New[0].addError(e.getMessage());
            }            
        }
    }
}