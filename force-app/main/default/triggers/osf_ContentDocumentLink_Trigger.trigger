trigger osf_ContentDocumentLink_Trigger on ContentDocumentLink (before insert) {
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            osf_ContentDocumentLink_TriggerHandler.doBeforeInsert(Trigger.new);
        }
    }
}