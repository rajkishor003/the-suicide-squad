trigger osf_FeedItem_Trigger on FeedItem (after insert) {
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            osf_FeedItem_TriggerHandler.doAfterInsert(Trigger.newMap);
        }
    }
}