/**
 * File:        osf_ccAccountGroupPriceList_Trigger
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Mar 24, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Trigger for CC AccountGroupPriceList object
  ************************************************************************
 * History:
 */

trigger osf_ccAccountGroupPriceList_Trigger on ccrz__E_AccountGroupPriceList__c (after insert, after update, after delete, after undelete) {
    if(Trigger.IsAfter) {
        Database.executeBatch(new osf_allowedCategoryJob(osf_constant_strings.STOREFRONT_NAME, osf_constant_strings.CURRENCY_LIST, osf_constant_strings.COUNTRY_LOCALE_SID_MAP.values()), 10);
    }
}