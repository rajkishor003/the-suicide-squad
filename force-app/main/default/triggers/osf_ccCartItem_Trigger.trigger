/**
 * File:        osf_ccCartItem_Trigger
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Dec 11, 2019
 * Created By:  Ozgun Eser
  ************************************************************************
 * Description: Trigger for CC Cart Item object
  ************************************************************************
 * History:
 */

trigger osf_ccCartItem_Trigger on ccrz__E_CartItem__c (before delete) {
    if(Trigger.IsBefore) {
        if(Trigger.IsDelete) {
            osf_ccCartItem_TriggerHandler.doBeforeDelete(Trigger.old);
        }
    }
}