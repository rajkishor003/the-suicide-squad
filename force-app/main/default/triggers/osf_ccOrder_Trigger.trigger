/**
 * File:        osf_ccOrder_Trigger
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        Nov 08, 2019
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Trigger for CC Order object
  ************************************************************************
 * History:
 */
trigger osf_ccOrder_Trigger on ccrz__E_Order__c (after insert, after update, before update) {
    if(Trigger.IsAfter) {
        if(Trigger.IsInsert) {
            osf_ccOrder_TriggerHandler.doAfterInsert(Trigger.new);
        } else if(Trigger.IsUpdate) {
            osf_ccOrder_TriggerHandler.doAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    } else {
        if(Trigger.IsUpdate) {
            osf_ccOrder_TriggerHandler.checkSalesOrderAcknowledgement(Trigger.newMap, Trigger.oldMap);
        }
    }
}