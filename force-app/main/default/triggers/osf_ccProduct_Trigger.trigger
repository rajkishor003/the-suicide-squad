/**
 * File:        osf_ccProduct_Trigger
 * Project:     Advantest (JP-ADVTEST-IFB-Advantest B2B Implementation (CCRZ))
 * Date:        May 26, 2020
 * Created By:  Alina Craciunel
  ************************************************************************
 * Description: Trigger for CC Product object
  ************************************************************************
 * History:
 */

trigger osf_ccProduct_Trigger on ccrz__E_Product__c (after insert, after update, after delete, after undelete) {
    osf_Product_TriggerHandler.runCategoriesJob(Trigger.newMap, Trigger.oldMap, Trigger.isUpdate);
}