trigger osf_requestForQuote_Trigger on osf_request_for_quote__c (before insert, after insert, before update, after update) {
    if(Trigger.IsAfter) {
        if(Trigger.IsInsert) {
            osf_requestForQuote_TriggerHandler.doAfterInsert(Trigger.new);
        } else if(Trigger.IsUpdate) {
            osf_requestForQuote_TriggerHandler.doAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    } else {
        if(Trigger.IsInsert) {
            osf_requestForQuote_TriggerHandler.doBeforeInsert(Trigger.new);
        } else if (Trigger.IsUpdate) {
            osf_requestForQuote_TriggerHandler.doBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
}